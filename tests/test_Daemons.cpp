#include "catch.hpp"

#include "Daemons.h"

enum daemon_id {
    d_none = 0,

    d_rollwand,
    d_doctor,
    d_stomach,
    d_runners,
    d_swander,
    d_nohste,
    d_unconfuse,
    d_unsee,
    d_sight,

    d_dont_save,

    d_nohaste,
    d_come_down,
    d_visuals,
    d_turn_see,
    d_land,

};

// FIXME: This warning seems more annoying than anything...
#pragma clang diagnostic ignored "-Wweak-vtables"

#include <vector>
using namespace std;

static vector<daemon_id> test_exec;

class TestDaemons : public Daemons<daemon_id> {
public:
    void executeAction(const Action &a) override {
        test_exec.push_back(a.d_id);
    }
};

TEST_CASE("Daemons: Daemons") {
    TestDaemons daemons;

    SECTION("actions should be empty") {
        REQUIRE(daemons.actions.empty());
    }

    SECTION("add a daemon and make sure it's there") {
        daemons.startDaemon(d_stomach, 0, TestDaemons::Before);
        REQUIRE(daemons.actions.size() == 1);
        REQUIRE(daemons.actions.front().d_id == d_stomach);

        SECTION("add another daemon and make sure it's after the first") {
            daemons.startDaemon(d_rollwand, 0, TestDaemons::Before);
            REQUIRE(daemons.actions.size() == 2);
            REQUIRE((++daemons.actions.begin())->d_id == d_rollwand);

            SECTION("run daemons in Before") {
                test_exec.clear();
                daemons.doDaemons(TestDaemons::Before);
                REQUIRE(test_exec.size() == 2);
                REQUIRE(test_exec[0] == d_stomach);
                REQUIRE(test_exec[1] == d_rollwand);
            }

            SECTION("run daemons in After") {
                test_exec.clear();
                daemons.doDaemons(TestDaemons::After);
                REQUIRE(test_exec.empty());
            }
        }
    }
}

TEST_CASE("Daemons: Fuses") {
    TestDaemons daemons;

    SECTION("add a fuse and make sure it's there") {
        daemons.startFuse(d_swander, 0, 1, TestDaemons::Before);
        REQUIRE(daemons.actions.size() == 1);
        REQUIRE(daemons.actions.front().d_id == d_swander);

        SECTION("add another fuse and make sure it's after the first") {
            daemons.startFuse(d_unconfuse, 0, 3, TestDaemons::Before);
            REQUIRE(daemons.actions.size() == 2);
            REQUIRE((++daemons.actions.begin())->d_id == d_unconfuse);

            SECTION("run fuses in After") {
                test_exec.clear();
                daemons.doFuses(TestDaemons::After);
                REQUIRE(test_exec.empty());
            }

            SECTION("run fuses in Before, only one should fire") {
                test_exec.clear();
                daemons.doFuses(TestDaemons::Before);
                REQUIRE(test_exec.size() == 1);
                REQUIRE(test_exec[0] == d_swander);
                REQUIRE(daemons.actions.size() == 1);

                SECTION("run fuses in Before, none should fire") {
                    test_exec.clear();
                    daemons.doFuses(TestDaemons::Before);
                    REQUIRE(test_exec.empty());
                    REQUIRE(daemons.actions.size() == 1);

                    SECTION("run fuses in Before, the last should fire") {
                        test_exec.clear();
                        daemons.doFuses(TestDaemons::Before);
                        REQUIRE(test_exec.size() == 1);
                        REQUIRE(test_exec[0] == d_unconfuse);
                        REQUIRE(daemons.actions.empty());
                    }
                }
            }
        }
    }

    SECTION("add fuse, lengthen fuse, try to make sure it adds up") {
        test_exec.clear();
        daemons.startFuse(d_swander, 0, 2, TestDaemons::Before);
        daemons.doFuses(TestDaemons::Before);
        REQUIRE(test_exec.empty());

        daemons.lengthen(d_swander, 2);
        REQUIRE(daemons.actions.front().d_time == 3);

        daemons.doFuses(TestDaemons::Before);
        REQUIRE(daemons.actions.size() == 1);
        daemons.doFuses(TestDaemons::Before);
        REQUIRE(daemons.actions.front().d_time == 1);
        daemons.doFuses(TestDaemons::Before);
        REQUIRE(daemons.actions.empty());
        REQUIRE(test_exec.size() == 1);
        REQUIRE(test_exec[0] == d_swander);
    }

    SECTION("mix before and after fuses and make sure the only trigger one or the other") {
        daemons.startFuse(d_swander, 0, 1, TestDaemons::Before);
        daemons.startFuse(d_nohaste, 0, 1, TestDaemons::After);
        daemons.startFuse(d_unconfuse, 0, 1, TestDaemons::Before);
        daemons.startFuse(d_sight, 0, 1, TestDaemons::After);
        REQUIRE(daemons.actions.size() == 4);

        test_exec.clear();
        daemons.doFuses(TestDaemons::Before);
        REQUIRE(daemons.actions.size() == 2);
        REQUIRE(test_exec.size() == 2);
        REQUIRE(test_exec[0] == d_swander);
        REQUIRE(test_exec[1] == d_unconfuse);

        test_exec.clear();
        daemons.doFuses(TestDaemons::After);
        REQUIRE(daemons.actions.empty());
        REQUIRE(test_exec.size() == 2);
        REQUIRE(test_exec[0] == d_nohaste);
        REQUIRE(test_exec[1] == d_sight);
    }

    SECTION("add and extingush some fuses") {
        daemons.startFuse(d_swander, 0, 1, TestDaemons::Before);
        daemons.startFuse(d_nohaste, 0, 1, TestDaemons::After);
        daemons.startFuse(d_unconfuse, 0, 1, TestDaemons::Before);
        daemons.startFuse(d_sight, 0, 1, TestDaemons::After);
        REQUIRE(daemons.actions.size() == 4);
        daemons.extinguish(d_swander);
        daemons.extinguish(d_unconfuse);
        daemons.extinguish(d_sight);
        REQUIRE(daemons.actions.size() == 1);
        REQUIRE(daemons.actions.front().d_id == d_nohaste);
    }
}
