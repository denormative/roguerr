
#define CATCH_CONFIG_MAIN
#include "catch.hpp"

TEST_CASE("Sanity test") {
    const int FT = 42;

    REQUIRE(FT == 42);
}
