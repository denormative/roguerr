/*
    state.c - Portable Rogue Save State Code

    Copyright (C) 1999, 2000, 2005 Nicholas J. Kisseberth
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions
    are met:
    1. Redistributions of source code must retain the above copyright
       notice, this list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright
       notice, this list of conditions and the following disclaimer in the
       documentation and/or other materials provided with the distribution.
    3. Neither the name(s) of the author(s) nor the names of other contributors
       may be used to endorse or promote products derived from this software
       without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE AUTHOR(S) AND CONTRIBUTORS ``AS IS'' AND
    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR(S) OR CONTRIBUTORS BE LIABLE
    FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
    DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
    OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
    HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
    LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
    OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
    SUCH DAMAGE.
*/

/************************************************************************/
/* Save State Code                                                      */
/************************************************************************/

#define RSID_STATS 0xABCD0001
#define RSID_MSTATS 0xABCD0002
#define RSID_THING 0xABCD0003
#define RSID_OBJECT 0xABCD0004
#define RSID_MAGICITEMS 0xABCD0005
#define RSID_KNOWS 0xABCD0006
#define RSID_GUESSES 0xABCD0007
#define RSID_OBJECTLIST 0xABCD0008
#define RSID_BAGOBJECT 0xABCD0009
#define RSID_MONSTERLIST 0xABCD000A
#define RSID_MONSTERSTATS 0xABCD000B
#define RSID_MONSTERS 0xABCD000C
#define RSID_TRAP 0xABCD000D
#define RSID_WINDOW 0xABCD000E
#define RSID_DAEMONS 0xABCD000F
#define RSID_STICKS 0xABCD0010
#define RSID_IARMOR 0xABCD0011
#define RSID_SPELLS 0xABCD0012
#define RSID_ILIST 0xABCD0013
#define RSID_HLIST 0xABCD0014
#define RSID_DEATHTYPE 0xABCD0015
#define RSID_CTYPES 0XABCD0016
#define RSID_COORDLIST 0XABCD0017
#define RSID_ROOMS 0XABCD0018

#include "rogue.h"

#define READSTAT (format_error || read_error)
#define WRITESTAT (write_error)

static int read_error = FALSE;
static int write_error = FALSE;
static int format_error = FALSE;
static int endian = 0x01020304;
#define big_endian (*((char *)&endian) == 0x01)

int rs_write(FILE *savef, void *ptr, size_t size) {
    if(write_error) {
        return (WRITESTAT);
    }

    if(encwrite(ptr, (size_t)size, savef) != size) {
        write_error = 1;
    }

    return (WRITESTAT);
}

int rs_read(FILE *inf, void *ptr, size_t size) {
    if(read_error || format_error) {
        return (READSTAT);
    }

    if(encread((char *)ptr, size, inf) != size) {
        read_error = 1;
    }

    return (READSTAT);
}

int rs_write_uchar(FILE *savef, unsigned char c) {
    if(write_error) {
        return (WRITESTAT);
    }

    rs_write(savef, &c, 1);

    return (WRITESTAT);
}

int rs_read_uchar(FILE *inf, unsigned char *c) {
    if(read_error || format_error) {
        return (READSTAT);
    }

    rs_read(inf, c, 1);

    return (READSTAT);
}

int rs_write_char(FILE *savef, char c) {
    if(write_error) {
        return (WRITESTAT);
    }

    rs_write(savef, &c, 1);

    return (WRITESTAT);
}

int rs_read_char(FILE *inf, char *c) {
    if(read_error || format_error) {
        return (READSTAT);
    }

    rs_read(inf, c, 1);

    return (READSTAT);
}

int rs_write_chars(FILE *savef, char *c, int count) {
    if(write_error) {
        return (WRITESTAT);
    }

    rs_write_int(savef, count);
    rs_write(savef, c, (size_t)count);

    return (WRITESTAT);
}

int rs_read_chars(FILE *inf, char *i, int count) {
    int value = 0;

    if(read_error || format_error) {
        return (READSTAT);
    }

    rs_read_int(inf, &value);

    if(value != count) {
        format_error = TRUE;
    }

    rs_read(inf, i, (size_t)count);

    return (READSTAT);
}

int rs_write_int(FILE *savef, int c) {
    unsigned char bytes[4];
    auto *buf = (unsigned char *)&c;

    if(write_error) {
        return (WRITESTAT);
    }

    if(big_endian) {
        bytes[3] = buf[0];
        bytes[2] = buf[1];
        bytes[1] = buf[2];
        bytes[0] = buf[3];
        buf = bytes;
    }

    rs_write(savef, buf, 4);

    return (WRITESTAT);
}

int rs_read_int(FILE *inf, int *i) {
    unsigned char bytes[4];
    int input = 0;
    auto *buf = (unsigned char *)&input;

    if(read_error || format_error) {
        return (READSTAT);
    }

    rs_read(inf, &input, 4);

    if(big_endian) {
        bytes[3] = buf[0];
        bytes[2] = buf[1];
        bytes[1] = buf[2];
        bytes[0] = buf[3];
        buf = bytes;
    }

    *i = *((int *)buf);

    return (READSTAT);
}

int rs_write_ints(FILE *savef, int *c, int count) {
    int n = 0;

    if(write_error) {
        return (WRITESTAT);
    }

    rs_write_int(savef, count);

    for(n = 0; n < count; n++) {
        if(rs_write_int(savef, c[n]) != 0) {
            break;
        }
    }

    return (WRITESTAT);
}

int rs_read_ints(FILE *inf, int *i, int count) {
    int n, value;

    if(read_error || format_error) {
        return (READSTAT);
    }

    rs_read_int(inf, &value);

    if(value != count) {
        format_error = TRUE;
    }

    for(n = 0; n < count; n++) {
        if(rs_read_int(inf, &i[n]) != 0) {
            break;
        }
    }

    return (READSTAT);
}

int rs_write_boolean(FILE *savef, bool c) {
    unsigned char buf = (c == 0) ? 0 : 1;

    if(write_error) {
        return (WRITESTAT);
    }

    rs_write(savef, &buf, 1);

    return (WRITESTAT);
}

int rs_read_boolean(FILE *inf, bool *i) {
    unsigned char buf = 0;

    if(read_error || format_error) {
        return (READSTAT);
    }

    rs_read(inf, &buf, 1);

    *i = (buf != 0);

    return (READSTAT);
}

int rs_write_booleans(FILE *savef, bool *c, int count) {
    int n = 0;

    if(write_error) {
        return (WRITESTAT);
    }

    rs_write_int(savef, count);

    for(n = 0; n < count; n++) {
        if(rs_write_boolean(savef, c[n]) != 0) {
            break;
        }
    }

    return (WRITESTAT);
}

int rs_read_booleans(FILE *inf, bool *i, int count) {
    int n = 0, value = 0;

    if(read_error || format_error) {
        return (READSTAT);
    }

    rs_read_int(inf, &value);

    if(value != count) {
        format_error = TRUE;
    }

    for(n = 0; n < count; n++) {
        if(rs_read_boolean(inf, &i[n]) != 0) {
            break;
        }
    }

    return (READSTAT);
}

int rs_write_uint(FILE *savef, unsigned int c) {
    unsigned char bytes[4];
    auto *buf = (unsigned char *)&c;

    if(write_error) {
        return (WRITESTAT);
    }

    if(big_endian) {
        bytes[3] = buf[0];
        bytes[2] = buf[1];
        bytes[1] = buf[2];
        bytes[0] = buf[3];
        buf = bytes;
    }

    rs_write(savef, buf, 4);

    return (WRITESTAT);
}

int rs_read_uint(FILE *inf, unsigned int *i) {
    unsigned char bytes[4];
    int input;
    auto *buf = (unsigned char *)&input;

    if(read_error || format_error) {
        return (READSTAT);
    }

    rs_read(inf, &input, 4);

    if(big_endian) {
        bytes[3] = buf[0];
        bytes[2] = buf[1];
        bytes[1] = buf[2];
        bytes[0] = buf[3];
        buf = bytes;
    }

    *i = *((unsigned int *)buf);

    return (READSTAT);
}

int rs_write_long(FILE *savef, long c) {
    int c2;
    unsigned char bytes[4];
    auto *buf = (unsigned char *)&c;

    if(write_error) {
        return (WRITESTAT);
    }

    if(sizeof(long) == 8) {
        c2 = (int)c;
        buf = (unsigned char *)&c2;
    }

    if(big_endian) {
        bytes[3] = buf[0];
        bytes[2] = buf[1];
        bytes[1] = buf[2];
        bytes[0] = buf[3];
        buf = bytes;
    }

    rs_write(savef, buf, 4);

    return (WRITESTAT);
}

int rs_read_long(FILE *inf, long *i) {
    unsigned char bytes[4];
    long input = 0;
    auto *buf = (unsigned char *)&input;

    if(read_error || format_error) {
        return (READSTAT);
    }

    rs_read(inf, &input, 4);

    if(big_endian) {
        bytes[3] = buf[0];
        bytes[2] = buf[1];
        bytes[1] = buf[2];
        bytes[0] = buf[3];
        buf = bytes;
    }

    *i = *((long *)buf);

    return (READSTAT);
}

int rs_write_longs(FILE *savef, long *c, int count) {
    int n = 0;

    if(write_error) {
        return (WRITESTAT);
    }

    rs_write_int(savef, count);

    for(n = 0; n < count; n++) {
        rs_write_long(savef, c[n]);
    }

    return (WRITESTAT);
}

int rs_read_longs(FILE *inf, long *i, int count) {
    int n = 0, value = 0;

    if(read_error || format_error) {
        return (READSTAT);
    }

    rs_read_int(inf, &value);

    if(value != count) {
        format_error = TRUE;
    }

    for(n = 0; n < value; n++) {
        if(rs_read_long(inf, &i[n]) != 0) {
            break;
        }
    }

    return (READSTAT);
}

int rs_write_ulong(FILE *savef, unsigned long c) {
    unsigned int c2;
    unsigned char bytes[4];
    auto *buf = (unsigned char *)&c;

    if(write_error) {
        return (WRITESTAT);
    }

    if((sizeof(long) == 8) && (sizeof(int) == 4)) {
        c2 = (unsigned int)c;
        buf = (unsigned char *)&c2;
    }

    if(big_endian) {
        bytes[3] = buf[0];
        bytes[2] = buf[1];
        bytes[1] = buf[2];
        bytes[0] = buf[3];
        buf = bytes;
    }

    rs_write(savef, buf, 4);

    return (WRITESTAT);
}

int rs_read_ulong(FILE *inf, unsigned long *i) {
    unsigned char bytes[4];
    unsigned long input = 0;
    auto *buf = (unsigned char *)&input;

    if(read_error || format_error) {
        return (READSTAT);
    }

    rs_read(inf, &input, 4);

    if(big_endian) {
        bytes[3] = buf[0];
        bytes[2] = buf[1];
        bytes[1] = buf[2];
        bytes[0] = buf[3];
        buf = bytes;
    }

    *i = *((unsigned long *)buf);

    return (READSTAT);
}

int rs_write_ulongs(FILE *savef, unsigned long *c, int count) {
    int n = 0;

    if(write_error) {
        return (WRITESTAT);
    }

    rs_write_int(savef, count);

    for(n = 0; n < count; n++) {
        if(rs_write_ulong(savef, c[n]) != 0) {
            break;
        }
    }

    return (WRITESTAT);
}

int rs_read_ulongs(FILE *inf, unsigned long *i, int count) {
    int n = 0, value = 0;

    if(read_error || format_error) {
        return (READSTAT);
    }

    rs_read_int(inf, &value);

    if(value != count) {
        format_error = TRUE;
    }

    for(n = 0; n < count; n++) {
        if(rs_read_ulong(inf, &i[n]) != 0) {
            break;
        }
    }

    return (READSTAT);
}

int rs_write_marker(FILE *savef, unsigned int id) {
    if(write_error) {
        return (WRITESTAT);
    }

    rs_write_int(savef, (int)id);

    return (WRITESTAT);
}

int rs_read_marker(FILE *inf, unsigned int id) {
    int nid;

    if(read_error || format_error) {
        return (READSTAT);
    }

    if(rs_read_int(inf, &nid) == 0) {
        if(id != (unsigned int)nid) {
            format_error = 1;
        }
    }

    return (READSTAT);
}

/******************************************************************************/

int rs_write_string(FILE *savef, char *s) {
    int len = 0;

    if(write_error) {
        return (WRITESTAT);
    }

    len = (s == nullptr) ? 0 : (int)strlen(s) + 1;

    rs_write_int(savef, len);
    rs_write_chars(savef, s, len);

    return (WRITESTAT);
}

int rs_read_string(FILE *inf, char *s, int max) {
    int len = 0;

    if(read_error || format_error) {
        return (READSTAT);
    }

    rs_read_int(inf, &len);

    if(len > max) {
        format_error = TRUE;
    }

    rs_read_chars(inf, s, len);

    return (READSTAT);
}

int rs_read_new_string(FILE *inf, char **s) {
    int len = 0;
    char *buf = nullptr;

    if(read_error || format_error) {
        return (READSTAT);
    }

    rs_read_int(inf, &len);

    if(len == 0) {
        buf = nullptr;
    } else {
        buf = (char *)malloc((size_t)len);

        if(buf == nullptr) {
            read_error = TRUE;
        }
    }

    rs_read_chars(inf, buf, len);

    *s = buf;

    return (READSTAT);
}

int rs_write_strings(FILE *savef, char *s[], int count) {
    int n = 0;

    if(write_error) {
        return (WRITESTAT);
    }

    rs_write_int(savef, count);

    for(n = 0; n < count; n++) {
        if(rs_write_string(savef, s[n]) != 0) {
            break;
        }
    }

    return (WRITESTAT);
}

int rs_read_strings(FILE *inf, char **s, int count, int max) {
    int n = 0;
    int value = 0;

    if(read_error || format_error) {
        return (READSTAT);
    }

    rs_read_int(inf, &value);

    if(value != count) {
        format_error = TRUE;
    }

    for(n = 0; n < count; n++) {
        if(rs_read_string(inf, s[n], max) != 0) {
            break;
        }
    }

    return (READSTAT);
}

int rs_read_new_strings(FILE *inf, char **s, int count) {
    int n = 0;
    int value = 0;

    if(read_error || format_error) {
        return (READSTAT);
    }

    rs_read_int(inf, &value);

    if(value != count) {
        format_error = TRUE;
    }

    for(n = 0; n < count; n++) {
        if(rs_read_new_string(inf, &s[n]) != 0) {
            break;
        }
    }

    return (READSTAT);
}

int rs_write_string_index(FILE *savef, const char *master[], int max, const char *str) {
    int i;

    if(write_error) {
        return (WRITESTAT);
    }

    for(i = 0; i < max; i++) {
        if(str == master[i]) {
            return (rs_write_int(savef, i));
        }
    }

    return (rs_write_int(savef, -1));
}

int rs_read_string_index(FILE *inf, const char **master, int maxindex, const char **str) {
    int i;

    if(read_error || format_error) {
        return (READSTAT);
    }

    rs_read_int(inf, &i);

    if(i > maxindex) {
        format_error = TRUE;
    } else if(i >= 0) {
        *str = master[i];
    } else {
        *str = nullptr;
    }

    return (READSTAT);
}

int rs_write_coord(FILE *savef, coord c) {
    if(write_error) {
        return (WRITESTAT);
    }

    rs_write_int(savef, c.x);
    rs_write_int(savef, c.y);

    return (WRITESTAT);
}

int rs_read_coord(FILE *inf, coord *c) {
    coord in;

    if(read_error || format_error) {
        return (READSTAT);
    }

    rs_read_int(inf, &in.x);
    rs_read_int(inf, &in.y);

    if(READSTAT == 0) {
        c->x = in.x;
        c->y = in.y;
    }

    return (READSTAT);
}

int rs_write_coord_list(FILE *savef, linked_list *l) {
    rs_write_marker(savef, RSID_COORDLIST);
    rs_write_int(savef, list_size(l));

    while(l != nullptr) {
        rs_write_coord(savef, *(coord *)l->l_data);
        l = l->l_next;
    }

    return (WRITESTAT);
}

int rs_read_coord_list(FILE *inf, linked_list **list) {
    int i, cnt;
    linked_list *l = nullptr, *previous = nullptr, *head = nullptr;

    rs_read_marker(inf, RSID_COORDLIST);

    if(rs_read_int(inf, &cnt) != 0) {
        return (READSTAT);
    }

    for(i = 0; i < cnt; i++) {
        l = new_item(sizeof(coord));
        l->l_prev = previous;

        if(previous != nullptr) {
            previous->l_next = l;
        }

        rs_read_coord(inf, (coord *)l->l_data);

        if(previous == nullptr) {
            head = l;
        }

        previous = l;
    }

    if(l != nullptr) {
        l->l_next = nullptr;
    }

    *list = head;

    return (READSTAT);
}

int rs_write_window(FILE *savef, WINDOW *win) {
    int row, col, height, width;

    if(write_error) {
        return (WRITESTAT);
    }

    width = getmaxx(win);
    height = getmaxy(win);

    rs_write_marker(savef, RSID_WINDOW);
    rs_write_int(savef, height);
    rs_write_int(savef, width);

    for(row = 0; row < height; row++) {
        for(col = 0; col < width; col++) {
            if(rs_write_int(savef, (int)mvwinch(win, row, col)) != 0) {
                return (WRITESTAT);
            }
        }
    }

    return (WRITESTAT);
}

int rs_read_window(FILE *inf, WINDOW *win) {
    int row, col, maxlines, maxcols, value, width, height;

    if(read_error || format_error) {
        return (READSTAT);
    }

    width = getmaxx(win);
    height = getmaxy(win);

    rs_read_marker(inf, RSID_WINDOW);

    rs_read_int(inf, &maxlines);
    rs_read_int(inf, &maxcols);

    for(row = 0; row < maxlines; row++) {
        for(col = 0; col < maxcols; col++) {
            if(rs_read_int(inf, &value) != 0) {
                return (READSTAT);
            }

            if((row < height) && (col < width)) {
                mvwaddch_int(win, row, col, value);
            }
        }
    }

    return (READSTAT);
}

/******************************************************************************/

void *get_list_item(linked_list *l, long i) {
    int count;

    for(count = 0; l != nullptr; count++, l = l->l_next) {
        if(count == i) {
            return (l->l_data);
        }
    }

    return (nullptr);
}

int find_list_ptr(linked_list *l, void *ptr) {
    int count;

    for(count = 0; l != nullptr; count++, l = l->l_next) {
        if(l->l_data == ptr) {
            return (count);
        }
    }

    return (-1);
}

int list_size(linked_list *l) {
    int count;

    for(count = 0; l != nullptr; count++, l = l->l_next) {
        ;
    }

    return (count);
}

/******************************************************************************/

int rs_write_levtype(FILE *savef, LEVTYPE c) {
    int lt;

    switch(c) {
        case NORMLEV:
            lt = 1;
            break;
        case POSTLEV:
            lt = 2;
            break;
        case MAZELEV:
            lt = 3;
            break;
        case OUTSIDE:
            lt = 4;
            break;
        case STARTLEV:
            lt = 5;
            break;
            // default:
            //    lt = -1;
            //    break;
    }

    rs_write_int(savef, lt);

    return (WRITESTAT);
}

int rs_read_levtype(FILE *inf, LEVTYPE *l) {
    int lt;

    rs_read_int(inf, &lt);

    switch(lt) {
        case 1:
            *l = NORMLEV;
            break;
        case 2:
            *l = POSTLEV;
            break;
        case 3:
            *l = MAZELEV;
            break;
        case 4:
            *l = OUTSIDE;
            break;
        case 5:
            *l = STARTLEV;
            break;
        default:
            *l = NORMLEV;
            break;
    }

    return (READSTAT);
}

int rs_write_stats(FILE *savef, stats *s) {
    if(write_error) {
        return (WRITESTAT);
    }

    rs_write_marker(savef, RSID_STATS);
    rs_write_long(savef, s->s_str);
    rs_write_long(savef, s->s_intel);
    rs_write_long(savef, s->s_wisdom);
    rs_write_long(savef, s->s_dext);
    rs_write_long(savef, s->s_const);
    rs_write_long(savef, s->s_charisma);
    rs_write_long(savef, s->s_exp);
    rs_write_int(savef, s->s_lvladj);
    rs_write_long(savef, s->s_lvl);
    rs_write_long(savef, s->s_arm);
    rs_write_long(savef, s->s_hpt);
    rs_write_long(savef, s->s_pack);
    rs_write_long(savef, s->s_carry);
    rs_write(savef, s->s_dmg, sizeof(s->s_dmg));

    return (WRITESTAT);
}

int rs_read_stats(FILE *inf, stats *s) {
    if(read_error || format_error) {
        return (READSTAT);
    }

    rs_read_marker(inf, RSID_STATS);
    rs_read_long(inf, &s->s_str);
    rs_read_long(inf, &s->s_intel);
    rs_read_long(inf, &s->s_wisdom);
    rs_read_long(inf, &s->s_dext);
    rs_read_long(inf, &s->s_const);
    rs_read_long(inf, &s->s_charisma);
    rs_read_long(inf, &s->s_exp);
    rs_read_int(inf, &s->s_lvladj);
    rs_read_long(inf, &s->s_lvl);
    rs_read_long(inf, &s->s_arm);
    rs_read_long(inf, &s->s_hpt);
    rs_read_long(inf, &s->s_pack);
    rs_read_long(inf, &s->s_carry);

    rs_read(inf, s->s_dmg, sizeof(s->s_dmg));

    return (READSTAT);
}

int rs_write_magic_items(FILE *savef, magic_item *i, int count) {
    int n;

    rs_write_marker(savef, RSID_MAGICITEMS);
    rs_write_int(savef, count);

    for(n = 0; n < count; n++) {
        /*      rs_write(savef,i[n].mi_name,sizeof(i[n].mi_name));  */
        rs_write_long(savef, i[n].mi_prob);
        /*      rs_write_int(savef,i[n].mi_worth);              */
        /*      rs_write_int(savef,i[n].mi_curse);              */
        /*      rs_write_int(savef,i[n].mi_bless);              */
    }

    return (WRITESTAT);
}

int rs_read_magic_items(FILE *inf, magic_item *mi, int count) {
    int n;
    int value;

    rs_read_marker(inf, RSID_MAGICITEMS);

    rs_read_int(inf, &value);

    if(value != count) {
        format_error = 1;
    } else {
        for(n = 0; n < value; n++) {
            /*         rs_read(inf,mi[n].mi_name,sizeof(mi[n].mi_name));     */
            rs_read_long(inf, &mi[n].mi_prob);
            /*          rs_read_int(inf,&mi[n].mi_worth);               */
            /*          rs_read_int(inf,&mi[n].mi_curse);               */
            /*          rs_read_int(inf,&mi[n].mi_bless);               */
        }
    }

    return (READSTAT);
}

int rs_write_scrolls(FILE *savef) {
    int i;

    if(write_error) {
        return (WRITESTAT);
    }

    for(i = 0; i < MAXSCROLLS; i++) {
        rs_write_string(savef, s_names[i]);
        rs_write_boolean(savef, s_know[i]);
        rs_write_string(savef, s_guess[i]);
    }

    return (WRITESTAT);
}

int rs_read_scrolls(FILE *inf) {
    int i;

    if(read_error || format_error) {
        return (READSTAT);
    }

    for(i = 0; i < MAXSCROLLS; i++) {
        rs_read_new_string(inf, &s_names[i]);
        rs_read_boolean(inf, &s_know[i]);
        rs_read_new_string(inf, &s_guess[i]);
    }

    return (READSTAT);
}

int rs_write_potions(FILE *savef) {
    int i;

    if(write_error) {
        return (WRITESTAT);
    }

    for(i = 0; i < MAXPOTIONS; i++) {
        rs_write_string_index(savef, rainbow, cNCOLORS, p_colors[i]);
        rs_write_boolean(savef, p_know[i]);
        rs_write_string(savef, p_guess[i]);
    }

    return (WRITESTAT);
}

int rs_read_potions(FILE *inf) {
    int i;

    if(read_error || format_error) {
        return (READSTAT);
    }

    for(i = 0; i < MAXPOTIONS; i++) {
        rs_read_string_index(inf, rainbow, cNCOLORS, &p_colors[i]);
        rs_read_boolean(inf, &p_know[i]);
        rs_read_new_string(inf, &p_guess[i]);
    }

    return (READSTAT);
}

int rs_write_rings(FILE *savef) {
    int i;

    if(write_error) {
        return (WRITESTAT);
    }

    for(i = 0; i < MAXRINGS; i++) {
        rs_write_string_index(savef, stones, cNSTONES, r_stones[i]);
        rs_write_boolean(savef, r_know[i]);
        rs_write_string(savef, r_guess[i]);
    }

    return (WRITESTAT);
}

int rs_read_rings(FILE *inf) {
    int i;

    if(read_error || format_error) {
        return (READSTAT);
    }

    for(i = 0; i < MAXRINGS; i++) {
        rs_read_string_index(inf, stones, cNSTONES, &r_stones[i]);
        rs_read_boolean(inf, &r_know[i]);
        rs_read_new_string(inf, &r_guess[i]);
    }

    return (READSTAT);
}

int rs_write_misc(FILE *savef) {
    int i;

    if(read_error || format_error) {
        return (READSTAT);
    }

    for(i = 0; i < MAXMM; i++) {
        rs_write_boolean(savef, m_know[i]);
        rs_write_string(savef, m_guess[i]);
    }

    return (READSTAT);
}

int rs_read_misc(FILE *inf) {
    int i;

    if(write_error) {
        return (WRITESTAT);
    }

    for(i = 0; i < MAXMM; i++) {
        rs_read_boolean(inf, &m_know[i]);
        rs_read_new_string(inf, &m_guess[i]);
    }

    return (WRITESTAT);
}

int rs_write_sticks(FILE *savef) {
    int i;

    if(write_error) {
        return (WRITESTAT);
    }

    rs_write_marker(savef, RSID_STICKS);

    for(i = 0; i < MAXSTICKS; i++) {
        if(strcmp(ws_type[i], "staff") == 0) {
            rs_write_int(savef, 0);
            rs_write_string_index(savef, wood, cNWOOD, ws_made[i]);
        } else {
            rs_write_int(savef, 1);
            rs_write_string_index(savef, metal, cNMETAL, ws_made[i]);
        }

        rs_write_boolean(savef, ws_know[i]);
        rs_write_string(savef, ws_guess[i]);
    }

    return (WRITESTAT);
}

int rs_read_sticks(FILE *inf) {
    int i = 0, list = 0;

    if(read_error || format_error) {
        return (READSTAT);
    }

    rs_read_marker(inf, RSID_STICKS);

    for(i = 0; i < MAXSTICKS; i++) {
        rs_read_int(inf, &list);
        ws_made[i] = nullptr;

        if(list == 0) {
            rs_read_string_index(inf, wood, cNWOOD, &ws_made[i]);
            ws_type[i] = "staff";
        } else {
            rs_read_string_index(inf, metal, cNMETAL, &ws_made[i]);
            ws_type[i] = "wand";
        }
        rs_read_boolean(inf, &ws_know[i]);
        rs_read_new_string(inf, &ws_guess[i]);
    }

    return (READSTAT);
}

int rs_write_daemons(FILE *savef, delayed_action *d_list, int count) {
    int i = 0;

    if(write_error) {
        return (WRITESTAT);
    }

    rs_write_marker(savef, RSID_DAEMONS);
    rs_write_int(savef, count);

    for(i = 0; i < count; i++) {
        rs_write_long(savef, d_list[i].d_type);
        rs_write_int(savef, (int)d_list[i].d_id);

        if(d_list[i].d_id == d_doctor) {
            rs_write_int(savef, 1);
        } else if(d_list[i].d_id == d_eat_gold) {
            int index;
            index = find_list_ptr(player.t_pack, d_list[i].d_.varg);
            rs_write_int(savef, index);
        } else if(d_list[i].d_id == d_changeclass) {
            rs_write_int(savef, (int)d_list[i].d_.arg);
        } else if(d_list[i].d_id == d_cloak_charge) {
            int index;
            index = find_list_ptr(player.t_pack, d_list[i].d_.varg);
            rs_write_int(savef, index);
        } else {
            rs_write_int(savef, (int)d_list[i].d_.arg);
        }

        rs_write_long(savef, d_list[i].d_time);
    }

    return (WRITESTAT);
}

int rs_read_daemons(FILE *inf, delayed_action *d_list, int count) {
    int i = 0;
    int func = 0;
    int value = 0;
    int dummy = 0;

    if(read_error || format_error) {
        return (READSTAT);
    }

    rs_read_marker(inf, RSID_DAEMONS);
    rs_read_int(inf, &value);

    if(value > count) {
        format_error = TRUE;
    }

    for(i = 0; i < count; i++) {
        func = 0;
        rs_read_long(inf, &d_list[i].d_type);
        rs_read_int(inf, &func);
        d_list[i].d_id = (enum daemon_id)func;

        if(d_list[i].d_id == d_doctor) {
            rs_read_int(inf, &dummy);
            d_list[i].d_.varg = &player;
        } else if(d_list[i].d_id == d_eat_gold) {
            rs_read_int(inf, &dummy);
            d_list[i].d_.varg = get_list_item(player.t_pack, dummy);
            if(d_list[i].d_.varg == nullptr) {
                d_list[i].d_type = 0;
            }
        } else if(d_list[i].d_id == d_changeclass) {
            rs_read_int(inf, (int *)&d_list[i].d_.arg);
        } else if(d_list[i].d_id == d_cloak_charge) {
            rs_read_int(inf, &dummy);
            d_list[i].d_.varg = get_list_item(player.t_pack, dummy);

            if(d_list[i].d_.varg == nullptr) {
                d_list[i].d_type = 0;
            }
        } else {
            rs_read_int(inf, (int *)&d_list[i].d_.arg);
        }

        rs_read_long(inf, &d_list[i].d_time);

        if(d_list[i].d_id == d_none) {
            d_list[i].d_time = 0;
            d_list[i].d_.varg = nullptr;
            d_list[i].d_type = 0;
        }
    }

    return (READSTAT);
}

int rs_write_room(FILE *savef, room *r) {
    linked_list *l;
    int i;

    if(write_error) {
        return (WRITESTAT);
    }

    rs_write_coord(savef, r->r_pos);
    rs_write_coord(savef, r->r_max);
    rs_write_long(savef, r->r_flags);

    l = r->r_fires;
    i = list_size(l);

    rs_write_int(savef, i);

    if(i > 0) {
        while(l != nullptr) {
            i = find_list_ptr(mlist, l->l_data);
            rs_write_int(savef, i);
            l = l->l_next;
        }
    }

    rs_write_coord_list(savef, r->r_exit);

    return (WRITESTAT);
}

int rs_read_room(FILE *inf, room *r) {
    int i = 0, index = 0;
    linked_list *fires = nullptr, *item = nullptr;

    if(read_error || format_error) {
        return (READSTAT);
    }

    rs_read_coord(inf, &r->r_pos);
    rs_read_coord(inf, &r->r_max);
    rs_read_long(inf, &r->r_flags);

    rs_read_int(inf, &i);
    fires = nullptr;

    while(i > 0) {
        rs_read_int(inf, &index);

        if(index >= 0) {
            void *data;
            data = get_list_item(mlist, index);
            item = creat_item();
            item->l_data = (char *)data;
            if(fires == nullptr) {
                fires = item;
            } else {
                attach(fires, item);
            }
        }
        i--;
    }

    r->r_fires = fires;

    rs_read_coord_list(inf, &r->r_exit);

    return (READSTAT);
}

int rs_write_rooms(FILE *savef, room r[], int count) {
    int n = 0;

    if(write_error) {
        return (WRITESTAT);
    }

    rs_write_int(savef, count);

    for(n = 0; n < count; n++) {
        rs_write_room(savef, &r[n]);
    }

    return (WRITESTAT);
}

int rs_read_rooms(FILE *inf, room *r, int count) {
    int value = 0, n = 0;

    if(read_error || format_error) {
        return (READSTAT);
    }

    rs_read_int(inf, &value);

    if(value > count) {
        format_error = TRUE;
    }

    for(n = 0; n < value; n++) {
        rs_read_room(inf, &r[n]);
    }

    return (READSTAT);
}

int rs_write_room_reference(FILE *savef, room *rp) {
    int i, room = -1;

    if(write_error) {
        return (WRITESTAT);
    }

    for(i = 0; i < MAXROOMS; i++) {
        if(&rooms[i] == rp) {
            room = i;
        }
    }

    rs_write_int(savef, room);

    return (WRITESTAT);
}

int rs_read_room_reference(FILE *inf, room **rp) {
    int i;

    if(read_error || format_error) {
        return (READSTAT);
    }

    rs_read_int(inf, &i);

    *rp = &rooms[i];

    return (READSTAT);
}

int rs_write_door_reference(FILE *savef, coord *exit) {
    int i, idx = 0;

    for(i = 0; i < MAXROOMS; i++) {
        idx = find_list_ptr(rooms[i].r_exit, exit);

        if(idx != -1) {
            break;
        }
    }

    if(i >= MAXROOMS) {
        rs_write_int(savef, -1);
        rs_write_int(savef, -1);
        if(exit != nullptr) {
            abort();
        }
    } else {
        rs_write_int(savef, i);
        rs_write_int(savef, idx);
    }

    return (WRITESTAT);
}

int rs_read_door_reference(FILE *inf, coord **exit) {
    int i, idx;

    rs_read_int(inf, &i);
    rs_read_int(inf, &idx);

    if((i == -1) || (idx == -1)) {
        *exit = nullptr;
    } else {
        *exit = (coord *)get_list_item(rooms[i].r_exit, idx);
    }

    return (READSTAT);
}

int rs_write_traps(FILE *savef, trap *trap, int count) {
    int n;

    rs_write_int(savef, (int)RSID_TRAP);
    rs_write_int(savef, count);

    for(n = 0; n < count; n++) {
        rs_write_int(savef, trap[n].tr_type);
        rs_write_int(savef, trap[n].tr_show);
        rs_write_coord(savef, trap[n].tr_pos);
        rs_write_long(savef, trap[n].tr_flags);
    }

    return (WRITESTAT);
}

int rs_read_traps(FILE *inf, trap *trap, int count) {
    int id = 0, value = 0, n = 0;

    if(rs_read_int(inf, &id) != 0) {
        format_error = TRUE;
    } else if(rs_read_int(inf, &value) != 0) {
        if(value != count) {
            format_error = TRUE;
        }
    } else {
        for(n = 0; n < value; n++) {
            rs_read_int(inf, &trap[n].tr_type);
            rs_read_int(inf, &trap[n].tr_show);
            rs_read_coord(inf, &trap[n].tr_pos);
            rs_read_long(inf, &trap[n].tr_flags);
        }
    }

    return (READSTAT);
}

int rs_write_monsters(FILE *savef, monster *m, int count) {
    int n;

    if(write_error) {
        return (WRITESTAT);
    }

    rs_write_marker(savef, RSID_MONSTERS);
    rs_write_int(savef, count);

    for(n = 0; n < count; n++) {
        rs_write_boolean(savef, m[n].m_normal);
        rs_write_boolean(savef, m[n].m_wander);
        rs_write_long(savef, m[n].m_numsum);
    }

    return (WRITESTAT);
}

int rs_read_monsters(FILE *inf, monster *m, int count) {
    int value = 0, n = 0;

    if(read_error || format_error) {
        return (READSTAT);
    }

    rs_read_marker(inf, RSID_MONSTERS);

    rs_read_int(inf, &value);

    if(value != count) {
        format_error = TRUE;
    }

    for(n = 0; n < count; n++) {
        rs_read_boolean(inf, &m[n].m_normal);
        rs_read_boolean(inf, &m[n].m_wander);
        rs_read_long(inf, &m[n].m_numsum);
    }

    return (READSTAT);
}

int rs_write_object(FILE *savef, object *o) {
    if(write_error) {
        return (WRITESTAT);
    }

    rs_write_marker(savef, RSID_OBJECT);
    rs_write_int(savef, o->o_type);
    rs_write_coord(savef, o->o_pos);
    rs_write_char(savef, o->o_launch);
    rs_write(savef, o->o_damage, sizeof(o->o_damage));
    rs_write(savef, o->o_hurldmg, sizeof(o->o_hurldmg));
    rs_write_object_list(savef, o->contents);
    rs_write_long(savef, o->o_count);
    rs_write_int(savef, o->o_which);
    rs_write_long(savef, o->o_hplus);
    rs_write_long(savef, o->o_dplus);
    rs_write_long(savef, o->o_ac);
    rs_write_long(savef, o->o_flags);
    rs_write_long(savef, o->o_group);
    rs_write_long(savef, o->o_weight);
    rs_write(savef, o->o_mark, MARKLEN);

    return (WRITESTAT);
}

int rs_read_object(FILE *inf, object *o) {
    if(read_error || format_error) {
        return (READSTAT);
    }

    rs_read_marker(inf, RSID_OBJECT);
    rs_read_int(inf, &o->o_type);
    rs_read_coord(inf, &o->o_pos);
    rs_read_char(inf, &o->o_launch);
    rs_read(inf, o->o_damage, sizeof(o->o_damage));
    rs_read(inf, o->o_hurldmg, sizeof(o->o_hurldmg));
    rs_read_object_list(inf, &o->contents);
    rs_read_long(inf, &o->o_count);
    rs_read_int(inf, &o->o_which);
    rs_read_long(inf, &o->o_hplus);
    rs_read_long(inf, &o->o_dplus);
    rs_read_long(inf, &o->o_ac);
    rs_read_long(inf, &o->o_flags);
    rs_read_long(inf, &o->o_group);
    rs_read_long(inf, &o->o_weight);
    rs_read(inf, o->o_mark, MARKLEN);

    return (READSTAT);
}

int rs_write_object_list(FILE *savef, linked_list *l) {
    if(write_error) {
        return (WRITESTAT);
    }

    rs_write_marker(savef, RSID_OBJECTLIST);
    rs_write_int(savef, list_size(l));

    for(; l != nullptr; l = l->l_next) {
        rs_write_object(savef, OBJPTR(l));
    }

    return (WRITESTAT);
}

int rs_read_object_list(FILE *inf, linked_list **list) {
    int i, cnt;
    linked_list *l = nullptr, *previous = nullptr, *head = nullptr;

    if(read_error || format_error) {
        return (READSTAT);
    }

    rs_read_marker(inf, RSID_OBJECTLIST);
    rs_read_int(inf, &cnt);

    for(i = 0; i < cnt; i++) {
        l = new_item(sizeof(object));

        l->l_prev = previous;

        if(previous != nullptr) {
            previous->l_next = l;
        }

        rs_read_object(inf, OBJPTR(l));

        if(previous == nullptr) {
            head = l;
        }

        previous = l;
    }

    if(l != nullptr) {
        l->l_next = nullptr;
    }

    *list = head;

    return (READSTAT);
}

int rs_write_object_reference(FILE *savef, linked_list *list, object *item) {
    int i;

    if(write_error) {
        return (WRITESTAT);
    }

    i = find_list_ptr(list, item);

    rs_write_int(savef, i);

    return (WRITESTAT);
}

int rs_read_object_reference(FILE *inf, linked_list *list, object **item) {
    int i;

    if(read_error || format_error) {
        return (READSTAT);
    }

    rs_read_int(inf, &i);

    *item = (object *)get_list_item(list, i);

    return (READSTAT);
}

int find_thing_coord(linked_list *monlist, coord *c) {
    linked_list *mitem;
    thing *tp;
    int i = 0;

    for(mitem = monlist; mitem != nullptr; mitem = mitem->l_next) {
        tp = THINGPTR(mitem);

        if(c == &tp->t_pos) {
            return (i);
        }

        i++;
    }

    return (-1);
}

int find_object_coord(linked_list *objlist, coord *c) {
    linked_list *oitem;
    object *obj;
    int i = 0;

    for(oitem = objlist; oitem != nullptr; oitem = oitem->l_next) {
        obj = OBJPTR(oitem);

        if(c == &obj->o_pos) {
            return (i);
        }

        i++;
    }

    return (-1);
}

int rs_write_thing(FILE *savef, thing *t) {
    int i = -1;

    if(write_error) {
        return (WRITESTAT);
    }

    rs_write_marker(savef, RSID_THING);

    if(t == nullptr) {
        rs_write_int(savef, 0);
        return (WRITESTAT);
    }

    rs_write_int(savef, 1);
    rs_write_boolean(savef, t->t_wasshot);
    rs_write_int(savef, t->t_type);
    rs_write_int(savef, t->t_disguise);
    rs_write_int(savef, t->t_oldch);
    rs_write_long(savef, t->t_ctype);
    rs_write_long(savef, t->t_index);
    rs_write_long(savef, t->t_no_move);
    rs_write_long(savef, t->t_quiet);
    rs_write_long(savef, t->t_movement);
    rs_write_int(savef, t->t_action);
    rs_write_long(savef, t->t_artifact);
    rs_write_long(savef, t->t_wand);
    rs_write_long(savef, t->t_summon);
    rs_write_long(savef, t->t_cast);
    rs_write_long(savef, t->t_breathe);

    rs_write_string(savef, t->t_name);
    rs_write_door_reference(savef, t->t_doorgoal);

    if(t->t_dest == &hero) {
        rs_write_int(savef, 0);
        rs_write_int(savef, 1);
    } else if(t->t_dest != nullptr) {
        i = find_thing_coord(mlist, t->t_dest);

        if(i >= 0) {
            rs_write_int(savef, 1);
            rs_write_int(savef, i);
        } else {
            i = find_object_coord(lvl_obj, t->t_dest);

            if(i >= 0) {
                rs_write_int(savef, 2);
                rs_write_int(savef, i);
            } else {
                rs_write_int(savef, 0);
                rs_write_int(savef, 1); /* chase the hero anyway */
            }
        }
    } else {
        rs_write_int(savef, 0);
        rs_write_int(savef, 0);
    }

    rs_write_coord(savef, t->t_pos);
    rs_write_coord(savef, t->t_oldpos);
    rs_write_coord(savef, t->t_newpos);
    rs_write_longs(savef, t->t_flags, 16);
    rs_write_object_list(savef, t->t_pack);
    i = -1;
    if(t->t_using != nullptr) {
        i = find_list_ptr(t->t_pack, t->t_using->l_data);
    }
    rs_write_int(savef, i);
    rs_write_int(savef, t->t_selection);
    rs_write_stats(savef, &t->t_stats);
    rs_write_stats(savef, &t->maxstats);

    return (WRITESTAT);
}

int rs_read_thing(FILE *inf, thing *t) {
    int listid = 0, index = -1;

    if(read_error || format_error) {
        return (READSTAT);
    }

    rs_read_marker(inf, RSID_THING);

    rs_read_int(inf, &index);

    if(index == 0) {
        return (READSTAT);
    }

    rs_read_boolean(inf, &t->t_wasshot);
    rs_read_int(inf, &t->t_type);
    rs_read_int(inf, &t->t_disguise);
    rs_read_int(inf, &t->t_oldch);
    rs_read_long(inf, &t->t_ctype);
    rs_read_long(inf, &t->t_index);
    rs_read_long(inf, &t->t_no_move);
    rs_read_long(inf, &t->t_quiet);
    rs_read_long(inf, &t->t_movement);
    rs_read_int(inf, &t->t_action);
    rs_read_long(inf, &t->t_artifact);
    rs_read_long(inf, &t->t_wand);
    rs_read_long(inf, &t->t_summon);
    rs_read_long(inf, &t->t_cast);
    rs_read_long(inf, &t->t_breathe);
    rs_read_new_string(inf, &t->t_name);
    rs_read_door_reference(inf, &t->t_doorgoal);

    rs_read_int(inf, &listid);
    rs_read_int(inf, &index);
    t->t_reserved = -1;

    if(listid == 0) { /* hero or NULL */
        if(index == 1) {
            t->t_dest = &hero;
        } else {
            t->t_dest = nullptr;
        }
    } else if(listid == 1) { /* monster/thing */
        t->t_dest = nullptr;
        t->t_reserved = index;
    } else if(listid == 2) { /* object */
        object *obj = (object *)get_list_item(lvl_obj, index);
        if(obj != nullptr) {
            t->t_dest = &obj->o_pos;
        }
    } else {
        t->t_dest = nullptr;
    }

    rs_read_coord(inf, &t->t_pos);
    rs_read_coord(inf, &t->t_oldpos);
    rs_read_coord(inf, &t->t_newpos);
    rs_read_longs(inf, t->t_flags, 16);
    rs_read_object_list(inf, &t->t_pack);
    rs_read_int(inf, &index);
    t->t_using = (linked_list *)get_list_item(t->t_pack, index);
    rs_read_int(inf, &t->t_selection);
    rs_read_stats(inf, &t->t_stats);
    rs_read_stats(inf, &t->maxstats);

    return (READSTAT);
}

void rs_fix_thing(thing *t) {
    thing *tp;

    if(t->t_reserved < 0) {
        return;
    }

    tp = (thing *)get_list_item(mlist, t->t_reserved);

    if(tp != nullptr) {
        t->t_dest = &tp->t_pos;
    }
}

int rs_write_thing_list(FILE *savef, linked_list *l) {
    int cnt = 0;

    if(write_error) {
        return (WRITESTAT);
    }

    rs_write_marker(savef, RSID_MONSTERLIST);

    cnt = list_size(l);

    rs_write_int(savef, cnt);

    if(cnt < 1) {
        return (WRITESTAT);
    }

    while(l != nullptr) {
        rs_write_thing(savef, (thing *)l->l_data);
        l = l->l_next;
    }

    return (WRITESTAT);
}

int rs_read_thing_list(FILE *inf, linked_list **list) {
    int i, cnt;
    linked_list *l = nullptr, *previous = nullptr, *head = nullptr;

    if(read_error || format_error) {
        return (READSTAT);
    }

    rs_read_marker(inf, RSID_MONSTERLIST);

    rs_read_int(inf, &cnt);

    for(i = 0; i < cnt; i++) {
        l = new_item(sizeof(thing));

        l->l_prev = previous;

        if(previous != nullptr) {
            previous->l_next = l;
        }

        rs_read_thing(inf, THINGPTR(l));

        if(previous == nullptr) {
            head = l;
        }

        previous = l;
    }

    if(l != nullptr) {
        l->l_next = nullptr;
    }

    *list = head;

    return (READSTAT);
}

void rs_fix_thing_list(linked_list *list) {
    linked_list *item;

    for(item = list; item != nullptr; item = item->l_next) {
        rs_fix_thing(THINGPTR(item));
    }
}

int rs_write_thing_reference(FILE *savef, linked_list *list, thing *item) {
    int i;

    if(write_error) {
        return (WRITESTAT);
    }

    if(item == nullptr) {
        rs_write_int(savef, -1);
    } else {
        i = find_list_ptr(list, item);

        rs_write_int(savef, i);
    }

    return (WRITESTAT);
}

int rs_read_thing_reference(FILE *inf, linked_list *list, thing **item) {
    int i;

    if(read_error || format_error) {
        return (READSTAT);
    }

    rs_read_int(inf, &i);

    if(i == -1) {
        *item = nullptr;
    } else {
        *item = (thing *)get_list_item(list, i);
    }

    return (READSTAT);
}

int rs_write_thing_references(FILE *savef, linked_list *list, thing *items[], int count) {
    int i;

    if(write_error) {
        return (WRITESTAT);
    }

    for(i = 0; i < count; i++) {
        rs_write_thing_reference(savef, list, items[i]);
    }

    return (WRITESTAT);
}

int rs_read_thing_references(FILE *inf, linked_list *list, thing *items[], int count) {
    int i;

    if(read_error || format_error) {
        return (READSTAT);
    }

    for(i = 0; i < count; i++) {
        rs_read_thing_reference(inf, list, &items[i]);
    }

    return (WRITESTAT);
}

int rs_save_file(FILE *savef) {
    int i;

    if(write_error) {
        return (WRITESTAT);
    }

    rs_write_object_list(savef, lvl_obj);
    rs_write_thing(savef, &player);
    rs_write_thing_list(savef, mlist);
    rs_write_thing_list(savef, tlist);
    rs_write_thing_list(savef, monst_dead);

    rs_write_traps(savef, traps, MAXTRAPS);
    rs_write_rooms(savef, rooms, MAXROOMS);
    rs_write_room_reference(savef, oldrp);

    rs_write_object_reference(savef, player.t_pack, cur_armor);

    for(i = 0; i < NUM_FINGERS; i++) {
        rs_write_object_reference(savef, player.t_pack, cur_ring[i]);
    }

    for(i = 0; i < NUM_MM; i++) {
        rs_write_object_reference(savef, player.t_pack, cur_misc[i]);
    }

    rs_write_ints(savef, cur_relic, MAXRELIC);

    rs_write_object_reference(savef, player.t_pack, cur_weapon);

    rs_write_long(savef, char_type);
    rs_write_int(savef, foodlev);
    rs_write_int(savef, ntraps);
    rs_write_int(savef, trader);
    rs_write_long(savef, curprice);
    rs_write_int(savef, seed);
    rs_write_int(savef, dnum);
    rs_write_int(savef, max_level);
    rs_write_int(savef, cur_max);
    rs_write_int(savef, mpos);
    rs_write_int(savef, level);
    rs_write_int(savef, purse);
    rs_write_int(savef, inpack);
    rs_write_int(savef, total);
    rs_write_int(savef, no_food);
    rs_write_int(savef, foods_this_level);
    rs_write_int(savef, count);
    rs_write_int(savef, food_left);
    rs_write_int(savef, group);
    rs_write_int(savef, hungry_state);
    rs_write_int(savef, infest_dam);
    rs_write_int(savef, lost_str);
    rs_write_int(savef, lastscore);
    rs_write_int(savef, hold_count);
    rs_write_int(savef, trap_tries);
    rs_write_int(savef, chant_time);
    rs_write_int(savef, pray_time);
    rs_write_int(savef, spell_power);
    rs_write_int(savef, turns);
    rs_write_int(savef, quest_item);
    rs_write_int(savef, COLS);
    rs_write_int(savef, LINES);
    rs_write_int(savef, nfloors);
    rs_write(savef, curpurch, LINELEN);
    rs_write_char(savef, PLAYER);
    rs_write_int(savef, take);
    rs_write(savef, prbuf, LINELEN * 2);
    rs_write(savef, outbuf, BUFSIZ);
    rs_write_int(savef, runch);
    rs_write_scrolls(savef);
    rs_write_potions(savef);
    rs_write_rings(savef);
    rs_write_sticks(savef);
    rs_write_misc(savef);
    rs_write(savef, whoami, LINELEN);
    rs_write(savef, huh, LINELEN);
    rs_write(savef, file_name, LINELEN);
    rs_write(savef, score_file, LINELEN);
    rs_write(savef, home, LINELEN);
    rs_write_window(savef, cw);
    rs_write_window(savef, hw);
    rs_write_window(savef, mw);
    rs_write_window(savef, msgw);
    rs_write_window(savef, stdscr);
    rs_write_boolean(savef, pool_teleport);
    rs_write_boolean(savef, inwhgt);
    rs_write_boolean(savef, after);
    rs_write_boolean(savef, waswizard);
    rs_write_boolean(savef, playing);
    rs_write_boolean(savef, running);
    rs_write_boolean(savef, wizard);
    rs_write_boolean(savef, notify);
    rs_write_boolean(savef, fight_flush);
    rs_write_boolean(savef, terse);
    rs_write_boolean(savef, auto_pickup);
    rs_write_boolean(savef, menu_overlay);
    rs_write_boolean(savef, door_stop);
    rs_write_boolean(savef, jump);
    rs_write_boolean(savef, slow_invent);
    rs_write_boolean(savef, firstmove);
    rs_write_boolean(savef, askme);
    rs_write_boolean(savef, daytime);
    rs_write_levtype(savef, levtype);
    rs_write_magic_items(savef, things, MAXTHINGS);
    rs_write_magic_items(savef, s_magic, MAXSCROLLS);
    rs_write_magic_items(savef, p_magic, MAXPOTIONS);
    rs_write_magic_items(savef, r_magic, MAXRINGS);
    rs_write_magic_items(savef, ws_magic, MAXSTICKS);
    rs_write_magic_items(savef, m_magic, MAXMM);
    rs_write_magic_items(savef, rel_magic, MAXRELIC);
    rs_write_magic_items(savef, foods, MAXFOODS);

    rs_write_monsters(savef, monsters, NUMMONST + 1);

    rs_write_daemons(savef, d_list, MAXDAEMONS);
    rs_write_daemons(savef, f_list, MAXFUSES);
    rs_write_int(savef, demoncnt);
    rs_write_int(savef, fusecnt);

    rs_write_int(savef, between);

    rs_write_int(savef, chance);

    return (WRITESTAT);
}

int rs_restore_file(FILE *inf) {
    int i;

    if(read_error || format_error) {
        return (READSTAT);
    }

    rs_read_object_list(inf, &lvl_obj);
    rs_read_thing(inf, &player);
    rs_read_thing_list(inf, &mlist);
    rs_read_thing_list(inf, &tlist);
    rs_read_thing_list(inf, &monst_dead);

    rs_fix_thing(&player);
    rs_fix_thing_list(mlist);
    rs_fix_thing_list(tlist);
    rs_fix_thing_list(monst_dead);

    rs_read_traps(inf, traps, MAXTRAPS);
    rs_read_rooms(inf, rooms, MAXROOMS);
    rs_read_room_reference(inf, &oldrp);

    rs_read_object_reference(inf, player.t_pack, &cur_armor);

    for(i = 0; i < NUM_FINGERS; i++) {
        rs_read_object_reference(inf, player.t_pack, &cur_ring[i]);
    }

    for(i = 0; i < NUM_MM; i++) {
        rs_read_object_reference(inf, player.t_pack, &cur_misc[i]);
    }

    rs_read_ints(inf, cur_relic, MAXRELIC);

    rs_read_object_reference(inf, player.t_pack, &cur_weapon);

    rs_read_long(inf, &char_type);
    rs_read_int(inf, &foodlev);
    rs_read_int(inf, &ntraps);
    rs_read_int(inf, &trader);
    rs_read_long(inf, &curprice);
    rs_read_int(inf, &seed);
    rs_read_int(inf, &dnum);
    rs_read_int(inf, &max_level);
    rs_read_int(inf, &cur_max);
    rs_read_int(inf, &mpos);
    rs_read_int(inf, &level);
    rs_read_int(inf, &purse);
    rs_read_int(inf, &inpack);
    rs_read_int(inf, &total);
    rs_read_int(inf, &no_food);
    rs_read_int(inf, &foods_this_level);
    rs_read_int(inf, &count);
    rs_read_int(inf, &food_left);
    rs_read_int(inf, &group);
    rs_read_int(inf, &hungry_state);
    rs_read_int(inf, &infest_dam);
    rs_read_int(inf, &lost_str);
    rs_read_int(inf, &lastscore);
    rs_read_int(inf, &hold_count);
    rs_read_int(inf, &trap_tries);
    rs_read_int(inf, &chant_time);
    rs_read_int(inf, &pray_time);
    rs_read_int(inf, &spell_power);
    rs_read_int(inf, &turns);
    rs_read_int(inf, &quest_item);
    rs_read_int(inf, &COLS);
    rs_read_int(inf, &LINES);
    rs_read_int(inf, &nfloors);
    rs_read(inf, curpurch, LINELEN);
    rs_read_char(inf, &PLAYER);
    rs_read_int(inf, &take);
    rs_read(inf, prbuf, LINELEN * 2);
    rs_read(inf, outbuf, BUFSIZ);
    rs_read_int(inf, &runch);
    rs_read_scrolls(inf);
    rs_read_potions(inf);
    rs_read_rings(inf);
    rs_read_sticks(inf);
    rs_read_misc(inf);
    rs_read(inf, whoami, LINELEN);
    rs_read(inf, huh, LINELEN);
    rs_read(inf, file_name, LINELEN);
    rs_read(inf, score_file, LINELEN);
    rs_read(inf, home, LINELEN);
    rs_read_window(inf, cw);
    rs_read_window(inf, hw);
    rs_read_window(inf, mw);
    rs_read_window(inf, msgw);
    rs_read_window(inf, stdscr);
    rs_read_boolean(inf, &pool_teleport);
    rs_read_boolean(inf, &inwhgt);
    rs_read_boolean(inf, &after);
    rs_read_boolean(inf, &waswizard);
    rs_read_boolean(inf, &playing);
    rs_read_boolean(inf, &running);
    rs_read_boolean(inf, &wizard);
    rs_read_boolean(inf, &notify);
    rs_read_boolean(inf, &fight_flush);
    rs_read_boolean(inf, &terse);
    rs_read_boolean(inf, &auto_pickup);
    rs_read_boolean(inf, &menu_overlay);
    rs_read_boolean(inf, &door_stop);
    rs_read_boolean(inf, &jump);
    rs_read_boolean(inf, &slow_invent);
    rs_read_boolean(inf, &firstmove);
    rs_read_boolean(inf, &askme);
    rs_read_boolean(inf, &daytime);
    rs_read_levtype(inf, &levtype);
    rs_read_magic_items(inf, things, MAXTHINGS);
    rs_read_magic_items(inf, s_magic, MAXSCROLLS);
    rs_read_magic_items(inf, p_magic, MAXPOTIONS);
    rs_read_magic_items(inf, r_magic, MAXRINGS);
    rs_read_magic_items(inf, ws_magic, MAXSTICKS);
    rs_read_magic_items(inf, m_magic, MAXMM);
    rs_read_magic_items(inf, rel_magic, MAXRELIC);
    rs_read_magic_items(inf, foods, MAXFOODS);
    rs_read_monsters(inf, monsters, NUMMONST + 1);

    rs_read_daemons(inf, d_list, MAXDAEMONS);
    rs_read_daemons(inf, f_list, MAXFUSES);
    rs_read_int(inf, &demoncnt);
    rs_read_int(inf, &fusecnt);

    rs_read_int(inf, &between);

    rs_read_int(inf, &chance);

    return (READSTAT);
}
