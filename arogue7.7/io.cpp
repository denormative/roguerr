/*
 * Advanced Rogue
 * Copyright (C) 1984, 1985, 1986 Michael Morgan, Ken Dalka and AT&T
 * All rights reserved.
 *
 * Based on "Rogue: Exploring the Dungeons of Doom"
 * Copyright (C) 1980, 1981 Michael Toy, Ken Arnold and Glenn Wichman
 * All rights reserved.
 *
 * See the file LICENSE.TXT for full copyright and licensing information.
 */

/*
 * Various input/output functions
 */

#include "rogue.h"
#include <cstdarg>

/*
 * msg:
 *    Display a message at the top of the screen.
 */

static char msgbuf[BUFSIZ];
static size_t newpos = 0;

/*VARARGS1*/
void msg(const char *fmt, ...) {
    va_list ap;
    /*
     * if the string is "", just clear the line
     */
    if(*fmt == '\0') {
        overwrite(cw, msgw);
        wmove(msgw, 0, 0);
        clearok(msgw, FALSE);
        draw(msgw);
        mpos = 0;
        return;
    }
    /*
     * otherwise add to the message and flush it out
     */
    va_start(ap, fmt);
    doadd(fmt, ap);
    va_end(ap);
    endmsg();
}

/*
 * add things to the current message
 */
void addmsg(const char *fmt, ...) {
    va_list ap;
    va_start(ap, fmt);
    doadd(fmt, ap);
    va_end(ap);
}

/*
 * Display a new msg (giving him a chance to see the previous one if it
 * is up there with the --More--)
 */
void endmsg() {
    /* Needed to track where we are for 5.0 (PC) curses */
    int x, y;

    strcpy(huh, msgbuf);
    if(mpos) {
        /*
         * If this message will fit on the line (plus space for --More--
         * then just add it (only during combat).
         */
        if(player.t_quiet < 0 && mpos + (int)newpos + (int)strlen(morestr) + 2 < COLS) {
            wmove(msgw, 0, mpos + 2);
            newpos += (size_t)mpos + 2;
        } else {
            wmove(msgw, 0, mpos);
            waddstr(msgw, morestr);
            draw(cw);
            draw(msgw);
            wait_for(' ');
            overwrite(cw, msgw);
            wmove(msgw, 0, 0);
            touchwin(cw);
        }
    } else {
        overwrite(cw, msgw);
        wmove(msgw, 0, 0);
    }
    waddstr(msgw, msgbuf);
    getyx(msgw, y, x);
    mpos = (int)newpos;
    newpos = 0;
    wmove(msgw, y, x);
    draw(cw);
    clearok(msgw, FALSE);
    draw(msgw);
}

void doadd(const char *fmt, va_list ap) {
    /* Do the printf into buf */
#pragma clang diagnostic ignored "-Wformat-nonliteral"
    vsprintf(&msgbuf[newpos], fmt, ap);
    newpos = strlen(msgbuf);
}

/*
 * step_ok:
 *    returns true if it is ok for type to step on ch
 *    flgptr will be NULL if we don't know what the monster is yet!
 */

bool step_ok(int y, int x, int can_on_monst, thing *flgptr) {
    /* can_on_monst = MONSTOK if all we care about are physical obstacles */
    linked_list *item;
    int ch;

    /* What is here?  Don't check monster window if MONSTOK is set */
    if(can_on_monst == MONSTOK) {
        ch = ssinch(y, x);
    } else {
        ch = winat(y, x);
    }

    if(can_on_monst == FIGHTOK && isalpha(ch) && (item = find_mons(y, x)) != nullptr) {
        thing *tp = THINGPTR(item); /* What monster is here? */

        /* We can hit it if we're after it */
        if(flgptr->t_dest == &tp->t_pos) {
            return TRUE;
        }

        /*
         * Otherwise, if we're friendly we'll hit it unless it is also
         * friendly or is our race.
         */
        if(off(*flgptr, ISFRIENDLY) || on(*tp, ISFRIENDLY) || flgptr->t_index == tp->t_index) {
            return FALSE;
        }
        return TRUE;
    }
    switch(ch) {
        case ' ':
        case '|':
        case '-':
        case SECRETDOOR:
            if(flgptr && on(*flgptr, CANINWALL)) {
                return (TRUE);
            }
            return FALSE;
        case SCROLL:
            if(can_on_monst == MONSTOK) {
                return (TRUE); /* Not a real obstacle */
            }
            /*
             * If it is a scroll, it might be a scare monster scroll
             * so we need to look it up to see what type it is.
             */
            if(flgptr && flgptr->t_ctype == C_MONSTER) {
                item = find_obj(y, x);
                if(item != nullptr && (OBJPTR(item))->o_which == S_SCARE &&
                   (flgptr == nullptr || flgptr->t_stats.s_intel < 16)) {
                    return (FALSE); /* All but smart ones are scared */
                }
            }
            return (TRUE);
        default:
            return (!isalpha(ch));
    }

    return (FALSE);
}
/*
 * shoot_ok:
 *    returns true if it is ok for type to shoot over ch
 */

bool shoot_ok(int ch) {
    switch(ch) {
        case ' ':
        case '|':
        case '-':
        case SECRETDOOR:
        case FOREST:
            return FALSE;
        default:
            return (!isalpha(ch));
    }
}

/*
 * readchar:
 *    flushes stdout so that screen is up to date and then returns
 *    getchar.
 */
int readchar() {
    int ch;

    ch = md_readchar(cw);

    if((ch == 3) || (ch == 0)) {
        quit(0);
        return (27);
    }

    return (ch);
}

/*
 * status:
 *    Display the important stats line.  Keep the cursor where it was.
 */

void status(bool display) {
    stats *stat_ptr, *max_ptr;
    int oy = 0, ox = 0;
    long temp;
    char *pb;
    static char buf[LINELEN];
    static int hpwidth = 0, s_hungry = -1;
    static long s_lvl = -1, s_hp = -1, s_str, maxs_str, s_ac = 0;
    static long s_intel, s_dext, s_wisdom, s_const, s_charisma;
    static long maxs_intel, maxs_dext, maxs_wisdom, maxs_const, maxs_charisma;
    static long s_exp = 0;
    static long s_carry, s_pack;
    bool first_line = FALSE;

    stat_ptr = &pstats;
    max_ptr = &max_stats;

    /*
     * If nothing has changed in the first line, then skip it
     */
    if(!display && s_lvl == level && s_intel == stat_ptr->s_intel &&
       s_wisdom == stat_ptr->s_wisdom && s_dext == dex_compute() && s_const == stat_ptr->s_const &&
       s_charisma == stat_ptr->s_charisma && s_str == str_compute() && s_hungry == hungry_state &&
       maxs_intel == max_ptr->s_intel && maxs_wisdom == max_ptr->s_wisdom &&
       maxs_dext == max_ptr->s_dext && maxs_const == max_ptr->s_const &&
       maxs_charisma == max_ptr->s_charisma && maxs_str == max_ptr->s_str) {
        goto line_two;
    }

    /* Display the first line */
    first_line = TRUE;
    getyx(cw, oy, ox);
    sprintf(buf, "Int:%ld(%ld)  Str:%ld", stat_ptr->s_intel, max_ptr->s_intel, str_compute());

    /* Maximum strength */
    pb = &buf[strlen(buf)];
    sprintf(pb, "(%ld)", max_ptr->s_str);

    pb = &buf[strlen(buf)];
    sprintf(pb, "  Wis:%ld(%ld)  Dxt:%ld(%ld)  Con:%ld(%ld)  Cha:%ld(%ld)", stat_ptr->s_wisdom,
            max_ptr->s_wisdom, dex_compute(), max_ptr->s_dext, stat_ptr->s_const, max_ptr->s_const,
            stat_ptr->s_charisma, max_ptr->s_charisma);

    /* Update first line status */
    s_intel = stat_ptr->s_intel;
    s_wisdom = stat_ptr->s_wisdom;
    s_dext = dex_compute();
    s_const = stat_ptr->s_const;
    s_charisma = stat_ptr->s_charisma;
    s_str = str_compute();
    maxs_intel = max_ptr->s_intel;
    maxs_wisdom = max_ptr->s_wisdom;
    maxs_dext = max_ptr->s_dext;
    maxs_const = max_ptr->s_const;
    maxs_charisma = max_ptr->s_charisma;
    maxs_str = max_ptr->s_str;

    /* Print the line */
    cwaddstr(LINES - 2, 0, buf);
    switch(hungry_state) {
        case F_OKAY:;
            break;
        case F_HUNGRY:
            waddstr(cw, "  Hungry");
            break;
        case F_WEAK:
            waddstr(cw, "  Weak");
            break;
        case F_FAINT:
            waddstr(cw, "  Fainting");
            break;
        case F_SATIATED:
            waddstr(cw, "  Satiated");
            break;
    }
    wclrtoeol(cw);
    s_hungry = hungry_state;

    /*
     * If nothing has changed since the last status, don't
     * bother.
     */
line_two:
    if(!display && s_lvl == level && s_hp == stat_ptr->s_hpt &&
       s_ac == ac_compute(FALSE) - dext_prot(s_dext) && s_pack == stat_ptr->s_pack &&
       s_carry == stat_ptr->s_carry && s_exp == stat_ptr->s_exp) {
        return;
    }

    if(!first_line) {
        getyx(cw, oy, ox);
    }
    if(s_hp != max_ptr->s_hpt) {
        temp = s_hp = max_ptr->s_hpt;
        for(hpwidth = 0; temp; hpwidth++) {
            temp /= 10;
        }
    }
    sprintf(buf, "Lvl:%d  Hp:%*ld(%*ld)  Ac:%ld  Carry:%ld(%ld)  Exp:%ld/%lu  %s", level, hpwidth,
            stat_ptr->s_hpt, hpwidth, max_ptr->s_hpt, ac_compute(FALSE) - dext_prot(s_dext),
            stat_ptr->s_pack / 10, stat_ptr->s_carry / 10, stat_ptr->s_lvl, stat_ptr->s_exp,
            cnames[player.t_ctype][min(stat_ptr->s_lvl - 1, NUM_CNAMES - 1L)]);

    /*
     * Save old status
     */
    s_lvl = level;
    s_hp = stat_ptr->s_hpt;
    s_ac = ac_compute(FALSE) - dext_prot(s_dext);
    s_pack = stat_ptr->s_pack;
    s_carry = stat_ptr->s_carry;
    s_exp = stat_ptr->s_exp;
    cwaddstr(LINES - 1, 0, buf);
    wclrtoeol(cw);
    wmove(cw, oy, ox);
}

/*
 * wait_for
 *    Sit around until the guy types the right key
 */

void wait_for(WINDOW *win, int ch) {
    int c;

    if(ch == '\n') {
        while((c = wgetch(win)) != '\n' && c != '\r') {
        }
    } else {
        while(wgetch(win) != ch) {
        }
    }
}

void wait_for(int ch) {
    wait_for(msgw, ch);
}

/*
 * over_win:
 *    Given a current window, a new window, and the max y and x of the
 *    new window, paint the new window on top of the old window without
 *    destroying any of the old window.  Current window and new window
 *    are assumed to have lines lines and cols columns (max y and max x
 *    pertain only the the useful information to be displayed.
 *    If redraw is non-zero, we wait for the character "redraw" to be
 *    typed and then redraw the starting screen.
 */

void over_win(WINDOW *oldwin, WINDOW *newin, long maxy, int maxx, int cursory, int cursorx,
              char redraw) {
    static char blanks[LINELEN + 1];
    int line, i;
    WINDOW *ow; /* Overlay window */

    /* Create a blanking line */
    for(i = 0; i < maxx && i < COLS && i < LINELEN; i++) {
        blanks[i] = ' ';
    }
    blanks[i] = '\0';

    /* Create the window we will display */
    ow = newwin(LINES, COLS, 0, 0);

    /* Blank out the area we want to use */
    if(oldwin == cw) {
        msg("");
        line = 1;
    } else {
        line = 0;
    }

    overwrite(oldwin, ow); /* Get a copy of the old window */

    /* Do the remaining blanking */
    for(; line < maxy; line++) {
        mvwaddstr(ow, line, 0, blanks);
    }

    overlay(newin, ow); /* Overlay our new window */

    /* Move the cursor to the specified location */
    wmove(ow, cursory, cursorx);

    clearok(ow, FALSE); /* Draw inventory without clearing */
    draw(ow);

    if(redraw) {
        wait_for(redraw);

        clearok(oldwin, FALSE); /* Setup to redraw current screen */
        touchwin(oldwin);       /* clearing first */
        draw(oldwin);
    }

    delwin(ow);
}

/*
 * show_win:
 *    function used to display a window and wait before returning
 */
void show_win(WINDOW *scr, const char *message) {
    mvwaddstr(scr, 0, 0, message);
    touchwin(scr);
    wmove(scr, hero.y, hero.x);
    draw(scr);
    wait_for(' ');
    clearok(cw, TRUE);
    touchwin(cw);
}

/*
 * dbotline:
 *    Displays message on bottom line and waits for a space to return
 */
void dbotline(WINDOW *scr, const char *message) {
    mvwaddstr(scr, LINES - 1, 0, message);
    draw(scr);
    wait_for(' ');
}

/*
 * restscr:
 *    Restores the screen to the terminal
 */
void restscr(WINDOW *scr) {
    clearok(scr, TRUE);
    touchwin(scr);
}

/*
 * netread:
 *    Read a byte, short, or long machine independently
 *    Always returns the value as an unsigned long.
 */

unsigned long netread(int *error, int size, FILE *stream) {
    unsigned long result = 0L, /* What we read in */
        partial;               /* Partial value */
    int nextc,                 /* The next byte */
        i;                     /* To index through the result a byte at a time */

    /* Be sure we have a right sized chunk */
    if(size < 1 || size > 4) {
        *error = 1;
        return (0L);
    }

    for(i = 0; i < size; i++) {
        nextc = getc(stream);
        if(nextc == EOF) {
            *error = 1;
            return (0L);
        }
        partial = (unsigned long)(nextc & 0xff);
        partial <<= 8 * i;
        result |= partial;
    }

    *error = 0;
    return (result);
}

/*
 * netwrite:
 *    Write out a byte, short, or long machine independently.
 */

int netwrite(unsigned long value, int size, FILE *stream) {
    int i;     /* Goes through value one byte at a time */
    char outc; /* The next character to be written */

    /* Be sure we have a right sized chunk */
    if(size < 1 || size > 4) {
        return (0);
    }

    for(i = 0; i < size; i++) {
        outc = (char)((value >> (8 * i)) & 0xff);
        putc(outc, stream);
    }
    return (size);
}
