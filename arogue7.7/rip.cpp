/*
 * Advanced Rogue
 * Copyright (C) 1984, 1985, 1986 Michael Morgan, Ken Dalka and AT&T
 * All rights reserved.
 *
 * Based on "Rogue: Exploring the Dungeons of Doom"
 * Copyright (C) 1980, 1981 Michael Toy, Ken Arnold and Glenn Wichman
 * All rights reserved.
 *
 * See the file LICENSE.TXT for full copyright and licensing information.
 */

/* Print flags for scoring */
static const int REALLIFE = 1;  /* Print out machine and logname */
static const int EDITSCORE = 2; /* Edit the current score file */
static const int ADDSCORE = 3;  /* Add a new score */

/*
 * File for the fun ends
 * Death or a total win
 */

#include "rogue.h"
#include <cerrno>
#include <csignal>
#include <ctime>
#include <fcntl.h>
#include <sys/types.h>
#include <unistd.h>

#ifdef PC7300
#include <sys/window.h>
extern struct uwdata wdata, oldwin;
extern char oldtext[WTXTNUM][WTXTLEN];
#endif

extern int update(sc_ent top_ten[], long amount, long quest, char *whoami, long flags,
                  long level, long monst, long ctype, char *system, char *login);

static const char *rip[] = {"                       __________",
                            "                      /          \\",
                            "                     /    REST    \\",
                            "                    /      IN      \\",
                            "                   /     PEACE      \\",
                            "                  /                  \\",
                            "                  |                  |",
                            "                  |                  |",
                            "                  |    killed by     |",
                            "                  |                  |",
                            "                  |       1984       |",
                            "                 *|     *  *  *      | *",
                            R"(         ________)/\\_//(\/(/\)/\//\/|_)_______)",
                            nullptr};

[[noreturn]] void byebye(int sig) {
    if(!isendwin()) {
        clear();
        endwin();
    }
#ifdef PC7300
    endhardwin();
#endif
    printf("\n");
    exit(sig);
}

/*
 * death:
 *    Do something really fun when he dies
 */

[[noreturn]] void death(long monst) {
    const char **dp = rip;
    const char *killer;
    struct tm *lt;
    time_t date;
    char buf[LINELEN];

    time(&date);
    lt = localtime(&date);
    clear();
    move(8, 0);
    while(*dp) {
        printw("%s\n", *dp++);
    }
    mvaddstr(14, 28 - (int)((strlen(whoami) + 1) / 2), whoami);
    sprintf(buf, "%ld Points", pstats.s_exp);
    mvaddstr(15, 28 - (int)((strlen(buf) + 1) / 2), buf);
    killer = killname(monst);
    mvaddstr(17, 28 - (int)((strlen(killer) + 1) / 2), killer);
    sprintf(prbuf, "%4d", 1900 + lt->tm_year);
    mvaddstr(18, 26, prbuf);
    move(LINES - 1, 0);
    refresh();
    score(pstats.s_exp, KILLED, monst);
    endwin();
#ifdef PC7300
    endhardwin();
#endif
    exit(0);
}

#ifdef PC7300
/*
 * Restore window characteristics on a hard window terminal (PC7300).
 */
endhardwin() {
    int i;
    struct utdata labelbuf;

    /* Restore the old window size */
    if(oldwin.uw_width) {
        ioctl(1, WIOCSETD, &oldwin);
    }

    /* Restore the old window text */
    for(i = 0; i < WTXTNUM; i++) {
        labelbuf.ut_num = i;
        strcpy(labelbuf.ut_text, oldtext[i]);
        ioctl(1, WIOCSETTEXT, &labelbuf);
    }
}
#endif

const char *killname(long monst) {
    static char mons_name[LINELEN];
    int i;

    if(monst > NUMMONST) {
        return ("a strange monster");
    }

    if(monst >= 0) {
        switch(monsters[monst].m_name[0]) {
            case 'a':
            case 'e':
            case 'i':
            case 'o':
            case 'u':
                sprintf(mons_name, "an %s", monsters[monst].m_name);
                break;
            default:
                sprintf(mons_name, "a %s", monsters[monst].m_name);
        }
        return (mons_name);
    }
    for(i = 0; i < DEATHNUM; i++) {
        if(deaths[i].reason == monst) {
            break;
        }
    }
    if(i >= DEATHNUM) {
        return ("strange death");
    }
    return (deaths[i].name);
}

/* score -- figure score and post it. */
void score(long amount, int flags, long monst) {
    static sc_ent top_ten[NUMSCORE];
    sc_ent *scp;
    sc_ent *sc2;
    FILE *outf;
    const char *killer;
    int prflags = 0;
    short upquest = 0, wintype = 0, uplevel = 0, uptype = 0; /* For network updating */
    char upsystem[SYSLEN], uplogin[LOGLEN];
    char *thissys; /* Holds the name of this system */
    char scoreline[100];
    static const int REASONLEN = 3;
    static const char *reason[] = {
        "killed",
        "quit",
        "A total winner",
        "somehow left",
    };
    const char *packend;

    signal(SIGINT, byebye);
    if(flags != WINNER && flags != SCOREIT && flags != UPDATE) {
        if(flags == CHICKEN) {
            packend = "when you quit";
        } else {
            packend = "at your untimely demise";
            mvaddstr(LINES - 1, 0, retstr);
            refresh();
            getstr(prbuf);
        }
        showpack(packend);
    }
    purse = 0; /* Steal all the gold */

    /* Open file and read list */
    if((outf = fopen(score_file, "r+b")) == nullptr) {
        fprintf(stderr, "errno = %d\n", errno);
        return;
    }

    /* Get this system's name */
    thissys = md_gethostname();

    for(scp = top_ten; scp <= &top_ten[NUMSCORE - 1]; scp++) {
        scp->sc_score = 0L;
        for(int i = 0; i < NAMELEN; i++) {
            scp->sc_name[i] = (char)rnd(255);
        }
        scp->sc_quest = RN;
        scp->sc_flags = RN;
        scp->sc_level = RN;
        scp->sc_monster = RN;
        scp->sc_ctype = 0;
        strncpy(scp->sc_system, thissys, SYSLEN);
        scp->sc_login[0] = '\0';
    }

    /*
     * If this is a SCOREIT optin (rogue -s), don't call byebye.  The
     * endwin() call in byebye() will result in a core dump.
     */
    if(flags == SCOREIT) {
        signal(SIGINT, SIG_DFL);
    } else {
        signal(SIGINT, byebye);
    }

    if(flags != SCOREIT && flags != UPDATE) {
        mvaddstr(LINES - 1, 0, retstr);
        refresh();
        fflush(stdout);
        getstr(prbuf);
    }

    /* Check for special options */
    if(strcmp(prbuf, "names") == 0) {
        prflags = REALLIFE;
    } else if(wizard) {
        if(strcmp(prbuf, "edit") == 0) {
            prflags = EDITSCORE;
        } else if(strcmp(prbuf, "add") == 0) {
            prflags = ADDSCORE;
            waswizard = FALSE; /* We want the new score recorded */
        }
    }

    fseek(outf, 0L, 0);
    /* Read the score and convert it to a compatible format */
    for(int i = 0; i < NUMSCORE; i++) {
        encread(top_ten[i].sc_name, NAMELEN, outf);
        encread(top_ten[i].sc_system, SYSLEN, outf);
        encread(top_ten[i].sc_login, LOGLEN, outf);
        encread(scoreline, 100, outf);
        sscanf(scoreline, " %lu %ld %ld %ld %ld %ld \n", &top_ten[i].sc_score, &top_ten[i].sc_flags,
               &top_ten[i].sc_level, &top_ten[i].sc_ctype, &top_ten[i].sc_monster,
               &top_ten[i].sc_quest);
    }

    /*
     * Insert player in list if need be
     */
    if(!waswizard) {
        char *login = nullptr;

        if(flags != UPDATE) {
            login = md_getusername();
        }

        if(flags == UPDATE) {
            update(top_ten, amount, upquest, whoami, wintype, uplevel, monst, uptype, upsystem,
                   uplogin);
        } else {
            if(prflags == ADDSCORE) { /* Overlay characteristic by new ones */
                char buffer[LINELEN];

                clear();
                mvaddstr(1, 0, "Score: ");
                mvaddstr(2, 0, "Quest (number): ");
                mvaddstr(3, 0, "Name: ");
                mvaddstr(4, 0, "System: ");
                mvaddstr(5, 0, "Login: ");
                mvaddstr(6, 0, "Level: ");
                mvaddstr(7, 0, "Char type: ");
                mvaddstr(8, 0, "Result: ");

                /* Get the score */
                move(1, 7);
                get_str(buffer, stdscr);
                amount = atol(buffer);

                /* Get the character's quest -- must be a number */
                move(2, 16);
                get_str(buffer, stdscr);
                quest_item = atoi(buffer);

                /* Get the character's name */
                move(3, 6);
                get_str(buffer, stdscr);
                strncpy(whoami, buffer, NAMELEN);

                /* Get the system */
                move(4, 8);
                get_str(buffer, stdscr);
                strncpy(thissys, buffer, SYSLEN);

                /* Get the login */
                move(5, 7);
                get_str(buffer, stdscr);
                strncpy(login, buffer, LOGLEN);

                /* Get the level */
                move(6, 7);
                get_str(buffer, stdscr);
                level = max_level = atoi(buffer);

                /* Get the character type */
                move(7, 11);
                get_str(buffer, stdscr);
                int ctype;
                for(ctype = 0; ctype < NUM_CHARTYPES; ctype++) {
                    if(EQSTR(buffer, char_class[ctype].name, strlen(buffer))) {
                        break;
                    }
                }
                player.t_ctype = ctype;

                /* Get the win type */
                move(8, 8);
                get_str(buffer, stdscr);
                switch(buffer[0]) {
                    case 'W':
                    case 'w':
                    case 'T':
                    case 't':
                        flags = WINNER;
                        break;

                    case 'Q':
                    case 'q':
                        flags = CHICKEN;
                        break;

                    case 'k':
                    case 'K':
                    default:
                        flags = KILLED;
                        break;
                }

                /* Get the monster if player was killed */
                if(flags == KILLED) {
                    mvaddstr(9, 0, "Death type: ");
                    get_str(buffer, stdscr);
                    if(buffer[0] == 'M' || buffer[0] == 'm') {
                        do {
                            monst = makemonster(TRUE, "Editing", "choose");
                        } while(monst < 0); /* Force a choice */
                    } else {
                        monst = getdeath();
                    }
                }
            }
            assert(login != nullptr);
            if(update(top_ten, amount, quest_item, whoami, flags,
                      (flags == WINNER) ? max_level : level, monst,
                      player.t_ctype, thissys, login)) {
            }
        }
    }

    /*
     * SCOREIT -- rogue -s option.  Never started curses if this option.
     * UPDATE -- network scoring update.  Never started curses if this option.
     * EDITSCORE -- want to delete or change a score.
     */
    /*   if (flags != SCOREIT && flags != UPDATE && prflags != EDITSCORE)
            endwin();    */

    if(flags != UPDATE) {
        if(flags != SCOREIT) {
            clear();
            refresh();
            endwin();
        }
        /*
         * Print the list
         */
        printf("\nTop %d Adventurers:\nRank     Score\tName\n", NUMSCORE);
        for(scp = top_ten; scp <= &top_ten[NUMSCORE - 1]; scp++) {
            const char *cclass;

            if(scp->sc_score != 0) {
                cclass = char_class[scp->sc_ctype].name;

                /* Make sure we have an in-bound reason */
                if(scp->sc_flags > REASONLEN) {
                    scp->sc_flags = REASONLEN;
                }

                printf("%3ld %10lu\t%s (%s)", scp - top_ten + 1, scp->sc_score, scp->sc_name,
                       cclass);

                if(prflags == REALLIFE) {
                    printf(" [in real life %.*s!%.*s]", SYSLEN, scp->sc_system, LOGLEN,
                           scp->sc_login);
                }
                printf(":\n\t\t%s on level %ld", reason[scp->sc_flags], scp->sc_level);

                switch(scp->sc_flags) {
                    case KILLED:
                        printf(" by");
                        killer = killname(scp->sc_monster);
                        printf(" %s", killer);
                        break;

                    case WINNER:
                        printf(" with the %s", rel_magic[scp->sc_quest].mi_name);
                        break;
                }

                if(prflags == EDITSCORE) {
                    fflush(stdout);
                    getstr(prbuf);
                    printf("\n");
                    if(prbuf[0] == 'd') {
                        for(sc2 = scp; sc2 < &top_ten[NUMSCORE - 1]; sc2++) {
                            *sc2 = *(sc2 + 1);
                        }
                        top_ten[NUMSCORE - 1].sc_score = 0;
                        for(int i = 0; i < NAMELEN; i++) {
                            top_ten[NUMSCORE - 1].sc_name[i] = (char)rnd(255);
                        }
                        top_ten[NUMSCORE - 1].sc_flags = RN;
                        top_ten[NUMSCORE - 1].sc_level = RN;
                        top_ten[NUMSCORE - 1].sc_monster = RN;
                        scp--;
                    } else if(prbuf[0] == 'e') {
                        printf("Death type: ");
                        getstr(prbuf);
                        if(prbuf[0] == 'M' || prbuf[0] == 'm') {
                            do {
                                scp->sc_monster = makemonster(TRUE, "Editing", "choose");
                            } while(scp->sc_monster < 0); /* Force a choice */
                        } else {
                            scp->sc_monster = getdeath();
                        }
                        clear();
                        refresh();
                    }
                } else {
                    printf("\n");
                }
            }
        }
        if((flags != SCOREIT) && (flags != UPDATE)) {
            printf("\n[Press return to exit]");
            fflush(stdout);
            fgets(prbuf, 80, stdin);
        }
    }

    fseek(outf, 0L, 0);
    /* Update the list file */
    for(int i = 0; i < NUMSCORE; i++) {
        memset(scoreline, 0, 100);
        encwrite(top_ten[i].sc_name, NAMELEN, outf);
        encwrite(top_ten[i].sc_system, SYSLEN, outf);
        encwrite(top_ten[i].sc_login, LOGLEN, outf);
        sprintf(scoreline, " %lu %ld %ld %ld %ld %ld \n", top_ten[i].sc_score, top_ten[i].sc_flags,
                top_ten[i].sc_level, top_ten[i].sc_ctype, top_ten[i].sc_monster,
                top_ten[i].sc_quest);
        encwrite(scoreline, 100, outf);
    }

    fclose(outf);
}

/*
 * showpack:
 *    Display the contents of the hero's pack
 */
void showpack(const char *howso) {
    char *iname;
    int cnt, packnum;
    linked_list *item;
    object *obj;

    idenpack();
    cnt = 1;
    clear();
    mvprintw(0, 0, "Contents of your pack %s:\n", howso);
    packnum = 'a';
    for(item = pack; item != nullptr; item = next(item)) {
        obj = OBJPTR(item);
        iname = inv_name(obj, FALSE);
        mvprintw(cnt, 0, "%c) %s\n", packnum++, iname);
        if(++cnt >= LINES - 2 && next(item) != nullptr) {
            cnt = 1;
            mvaddstr(LINES - 1, 0, morestr);
            refresh();
            wait_for(' ');
            clear();
        }
    }
    mvprintw(cnt + 1, 0, "--- %d  Gold Pieces ---", purse);
    refresh();
}

[[noreturn]] void total_winner() {
    linked_list *item;
    char c;
    int oldpurse;

    clear();
    standout();
    addstr("                                                               \n");
    addstr("  @   @               @   @           @          @@@  @     @  \n");
    addstr("  @   @               @@ @@           @           @   @     @  \n");
    addstr("  @   @  @@@  @   @   @ @ @  @@@   @@@@  @@@      @  @@@    @  \n");
    addstr("   @@@@ @   @ @   @   @   @     @ @   @ @   @     @   @     @  \n");
    addstr("      @ @   @ @   @   @   @  @@@@ @   @ @@@@@     @   @     @  \n");
    addstr("  @   @ @   @ @  @@   @   @ @   @ @   @ @         @   @  @     \n");
    addstr("   @@@   @@@   @@ @   @   @  @@@@  @@@@  @@@     @@@   @@   @  \n");
    addstr("                                                               \n");
    addstr("     Congratulations, you have made it to the light of day!    \n");
    standend();
    addstr("\nYou have joined the elite ranks of those who have escaped the\n");
    addstr("Dungeons of Doom alive.  You journey home and sell all your loot at\n");
    addstr("a great profit and are appointed leader of a ");
    switch(player.t_ctype) {
        case C_MAGICIAN:
            addstr("magic user's guild.\n");
            break;
        case C_FIGHTER:
            addstr("fighter's guild.\n");
            break;
        case C_RANGER:
            addstr("ranger's guild.\n");
            break;
        case C_CLERIC:
            addstr("monastery.\n");
            break;
        case C_PALADIN:
            addstr("monastery.\n");
            break;
        case C_MONK:
            addstr("monastery.\n");
            break;
        case C_DRUID:
            addstr("monastery.\n");
            break;
        case C_THIEF:
            addstr("thief's guild.\n");
            break;
        case C_ASSASIN:
            addstr("assassin's guild.\n");
            break;
        default:
            addstr("tavern.\n");
    }
    mvaddstr(LINES - 1, 0, spacemsg);
    refresh();
    wait_for(' ');
    clear();
    mvaddstr(0, 0, "   Worth  Item");
    oldpurse = purse;
    for(c = 'a', item = pack; item != nullptr; c++, item = next(item)) {
        object *obj = OBJPTR(item);
        long worth = get_worth(obj);
        if(obj->o_group == 0) {
            worth *= obj->o_count;
        }
        whatis(item);
        mvprintw(c - 'a' + 1, 0, "%c) %6d  %s", c, worth, inv_name(obj, FALSE));
        purse += worth;
    }
    mvprintw(c - 'a' + 1, 0, "   %5d  Gold Pieces          ", oldpurse);
    refresh();
    score(pstats.s_exp + purse, WINNER, '\0');
    endwin();
#ifdef PC7300
    endhardwin();
#endif
    exit(0);
}

int update(sc_ent top_ten[], long amount, long quest, char *whoami, long flags, long level,
           long monst, long ctype, char *system, char *login) {
    sc_ent *scp, *sc2;
    int retval = 0; /* 1 if a change, 0 otherwise */

    for(scp = top_ten; scp < &top_ten[NUMSCORE]; scp++) {
        if(amount >= scp->sc_score) {
            break;
        }
    }

    if(scp < &top_ten[NUMSCORE]) {
        retval = 1;

        for(sc2 = &top_ten[NUMSCORE - 1]; sc2 > scp; sc2--) {
            *sc2 = *(sc2 - 1);
        }
        scp->sc_score = amount;
        scp->sc_quest = quest;
        strncpy(scp->sc_name, whoami, NAMELEN);
        scp->sc_flags = flags;
        scp->sc_level = level;
        scp->sc_monster = monst;
        scp->sc_ctype = ctype;
        strncpy(scp->sc_system, system, SYSLEN);
        strncpy(scp->sc_login, login, LOGLEN);
    }

    return (retval);
}
