/*
 * Advanced Rogue
 * Copyright (C) 1984, 1985, 1986 Michael Morgan, Ken Dalka and AT&T
 * All rights reserved.
 *
 * Based on "Rogue: Exploring the Dungeons of Doom"
 * Copyright (C) 1980, 1981 Michael Toy, Ken Arnold and Glenn Wichman
 * All rights reserved.
 *
 * See the file LICENSE.TXT for full copyright and licensing information.
 */

/*
 * Rogue definitions and variable declarations
 */
#define AROGUE77

#include "metarog.h"
#include <curses.h>

/*
 * Maximum number of different things
 */
#define MAXDAEMONS 10
#define MAXFUSES 20
#define MAXROOMS 9
#define MAXTHINGS 9
#define MAXOBJ 9
#define MAXSTATS 72 /* max total of all stats at startup */
#define MAXPACK 23
#define MAXDOUBLE 14 /* Maximum number of times exppts is doubled */
#define MAXCONTENTS 10
#define MAXENCHANT 10 /* max number of enchantments on an item */
#define MAXTREAS 15   /* number monsters/treasure in treasure room */
#define MAXTRAPS 25
#define MAXTRPTRY 8                      /* attempts/level allowed for setting traps */
#define MAXDOORS 4                       /* Maximum doors to a room */
#define MAXCHANTS 16                     /* Maximum number of chants for a druid */
#define MAXPRAYERS 18                    /* Maximum number of prayers for cleric */
#define MAXSPELLS 20                     /* Maximum number of spells (for magician) */
#define MAXQUILL 13                      /* scrolls the Quill of Nagrom can write */
#define QUILLCHARGES 160                 /* max num of charges in the Quill of Nagrom */
#define NUMMONST 125                     /* Current number of monsters */
#define NUM_CNAMES 17                    /* number of names per character level */
#define NUMUNIQUE 27                     /* number of UNIQUE creatures */
#define NLEVMONS 3                       /* Number of new monsters per level */
#define NUMSCORE 10                      /* number of entries in score file */
#define HARDER 35                        /* at this level start making things harder */
#define LINELEN 256                      /* characters in a buffer */
#define JUG_EMPTY -1                     /* signifys that the alchemy jug is empty */
#define MAXPURCH (pstats.s_charisma / 3) /* # of purchases at post */

/* Movement penalties */
#define BACKPENALTY 3
#define SHOTPENALTY 2 /* In line of sight of missile */
#define DOORPENALTY 1 /* Moving out of current room */

/*
 * these defines are used in calls to get_item() to signify what
 * it is we want
 */
#define ALL -1
#define WEARABLE -2
#define CALLABLE -3
#define WIELDABLE -4
#define USEABLE -5
#define IDENTABLE -6
#define REMOVABLE -7
#define PROTECTABLE -8
#define ZAPPABLE -9
#define READABLE -10  /* (7.7) */
#define QUAFFABLE -11 /* (7.7) */

/*
 * stuff to do with encumberance
 */
#define NORMENCB 1500 /* normal encumberance */
#define F_SATIATED 0  /* player's stomach is very full (5.8s) (7.7) */
#define F_OKAY 1      /* have plenty of food in stomach */
#define F_HUNGRY 2    /* player is hungry */
#define F_WEAK 3      /* weak from lack of food */
#define F_FAINT 4     /* fainting from lack of food */

/*
 * actions a player/monster will take
 */
#define A_MOVE 0200     /* normal movement */
#define A_FREEZE 0201   /* frozen in place */
#define A_ATTACK 0202   /* trying to hit */
#define A_SELL 0203     /* trying to sell goods */
#define A_NIL 0204      /* not doing anything */
#define A_BREATHE 0205  /* breathing */
#define A_MISSILE 0206  /* Firing magic missiles */
#define A_SONIC 0207    /* Sounding a sonic blast */
#define A_SUMMON 0210   /* Summoning help */
#define A_USERELIC 0211 /* Monster uses a relic */
#define A_SLOW 0212     /* monster slows the player */
#define A_ZAP 0213      /* monster shoots a wand */
#define A_PICKUP 0214   /* player is picking something up */
#define A_USEWAND 0215  /* monster is shooting a wand */
#define A_THROW 't'
#define C_CAST 'C'
#define C_COUNT '*'
#define C_DIP 'D'
#define C_DROP 'd'
#define C_EAT 'e'
#define C_PRAY 'p'
#define C_CHANT 'c'
#define C_QUAFF 'q'
#define C_READ 'r'
#define C_SEARCH 's'
#define C_SETTRAP '^'
#define C_TAKEOFF 'T'
#define C_USE CTRL('U')
#define C_WEAR 'W'
#define C_WIELD 'w'
#define C_ZAP 'z'

/* Possible ways for the hero to move */
#define H_TELEPORT 0

/*
 * return values for get functions
 */
#define NORM 0  /* normal exit */
#define QUIT 1  /* quit option setting */
#define MINUS 2 /* back up one option */

/*
 * The character types
 */
#define C_FIGHTER 0
#define C_RANGER 1
#define C_PALADIN 2
#define C_MAGICIAN 3
#define C_CLERIC 4
#define C_THIEF 5
#define C_ASSASIN 6
#define C_DRUID 7
#define C_MONK 8
#define C_MONSTER 9
#define NUM_CHARTYPES 10

/* define the ability types */
#define A_INTELLIGENCE 0
#define A_STRENGTH 1
#define A_WISDOM 2
#define A_DEXTERITY 3
#define A_CONSTITUTION 4
#define A_CHARISMA 5
#define NUMABILITIES 6

/* values for games end */
#define UPDATE -2
#define SCOREIT -1
#define KILLED 0
#define CHICKEN 1
#define WINNER 2

/*
 * definitions for function step_ok:
 *    MONSTOK indicates it is OK to step on a monster -- it
 *    is only OK when stepping diagonally AROUND a monster;
 *     it is also OK if the stepper is a friendly monster and
 *     is in a fighting mood.
 */
#define MONSTOK 1
#define NOMONST 2
#define FIGHTOK 3

/*
 * used for ring stuff
 */
#define LEFT_1 0
#define LEFT_2 1
#define LEFT_3 2
#define LEFT_4 3
#define RIGHT_1 4
#define RIGHT_2 5
#define RIGHT_3 6
#define RIGHT_4 7
#define NUM_FINGERS 8

/*
 * used for micellaneous magic (MM) stuff
 */
#define WEAR_BOOTS 0
#define WEAR_BRACERS 1
#define WEAR_CLOAK 2
#define WEAR_GAUNTLET 3
#define WEAR_JEWEL 4
#define WEAR_NECKLACE 5
#define NUM_MM 6

/*
 * All the fun defines
 */
#define inroom(rp, cp)                                                                             \
    ((cp)->x <= (rp)->r_pos.x + ((rp)->r_max.x - 1) && (rp)->r_pos.x <= (cp)->x &&                 \
     (cp)->y <= (rp)->r_pos.y + ((rp)->r_max.y - 1) && (rp)->r_pos.y <= (cp)->y)
#define winat(y, x) ((mvwinch(mw, y, x) == ' ' ? mvwinch(stdscr, y, x) : winch(mw)) & A_CHARTEXT)
#define debug                                                                                      \
    if(wizard)                                                                                     \
    msg
#define RN (((seed = seed * 11109 + 13849) & 0x7fff) >> 1)
#define unc(cp) (cp).y, (cp).x
#define cmov(xy) move((xy).y, (xy).x)
#define DISTANCE(y1, x1, y2, x2) ((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1))
#define OBJPTR(what) (object *)((*what).l_data)
#define THINGPTR(what) (thing *)((*what).l_data)
#define DOORPTR(what) (coord *)((*what).l_data)
#define ce(a, b) ((a).x == (b).x && (a).y == (b).y)
#define draw(window) wrefresh(window)
#define hero player.t_pos
#define pstats player.t_stats
#define max_stats player.maxstats
#define pack player.t_pack
#define attach(a, b) _attach(&a, b)
#define detach(a, b) _detach(&a, b)
#define o_free_list(a) _o_free_list(&a)
#define r_free_list(a) _r_free_list(&a)
#define t_free_list(a) _t_free_list(&a)

#undef CTRL
#define CTRL(ch) (ch & 037)

#define ALLOC(x) calloc((unsigned int)(x), 1)
#define FREE(x) free((char *)x)
#define EQSTR(a, b, c) (strncmp(a, b, (size_t)c) == 0)
#define EQUAL(a, b) (strcmp(a, b) == 0)
#define ISRING(h, r) (cur_ring[h] != NULL && cur_ring[h]->o_which == r)
#define ISWEARING(r)                                                                               \
    (ISRING(LEFT_1, r) || ISRING(LEFT_2, r) || ISRING(LEFT_3, r) || ISRING(LEFT_4, r) ||           \
     ISRING(RIGHT_1, r) || ISRING(RIGHT_2, r) || ISRING(RIGHT_3, r) || ISRING(RIGHT_4, r))
#define newgrp() ++group
#define o_charges o_ac
#define o_kind o_ac
#define ISMULT(type) (type == FOOD)
#define isrock(ch) ((ch == WALL) || (ch == '-') || (ch == '|') || (ch == SECRETDOOR))
#define invisible(monst)                                                                           \
    (((on(*monst, ISINVIS) || (on(*monst, ISSHADOW) && rnd(100) < 90)) && off(player, CANSEE)) ||  \
     (on(*monst, CANSURPRISE) && !ISWEARING(R_ALERT)))
#define is_stealth(tp) (rnd(25) < (tp)->t_stats.s_dext || (tp == &player && ISWEARING(R_STEALTH)))

#define has_light(rp) (((rp)->r_flags & HASFIRE) || ISWEARING(R_LIGHT))

#define mi_wght mi_worth
#define mi_food mi_curse

/*
 * Ways to die
 */
#define D_PETRIFY 0x10000 + 1
#define D_ARROW 0x10000 + 2
#define D_DART 0x10000 + 3
#define D_POISON 0x10000 + 4
#define D_BOLT 0x10000 + 5
#define D_SUFFOCATION 0x10000 + 6
#define D_POTION 0x10000 + 7
#define D_INFESTATION 0x10000 + 8
#define D_DROWN 0x10000 + 9
#define D_ROT 0x10000 + 10
#define D_CONSTITUTION 0x10000 + 11
#define D_STRENGTH 0x10000 + 12
#define D_SIGNAL 0x10000 + 13
#define D_CHOKE 0x10000 + 14
#define D_STRANGLE 0x10000 + 15
#define D_FALL 0x10000 + 16
#define D_RELIC 0x10000 + 17
#define D_STARVATION 0x10000 + 18 /* (5.8s) (7.7) */
#define D_FOOD_CHOKE 0x10000 + 19 /* (5.8s) (7.7) */
#define D_SCROLL 0x10000 + 20 /* (7.7) */
#define DEATHNUM 20 /* number of ways to die */

/*
 * Things that appear on the screens
 */
#define WALL ' '
#define PASSAGE '#'
#define DOOR '+'
#define FLOOR '.'
#define VPLAYER '@'
#define IPLAYER '_'
#define POST '^'
#define TRAPDOOR '>'
#define ARROWTRAP '{'
#define SLEEPTRAP '$'
#define BEARTRAP '}'
#define TELTRAP '~'
#define DARTTRAP '`'
#define POOL '"'
#define MAZETRAP '\''
#define SECRETDOOR '&'
#define STAIRS '%'
#define GOLD '*'
#define POTION '!'
#define SCROLL '?'
#define MAGIC '$'
#define BMAGIC '>' /*    Blessed    magic    */
#define CMAGIC '<' /*    Cursed    magic    */
#define FOOD ':'
#define WEAPON ')'
#define MISSILE '*' /*    Magic Missile    */
#define ARMOR ']'
#define MM ';'
#define RELIC ','
#define RING '='
#define STICK '/'
#define FOREST '\\'

/*
 * Various constants
 */
#define PASSWD "foobar"
#define FIGHTBASE 10
#define SPELLTIME ((max(30 - pstats.s_lvl, 5L)))
#define BEARTIME 17
#define CLOAK_TIME (roll(20, 20))
#define SLEEPTIME 7
#define FREEZETIME 11
#define PAINTIME (roll(2, 12))
#define HEALTIME 30
#define CHILLTIME (roll(20, 4))
#define SMELLTIME 20
#define STONETIME (roll(10, 2))
#define HASTETIME 11
#define SICKTIME 25
#define WANDERTIME (max(5, (HARDER * 2) - rnd(vlevel)))
#define BEFORE 1
#define AFTER 2
#define HUHDURATION (50 + rnd(30))
#define SEEDURATION 850
#define SKILLDURATION (100 + rnd(50))
#define CLRDURATION 50
#define GONETIME 200
#define FIRETIME (200 + roll(5, 5))
#define COLDTIME (200 + roll(5, 5))
#define BOLTTIME (200 + roll(5, 5))
#define FLYTIME 300
#define DUSTTIME (30 + roll(5, 10))
#define PHASEDURATION 300
#define MORETIME 100
#define STINKTIME 16
#define STOMACHSIZE 1500
#define ESCAPE 27
#define BOLT_LENGTH 10
#define MARKLEN 20
#define DAYLENGTH 400
#define ALCHEMYTIME (400 + rnd(150))

/* Save against things */
#define VS_POISON 00
#define VS_PARALYZATION 00
#define VS_DEATH 00
#define VS_PETRIFICATION 01
#define VS_WAND 02
#define VS_BREATH 03
#define VS_MAGIC 04

/* attributes for treasures in dungeon */
#define ISCURSED 01
#define ISKNOW 02
#define ISPOST 04 /* object is in a trading post (5.8) (7.7) */
#define ISMETAL 010
#define ISPROT 020 /* object is protected */
#define ISBLESSED 040
#define ISPOISON 0100
#define ISMISL 020000
#define ISMANY 040000

/* Various flag bits */
#define ISDARK 0x00000001
#define ISGONE 0x00000002
#define ISTREAS 0x00000004
#define ISFOUND 0x00000008
#define ISTHIEFSET 0x00000010
#define FORCEDARK 0x00000020

/* 1st set of creature flags (this might include player) */
#define ISBLIND 0x00000001u
#define ISINWALL 0x00000002u
#define ISRUN 0x00000004u
#define ISFLEE 0x00000008u
#define ISINVIS 0x00000010u
#define ISMEAN 0x00000020u
#define ISGREED 0x00000040u
#define CANSHOOT 0x00000080u
#define ISHELD 0x00000100u
#define ISHUH 0x00000200u
#define ISREGEN 0x00000400u
#define CANHUH 0x00000800u
#define CANSEE 0x00001000u
#define HASFIRE 0x00002000u
#define ISSLOW 0x00004000u
#define ISHASTE 0x00008000u
#define ISCLEAR 0x00010000u
#define CANINWALL 0x00020000u
#define ISDISGUISE 0x00040000u
#define CANBLINK 0x00080000u
#define CANSNORE 0x00100000u
#define HALFDAMAGE 0x00200000u
#define CANSUCK 0x00400000u
#define CANRUST 0x00800000u
#define CANPOISON 0x01000000u
#define CANDRAIN 0x02000000u
#define ISUNIQUE 0x04000000u
#define STEALGOLD 0x08000000u
/*
 * Second set of flags
 */
#define STEALMAGIC 0x10000001u
#define CANDISEASE 0x10000002u
#define HASDISEASE 0x10000004u
#define CANSUFFOCATE 0x10000008u
#define DIDSUFFOCATE 0x10000010u
#define BOLTDIVIDE 0x10000020u
#define BLOWDIVIDE 0x10000040u
#define NOCOLD 0x10000080u
#define TOUCHFEAR 0x10000100u
#define BMAGICHIT 0x10000200u
#define NOFIRE 0x10000400u
#define NOBOLT 0x10000800u
#define CARRYGOLD 0x10001000u
#define CANITCH 0x10002000u
#define HASITCH 0x10004000u
#define DIDDRAIN 0x10008000u
#define WASTURNED 0x10010000u
#define CANSELL 0x10020000u
#define CANBLIND 0x10040000u
#define NOACID 0x10080000u
#define NOSLOW 0x10100000u
#define NOFEAR 0x10200000u
#define NOSLEEP 0x10400000u
#define NOPARALYZE 0x10800000u
#define NOGAS 0x11000000u
#define CANMISSILE 0x12000000u
#define CMAGICHIT 0x14000000u
#define CANPAIN 0x18000000u

/*
 * Third set of flags
 */
#define CANSLOW 0x20000001u
#define CANTUNNEL 0x20000002u
#define TAKEWISDOM 0x20000004u
#define NOMETAL 0x20000008u
#define MAGICHIT 0x20000010u
#define CANINFEST 0x20000020u
#define HASINFEST 0x20000040u
#define NOMOVE 0x20000080u
#define CANSHRIEK 0x20000100u
#define CANDRAW 0x20000200u
#define CANSMELL 0x20000400u
#define CANPARALYZE 0x20000800u
#define CANROT 0x20001000u
#define ISSCAVENGE 0x20002000u
#define DOROT 0x20004000u
#define CANSTINK 0x20008000u
#define HASSTINK 0x20010000u
#define ISSHADOW 0x20020000u
#define CANCHILL 0x20040000u
#define CANHUG 0x20080000u
#define CANSURPRISE 0x20100000u
#define CANFRIGHTEN 0x20200000u
#define CANSUMMON 0x20400000u
#define TOUCHSTONE 0x20800000u
#define LOOKSTONE 0x21000000u
#define CANHOLD 0x22000000u
#define DIDHOLD 0x24000000u
#define DOUBLEDRAIN 0x28000000u

/*
 * Fourth set of flags
 */
#define CANBRANDOM 0x30000001u /* Types of breath */
#define CANBACID 0x30000002u   /* acid */
#define CANBFIRE 0x30000004u   /* Fire */
#define CANBCGAS 0x30000008u   /* confusion gas */
#define CANBBOLT 0x30000010u   /* lightning bolt */
#define CANBGAS 0x30000020u    /* clorine gas */
#define CANBICE 0x30000040u    /* ice */
#define CANBFGAS 0x30000080u   /* Fear gas */
#define CANBPGAS 0x30000100u   /* Paralyze gas */
#define CANBSGAS 0x30000200u   /* Sleeping gas */
#define CANBSLGAS 0x30000400u  /* Slow gas */
#define CANBREATHE 0x300007ffu /* Can it breathe at all? */
/*
 * Fifth set of flags
 */
#define ISUNDEAD 0x40000001u
#define CANSONIC 0x40000002u
#define TURNABLE 0x40000004u
#define TAKEINTEL 0x40000008u
#define NOSTAB 0x40000010u
#define CANDISSOLVE 0x40000020u
#define ISFLY 0x40000040u       /* creature can fly */
#define CANTELEPORT 0x40000080u /* creature can teleport */
#define CANEXPLODE 0x40000100u  /* creature explodes when hit */
#define CANDANCE 0x40000200u    /* creature can make hero "dance" */
#define ISDANCE 0x40000400u     /* creature (hero) is dancing */
#define CARRYFOOD 0x40000800u
#define CARRYSCROLL 0x40001000u
#define CARRYPOTION 0x40002000u
#define CARRYRING 0x40004000u
#define CARRYSTICK 0x40008000u
#define CARRYMISC 0x40010000u
#define CARRYMDAGGER 0x40020000u  /* Dagger of Musty */
#define CARRYCLOAK 0x40040000u    /* Cloak of Emori */
#define CARRYANKH 0x40080000u     /* Ankh of Heil */
#define CARRYSTAFF 0x40100000u    /* Staff of Ming */
#define CARRYWAND 0x40200000u     /* Wand of Orcus */
#define CARRYROD 0x40400000u      /* Rod of Asmodeus */
#define CARRYYAMULET 0x40800000u  /* Amulet of Yendor */
#define CARRYMANDOLIN 0x41000000u /* Mandolin of Brian */
#define MISSEDDISP 0x42000000u    /* Missed Cloak of Displacement */
#define CANBSTAB 0x44000000u      /* Can backstab */
#define ISGUARDIAN 0x48000000u    /* Guardian of a treasure room */

#define CARRYHORN 0x50000001u       /* Horn of Geryon */
#define CARRYMSTAR 0x50000002u      /* Morning Star of Hruggek */
#define CARRYFLAIL 0x50000004u      /* Flail of Yeenoghu */
#define CARRYWEAPON 0x50000008u     /* A generic weapon */
#define CANAGE 0x50000010u          /* can age you */
#define CARRYDAGGER 0x50000020u     /* carry's a dumb old dagger */
#define AREMANY 0x50000040u         /* they come in droves */
#define CARRYEYE 0x50000080u        /* has the eye of Vecna */
#define HASSUMMONED 0x50000100u     /* has already summoned */
#define ISSTONE 0x50000200u         /* has been turned to stone */
#define NODETECT 0x50000400u        /* detect monster will not show him */
#define NOSTONE 0x50000800u         /* creature made its save vrs stone */
#define CARRYQUILL 0x50001000u      /* has the quill of Nagrom */
#define CARRYAXE 0x50002000u        /* has the axe of Aklad */
#define TOUCHSLOW 0x50004000u       /* touch will slow hero */
#define WASDISRUPTED 0x50008000u    /* creature was disrupted by player */
#define CARRYARMOR 0x50010000u      /* creature will pick up armor */
#define CARRYBAMULET 0x50020000u    /* amulet of skoraus stonebones */
#define CARRYSURTURRING 0x50040000u /* ring of Surtur */
#define ISCHARMED 0x50080000u       /* is the monster charmed? */
#define ISFRIENDLY 0x50080000u      /* monster friendly for any reason? */

#define ISREADY 0x60000001u
#define ISDEAD 0x60000002u
#define ISELSEWHERE 0x60000004u

/* Masks for choosing the right flag */
#define FLAGMASK 0xf0000000
#define FLAGINDEX 0x0000000f
#define FLAGSHIFT 28
#define MAXFLAGS 25 /* max initial flags per creature */

/*
 * Mask for cancelling special abilities
 * The flags listed here will be the ones left on after the
 * cancellation takes place
 */
#define CANC0MASK                                                                                  \
    (ISBLIND | ISINWALL | ISRUN | ISFLEE | ISMEAN | ISGREED | CANSHOOT | ISHELD | ISHUH | ISSLOW | \
     ISHASTE | ISCLEAR | ISUNIQUE)
#define CANC1MASK (HASDISEASE | DIDSUFFOCATE | CARRYGOLD | HASITCH | CANSELL | DIDDRAIN | WASTURNED)
#define CANC2MASK (HASINFEST | NOMOVE | ISSCAVENGE | DOROT | HASSTINK | DIDHOLD)
#define CANC3MASK (CANBREATHE)
#define CANC4MASK                                                                                  \
    (ISUNDEAD | CANSONIC | NOSTAB | ISFLY | CARRYFOOD | CANEXPLODE | ISDANCE | CARRYSCROLL |       \
     CARRYPOTION | CARRYRING | CARRYSTICK | CARRYMISC | CARRYMDAGGER | CARRYCLOAK | CARRYANKH |    \
     CARRYSTAFF | CARRYWAND | CARRYROD | CARRYYAMULET | CARRYMANDOLIN | ISGUARDIAN)
#define CANC5MASK                                                                                  \
    (CARRYHORN | CARRYMSTAR | CARRYFLAIL | CARRYEYE | CARRYDAGGER | HASSUMMONED | AREMANY |        \
     CARRYWEAPON | NOSTONE | CARRYQUILL | CARRYAXE | WASDISRUPTED | CARRYARMOR | CARRYBAMULET |    \
     CARRYSURTURRING)
#define CANC6MASK (0)
#define CANC7MASK (0)
#define CANC8MASK (0)
#define CANC9MASK (0)
#define CANCAMASK (0)
#define CANCBMASK (0)
#define CANCCMASK (0)
#define CANCDMASK (0)
#define CANCEMASK (0)
#define CANCFMASK (0)

/* types of things */
#define TYP_POTION 0
#define TYP_SCROLL 1
#define TYP_FOOD 2
#define TYP_WEAPON 3
#define TYP_ARMOR 4
#define TYP_RING 5
#define TYP_STICK 6
#define TYP_MM 7
#define TYP_RELIC 8
#define NUMTHINGS 9
/*
 * food types
 */
#define E_RATION 0
#define E_APPLE 1
#define E_BANANA 2
#define E_BLUEBERRY 3
#define E_CANDLEBERRY 4
#define E_CAPRIFIG 5
#define E_DEWBERRY 6
#define E_ELDERBERRY 7
#define E_GOOSEBERRY 8
#define E_GUANABANA 9
#define E_HAGBERRY 10
#define E_JABOTICABA 11
#define E_PEACH 12
#define E_PITANGA 13
#define E_PRICKLEY 14
#define E_RAMBUTAN 15
#define E_SAPODILLA 16
#define E_SOURSOP 17
#define E_STRAWBERRY 18
#define E_SWEETSOP 19
#define E_WHORTLEBERRY 20
#define MAXFOODS 21
/*
 * Potion types
 */
#define P_CLEAR 0
#define P_ABIL 1
#define P_SEEINVIS 2
#define P_HEALING 3
#define P_MFIND 4
#define P_TFIND 5
#define P_RAISE 6
#define P_HASTE 7
#define P_RESTORE 8
#define P_PHASE 9
#define P_INVIS 10
#define P_FLY 11
#define P_FFIND 12
#define P_SKILL 13
#define P_FIRE 14
#define P_COLD 15
#define P_LIGHTNING 16
#define P_POISON 17
#define MAXPOTIONS 18
/*
 * Scroll types
 */
#define S_CONFUSE 0
#define S_MAP 1
#define S_LIGHT 2
#define S_HOLD 3
#define S_SLEEP 4
#define S_ALLENCH 5
#define S_IDENT 6
#define S_SCARE 7
#define S_GFIND 8
#define S_TELEP 9
#define S_CREATE 10
#define S_REMOVE 11
#define S_PETRIFY 12
#define S_GENOCIDE 13
#define S_CURING 14
#define S_MAKEIT 15
#define S_PROTECT 16
#define S_FINDTRAPS 17
#define S_RUNES 18
#define S_CHARM 19
#define MAXSCROLLS 20

/*
 * Weapon types
 */
#define MACE 0        /* mace */
#define SWORD 1       /* long sword */
#define BOW 2         /* short bow */
#define ARROW 3       /* arrow */
#define DAGGER 4      /* dagger */
#define ROCK 5        /* rocks */
#define TWOSWORD 6    /* two-handed sword */
#define SLING 7       /* sling */
#define DART 8        /* darts */
#define CROSSBOW 9    /* crossbow */
#define BOLT 10       /* crossbow bolt */
#define SPEAR 11      /* spear */
#define TRIDENT 12    /* trident */
#define SPETUM 13     /* spetum */
#define BARDICHE 14   /* bardiche */
#define PIKE 15       /* pike */
#define BASWORD 16    /* bastard sword */
#define HALBERD 17    /* halberd */
#define BATTLEAXE 18  /* battle axe */
#define MAXWEAPONS 19 /* types of weapons */
#define NONE 100      /* no weapon */

/*
 * Armor types
 */
#define LEATHER 0
#define RING_MAIL 1
#define STUDDED_LEATHER 2
#define SCALE_MAIL 3
#define PADDED_ARMOR 4
#define CHAIN_MAIL 5
#define SPLINT_MAIL 6
#define BANDED_MAIL 7
#define PLATE_MAIL 8
#define PLATE_ARMOR 9
#define MAXARMORS 10

/*
 * Ring types
 */
#define R_PROTECT 0
#define R_ADDSTR 1
#define R_SUSABILITY 2
#define R_SEARCH 3
#define R_SEEINVIS 4
#define R_ALERT 5
#define R_AGGR 6
#define R_ADDHIT 7
#define R_ADDDAM 8
#define R_REGEN 9
#define R_DIGEST 10
#define R_TELEPORT 11
#define R_STEALTH 12
#define R_ADDINTEL 13
#define R_ADDWISDOM 14
#define R_HEALTH 15
#define R_CARRY 16
#define R_LIGHT 17
#define R_DELUSION 18
#define R_FEAR 19
#define R_HEROISM 20
#define R_FIRE 21
#define R_WARMTH 22
#define R_VAMPREGEN 23
#define R_FREEDOM 24
#define R_TELCONTROL 25
#define MAXRINGS 26

/*
 * Rod/Wand/Staff types
 */

#define WS_LIGHT 0
#define WS_HIT 1
#define WS_ELECT 2
#define WS_FIRE 3
#define WS_COLD 4
#define WS_POLYMORPH 5
#define WS_MISSILE 6
#define WS_SLOW_M 7
#define WS_DRAIN 8
#define WS_CHARGE 9
#define WS_TELMON 10
#define WS_CANCEL 11
#define WS_CONFMON 12
#define WS_DISINTEGRATE 13
#define WS_PETRIFY 14
#define WS_PARALYZE 15
#define WS_MDEG 16
#define WS_CURING 17
#define WS_WONDER 18
#define WS_FEAR 19
#define MAXSTICKS 20

/*
 * miscellaneous magic items
 */
#define MM_JUG 0
#define MM_BEAKER 1
#define MM_BOOK 2
#define MM_ELF_BOOTS 3
#define MM_BRACERS 4
#define MM_OPEN 5
#define MM_HUNGER 6
#define MM_DISP 7
#define MM_PROTECT 8
#define MM_DRUMS 9
#define MM_DISAPPEAR 10
#define MM_CHOKE 11
#define MM_G_DEXTERITY 12
#define MM_G_OGRE 13
#define MM_JEWEL 14
#define MM_KEOGHTOM 15
#define MM_R_POWERLESS 16
#define MM_FUMBLE 17
#define MM_ADAPTION 18
#define MM_STRANGLE 19
#define MM_DANCE 20
#define MM_SKILLS 21
#define MAXMM 22

/*
 * Relic types
 */
#define MUSTY_DAGGER 0
#define EMORI_CLOAK 1
#define HEIL_ANKH 2
#define MING_STAFF 3
#define ORCUS_WAND 4
#define ASMO_ROD 5
#define YENDOR_AMULET 6
#define BRIAN_MANDOLIN 7
#define GERYON_HORN 8
#define HRUGGEK_MSTAR 9
#define YEENOGHU_FLAIL 10
#define EYE_VECNA 11
#define AXE_AKLAD 12
#define QUILL_NAGROM 13
#define STONEBONES_AMULET 14
#define SURTUR_RING 15
#define MAXRELIC 16

#define LEVEL 600
#define vlevel max(level, turns / LEVEL + 1)
/*
 * Now we define the structures and types
 */

enum daemon_id {
    d_none,
    d_rollwand,
    d_doctor,
    d_stomach,
    d_trap_look,
    d_eat_gold,
    d_ring_search,
    d_ring_teleport,
    d_fumble,
    d_strangle,
    d_unconfuse,
    d_swander,
    d_spell_recovery,
    d_chant_recovery,
    d_prayer_recovery,
    d_cure_disease,
    d_unstink,
    d_res_strength,
    d_undance,
    d_suffocate,
    d_wghtchk,
    d_dust_appear,
    d_unchoke,
    d_sight,
    d_changeclass,
    d_cloak_charge,
    d_quill_charge,
    d_nohaste,
    d_noslow,
    d_unclrhead,
    d_unsee,
    d_unphase,
    d_land,
    d_appear,
    d_unskill,
    d_nofire,
    d_nocold,
    d_nobolt,

    d_dont_save,

    d_alchemy,

};

struct delayed_action {
    long d_type;
    enum daemon_id d_id;
    union {
        intptr_t arg;
        void *varg;
    } d_;
    long d_time;
};

/*
 * character types
 */
struct character_types {
    char name[40];  /* name of character class          */
    long start_exp; /* starting exp pts for 2nd level     */
    long cap;       /* stop doubling here               */
    int hit_pts;    /* hit pts gained per level          */
    int base;       /* Base to-hit value (AC 10)          */
    long max_lvl;   /* Maximum level for changing value     */
    int factor;     /* Amount base changes each time     */
    int offset;     /* What to offset level               */
    int range;      /* Range of levels for each offset     */
};

/*
 * level types
 */
typedef enum {
    NORMLEV, /* normal level */
    POSTLEV, /* trading post level */
    MAZELEV, /* maze level */
    OUTSIDE, /* outside level */
    STARTLEV /* beginning of the game (7.7) */
} LEVTYPE;

/*
 * Help list
 */

struct h_list {
    int h_ch;
    const char *h_desc;
};

/*
 * Coordinate data type
 */
struct coord {
    int x;
    int y;
};

/*
 * structure for the ways to die
 */
struct death_type {
    long reason;
    const char *name;
};

/*
 * Linked list data type
 */
struct linked_list {
    linked_list *l_next;
    linked_list *l_prev;
    char *l_data; /* Various structure pointers */
};

inline linked_list *next(linked_list *ptr) {
    return (*ptr).l_next;
}
inline linked_list *prev(linked_list *ptr) {
    return (*ptr).l_prev;
}
inline char *&ldata(linked_list *ptr) {
    return (*ptr).l_data;
}

/*
 * Stuff about magic items
 */

struct magic_item {
    const char *mi_name;
    long mi_prob;
    long mi_worth;
    long mi_curse;
    long mi_bless;
};

/*
 * Room structure
 */
struct room {
    coord r_pos;          /* Upper left corner */
    coord r_max;          /* Size of room */
    long r_flags;         /* Info about the room */
    linked_list *r_fires; /* List of fire creatures in room */
    linked_list *r_exit;  /* Linked list of exits */
};

/*
 * Array of all traps on this level
 */

struct trap {
    int tr_type;   /* What kind of trap */
    int tr_show;   /* Where disguised trap looks like */
    coord tr_pos;  /* Where trap is */
    long tr_flags; /* Info about trap (i.e. ISFOUND) */
};

/*
 * Structure describing a fighting being
 */
struct stats {
    long s_str;      /* Strength */
    long s_intel;    /* Intelligence */
    long s_wisdom;   /* Wisdom */
    long s_dext;     /* Dexterity */
    long s_const;    /* Constitution */
    long s_charisma; /* Charisma */
    long s_exp;      /* Experience */
    int s_lvladj;    /* how much level is adjusted */
    long s_lvl;      /* Level of mastery */
    long s_arm;      /* Armor class */
    long s_hpt;      /* Hit points */
    long s_pack;     /* current weight of his pack */
    long s_carry;    /* max weight he can carry */
    char s_dmg[30];  /* String describing damage done */
};

/*
 * Structure describing a fighting being (monster at initialization)
 */
struct mstats {
    long s_str;         /* Strength */
    long s_dex;         /* dexterity */
    long s_move;        /* movement rate */
    unsigned int s_exp; /* Experience */
    long s_lvl;         /* Level of mastery */
    long s_arm;         /* Armor class */
    const char *s_hpt;  /* Hit points */
    const char *s_dmg;  /* String describing damage done */
};

/*
 * Structure for monsters and player
 */
struct thing {
    bool t_wasshot;       /* Was character shot last round? */
    int t_type;           /* What it is */
    int t_disguise;       /* What mimic looks like */
    int t_oldch;          /* Character that was where it was */
    long t_ctype;         /* Character type */
    long t_index;         /* Index into monster table */
    long t_no_move;       /* How long the thing can't move */
    long t_quiet;         /* used in healing */
    long t_movement;      /* Base movement rate */
    int t_action;         /* Action we're waiting to do */
    long t_artifact;      /* base chance of using artifact */
    long t_wand;          /* base chance of using wands */
    long t_summon;        /* base chance of summoning */
    long t_cast;          /* base chance of casting a spell */
    long t_breathe;       /* base chance to swing at player */
    char *t_name;         /* name player gave his pet */
    coord *t_doorgoal;    /* What door are we heading to? */
    coord *t_dest;        /* Where it is running to */
    coord t_pos;          /* Position */
    coord t_oldpos;       /* Last position */
    coord t_newpos;       /* Where we want to go */
    long t_flags[16];     /* State word */
    linked_list *t_pack;  /* What the thing is carrying */
    linked_list *t_using; /* What the thing is using */
    int t_selection;
    stats t_stats;  /* Physical description */
    stats maxstats; /* maximum(or initial) stats */
    long t_reserved;
};

inline auto on(thing &th, long flag) {
    return (((th).t_flags[(flag >> FLAGSHIFT) & FLAGINDEX] & flag) != 0);
}
inline auto off(thing &th, long flag) {
    return (((th).t_flags[(flag >> FLAGSHIFT) & FLAGINDEX] & flag) == 0);
}
inline void turn_on(thing &th, long flag) {
    ((th).t_flags[(flag >> FLAGSHIFT) & FLAGINDEX] |= (flag & ~FLAGMASK));
}
inline void turn_off(thing &th, long flag) {
    ((th).t_flags[(flag >> FLAGSHIFT) & FLAGINDEX] &= ~flag);
}

/*
 * Array containing information on all the various types of monsters
 */
struct monster {
    const char *m_name;     /* What to call the monster */
    long m_carry;           /* Probability of carrying something */
    bool m_normal;          /* Does monster exist? */
    bool m_wander;          /* Does monster wander? */
    int m_appear;           /* What does monster look like? */
    const char *m_intel;    /* Intelligence range */
    long m_flags[MAXFLAGS]; /* Things about the monster */
    const char *m_typesum;  /* type of creature can he summon */
    long m_numsum;          /* how many creatures can he summon */
    long m_add_exp;         /* Added experience per hit point */
    mstats m_stats;         /* Initial stats */
};

/*
 * Structure for a thing that the rogue can carry
 */

struct object {
    int o_type;            /* What kind of object it is */
    coord o_pos;           /* Where it lives on the screen */
    const char *o_text;    /* What it says if you read it */
    char o_launch;         /* What you need to launch it */
    char o_damage[8];      /* Damage if used like sword */
    char o_hurldmg[8];     /* Damage if thrown */
    linked_list *contents; /* contents of this object */
    long o_count;          /* Count for plural objects */
    int o_which;           /* Which object of a type it is */
    long o_hplus;          /* Plusses to hit */
    long o_dplus;          /* Plusses to damage */
    long o_ac;             /* Armor class */
    long o_flags;          /* Information about objects */
    long o_group;          /* Group number for this object */
    long o_weight;         /* weight of this object */
    char o_mark[MARKLEN];  /* Mark the specific object */
};
/*
 * weapon structure
 */
struct init_weps {
    const char *w_name; /* name of weapon */
    const char *w_dam;  /* hit damage */
    const char *w_hrl;  /* hurl damage */
    char w_launch;      /* need to launch it */
    long w_flags;       /* flags */
    int w_rate;         /* rate of fire */
    long w_wght;        /* weight of weapon */
    long w_worth;       /* worth of this weapon */
};

/*
 * armor structure
 */
struct init_armor {
    const char *a_name; /* name of armor */
    long a_prob;        /* chance of getting armor */
    long a_class;       /* normal armor class */
    long a_worth;       /* worth of armor */
    long a_wght;        /* weight of armor */
};

struct spells {
    long s_which; /* which scroll or potion */
    long s_cost;  /* cost of casting spell */
    long s_type;  /* scroll or potion */
    long s_flag;  /* is the spell blessed/cursed? */
};

struct quill {
    int s_which; /* which scroll to write */
    long s_cost;  /* cost of writing it */
};

/*
 * Now all the global variables
 */
extern trap traps[];
extern h_list helpstr[];
extern h_list wiz_help[];
extern character_types char_class[];        /* character classes */
extern room rooms[];                        /* One for each room -- A level */
extern room *oldrp;                         /* Roomin(&oldpos) */
extern linked_list *mlist;                  /* List of monsters on the level */
extern linked_list *tlist;                  /* list of monsters fallen down traps */
extern death_type deaths[];                 /* all the ways to die */
extern thing player;                        /* The rogue */
extern monster monsters[NUMMONST + 1];      /* The initial monster states */
extern linked_list *lvl_obj;                /* List of objects on this level */
extern linked_list *monst_dead;             /* Indicates monster that got killed */
extern object *cur_weapon;                  /* Which weapon he is weilding */
extern object *cur_armor;                   /* What a well dresssed rogue wears */
extern object *cur_ring[];                  /* Which rings are being worn */
extern object *cur_misc[];                  /* which MM's are in use */
extern magic_item things[];                 /* Chances for each type of item */
extern magic_item s_magic[];                /* Names and chances for scrolls */
extern magic_item p_magic[];                /* Names and chances for potions */
extern magic_item r_magic[];                /* Names and chances for rings */
extern magic_item ws_magic[];               /* Names and chances for sticks */
extern magic_item m_magic[];                /* Names and chances for MM */
extern magic_item rel_magic[];              /* Names and chances for relics */
extern magic_item foods[];                  /* Names and chances for foods */
extern spells magic_spells[];               /* spells for magic users */
extern spells cleric_spells[];              /* spells for clerics */
extern spells druid_spells[];               /* spells for druids */
extern quill quill_scrolls[];               /* scrolls for quill */
extern const char *cnames[][17];            /* Character level names */
extern const char *abilities[NUMABILITIES]; /* Names of the various abilities */
extern char curpurch[];                     /* name of item ready to buy */
extern char PLAYER;                         /* what the player looks like */
extern int nfloors;                         /* Number of floors in this dungeon */
extern int COLS;                            /* number of columns on terminal */
extern int LINES;                           /* number of lines in terminal */
extern long char_type;                      /* what type of character is player */
extern int foodlev;                         /* how fast he eats food */
extern int level;                           /* What level rogue is on */
extern int trader;                          /* number of purchases */
extern long curprice;                       /* price of an item */
extern int purse;                           /* How much gold the rogue has */
extern int mpos;                            /* Where cursor is on top line */
extern int ntraps;                          /* Number of traps on this level */
extern int inpack;                          /* Number of things in pack */
extern int total;                           /* Total dynamic memory bytes */
extern int lastscore;                       /* Score before this turn */
extern int no_food;                         /* Number of levels without food */
extern int foods_this_level;                /* num of foods this level */
extern int seed;                            /* Random number seed */
extern int count;                           /* Number of times to repeat command */
extern int dnum;                            /* Dungeon number */
extern int max_level;                       /* Deepest player has gone */
extern int cur_max;                         /* Deepest player has gone currently */
extern int food_left;                       /* Amount of food in hero's stomach */
extern int group;                           /* Current group number */
extern int hungry_state;                    /* How hungry is he */
extern int infest_dam;                      /* Damage from parasites */
extern int lost_str;                        /* Amount of strength lost */
extern int hold_count;                      /* Number of monsters holding player */
extern int trap_tries;                      /* Number of attempts to set traps */
extern int chant_time;                      /* Number of chant points/exp level */
extern int pray_time;                       /* Number of prayer points/exp level */
extern int spell_power;                     /* Spell power left at this level */
extern int turns;                           /* Number of turns player has taken */
extern int quest_item;                      /* Item hero is looking for */
extern int cur_relic[];                     /* Current relics */
extern int take;                            /* Thing the rogue is taking */
extern char prbuf[];                        /* Buffer for sprintfs */
extern char outbuf[];                       /* Output buffer for stdout */
extern int runch;                           /* Direction player is running */
extern char *s_names[];                     /* Names of the scrolls */
extern const char *p_colors[];              /* Colors of the potions */
extern const char *r_stones[];              /* Stone settings of the rings */
extern init_weps weaps[];                   /* weapons and attributes */
extern init_armor armors[];                 /* armors and attributes */
extern const char *ws_made[];               /* What sticks are made of */
extern const char *release;                 /* Release number of rogue */
extern char whoami[];                       /* Name of player */
extern char fruit[];                        /* Favorite fruit */
extern char huh[];                          /* The last message printed */
extern char *s_guess[];                     /* Players guess at what scroll is */
extern char *p_guess[];                     /* Players guess at what potion is */
extern char *r_guess[];                     /* Players guess at what ring is */
extern char *ws_guess[];                    /* Players guess at what wand is */
extern char *m_guess[];                     /* Players guess at what MM is */
extern const char *ws_type[];               /* Is it a wand or a staff */
extern char file_name[];                    /* Save file name */
extern char score_file[];                   /* Score file name */
extern char home[];                         /* User's home directory */
extern WINDOW *cw;                          /* Window that the player sees */
extern WINDOW *hw;                          /* Used for the help command */
extern WINDOW *mw;                          /* Used to store mosnters */
extern WINDOW *msgw;                        /* Message window */
extern bool pool_teleport;                  /* just teleported from a pool */
extern bool inwhgt;                         /* true if from wghtchk() */
extern bool running;                        /* True if player is running */
extern bool playing;                        /* True until he quits */
extern bool wizard;                         /* True if allows wizard commands */
extern bool after;                          /* True if we want after daemons */
extern bool notify;                         /* True if player wants to know */
extern bool fight_flush;                    /* True if toilet input */
extern bool terse;                          /* True if we should be short */
extern bool auto_pickup;                    /* pick up things automatically? */
extern bool menu_overlay;                   /* use overlay type menu */
extern bool door_stop;                      /* Stop running when we pass a door */
extern bool jump;                           /* Show running as series of jumps */
extern bool slow_invent;                    /* Inventory one line at a time */
extern bool firstmove;                      /* First move after setting door_stop */
extern bool waswizard;                      /* Was a wizard sometime */
extern bool askme;                          /* Ask about unidentified things */
extern bool s_know[];                       /* Does he know what a scroll does */
extern bool p_know[];                       /* Does he know what a potion does */
extern bool r_know[];                       /* Does he know what a ring does */
extern bool ws_know[];                      /* Does he know what a stick does */
extern bool m_know[];                       /* Does he know what a MM does */
extern bool daytime;                        /* Indicates whether it is daytime */
extern coord oldpos;                        /* Position before last look() call */
extern coord grid[];                        /* used for random pos generation */
extern const char *nothing;                 /* "nothing happens" msg */
extern const char *spacemsg;
extern const char *morestr;
extern const char *retstr;
extern LEVTYPE levtype;
extern void (*add_abil[NUMABILITIES])(int); /* Functions to change abilities */
extern void (*res_abil[NUMABILITIES])(int); /* Functions to change abilities */
extern int cNCOLORS, cNWOOD, cNMETAL, cNSTONES;
extern const char *rainbow[], *stones[], *wood[], *metal[];
extern delayed_action d_list[MAXDAEMONS];
extern delayed_action f_list[MAXFUSES];
extern int demoncnt, fusecnt, between, chance;
extern const char version[];

// actions.h
void dsrpt_monster(thing *tp, bool always, bool see_him);
void dsrpt_player();
void m_act(thing *tp);
void m_breathe(thing *tp);
void m_select(thing *th, bool flee);
void m_sonic(thing *tp);
void m_spell(thing *tp);
void m_summon(thing *tp);
bool m_use_it(thing *tp, bool flee, room *rer, room *ree);
long runners(long segments);
bool m_use_pack(thing *monster, coord *monst_pos, coord *defend_pos, int dist, coord *shoot_dir);

// chase.h
bool can_blink(thing *tp);
coord *can_shoot(coord *er, coord *ee);
void chase(thing *tp, coord *ee, room *rer, room *ree, bool flee);
void do_chase(thing *th);
linked_list *get_hurl(thing *tp);
void runto(thing *runner, coord *spot);
bool straight_shot(int ery, int erx, int eey, int eex, coord *shooting);

// command.h
void command();
void display();
void quit(int sig);
[[noreturn]] void bugkill(int sig);
void search(bool is_thief, bool door_chime);
void help();
void identify(int ch = 0);
void d_level();
void u_level();
void nameit();
void nameitem(linked_list *item, bool mark);
void namemonst();
void count_gold();

// daemon.h
delayed_action *d_slot();
delayed_action *f_slot();
delayed_action *find_slot(enum daemon_id did);
void start_daemon_void(enum daemon_id did, void *arg, int type);
void start_daemon_int(enum daemon_id did, intptr_t arg, int type);
void kill_daemon(daemon_id did);
void do_daemons(int flag);
void fuse_int(enum daemon_id did, int arg, long time, int type);
void fuse_void(enum daemon_id did, void *arg, long time, int type);
void lengthen(enum daemon_id did, long xtime);
void extinguish(enum daemon_id did);
void do_fuses(int flag);
void activity();

// daemons.h
void doctor(thing *tp);
void swander();
void rollwand();
void trap_look();
void unconfuse();
void unsee();
void unstink();
void unclrhead();
void unphase();
void land();
void sight();
void res_strength(int howmuch);
void nohaste();
void noslow();
[[noreturn]] void suffocate();
void stomach();
void cure_disease();
void appear();
void dust_appear();
void unchoke();
void alchemy(object *obj);
void undance();
void strangle();
void fumble();
void ring_search();
void ring_teleport();
void quill_charge();
void unskill();
void cloak_charge(object *obj);
void nofire();
void nocold();
void nobolt();
void eat_gold(object *obj);
void spell_recovery();
void prayer_recovery();
void chant_recovery();

// eat.h
void eat();

// effects.h
long effect(thing *att, thing *def, object *weap, bool thrown, bool see_att, bool see_def);

// encumb.h
void updpack(int getmax, thing *tp);
int packweight(thing *tp);
long itemweight(object *wh);
long playenc(thing *tp);
long totalenc(thing *tp);
void wghtchk();
int hitweight();

// fight.h
int player_can_hit(thing *tp, object *weap);
int fight(coord *mp, object *weap, bool thrown);
int attack(thing *mp, object *weapon, bool thrown);
int swing(long cclass, long at_lvl, long op_arm, long wplus);
int roll_em(thing *att_er, thing *def_er, object *weap, bool hurl, object *cur_weapon,
            bool back_stab);
const char *prname(const char *who, bool upper);
void hit(object *weapon, bool see_att, bool see_def, const char *er, const char *ee, bool back_stab,
         bool thrown, bool short_msg);
void miss(object *weapon, bool see_att, bool see_def, const char *er, const char *ee, bool thrown,
          bool short_msg);
long dext_plus(long dexterity);
long dext_prot(long dexterity);
long str_plus(long str);
long add_dam(long str);
int hung_dam();
void thunk(object *weap, thing *tp, const char *mname);
void m_thunk(object *weap, thing *tp, const char *mname);
void bounce(object *weap, thing *tp, const char *mname);
void m_bounce(object *weap, thing *tp, const char *mname);
int is_magic(object *obj);
void killed(linked_list *item, bool pr, bool points, bool treasure);
linked_list *wield_weap(object *thrown, thing *mp);
void explode(thing *tp);
int skirmish(thing *attacker, coord *mp, object *weap, bool thrown);

// init.h
void badcheck(const char *name, magic_item *magic, int bound);
void init_colors();
void init_foods();
void init_materials();
void init_misc();
void init_names();
void init_player();
void init_stones();
void init_things();

// io.h
void msg(const char *fmt, ...);
void addmsg(const char *fmt, ...);
void endmsg();
void doadd(const char *fmt, va_list ap);
bool step_ok(int y, int x, int can_on_monst, thing *flgptr);
bool shoot_ok(int ch);
int readchar();
void status(bool display);
void wait_for(int ch);
void over_win(WINDOW *oldwin, WINDOW *newin, long maxy, int maxx, int cursory, int cursorx,
              char redraw);
void show_win(WINDOW *scr, const char *message);
void dbotline(WINDOW *scr, const char *message);
void restscr(WINDOW *scr);
unsigned long netread(int *error, int size, FILE *stream);
int netwrite(unsigned long value, int size, FILE *stream);

// list.h
void _detach(linked_list **list, linked_list *item);
void _attach(linked_list **list, linked_list *item);
void _o_free_list(linked_list **ptr);
void o_discard(linked_list *item);
void _r_free_list(linked_list **ptr);
void r_discard(linked_list *item);
void _t_free_list(linked_list **ptr);
void t_discard(linked_list *item);
void destroy_item(linked_list *item);
linked_list *new_item(size_t size);
linked_list *creat_item();
char *newalloc(size_t size);

// main.h
int main(int argc, char **argv);
[[noreturn]] void endit(int sig);
[[noreturn]] void fatal(const char *s, const int sig);
int rnd(int range);
long rnd(long range);
size_t rnd(size_t range);
long roll(long number, long sides);
void tstp(int sig);
void setup();
[[noreturn]] void playit();

// maze.h
void crankout();
void do_maze();
void draw_maze();
int findcells(int y, int x);
char *foffset(int y, int x);
bool maze_view(int y, int x);
char *moffset(int y, int x);
void rmwall(int newy, int newx, int oldy, int oldx);

// mdport.h
int md_readchar(WINDOW *win);

// misc.h
void changeclass(long newclass);
void m_use_relic(thing *monster);
void put_contents(object *bag, linked_list *item);
void take_contents(object *bag, linked_list *item);
void do_bag(linked_list *item);
void do_panic(int who);
const char *misc_name(object *obj);
void use_emori();
void use_quill(object *obj);
void use_mm(long which);
int usage_time(linked_list *item);

// monsters.h
void check_residue(thing *tp);
bool creat_mons(thing *person, long monster, bool report);
void genmonsters(int least, bool treas);
long id_monst(int monster);
void new_monster(linked_list *item, long type, coord *cp, bool max_monster);
long randmonster(bool wander, bool no_unique);
void sell(thing *tp);
linked_list *wake_monster(int y, int x);
void wanderer();

// move.h
int be_trapped(thing *th, coord *tc);
bool blue_light(bool blessed, bool cursed);
void corr_move(int dy, int dx);
void dip_it();
void do_move(int dy, int dx);
void do_run(int ch);
bool getdelta(int match, int *dy, int *dx);
bool isatrap(int ch);
void light(coord *cp);
bool lit_room(room *rp);
int movement(thing *tp);
coord *rndmove(thing *who);
void set_trap(thing *tp, int y, int x);
int show(int y, int x);
trap *trap_at(int y, int x);
int weap_move(thing *wielder, object *weap);

// new_level.h
void new_level(LEVTYPE ltype);
int rnd_room();
void put_things(LEVTYPE ltype);

// options.h
int get_abil(void *vp, WINDOW *win);
int get_quest(void *vp, WINDOW *win);
int get_ro(WINDOW *win, int oy, int ox);
int get_bool(void *vp, WINDOW *win);
int get_str(void *vp, WINDOW *win);
void option();
void parse_opts(char *str);
void put_abil(void *vp, WINDOW *win);
void put_quest(void *vp, WINDOW *win);
void put_bool(void *vp, WINDOW *win);
void put_str(void *vp, WINDOW *win);

// outside.h
void init_terrain();
void do_terrain(int basey, int basex, int deltay, int deltax, bool fresh);
int rnd_terrain();
int get_terrain(int one, int two, int three, int four);
void lake_check(coord *place);

// pack.h
bool add_pack(linked_list *item, bool silent, linked_list **packret = nullptr);
int inventory(linked_list *list, int type);
void picky_inven();
linked_list *get_item(linked_list *list, const char *purpose, int type, bool askfirst,
                      bool showcost);
int pack_char(linked_list *list, object *obj);
void cur_null(object *op);
void idenpack();
bool is_type(object *obj, int type);
void del_pack(linked_list *item);
void carry_obj(thing *mp, long chance);
int grab(int y, int x);
void make_sell_pack(thing *tp);

// passages.h
void do_passages();
void conn(int r1, int r2);
void door(room *rm, coord *cp);

// player.h
void affect();
void cast();
void chant();
long const_bonus();
void gsense();
void pray();
void steal();
int pick_spell(spells spells[], long ability, long num_spells, long power, const char *prompt,
               const char *type);

// potions.h
void add_constitution(int change);
void add_charisma(int change);
void add_dexterity(int change);
void add_haste(bool blessed);
void add_intelligence(int change);
void add_slow();
void add_strength(int change);
void add_wisdom(int change);
void quaff(long which, long kind, long flags, bool is_potion);
void res_dexterity(int howmuch);
void res_intelligence(int howmuch);
void res_wisdom(int howmuch);
void res_constitution(int howmuch);
void res_charisma(int howmuch);

// rings.h
long ring_eat(int hand);
void ring_on(linked_list *item);
const char *ring_num(object *obj);
int ring_value(int type);

/*
 * If you change this structure, change the compatibility routines
 * scoreout() and scorein() to reflect the change.
 */
#define NAMELEN 80
#define SYSLEN 9
#define LOGLEN 8
struct sc_ent {
    long sc_score;
    char sc_name[NAMELEN];
    char sc_system[SYSLEN];
    char sc_login[LOGLEN];
    long sc_flags;
    long sc_level;
    long sc_ctype;
    long sc_monster;
    long sc_quest;
};

// rip.h
[[noreturn]] void byebye(int sig);
[[noreturn]] void death(long monst);
const char *killname(long monst);
void score(long amount, int flags, long monst);
void showpack(const char *howso);
[[noreturn]] void total_winner();

// rogue.h

// rooms.h
void do_rooms();
coord *doorway(room *rp, coord *door);
void draw_room(room *rp);
void horiz(int cnt);
void rnd_pos(room *rp, coord *cp);
room *roomin(coord *cp);
void vert(int cnt);

// save.h
bool save_game();
[[noreturn]] void auto_save(int sig);
bool save_file(FILE *savef);
int restore(char *file);
size_t encwrite(const void *start, size_t size, FILE *outf);
size_t encread(void *start, size_t size, FILE *inf);

// scrolls.h
void genocide();
void read_scroll(long which, long flag, bool is_scroll);

// state.h
int rs_write_int(FILE *savef, int c);
int rs_read_int(FILE *inf, int *i);
int list_size(linked_list *l);
int rs_write_object_list(FILE *savef, linked_list *l);
int rs_read_object_list(FILE *inf, linked_list **list);
int rs_save_file(FILE *savef);
int rs_restore_file(FILE *inf);

// sticks.h
void do_zap(thing *zapper, object *obj, coord *direction, long which, long flags);
void drain(int ymin, int ymax, int xmin, int xmax);
void fix_stick(object *cur);
void m_use_wand(thing *monster);
bool need_dir(int type, long which);
int player_zap(long which, long flag);
void shoot_bolt(thing *shooter, coord start, coord dir, bool get_points, long reason,
                const char *name, long damage);

// things.h
char *charge_str(object *obj);
char *inv_name(object *obj, bool drop);
const char *weap_name(object *obj);
int drop(linked_list *item);
int dropcheck(object *op);
linked_list *new_thing(int thing_type, bool allow_curse);
linked_list *spec_item(int type, int which, long hit, long damage);
int pick_one(magic_item *magic, int nitems);
const char *blesscurse(long flags);
const char *p_kind(object *obj);
int extras();

// trader.h
void buy_it();
void do_post(bool startup);
long get_worth(object *obj);
int open_market();
bool price_it();
void sell_it();
void trans_line();
char *typ_name(object *obj);

// util.h
long ac_compute(bool ignoremetal);
void aggravate(bool do_uniques, bool do_good);
int cansee(int y, int x);
long check_level();
void chg_str(long amt);
void confus_player();
long dex_compute();
int diag_ok(coord *sp, coord *ep, thing *flgptr);
coord *fallpos(coord *pos, bool be_clear, int range);
int findmindex(const char *name);
linked_list *find_mons(int y, int x);
linked_list *find_obj(int y, int x);
coord get_coordinates();
bool get_dir(coord *direction);
int is_current(object *obj);
void look(bool wakeup, bool runend);
void lower_level(long who);
char *monster_name(thing *tp);
bool move_hero(int why);
void raise_level();
int save(int which, thing *who, long adj);
int secretdoor(int y, int x);
long str_compute();
void strucpy(char *s1, char *s2, size_t len);
const char *tr_name(int ch);
const char *vowelstr(const char *str);
void wake_room(room *rp);
void waste_time();

// vers.h

// weapons.h
void boomerang(int ydelta, int xdelta, linked_list *item, thing *tp);
void do_motion(object *obj, int ydelta, int xdelta, thing *tp);
void fall(linked_list *item, bool pr);
bool hit_monster(int y, int x, object *obj, thing *tp);
void init_weapon(object *weap, int type);
void missile(int ydelta, int xdelta, linked_list *item, thing *tp);
const char *num(long n1, long n2);
void wield();

// wear.h
void take_off();
void wear();
long dress_units(linked_list *item);

// wizard.h
void create_obj(bool prompt, int which_item, int which_type);
int getbless();
long getdeath();
long makemonster(bool showall, const char *label, const char *action);
int passwd();
int teleport();
void whatis(linked_list *what);

inline auto GOLDCALC() {
    return (rnd(50 + 10 * level) + 2);
}
/*
 * define that rogue should "nice()" itself
 */

#undef DUMP /* 1 */ /* dump core rather than catch the signal     */
