/*
 * Advanced Rogue
 * Copyright (C) 1984, 1985, 1986 Michael Morgan, Ken Dalka and AT&T
 * All rights reserved.
 *
 * Based on "Rogue: Exploring the Dungeons of Doom"
 * Copyright (C) 1980, 1981 Michael Toy, Ken Arnold and Glenn Wichman
 * All rights reserved.
 *
 * See the file LICENSE.TXT for full copyright and licensing information.
 */

/*
 * setup code
 */

#include "rogue.h"
#include <csignal>
#include <ctime>
#include <unistd.h>

#ifdef PC7300
#include <sys/window.h>
extern struct uwdata wdata, oldwin;
extern char oldtext[WTXTNUM][WTXTLEN];
#endif

int main(int argc, char **argv) {
    char *env;
    int lowtime;
    time_t now;
#ifdef PC7300
    int hardwindow; /* Do we have a hardware window? */
#endif

    /*
     * get home and options from environment
     */

    strncpy(home, md_gethomedir(), LINELEN);

    /* Get default save file */
    strcpy(file_name, home);
    strcat(file_name, "arogue77.sav");

    /* Get default score file */
    strcpy(score_file, md_getroguedir());

    if(*score_file) {
        strcat(score_file, "/");
    }

    strcat(score_file, "arogue77.scr");

    if((env = getenv("ROGUEOPTS")) != nullptr) {
        parse_opts(env);
    }

    if(whoami[0] == '\0') {
        strucpy(whoami, md_getusername(), strlen(md_getusername()));
    }

    /*
     * check for print-score option
     */
    if(argc == 2 && strcmp(argv[1], "-s") == 0) {
        waswizard = TRUE;
        score(0, SCOREIT, 0);
        exit(0);
    }

    /*
     * Check to see if he is a wizard
     */
    if(argc >= 2 && argv[1][0] == '\0') {
        if(strcmp(PASSWD, md_crypt(md_getpass("Wizard's password: "))) == 0) {
            wizard = TRUE;
            argv++;
            argc--;
        }
    }

    if(argc == 2) {
        if(!restore(argv[1])) { /* Note: restore will never return */
            exit(1);
        }
    }
    lowtime = (int)time(&now);
    dnum = (wizard && getenv("SEED") != nullptr ? atoi(getenv("SEED")) : lowtime + getpid());
    if(wizard) {
        printf("Hello %s, welcome to dungeon #%d", whoami, dnum);
    } else {
        printf("Hello %s, just a moment while I dig the dungeon...", whoami);
    }
    fflush(stdout);
    seed = dnum;
    md_srand(seed);

#ifdef PC7300
    /* Store static window parameters */
    hardwindow = ioctl(0, WIOCGETD, &wdata);
    if(hardwindow >= 0) { /* We have a hardware window */
        extern char **environ;

        /* Make sure our window is the right size */
        oldwin = wdata;
        if((wdata.uw_height / wdata.uw_vs) < 23 || (wdata.uw_width / wdata.uw_hs) < 75) {
            wdata.uw_width = 80 * wdata.uw_hs;
            wdata.uw_height = 24 * wdata.uw_vs;
            wdata.uw_x = 0;
            wdata.uw_y = wdata.uw_vs;
            wdata.uw_uflags = NBORDER;

            /* Make the change */
            if(ioctl(1, WIOCSETD, &wdata) >= 0 && environ) {
                char **eptr, *tptr, *nptr, *newenv, *lptr = 0, *cptr = 0;
                int i, nlines = -1, ncols = -1, nlindig = 0, ncoldig = 0;
                struct utdata labelbuf;

                /* Save and change window-associated text */
                for(i = 0; i < WTXTNUM; i++) {
                    labelbuf.ut_num = i;
                    ioctl(1, WIOCGETTEXT, &labelbuf);
                    strncpy(oldtext[i], labelbuf.ut_text, WTXTLEN - 1);
                    if(*labelbuf.ut_text) {
                        *labelbuf.ut_text = '\0';
                        ioctl(1, WIOCSETTEXT, &labelbuf);
                    }
                }

                labelbuf.ut_num = WTXTLABEL;
                strcpy(labelbuf.ut_text, "Advanced Rogue");
                ioctl(1, WIOCSETTEXT, &labelbuf);

                /* We have to change the TERMCAP entry */
                eptr = environ;
                while(*eptr) {
                    if(strncmp(*eptr, "TERMCAP=", 8) == 0) {
                        break;
                    } else {
                        eptr++;
                    }
                }

                /* We found a TERMCAP entry */
                if(*eptr) {
                    /* Search for li# and co# */
                    tptr = *eptr;
                    while(*tptr) {
                        switch(*tptr) {
                            case 'l':
                                if(nlines == -1 && strncmp(tptr, "li#", 3) == 0) {
                                    tptr += 3;
                                    lptr = tptr;
                                    lines = atoi(tptr);
                                    while(isdigit(*tptr)) {
                                        nlindig++;
                                        ;
                                        tptr++;
                                    }
                                } else {
                                    tptr++;
                                }
                                break;
                            case 'c':
                                if(ncols == -1 && strncmp(tptr, "co#", 3) == 0) {
                                    tptr += 3;
                                    cptr = tptr;
                                    cols = atoi(tptr);
                                    while(isdigit(*tptr)) {
                                        ncoldig++;
                                        tptr++;
                                    }
                                } else {
                                    tptr++;
                                }
                                break;
                            default:
                                tptr++;
                        }
                    }

                    /* Change the entry */
                    if(ncoldig != 2 || nlindig != 2) {
                        int length;

                        /* Add in difference in num lengths plus NULL */
                        length = strlen(*eptr) - ncoldig - nlindig + 5;

                        if(ncoldig == 0) {
                            length += 4; /* For :co# */
                        }
                        if(nlindig == 0) {
                            length += 4; /* For :li# */
                        }

                        newenv = malloc(length);
                        tptr = *eptr;
                        nptr = newenv;

                        if(nlindig == 0 || ncoldig == 0) {
                            /* Copy up to the first : */
                            while(*tptr && *tptr != ':') {
                                *nptr++ = *tptr++;
                            }

                            /* Do we have to add a field? */
                            if(nlindig == 0) {
                                strcpy(nptr, ":li#24");
                                nptr += 6;
                            }
                            if(ncoldig == 0) {
                                strcpy(nptr, ":co#80");
                                nptr += 6;
                            }
                        }
                        while(*tptr) {
                            if(tptr == lptr) {
                                strcpy(nptr, "24");
                                nptr += 2;
                                tptr += nlindig;
                            } else if(tptr == cptr) {
                                strcpy(nptr, "80");
                                nptr += 2;
                                tptr += ncoldig;
                            } else {
                                *nptr++ = *tptr++;
                            }
                        }

                        *nptr = '\0';

                        /* Replace the old one */
                        free(*eptr);
                        *eptr = newenv;
                    } else {
                        /* Just overwrite the old numbers */
                        *lptr++ = '2';
                        *lptr = '4';
                        *cptr++ = '8';
                        *cptr = '0';
                    }
                }
            }
        }
    }
#endif
    init_things();    /* Set up probabilities of things */
    init_colors();    /* Set up colors of potions */
    init_stones();    /* Set up stone settings of rings */
    init_materials(); /* Set up materials of wands */
    initscr();        /* Start up cursor package */
    init_names();     /* Set up names of scrolls */
    init_misc();      /* Set up miscellaneous magic */
    init_foods();     /* set up the food table */

    if(COLS > 85) {
        COLS = 85;
    }
    if(LINES > 24) {
        LINES = 24;
    }
    if(LINES < 23 || COLS < 75) { /* give player a break if larger font used */
        printf("\nERROR: screen size too small for rogue\n");
        byebye(0);
    }

    /*
     * Now that we have cols and lines, we can update our window
     * structure for non-hardware windows.
     */
#ifdef PC7300
    if(hardwindow < 0) {
        wdata.uw_x = 0;
        wdata.uw_y = 0;
        wdata.uw_width = COLS;
        wdata.uw_height = LINES;
        wdata.uw_uflags = 0;
        wdata.uw_hs = 1;
        wdata.uw_vs = 1;
        wdata.uw_baseline = 0;
    }
#endif
    setup();
    /*
     * Set up windows
     */
    cw = newwin(LINES, COLS, 0, 0);
    mw = newwin(LINES, COLS, 0, 0);
    hw = newwin(LINES, COLS, 0, 0);
    msgw = newwin(4, COLS, 0, 0);
    keypad(cw, TRUE);
    keypad(msgw, TRUE);

    init_player(); /* Roll up the rogue */
    waswizard = wizard;

    /* A super wizard doesn't have to get equipped */
    if(wizard && strcmp(getenv("SUPER"), "YES") == 0) {
        level = 1;
        new_level(NORMLEV);
    } else {
        new_level(STARTLEV); /* Draw current level */
    }
    /*
     * Start up daemons and fuses
     */
    start_daemon_void(d_doctor, &player, AFTER);
    fuse_int(d_swander, 0, WANDERTIME, AFTER);
    if(player.t_ctype == C_MAGICIAN || player.t_ctype == C_RANGER) {
        fuse_int(d_spell_recovery, 0, SPELLTIME, AFTER);
    }
    if(player.t_ctype == C_DRUID || player.t_ctype == C_RANGER) {
        fuse_int(d_chant_recovery, 0, SPELLTIME, AFTER);
    }
    if(player.t_ctype == C_CLERIC || player.t_ctype == C_PALADIN) {
        fuse_int(d_prayer_recovery, 0, SPELLTIME, AFTER);
    }
    start_daemon_int(d_stomach, 0, AFTER);
    if(player.t_ctype == C_THIEF || player.t_ctype == C_ASSASIN || player.t_ctype == C_MONK) {
        start_daemon_int(d_trap_look, 0, AFTER);
    }

    /* Does this character have any special knowledge? */
    switch(player.t_ctype) {
        case C_ASSASIN:
            /* Assassins automatically recognize poison */
            p_know[P_POISON] = TRUE;
    }

    /* Choose a quest item */
    quest_item = rnd(MAXRELIC);
    draw(cw);
    msg("You have been quested to retrieve the %s....", rel_magic[quest_item].mi_name);
    mpos = 0;
    playit();
}

/*
 * endit:
 *    Exit the program abnormally.
 */

[[noreturn]] void endit(int sig) {
    fatal("Ok, if you want to exit that badly, I'll have to allow it\n", sig);
}

/*
 * fatal:
 *    Exit the program, printing a message.
 */

[[noreturn]] void fatal(const char *s, const int sig) {
    clear();
    move(LINES - 2, 0);
    printw("%s", s);
    draw(stdscr);
    endwin();
#ifdef PC7300
    endhardwin();
#endif
    printf("\n"); /* So the curser doesn't stop at the end of the line */
    exit(sig);
}

/*
 * rnd:
 *    Pick a very random number.
 */
int rnd(int range) {
    return (range <= 0 ? 0 : md_rand() % range);
}

long rnd(long range) {
    return (range <= 0 ? 0 : md_rand() % range);
}

size_t rnd(size_t range) {
    return (range <= 0 ? 0 : (size_t)md_rand() % range);
}

/*
 * roll:
 *    roll a number of dice
 */

long roll(long number, long sides) {
    int dtotal = 0;

    while(number--) {
        dtotal += rnd(sides) + 1;
    }
    return dtotal;
}
#ifdef SIGTSTP
/*
 * handle stop and start signals
 */
void tstp(int sig) {
    sig = 0; // silence warning
    mvcur(0, COLS - 1, LINES - 1, 0);
    endwin();
    fflush(stdout);
    kill(0, SIGTSTP);
    signal(SIGTSTP, tstp);
    raw();
    noecho();
    keypad(cw, 1);
    keypad(msgw, 1);
    clearok(curscr, TRUE);
    touchwin(cw);
    draw(cw);
    flushinp();
}
#endif

void setup() {
    /*
    #ifndef DUMP
        signal(SIGILL, bugkill);
    #ifdef SIGTRAP
        signal(SIGTRAP, bugkill);
    #endif
    #ifdef SIGIOT
        signal(SIGIOT, bugkill);
    #endif
    #ifdef SIGEMT
        signal(SIGEMT, bugkill);
    #endif
        signal(SIGFPE, bugkill);
    #ifdef SIGBUS
        signal(SIGBUS, bugkill);
    #endif
        signal(SIGSEGV, bugkill);
    #ifdef SIGSYS
        signal(SIGSYS, bugkill);
    #endif
    #ifdef SIGPIPE
        signal(SIGPIPE, bugkill);
    #endif
    #endif
    */
#ifdef SIGTSTP
    signal(SIGTSTP, tstp);
#endif

#ifdef SIGHUP
    signal(SIGHUP, auto_save);
#endif
    signal(SIGTERM, auto_save);
    signal(SIGINT, quit);
#ifdef SIGQUIT
    signal(SIGQUIT, endit);
#endif
    raw();    /* Cbreak mode */
    noecho(); /* Echo off */
}

/*
 * playit:
 *    The main loop of the program.  Loop until the game is over,
 * refreshing things and looking at the proper times.
 */

[[noreturn]] void playit() {
    char *opts;

    /*
     * parse environment declaration of options
     */
    if((opts = getenv("ROGUEOPTS")) != nullptr) {
        parse_opts(opts);
    }

    player.t_oldpos = hero;
    oldrp = roomin(&hero);
    after = TRUE;
    command(); /* Command execution */
    endit(0);
}
