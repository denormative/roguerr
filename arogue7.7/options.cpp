/*
 * Advanced Rogue
 * Copyright (C) 1984, 1985, 1986 Michael Morgan, Ken Dalka and AT&T
 * All rights reserved.
 *
 * Based on "Rogue: Exploring the Dungeons of Doom"
 * Copyright (C) 1980, 1981 Michael Toy, Ken Arnold and Glenn Wichman
 * All rights reserved.
 *
 * See the file LICENSE.TXT for full copyright and licensing information.
 */

/*
 * This file has all the code for the option command.
 * I would rather this command were not necessary, but
 * it is the only way to keep the wolves off of my back.
 */

#include "rogue.h"

#define NUM_OPTS (sizeof optlist / sizeof(OPTION))

/*
 * description of an option and what to do with it
 */
struct optstruct {
    const char *o_name;                  /* option name */
    const char *o_prompt;                /* prompt for interactive entry */
    void *o_opt;                         /* pointer to thing to set */
    void (*o_putfunc)(void *, WINDOW *); /* function to print value */
    int (*o_getfunc)(void *, WINDOW *);  /* function to get value interactively */
};

using OPTION = optstruct;

static OPTION optlist[] = {
    {"terse", "Terse output: ", (int *)&terse, put_bool, get_bool},
    {"flush", "Flush typeahead during battle: ", (int *)&fight_flush, put_bool, get_bool},
    {"jump", "Show position only at end of run: ", (int *)&jump, put_bool, get_bool},
    {"step", "Do inventories one line at a time: ", (int *)&slow_invent, put_bool, get_bool},
    {"askme", "Ask me about unidentified things: ", (int *)&askme, put_bool, get_bool},
    {"pickup", "Pick things up automatically: ", (int *)&auto_pickup, put_bool, get_bool},
    {"overlay", "Overlay menu: ", (int *)&menu_overlay, put_bool, get_bool},
    {"name", "Name: ", (int *)whoami, put_str, get_str},
    {"file", "Save file: ", (int *)file_name, put_str, get_str},
    {"score", "Score file: ", (int *)score_file, put_str, get_str},
    {"class", "Character class: ", (int *)&char_type, put_abil, get_abil},
    {"quest", "Quest item: ", (int *)&quest_item, put_quest, get_quest}};

/*
 * The ability field is read-only
 */
int get_abil(void *vp, WINDOW *win) {
    int *abil = (int *)vp;
    int oy, ox;

    getyx(win, oy, ox);
    put_abil(abil, win);
    get_ro(win, oy, ox);

    return NORM;
}

/*
 * The quest field is read-only
 */
int get_quest(void *vp, WINDOW *win) {
    int *quest = (int *)vp;
    int oy, ox;

    getyx(win, oy, ox);
    waddstr(win, rel_magic[*quest].mi_name);
    get_ro(win, oy, ox);

    return NORM;
}

/*
 * get_ro:
 *    "Get" a read-only value.
 */

int get_ro(WINDOW *win, int oy, int ox) {
    int ny, nx;
    bool op_bad;

    op_bad = TRUE;
    getyx(win, ny, nx);
    while(op_bad) {
        wmove(win, oy, ox);
        draw(win);
        switch(wgetch(win)) {
            case '\n':
            case '\r':
                op_bad = FALSE;
                break;
            case '\033':
            case '\007':
                return QUIT;
            case '-':
                return MINUS;
            default:
                mvwaddstr(win, ny, nx + 5, "(no change allowed)");
        }
    }
    wmove(win, ny, nx + 5);
    wclrtoeol(win);
    wmove(win, ny, nx);
    waddch(win, '\n');
    return NORM;
}

/*
 * allow changing a boolean option and print it out
 */

int get_bool(void *vp, WINDOW *win) {
    bool *bp = (bool *)vp;
    int oy, ox;
    bool op_bad;

    op_bad = TRUE;
    getyx(win, oy, ox);
    waddstr(win, *bp ? "True" : "False");
    while(op_bad) {
        wmove(win, oy, ox);
        draw(win);
        switch(wgetch(win)) {
            case 't':
            case 'T':
                *bp = TRUE;
                op_bad = FALSE;
                break;
            case 'f':
            case 'F':
                *bp = FALSE;
                op_bad = FALSE;
                break;
            case '\n':
            case '\r':
                op_bad = FALSE;
                break;
            case '\033':
            case '\007':
                return QUIT;
            case '-':
                return MINUS;
            default:
                mvwaddstr(win, oy, ox + 10, "(T or F)");
        }
    }
    wmove(win, oy, ox);
    wclrtoeol(win);
    waddstr(win, *bp ? "True" : "False");
    waddch(win, '\n');
    return NORM;
}

/*
 * set a string option
 */
int get_str(void *vp, WINDOW *win) {
    char *opt = (char *)vp;
    char *sp;
    int c, oy, ox;
    char buf[LINELEN];

    draw(win);
    getyx(win, oy, ox);
    /*
     * loop reading in the string, and put it in a temporary buffer
     */
    for(sp = buf; (c = wgetch(win)) != '\n' && c != '\r' && c != '\033' && c != '\007' &&
                  sp < &buf[LINELEN - 1];
        wclrtoeol(win), draw(win)) {
        if(c == -1) {
            continue;
        }
        if(c == erasechar()) { /* process erase character */
            if(sp > buf) {
                sp--;
                for(size_t i = strlen(unctrl_int(*sp)); i; i--) {
                    waddch(win, '\b');
                }
            }
            continue;
        }
        if(c == killchar()) { /* process kill character */
            sp = buf;
            wmove(win, oy, ox);
            continue;
        }
        if(sp == buf) {
            if(c == '-' && win == hw) { /* To move back a line in hw */
                break;
            }
            if(c == '~') {
                strcpy(buf, home);
                waddstr(win, home);
                sp += strlen(home);
                continue;
            }
        }
        *sp++ = (char)c;
        waddstr(win, unctrl_int(c));
    }
    *sp = '\0';
    if(sp > buf) { /* only change option if something has been typed */
        strucpy(opt, buf, strlen(buf));
    }
    wmove(win, oy, ox);
    waddstr(win, opt);
    waddch(win, '\n');
    draw(win);
    if(win == msgw) {
        mpos += (int)(sp - buf);
    }
    if(c == '-') {
        return MINUS;
    }
    if(c == '\033' || c == '\007') {
        return QUIT;
    }
    return NORM;
}

/*
 * print and then set options from the terminal
 */
void option() {
    OPTION *op;
    int retval;

    wclear(hw);
    touchwin(hw);
    /*
     * Display current values of options
     */
    for(op = optlist; op <= &optlist[NUM_OPTS - 1]; op++) {
        waddstr(hw, op->o_prompt);
        (*op->o_putfunc)(op->o_opt, hw);
        waddch(hw, '\n');
    }
    /*
     * Set values
     */
    wmove(hw, 0, 0);
    for(op = optlist; op <= &optlist[NUM_OPTS - 1]; op++) {
        waddstr(hw, op->o_prompt);
        retval = (*op->o_getfunc)(op->o_opt, hw);
        if(retval) {
            if(retval == QUIT) {
                break;
            }
            if(op > optlist) { /* MINUS */
                wmove(hw, (int)(op - optlist) - 1, 0);
                op -= 2;
            } else { /* trying to back up beyond the top */
                putchar('\007');
                wmove(hw, 0, 0);
                op--;
            }
        }
    }
    /*
     * Switch back to original screen
     */
    hwaddstr(LINES - 1, 0, spacemsg);
    draw(hw);
    wait_for(' ');
    clearok(cw, TRUE);
    touchwin(cw);
    after = FALSE;
}

/*
 * parse options from string, usually taken from the environment.
 * the string is a series of comma seperated values, with booleans
 * being stated as "name" (true) or "noname" (false), and strings
 * being "name=....", with the string being defined up to a comma
 * or the end of the entire option string.
 */

void parse_opts(char *str) {
    char *sp;
    OPTION *op;
    int len;

    while(*str) {
        /*
         * Get option name
         */
        for(sp = str; isalpha(*sp); sp++) {
        }
        len = (int)(sp - str);
        /*
         * Look it up and deal with it
         */
        for(op = optlist; op <= &optlist[NUM_OPTS - 1]; op++) {
            if(EQSTR(str, op->o_name, len)) {
                if(op->o_putfunc == put_bool) { /* if option is a boolean */
                    *(bool *)op->o_opt = TRUE;
                } else { /* string option */
                    char *start;
                    char value[LINELEN];

                    /* Skip to start of string value */
                    for(str = sp + 1; *str == '='; str++) {
                    }
                    if(*str == '~') {
                        strcpy((char *)value, home);
                        start = (char *)value + strlen(home);
                        while(*++str == '/') {
                        }
                    } else {
                        start = (char *)value;
                    }
                    /* Skip to end of string value */
                    for(sp = str + 1; *sp && *sp != ','; sp++) {
                    }
                    strucpy(start, str, (size_t)(sp - str));

                    /* Put the value into the option field */
                    if(op->o_putfunc != put_abil) {
                        strcpy((char *)op->o_opt, value);
                    }

                    else if(*(int *)op->o_opt == -1) { /* Only init ability once */
                        size_t vlen = strlen(value);

                        if(isupper(value[0])) {
                            value[0] = (char)tolower(value[0]);
                        }
                        for(int i = 0; i < NUM_CHARTYPES - 1; i++) {
                            if(EQSTR(value, char_class[i].name, vlen)) {
                                *(int *)op->o_opt = i;
                                break;
                            }
                        }
                    }
                }
                break;
            }
            /*
             * check for "noname" for booleans
             */
            if(op->o_putfunc == put_bool && EQSTR(str, "no", 2) &&
               EQSTR(str + 2, op->o_name, len - 2)) {
                *(bool *)op->o_opt = FALSE;
                break;
            }
        }

        /*
         * skip to start of next option name
         */
        while(*sp && !isalpha(*sp)) {
            sp++;
        }
        str = sp;
    }
}

/*
 * print the character type
 */
void put_abil(void *vp, WINDOW *win) {
    const int *ability = (const int *)vp;
    waddstr(win, char_class[*ability].name);
}

/*
 * print out the quest
 */

void put_quest(void *vp, WINDOW *win) {
    const int *quest = (const int *)vp;
    waddstr(win, rel_magic[*quest].mi_name);
}

/*
 * put out a boolean
 */
void put_bool(void *vp, WINDOW *win) {
    const bool *b = (bool *)vp;
    waddstr(win, *b ? "True" : "False");
}

/*
 * put out a string
 */
void put_str(void *vp, WINDOW *win) {
    char *str = (char *)vp;
    waddstr(win, str);
}
