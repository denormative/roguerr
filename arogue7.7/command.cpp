/*
 * Advanced Rogue
 * Copyright (C) 1984, 1985, 1986 Michael Morgan, Ken Dalka and AT&T
 * All rights reserved.
 *
 * Based on "Rogue: Exploring the Dungeons of Doom"
 * Copyright (C) 1980, 1981 Michael Toy, Ken Arnold and Glenn Wichman
 * All rights reserved.
 *
 * See the file LICENSE.TXT for full copyright and licensing information.
 */

/*
 * Read and execute the user commands
 */

#include "rogue.h"

#include <csignal>
#ifdef PC7300
#include <sys/window.h>
extern struct uwdata wdata;
#endif

/*
 * command:
 *    Process the user commands
 */

void command() {
    unsigned char ch;
    linked_list *item;
    unsigned char countch = 0, direction = 0, newcount = FALSE;
    int segment = 1;
    long monst_limit, monst_current;

    monst_limit = monst_current = 1;
    while(playing) {
        /*
         * Let the daemons start up, but only do them once a round
         * (round = 10 segments).
         */
        if(segment >= 10) {
            do_daemons(BEFORE);
            do_fuses(BEFORE);
        }

        after = TRUE;
        do {
            /* One more tick of the clock. */
            if(segment >= 10 && after && (++turns % DAYLENGTH) == 0) {
                daytime ^= TRUE;
                if(levtype == OUTSIDE) {
                    if(daytime) {
                        msg("The sun rises above the horizon");
                    } else {
                        msg("The sun sinks below the horizon");
                    }
                }
                light(&hero);
            }

            /*
             * Don't bother with these updates unless the player's going
             * to do something.
             */
            if(player.t_action == A_NIL && player.t_no_move <= 1) {
                look(after, FALSE);
                lastscore = purse;
                wmove(cw, hero.y, hero.x);
                if(!((running || count) && jump)) {
                    status(FALSE);
                }
            }

            /* Draw the screen */
            if(!((running || count) && jump)) {
                wmove(cw, hero.y, hero.x);
                draw(cw);
            }

            after = TRUE;

            /*
             * Read command or continue run
             */
            if(--player.t_no_move <= 0) {
                take = 0;             /* Nothing here to start with */
                player.t_no_move = 0; /* Be sure we don't go too negative */
                if(!running) {
                    door_stop = FALSE;
                }

                /* Was the player being held? */
                if(player.t_action == A_FREEZE) {
                    player.t_action = A_NIL;
                    msg("You can move again.");
                }

                if(player.t_action != A_NIL) {
                    ch = (unsigned char)player.t_action;
                } else if(running) {
                    /* If in a corridor or maze, if we are at a turn with only one way to go, turn that way. */
                    int scratch = winat(hero.y, hero.x);
                    if((scratch == PASSAGE || scratch == DOOR || levtype == MAZELEV) &&
                       off(player, ISHUH) && off(player, ISBLIND)) {
                        int y, x;
                        if(getdelta(runch, &y, &x) == TRUE) {
                            corr_move(y, x);
                        }
                    }
                    ch = (unsigned char)runch;
                } else if(count) {
                    ch = countch;
                } else {
                    ch = (unsigned char)readchar();
                    if(mpos != 0 && !running) { /* Erase message if its there */
                        msg("");
                    }
                }

                /* check for prefixes */
                if(isdigit(ch)) {
                    count = 0;
                    newcount = TRUE;
                    while(isdigit(ch)) {
                        count = count * 10 + (ch - '0');
                        if(count > 255) {
                            count = 255;
                        }
                        ch = (unsigned char)readchar();
                    }
                    countch = ch;
                    /* turn off count for commands which don't make sense to repeat */
                    switch(ch) {
                        case 'h':
                        case 'j':
                        case 'k':
                        case 'l':
                        case 'y':
                        case 'u':
                        case 'b':
                        case 'n':
                        case 'H':
                        case 'J':
                        case 'K':
                        case 'L':
                        case 'Y':
                        case 'U':
                        case 'B':
                        case 'N':
                        case C_SEARCH:
                        case '.':
                            break;
                        default:
                            count = 0;
                    }
                }

                /* Save current direction */
                if(!running) { /* If running, it is already saved */
                    switch(ch) {
                        case 'h':
                        case 'j':
                        case 'k':
                        case 'l':
                        case 'y':
                        case 'u':
                        case 'b':
                        case 'n':
                        case 'H':
                        case 'J':
                        case 'K':
                        case 'L':
                        case 'Y':
                        case 'U':
                        case 'B':
                        case 'N':
                            runch = (char)tolower(ch);
                    }
                }

                /* Perform the action */
                switch(ch) {
                    case 'f':
                        if(!on(player, ISBLIND)) {
                            door_stop = TRUE;
                            firstmove = TRUE;
                        }
                        if(count && !newcount) {
                            ch = direction;
                        } else {
                            ch = (unsigned char)readchar();
                        }
                        switch(ch) {
                            case 'h':
                            case 'j':
                            case 'k':
                            case 'l':
                            case 'y':
                            case 'u':
                            case 'b':
                            case 'n':
                                ch = (unsigned char)toupper(ch);
                        }
                        direction = ch;
                }
                newcount = FALSE;

                /*
                 * execute a command
                 */
                if(count && !running) {
                    count--;
                }
                switch(ch) {
                    case 'h':
                        do_move(0, -1);
                        break;
                    case 'j':
                        do_move(1, 0);
                        break;
                    case 'k':
                        do_move(-1, 0);
                        break;
                    case 'l':
                        do_move(0, 1);
                        break;
                    case 'y':
                        do_move(-1, -1);
                        break;
                    case 'u':
                        do_move(-1, 1);
                        break;
                    case 'b':
                        do_move(1, -1);
                        break;
                    case 'n':
                        do_move(1, 1);
                        break;
                    case 'H':
                        do_run('h');
                        break;
                    case 'J':
                        do_run('j');
                        break;
                    case 'K':
                        do_run('k');
                        break;
                    case 'L':
                        do_run('l');
                        break;
                    case 'Y':
                        do_run('y');
                        break;
                    case 'U':
                        do_run('u');
                        break;
                    case 'B':
                        do_run('b');
                        break;
                    case 'N':
                        do_run('n');
                        break;
                    case A_PICKUP:
                        player.t_action = A_NIL;
                        if(add_pack(nullptr, FALSE)) {
                            int tch = ssinch(hero.y, hero.x);
                            if(tch != FLOOR && tch != PASSAGE) {
                                player.t_action = A_PICKUP; /*get more */
                                player.t_no_move += 2 * movement(&player);
                            }
                        }
                        break;
                    case A_ATTACK:
                        /* Is our attackee still there? */
                        if(isalpha((int)winat(player.t_newpos.y, player.t_newpos.x))) {
                            /* Our friend is still here */
                            player.t_action = A_NIL;
                            fight(&player.t_newpos, cur_weapon, FALSE);
                        } else { /* Our monster has moved */
                            player.t_action = A_NIL;
                        }
                        break;
                    case A_THROW:
                        if(player.t_action == A_NIL) {
                            item = get_item(pack, "throw", ALL, FALSE, FALSE);
                            if(item != nullptr && get_dir(&player.t_newpos)) {
                                player.t_action = A_THROW;
                                player.t_using = item;
                                player.t_no_move = 2 * movement(&player);
                            } else {
                                after = FALSE;
                            }
                        } else {
                            missile(player.t_newpos.y, player.t_newpos.x, player.t_using, &player);
                            player.t_action = A_NIL;
                            player.t_using = nullptr;
                        }
                        break;
                    case 'Q':
                        after = FALSE;
                        quit(0);
                        break;
                    case 'i':
                        after = FALSE;
                        inventory(pack, ALL);
                        break;
                    case 'I':
                        after = FALSE;
                        picky_inven();
                        break;
                    case C_DROP:
                        player.t_action = C_DROP;
                        drop(nullptr);
                        break;
                    case 'P':
                        if(levtype != POSTLEV) {
                            /* We charge 2 movement units per item */
                            player.t_no_move =
                                (2 * grab(hero.y, hero.x) * movement(&player));
                        } else {
                            /* Let's quote the wise guy a price */
                            buy_it();
                            after = FALSE;
                        }
                        break;
                    case C_COUNT:
                        count_gold();
                        break;
                    case C_QUAFF:
                        quaff(-1, 0, 0, TRUE);
                        break;
                    case C_READ:
                        read_scroll(-1, 0, TRUE);
                        break;
                    case C_EAT:
                        eat();
                        break;
                    case C_WIELD:
                        wield();
                        break;
                    case C_WEAR:
                        wear();
                        break;
                    case C_TAKEOFF:
                        take_off();
                        break;
                    case 'o':
                        option();
                        break;
                    case CTRL('N'):
                        nameit();
                        break;
                    case '=':
                        after = FALSE;
                        display();
                        break;
                    case 'm':
                        nameitem(nullptr, TRUE);
                        break;
                    case '>':
                        after = FALSE;
                        d_level();
                        break;
                    case '<':
                        after = FALSE;
                        u_level();
                        break;
                    case '?':
                        after = FALSE;
                        help();
                        break;
                    case '/':
                        after = FALSE;
                        identify(0);
                        break;
                    case C_USE:
                        use_mm(-1);
                        break;
                    case CTRL('T'):
                        if(player.t_action == A_NIL) {
                            if(get_dir(&player.t_newpos)) {
                                player.t_action = CTRL('T');
                                player.t_no_move = 2 * movement(&player);
                            } else {
                                after = FALSE;
                            }
                        } else {
                            steal();
                            player.t_action = A_NIL;
                        }
                        break;
                    case C_DIP:
                        dip_it();
                        break;
                    case 'G':
                        if(player.t_action == A_NIL) {
                            player.t_action = 'G';
                            player.t_no_move = movement(&player);
                        } else {
                            gsense();
                            player.t_action = A_NIL;
                        }
                        break;
                    case C_SETTRAP:
                        set_trap(&player, hero.y, hero.x);
                        break;
                    case C_SEARCH:
                        if(player.t_action == A_NIL) {
                            player.t_action = C_SEARCH;
                            player.t_no_move = 5 * movement(&player);
                        } else {
                            search(FALSE, FALSE);
                            player.t_action = A_NIL;
                        }
                        break;
                    case C_ZAP:
                        if(!player_zap(0, FALSE)) {
                            after = FALSE;
                        }
                        break;
                    case C_PRAY:
                        pray();
                        break;
                    case C_CHANT:
                        chant();
                        break;
                    case C_CAST:
                        cast();
                        break;
                    case 'a':
                        if(player.t_action == A_NIL) {
                            if(get_dir(&player.t_newpos)) {
                                player.t_action = 'a';
                                player.t_no_move = 2 * movement(&player);
                            } else {
                                after = FALSE;
                            }
                        } else {
                            affect();
                            player.t_action = A_NIL;
                        }
                        break;
                    case 'v':
                        after = FALSE;
                        msg("Advanced Rogue Version %s.", release);
                        break;
                    case CTRL('L'):
                        after = FALSE;
                        clearok(curscr, TRUE);
                        touchwin(cw); /* MMMMMMMMMM */
                        break;
                    case CTRL('R'):
                        after = FALSE;
                        msg(huh);
                        break;
                    case 'S':
                        after = FALSE;
                        if(save_game()) {
                            wclear(cw);
                            draw(cw);
                            endwin();
#ifdef PC7300
                            endhardwin();
#endif
                            exit(0);
                        }
                        break;
                    case '.':
                        player.t_no_move = movement(&player); /* Rest */
                        player.t_action = A_NIL;
                        break;
                    case ' ':
                        after = FALSE; /* Do Nothing */
                        break;
                    case CTRL('P'):
                        after = FALSE;
                        if(wizard) {
                            wizard = FALSE;
                            trader = 0;
                            msg("Not wizard any more");
                        } else {
                            if(waswizard || passwd()) {
                                msg("Welcome, oh mighty wizard.");
                                wizard = waswizard = TRUE;
                            } else {
                                msg("Sorry");
                            }
                        }
                        break;
                    case ESCAPE: /* Escape */
                        door_stop = FALSE;
                        count = 0;
                        after = FALSE;
                        break;
                    case '#':
                        if(levtype == POSTLEV) { /* buy something */
                            buy_it();
                        }
                        after = FALSE;
                        break;
                    case '$':
                        if(levtype == POSTLEV) { /* price something */
                            price_it();
                        }
                        after = FALSE;
                        break;
                    case '%':
                        if(levtype == POSTLEV) { /* sell something */
                            sell_it();
                        }
                        after = FALSE;
                        break;
                    default:
                        after = FALSE;
                        if(wizard) {
                            switch(ch) {
                                case 'M':
                                    create_obj(TRUE, 0, 0);
                                    break;
                                case CTRL('W'):
                                    wanderer();
                                    break;
                                case CTRL('I'):
                                    inventory(lvl_obj, ALL);
                                    break;
                                case CTRL('Z'):
                                    whatis(nullptr);
                                    break;
                                case CTRL('D'):
                                    level++;
                                    new_level(NORMLEV);
                                    break;
                                case CTRL('F'):
                                    overlay(stdscr, cw);
                                    break;
                                case CTRL('X'):
                                    overlay(mw, cw);
                                    break;
                                case CTRL('J'):
                                    teleport();
                                    break;
                                case CTRL('E'):
                                    msg("food left: %d\tfood level: %d", food_left, foodlev);
                                    break;
                                case CTRL('A'):
                                    activity();
                                    break;
                                case CTRL('C'): {
                                    int tlev;
                                    prbuf[0] = 0;
                                    msg("Which level? ");
                                    if(get_str(prbuf, msgw) == NORM) {
                                        tlev = atoi(prbuf);
                                        if(tlev < 1) {
                                            mpos = 0;
                                            msg("Illegal level.");
                                        } else if(tlev > 199) {
                                            levtype = MAZELEV;
                                            level = tlev - 200 + 1;
                                        } else if(tlev > 99) {
                                            levtype = POSTLEV;
                                            level = tlev - 100 + 1;
                                        } else {
                                            levtype = NORMLEV;
                                            level = tlev;
                                        }
                                        new_level(levtype);
                                    }
                                } break;
                                case CTRL('G'): {
                                    item = get_item(pack, "charge", STICK, FALSE, FALSE);
                                    if(item != nullptr) {
                                        (OBJPTR(item))->o_charges = 10000;
                                    }
                                } break;
                                case CTRL('H'): {
                                    int i;
                                    object *obj;

                                    for(i = 0; i < 9; i++) {
                                        raise_level();
                                    }
                                    /*
                                     * Give the rogue a sword
                                     */
                                    if(cur_weapon == nullptr || cur_weapon->o_type != RELIC) {
                                        item = spec_item(WEAPON, TWOSWORD, 5, 5);
                                        add_pack(item, TRUE, nullptr);
                                        cur_weapon = OBJPTR(item);
                                        (OBJPTR(item))->o_flags |= (ISKNOW | ISPROT);
                                    }
                                    /*
                                     * And his suit of armor
                                     */
                                    if(player.t_ctype != C_MONK) {
                                        item = spec_item(ARMOR, PLATE_ARMOR, 10, 0);
                                        obj = OBJPTR(item);
                                        if(player.t_ctype == C_THIEF) {
                                            obj->o_which = STUDDED_LEATHER;
                                        }
                                        obj->o_flags |= (ISKNOW | ISPROT);
                                        obj->o_weight = armors[PLATE_ARMOR].a_wght;
                                        cur_armor = obj;
                                        add_pack(item, TRUE, nullptr);
                                    }
                                    purse += 20000;
                                } break;
                                default:
                                    msg("Illegal command '%s'.", unctrl(ch));
                                    count = 0;
                            }
                        } else {
                            msg("Illegal command '%s'.", unctrl(ch));
                            count = 0;
                            after = FALSE;
                        }
                }

                /*
                 * If he ran into something to take, let him pick it up.
                 * unless it's a trading post
                 */
                if(auto_pickup && take != 0 && levtype != POSTLEV) {
                    /* get ready to pick it up */
                    player.t_action = A_PICKUP;
                    player.t_no_move += 2 * movement(&player);
                }
            }

            /* If he was fighting, let's stop (for now) */
            if(player.t_quiet < 0) {
                player.t_quiet = 0;
            }

            if(!running) {
                door_stop = FALSE;
            }

            if(after && segment >= 10) {
                /*
                 * Kick off the rest if the daemons and fuses
                 */

                /*
                 * If player is infested, take off a hit point
                 */
                if(on(player, HASINFEST)) {
                    if((pstats.s_hpt -= infest_dam) <= 0) {
                        death(D_INFESTATION);
                    }
                }

                /*
                 * The eye of Vecna is a constant drain on the player
                 */
                if(cur_relic[EYE_VECNA]) {
                    if((--pstats.s_hpt) <= 0) {
                        death(D_RELIC);
                    }
                }

                /*
                 * if player has body rot then take off five hits
                 */
                if(on(player, DOROT)) {
                    if((pstats.s_hpt -= 5) <= 0) {
                        death(D_ROT);
                    }
                }
                do_daemons(AFTER);
                do_fuses(AFTER);
            }
        } while(after == FALSE);

        /* Make the monsters go */
        if(--monst_current <= 0) {
            monst_current = monst_limit = runners(monst_limit);
        }

#ifdef NEEDLOOK
        /* Shall we take a look? */
        if(player.t_no_move <= 1 && !((running || count) && jump)) {
            look(FALSE, FALSE);
        }
#endif

        if(++segment > 10) {
            segment = 1;
        }
        t_free_list(monst_dead);
    }
}

/*
 * display
 *      tell the player what is at a certain coordinates assuming
 *    it can be seen.
 */
void display() {
    coord c;
    linked_list *item;
    thing *tp;
    int what;

    msg("What do you want to display (* for help)?");
    c = get_coordinates();
    mpos = 0;
    if(!cansee(c.y, c.x)) {
        msg("You can't see what is there.");
        return;
    }
    what = cwinch(c.y, c.x);
    if(isalpha(what)) {
        item = find_mons(c.y, c.x);
        tp = THINGPTR(item);
        msg("%s", monster_name(tp));
        return;
    }
    if((item = find_obj(c.y, c.x)) != nullptr) {
        msg("%s", inv_name(OBJPTR(item), FALSE));
        return;
    }
    identify((char)what);
}

/*
 * quit:
 *    Have player make certain, then exit.
 */

void quit(int sig) {
    sig = 0; // silence warning
    /*
     * Reset the signal in case we got here via an interrupt
     */
    if(signal(SIGINT, quit) != quit) {
        mpos = 0;
    }
    msg("Really quit? <yes or no> ");
    draw(cw);
    prbuf[0] = '\0';
    if((get_str(prbuf, msgw) == NORM) && strcmp(prbuf, "yes") == 0) {
        clear();
        move(LINES - 1, 0);
        draw(stdscr);
        score(pstats.s_exp + purse, CHICKEN, 0);
#ifdef PC7300
        endhardwin();
#endif
        exit(0);
    } else {
        signal(SIGINT, quit);
        wmove(cw, 0, 0);
        wclrtoeol(cw);
        status(FALSE);
        draw(cw);
        mpos = 0;
        count = 0;
        running = FALSE;
    }
}

/*
 * bugkill:
 *    killed by a program bug instead of voluntarily.
 */

[[noreturn]] void bugkill(int sig) {
    signal(sig, quit); /* If we get it again, give up */
    death(D_SIGNAL);   /* Killed by a bug */
}

/*
 * search:
 *    Player gropes about him to find hidden things.
 */

void search(bool is_thief, bool door_chime) {
    int x, y;
    int ch,  /* The trap or door character */
        sch, /* Trap or door character (as seen on screen) */
        mch; /* Monster, if a monster is on the trap or door */
    linked_list *item;
    thing *mp; /* Status on surrounding monster */

    /*
     * Look all around the hero, if there is something hidden there,
     * give him a chance to find it.  If its found, display it.
     */
    if(on(player, ISBLIND)) {
        return;
    }
    for(x = hero.x - 1; x <= hero.x + 1; x++) {
        for(y = hero.y - 1; y <= hero.y + 1; y++) {
            if(y == hero.y && x == hero.x) {
                continue;
            }

            /* Mch and ch will be the same unless there is a monster here */
            mch = winat(y, x);
            ch = ssinch(y, x);
            sch = cwinch(y, x); /* What's on the screen */

            if(door_chime == FALSE && isatrap(ch)) {
                trap *tp;

                /* Is there a monster on the trap? */
                if(mch != ch && (item = find_mons(y, x)) != nullptr) {
                    mp = THINGPTR(item);
                    if(sch == mch) {
                        sch = mp->t_oldch;
                    }
                } else {
                    mp = nullptr;
                }

                /*
                 * is this one found already?
                 */
                if(isatrap(sch)) {
                    continue; /* give him chance for other traps */
                }
                tp = trap_at(y, x);
                /*
                 * if the thief set it then don't display it.
                 * if its not a thief he has 50/50 shot
                 */
                if((tp->tr_flags & ISTHIEFSET) || (!is_thief && rnd(100) > 50)) {
                    continue; /* give him chance for other traps */
                }
                tp->tr_flags |= ISFOUND;

                /* Let's update the screen */
                if(mp != nullptr && (char)cwinch(y, x) == mch) {
                    mp->t_oldch = ch; /* Will change when monst moves */
                } else {
                    cwaddch(y, x, ch);
                }

                count = 0;
                running = FALSE;

                /* Stop what we were doing */
                player.t_no_move = movement(&player);
                player.t_action = A_NIL;
                player.t_using = nullptr;

                if(fight_flush) {
                    flushinp();
                }
                msg(tr_name(tp->tr_type));
            } else if(ch == SECRETDOOR) {
                if(door_chime == TRUE || (!is_thief && rnd(100) < 20)) {
                    room *rp;
                    coord cp;

                    /* Is there a monster on the door? */
                    if(mch != ch && (item = find_mons(y, x)) != nullptr) {
                        mp = THINGPTR(item);

                        /* Screen will change when monster moves */
                        if(sch == mch) {
                            mp->t_oldch = ch;
                        }
                    }
                    mvaddch(y, x, DOOR);
                    count = 0;
                    /*
                     * if its the entrance to a treasure room, wake it up
                     */
                    cp.y = y;
                    cp.x = x;
                    rp = roomin(&cp);
                    if(rp->r_flags & ISTREAS) {
                        wake_room(rp);
                    }

                    /* Make sure we don't shoot into the room */
                    if(door_chime == FALSE) {
                        count = 0;
                        running = FALSE;

                        /* Stop what we were doing */
                        player.t_no_move = movement(&player);
                        player.t_action = A_NIL;
                        player.t_using = nullptr;
                    }
                }
            }
        }
    }
}

/*
 * help:
 *    Give single character help, or the whole mess if he wants it
 */

void help() {
    h_list *strp = helpstr;
    h_list *wizp = wiz_help;
    char helpch;
    int cnt;

    msg("Character you want help for (* for all): ");
    helpch = (char)readchar();
    mpos = 0;
    /*
     * If its not a *, print the right help string
     * or an error if he typed a funny character.
     */
    if(helpch != '*') {
        wmove(msgw, 0, 0);
        while(strp->h_ch) {
            if(strp->h_ch == helpch) {
                msg("%s%s", unctrl_int(strp->h_ch), strp->h_desc);
                return;
            }
            strp++;
        }
        if(wizard) {
            while(wizp->h_ch) {
                if(wizp->h_ch == helpch) {
                    msg("%s%s", unctrl_int(wizp->h_ch), wizp->h_desc);
                    return;
                }
                wizp++;
            }
        }

        msg("Unknown character '%s'", unctrl_int(helpch));
        return;
    }
    /*
     * Here we print help for everything.
     * Then wait before we return to command mode
     */
    wclear(hw);
    cnt = 0;
    while(strp->h_ch) {
        hwaddstr(cnt % 23, cnt > 22 ? 40 : 0, unctrl_int(strp->h_ch));
        waddstr(hw, strp->h_desc);
        strp++;
        if(++cnt >= 46 && strp->h_ch) {
            wmove(hw, LINES - 1, 0);
            wprintw(hw, morestr);
            draw(hw);
            wait_for(' ');
            wclear(hw);
            cnt = 0;
        }
    }
    if(wizard) {
        while(wizp->h_ch) {
            hwaddstr(cnt % 23, cnt > 22 ? 40 : 0, unctrl_int(wizp->h_ch));
            waddstr(hw, wizp->h_desc);
            wizp++;
            if(++cnt >= 46 && wizp->h_ch) {
                wmove(hw, LINES - 1, 0);
                wprintw(hw, morestr);
                draw(hw);
                wait_for(' ');
                wclear(hw);
                cnt = 0;
            }
        }
    }
    wmove(hw, LINES - 1, 0);
    wprintw(hw, spacemsg);
    draw(hw);
    wait_for(' ');
    wclear(hw);
    draw(hw);
    wmove(cw, 0, 0);
    wclrtoeol(cw);
    status(FALSE);
    touchwin(cw);
}
/*
 * identify:
 *    Tell the player what a certain thing is.
 */

void identify(int ch) {
    const char *str;

    if(ch == 0) {
        msg("What do you want identified? ");
        ch = (char)readchar();
        mpos = 0;
        if(ch == ESCAPE) {
            msg("");
            return;
        }
    }
    if(isalpha(ch)) {
        str = monsters[id_monst(ch)].m_name;
    } else {
        switch(ch) {
            case '|':
            case '-':
                str = (levtype == OUTSIDE) ? "boundary of sector" : "wall of a room";
                break;
            case GOLD:
                str = "gold";
                break;
            case STAIRS:
                str = (levtype == OUTSIDE) ? "entrance to a dungeon" : "passage leading down";
                break;
            case DOOR:
                str = "door";
                break;
            case FLOOR:
                str = (levtype == OUTSIDE) ? "meadow" : "room floor";
                break;
            case VPLAYER:
                str = "the hero of the game ---> you";
                break;
            case IPLAYER:
                str = "you (but invisible)";
                break;
            case PASSAGE:
                str = "passage";
                break;
            case POST:
                str = "trading post";
                break;
            case POOL:
                str = (levtype == OUTSIDE) ? "lake" : "a shimmering pool";
                break;
            case TRAPDOOR:
                str = "trapdoor";
                break;
            case ARROWTRAP:
                str = "arrow trap";
                break;
            case SLEEPTRAP:
                str = "sleeping gas trap";
                break;
            case BEARTRAP:
                str = "bear trap";
                break;
            case TELTRAP:
                str = "teleport trap";
                break;
            case DARTTRAP:
                str = "dart trap";
                break;
            case MAZETRAP:
                str = "entrance to a maze";
                break;
            case FOREST:
                str = "forest";
                break;
            case POTION:
                str = "potion";
                break;
            case SCROLL:
                str = "scroll";
                break;
            case FOOD:
                str = "food";
                break;
            case WEAPON:
                str = "weapon";
                break;
            case ' ':
                str = "solid rock";
                break;
            case ARMOR:
                str = "armor";
                break;
            case MM:
                str = "miscellaneous magic";
                break;
            case RING:
                str = "ring";
                break;
            case STICK:
                str = "wand or staff";
                break;
            case SECRETDOOR:
                str = "secret door";
                break;
            case RELIC:
                str = "artifact";
                break;
            default:
                str = "unknown character";
        }
    }
    msg("'%s' : %s", unctrl_int(ch), str);
}

/*
 * d_level:
 *    He wants to go down a level
 */

void d_level() {
    bool no_phase = FALSE;
    int position = winat(hero.y, hero.x);

    /* If we are on a trading post, go to a trading post level. */
    if(position == POST) {
        new_level(POSTLEV);
        return;
    }

    /* If we are at a top-level trading post, we probably can't go down */
    if(levtype == POSTLEV && level == 0 && position != STAIRS) {
        msg("I see no way down.");
        return;
    }

    if(winat(hero.y, hero.x) != STAIRS) {
        if(off(player, CANINWALL) || /* Must use stairs if can't phase */
           (levtype == OUTSIDE && rnd(100) < 90)) {
            msg("I see no way down.");
            return;
        }

        /* Is there any dungeon left below? */
        if(level >= nfloors) {
            msg("There is only solid rock below.");
            return;
        }

        extinguish(d_unphase); /* Using phase to go down gets rid of it */
        no_phase = TRUE;
    }

    /* Is this the bottom? */
    if(level >= nfloors) {
        msg("The stairway only goes up.");
        return;
    }

    level++;
    new_level(NORMLEV);
    if(no_phase) {
        unphase();
    }
}

/*
 * u_level:
 *    He wants to go up a level
 */

void u_level() {
    bool no_phase = FALSE;
    linked_list *item;
    thing *tp;
    object *obj;

    if(winat(hero.y, hero.x) != STAIRS) {
        if(off(player, CANINWALL)) { /* Must use stairs if can't phase */
            msg("I see no way up.");
            return;
        }

        extinguish(d_unphase);
        no_phase = TRUE;
    }

    if(level == 0) {
        msg("The stairway only goes down.");
        return;
    }

    /*
     * does he have the item he was quested to get?
     */
    if(level == 1) {
        for(item = pack; item != nullptr; item = next(item)) {
            obj = OBJPTR(item);
            if(obj->o_type == RELIC && obj->o_which == quest_item) {
                total_winner();
            }
        }
    }
    /*
     * check to see if he trapped a UNIQUE, If he did then put it back
     * in the monster table for next time
     */
    for(item = tlist; item != nullptr; item = next(item)) {
        tp = THINGPTR(item);
        if(on(*tp, ISUNIQUE)) {
            monsters[tp->t_index].m_normal = TRUE;
        }
    }
    t_free_list(tlist); /* Monsters that fell below are long gone! */

    if(levtype != POSTLEV) {
        level--;
    }
    if(level > 0) {
        new_level(NORMLEV);
    } else {
        level = -1; /* Indicate that we are new to the outside */
        msg("You emerge into the %s", daytime ? "light" : "night");
        new_level(OUTSIDE); /* Leaving the dungeon */
    }

    if(no_phase) {
        unphase();
    }
}

/*
 * see what we want to name -- an item or a monster.
 */
void nameit() {
    char answer;

    msg("Name monster or item (m or i)? ");
    answer = (char)readchar();
    mpos = 0;

    while(answer != 'm' && answer != 'i' && answer != ESCAPE) {
        mpos = 0;
        msg("Please specify m or i, for monster or item - ");
        answer = (char)readchar();
    }

    switch(answer) {
        case 'm':
            namemonst();
            break;
        case 'i':
            nameitem(nullptr, FALSE);
    }
}

/*
 * allow a user to call a potion, scroll, or ring something
 */
void nameitem(linked_list *item, bool mark) {
    object *obj;
    char **guess = nullptr;
    const char *elsewise = nullptr;
    bool *know;

    if(item == nullptr) {
        if(mark) {
            item = get_item(pack, "mark", ALL, FALSE, FALSE);
        } else {
            item = get_item(pack, "name", CALLABLE, FALSE, FALSE);
        }
        if(item == nullptr) {
            return;
        }
    }
    /*
     * Make certain that it is somethings that we want to wear
     */
    obj = OBJPTR(item);
    switch(obj->o_type) {
        case RING:
            guess = r_guess;
            know = r_know;
            elsewise =
                (r_guess[obj->o_which] != nullptr ? r_guess[obj->o_which] : r_stones[obj->o_which]);
            break;
        case POTION:
            guess = p_guess;
            know = p_know;
            elsewise =
                (p_guess[obj->o_which] != nullptr ? p_guess[obj->o_which] : p_colors[obj->o_which]);
            break;
        case SCROLL:
            guess = s_guess;
            know = s_know;
            elsewise =
                (s_guess[obj->o_which] != nullptr ? s_guess[obj->o_which] : s_names[obj->o_which]);
            break;
        case STICK:
            guess = ws_guess;
            know = ws_know;
            elsewise = (ws_guess[obj->o_which] != nullptr ? ws_guess[obj->o_which]
                                                          : ws_made[obj->o_which]);
            break;
        case MM:
            guess = m_guess;
            know = m_know;
            elsewise = (m_guess[obj->o_which] != nullptr ? m_guess[obj->o_which] : "nothing");
            break;
        default:
            if(!mark) {
                msg("You can't call that anything.");
                return;
            } else {
                know = (bool *)nullptr;
            }
    }
    if((obj->o_flags & ISPOST) || ((know && know[obj->o_which]) && !mark)) {
        msg("That has already been identified.");
        return;
    }
    if(mark) {
        if(obj->o_mark[0]) {
            addmsg(terse ? "M" : "Was m");
            msg("arked \"%s\"", obj->o_mark);
        }
        msg(terse ? "Mark it: " : "What do you want to mark it? ");
        prbuf[0] = '\0';
    } else {
        if(elsewise) {
            addmsg(terse ? "N" : "Was n");
            msg("amed \"%s\"", elsewise);
            strcpy(prbuf, elsewise);
        } else {
            prbuf[0] = '\0';
        }
        msg(terse ? "Name it: " : "What do you want to name it? ");
    }
    if(get_str(prbuf, msgw) == NORM) {
        if(mark) {
            strncpy(obj->o_mark, prbuf, MARKLEN - 1);
            obj->o_mark[MARKLEN - 1] = '\0';
        } else if(prbuf[0] != '\0') {
            assert(guess != nullptr);
            if(guess[obj->o_which] != nullptr) {
                free(guess[obj->o_which]);
            }
            guess[obj->o_which] = newalloc(strlen(prbuf) + 1);
            strcpy(guess[obj->o_which], prbuf);
        }
    }
}

/* Name a monster */

void namemonst() {
    thing *tp;
    linked_list *item;
    coord c;

    /* Find the monster */
    msg("Choose the monster (* for help)");
    c = get_coordinates();

    /* Make sure we can see it and that it is a monster. */
    mpos = 0;
    if(!cansee(c.y, c.x)) {
        msg("You can't see what is there.");
        return;
    }

    if(isalpha(cwinch(c.y, c.x))) {
        item = find_mons(c.y, c.x);
        if(item != nullptr) {
            tp = THINGPTR(item);
            if(tp->t_name == nullptr) {
                strcpy(prbuf, monsters[tp->t_index].m_name);
            } else {
                strcpy(prbuf, tp->t_name);
            }

            addmsg(terse ? "N" : "Was n");
            msg("amed \"%s\"", prbuf);
            msg(terse ? "Name it: " : "What do you want to name it? ");

            if(get_str(prbuf, msgw) == NORM) {
                if(prbuf[0] != '\0') {
                    if(tp->t_name != nullptr) {
                        free(tp->t_name);
                    }
                    tp->t_name = newalloc(strlen(prbuf) + 1);
                    strcpy(tp->t_name, prbuf);
                }
            }
            return;
        }
    }

    msg("There is no monster there to name.");
}

void count_gold() {
    if(player.t_action != C_COUNT) {
        msg("You take a break to count your money...");
        player.t_using = nullptr;
        player.t_action = C_COUNT; /* We are counting */
        player.t_no_move = ((purse / 300 + 1) * movement(&player));
        return;
    }
    if(purse > 100) {
        msg("You think you have about %d gold pieces.", purse);
    } else {
        msg("You have %d gold pieces.", purse);
    }
    player.t_action = A_NIL;
}
