/*
 * Advanced Rogue
 * Copyright (C) 1984, 1985, 1986 Michael Morgan, Ken Dalka and AT&T
 * All rights reserved.
 *
 * Based on "Rogue: Exploring the Dungeons of Doom"
 * Copyright (C) 1980, 1981 Michael Toy, Ken Arnold and Glenn Wichman
 * All rights reserved.
 *
 * See the file LICENSE.TXT for full copyright and licensing information.
 */

/*
 * Routines to deal with the pack
 */

#include "rogue.h"

#ifdef PC7300
#include "menu.h"
#endif

/*
 * add_pack:
 *    Pick up an object and add it to the pack.  If the argument is non-null
 * use it as the linked_list pointer instead of gettting it off the ground.
 */
bool add_pack(linked_list *item, bool silent, linked_list **packret) {
    linked_list *ip, *lp = nullptr, *ap;
    object *obj, *op = nullptr;
    bool exact, from_floor;

    if(packret != nullptr) {
        *packret = nullptr;
    }

    if(item == nullptr) {
        from_floor = TRUE;
        if((item = find_obj(hero.y, hero.x)) == nullptr) {
            return (FALSE);
        }
    } else {
        from_floor = FALSE;
    }
    obj = OBJPTR(item);
    /*
     * If it is gold, just add its value to rogue's purse and get rid
     * of it.
     */
    if(obj->o_type == GOLD) {
        linked_list *mitem;
        thing *tp;

        if(!silent) {
            if(!terse) {
                addmsg("You found ");
            }
            msg("%d gold pieces.", obj->o_count);
        }

        /* First make sure no greedy monster is after this gold.
         * If so, make the monster run after the rogue instead.
         */
        for(mitem = mlist; mitem != nullptr; mitem = next(mitem)) {
            tp = THINGPTR(mitem);
            if(tp->t_dest == &obj->o_pos) {
                tp->t_dest = &hero;
            }
        }

        purse += obj->o_count;
        if(from_floor) {
            detach(lvl_obj, item);
            if((ap = find_obj(hero.y, hero.x)) == nullptr) {
                mvaddch(hero.y, hero.x, (roomin(&hero) == nullptr ? PASSAGE : FLOOR));
            } else {
                mvaddch_int(hero.y, hero.x, (OBJPTR(ap))->o_type);
            }
        }
        o_discard(item);
        return (TRUE);
    }

    /*
     * see if he can carry any more weight
     */
    if(itemweight(obj) + pstats.s_pack > pstats.s_carry) {
        msg("Too much for you to carry.");
        return FALSE;
    }
    /*
     * Link it into the pack.  Search the pack for a object of similar type
     * if there isn't one, stuff it at the beginning, if there is, look for one
     * that is exactly the same and just increment the count if there is.
     * it  that.  Food is always put at the beginning for ease of access, but
     * is not ordered so that you can't tell good food from bad.  First check
     * to see if there is something in thr same group and if there is then
     * increment the count.
     */
    if(obj->o_group) {
        for(ip = pack; ip != nullptr; ip = next(ip)) {
            op = OBJPTR(ip);
            if(op->o_group == obj->o_group) {
                /*
                 * Put it in the pack and notify the user
                 */
                op->o_count += obj->o_count;
                if(from_floor) {
                    detach(lvl_obj, item);
                    if((ap = find_obj(hero.y, hero.x)) == nullptr) {
                        mvaddch(hero.y, hero.x, (roomin(&hero) == nullptr ? PASSAGE : FLOOR));
                    } else {
                        mvaddch_int(hero.y, hero.x, (OBJPTR(ap))->o_type);
                    }
                }
                o_discard(item);
                item = ip;
                goto picked_up;
            }
        }
    }

    /*
     * Check for and deal with scare monster scrolls
     */
    if(obj->o_type == SCROLL && obj->o_which == S_SCARE) {
        if(obj->o_flags & ISCURSED) {
            msg("The scroll turns to dust as you pick it up.");
            detach(lvl_obj, item);
            if((ap = find_obj(hero.y, hero.x)) == nullptr) {
                mvaddch(hero.y, hero.x, (roomin(&hero) == nullptr ? PASSAGE : FLOOR));
            } else {
                mvaddch_int(hero.y, hero.x, (OBJPTR(ap))->o_type);
            }
            return (TRUE);
        }
    }

    /*
     * Search for an object of the same type
     */
    exact = FALSE;
    for(ip = pack; ip != nullptr; ip = next(ip)) {
        op = OBJPTR(ip);
        if(obj->o_type == op->o_type) {
            break;
        }
    }
    if(ip == nullptr) {
        /*
         * Put it at the end of the pack since it is a new type
         */
        for(ip = pack; ip != nullptr; ip = next(ip)) {
            op = OBJPTR(ip);
            if(op->o_type != FOOD) {
                break;
            }
            lp = ip;
        }
    } else {
        /*
         * Search for an object which is exactly the same
         */
        assert(op != nullptr);
        while(ip != nullptr && op->o_type == obj->o_type) {
            if(op->o_which == obj->o_which) {
                exact = TRUE;
                break;
            }
            lp = ip;
            if((ip = next(ip)) == nullptr) {
                break;
            }
            op = OBJPTR(ip);
        }
    }
    /*
     * Check if there is room
     */
    if(ip == nullptr || !exact || !ISMULT(obj->o_type)) {
        if(inpack == MAXPACK - 1) {
            msg(terse ? "No room." : "You can't carry anything else.");
            return (FALSE);
        }
    }
    inpack++;
    if(from_floor) {
        detach(lvl_obj, item);
        if((ap = find_obj(hero.y, hero.x)) == nullptr) {
            mvaddch(hero.y, hero.x, (roomin(&hero) == nullptr ? PASSAGE : FLOOR));
        } else {
            mvaddch_int(hero.y, hero.x, (OBJPTR(ap))->o_type);
        }
    }
    if(ip == nullptr) {
        /*
         * Didn't find an exact match, just stick it here
         */
        if(pack == nullptr) {
            pack = item;
        } else {
            assert(lp != nullptr);
            lp->l_next = item;
            item->l_prev = lp;
            item->l_next = nullptr;
        }
    } else {
        /*
         * If we found an exact match.  If it is food,
         * increase the count, otherwise put it with its clones.
         */
        if(exact && ISMULT(obj->o_type)) {
            op->o_count += obj->o_count;
            inpack--; /* adjust for previous addition */
            o_discard(item);
            item = ip;
            goto picked_up;
        }
        if((item->l_prev = prev(ip)) != nullptr) {
            item->l_prev->l_next = item;
        } else {
            pack = item;
        }
        item->l_next = ip;
        ip->l_prev = item;
    }
picked_up:
    /*
     * Notify the user
     */
    obj = OBJPTR(item);
    if(!silent) {
        if(!terse) {
            addmsg("You now have ");
        }
        msg("%s (%c)", inv_name(obj, !terse), pack_char(pack, obj));
    }

    /* Relics can do strange things when you pick them up */
    if(obj->o_type == RELIC) {
        switch(obj->o_which) {
            /* the ankh of Heil gives you prayers */
            case HEIL_ANKH:
                msg("The ankh welds itself into your hand.");
                if(player.t_ctype != C_CLERIC && player.t_ctype != C_PALADIN) {
                    fuse_int(d_prayer_recovery, 0, SPELLTIME, AFTER);
                }

                /* A cloak must be worn. */
                break;
            case EMORI_CLOAK:
                if(cur_armor != nullptr || cur_misc[WEAR_CLOAK]) {
                    msg("The cloak insists you remove your current garments.");
                    if(!dropcheck(cur_armor != nullptr ? cur_armor : cur_misc[WEAR_CLOAK])) {
                        pstats.s_hpt = -1;
                        msg("The cloak constricts around you.");
                        msg("It draws your life force from you!!! -- More --");
                        wait_for(' ');
                        death(D_RELIC);
                    }
                }
                if(obj->o_charges < 0) { /* should never happen, but.... */
                    obj->o_charges = 0;
                }
                if(obj->o_charges == 0) {
                    fuse_void(d_cloak_charge, obj, CLOAK_TIME, AFTER);
                }

                /* The amulet must be worn. */
                break;
            case STONEBONES_AMULET:
            case YENDOR_AMULET:
                if(cur_misc[WEAR_JEWEL] || cur_relic[STONEBONES_AMULET] ||
                   cur_relic[YENDOR_AMULET]) {
                    msg("You have an urge to remove your current amulet.");
                }
                if((cur_misc[WEAR_JEWEL] && !dropcheck(cur_misc[WEAR_JEWEL])) ||
                   cur_relic[STONEBONES_AMULET] || cur_relic[YENDOR_AMULET]) {
                    pstats.s_hpt = -1;
                    msg("The %s begins pulsing.", inv_name(obj, TRUE));
                    msg("It fades away.... -- More --");
                    wait_for(' ');
                    death(D_RELIC);
                }
                msg("The %s welds itself into your chest.", inv_name(obj, TRUE));

                /* The eye must be inserted in eye socket */
                break;
            case EYE_VECNA:
                msg("The eye forces you to jam it into your eye socket!");
                pstats.s_hpt -= 75;
                if(pstats.s_hpt < 0) {
                    msg("The pain is too much for you! -- More --");
                    wait_for(' ');
                    death(D_RELIC);
                }
                waste_time();
                msg("The excrutiating pain slowly turns into a dull throb.");

                break;
            case QUILL_NAGROM:
                fuse_int(d_quill_charge, 0, player.t_ctype == C_MAGICIAN ? 4 : 8, AFTER);

                /* Weapons will insist on being wielded. */
                break;
            case MUSTY_DAGGER:
            case HRUGGEK_MSTAR:
            case YEENOGHU_FLAIL:
            case AXE_AKLAD:
                /* For the daggers start a fuse to change player to a thief. */
                /* and set a daemon to eat gold.                    */
                if(obj->o_which == MUSTY_DAGGER) {
                    fuse_int(d_changeclass, C_THIEF, roll(20, 20), AFTER);
                    if(purse > 0) {
                        msg("Your purse feels lighter");
                    } else {
                        purse = 1; /* fudge to get right msg from eat_gold() */
                    }
                    eat_gold(obj);
                    start_daemon_void(d_eat_gold, obj, AFTER);
                }
                /* For the axe start a fuse to change player to a fighter. */
                if(obj->o_which == AXE_AKLAD) {
                    fuse_int(d_changeclass, C_FIGHTER, roll(20, 20), AFTER);
                }
                if(cur_weapon != nullptr) {
                    msg("The artifact insists you release your current weapon.");
                    if(!dropcheck(cur_weapon)) {
                        pstats.s_hpt = -1;
                        msg("The artifact forces your weapon into your heart.");
                        msg("It hums with satisfaction. -- More --");
                        wait_for(' ');
                        death(D_RELIC);
                    }
                }
                cur_weapon = obj;

                break;
            case SURTUR_RING:
                msg("The ring forces itself through your nose!");
                pstats.s_hpt -= 22;
                if(pstats.s_hpt < 0) {
                    msg("The pain is too much for you! -- More --");
                    wait_for(' ');
                    death(D_RELIC);
                }
                waste_time();
                turn_on(player, NOFIRE);
                msg("The pain slowly subsides.");
                break;
            default:
                break;
        }
        cur_relic[obj->o_which]++; /* Note that we have it */
    }

    updpack(FALSE, &player);
    if(packret != nullptr) {
        *packret = item;
    }
    return (TRUE);
}

#ifdef PC7300
static menu_t Display;                           /* The menu structure */
static mitem_t Dispitems[MAXPACK + 1];           /* Info for each line */
static char Displines[MAXPACK + 1][LINELEN + 1]; /* The lines themselves */
#endif

/*
 * inventory:
 *    list what is in the pack
 */
int inventory(linked_list *list, int type) {
    object *obj;
    char ch;
    int n_objs, cnt, maxx = 0, curx;
    char inv_temp[2 * LINELEN + 1];

    cnt = 0;
    n_objs = 0;
    for(ch = 'a'; list != nullptr; ch++, list = next(list)) {
        obj = OBJPTR(list);
        if(!is_type(obj, type)) {
            continue;
        }
        switch(n_objs++) {
            /*
             * For the first thing in the inventory, just save the string
             * in case there is only one.
             */
            case 0:
                sprintf(inv_temp, "%c) %s", ch, inv_name(obj, FALSE));
                break;
            /*
             * If there is more than one, clear the screen, print the
             * saved message and fall through to ...
             */
            case 1:
                if(slow_invent) {
                    msg(inv_temp);
                } else {
                    wclear(hw);
                    waddstr(hw, inv_temp);
                    waddch(hw, '\n');

                    maxx = (int)strlen(inv_temp); /* Length of the listing */

#ifdef PC7300
                    /* Put it into the PC menu display */
                    strcpy(Displines[0], inv_temp);
                    Dispitems[0].mi_name = Displines[0];
                    Dispitems[0].mi_flags = 0;
                    Dispitems[0].mi_val = 0;
#endif
                }
                /*
                 * Print the line for this object
                 */
                [[clang::fallthrough]];
            default:
                if(ch > 'z') {
                    ch = 'A';
                }
                if(slow_invent) {
                    msg("%c) %s", ch, inv_name(obj, FALSE));
                } else {
                    if(++cnt >= LINES - 2) { /* if bottom of screen */
                        dbotline(hw, morestr);
                        cnt = 0;
                        wclear(hw);
                    }
                    sprintf(inv_temp, "%c) %s\n", ch, inv_name(obj, FALSE));
                    curx = (int)strlen(inv_temp) - 1; /* Don't count new-line */
                    if(curx > maxx) {
                        maxx = curx;
                    }
                    waddstr(hw, inv_temp);
#ifdef PC7300
                    /* Put it into the PC menu display */
                    strcpy(Displines[n_objs - 1], inv_temp);
                    Displines[n_objs - 1][curx] = '\0'; /* Strip newline */
                    Dispitems[n_objs - 1].mi_name = Displines[n_objs - 1];
                    Dispitems[n_objs - 1].mi_flags = 0;
                    Dispitems[n_objs - 1].mi_val = 0;
#endif
                }
        }
    }
    if(n_objs == 0) {
        if(terse) {
            msg(type == ALL ? "Empty handed." : "Nothing appropriate");
        } else {
            msg(type == ALL ? "You are empty handed." : "You don't have anything appropriate");
        }
        return FALSE;
    }
    if(n_objs == 1) {
        msg(inv_temp);
        return TRUE;
    }
    if(!slow_invent) {
#ifdef PC7300
        /* Place an end marker for the items */
        Dispitems[n_objs].mi_name = 0;

        /* Set up the main menu structure */
        Display.m_label = "Inventory";
        Display.m_title = "Pack Contents";
        Display.m_prompt = "Press Cancl to continue.";
        Display.m_curptr = '\0';
        Display.m_markptr = '\0';
        Display.m_flags = 0;
        Display.m_selcnt = 0;
        Display.m_items = Dispitems;
        Display.m_curi = 0;

        /*
         * Try to display the menu.  If we don't have a local terminal,
         * the call will fail and we will just continue with the
         * normal mode.
         */
        if(menu(&Display) >= 0) {
            return TRUE;
        }
#endif
        waddstr(hw, spacemsg);
        curx = (int)strlen(spacemsg);
        if(curx > maxx) {
            maxx = curx;
        }

        /*
         * If we have fewer than half a screenful, don't clear the screen.
         * Leave an extra blank line at the bottom and 3 blank columns
         * to he right.
         */
        if(menu_overlay && n_objs < LINES / 2 + 2) {
            over_win(cw, hw, n_objs + 2, maxx + 3, n_objs, curx, ' ');
            return TRUE;
        }

        draw(hw);
        wait_for(' ');
        clearok(cw, TRUE);
        touchwin(cw);
    }
    return TRUE;
}

/*
 * picky_inven:
 *    Allow player to inventory a single item
 */
void picky_inven() {
    linked_list *item;
    char ch, mch;

    if(pack == nullptr) {
        msg("You aren't carrying anything");
    } else if(next(pack) == nullptr) {
        msg("a) %s", inv_name(OBJPTR(pack), FALSE));
    } else {
        msg(terse ? "Item: " : "Which item do you wish to inventory: ");
        mpos = 0;
        if((mch = (char)readchar()) == ESCAPE) {
            msg("");
            return;
        }

        /* Check for a special character */
        switch(mch) {
            case FOOD:
            case SCROLL:
            case POTION:
            case RING:
            case STICK:
            case RELIC:
            case ARMOR:
            case WEAPON:
            case MM:
                msg("");
                if(get_item(pack, nullptr, mch, FALSE, FALSE) == nullptr) {
                    if(terse) {
                        msg("None in pack.");
                    } else {
                        msg("You have no %c in your pack.", mch);
                    }
                }
                return;
        }

        for(ch = 'a', item = pack; item != nullptr; item = next(item), ch++) {
            if(ch == mch) {
                msg("%c) %s", ch, inv_name(OBJPTR(item), FALSE));
                return;
            }
        }
        if(!terse) {
            msg("'%s' not in pack.", unctrl_int(mch));
        }
        msg("Range is 'a' to '%c'", --ch);
    }
}

/*
 * get_item:
 *    pick something out of a pack for a purpose
 */
linked_list *get_item(linked_list *list, const char *purpose, int type, bool askfirst,
                      bool showcost) {
    linked_list *item;
    object *obj;
    int cnt, pagecnt, ch, och, maxx, curx, confused;
    linked_list *saveitem = nullptr;
    char description[2 * LINELEN + 1];
    char cost[LINELEN / 2];
#ifdef PC7300
    int menucount = 0;
    int usemenu = 1;
#endif

    /*
     * If this is the player's pack and the player is confused, we
     * might just take anything.
     */
    if(list == player.t_pack && on(player, ISHUH) && rnd(100) < 75) {
        confused = 1;
    } else {
        confused = 0;
    }

    cnt = 0;
    if(list == nullptr) {
        msg("You aren't carrying anything.");
        return nullptr;
    }
    /* see if we have any of the type requested */
    for(ch = 'a', item = list; item != nullptr; item = next(item), ch++) {
        obj = OBJPTR(item);
        if(is_type(obj, type)) {
            cnt++;
            saveitem = item;
        }
    }
    if(cnt == 0) {
        if(purpose) {
            msg("Nothing to %s", purpose);
        }
        after = FALSE;
        return nullptr;
    }
    if(cnt == 1) { /* only found one of 'em */
        assert(saveitem != nullptr);
        obj = OBJPTR(saveitem);
        while(TRUE) {
            if(purpose) { /* Should we prompt the player? */
                msg("%s what (* for the item)?", purpose);
                ch = tolower(readchar());
            } else {
                ch = pack_char(list, obj);
                msg("%c) %s", ch, inv_name(obj, FALSE));
            }

            if(ch == '*') {
                mpos = 0;
                msg("%c) %s", pack_char(list, obj), inv_name(obj, FALSE));
                continue;
            }
            if(ch == ESCAPE) {
                msg("");
                after = FALSE;
                return nullptr;
            }
            for(item = list, och = 'a'; item != nullptr; item = next(item), och++) {
                if(ch == och) {
                    break;
                }
                if(och == 'z') {
                    och = 'A' - 1;
                }
            }
            if(item == nullptr) {
                msg("Please specify a letter between 'a' and '%c'", och == 'A' ? 'z' : och - 1);
                continue;
            }
            if(is_type(OBJPTR(item), type)) {
                if(purpose) {
                    mpos = 0;
                }
                return item;
            }
            msg("You can't %s that!", purpose);
        }
    }
    while(TRUE) {
        if(!askfirst && purpose) {
            msg("%s what? (* for list): ", purpose);
            ch = readchar();
        } else {
            ch = '*';
        }

        mpos = 0;
        if(ch == ESCAPE) { /* abort if escape hit */
            after = FALSE;
            msg(""); /* clear display */
            return nullptr;
        }

        if(ch == '*') {
            wclear(hw);
            pagecnt = 0;
            maxx = 0;
            for(item = list, ch = 'a'; item != nullptr; item = next(item), ch++) {
                obj = OBJPTR(item);
                if(!is_type(OBJPTR(item), type)) {
                    continue;
                }
                cost[0] = '\0';
                if(showcost) {
                    sprintf(description, "[%ld] ", get_worth(obj));
                    sprintf(cost, "%8.8s", description);
                }
                sprintf(description, "%c) %s%s\n\r", ch, cost, inv_name(obj, FALSE));
                waddstr(hw, description);
                curx = (int)strlen(description) - 2; /* Don't count \n or \r */
                if(maxx < curx) {
                    maxx = curx;
                }
#ifdef PC7300
                if(usemenu) {
                    /* Put it into the PC menu display */
                    strcpy(Displines[menucount], description);
                    Displines[menucount][curx] = '\0'; /* Strip newline */
                    Dispitems[menucount].mi_name = Displines[menucount];
                    Dispitems[menucount].mi_flags = 0;
                    Dispitems[menucount++].mi_val = (int)item;
                }
#endif
                if(++pagecnt >= LINES - 2 && next(item) != nullptr) {
                    pagecnt = 0;
                    dbotline(hw, spacemsg);
                    wclear(hw);
                }
                if(ch == 'z') {
                    ch = 'A' - 1;
                }
            }

            /* Put in the prompt */
            if(purpose) {
                sprintf(description, "%s what? ", purpose);
            } else {
                strcpy(description, spacemsg);
            }
            waddstr(hw, description);
            curx = (int)strlen(description);
            if(maxx < curx) {
                maxx = curx;
            }

#ifdef PC7300
            if(usemenu) {
                /* Place an end marker for the items */
                Dispitems[menucount].mi_name = 0;

                /* Set up the main menu structure */
                Display.m_label = "Sub-Inventory";
                if(purpose) {
                    Display.m_title = description;
                    Display.m_prompt = "Select an item or press Cancl for no selection.";
                    Display.m_selcnt = 1;
                } else {
                    Display.m_title = 0;
                    Display.m_prompt = "Press Cancl to continue.";
                    Display.m_selcnt = 0;
                }
                Display.m_curptr = '\0';
                Display.m_markptr = '\0';
                Display.m_flags = 0;
                Display.m_items = Dispitems;
                Display.m_curi = 0;

                /*
                 * Try to display the menu.  If we don't have a local terminal,
                 * the call will fail and we will just continue with the
                 * normal mode.
                 */
                if(menu(&Display) >= 0) {
                    msg("");
                    if(Display.m_selcnt == 0) {
                        /* Menu was cancelled */
                        if(purpose) {
                            after = FALSE;
                            return NULL; /* all done if abort */
                        } else {
                            return saveitem;
                        }
                    } else {
                        return (linked_list *)Display.m_curi->mi_val;
                    }
                } else {
                    usemenu = 0; /* Can't use the menu facilities */
                }
            }
#endif
            /* Write the screen */
            if((menu_overlay && cnt < LINES / 2 + 2) || cnt == 1) {
                over_win(cw, hw, cnt + 2, maxx + 3, cnt, curx, 0);
                cnt = -1; /* Indicate we used over_win */
            } else {
                draw(hw);
            }

            if(purpose) {
                do {
                    ch = tolower(readchar());
                } while(!(isalpha(ch) || ch == ESCAPE));
            } else {
                ch = pack_char(list, OBJPTR(saveitem)); /* Pick a valid item */
                wait_for(' ');
            }

            /* Redraw original screen */
            if(cnt < 0) {
                clearok(cw, FALSE); /* Setup to redraw current screen */
                touchwin(cw);       /* clearing first */
            } else {
                restscr(cw);
            }

            if(ch == ESCAPE) {
                after = FALSE;
                msg("");        /* clear top line */
                return nullptr; /* all done if abort */
            }
            /* ch has item to get from list */
        }

        for(item = list, och = 'a'; item != nullptr; item = next(item), och++) {
            if(confused) {
                /*
                 * Confused is incremented each time so that if the rnd(cnt)
                 * clause keeps failing, confused will equal cnt for the
                 * last item of the correct type and rnd(cnt) < cnt will
                 * have to be true.
                 */
                if(is_type(OBJPTR(item), type) && rnd(cnt) < confused++) {
                    break;
                }
            } else if(ch == och) {
                break;
            }
            if(och == 'z') {
                och = 'A' - 1;
            }
        }

        if(item == nullptr) {
            msg("Please specify a letter between 'a' and '%c'", och == 'A' ? 'z' : och - 1);
            continue;
        }

        if(is_type(OBJPTR(item), type)) {
            return (item);
        }
        msg("You can't %s that!", purpose);
    }
}

int pack_char(linked_list *list, object *obj) {
    linked_list *item;
    char c;

    c = 'a';
    for(item = list; item != nullptr; item = next(item)) {
        if(OBJPTR(item) == obj) {
            return c;
        }
        if(c == 'z') {
            c = 'A';
        } else {
            c++;
        }
    }
    return 'z';
}

/*
 * cur_null:
 *    This updates cur_weapon etc for dropping things
 */
void cur_null(object *op) {
    if(op == cur_weapon) {
        cur_weapon = nullptr;
    } else if(op == cur_armor) {
        cur_armor = nullptr;
    } else if(op == cur_ring[LEFT_1]) {
        cur_ring[LEFT_1] = nullptr;
    } else if(op == cur_ring[LEFT_2]) {
        cur_ring[LEFT_2] = nullptr;
    } else if(op == cur_ring[LEFT_3]) {
        cur_ring[LEFT_3] = nullptr;
    } else if(op == cur_ring[LEFT_4]) {
        cur_ring[LEFT_4] = nullptr;
    } else if(op == cur_ring[RIGHT_1]) {
        cur_ring[RIGHT_1] = nullptr;
    } else if(op == cur_ring[RIGHT_2]) {
        cur_ring[RIGHT_2] = nullptr;
    } else if(op == cur_ring[RIGHT_3]) {
        cur_ring[RIGHT_3] = nullptr;
    } else if(op == cur_ring[RIGHT_4]) {
        cur_ring[RIGHT_4] = nullptr;
    } else if(op == cur_misc[WEAR_BOOTS]) {
        cur_misc[WEAR_BOOTS] = nullptr;
    } else if(op == cur_misc[WEAR_JEWEL]) {
        cur_misc[WEAR_JEWEL] = nullptr;
    } else if(op == cur_misc[WEAR_GAUNTLET]) {
        cur_misc[WEAR_GAUNTLET] = nullptr;
    } else if(op == cur_misc[WEAR_CLOAK]) {
        cur_misc[WEAR_CLOAK] = nullptr;
    } else if(op == cur_misc[WEAR_BRACERS]) {
        cur_misc[WEAR_BRACERS] = nullptr;
    } else if(op == cur_misc[WEAR_NECKLACE]) {
        cur_misc[WEAR_NECKLACE] = nullptr;
    }
}

/*
 * idenpack:
 *    Identify all the items in the pack
 */
void idenpack() {
    linked_list *pc;

    for(pc = pack; pc != nullptr; pc = next(pc)) {
        whatis(pc);
    }
}

bool is_type(object *obj, int type) {
    bool current;

    if(type == obj->o_type) {
        return (TRUE);
    }

    switch(type) {
        case ALL:
            return (TRUE);
        case READABLE:
            if(obj->o_type == SCROLL || (obj->o_type == MM && obj->o_which == MM_SKILLS)) {
                return (TRUE);
            }
            break;
        case QUAFFABLE:
            if(obj->o_type == POTION || (obj->o_type == MM && obj->o_which == MM_JUG)) {
                return (TRUE);
            }
            break;
        case ZAPPABLE:
            if(obj->o_type == STICK) {
                return (TRUE);
            }
            if(obj->o_type == RELIC) {
                switch(obj->o_which) {
                    case MING_STAFF:
                    case ASMO_ROD:
                    case ORCUS_WAND:
                    case EMORI_CLOAK:
                        return (TRUE);
                }
            }
            break;
        case WEARABLE:
        case REMOVABLE:
            current = is_current(obj);

            /*
             * Don't wear thing we are already wearing or remove things
             * we aren't wearing.
             */
            if(type == WEARABLE && current) {
                return (FALSE);
            }
            if(type == REMOVABLE && !current) {
                return (FALSE);
            }

            switch(obj->o_type) {
                case RELIC:
                    switch(obj->o_which) {
                        case HEIL_ANKH:
                        case EMORI_CLOAK:
                            return (TRUE);
                    }
                    break;
                case MM:
                    switch(obj->o_which) {
                        case MM_ELF_BOOTS:
                        case MM_DANCE:
                        case MM_BRACERS:
                        case MM_DISP:
                        case MM_PROTECT:
                        case MM_G_DEXTERITY:
                        case MM_G_OGRE:
                        case MM_JEWEL:
                        case MM_R_POWERLESS:
                        case MM_FUMBLE:
                        case MM_STRANGLE:
                        case MM_ADAPTION:
                            return (TRUE);
                    }
                    break;
                case ARMOR:
                case RING:
                    return (TRUE);
            }
            break;
        case CALLABLE:
            switch(obj->o_type) {
                case RING:
                    if(!r_know[obj->o_which]) {
                        return (TRUE);
                    }
                    break;
                case POTION:
                    if(!p_know[obj->o_which]) {
                        return (TRUE);
                    }
                    break;
                case STICK:
                    if(!ws_know[obj->o_which]) {
                        return (TRUE);
                    }
                    break;
                case SCROLL:
                    if(!s_know[obj->o_which]) {
                        return (TRUE);
                    }
                    break;
                case MM:
                    if(!m_know[obj->o_which]) {
                        return (TRUE);
                    }
            }
            break;
        case WIELDABLE:
            switch(obj->o_type) {
                case STICK:
                case WEAPON:
                    return (TRUE);
                case RELIC:
                    switch(obj->o_which) {
                        case MUSTY_DAGGER:
                        case HRUGGEK_MSTAR:
                        case YEENOGHU_FLAIL:
                        case AXE_AKLAD:
                        case MING_STAFF:
                        case ORCUS_WAND:
                        case ASMO_ROD:
                            return (TRUE);
                    }
            }
            break;
        case IDENTABLE:
            if(!(obj->o_flags & ISKNOW) && obj->o_type != FOOD) {
                return (TRUE);
            }
            if(obj->o_type == MM) {
                switch(obj->o_which) {
                    case MM_JUG:
                        /* Can still identify a jug if we don't know the potion */
                        if(obj->o_ac != JUG_EMPTY && !p_know[obj->o_ac]) {
                            return (TRUE);
                        }
                }
            }
            break;
        case USEABLE:
            if(obj->o_type == MM) {
                switch(obj->o_which) {
                    case MM_BEAKER:
                    case MM_BOOK:
                    case MM_OPEN:
                    case MM_HUNGER:
                    case MM_DRUMS:
                    case MM_DISAPPEAR:
                    case MM_CHOKE:
                    case MM_KEOGHTOM:
                        return (TRUE);
                }
            } else if(obj->o_type == RELIC) {
                switch(obj->o_which) {
                    case EMORI_CLOAK:
                    case BRIAN_MANDOLIN:
                    case HEIL_ANKH:
                    case YENDOR_AMULET:
                    case STONEBONES_AMULET:
                    case GERYON_HORN:
                    case EYE_VECNA:
                    case QUILL_NAGROM:
                    case SURTUR_RING:
                        return (TRUE);
                }
            } else if(obj->o_type == POTION) {
                /*
                 * only assassins can use poison
                 */
                if(player.t_ctype == C_ASSASIN && obj->o_which == P_POISON) {
                    return (TRUE);
                }
            }
            break;
        case PROTECTABLE:
            switch(obj->o_type) {
                case WEAPON:
                    if((obj->o_flags & ISMETAL) == 0) {
                        return (FALSE);
                    }

                    /* Fall through */
                    [[clang::fallthrough]];
                case ARMOR:
                    return (TRUE);
                case MM:
                    if(obj->o_which == MM_BRACERS) {
                        return (TRUE);
                    }
            }
    }
    return (FALSE);
}

void del_pack(linked_list *item) {
    object *obj;

    obj = OBJPTR(item);
    if(obj->o_count > 1) {
        obj->o_count--;
    } else {
        cur_null(obj);
        detach(pack, item);
        o_discard(item);
        inpack--;
    }
}

/*
 * carry_obj:
 *    Check to see if a monster is carrying something and, if so, give it to him.
 */

void carry_obj(thing *mp, long chance) {
    linked_list *item;
    object *obj;

    /*
     * If there is no chance, just return.
     * Note that this means there must be a "chance" in order for
     * the creature to carry a relic.
     */
    if(chance <= 0) {
        return;
    }

    /*
     * check for the relic/artifacts
     * Do the relics first so they end up last in the pack. Attach()
     * always adds things to the beginning. This way they will be the
     * last things dropped when the creature is killed. This will ensure
     * the relic will be on top if there is a stack of item lying on the
     * floor and so the hero will know where it is if he's trying to
     * avoid it. Note that only UNIQUEs carry relics.
     */
    if(on(*mp, ISUNIQUE)) {
        if(on(*mp, CARRYMDAGGER)) {
            item = spec_item(RELIC, MUSTY_DAGGER, 0, 0);
            obj = OBJPTR(item);
            obj->o_pos = mp->t_pos;
            attach(mp->t_pack, item);
        }

        if(on(*mp, CARRYCLOAK)) {
            item = spec_item(RELIC, EMORI_CLOAK, 0, 0);
            obj = OBJPTR(item);
            obj->o_pos = mp->t_pos;
            attach(mp->t_pack, item);
        }

        if(on(*mp, CARRYANKH)) {
            item = spec_item(RELIC, HEIL_ANKH, 0, 0);
            obj = OBJPTR(item);
            obj->o_pos = mp->t_pos;
            attach(mp->t_pack, item);
        }

        if(on(*mp, CARRYSTAFF)) {
            item = spec_item(RELIC, MING_STAFF, 0, 0);
            obj = OBJPTR(item);
            obj->o_pos = mp->t_pos;
            attach(mp->t_pack, item);
        }

        if(on(*mp, CARRYWAND)) {
            item = spec_item(RELIC, ORCUS_WAND, 0, 0);
            obj = OBJPTR(item);
            obj->o_pos = mp->t_pos;
            attach(mp->t_pack, item);
        }

        if(on(*mp, CARRYROD)) {
            item = spec_item(RELIC, ASMO_ROD, 0, 0);
            obj = OBJPTR(item);
            obj->o_pos = mp->t_pos;
            attach(mp->t_pack, item);
        }

        if(on(*mp, CARRYYAMULET)) {
            item = spec_item(RELIC, YENDOR_AMULET, 0, 0);
            obj = OBJPTR(item);
            obj->o_pos = mp->t_pos;
            attach(mp->t_pack, item);
        }

        if(on(*mp, CARRYBAMULET)) {
            item = spec_item(RELIC, STONEBONES_AMULET, 0, 0);
            obj = OBJPTR(item);
            obj->o_pos = mp->t_pos;
            attach(mp->t_pack, item);
        }

        if(on(*mp, CARRYMANDOLIN)) {
            item = spec_item(RELIC, BRIAN_MANDOLIN, 0, 0);
            obj = OBJPTR(item);
            obj->o_pos = mp->t_pos;
            attach(mp->t_pack, item);
        }
        if(on(*mp, CARRYEYE)) {
            item = spec_item(RELIC, EYE_VECNA, 0, 0);
            obj = OBJPTR(item);
            obj->o_pos = mp->t_pos;
            attach(mp->t_pack, item);
        }
        if(on(*mp, CARRYAXE)) {
            item = spec_item(RELIC, AXE_AKLAD, 0, 0);
            obj = OBJPTR(item);
            obj->o_pos = mp->t_pos;
            attach(mp->t_pack, item);
        }
        if(on(*mp, CARRYQUILL)) {
            item = spec_item(RELIC, QUILL_NAGROM, 0, 0);
            obj = OBJPTR(item);
            obj->o_pos = mp->t_pos;
            obj->o_charges = rnd(QUILLCHARGES);
            attach(mp->t_pack, item);
            long howmany = roll(4, 3);
            for(long i = 0; i < howmany; i++) {
                /*
                 * the quill writes scrolls so give him a bunch
                 */
                item = new_thing(TYP_SCROLL, FALSE);
                obj = OBJPTR(item);
                obj->o_pos = mp->t_pos;
                attach(mp->t_pack, item);
            }
        }
        if(on(*mp, CARRYMSTAR)) {
            item = spec_item(RELIC, HRUGGEK_MSTAR, 0, 0);
            obj = OBJPTR(item);
            obj->o_pos = mp->t_pos;
            attach(mp->t_pack, item);
        }
        if(on(*mp, CARRYFLAIL)) {
            item = spec_item(RELIC, YEENOGHU_FLAIL, 0, 0);
            obj = OBJPTR(item);
            obj->o_pos = mp->t_pos;
            attach(mp->t_pack, item);
        }
        if(on(*mp, CARRYHORN)) {
            item = spec_item(RELIC, GERYON_HORN, 0, 0);
            obj = OBJPTR(item);
            obj->o_pos = mp->t_pos;
            attach(mp->t_pack, item);
        }
        if(on(*mp, CARRYSURTURRING)) {
            item = spec_item(RELIC, SURTUR_RING, 0, 0);
            obj = OBJPTR(item);
            obj->o_pos = mp->t_pos;
            attach(mp->t_pack, item);
        }
    }
    /*
     * If it carries gold, give it some
     */
    if(on(*mp, CARRYGOLD) && rnd(100) < chance) {
        item = spec_item(GOLD, 0, 0, 0);
        obj = OBJPTR(item);
        obj->o_count = GOLDCALC() + GOLDCALC();
        obj->o_pos = mp->t_pos;
        attach(mp->t_pack, item);
    }

    /*
     * If it carries food, give it some
     */
    if(on(*mp, CARRYFOOD) && rnd(100) < chance) {
        item = spec_item(FOOD, 0, 0, 0);
        obj = OBJPTR(item);
        obj->o_weight = things[TYP_FOOD].mi_wght;
        obj->o_pos = mp->t_pos;
        attach(mp->t_pack, item);
    }

    /*
     * If it carries a weapon, give it one
     */
    if(on(*mp, CARRYWEAPON) && rnd(100) < chance) {
        int type, hit, dam;

        /* Get the "bonuses" */
        hit = rnd(5 + (vlevel / 5)) - 2;
        dam = rnd(5 + (vlevel / 5)) - 2;

        /* Only choose an appropriate type of weapon */
        switch(rnd(12)) {
            case 0:
                type = DAGGER;
                break;
            case 1:
                type = BATTLEAXE;
                break;
            case 2:
                type = MACE;
                break;
            case 3:
                type = SWORD;
                break;
            case 4:
                type = PIKE;
                break;
            case 5:
                type = HALBERD;
                break;
            case 6:
                type = SPETUM;
                break;
            case 7:
                type = BARDICHE;
                break;
            case 8:
                type = TRIDENT;
                break;
            case 9:
                type = BASWORD;
                break;
            case 10:
                type = DART;
                break;
            default:
                type = TWOSWORD;
        }

        /* Create the item */
        item = spec_item(WEAPON, type, hit, dam);
        obj = OBJPTR(item);
        obj->o_pos = mp->t_pos;
        attach(mp->t_pack, item);
    }

    /*
     * If it carries a dagger, give it one
     */
    if(on(*mp, CARRYDAGGER) && rnd(100) < chance) {
        int hit, dam;

        /* Get the "bonuses" */
        hit = rnd(3 + (vlevel / 5)) - 1;
        dam = rnd(3 + (vlevel / 5)) - 1;

        /* Create the item */
        item = spec_item(WEAPON, DAGGER, hit, dam);
        obj = OBJPTR(item);
        obj->o_pos = mp->t_pos;
        attach(mp->t_pack, item);
    }

    /*
     * If it carries a scroll, give it one
     */
    if(on(*mp, CARRYSCROLL) && rnd(100) < chance) {
        item = new_thing(TYP_SCROLL, TRUE);
        obj = OBJPTR(item);
        obj->o_pos = mp->t_pos;

        /* Can the monster carry this scroll? */
        if(obj->o_which == S_SCARE && mp->t_stats.s_intel < 16) {
            fall(item, FALSE); /* This would scare us! */
        } else {
            attach(mp->t_pack, item);
        }
    }

    /*
     * If it carries a potion, give it one
     */
    if(on(*mp, CARRYPOTION) && rnd(100) < chance) {
        item = new_thing(TYP_POTION, TRUE);
        obj = OBJPTR(item);
        obj->o_pos = mp->t_pos;
        attach(mp->t_pack, item);
    }

    /*
     * If it carries a ring, give it one
     */
    if(on(*mp, CARRYRING) && rnd(100) < chance) {
        item = new_thing(TYP_RING, TRUE);
        obj = OBJPTR(item);
        obj->o_pos = mp->t_pos;
        attach(mp->t_pack, item);
    }

    /*
     * If it carries a wand or staff, give it one
     */
    if(on(*mp, CARRYSTICK) && rnd(100) < chance) {
        item = new_thing(TYP_STICK, TRUE);
        obj = OBJPTR(item);
        obj->o_pos = mp->t_pos;
        attach(mp->t_pack, item);
    }

    /*
     * If it carries any miscellaneous magic, give it one
     */
    if(on(*mp, CARRYMISC) && rnd(100) < chance) {
        item = new_thing(TYP_MM, TRUE);
        obj = OBJPTR(item);
        obj->o_pos = mp->t_pos;
        attach(mp->t_pack, item);
    }

    /* Update the monster's encumberance */
    updpack(TRUE, mp);
}

/*
 * grab():
 *    See what is on the spot where the player is standing.  If
 *    nothing is there, do nothing.  If there is one thing, pick it
 *    up.  If there are multiple things, prompt the player for what
 *    he wants (* means everything).
 */

int grab(int y, int x) {
    linked_list *next_item, *item;
    object *obj;
    int cnt, pagecnt;
    int num_there = 0, ch, och;

    /*
     * Count how many objects there are and move them to the front
     * of the level list.
     */
    for(item = lvl_obj; item != nullptr; item = next_item) {
        obj = OBJPTR(item);
        next_item = next(item);
        if(obj->o_pos.y == y && obj->o_pos.x == x) {
            num_there++;
            detach(lvl_obj, item); /* Remove it from the list */
            attach(lvl_obj, item); /* Place it at the front of the list */
        }
    }

    /* Nothing there. */
    if(num_there < 1) {
        msg("Nothing %s", terse ? "there." : "to pick up.");
    }

    /* Something or things there */
    else {
        char linebuf[2 * LINELEN + 1];
        int curlen, maxlen = 0;

        wclear(hw);
        cnt = 0;
        pagecnt = 0;
        for(item = lvl_obj, ch = 'a'; item != nullptr && cnt < num_there;
            item = next(item), ch++, cnt++) {
            obj = OBJPTR(item);
            /* Construct how the line will look */
            sprintf(linebuf, "%c) %s\n\r", ch, inv_name(obj, FALSE));

            /* See how long it is */
            curlen = (int)strlen(linebuf) - 2; /* Don't count \n or \r */
            if(maxlen < curlen) {
                maxlen = curlen;
            }

            /* Draw it in the window */
            waddstr(hw, linebuf);

            if(++pagecnt >= LINES - 2 && next(item) != nullptr) {
                pagecnt = 0;
                maxlen = 0;
                dbotline(hw, spacemsg);
                wclear(hw);
            }
            if(ch == 'z') {
                ch = 'A' - 1;
            }
        }

        strcpy(linebuf, "Pick up what? (* for all): ");

        /* See how long it is */
        curlen = (int)strlen(linebuf); /* Don't count \n or \r */
        if(maxlen < curlen) {
            maxlen = curlen;
        }

        /* Draw it in the window */
        waddstr(hw, linebuf);

        /*
         * If we have fewer than half a screenful, don't clear the screen.
         * Leave an extra blank line at the bottom and 3 blank columns
         * to he right.
         */
        if(menu_overlay && num_there < LINES / 2 + 2) {
            over_win(cw, hw, num_there + 2, maxlen + 3, num_there, curlen, 0);
            pagecnt = -1; /* Indicate we used over_win */
        } else {
            draw(hw); /* write screen */
        }

        for(;;) {
            do {
                ch = tolower(readchar());
            } while(!(isalpha(ch) || ch == '*' || ch == ESCAPE));

            /* Redraw original screen */
            if(pagecnt < 0) {
                clearok(cw, FALSE); /* Setup to redraw current screen */
                touchwin(cw);       /* clearing first */
            } else {
                restscr(cw);
            }

            if(ch == ESCAPE) {
                after = FALSE;
                msg(""); /* clear top line */
                break;
            }
            if(ch == '*') {
                player.t_action = A_PICKUP;
                return (1); /* set action to PICKUP and delay for first one */
            }
            /* ch has item to get from list */

            cnt = 0;
            for(item = lvl_obj, och = 'a'; item != nullptr && cnt < num_there;
                item = next(item), och++, cnt++) {
                if(ch == och) {
                    break;
                }
                if(och == 'z') {
                    och = 'A' - 1;
                }
            }
            if(item == nullptr || cnt >= num_there) {
                wmove(hw, pagecnt < 0 ? num_there : pagecnt, 25);
                wprintw(hw, " [between 'a' and '%c']: ", och == 'A' ? 'z' : och - 1);
                if(maxlen < 49) {
                    maxlen = 49;
                }

                /*
                 * If we have fewer than half a screenful, don't clear the
                 * screen.  Leave an extra blank line at the bottom and
                 * 3 blank columns to he right.
                 */
                if(menu_overlay && num_there < LINES / 2 + 2) {
                    over_win(cw, hw, num_there + 2, maxlen + 3, num_there, 49, 0);
                    cnt = -1; /* Indicate we used over_win */
                } else {
                    draw(hw); /* write screen */
                }
                continue;
            }
            detach(lvl_obj, item);
            if(add_pack(item, FALSE, nullptr)) {
                /*
                 * We make sure here that the dungeon floor gets
                 * updated with what's left on the vacated spot.
                 */
                if((item = find_obj(y, x)) == nullptr) {
                    coord roomcoord; /* Needed to pass to roomin() */

                    roomcoord.y = y;
                    roomcoord.x = x;
                    mvaddch(y, x, (roomin(&roomcoord) == nullptr ? PASSAGE : FLOOR));
                } else {
                    mvaddch_int(y, x, (OBJPTR(item))->o_type);
                }
                return (1);
            }
            attach(lvl_obj, item); /* Couldn't pick it up! */

            break;
        }
    }

    return (0);
}

/*
 * make_sell_pack:
 *
 *    Create a pack for sellers (a la quartermaster)
 */

void make_sell_pack(thing *tp) {
    linked_list *item;
    object *obj;
    int sell_type = 0, nitems, i;

    /* Select the items */
    nitems = rnd(6) + 5;

    switch(rnd(9)) {
        /* Armor */
        case 0:
        case 1:
            turn_on(*tp, CARRYARMOR);
            sell_type = TYP_ARMOR;
            break;

        /* Weapon */
        case 2:
        case 3:
            turn_on(*tp, CARRYWEAPON);
            sell_type = TYP_WEAPON;
            break;

        /* Staff or wand */
        case 4:
            turn_on(*tp, CARRYSTICK);
            sell_type = TYP_STICK;
            break;

        /* Ring */
        case 5:
            turn_on(*tp, CARRYRING);
            sell_type = TYP_RING;
            break;

        /* scroll */
        case 6:
            turn_on(*tp, CARRYSCROLL);
            sell_type = TYP_SCROLL;
            break;

        /* potions */
        case 7:
            turn_on(*tp, CARRYPOTION);
            sell_type = TYP_POTION;
            break;

        /* Miscellaneous magic */
        case 8:
            turn_on(*tp, CARRYMISC);
            sell_type = TYP_MM;
            break;
    }
    for(i = 0; i < nitems; i++) {
        item = new_thing(sell_type, FALSE);
        obj = OBJPTR(item);
        obj->o_pos = tp->t_pos;
        attach(tp->t_pack, item);
    }
}
