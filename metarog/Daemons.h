#pragma once

#include <algorithm>
#include <list>

template <typename DaemonID_t>
struct DelayedAction {
    int d_type;
    DaemonID_t d_id;
    int d_arg;
    int d_time;
};

template <typename DaemonID_t>
class Daemons {
    static const int Daemon = -1;

public:
    static const int Before = 1;
    static const int After = 2;

    typedef DelayedAction<DaemonID_t> Action;
    std::list<Action> actions;

    Daemons() {
    }
    virtual ~Daemons() {
    }

    Action &newAction() {
        actions.emplace_back(Action{});
    }

    void startDaemon(DaemonID_t did, int arg, int type) {
        actions.emplace_back(Action{type, did, arg, Daemon});
    }

    void killDaemon(DaemonID_t did) {
        actions.remove_if([did](const Action &a) { return a.d_id == did; });
    }

    virtual void executeAction(const Action &a) = 0;

    void doDaemons(int flag) {
        for(const auto &a : actions) {
            if(a.d_type == flag && a.d_time == Daemon) {
                executeAction(a);
            }
        }
    }

    // FIXME: time is in the wrong order
    void startFuse(DaemonID_t did, int arg, int time, int type) {
        actions.emplace_back(Action{type, did, arg, time});
    }

    void lengthen(DaemonID_t did, int xtime) {
        auto a = std::find_if(actions.begin(), actions.end(),
                              [did](const Action &aa) { return aa.d_id == did; });

        if(a != actions.end()) {
            a->d_time += xtime;
        }
    }

    void extinguish(DaemonID_t did) {
        killDaemon(did);
    }

    // FIXME: the cleanup part is ugly.
    void doFuses(int flag) {
        bool cleanup = false;

        for(auto &a : actions) {
            if(a.d_type == flag && a.d_time > 0 && --a.d_time == 0) {
                executeAction(a);
                a.d_id = static_cast<DaemonID_t>(0) /*d_none*/;
                cleanup = true;
            }
        }

        if(cleanup) {
            killDaemon(static_cast<DaemonID_t>(0) /*d_none*/);
        }
    }
};
