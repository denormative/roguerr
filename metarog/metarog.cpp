#include "metarog.h"
#include <climits>

#if defined(_WIN32)
#include <Lmcons.h>
#include <Windows.h>
#include <process.h>
#include <shlobj.h>
#include <sys/types.h>
#undef MOUSE_MOVED
#else
#include <pwd.h>
#include <sys/stat.h>
#include <sys/utsname.h>
#include <unistd.h>
#endif

bool directory_exists(const char *dirname) {
    struct stat sb;

    if(stat(dirname, &sb) == 0) { /* path exists */
        return (sb.st_mode & S_IFDIR);
    }

    return (0);
}

char *md_getusername() {
    static char login[80];
    const char *l = nullptr;
#ifdef _WIN32
    LPSTR mybuffer;
    DWORD size = UNLEN + 1;
    TCHAR buffer[UNLEN + 1];

    mybuffer = buffer;
    GetUserName(mybuffer, &size);
    l = mybuffer;
#else
    struct passwd *pw;

    pw = getpwuid(getuid());

    l = pw->pw_name;
#endif

    if((l == nullptr) || (*l == '\0')) {
        if((l = getenv("USERNAME")) == nullptr) {
            if((l = getenv("LOGNAME")) == nullptr) {
                if((l = getenv("USER")) == nullptr) {
                    l = "nobody";
                }
            }
        }
    }

    strncpy(login, l, 80);
    login[79] = 0;

    return (login);
}

const char *md_getroguedir() {
    static char path[1024];
    char *end, *home;

    if((home = getenv("ROGUEHOME")) != nullptr) {
        if(*home) {
            strncpy(path, home, PATH_MAX - 20);

            end = &path[strlen(path) - 1];

            while((end >= path) && ((*end == '/') || (*end == '\\'))) {
                *end-- = '\0';
            }

            if(directory_exists(path)) {
                return (path);
            }
        }
    }

    if(directory_exists("/var/games/roguelike")) {
        return ("/var/games/roguelike");
    }
    if(directory_exists("/var/lib/roguelike")) {
        return ("/var/lib/roguelike");
    }
    if(directory_exists("/var/roguelike")) {
        return ("/var/roguelike");
    }
    if(directory_exists("/usr/games/lib")) {
        return ("/usr/games/lib");
    }
    if(directory_exists("/games/roguelik")) {
        return ("/games/roguelik");
    }
    if(directory_exists(md_gethomedir())) {
        return (md_gethomedir());
    }
    return ("");
}

char *md_gethomedir() {
    static char homedir[PATH_MAX];
    const char *h = nullptr;
    size_t len;
#if defined(_WIN32)
    TCHAR szPath[MAX_PATH];
    char slash = '\\';
#else
    char slash = '/';
    struct passwd *pw;
    pw = getpwuid(getuid());

    h = pw->pw_dir;

    if(strcmp(h, "/") == 0) {
        h = nullptr;
    }
#endif
    homedir[0] = 0;
#ifdef _WIN32
    if(SUCCEEDED(SHGetFolderPath(NULL, CSIDL_PERSONAL, NULL, 0, szPath))) {
        h = szPath;
    }
#endif

    if((h == nullptr) || (*h == '\0')) {
        if((h = getenv("HOME")) == nullptr) {
            if((h = getenv("HOMEDRIVE")) == nullptr) {
                h = "";
            } else {
                strncpy(homedir, h, PATH_MAX - 1);
                homedir[PATH_MAX - 1] = 0;

                if((h = getenv("HOMEPATH")) == nullptr) {
                    h = "";
                }
            }
        }
    }

    len = strlen(homedir);
    strncat(homedir, h, PATH_MAX - len - 1);
    len = strlen(homedir);

    if((len > 0) && (homedir[len - 1] != slash)) {
        homedir[len] = slash;
        homedir[len + 1] = 0;
    }

    return (homedir);
}

int md_unlink(const char *file) {
#ifdef _WIN32
    chmod(file, 0600);
    return (_unlink(file));
#else
    return file!=nullptr;//(unlink(file));
#endif
}

const char *md_crypt(const char *key) {
    return key;
}

int md_rand() {
#ifdef _WIN32
    return (rand());
#else
    return (int)(random());
#endif
}

int md_rand(int range) {
#ifdef _WIN32
    return (range <= 0 ? 0 : rand() % range);
#else
    return (range <= 0 ? 0 : random() % range);
#endif
}

void md_srand(int seed) {
#ifdef _WIN32
    srand(seed);
#else
    srandom((unsigned int)seed);
#endif
}

const char *md_getpass(const char *prompt) {
#ifdef _WIN32
    static char password_buffer[9];
    char *p = password_buffer;
    int c, count = 0;
    int max_length = 9;

    fflush(stdout);
    /* If we can't prompt, abort */
    if(fputs(prompt, stderr) < 0) {
        *p = '\0';
        return NULL;
    }

    for(;;) {
        /* Get a character with no echo */
        c = _getch();

        /* Exit on interrupt (^c or ^break) */
        if(c == '\003' || c == 0x100) {
            exit(1);
        }

        /* Terminate on end of line or file (^j, ^m, ^d, ^z) */
        if(c == '\r' || c == '\n' || c == '\004' || c == '\032') {
            break;
        }

        /* Back up on backspace */
        if(c == '\b') {
            if(count) {
                count--;
            } else if(p > password_buffer) {
                p--;
            }
            continue;
        }

        /* Ignore DOS extended characters */
        if((c & 0xff) != c) {
            continue;
        }

        /* Add to password if it isn't full */
        if(p < password_buffer + max_length - 1) {
            *p++ = (char)c;
        } else {
            count++;
        }
    }
    *p = '\0';

    fputc('\n', stderr);

    return password_buffer;
#else
    return (getpass(prompt));
#endif
}

char *md_gethostname() {
    static char nodename[80];
    const char *n = nullptr;
#if !defined(_WIN32)
    struct utsname ourname;

    if(uname(&ourname) == 0) {
        n = ourname.nodename;
    }
#endif
    if((n == nullptr) || (*n == '\0')) {
        if((n = getenv("COMPUTERNAME")) == nullptr) {
            if((n = getenv("HOSTNAME")) == nullptr) {
                n = "localhost";
            }
        }
    }

    strncpy(nodename, n, 80);
    nodename[79] = 0;

    return (nodename);
}
