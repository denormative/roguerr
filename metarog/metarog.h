
#pragma once

#include "cmetarog.h"
#include <curses.h>
// FIXME: cassert not really needed, probably should be cleaned in future
#include <array>
#include <cassert>
#include <cctype>
#include <cstring>
using std::array;
#include <string>
using std::string;

inline int mvwaddch(WINDOW *win, int y, int x, const int ch) {
    return mvwaddch(win, y, x, static_cast<const chtype>(ch));
}

inline int mvwaddch(WINDOW *win, int y, int x, const char ch) {
    return mvwaddch(win, y, x, static_cast<const chtype>(ch));
}

inline int mvwaddch(WINDOW *win, int y, int x, const long ch) {
    return mvwaddch(win, y, x, static_cast<const chtype>(ch));
}

inline int mvaddch(int y, int x, const int ch) {
    return mvaddch(y, x, static_cast<const chtype>(ch));
}

inline int mvaddch(int y, int x, const long ch) {
    return mvaddch(y, x, static_cast<const chtype>(ch));
}

inline char *unctrl(int c) {
    return unctrl(static_cast<const chtype>(c));
}

inline int waddch(WINDOW *win, const int ch) {
    return waddch(win, static_cast<const chtype>(ch));
}

inline int addch(const int ch) {
    return addch((const chtype)ch);
}

inline int imvwinch(WINDOW *win, int y, int x) {
    return (int)mvwinch(win, y, x);
}

inline int imvinch(int y, int x) {
    return (int)mvinch(y, x);
}

inline int isalpha(chtype _c) {
    return isalpha(static_cast<int>(_c));
}

template <typename T>
auto max(T a, T b) {
    return ((a) > (b) ? (a) : (b));
}

template <typename T>
auto min(T a, T b) {
    return ((a) < (b) ? (a) : (b));
}

char *md_getusername();
const char *md_getroguedir();
char *md_gethomedir();
int md_unlink(const char *file);
const char *md_crypt(const char *key);
int md_rand();
int md_rand(int range);
void md_srand(int seed);
const char *md_getpass(const char *prompt);
char *md_gethostname();

inline int ssch() {
    return winch(stdscr) & A_CHARTEXT;
}

inline int ssinch(int y, int x) {
    return mvwinch(stdscr, y, x) & A_CHARTEXT;
}

inline int cwinch(int y, int x) {
    extern WINDOW *cw;
    return mvwinch(cw, y, x) & A_CHARTEXT;
}

inline int mwinch(int y, int x) {
    extern WINDOW *mw;
    return mvwinch(mw, y, x) & A_CHARTEXT;
}

inline int hwinch(int y, int x) {
    extern WINDOW *hw;
    return mvwinch(hw, y, x) & A_CHARTEXT;
}

inline int cwaddch(int y, int x, const int ch) {
    extern WINDOW *cw;
    return mvwaddch(cw, y, x, static_cast<const chtype>(ch));
}

inline int mwaddch(int y, int x, const int ch) {
    extern WINDOW *mw;
    return mvwaddch(mw, y, x, static_cast<const chtype>(ch));
}

inline int hwaddch(int y, int x, const int ch) {
    extern WINDOW *hw;
    return mvwaddch(hw, y, x, static_cast<const chtype>(ch));
}

inline int cwaddstr(int y, int x, const char *str) {
    extern WINDOW *cw;
    return mvwaddstr(cw, y, x, str);
}
inline int hwaddstr(int y, int x, const char *str) {
    extern WINDOW *hw;
    return mvwaddstr(hw, y, x, str);
}

#include "lib/platform_folders.h"

inline string saveDir(const string name) {
    return sago::getSaveGamesFolder1() + "/" + name + "/";
}
