#pragma once

// #include "lib/platform_folders.h"
// using namespace sago;

#pragma clang diagnostic ignored "-Wempty-translation-unit"

#define errormsg(X)                                                                                \
    do {                                                                                           \
        printf("error: %s¥n%s%d", (X), __FILE__, __LINE__);                                        \
        exit(-1);                                                                                  \
    } while(0)
//#define if_error(X, Y) do ( if((X)) { error((Y)); } ) while(0)

#define unctrl_int(X) unctrl((chtype)(X))
#define addch_int(X) addch((chtype)(X))
#define mvaddch_int(y, x, ch) mvaddch((y), (x), (chtype)(ch))
#define mvaddstr_int(y, x, str) mvaddstr((int)(y), (int)(x), str)
#define mvwaddch_int(win, y, x, ch) mvwaddch((win), (y), (x), (chtype)(ch))
#define waddch_int(win, ch) waddch((win), (chtype)(ch))

extern int metarog_version;
