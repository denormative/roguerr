#pragma once

#include <string>
using std::string;
#include <time.h>
#include <vector>
using std::vector;
#include <fstream>

#include <cereal/archives/json.hpp>
#include <cereal/types/string.hpp>
#include <cereal/types/vector.hpp>

// TODO: most of these are useless, should clean them up
struct ScoreEntry {
    long sc_score;  /* gold */
    string sc_name; /* players name */
    long sc_flags;  /* reason for being here */
    long sc_level;  /* dungeon level */
    long sc_ctype;
    long sc_monster; /* killer */
    long sc_quest;
    long sc_time;
    long sc_explvl; /* experience level */
    long sc_exppts; /* experience points */
    time_t sc_date; /* time this score was posted */
    long sc_artifacts;
    long sc_gold;
    string sc_reason;
    bool was_wizard;

    template <class Archive>
    void serialize(Archive &ar) {
        ar(CEREAL_NVP(sc_score), CEREAL_NVP(sc_name), CEREAL_NVP(sc_flags), CEREAL_NVP(sc_level),
           CEREAL_NVP(sc_ctype), CEREAL_NVP(sc_monster), CEREAL_NVP(sc_quest), CEREAL_NVP(sc_time),
           CEREAL_NVP(sc_explvl), CEREAL_NVP(sc_exppts), CEREAL_NVP(sc_date),
           CEREAL_NVP(sc_artifacts), CEREAL_NVP(sc_gold), CEREAL_NVP(sc_reason),
           CEREAL_NVP(was_wizard));
    }
};

class Scores {
    const string name;

public: // FIXME: maybe private?
    vector<ScoreEntry> scores;

public:
    Scores(string file_name) : name(file_name) {
    }

    void load() {
        std::ifstream is(name + ".json", std::ios::binary);

        // Don't care if we score file doesn't exist yet.
        if(is.fail())
            return;

        cereal::JSONInputArchive archive(is);
        archive(CEREAL_NVP(scores));
    }

    void save() {
        std::ofstream os(name + ".json", std::ios::binary);

        // FIXME: probably should handle not being able to save scores, or at least log the error
        if(os.fail())
            return;

        cereal::JSONOutputArchive archive(os);
        archive(CEREAL_NVP(scores));
    }

    void addScore(const ScoreEntry &entry) {
        // TODO: Limit this to 'top 10'?
        scores.push_back(entry);
        std::sort(scores.begin(), scores.end(),
                  [](const ScoreEntry &a, const ScoreEntry &b) { return a.sc_score > b.sc_score; });
    }
};
