/*
        list.c - Functions for dealing with linked lists of goodies

        XRogue: Expeditions into the Dungeons of Doom
        Copyright (C) 1991 Robert Pietkivitch
        All rights reserved.

        Based on "Advanced Rogue"
        Copyright (C) 1984, 1985 Michael Morgan, Ken Dalka and AT&T
        All rights reserved.

        Based on "Rogue: Exploring the Dungeons of Doom"
        Copyright (C) 1980, 1981 Michael Toy, Ken Arnold and Glenn Wichman
        All rights reserved.

        See the file LICENSE.TXT for full copyright and licensing information.
*/

#include "rogue.h"

/*
 * detach:
 *      Takes an item out of whatever linked list it might be in
 */

void _detach(linked_list **list, linked_list *item) {
    if(*list == item) {
        *list = next(item);
    }
    if(prev(item) != nullptr) {
        item->l_prev->l_next = next(item);
    }
    if(next(item) != nullptr) {
        item->l_next->l_prev = prev(item);
    }
    item->l_next = nullptr;
    item->l_prev = nullptr;
}

/*
 * _attach:
 *      add an item to the head of a list
 */

void _attach(linked_list **list, linked_list *item) {
    if(*list != nullptr) {
        item->l_next = *list;
        (*list)->l_prev = item;
        item->l_prev = nullptr;
    } else {
        item->l_next = nullptr;
        item->l_prev = nullptr;
    }

    *list = item;
}

/*
 * o_free_list:
 *      Throw the whole object list away
 */

void _o_free_list(linked_list **ptr) {
    linked_list *item;

    while(*ptr != nullptr) {
        item = *ptr;
        *ptr = next(item);
        o_discard(item);
    }
}

/*
 * o_discard:
 *      free up an item and its object(and maybe contents)
 */

void o_discard(linked_list *item) {
    object *obj;

    obj = OBJPTR(item);
    if(obj->contents != nullptr) {
        o_free_list(obj->contents);
    }
    total -= 2;
    FREE(obj);
    FREE(item);
}

/*
   r_free_fire_list
           Throw the whole list of fire monsters away. But don't
           discard the item (monster) itself as that belong to mlist.
*/

void _r_free_fire_list(linked_list **ptr) {
    linked_list *item;

    while(*ptr != nullptr) {
        item = *ptr;
        *ptr = next(item);
        free(item);
    }
}
/*
 * r_free_list:
 *      Throw the whole list of room exits away
 */

void _r_free_list(linked_list **ptr) {
    linked_list *item;

    while(*ptr != nullptr) {
        item = *ptr;
        *ptr = next(item);
        r_discard(item);
    }
}

/*
 * r_discard:
 *      free up an item and its room
 */

void r_discard(linked_list *item) {
    total -= 2;
    FREE(DOORPTR(item));
    FREE(item);
}

/*
 * t_free_list:
 *      Throw the whole thing list away
 */

void _t_free_list(linked_list **ptr) {
    linked_list *item;

    while(*ptr != nullptr) {
        item = *ptr;
        *ptr = next(item);
        t_discard(item);
    }
}

/*
 * t_discard:
 *      free up an item and its thing
 */

void t_discard(linked_list *item) {
    thing *tp;

    total -= 2;
    tp = THINGPTR(item);
    if(tp->t_name != nullptr) {
        FREE(tp->t_name);
    }
    if(tp->t_pack != nullptr) {
        o_free_list(tp->t_pack);
    }
    FREE(tp);
    FREE(item);
}

/*
 * destroy_item:
 *      get rid of an item structure -- don't worry about contents
 */

void destroy_item(linked_list *item) {
    total--;
    FREE(item);
}

/*
 * new_item
 *      get a new item with a specified size
 */

linked_list *new_item(size_t size) {
    linked_list *item;

    if((item = (linked_list *)newalloc(sizeof *item)) == nullptr) {
        msg("Ran out of memory for header after %d items", total);
    }
    if((item->l_data = newalloc(size)) == nullptr) {
        msg("Ran out of memory for data after %d items", total);
    }
    item->l_next = item->l_prev = nullptr;
    memset(item->l_data, 0, (size_t)size);
    return item;
}

/*
 * creat_item:
 *      Create just an item structure -- don't make any contents
 */

linked_list *creat_item() {
    linked_list *item;

    if((item = (linked_list *)newalloc(sizeof *item)) == nullptr) {
        msg("Ran out of memory for header after %d items", total);
    }
    item->l_next = item->l_prev = nullptr;
    return item;
}

char *newalloc(size_t size) {
    char *space = (char *)ALLOC(size);

    if(space == nullptr) {
        sprintf(prbuf, "Rogue ran out of memory (wanted = %zd).", size);
        fatal(prbuf, -1);
    }
    total++;
    return space;
}
