/*
    state.c - Portable Rogue Save State Code

    Copyright (C) 2000 Nicholas J. Kisseberth

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions
    are met:
    1. Redistributions of source code must retain the above copyright
       notice, this list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright
       notice, this list of conditions and the following disclaimer in the
       documentation and/or other materials provided with the distribution.
    3. Neither the name(s) of the author(s) nor the names of other contributors
       may be used to endorse or promote products derived from this software
       without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE AUTHOR(S) AND CONTRIBUTORS ``AS IS'' AND
    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR(S) OR CONTRIBUTORS BE LIABLE
    FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
    DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
    OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
    HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
    LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
    OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
    SUCH DAMAGE.
*/

#define RSXR_STATS 0xABCD0001
#define RSXR_THING 0xABCD0002
#define RSXR_OBJECT 0xABCD0003
#define RSXR_MAGICITEMS 0xABCD0004
#define RSXR_KNOWS 0xABCD0005
#define RSXR_GUESSES 0xABCD0006
#define RSXR_OBJECTLIST 0xABCD0007
#define RSXR_BAGOBJECT 0xABCD0008
#define RSXR_MONSTERLIST 0xABCD0009
#define RSXR_MONSTERSTATS 0xABCD000A
#define RSXR_MONSTERS 0xABCD000B
#define RSXR_TRAP 0xABCD000C
#define RSXR_WINDOW 0xABCD000D
#define RSXR_DAEMONS 0xABCD000E
#define RSXR_IWEAPS 0xABCD000F
#define RSXR_IARMOR 0xABCD0010
#define RSXR_SPELLS 0xABCD0011
#define RSXR_ILIST 0xABCD0012
#define RSXR_HLIST 0xABCD0013
#define RSXR_DEATHTYPE 0xABCD0014
#define RSXR_CTYPES 0xABCD0015
#define RSXR_COORDLIST 0xABCD0016
#define RSXR_ROOMS 0xABCD0017

#if defined(_WIN32)
#include <Lmcons.h>
#include <Shlwapi.h>
#include <Windows.h>
#include <process.h>
#include <shlobj.h>
#undef MOUSE_MOVED
#else
#include <pwd.h>
#include <sys/utsname.h>
#include <unistd.h>
#endif

#include "rogue.h"
#include <climits>
#include <csignal>
#include <cstdarg>
#include <cstdio>
#include <ctime>
#include <sys/stat.h>

#define READSTAT ((format_error == 0) && (read_error == 0))
#define WRITESTAT (write_error == 0)

static int read_error = FALSE;
static int write_error = FALSE;
static int format_error = FALSE;

static int save_debug = FALSE;
#define DBG(x)                                                                                     \
    {                                                                                              \
        if(save_debug)                                                                             \
            rsPrintf x;                                                                            \
    }

int rsPrintf(const char *fmt, ...) {
    va_list ap;

    va_start(ap, fmt);
#pragma clang diagnostic ignored "-Wformat-nonliteral"
    vfprintf(stderr, fmt, ap);
    va_end(ap);

    return (0);
}

void *get_list_item(linked_list *l, int i) {
    int count = 0;

    while(l != nullptr) {
        if(count == i) {
            return (l->l_data);
        }

        l = l->l_next;

        count++;
    }

    return (nullptr);
}

int rs_write(FILE *savef, void *ptr, size_t size) {
    size_t i = 0;

    if(!write_error) {
        i = (size_t)encwrite((char *)ptr, size, savef);
    }
    if(i != size) {
        write_error = TRUE;
    }

    assert(write_error == 0);
    return (WRITESTAT);
}

static int end_of_file = FALSE;

int rs_read(int inf, void *ptr, size_t size) {
    int actual;
    end_of_file = FALSE;
    if(!read_error && !format_error) {
        actual = (int)encread((char *)ptr, size, inf);

        if((actual == 0) && (size != 0)) {
            end_of_file = TRUE;
        }
    }

    if(read_error) {
        printf("read error has occurred. restore short-circuited.\n");
        abort();
    }
    if(format_error) {
        printf("game format invalid. restore short-circuited.\n");
        abort();
    }

    return (READSTAT);
}

int big_endian = 0;

int rs_write_uint(FILE *savef, unsigned int c) {
    char bytes[4];
    char *buf = (char *)&c;

    if(big_endian) {
        bytes[3] = buf[0];
        bytes[2] = buf[1];
        bytes[1] = buf[2];
        bytes[0] = buf[3];
        buf = bytes;
    }

    rs_write(savef, buf, 4);

    return (WRITESTAT);
}

int rs_write_int(FILE *savef, int c) {
    char bytes[4];
    char *buf = (char *)&c;

    if(big_endian) {
        bytes[3] = buf[0];
        bytes[2] = buf[1];
        bytes[1] = buf[2];
        bytes[0] = buf[3];
        buf = bytes;
    }

    rs_write(savef, buf, 4);

    return (WRITESTAT);
}

int rs_write_ulong(FILE *savef, unsigned long c) {
    char bytes[4];
    char *buf = (char *)&c;

    if(big_endian) {
        bytes[3] = buf[0];
        bytes[2] = buf[1];
        bytes[1] = buf[2];
        bytes[0] = buf[3];
        buf = bytes;
    }

    rs_write(savef, buf, 4);

    return (WRITESTAT);
}

int rs_write_long(FILE *savef, long c) {
    char bytes[4];
    char *buf = (char *)&c;

    if(big_endian) {
        bytes[3] = buf[0];
        bytes[2] = buf[1];
        bytes[1] = buf[2];
        bytes[0] = buf[3];
        buf = bytes;
    }

    rs_write(savef, buf, 4);

    return (WRITESTAT);
}

int rs_write_boolean(FILE *savef, bool c) {
    char buf;

    if(c == 0) {
        buf = 0;
    } else {
        buf = 1;
    }

    rs_write(savef, &buf, 1);

    return (WRITESTAT);
}

int rs_read_int(int inf, int *i) {
    char bytes[4];
    int input;
    char *buf = (char *)&input;

    rs_read(inf, &input, 4);

    if(big_endian) {
        bytes[3] = buf[0];
        bytes[2] = buf[1];
        bytes[1] = buf[2];
        bytes[0] = buf[3];
        buf = bytes;
    }

    *i = *((int *)buf);

    return (READSTAT);
}

int rs_read_uint(int inf, unsigned int *i) {
    char bytes[4];
    int input;
    char *buf = (char *)&input;

    rs_read(inf, &input, 4);

    if(big_endian) {
        bytes[3] = buf[0];
        bytes[2] = buf[1];
        bytes[1] = buf[2];
        bytes[0] = buf[3];
        buf = bytes;
    }

    *i = *((unsigned int *)buf);

    return (READSTAT);
}

int rs_read_ulong(int inf, unsigned long *i) {
    char bytes[4];
    unsigned long input;
    char *buf = (char *)&input;

    rs_read(inf, &input, 4);

    if(big_endian) {
        bytes[3] = buf[0];
        bytes[2] = buf[1];
        bytes[1] = buf[2];
        bytes[0] = buf[3];
        buf = bytes;
    }

    *i = *((unsigned long *)buf);
    return (READSTAT);
}

int rs_read_long(int inf, long *i) {
    char bytes[4];
    long input;
    char *buf = (char *)&input;

    rs_read(inf, &input, 4);

    if(big_endian) {
        bytes[3] = buf[0];
        bytes[2] = buf[1];
        bytes[1] = buf[2];
        bytes[0] = buf[3];
        buf = bytes;
    }

    *i = *((long *)buf);
    return (READSTAT);
}

int rs_read_boolean(int inf, bool *i) {
    char buf;

    rs_read(inf, &buf, 1);

    *i = buf;

    return (READSTAT);
}

int rs_write_ints(FILE *savef, int *c, int count) {
    int n = 0;

    rs_write_int(savef, count);

    for(n = 0; n < count; n++) {
        rs_write_int(savef, c[n]);
    }

    return (WRITESTAT);
}

int rs_write_short(FILE *savef, short c) {
    char bytes[2];
    char *buf = (char *)&c;

    if(big_endian) {
        bytes[1] = buf[0];
        bytes[0] = buf[1];
        buf = bytes;
    }

    rs_write(savef, buf, 2);

    return (WRITESTAT);
}

int rs_read_short(int inf, short *s) {
    char bytes[2];
    short input;
    char *buf = (char *)&input;

    rs_read(inf, &input, 2);

    if(big_endian) {
        bytes[1] = buf[0];
        bytes[0] = buf[1];
        buf = bytes;
    }

    *s = *((short *)buf);
    return (READSTAT);
}

int rs_write_shorts(FILE *savef, short *c, int count) {
    int n = 0;

    rs_write_int(savef, count);

    for(n = 0; n < count; n++) {
        rs_write_short(savef, c[n]);
    }

    return (WRITESTAT);
}

int rs_write_longs(FILE *savef, long *c, int count) {
    int n = 0;

    rs_write_int(savef, count);

    for(n = 0; n < count; n++) {
        rs_write_long(savef, c[n]);
    }

    return (WRITESTAT);
}

int rs_write_ulongs(FILE *savef, unsigned long *c, int count) {
    int n = 0;

    rs_write_int(savef, count);

    for(n = 0; n < count; n++) {
        rs_write_ulong(savef, c[n]);
    }

    return (WRITESTAT);
}

int rs_write_booleans(FILE *savef, bool *c, int count) {
    int n = 0;

    rs_write_int(savef, count);

    for(n = 0; n < count; n++) {
        rs_write_boolean(savef, c[n]);
    }

    return (WRITESTAT);
}

int rs_read_ints(int inf, int *i, int count) {
    int n = 0, value = 0;

    if(rs_read_int(inf, &value) != 0) {
        if(value != count) {
            format_error = TRUE;
        } else {
            for(n = 0; n < value; n++) {
                rs_read_int(inf, &i[n]);
            }
        }
    }

    return (READSTAT);
}

int rs_read_shorts(int inf, short *i, int count) {
    int n = 0, value = 0;

    if(rs_read_int(inf, &value) != 0) {
        if(value != count) {
            format_error = TRUE;
        } else {
            for(n = 0; n < value; n++) {
                rs_read_short(inf, &i[n]);
            }
        }
    }

    return (READSTAT);
}

int rs_read_longs(int inf, long *i, int count) {
    int n = 0, value = 0;

    if(rs_read_int(inf, &value) != 0) {
        if(value != count) {
            format_error = TRUE;
        } else {
            for(n = 0; n < value; n++) {
                rs_read_long(inf, &i[n]);
            }
        }
    }

    return (READSTAT);
}

int rs_read_ulongs(int inf, unsigned long *i, int count) {
    int n = 0, value = 0;

    if(rs_read_int(inf, &value) != 0) {
        if(value != count) {
            format_error = TRUE;
        } else {
            for(n = 0; n < value; n++) {
                rs_read_ulong(inf, &i[n]);
            }
        }
    }

    return (READSTAT);
}

int rs_read_booleans(int inf, bool *i, int count) {
    int n = 0, value = 0;

    if(rs_read_int(inf, &value) != 0) {
        if(value != count) {
            format_error = TRUE;
        } else {
            for(n = 0; n < value; n++) {
                rs_read_boolean(inf, &i[n]);
            }
        }
    }

    return (READSTAT);
}

int rs_write_levtype(FILE *savef, LEVTYPE c) {
    int lt;

    switch(c) {
        case NORMLEV:
            lt = 1;
            break;
        case POSTLEV:
            lt = 2;
            break;
        case MAZELEV:
            lt = 3;
            break;
        case OUTSIDE:
            lt = 4;
            break;
        case STARTLEV:
            lt = 5;
            break;
            // default:
            //     lt = -1;
            //     break;
    }

    rs_write_int(savef, lt);

    return (WRITESTAT);
}

int rs_read_levtype(int inf, LEVTYPE *l) {
    int lt;

    rs_read_int(inf, &lt);

    switch(lt) {
        case 1:
            *l = NORMLEV;
            break;
        case 2:
            *l = POSTLEV;
            break;
        case 3:
            *l = MAZELEV;
            break;
        case 4:
            *l = OUTSIDE;
            break;
        case 5:
            *l = STARTLEV;
            break;
        default:
            *l = NORMLEV;
            break;
    }

    return (READSTAT);
}

int rs_write_char(FILE *savef, char c) {
    rs_write(savef, &c, 1);
    DBG(("%c", c));

    return (WRITESTAT);
}

int rs_read_char(int inf, char *c) {
    rs_read(inf, c, 1);

    return (READSTAT);
}

int rs_write_uchar(FILE *savef, unsigned char c) {
    rs_write(savef, &c, 1);
    DBG(("%c", c));

    return (WRITESTAT);
}

int rs_read_uchar(int inf, unsigned char *c) {
    rs_read(inf, c, 1);

    return (READSTAT);
}

int rs_write_string(FILE *savef, char *s) {
    int len = 0;

    len = (s == nullptr) ? 0 : (int)strlen(s) + 1;

    rs_write_uint(savef, (unsigned int)len);
    rs_write(savef, s, (size_t)len);

    return (WRITESTAT);
}

int rs_read_string_index(int inf, words master[], int maxindex, char **str) {
    int i;

    if(rs_read_int(inf, &i) != 0) {
        if(i > maxindex) {
            printf("String index is out of range. %d > %d\n", i, maxindex);
            printf("Sorry, invalid save game format\n");
            format_error = TRUE;
        } else if(i >= 0) {
            *str = master[i].w_string;
        } else {
            *str = nullptr;
        }
    }
    return (READSTAT);
}

int rs_write_string_index(FILE *savef, words master[], int maxindex, const char *str) {
    int i;

    for(i = 0; i < maxindex; i++) {
        if(str == master[i].w_string) {
            rs_write_int(savef, i);
            return (WRITESTAT);
        }
    }

    rs_write_int(savef, -1);
    return (WRITESTAT);
}

int rs_read_scrolls(int inf) {
    int i;

    for(i = 0; i < MAXSCROLLS; i++) {
        rs_read_new_string(inf, &s_names[i]);
        rs_read_boolean(inf, &s_know[i]);
        rs_read_new_string(inf, &s_guess[i]);
    }

    return (READSTAT);
}

int rs_write_scrolls(FILE *savef) {
    int i;

    for(i = 0; i < MAXSCROLLS; i++) {
        rs_write_string(savef, s_names[i]);
        rs_write_boolean(savef, s_know[i]);
        rs_write_string(savef, s_guess[i]);
    }
    return (READSTAT);
}

int rs_read_potions(int inf) {
    int i;

    for(i = 0; i < MAXPOTIONS; i++) {
        rs_read_string_index(inf, rainbow, NCOLORS, &p_colors[i]);
        rs_read_boolean(inf, &p_know[i]);
        rs_read_new_string(inf, &p_guess[i]);
    }

    return (READSTAT);
}

int rs_write_potions(FILE *savef) {
    int i;

    for(i = 0; i < MAXPOTIONS; i++) {
        rs_write_string_index(savef, rainbow, NCOLORS, p_colors[i]);
        rs_write_boolean(savef, p_know[i]);
        rs_write_string(savef, p_guess[i]);
    }

    return (WRITESTAT);
}

int rs_read_rings(int inf) {
    int i;

    for(i = 0; i < MAXRINGS; i++) {
        rs_read_string_index(inf, stones, NSTONES, &r_stones[i]);
        rs_read_boolean(inf, &r_know[i]);
        rs_read_new_string(inf, &r_guess[i]);
    }

    return (READSTAT);
}

int rs_write_rings(FILE *savef) {
    int i;

    for(i = 0; i < MAXRINGS; i++) {
        rs_write_string_index(savef, stones, NSTONES, r_stones[i]);
        rs_write_boolean(savef, r_know[i]);
        rs_write_string(savef, r_guess[i]);
    }

    return (WRITESTAT);
}

int rs_read_misc(int inf) {
    int i;

    for(i = 0; i < MAXMM; i++) {
        rs_read_boolean(inf, &m_know[i]);
        rs_read_new_string(inf, &m_guess[i]);
    }

    return (READSTAT);
}

int rs_write_misc(FILE *savef) {
    int i;

    for(i = 0; i < MAXMM; i++) {
        rs_write_boolean(savef, m_know[i]);
        rs_write_string(savef, m_guess[i]);
    }

    return (WRITESTAT);
}

int rs_write_sticks(FILE *savef) {
    int i;

    for(i = 0; i < MAXSTICKS; i++) {
        if(strcmp(ws_type[i], "staff") == 0) {
            rs_write_int(savef, 0);
            rs_write_string_index(savef, wood, NWOOD, ws_made[i]);
        } else {
            rs_write_int(savef, 1);
            rs_write_string_index(savef, metal, NMETAL, ws_made[i]);
        }
        rs_write_boolean(savef, ws_know[i]);
        rs_write_string(savef, ws_guess[i]);
    }

    return (WRITESTAT);
}

int rs_read_sticks(int inf) {
    int i = 0, list = 0;

    for(i = 0; i < MAXSTICKS; i++) {
        rs_read_int(inf, &list);
        if(list == 0) {
            rs_read_string_index(inf, wood, NWOOD, &ws_made[i]);
            ws_type[i] = "staff";
        } else {
            rs_read_string_index(inf, metal, NMETAL, &ws_made[i]);
            ws_type[i] = "wand";
        }
        rs_read_boolean(inf, &ws_know[i]);
        rs_read_new_string(inf, &ws_guess[i]);
    }

    return (READSTAT);
}

int rs_read_string(int inf, char *s, int max) {
    int len = 0;

    if(rs_read_int(inf, &len) != FALSE) {
        if(len > max) {
            printf("String too long to restore. %d > %d\n", len, max);
            printf("Sorry, invalid save game format\n");
            format_error = TRUE;
        }

        rs_read(inf, s, (size_t)len);
    }

    return (READSTAT);
}

int rs_read_new_string(int inf, char **s) {
    int len = 0;
    char *buf = nullptr;

    if(rs_read_int(inf, &len) != 0) {
        if(len == 0) {
            *s = nullptr;
        } else {
            buf = (char *)malloc((size_t)len);

            if(buf == nullptr) {
                read_error = TRUE;
            } else {
                rs_read(inf, buf, (size_t)len);
                *s = buf;
            }
        }
    }

    return (READSTAT);
}

int rs_write_strings(FILE *savef, char *s[], int count) {
    int len = 0;
    int n = 0;

    rs_write_int(savef, count);

    for(n = 0; n < count; n++) {
        len = (s[n] == nullptr) ? 0L : (int)strlen(s[n]) + 1;
        rs_write_int(savef, len);
        rs_write(savef, s[n], (size_t)len);
        DBG(("%s", s[n]));
    }

    return (WRITESTAT);
}

int rs_write_words(FILE *savef, words *w, int count) {
    int n = 0;

    rs_write_int(savef, count);

    for(n = 0; n < count; n++) {
        rs_write(savef, w[n].w_string, sizeof(w[n].w_string));
        DBG(("%s", w[n].w_string));
    }

    return (WRITESTAT);
}

int rs_read_words(int inf, words *w, int count) {
    int n = 0;
    int value = 0;

    rs_read_int(inf, &value);

    if(value != count) {
        printf("Incorrect number of words in block. %d != %d.", value, count);
        printf("Sorry, invalid save game format");
        format_error = TRUE;
    } else {
        for(n = 0; n < count; n++) {
            rs_read(inf, w[n].w_string, sizeof(w[n].w_string));
        }
    }

    return (READSTAT);
}

int rs_read_new_strings(int inf, char **s, int count) {
    int len = 0;
    int n = 0;
    int value = 0;

    if(rs_read_int(inf, &value) != 0) {
        if(value != count) {
            printf("Incorrect number of strings in block. %d > %d.", value, count);
            printf("Sorry, invalid save game format");
            format_error = TRUE;
        } else {
            for(n = 0; n < value; n++) {
                rs_read_int(inf, &len);

                if(len == 0) {
                    s[n] = nullptr;
                } else {
                    s[n] = (char *)malloc((size_t)len);
                    rs_read(inf, s[n], (size_t)len);
                }
            }
        }
    }

    return (READSTAT);
}

int rs_write_coord(FILE *savef, coord *c) {
    DBG(("X ="));
    rs_write_int(savef, c->x);
    DBG(("Y ="));
    rs_write_int(savef, c->y);

    return (WRITESTAT);
}

int rs_read_coord(int inf, coord *c) {
    rs_read_int(inf, &c->x);
    rs_read_int(inf, &c->y);

    return (READSTAT);
}

int rs_write_daemons(FILE *savef, delayed_action *d_list, int count) {
    int i = 0;

    DBG(("Daemons\n"));
    rs_write_uint(savef, RSXR_DAEMONS);
    rs_write_int(savef, count);

    for(i = 0; i < count; i++) {
        rs_write_int(savef, d_list[i].d_type);
        rs_write_int(savef, (int)d_list[i].d_id);

        if(d_list[i].d_id == d_doctor) {
            rs_write_int(savef, 1);
        } else if(d_list[i].d_id == d_eat_gold) {
            int index;
            index = find_list_ptr(player.t_pack, d_list[i].d_arg.vp);
            rs_write_int(savef, index);
        } else if(d_list[i].d_id == d_changeclass) {
            rs_write_int(savef, (int)d_list[i].d_arg.i);
        } else if(d_list[i].d_id == d_cloak_charge) {
            int index;
            index = find_list_ptr(player.t_pack, d_list[i].d_arg.vp);
            rs_write_int(savef, index);
        } else {
            rs_write_int(savef, (int)d_list[i].d_arg.i);
        }

        rs_write_int(savef, d_list[i].d_time);
    }

    return (WRITESTAT);
}

int rs_read_daemons(int inf, delayed_action *d_list, int count) {
    int i = 0;
    int func = 0;
    int value = 0;
    unsigned int id = 0;
    int dummy = 0;

    if(d_list == nullptr) {
        printf("HELP THERE ARE NO DAEMONS\n");
    }

    if(rs_read_uint(inf, &id) != 0) {
        if(id != RSXR_DAEMONS) {
            printf("Invalid id. %x != %x(RSXR_DAEMONS)\n", id, RSXR_DAEMONS);
            printf("Sorry, invalid save game format");
            format_error = TRUE;
        } else if(rs_read_int(inf, &value) != 0) {
            if(value > count) {
                printf("Incorrect number of daemons in block. %d > %d.", value, count);
                printf("Sorry, invalid save game format");
                format_error = TRUE;
            } else {
                for(i = 0; i < value; i++) {
                    func = 0;
                    rs_read_int(inf, &d_list[i].d_type);
                    rs_read_int(inf, &func);
                    d_list[i].d_id = (enum daemon_id)func;

                    if(d_list[i].d_id == d_doctor) {
                        rs_read_int(inf, &dummy);
                        d_list[i].d_arg.vp = (void *)&player;
                    } else if(d_list[i].d_id == d_eat_gold) {
                        rs_read_int(inf, &dummy);
                        d_list[i].d_arg.vp = get_list_item(player.t_pack, dummy);
                        if(d_list[i].d_arg.vp == nullptr) {
                            d_list[i].d_type = 0;
                        }
                    } else if(d_list[i].d_id == d_changeclass) {
                        rs_read_int(inf, (int *)&d_list[i].d_arg.i);
                    } else if(d_list[i].d_id == d_cloak_charge) {
                        rs_read_int(inf, &dummy);
                        d_list[i].d_arg.vp = get_list_item(player.t_pack, dummy);
                        if(d_list[i].d_arg.vp == nullptr) {
                            d_list[i].d_type = 0;
                        }
                    } else {
                        rs_read_int(inf, (int *)&d_list[i].d_arg.i);
                    }

                    rs_read_int(inf, &d_list[i].d_time);

                    if(d_list[i].d_id == d_none) {
                        d_list[i].d_time = 0;
                        d_list[i].d_arg.vp = nullptr;
                        d_list[i].d_type = 0;
                    }
                }
            }
        }
    }

    return (READSTAT);
}

int rs_write_rooms(FILE *savef, room r[], int count) {
    int n = 0, i = -1;
    linked_list *l;

    DBG(("Rooms\n"));
    rs_write_uint(savef, RSXR_ROOMS);
    rs_write_int(savef, count);

    for(n = 0; n < count; n++) {
        rs_write_coord(savef, &r[n].r_pos);
        rs_write_coord(savef, &r[n].r_max);
        rs_write_long(savef, r[n].r_flags);
        rs_write_coord_list(savef, r[n].r_exit);

        l = r[n].r_fires;
        i = list_size(l);

        rs_write_int(savef, i);

        if(i > 0) {
            while(l != nullptr) {
                i = find_list_ptr(mlist, l->l_data);
                rs_write_int(savef, i);
                l = l->l_next;
            }
        }
    }
    return (WRITESTAT);
}

int rs_read_rooms(int inf, room *r, int count) {
    int value = 0, n = 0, i = 0, index = 0;
    unsigned int id = 0;
    linked_list *fires = nullptr, *item = nullptr;

    if(rs_read_uint(inf, &id) != 0) {
        if(id != RSXR_ROOMS) {
            printf("Invalid id. %x != %x(RSXR_ROOMS)\n", id, RSXR_ROOMS);
            printf("Sorry, invalid save game format");
            format_error = TRUE;
        } else if(rs_read_int(inf, &value) != 0) {
            if(value != count) {
                printf("Incorrect number of rooms in block. %d > %d.", value, count);
                printf("Sorry, invalid save game format");
                format_error = TRUE;
            } else {
                for(n = 0; n < value; n++) {
                    rs_read_coord(inf, &r[n].r_pos);
                    rs_read_coord(inf, &r[n].r_max);
                    rs_read_long(inf, &r[n].r_flags);
                    rs_read_coord_list(inf, &r[n].r_exit);

                    rs_read_int(inf, &i);
                    fires = nullptr;
                    while(i > 0) {
                        rs_read_int(inf, &index);

                        if(index >= 0) {
                            void *data;
                            data = get_list_item(mlist, index);
                            item = creat_item();
                            item->l_data = (char *)data;
                            if(fires == nullptr) {
                                fires = item;
                            } else {
                                attach(fires, item);
                            }
                        }
                        i--;
                    }
                    r[n].r_fires = fires;
                }
            }
        }
    }

    return (READSTAT);
}

int rs_write_object(FILE *savef, object *o) {
    rs_write_uint(savef, RSXR_OBJECT);
    rs_write_int(savef, o->o_type);
    rs_write_coord(savef, &o->o_pos);
    rs_write_char(savef, o->o_launch);
    rs_write(savef, o->o_damage, sizeof(o->o_damage));
    rs_write(savef, o->o_hurldmg, sizeof(o->o_hurldmg));
    rs_write_int(savef, o->o_count);
    rs_write_int(savef, o->o_which);
    rs_write_int(savef, o->o_hplus);
    rs_write_int(savef, o->o_dplus);
    rs_write_int(savef, o->o_ac);
    rs_write_long(savef, o->o_flags);
    rs_write_int(savef, o->o_group);
    rs_write_int(savef, o->o_weight);
    rs_write(savef, o->o_mark, MARKLEN);

    DBG(("Object\n"));
    DBG(("    SaveID  : %X\n", RSXR_OBJECT));
    DBG(("    Type    : %d\n", o->o_type));
    DBG(("    Pos     : %d %d\n", o->o_pos.x, o->o_pos.y));
    DBG(("    Launch  : %c\n", o->o_launch));
    DBG(("    Damage  : %s\n", o->o_damage));
    DBG(("    Hurl    : %s\n", o->o_hurldmg));
    DBG(("    Count   : %d\n", o->o_count));
    DBG(("    Which   : %d\n", o->o_which));
    DBG(("    HPlus   : %d\n", o->o_hplus));
    DBG(("    DPlus   : %d\n", o->o_dplus));
    DBG(("    AC      : %d\n", o->o_ac));
    DBG(("    Flags   : %X\n", o->o_flags));
    DBG(("    Group   : %d\n", o->o_group));
    DBG(("    Weight  : %d\n", o->o_weight));
    DBG(("    Mark    : %s\n", o->o_mark));
    if(o->contents == nullptr) {
        DBG(("    Contents: None\n"));
    } else {
        DBG(("    CONTENTS\n"));
    }

    rs_write_object_list(savef, o->contents);

    if(o->contents != nullptr) {
        DBG(("    END_CONTENTS\n"));
    }

    return (WRITESTAT);
}

int rs_read_object(int inf, object *o) {
    unsigned int id;

    if(rs_read_uint(inf, &id) != 0) {
        if(id != RSXR_OBJECT) {
            printf("Invalid id. %x != %x(RSXR_OBJECT)\n", id, RSXR_OBJECT);
            printf("Sorry, invalid save game format");
            format_error = TRUE;
        } else {
            rs_read_int(inf, &o->o_type);
            rs_read_coord(inf, &o->o_pos);
            rs_read_char(inf, &o->o_launch);
            rs_read(inf, o->o_damage, sizeof(o->o_damage));
            rs_read(inf, o->o_hurldmg, sizeof(o->o_hurldmg));
            rs_read_int(inf, &o->o_count);
            rs_read_int(inf, &o->o_which);
            rs_read_int(inf, &o->o_hplus);
            rs_read_int(inf, &o->o_dplus);
            rs_read_int(inf, &o->o_ac);
            rs_read_long(inf, &o->o_flags);
            rs_read_int(inf, &o->o_group);
            rs_read_int(inf, &o->o_weight);
            rs_read(inf, o->o_mark, MARKLEN);
            rs_read_object_list(inf, &o->contents);
        }
    }

    return (READSTAT);
}

int rs_write_stats(FILE *savef, stats *s) {
    DBG(("Stats\n"));
    rs_write_uint(savef, RSXR_STATS);

    rs_write_short(savef, s->s_str);
    rs_write_short(savef, s->s_intel);
    rs_write_short(savef, s->s_wisdom);
    rs_write_short(savef, s->s_dext);
    rs_write_short(savef, s->s_const);
    rs_write_short(savef, s->s_charisma);
    rs_write_int(savef, (int)s->s_exp);
    rs_write_int(savef, s->s_lvladj);
    rs_write_int(savef, s->s_lvl);
    rs_write_int(savef, s->s_arm);
    rs_write_int(savef, s->s_hpt);
    rs_write_int(savef, s->s_pack);
    rs_write_int(savef, s->s_carry);
    rs_write(savef, s->s_dmg, sizeof(s->s_dmg));

    return (WRITESTAT);
}

int rs_read_stats(int inf, stats *s) {
    unsigned int id;

    if(rs_read_uint(inf, &id) != 0) {
        if(id != RSXR_STATS) {
            printf("Invalid id. %x != %x(RSXR_STATS)\n", id, RSXR_STATS);
            printf("Sorry, invalid save game format");
            format_error = TRUE;
        } else {
            rs_read_short(inf, &s->s_str);
            rs_read_short(inf, &s->s_intel);
            rs_read_short(inf, &s->s_wisdom);
            rs_read_short(inf, &s->s_dext);
            rs_read_short(inf, &s->s_const);
            rs_read_short(inf, &s->s_charisma);
            rs_read_int(inf, (int *)&s->s_exp);
            rs_read_int(inf, &s->s_lvladj);
            rs_read_int(inf, &s->s_lvl);
            rs_read_int(inf, &s->s_arm);
            rs_read_int(inf, &s->s_hpt);
            rs_read_int(inf, &s->s_pack);
            rs_read_int(inf, &s->s_carry);
            rs_read(inf, s->s_dmg, sizeof(s->s_dmg));
        }
    }

    return (READSTAT);
}

int rs_write_mstats(FILE *savef, mstats *s) {
    DBG(("M-Stats\n"));
    rs_write_uint(savef, RSXR_STATS);
    rs_write_short(savef, s->ms_str);
    /*printf("    Strength: %d\n",s->ms_str);*/
    rs_write_short(savef, s->ms_dex);
    rs_write_short(savef, s->ms_move);
    rs_write_ulong(savef, s->ms_exp);
    rs_write_short(savef, s->ms_lvl);
    rs_write_short(savef, s->ms_arm);
    rs_write(savef, s->ms_hpt, sizeof(s->ms_hpt));
    rs_write(savef, s->ms_dmg, sizeof(s->ms_dmg));
    /*printf("    Damage: %s\n",s->ms_dmg);*/

    return (WRITESTAT);
}

int rs_read_mstats(int inf, mstats *s) {
    unsigned int id;

    if(rs_read_uint(inf, &id) != 0) {
        if(id != RSXR_STATS) {
            printf("Invalid id. %x != %x(RSXR_STATS)\n", id, RSXR_STATS);
            printf("Sorry, invalid save game format");
            format_error = TRUE;
        } else {
            rs_read_short(inf, &s->ms_str);
            /*printf("    Strength: %d\n",s->ms_str);*/
            rs_read_short(inf, &s->ms_dex);
            /*printf("    Dexterity: %d\n",s->ms_dex);*/
            rs_read_short(inf, &s->ms_move);
            /*printf("    Moves: %d\n",s->ms_move);*/
            rs_read_uint(inf, &s->ms_exp);
            /*printf("    Experience: %d\n",s->ms_exp);*/
            rs_read_short(inf, &s->ms_lvl);
            /*printf("    Level: %d\n",s->ms_lvl);*/
            rs_read_short(inf, &s->ms_arm);
            /*printf("    Armor: %d\n",s->ms_arm);*/
            rs_read(inf, s->ms_hpt, sizeof(s->ms_hpt));
            /*printf("    HP: %s\n",s->ms_hpt);*/
            rs_read(inf, s->ms_dmg, sizeof(s->ms_dmg));
            /*printf("    Damage: %s\n",s->ms_dmg);*/
        }
    }

    return (READSTAT);
}

int rs_write_init_weps(FILE *savef, init_weps *w, int count) {
    int i;

    DBG(("Init-Weps\n"));
    rs_write_uint(savef, RSXR_IWEAPS);
    rs_write_int(savef, count);

    for(i = 0; i < count; i++) {
        rs_write(savef, w[i].w_name, sizeof(w[i].w_name));
        rs_write(savef, w[i].w_dam, sizeof(w[i].w_dam));
        rs_write(savef, w[i].w_hrl, sizeof(w[i].w_hrl));
        rs_write_char(savef, w[i].w_launch);
        rs_write_int(savef, w[i].w_flags);
        rs_write_int(savef, w[i].w_rate);
        rs_write_int(savef, w[i].w_wght);
        rs_write_int(savef, w[i].w_worth);
    }
    return (WRITESTAT);
}

int rs_read_init_weps(int inf, init_weps *w, int count) {
    int id, value, i;

    rs_read_int(inf, &id);
    rs_read_int(inf, &value);

    if(value != count) {
        printf("Incorrect number of init_weps in block. %d != %d.", value, count);
        printf("Sorry, invalid save game format");
        format_error = TRUE;
    } else {
        for(i = 0; i < count; i++) {
            rs_read(inf, w[i].w_name, sizeof(w[i].w_name));
            rs_read(inf, w[i].w_dam, sizeof(w[i].w_dam));
            rs_read(inf, w[i].w_hrl, sizeof(w[i].w_hrl));
            rs_read_char(inf, &w[i].w_launch);
            rs_read_int(inf, &w[i].w_flags);
            rs_read_int(inf, &w[i].w_rate);
            rs_read_int(inf, &w[i].w_wght);
            rs_read_int(inf, &w[i].w_worth);
        }
    }
    return (READSTAT);
}

int rs_write_init_armor(FILE *savef, init_armor *a, int count) {
    int i;
    DBG(("Init-Armor\n"));
    rs_write_uint(savef, RSXR_IARMOR);
    rs_write_int(savef, count);
    for(i = 0; i < count; i++) {
        rs_write(savef, a[i].a_name, sizeof(a[i].a_name));
        rs_write_int(savef, a[i].a_prob);
        rs_write_int(savef, a[i].a_class);
        rs_write_int(savef, a[i].a_worth);
        rs_write_int(savef, a[i].a_wght);
    }
    return (WRITESTAT);
}

int rs_read_init_armor(int inf, init_armor *a, int count) {
    int id, value, i;

    rs_read_int(inf, &id);
    rs_read_int(inf, &value);

    for(i = 0; i < count; i++) {
        rs_read(inf, a[i].a_name, sizeof(a[i].a_name));
        rs_read_int(inf, &a[i].a_prob);
        rs_read_int(inf, &a[i].a_class);
        rs_read_int(inf, &a[i].a_worth);
        rs_read_int(inf, &a[i].a_wght);
    }

    return (READSTAT);
}

int rs_write_spells(FILE *savef, spells *s, int count) {
    int i;
    DBG(("Spells\n"));
    rs_write_uint(savef, RSXR_SPELLS);
    rs_write_int(savef, count);
    for(i = 0; i < count; i++) {
        rs_write_short(savef, s[i].s_which);
        rs_write_short(savef, s[i].s_cost);
        rs_write_short(savef, s[i].s_type);
        rs_write_int(savef, s[i].s_flag);
    }
    return (WRITESTAT);
}

int rs_read_spells(int inf, spells *s, int count) {
    int id, value, i;

    rs_read_int(inf, &id);
    rs_read_int(inf, &value);

    for(i = 0; i < count; i++) {
        rs_read_short(inf, &s[i].s_which);
        rs_read_short(inf, &s[i].s_cost);
        rs_read_short(inf, &s[i].s_type);
        rs_read_int(inf, &s[i].s_flag);
    }
    return (READSTAT);
}

int rs_write_item_list(FILE *savef, item_list *i) {
    DBG(("Item List\n"));
    rs_write_uint(savef, RSXR_ILIST);
    rs_write_char(savef, (char)i->item_ch);
    rs_write(savef, i->item_desc, sizeof(i->item_desc));
    return (WRITESTAT);
}

int rs_read_item_list(int inf, item_list *i) {
    int id;

    rs_read_int(inf, &id);

    rs_read_uchar(inf, &i->item_ch);
    rs_read(inf, i->item_desc, sizeof(i->item_desc));
    return (READSTAT);
}

int rs_write_h_list(FILE *savef, h_list *h) {
    DBG(("H List\n"));
    rs_write_uint(savef, RSXR_HLIST);
    rs_write_char(savef, h->h_ch);
    rs_write(savef, h->h_desc, sizeof(h->h_desc));
    return (WRITESTAT);
}

int rs_read_h_list(int inf, h_list *h) {
    int id;

    rs_read_int(inf, &id);

    rs_read_char(inf, &h->h_ch);
    rs_read(inf, h->h_desc, sizeof(h->h_desc));
    return (READSTAT);
}

int rs_write_death_types(FILE *savef, death_type *d, int count) {
    int i;

    DBG(("Death Types\n"));
    rs_write_uint(savef, RSXR_DEATHTYPE);
    rs_write_int(savef, count);

    for(i = 0; i < count; i++) {
        rs_write_int(savef, d[i].reason);
        rs_write(savef, d[i].name, sizeof(d[i].name));
    }
    return (WRITESTAT);
}
int rs_read_death_types(int inf, death_type *d, int count) {
    int id, value, i;

    rs_read_int(inf, &id);
    rs_read_int(inf, &value);
    if(value != count) {
        printf("Incorrect number of death_types in block. %d > %d.", value, count);
        printf("Sorry, invalid save game format");
        format_error = TRUE;
    } else {
        for(i = 0; i < count; i++) {
            rs_read_int(inf, &d[i].reason);
            rs_read(inf, d[i].name, sizeof(d[i].name));
        }
    }
    return (READSTAT);
}

int rs_write_character_types(FILE *savef, character_types *c, int count) {
    int i;

    DBG(("Character Types\n"));
    rs_write_uint(savef, RSXR_CTYPES);
    rs_write_int(savef, count);

    for(i = 0; i < count; i++) {
        rs_write(savef, c[i].name, sizeof(c[i].name));
        rs_write_long(savef, c[i].start_exp);
        rs_write_long(savef, c[i].cap);
        rs_write_int(savef, c[i].hit_pts);
        rs_write_int(savef, c[i].base);
        rs_write_int(savef, c[i].max_lvl);
        rs_write_int(savef, c[i].factor);
        rs_write_int(savef, c[i].offset);
        rs_write_int(savef, c[i].range);
    }
    return (WRITESTAT);
}

int rs_read_character_types(int inf, character_types *c, int count) {
    int id, value, i;

    rs_read_int(inf, &id);
    rs_read_int(inf, &value);

    if(value != count) {
        printf("Incorrect number of character types in block. %d > %d.", value, count);
        printf("Sorry, invalid save game format");
        format_error = TRUE;
    } else {
        for(i = 0; i < count; i++) {
            rs_read(inf, c[i].name, sizeof(c[i].name));
            rs_read_long(inf, &c[i].start_exp);
            rs_read_long(inf, &c[i].cap);
            rs_read_int(inf, &c[i].hit_pts);
            rs_read_int(inf, &c[i].base);
            rs_read_int(inf, &c[i].max_lvl);
            rs_read_int(inf, &c[i].factor);
            rs_read_int(inf, &c[i].offset);
            rs_read_int(inf, &c[i].range);
        }
    }
    return (READSTAT);
}

int rs_write_traps(FILE *savef, trap *trap, int count) {
    int n;

    DBG(("Traps\n"));
    rs_write_uint(savef, RSXR_TRAP);
    rs_write_int(savef, count);

    for(n = 0; n < count; n++) {
        rs_write_char(savef, (char)trap[n].tr_type);
        rs_write_char(savef, (char)trap[n].tr_show);
        rs_write_coord(savef, &trap[n].tr_pos);
        rs_write_long(savef, trap[n].tr_flags);
    }

    return (WRITESTAT);
}

int rs_read_traps(int inf, trap *trap, int count) {
    unsigned int id = 0;
    int value = 0, n = 0;

    if(rs_read_uint(inf, &id) != 0) {
        if(id != RSXR_TRAP) {
            printf("Invalid id. %x != %x(RSXR_TRAP)\n", id, RSXR_TRAP);
            printf("Sorry, invalid save game format");
            format_error = TRUE;
        } else if(rs_read_int(inf, &value) != 0) {
            if(value != count) {
                printf("Incorrect number of traps in block. %d > %d.", value, count);
                printf("Sorry, invalid save game format\n");
                format_error = TRUE;
            } else {
                for(n = 0; n < value; n++) {
                    rs_read_uchar(inf, &trap[n].tr_type);
                    rs_read_uchar(inf, &trap[n].tr_show);
                    rs_read_coord(inf, &trap[n].tr_pos);
                    rs_read_long(inf, &trap[n].tr_flags);
                }
            }
        } else {
            format_error = TRUE;
        }
    }

    return (READSTAT);
}

int rs_write_monsters(FILE *savef, monster *m, int count) {
    int n;

    DBG(("Monsters\n"));
    rs_write_uint(savef, RSXR_MONSTERS);
    rs_write_int(savef, count);

    for(n = 0; n < count; n++) {
        rs_write(savef, m[n].m_name, sizeof(m[n].m_name));
        /*printf("Monster: %s/%d/%d\n",m[n].m_name,sizeof(m[n].m_name),strlen(m[n].m_name));*/
        rs_write_short(savef, m[n].m_carry);
        rs_write_boolean(savef, m[n].m_normal);
        rs_write_boolean(savef, m[n].m_wander);
        rs_write_char(savef, m[n].m_appear);
        rs_write(savef, m[n].m_intel, sizeof(m[n].m_intel));
        rs_write_longs(savef, m[n].m_flags, MAXFLAGS);
        rs_write(savef, m[n].m_typesum, sizeof(m[n].m_typesum));
        rs_write_short(savef, m[n].m_numsum);
        rs_write_short(savef, m[n].m_add_exp);

        rs_write_mstats(savef, &m[n].m_stats);
    }

    return (WRITESTAT);
}

int rs_read_monsters(int inf, monster *m, int count) {
    unsigned int id = 0;
    int value = 0, n = 0;
    char buffer[1024];

    if(rs_read_uint(inf, &id) != 0) {
        if(id != RSXR_MONSTERS) {
            printf("Invalid id. %x != %x(RSXR_MONSTERS)\n", id, RSXR_MONSTERS);
            printf("Sorry, invalid save game format");
            format_error = TRUE;
        } else if(rs_read_int(inf, &value) != 0) {
            if(value != count) {
                printf("Incorrect number of monsters in block. %d != %d.", value, count);
                printf("Sorry, invalid save game format\n");
                format_error = TRUE;

            } else {
                for(n = 0; n < value; n++) {
                    rs_read(inf, buffer, sizeof(m[n].m_name));
                    assert(strcmp(buffer, m[n].m_name) == 0);
                    /*printf("Monster: %s\n",m[n].m_name);*/
                    rs_read_short(inf, &m[n].m_carry);
                    /*printf("    Carry: %d\n",m[n].m_carry); */
                    rs_read_boolean(inf, &m[n].m_normal);
                    /*printf("    Normal: %d\n",m[n].m_normal);*/
                    rs_read_boolean(inf, &m[n].m_wander);
                    /*printf("    Wander: %d\n",m[n].m_wander);*/
                    rs_read_char(inf, &m[n].m_appear);
                    /*printf("    Appears: %c\n",m[n].m_appear);*/
                    rs_read(inf, m[n].m_intel, sizeof(m[n].m_intel));
                    /*printf("    Intelligence: %s\n",m[n].m_intel);*/
                    rs_read_longs(inf, m[n].m_flags, MAXFLAGS);
                    /*printf("    Flags: %X\n",m[n].m_flags);*/
                    rs_read(inf, m[n].m_typesum, sizeof(m[n].m_typesum));
                    /*printf("    Summons: %s\n",m[n].m_typesum);*/
                    rs_read_short(inf, &m[n].m_numsum);
                    /*printf("    # Summons: %d\n",m[n].m_numsum);*/
                    rs_read_short(inf, &m[n].m_add_exp);
                    /*printf("    Experience: %d\n",m[n].m_add_exp);*/
                    rs_read_mstats(inf, &m[n].m_stats);
                }
            }
        } else {
            format_error = TRUE;
        }
    }

    return (READSTAT);
}

/*****************************************************************************/

int rs_write_coord_list(FILE *savef, linked_list *l) {
    DBG(("Coordinate List\n"));
    rs_write_uint(savef, RSXR_COORDLIST);
    rs_write_int(savef, list_size(l));

    while(l != nullptr) {
        rs_write_coord(savef, (coord *)l->l_data);
        l = l->l_next;
    }

    return (WRITESTAT);
}

int rs_read_coord_list(int inf, linked_list **list) {
    unsigned int id;
    int i, cnt;
    linked_list *l = nullptr, *previous = nullptr, *head = nullptr;

    if(rs_read_uint(inf, &id) != 0) {
        if(id != RSXR_COORDLIST) {
            printf("Invalid id. %x != %x(RSXR_COORDLIST)\n", id, RSXR_COORDLIST);
            printf("Sorry, invalid save game format");
            format_error = TRUE;
        } else if(rs_read_int(inf, &cnt) != 0) {
            for(i = 0; i < cnt; i++) {
                l = new_item(sizeof(coord));
                l->l_prev = previous;
                if(previous != nullptr) {
                    previous->l_next = l;
                }
                rs_read_coord(inf, (coord *)l->l_data);
                if(previous == nullptr) {
                    head = l;
                }
                previous = l;
            }

            if(l != nullptr) {
                l->l_next = nullptr;
            }

            *list = head;
        } else {
            format_error = TRUE;
        }
    } else {
        format_error = TRUE;
    }

    return (READSTAT);
}

int rs_write_object_list(FILE *savef, linked_list *l) {
    DBG(("Object List\n"));
    rs_write_uint(savef, RSXR_OBJECTLIST);
    rs_write_int(savef, list_size(l));

    while(l != nullptr) {
        rs_write_object(savef, (object *)l->l_data);
        l = l->l_next;
    }

    return (WRITESTAT);
}

int rs_read_object_list(int inf, linked_list **list) {
    int id;
    int i, cnt;
    linked_list *l = nullptr, *previous = nullptr, *head = nullptr;

    if(rs_read_int(inf, &id) != 0) {
        if(rs_read_int(inf, &cnt) != 0) {
            for(i = 0; i < cnt; i++) {
                l = new_item(sizeof(object));
                memset(l->l_data, 0, sizeof(object));
                l->l_prev = previous;
                if(previous != nullptr) {
                    previous->l_next = l;
                }
                rs_read_object(inf, (object *)l->l_data);
                if(previous == nullptr) {
                    head = l;
                }
                previous = l;
            }

            if(l != nullptr) {
                l->l_next = nullptr;
            }

            *list = head;
        } else {
            format_error = TRUE;
        }
    } else {
        format_error = TRUE;
    }

    return (READSTAT);
}

int find_thing_coord(linked_list *monlist, coord *c) {
    linked_list *mitem;
    thing *tp;
    int i = 0;

    for(mitem = monlist; mitem != nullptr; mitem = mitem->l_next) {
        tp = THINGPTR(mitem);
        if(c == &tp->t_pos) {
            return (i);
        }
        i++;
    }

    return (-1);
}

int find_object_coord(linked_list *objlist, coord *c) {
    linked_list *oitem;
    object *obj;
    int i = 0;

    for(oitem = objlist; oitem != nullptr; oitem = oitem->l_next) {
        obj = OBJPTR(oitem);
        if(c == &obj->o_pos) {
            return (i);
        }
        i++;
    }

    return (-1);
}

int rs_write_thing(FILE *savef, thing *t) {
    int i = -1;

    DBG(("Thing\n"));
    rs_write_uint(savef, RSXR_THING);
    rs_write_boolean(savef, t->t_wasshot);
    rs_write_char(savef, (char)t->t_type);
    rs_write_char(savef, (char)t->t_disguise);
    rs_write_char(savef, (char)t->t_oldch);
    rs_write_short(savef, t->t_ctype);
    rs_write_short(savef, t->t_index);
    rs_write_short(savef, t->t_no_move);
    rs_write_short(savef, t->t_quiet);
    rs_write_short(savef, t->t_movement);
    rs_write_short(savef, t->t_action);
    rs_write_short(savef, t->t_artifact);
    rs_write_short(savef, t->t_wand);
    rs_write_short(savef, t->t_summon);
    rs_write_short(savef, t->t_cast);
    rs_write_short(savef, t->t_breathe);

    rs_write_string(savef, t->t_name);
    rs_write_coord(savef, &t->t_doorgoal);

    if(t->t_dest == &hero) {
        rs_write_int(savef, 0);
        rs_write_int(savef, 1);
    } else if(t->t_dest != nullptr) {
        i = find_thing_coord(mlist, t->t_dest);

        if(i >= 0) {
            rs_write_int(savef, 1);
            rs_write_int(savef, i);
        } else {
            i = find_object_coord(lvl_obj, t->t_dest);

            if(i >= 0) {
                rs_write_int(savef, 2);
                rs_write_int(savef, i);
            } else {
                rs_write_int(savef, 0);
                rs_write_int(savef, 1); /* chase the hero anyway */
            }
        }
    } else {
        rs_write_int(savef, 0);
        rs_write_int(savef, 0);
    }

    rs_write_coord(savef, &t->t_pos);
    rs_write_coord(savef, &t->t_oldpos);
    rs_write_coord(savef, &t->t_newpos);
    rs_write_ulongs(savef, t->t_flags, 16);

    DBG(("Thing\n"));
    DBG(("    SaveID  : %X\n", RSXR_THING));
    DBG(("    Name    : %s\n", t->t_name));
    DBG(("    WasShot : %d\n", t->t_wasshot));
    DBG(("    Type    : %c(%d)\n", t->t_type, t->t_type));
    DBG(("    Disguise: %c(%d)\n", t->t_disguise, t->t_disguise));
    DBG(("    OldCh   : %c(%d)\n", t->t_oldch, t->t_oldch));
    DBG(("    CType   : %d\n", t->t_ctype));
    DBG(("    Index   : %d\n", t->t_index));
    DBG(("    NoMove  : %d\n", t->t_no_move));
    DBG(("    Quiet   : %d\n", t->t_quiet));
    DBG(("    Movement: %d\n", t->t_movement));
    DBG(("    Action  : %d\n", t->t_action));
    DBG(("    Artifact: %d\n", t->t_artifact));
    DBG(("    Wand    : %d\n", t->t_wand));
    DBG(("    Summon  : %d\n", t->t_summon));
    DBG(("    Cast    : %d\n", t->t_cast));
    DBG(("    Breathe : %d\n", t->t_breathe));
    DBG(("    DoorGoal: %d %d\n", t->t_doorgoal.x, t->t_doorgoal.y));
    if(t->t_dest) {
        DBG(("    Dest    : %d %d\n", t->t_dest->x, t->t_dest->y));
    } else {
        DBG(("    Dest    : None\n"));
    }
    DBG(("    Pos     : %d %d\n", t->t_pos.x, t->t_pos.y));
    DBG(("    OldPos  : %d %d\n", t->t_oldpos.x, t->t_oldpos.y));
    DBG(("    NewPos  : %d %d\n", t->t_newpos.x, t->t_newpos.y));
    DBG(("    Flags   : "));
    {
        int i;
        for(i = 0; i < 16; i++) {
            DBG(("%d ", t->t_flags[i]));
        }
        DBG(("\n"));
    }

    rs_write_object_list(savef, t->t_pack);
    i = -1;
    if(t->t_using != nullptr) {
        i = find_list_ptr(t->t_pack, t->t_using->l_data);
    }
    rs_write_int(savef, i);
    rs_write_stats(savef, &t->t_stats);
    rs_write_stats(savef, &t->maxstats);

    return (WRITESTAT);
}

void rs_fix_thing(thing *t) {
    linked_list *item;
    thing *tp;

    if(t->t_reserved < 0) {
        return;
    }

    item = (linked_list *)get_list_item(mlist, t->t_reserved);

    if(item != nullptr) {
        tp = THINGPTR(item);
        t->t_dest = &tp->t_pos;
    }
}

int rs_read_thing(int inf, thing *t) {
    unsigned int id;
    int listid = 0, index = -1;
    linked_list *item;

    if(rs_read_uint(inf, &id) != 0) {
        if(id != RSXR_THING) {
            format_error = TRUE;
        } else {
            rs_read_boolean(inf, &t->t_wasshot);
            rs_read_uchar(inf, &t->t_type);
            rs_read_uchar(inf, &t->t_disguise);
            rs_read_uchar(inf, &t->t_oldch);
            rs_read_short(inf, &t->t_ctype);
            rs_read_short(inf, &t->t_index);
            rs_read_short(inf, &t->t_no_move);
            rs_read_short(inf, &t->t_quiet);
            rs_read_short(inf, &t->t_movement);
            rs_read_short(inf, &t->t_action);
            rs_read_short(inf, &t->t_artifact);
            rs_read_short(inf, &t->t_wand);
            rs_read_short(inf, &t->t_summon);
            rs_read_short(inf, &t->t_cast);
            rs_read_short(inf, &t->t_breathe);
            rs_read_new_string(inf, &t->t_name);
            rs_read_coord(inf, &t->t_doorgoal);

            rs_read_int(inf, &listid);
            rs_read_int(inf, &index);
            t->t_reserved = -1;
            if(listid == 0) {
                if(index == 1) {
                    t->t_dest = &hero;
                } else {
                    t->t_dest = nullptr;
                }
            } else if(listid == 1) {
                t->t_dest = nullptr;
                t->t_reserved = index;
            } else if(listid == 2) {
                object *obj;
                item = (linked_list *)get_list_item(lvl_obj, index);
                if(item != nullptr) {
                    obj = OBJPTR(item);
                    t->t_dest = &obj->o_pos;
                }
            } else {
                t->t_dest = nullptr;
            }

            rs_read_coord(inf, &t->t_pos);
            rs_read_coord(inf, &t->t_oldpos);
            rs_read_coord(inf, &t->t_newpos);
            rs_read_ulongs(inf, t->t_flags, 16);
            rs_read_object_list(inf, &t->t_pack);
            rs_read_int(inf, &index);
            t->t_using = (linked_list *)get_list_item(t->t_pack, index);
            rs_read_stats(inf, &t->t_stats);
            rs_read_stats(inf, &t->maxstats);
        }
    } else {
        format_error = TRUE;
    }

    return (READSTAT);
}

int find_list_ptr(linked_list *l, void *ptr) {
    int count = 0;

    while(l != nullptr) {
        if(l->l_data == ptr) {
            return (count);
        }

        l = l->l_next;
        count++;
    }

    return (-1);
}

int list_size(linked_list *l) {
    int count = 0;

    while(l != nullptr) {
        if(l->l_data == nullptr) {
            return (count);
        }

        count++;

        l = l->l_next;
    }

    return (count);
}

int rs_write_monster_list(FILE *savef, linked_list *l) {
    int cnt = 0;

    DBG(("Monster List\n"));
    rs_write_uint(savef, RSXR_MONSTERLIST);

    cnt = list_size(l);

    rs_write_int(savef, cnt);

    if(cnt < 1) {
        return (WRITESTAT);
    }

    while(l != nullptr) {
        rs_write_thing(savef, (thing *)l->l_data);
        l = l->l_next;
    }

    return (WRITESTAT);
}

int rs_fix_monster_list(linked_list *list) {
    linked_list *item;

    for(item = list; item != nullptr; item = item->l_next) {
        rs_fix_thing(THINGPTR(item));
    }

    return (READSTAT);
}

int rs_read_monster_list(int inf, linked_list **list) {
    unsigned int id;
    int i, cnt;
    linked_list *l = nullptr, *previous = nullptr, *head = nullptr;

    if(rs_read_uint(inf, &id) != 0) {
        if(id != RSXR_MONSTERLIST) {
            printf("Invalid id. %x != %x(RSXR_MONSTERLIST)\n", id, RSXR_MONSTERLIST);
            printf("Sorry, invalid save game format");
            format_error = TRUE;
        } else if(rs_read_int(inf, &cnt) != 0) {
            for(i = 0; i < cnt; i++) {
                l = new_item(sizeof(thing));
                l->l_prev = previous;
                if(previous != nullptr) {
                    previous->l_next = l;
                }
                rs_read_thing(inf, (thing *)l->l_data);
                if(previous == nullptr) {
                    head = l;
                }
                previous = l;
            }

            if(l != nullptr) {
                l->l_next = nullptr;
            }

            *list = head;
        }
    } else {
        format_error = TRUE;
    }

    return (READSTAT);
}

int rs_write_magic_items(FILE *savef, magic_item *i, int count) {
    int n;

    DBG(("Magic Items\n"));
    rs_write_uint(savef, RSXR_MAGICITEMS);
    rs_write_int(savef, count);

    for(n = 0; n < count; n++) {
        rs_write(savef, i[n].mi_name, sizeof(i[n].mi_name));
        rs_write_int(savef, i[n].mi_prob);
        rs_write_int(savef, i[n].mi_worth);
        rs_write_int(savef, i[n].mi_curse);
        rs_write_int(savef, i[n].mi_bless);
    }

    return (WRITESTAT);
}

int rs_read_magic_items(int inf, magic_item *mi, int count) {
    unsigned int id;
    int n;
    int value;

    if(rs_read_uint(inf, &id) != 0) {
        if(id != RSXR_MAGICITEMS) {
            printf("Invalid id. %x != %x(RSXR_MAGICITEMS)\n", id, RSXR_MAGICITEMS);
            printf("Sorry, invalid save game format");
            format_error = TRUE;
        } else if(rs_read_int(inf, &value) != 0) {
            if(value > count) {
                printf("Incorrect number of magic items in block. %d > %d.", value, count);
                printf("Sorry, invalid save game format");
                format_error = TRUE;
            } else {
                for(n = 0; n < value; n++) {
                    rs_read(inf, mi[n].mi_name, sizeof(mi[n].mi_name));
                    rs_read_int(inf, &mi[n].mi_prob);
                    rs_read_int(inf, &mi[n].mi_worth);
                    rs_read_int(inf, &mi[n].mi_curse);
                    rs_read_int(inf, &mi[n].mi_bless);
                }
            }
        }
    }

    return (READSTAT);
}

int rs_write_window(FILE *savef, WINDOW *win) {
    int row, col, height, width;
    width = getmaxx(win);
    height = getmaxy(win);
    DBG(("Window\n"));
    rs_write_int(savef, height);
    rs_write_int(savef, width);

    for(row = 0; row < height; row++) {
        for(col = 0; col < width; col++) {
            rs_write_int(savef, (int)mvwinch(win, row, col));
        }
    }

    return (WRITESTAT);
}

int rs_read_window(int inf, WINDOW *win) {
    int row, col, maxlines, maxcols, value, width, height;

    width = getmaxx(win);
    height = getmaxy(win);

    rs_read_int(inf, &maxlines);
    rs_read_int(inf, &maxcols);
    if(maxlines > height) {
        abort();
    }
    if(maxcols > width) {
        abort();
    }

    for(row = 0; row < maxlines; row++) {
        for(col = 0; col < maxcols; col++) {
            rs_read_int(inf, &value);
            if((value == '-') || (value == 196)) {
                value = HORZWALL;
            } else if((value == '|') || (value == 179)) {
                value = VERTWALL;
            }
            mvwaddch_int(win, row, col, value);
        }
    }

    return (READSTAT);
}

int rs_save_file(FILE *savef) {
    int i, weapon, armor, ring, misc, room = -1;
    int endian = 0x01020304;
    big_endian = (*((char *)&endian) == 0x01);

    rs_write_thing(savef, &player);       /* rogue.c          */
    rs_write_object_list(savef, lvl_obj); /* rogue.c          */
    rs_write_monster_list(savef, mlist);  /* rogue.c          */
    rs_write_monster_list(savef, tlist);  /* rogue.c          */

    rs_write_traps(savef, traps, MAXTRAPS); /* rogue.c          */

    armor = find_list_ptr(player.t_pack, cur_armor);
    rs_write_int(savef, armor); /* rogue.c          */

    for(i = 0; i < NUM_FINGERS; i++) {
        ring = find_list_ptr(player.t_pack, cur_ring[i]);
        rs_write_int(savef, ring); /* rogue.c          */
    }

    for(i = 0; i < NUM_MM; i++) {
        misc = find_list_ptr(player.t_pack, cur_misc[i]);
        rs_write_int(savef, misc); /* rogue.c          */
    }

    for(i = 0; i < MAXRELIC; i++) {
        rs_write_int(savef, cur_relic[i]); /* rogue.c          */
    }

    rs_write_rooms(savef, rooms, MAXROOMS); /* rogue.c          */

    for(i = 0; i < MAXROOMS; i++) {
        if(&rooms[i] == oldrp) {
            room = i;
        }
    }
    rs_write_int(savef, room); /* rogue.c          */

    weapon = find_list_ptr(player.t_pack, cur_weapon);
    rs_write_int(savef, weapon); /* rogue..c         */
    rs_write_int(savef, char_type);
    rs_write_int(savef, foodlev);
    rs_write_int(savef, ntraps);
    rs_write_int(savef, trader);
    rs_write_int(savef, curprice);
    rs_write_int(savef, seed);
    rs_write_int(savef, max_level);
    rs_write_int(savef, cur_max);
    rs_write_int(savef, prev_max);
    rs_write_int(savef, move_free);
    rs_write_int(savef, mpos);
    rs_write_int(savef, level);
    rs_write_long(savef, purse);
    rs_write_int(savef, inpack);
    rs_write_int(savef, total);
    rs_write_int(savef, no_food);
    rs_write_int(savef, foods_this_level);
    rs_write_int(savef, count);
    rs_write_int(savef, food_left);
    rs_write_int(savef, group);
    rs_write_int(savef, hungry_state);
    rs_write_int(savef, infest_dam);
    rs_write_int(savef, lost_str);
    rs_write_int(savef, lastscore);
    rs_write_int(savef, hold_count);
    rs_write_int(savef, trap_tries);
    rs_write_int(savef, chant_time);
    rs_write_int(savef, pray_time);
    rs_write_int(savef, spell_power);
    rs_write_int(savef, turns);
    rs_write_int(savef, quest_item);
    rs_write_int(savef, cols);
    rs_write_int(savef, lines);
    rs_write_int(savef, nfloors);
    rs_write(savef, curpurch, LINELEN);
    rs_write_char(savef, PLAYER);
    rs_write_char(savef, take);
    rs_write_int(savef, 1234); /*checkpoint*/
    rs_write(savef, prbuf, LINELEN * 2);
    rs_write_int(savef, 1234); /*checkpoint*/
    rs_write_char(savef, runch);
    rs_write_int(savef, 1234); /*checkpoint*/
    rs_write_scrolls(savef);
    rs_write_potions(savef);
    rs_write_rings(savef);
    rs_write_sticks(savef);
    rs_write_misc(savef);
    rs_write_int(savef, 1234); /*checkpoint*/
    rs_write(savef, whoami, LINELEN);
    rs_write_window(savef, cw);
    rs_write_window(savef, hw);
    rs_write_window(savef, mw);
    rs_write_window(savef, msgw);
    rs_write_window(savef, stdscr);
    rs_write_boolean(savef, pool_teleport);
    rs_write_boolean(savef, inwhgt);
    rs_write_boolean(savef, after);
    rs_write_boolean(savef, waswizard);
    rs_write_boolean(savef, playing);      /* rogue.h/init.c   */
    rs_write_boolean(savef, running);      /* rogue.h/init.c   */
    rs_write_boolean(savef, wizard);       /* rogue.h/init.c   */
    rs_write_boolean(savef, notify);       /* rogue.h/init.c   */
    rs_write_boolean(savef, fight_flush);  /* rogue.h/init.c   */
    rs_write_boolean(savef, terse);        /* rogue.h/init.c   */
    rs_write_boolean(savef, auto_pickup);  /* rogue.h/init.c   */
    rs_write_boolean(savef, def_attr);     /* rogue.h/init.c   */
    rs_write_boolean(savef, menu_overlay); /* rogue.h/init.c   */
    rs_write_boolean(savef, door_stop);    /* rogue.h/init.c   */
    rs_write_boolean(savef, jump);         /* rogue.h/init.c   */
    rs_write_boolean(savef, slow_invent);  /* rogue.h/init.c   */
    rs_write_boolean(savef, firstmove);    /* rogue.h/init.c   */
    rs_write_boolean(savef, askme);        /* rogue.h/init.c   */
    rs_write_boolean(savef, daytime);      /* rogue.h/init.c   */
    rs_write_boolean(savef, funfont);      /* rogue.h/init.c   */
    rs_write_levtype(savef, levtype);
    rs_write_character_types(savef, char_class, NUM_CHARTYPES);
    rs_write_words(savef, abilities, NUMABILITIES);
    for(i = 0; i < 9; i++) {
        rs_write_coord(savef, &grid[i]);
    }
    rs_write_death_types(savef, deaths, DEATHNUM);
    rs_write_init_weps(savef, weaps, MAXWEAPONS);
    rs_write_init_armor(savef, armors, MAXARMORS);
    rs_write_magic_items(savef, things, NUMTHINGS);   /* rogue.h/init.c   */
    rs_write_magic_items(savef, s_magic, MAXSCROLLS); /* rogue.h/init.c   */
    rs_write_magic_items(savef, p_magic, MAXPOTIONS); /* rogue.h/init.c   */
    rs_write_magic_items(savef, r_magic, MAXRINGS);   /* rogue.h/init.c   */
    rs_write_magic_items(savef, ws_magic, MAXSTICKS); /* rogue.h/init.c   */
    rs_write_magic_items(savef, m_magic, MAXMM);      /* rogue.h/init.c   */
    rs_write_magic_items(savef, rel_magic, MAXRELIC); /* rogue.h/init.c   */
    rs_write_magic_items(savef, foods, MAXFOODS);     /* rogue.h/init.c   */
    rs_write_spells(savef, magic_spells, MAXSPELLS);
    rs_write_spells(savef, cleric_spells, MAXPRAYERS);
    rs_write_spells(savef, druid_spells, MAXCHANTS);
    rs_write_spells(savef, quill_scrolls, MAXQUILL);

    rs_write_int(savef, mf_count);                                          /* actions.c        */
    rs_write_int(savef, mf_jmpcnt);                                         /* actions.c        */
    rs_write_daemons(savef, d_list, MAXDAEMONS);                            /* daemon.c         */
    rs_write_daemons(savef, f_list, MAXFUSES);                              /* daemon.c         */
    rs_write_int(savef, demoncnt);                                          /* daemon.c         */
    rs_write_int(savef, fusecnt);                                           /* daemon.c         */
    rs_write_int(savef, killed_chance);                                     /* fight.c          */
    rs_write_words(savef, rainbow, NCOLORS);                                /* init.c   */
    rs_write_words(savef, sylls, NSYLLS);                                   /* init.c   */
    rs_write_words(savef, stones, NSTONES);                                 /* init.c   */
    rs_write_words(savef, wood, NWOOD);                                     /* init.c   */
    rs_write_words(savef, metal, NMETAL);                                   /* init.c   */
    rs_write_monsters(savef, monsters, sizeof(monsters) / sizeof(monster)); /* mons_def.c       */
    rs_write_coord(savef, &move_nh);                                        /* move.c           */
    return (WRITESTAT);
}

int rs_restore_file(int inf) {
    int weapon, armor, ring, misc, room = -1, i, checkpoint;
    int endian = 0x01020304;
    big_endian = (*((char *)&endian) == 0x01);

    rs_read_thing(inf, &player);        /* rogue.h          */
    rs_read_object_list(inf, &lvl_obj); /* rogue.h/init.c   */
    rs_read_monster_list(inf, &mlist);  /* rogue.h/init.c   */
    rs_read_monster_list(inf, &tlist);  /* rogue.h/init.c   */
    rs_fix_thing(&player);
    rs_fix_monster_list(mlist);
    rs_read_traps(inf, traps, MAXTRAPS);

    rs_read_int(inf, &armor); /* rogue.h          */
    cur_armor = (object *)get_list_item(player.t_pack, armor);

    for(i = 0; i < NUM_FINGERS; i++) {
        rs_read_int(inf, &ring);
        cur_ring[i] = (object *)get_list_item(player.t_pack, ring);
    }

    for(i = 0; i < NUM_MM; i++) {
        rs_read_int(inf, &misc);
        cur_misc[i] = (object *)get_list_item(player.t_pack, misc);
    }

    for(i = 0; i < MAXRELIC; i++) {
        rs_read_int(inf, &cur_relic[i]);
    }

    rs_read_rooms(inf, rooms, MAXROOMS);
    rs_read_int(inf, &room);

    oldrp = &rooms[room];

    rs_read_int(inf, &weapon);
    cur_weapon = (object *)get_list_item(player.t_pack, weapon);

    rs_read_int(inf, &char_type);
    rs_read_int(inf, &foodlev);
    rs_read_int(inf, &ntraps);
    rs_read_int(inf, &trader);
    rs_read_int(inf, &curprice);
    rs_read_int(inf, &seed);
    rs_read_int(inf, &max_level);
    rs_read_int(inf, &cur_max);
    rs_read_int(inf, &prev_max);
    rs_read_int(inf, &move_free);
    rs_read_int(inf, &mpos);
    rs_read_int(inf, &level);
    rs_read_long(inf, &purse);
    rs_read_int(inf, &inpack);
    rs_read_int(inf, &total);
    rs_read_int(inf, &no_food);
    rs_read_int(inf, &foods_this_level);
    rs_read_int(inf, &count);
    rs_read_int(inf, &food_left);
    rs_read_int(inf, &group);
    rs_read_int(inf, &hungry_state);
    rs_read_int(inf, &infest_dam);
    rs_read_int(inf, &lost_str);
    rs_read_int(inf, &lastscore);
    rs_read_int(inf, &hold_count);
    rs_read_int(inf, &trap_tries);
    rs_read_int(inf, &chant_time);
    rs_read_int(inf, &pray_time);
    rs_read_int(inf, &spell_power);
    rs_read_int(inf, &turns);
    rs_read_int(inf, &quest_item);
    rs_read_int(inf, &cols);
    rs_read_int(inf, &lines);
    rs_read_int(inf, &nfloors);
    rs_read(inf, curpurch, LINELEN);
    rs_read_char(inf, &PLAYER);
    rs_read_char(inf, &take);
    rs_read_int(inf, &checkpoint);
    if(checkpoint != 1234) {
        printf("Checkpoint failed");
        abort();
    }
    rs_read(inf, prbuf, LINELEN * 2);
    rs_read_int(inf, &checkpoint);
    if(checkpoint != 1234) {
        printf("Checkpoint failed");
        abort();
    }
    rs_read_char(inf, &runch);
    rs_read_int(inf, &checkpoint);
    if(checkpoint != 1234) {
        printf("Checkpoint failed");
        abort();
    }
    rs_read_scrolls(inf);
    rs_read_potions(inf);
    rs_read_rings(inf);
    rs_read_sticks(inf);
    rs_read_misc(inf);
    rs_read_int(inf, &checkpoint);
    if(checkpoint != 1234) {
        printf("Checkpoint failed");
        abort();
    }
    rs_read(inf, whoami, LINELEN);
    rs_read_window(inf, cw);
    rs_read_window(inf, hw);
    rs_read_window(inf, mw);
    rs_read_window(inf, msgw);
    rs_read_window(inf, stdscr);
    rs_read_boolean(inf, &pool_teleport);
    rs_read_boolean(inf, &inwhgt);
    rs_read_boolean(inf, &after);
    rs_read_boolean(inf, &waswizard);
    rs_read_boolean(inf, &playing);      /* rogue.h/init.c   */
    rs_read_boolean(inf, &running);      /* rogue.h/init.c   */
    rs_read_boolean(inf, &wizard);       /* rogue.h/init.c   */
    rs_read_boolean(inf, &notify);       /* rogue.h/init.c   */
    rs_read_boolean(inf, &fight_flush);  /* rogue.h/init.c   */
    rs_read_boolean(inf, &terse);        /* rogue.h/init.c   */
    rs_read_boolean(inf, &auto_pickup);  /* rogue.h/init.c   */
    rs_read_boolean(inf, &def_attr);     /* rogue.h/init.c   */
    rs_read_boolean(inf, &menu_overlay); /* rogue.h/init.c   */
    rs_read_boolean(inf, &door_stop);    /* rogue.h/init.c   */
    rs_read_boolean(inf, &jump);         /* rogue.h/init.c   */
    rs_read_boolean(inf, &slow_invent);  /* rogue.h/init.c   */
    rs_read_boolean(inf, &firstmove);    /* rogue.h/init.c   */
    rs_read_boolean(inf, &askme);        /* rogue.h/init.c   */
    rs_read_boolean(inf, &daytime);      /* rogue.h/init.c   */
    rs_read_boolean(inf, &funfont);      /* rogue.h/init.c   */
    rs_read_levtype(inf, &levtype);
    rs_read_character_types(inf, char_class, NUM_CHARTYPES);
    rs_read_words(inf, abilities, NUMABILITIES);
    for(i = 0; i < 9; i++) {
        rs_read_coord(inf, &grid[i]);
    }
    rs_read_death_types(inf, deaths, DEATHNUM);
    rs_read_init_weps(inf, weaps, MAXWEAPONS);
    rs_read_init_armor(inf, armors, MAXARMORS);
    rs_read_magic_items(inf, things, NUMTHINGS);   /* rogue.h/init.c   */
    rs_read_magic_items(inf, s_magic, MAXSCROLLS); /* rogue.h/init.c   */
    rs_read_magic_items(inf, p_magic, MAXPOTIONS); /* rogue.h/init.c   */
    rs_read_magic_items(inf, r_magic, MAXRINGS);   /* rogue.h/init.c   */
    rs_read_magic_items(inf, ws_magic, MAXSTICKS); /* rogue.h/init.c   */
    rs_read_magic_items(inf, m_magic, MAXMM);      /* rogue.h/init.c   */
    rs_read_magic_items(inf, rel_magic, MAXRELIC); /* rogue.h/init.c   */
    rs_read_magic_items(inf, foods, MAXFOODS);     /* rogue.h/init.c   */
    rs_read_spells(inf, magic_spells, MAXSPELLS);
    rs_read_spells(inf, cleric_spells, MAXPRAYERS);
    rs_read_spells(inf, druid_spells, MAXCHANTS);
    rs_read_spells(inf, quill_scrolls, MAXQUILL);

    rs_read_int(inf, &mf_count);              /* actions.c        */
    rs_read_int(inf, &mf_jmpcnt);             /* actions.c        */
    rs_read_daemons(inf, d_list, MAXDAEMONS); /* daemon.c         */
    rs_read_daemons(inf, f_list, MAXFUSES);   /* daemon.c         */
    rs_read_int(inf, &demoncnt);              /* daemon.c         */
    rs_read_int(inf, &fusecnt);               /* daemon.c         */
    rs_read_int(inf, &killed_chance);         /* fight.c          */
    rs_read_words(inf, rainbow, NCOLORS);     /* init.c   */
    rs_read_words(inf, sylls, NSYLLS);        /* init.c   */
    rs_read_words(inf, stones, NSTONES);      /* init.c   */
    rs_read_words(inf, wood, NWOOD);          /* init.c   */
    rs_read_words(inf, metal, NMETAL);        /* init.c   */

    rs_read_monsters(inf, monsters, sizeof(monsters) / sizeof(monster)); /* mons_def.c */
    rs_read_coord(inf, &move_nh);                                        /* move.c           */

    return (READSTAT);
}

int rs_write_scorefile(FILE *savef, sc_ent *entries, int count) {
    int i;

    rs_write_int(savef, count);
    for(i = 0; i < count; i++) {
        rs_write_ulong(savef, entries[i].sc_score);
        rs_write(savef, entries[i].sc_name, sizeof(entries[i].sc_name));
        rs_write(savef, entries[i].sc_system, sizeof(entries[i].sc_system));
        rs_write(savef, entries[i].sc_login, sizeof(entries[i].sc_login));
        rs_write_short(savef, entries[i].sc_flags);
        rs_write_short(savef, entries[i].sc_level);
        rs_write_short(savef, entries[i].sc_ctype);
        rs_write_short(savef, entries[i].sc_monster);
        rs_write_short(savef, entries[i].sc_quest);
    }

    return (WRITESTAT);
}

int rs_read_scorefile(FILE *savef, sc_ent *entries, int count) {
    int i, available = 0;

    rs_read_int(fileno(savef), &available);

    if(end_of_file) {
        return (-1);
    }

    if(available != count) {
        return (-2);
    }

    for(i = 0; i < count; i++) {
        rs_read_ulong(fileno(savef), &entries[i].sc_score);
        rs_read(fileno(savef), entries[i].sc_name, sizeof(entries[i].sc_name));
        rs_read(fileno(savef), entries[i].sc_system, sizeof(entries[i].sc_system));
        rs_read(fileno(savef), entries[i].sc_login, sizeof(entries[i].sc_login));
        rs_read_short(fileno(savef), &entries[i].sc_flags);
        rs_read_short(fileno(savef), &entries[i].sc_level);
        rs_read_short(fileno(savef), &entries[i].sc_ctype);
        rs_read_short(fileno(savef), &entries[i].sc_monster);
        rs_read_short(fileno(savef), &entries[i].sc_quest);
    }

    return (0);
}

void rs_print_thing(FILE *outf, thing *thing, const char *prefix, int list, int index) {
    int i;

    fprintf(outf, "%sList Ident : %d\n", prefix, list);
    fprintf(outf, "%sList Index : %d\n", prefix, index);
    fprintf(outf, "%st_wasshot  : %d\n", prefix, thing->t_wasshot);
    fprintf(outf, "%st_type     : %c\n", prefix, thing->t_type);
    fprintf(outf, "%st_disguise : %c\n", prefix, thing->t_disguise);
    fprintf(outf, "%st_oldch    : %c\n", prefix, thing->t_oldch);
    fprintf(outf, "%st_ctype    : %d\n", prefix, thing->t_ctype);
    fprintf(outf, "%st_index    : %d\n", prefix, thing->t_index);
    fprintf(outf, "%st_no_move  : %d\n", prefix, thing->t_no_move);
    fprintf(outf, "%st_quiet    : %d\n", prefix, thing->t_quiet);
    fprintf(outf, "%st_movement : %d\n", prefix, thing->t_movement);
    fprintf(outf, "%st_action   : %d\n", prefix, thing->t_action);
    fprintf(outf, "%st_artificat: %d\n", prefix, thing->t_artifact);
    fprintf(outf, "%st_wand     : %d\n", prefix, thing->t_wand);
    fprintf(outf, "%st_summon   : %d\n", prefix, thing->t_summon);
    fprintf(outf, "%st_cast     : %d\n", prefix, thing->t_cast);
    fprintf(outf, "%st_breathe  : %d\n", prefix, thing->t_breathe);
    fprintf(outf, "%st_name     : %s\n", prefix,
            (thing->t_name == nullptr) ? "none" : thing->t_name);
    fprintf(outf, "%st_doorgoal : %d %d\n", prefix, thing->t_doorgoal.x, thing->t_doorgoal.y);
    fprintf(outf, "%st_dest     : %p\n", prefix, (void *)thing->t_dest);
    fprintf(outf, "%st_pos      : %d %d (%p)\n", prefix, thing->t_pos.x, thing->t_pos.y,
            (void *)&thing->t_pos);
    fprintf(outf, "%st_oldpos   : %d %d\n", prefix, thing->t_oldpos.x, thing->t_oldpos.y);
    fprintf(outf, "%st_newpos   : %d %d\n", prefix, thing->t_newpos.x, thing->t_newpos.y);
    fprintf(outf, "%st_flags    : ", prefix);

    for(i = 0; i < 16; i++) {
        fprintf(outf, "%lX ", thing->t_flags[i]);
    }
    fprintf(outf, "\n");

    fprintf(outf, "%st_pack     : %p\n", prefix, (void *)thing->t_pack);
    fprintf(outf, "%st_using    : %p\n", prefix, (void *)thing->t_using);
    fprintf(outf, "%st_stats    : Not Implemented\n", prefix);
    fprintf(outf, "%st_maxstats : Not Implemented\n", prefix);
    fprintf(outf, "%st_reserved : %d\n", prefix, thing->t_reserved);
}

void rs_print_game_state(FILE *outf) {
    fprintf(outf, "Player\n");

    rs_print_thing(outf, &player, "    ", 0, 0);
}

/****
 Machine Dependent Functions

 md_getusername()
 md_gethostname()
 md_gethomedir()
 md_getroguedir()
 md_getpass()
 md_crypt()
 md_nstoh()
 md_unlink()
 md_isdir()

****/

#ifdef SIGTSTP

/*
 * handle stop and start signals
 */

/*UNUSED*/
void tstp(int a) {
    a = 0; // silence warning
    mvcur(0, cols - 1, lines - 1, 0);
    fflush(stdout);
    kill(0, SIGTSTP);
    signal(SIGTSTP, tstp);
    crmode();
    noecho();
    clearok(curscr, TRUE);
    touchwin(cw);
    draw(cw);
    flushinp();
}
#endif

void md_setup() {
#ifdef SIGTSTP
    signal(SIGTSTP, tstp);
#endif
#ifdef SIGHUP
    signal(SIGHUP, auto_save);
#endif
    signal(SIGTERM, auto_save);
    signal(SIGINT, quit);
#ifdef SIGQUIT
    signal(SIGQUIT, endit);
#endif
    crmode(); /* Cbreak mode */
    noecho(); /* Echo off */
}
