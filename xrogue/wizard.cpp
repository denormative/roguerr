/*
        wizard.c - Special wizard commands

        XRogue: Expeditions into the Dungeons of Doom
        Copyright (C) 1991 Robert Pietkivitch
        All rights reserved.

        Based on "Advanced Rogue"
        Copyright (C) 1984, 1985 Michael Morgan, Ken Dalka and AT&T
        All rights reserved.

        Based on "Rogue: Exploring the Dungeons of Doom"
        Copyright (C) 1980, 1981 Michael Toy, Ken Arnold and Glenn Wichman
        All rights reserved.

        See the file LICENSE.TXT for full copyright and licensing information.
*/

/*
 * Special wizard commands (some of which are also non-wizard commands
 * under strange circumstances)
 */

#include "rogue.h"

/*
 * create_obj:
 *      Create any object for wizard, scroll, magician, or cleric
 */

void create_obj(bool prompt, int which_item, int which_type) {
    linked_list *item;
    object *obj;
    int wh;
    const char *pt;
    int ch, whc, newtype = 0, msz, newitem;
    WINDOW *thiswin;

    thiswin = cw;
    if(prompt) {
        bool nogood = TRUE;

        thiswin = hw;
        wclear(hw);
        wprintw(hw, "Item\t\t\tKey\n\n");
        wprintw(hw, "%s\t\t\t%c\n%s\t\t\t%c\n", things[TYP_RING].mi_name, RING,
                things[TYP_STICK].mi_name, STICK);
        wprintw(hw, "%s\t\t\t%c\n%s\t\t\t%c\n", things[TYP_POTION].mi_name, POTION,
                things[TYP_SCROLL].mi_name, SCROLL);
        wprintw(hw, "%s\t\t\t%c\n%s\t\t\t%c\n", things[TYP_ARMOR].mi_name, ARMOR,
                things[TYP_WEAPON].mi_name, WEAPON);
        wprintw(hw, "%s\t%c\n", things[TYP_MM].mi_name, MM);
        wprintw(hw, "%s\t\t\t%c\n", things[TYP_FOOD].mi_name, FOOD);
        if(wizard) {
            wprintw(hw, "%s\t\t%c\n", things[TYP_RELIC].mi_name, RELIC);
            wprintw(hw, "monster\t\t\tm");
        }
        wprintw(hw, "\n\nWhat do you want to create? ");
        draw(hw);
        do {
            ch = wgetch(hw);
            if(ch == ESC) {
                restscr(cw);
                return;
            }
            switch(ch) {
                case RING:
                case STICK:
                case POTION:
                case SCROLL:
                case ARMOR:
                case WEAPON:
                case FOOD:
                case MM:
                    nogood = FALSE;
                    break;
                case RELIC:
                case 'm':
                    if(wizard) {
                        nogood = FALSE;
                    }
                    break;
                default:
                    nogood = TRUE;
            }
        } while(nogood);
        newitem = ch;
    } else {
        newitem = which_item;
    }

    pt = "those";
    msz = 0;
    if(newitem == 'm') {
        /* make monster and be done with it */
        wh = makemonster(TRUE, "create");
        if(wh > 0) {
            creat_mons(&player, (short)wh, TRUE);
            light(&hero);
        }
        return;
    }
    if(newitem == GOLD) {
        pt = "gold";
    } else if(isatrap((char)newitem)) {
        pt = "traps";
    }

    switch(newitem) {
        case POTION:
            whc = TYP_POTION;
            msz = MAXPOTIONS;
            break;
        case SCROLL:
            whc = TYP_SCROLL;
            msz = MAXSCROLLS;
            break;
        case WEAPON:
            whc = TYP_WEAPON;
            msz = MAXWEAPONS;
            break;
        case ARMOR:
            whc = TYP_ARMOR;
            msz = MAXARMORS;
            break;
        case RING:
            whc = TYP_RING;
            msz = MAXRINGS;
            break;
        case STICK:
            whc = TYP_STICK;
            msz = MAXSTICKS;
            break;
        case MM:
            whc = TYP_MM;
            msz = MAXMM;
            break;
        case RELIC:
            whc = TYP_RELIC;
            msz = MAXRELIC;
            break;
        case FOOD:
            whc = TYP_FOOD;
            msz = MAXFOODS;
            break;
        default:
            if(thiswin == hw) {
                restscr(cw);
            }
            mpos = 0;
            msg("Even wizards can't create %s !!", pt);
            return;
    }
    if(msz == 1) { /* if only one type of item */
        ch = 'a';
    } else if(prompt) {
        magic_item *wmi;
        char wmn;
        int ii;
        int old_prob;

        mpos = 0;
        wmi = nullptr;
        wmn = 0;
        switch(newitem) {
            case POTION:
                wmi = &p_magic[0];
                break;
            case SCROLL:
                wmi = &s_magic[0];
                break;
            case RING:
                wmi = &r_magic[0];
                break;
            case STICK:
                wmi = &ws_magic[0];
                break;
            case MM:
                wmi = &m_magic[0];
                break;
            case RELIC:
                wmi = &rel_magic[0];
                break;
            case FOOD:
                wmi = &foods[0];
                break;
            case WEAPON:
                wmn = 1;
                break;
            case ARMOR:
                wmn = 2;
        }
        wclear(hw);
        thiswin = hw;
        if(wmi != nullptr) {
            ii = old_prob = 0;
            while(ii < msz) {
                if(wmi->mi_prob == old_prob && wizard == FALSE) {
                    msz--; /* can't make a unique item */
                } else {
                    mvwaddch_int(hw, ii % 13, ii > 12 ? cols / 2 : 0, ii + 'a');
                    waddstr(hw, ") ");
                    waddstr(hw, wmi->mi_name);
                    ii++;
                }
                old_prob = wmi->mi_prob;
                wmi++;
            }
        } else if(wmn != 0) {
            for(ii = 0; ii < msz; ii++) {
                mvwaddch_int(hw, ii % 13, ii > 12 ? cols / 2 : 0, ii + 'a');
                waddstr(hw, ") ");
                if(wmn == 1) {
                    waddstr(hw, weaps[ii].w_name);
                } else {
                    waddstr(hw, armors[ii].a_name);
                }
            }
        }
        sprintf(prbuf, "Which %s? ", things[whc].mi_name);
        mvwaddstr(hw, lines - 1, 0, prbuf);
        draw(hw);
        do {
            ch = wgetch(hw);
            if(ch == ESC) {
                restscr(cw);
                msg("");
                return;
            }
        }
        until(isalpha(ch));
        if(thiswin == hw) { /* restore screen if need be */
            restscr(cw);
        }
        newtype = ch - 'a';
        if(newtype < 0 || newtype >= msz) { /* if an illegal value */
            mpos = 0;
            msg("There is no such %s", things[whc].mi_name);
            return;
        }
    } else {
        newtype = which_type;
    }
    item = new_item(sizeof *obj); /* get some memory */
    obj = OBJPTR(item);
    obj->o_type = newitem; /* store the new items */
    obj->o_mark[0] = '\0';
    obj->o_which = newtype;
    obj->o_group = 0;
    obj->contents = nullptr;
    obj->o_count = 1;
    obj->o_flags = 0;
    obj->o_dplus = obj->o_hplus = 0;
    obj->o_weight = 0;
    wh = obj->o_which;
    mpos = 0;
    if(!wizard) { /* users get 0 to +5 */
        whc = rnd(6);
    } else { /* wizard gets to choose */
        whc = getbless();
    }
    if(whc < 0) {
        obj->o_flags |= ISCURSED;
    }
    switch(obj->o_type) {
        case WEAPON:
        case ARMOR:
            if(obj->o_type == WEAPON) {
                init_weapon(obj, (char)wh);
                obj->o_hplus += whc;
                if(!wizard) {
                    whc = rnd(6);
                }
                obj->o_dplus += whc;
            } else { /* armor here */
                obj->o_weight = armors[wh].a_wght;
                obj->o_ac = armors[wh].a_class - whc;
            }
            break;
        case RING:
            r_know[wh] = TRUE;
            switch(wh) {
                case R_ADDSTR:
                case R_ADDWISDOM:
                case R_ADDINTEL:
                case R_PROTECT:
                case R_ADDHIT:
                case R_ADDDAM:
                case R_DIGEST:
                    obj->o_ac = whc + 2;
                    break;
                default:
                    obj->o_ac = 0;
            }
            obj->o_weight = things[TYP_RING].mi_wght;
            break;
        case MM:
            if(whc > 1 && m_magic[wh].mi_bless != 0) {
                obj->o_flags |= ISBLESSED;
            }
            m_know[wh] = TRUE;
            switch(wh) {
                case MM_JUG:
                    switch(rnd(11)) {
                        case 0:
                            obj->o_ac = P_PHASE;
                            break;
                        case 1:
                            obj->o_ac = P_CLEAR;
                            break;
                        case 2:
                            obj->o_ac = P_SEEINVIS;
                            break;
                        case 3:
                            obj->o_ac = P_HEALING;
                            break;
                        case 4:
                            obj->o_ac = P_MFIND;
                            break;
                        case 5:
                            obj->o_ac = P_TFIND;
                            break;
                        case 6:
                            obj->o_ac = P_HASTE;
                            break;
                        case 7:
                            obj->o_ac = P_RESTORE;
                            break;
                        case 8:
                            obj->o_ac = P_FLY;
                            break;
                        case 9:
                            obj->o_ac = P_SKILL;
                            break;
                        case 10:
                            obj->o_ac = P_FFIND;
                    }
                    break;
                case MM_HUNGER:
                case MM_CHOKE:
                    if(whc < 0) {
                        whc = -whc; /* cannot be negative */
                    }
                    obj->o_ac = (whc + 1) * 2;
                    break;
                case MM_OPEN:
                case MM_DRUMS:
                case MM_DISAPPEAR:
                case MM_KEOGHTOM:
                    if(whc < 0) {
                        whc = -whc; /* these cannot be negative */
                    }
                    obj->o_ac = (whc + 3) * 5;
                    break;
                case MM_BRACERS:
                    obj->o_ac = whc + 4;
                    break;
                case MM_DISP:
                    obj->o_ac = 3;
                    break;
                case MM_PROTECT:
                    obj->o_ac = whc + 4;
                    break;
                case MM_SKILLS:
                    if(whc < 2) {
                        obj->o_ac = rnd(NUM_CHARTYPES - 1);
                    } else {
                        obj->o_ac = player.t_ctype;
                    }
                    break;
                case MM_CRYSTAL:
                    obj->o_ac = 1;
                    break;
                default:
                    obj->o_ac = 0;
            }
            obj->o_weight = things[TYP_MM].mi_wght;
            break;
        case STICK:
            if(whc > 1 && ws_magic[wh].mi_bless != 0) {
                obj->o_flags |= ISBLESSED;
            }
            ws_know[wh] = TRUE;
            fix_stick(obj);
            break;
        case SCROLL:
            if(whc > 3 && s_magic[wh].mi_bless != 0) {
                obj->o_flags |= ISBLESSED;
            }
            obj->o_weight = things[TYP_SCROLL].mi_wght;
            s_know[wh] = TRUE;
            break;
        case POTION:
            if(whc > 3 && p_magic[wh].mi_bless != 0) {
                obj->o_flags |= ISBLESSED;
            }
            obj->o_weight = things[TYP_POTION].mi_wght;
            if(wh == P_ABIL) {
                obj->o_kind = rnd(NUMABILITIES);
            }
            p_know[wh] = TRUE;
            break;
        case RELIC:
            obj->o_weight = things[TYP_RELIC].mi_wght;
            switch(obj->o_which) {
                case QUILL_NAGROM:
                    obj->o_charges = QUILLCHARGES;
                    break;
                case EMORI_CLOAK:
                    obj->o_charges = 1;
                    break;
                default:
                    break;
            }
            break;
        case FOOD:
            obj->o_weight = things[TYP_FOOD].mi_wght;
    }
    mpos = 0;
    obj->o_flags |= ISKNOW;
    if(add_pack(item, FALSE) == FALSE) {
        obj->o_pos = hero;
        fall(item, TRUE);
    }
}

/*
 * getbless:
 *      Get a blessing for a wizards object
 */

int getbless() {
    char bless;

    msg("Blessing? (+,-,n)");
    bless = (char)wgetch(msgw);
    if(bless == '+') {
        return (15);
    }
    if(bless == '-') {
        return (-1);
    }
    return (0);
}

/*
 * get a non-monster death type
 */

int getdeath() {
    int i;
    int which_death;
    char label[80];

    clear();
    for(i = 0; i < DEATHNUM; i++) {
        sprintf(label, "[%d] %s", i + 1, deaths[i].name);
        mvaddstr(i + 2, 0, label);
    }
    mvaddstr(0, 0, "Which death? ");
    refresh();

    /* Get the death */
    for(;;) {
        get_str(label, stdscr);
        which_death = atoi(label);
        if((which_death < 1 || which_death > DEATHNUM)) {
            mvaddstr(0, 0, "Please enter a number in the displayed range -- ");
            refresh();
        } else {
            break;
        }
    }
    return (deaths[which_death - 1].reason);
}

/*
 * make a monster for the wizard
 */

int makemonster(bool showall, const char *action) {
    int i;
    short which_monst;
    int num_monst = NUMMONST, pres_monst = 1, num_lines = 2 * (lines - 3);
    int max_monster;
    char monst_name[40];

    /* If we're not showing all, subtract UNIQUES, DINOS, and quartermaster */
    if(!showall) {
        num_monst -= NUMUNIQUE + NUMDINOS + 1;
    }
    max_monster = num_monst;

    /* Print out the monsters */

    if(levtype == OUTSIDE) {
        num_monst = NUMDINOS;
        max_monster = NUMMONST - 1;
        pres_monst = (pres_monst + NUMMONST - NUMDINOS - 1);
    }

    while(num_monst > 0) {
        int left_limit;

        if(num_monst < num_lines) {
            left_limit = (num_monst + 1) / 2;
        } else {
            left_limit = num_lines / 2;
        }

        wclear(hw);
        touchwin(hw);

        /* Print left column */
        wmove(hw, 2, 0);
        for(i = 0; i < left_limit; i++) {
            sprintf(monst_name, "[%d] %c%s\n", pres_monst,
                    (showall || monsters[pres_monst].m_normal) ? ' ' : '*',
                    monsters[pres_monst].m_name);
            waddstr(hw, monst_name);
            pres_monst++;
        }

        /* Print right column */
        for(i = 0; i < left_limit && pres_monst <= max_monster; i++) {
            sprintf(monst_name, "[%d] %c%s", pres_monst,
                    (showall || monsters[pres_monst].m_normal) ? ' ' : '*',
                    monsters[pres_monst].m_name);
            wmove(hw, i + 2, cols / 2);
            waddstr(hw, monst_name);
            pres_monst++;
        }

        if((num_monst -= num_lines) > 0) {
            mvwaddstr(hw, lines - 1, 0, morestr);
            draw(hw);
            wait_for(' ');
        }

        else {
            mvwaddstr(hw, 0, 0, "Which monster");
            if(!terse) {
                waddstr(hw, " do you wish to ");
                waddstr(hw, action);
            }
            waddstr(hw, "? ");
            draw(hw);
        }
    }

get_monst:
    get_str(monst_name, hw);
    which_monst = (short)atoi(monst_name);
    if(levtype == OUTSIDE) {
        if((which_monst < NUMMONST - NUMDINOS || which_monst > max_monster)) {
            mvwaddstr(hw, 0, 0, "Please enter a number in the displayed range -- ");
            draw(hw);
            goto get_monst;
        }
    }
    if((which_monst < 1 || which_monst > max_monster)) {
        mvwaddstr(hw, 0, 0, "Please enter a number in the displayed range -- ");
        draw(hw);
        goto get_monst;
    }
    restscr(cw);
    return (which_monst);
}

/*
 * passwd:
 *      see if user knows password
 */

bool passwd() {
    char *sp, c;
    char buf[LINELEN];

    msg("Wizard's Password:");
    mpos = 0;
    sp = buf;
    while((c = (char)wgetch(cw)) != '\n' && c != '\r' && c != '\033') {
        if(c == killchar()) {
            sp = buf;
        } else if(c == erasechar() && sp > buf) {
            sp--;
        } else {
            *sp++ = c;
        }
    }
    if(sp == buf) {
        return FALSE;
    }
    *sp = '\0';
    return (strcmp(PASSWD, md_crypt(buf)) == 0);

    /* don't mess with the password here or elsewhere.
     *
     * If anyone goes wizard they forfeit being placed in the scorefile.
     * So, no need to be secretive about it.  Let them have it!
     *
     * Additionally, you can begin the game as wizard by starting it
     * with a null argument, as in: xrogue ""
     */
}

/*
 * teleport:
 *      Bamf the hero someplace else
 */

void teleport() {
    room *new_rp = nullptr, *old_rp = roomin(&hero);
    int rm, which;
    coord old;
    bool got_position = FALSE;

    /* Disrupt whatever the hero was doing */
    dsrpt_player();

    /*
     * If the hero wasn't doing something disruptable, NULL out his
     * action anyway and let him know about it.  We don't want him
     * swinging or moving into his old place.
     */
    if(player.t_action != A_NIL) {
        player.t_action = A_NIL;
        msg("You feel momentarily disoriented.");
    }

    old = hero;
    mvwaddch(cw, hero.y, hero.x, mvwinch(stdscr, hero.y, hero.x));
    if(ISWEARING(R_TELCONTROL) || wizard) {
        got_position = move_hero(H_TELEPORT);
        if(!got_position) {
            msg("Your attempt fails.");
        } else {
            new_rp = roomin(&hero);
            msg("You teleport successfully.");
        }
    }
    if(!got_position) {
        do {
            rm = rnd_room();
            rnd_pos(&rooms[rm], &hero);
        }
        until(winat(hero.y, hero.x) == FLOOR);
        new_rp = &rooms[rm];
    }
    player.t_oldpos = old; /* Save last position */

    /* If hero gets moved, darken old room */
    if(old_rp && old_rp != new_rp) {
        old_rp->r_flags |= FORCEDARK; /* Fake darkness */
        light(&old);
        old_rp->r_flags &= ~FORCEDARK; /* Restore light state */
    }

    /* Darken where we just came from */
    else if(levtype == MAZELEV) {
        light(&old);
    }

    light(&hero);
    mvwaddch_int(cw, hero.y, hero.x, PLAYER);
    /* if entering a treasure room, wake everyone up......Surprise! */
    if(new_rp->r_flags & ISTREAS) {
        wake_room(new_rp);
    }

    /* Reset current room and position */
    oldrp = new_rp; /* Used in look() */
    player.t_oldpos = hero;
    /*
     * make sure we set/unset the ISINWALL on a teleport
     */
    which = (int)winat(hero.y, hero.x);
    if(isrock(which)) {
        turn_on(player, ISINWALL);
    } else {
        turn_off(player, ISINWALL);
    }

    /*
     * turn off ISHELD in case teleportation was done while fighting
     * something that holds you
     */
    if(on(player, ISHELD)) {
        linked_list *ip, *nip;
        thing *mp;

        turn_off(player, ISHELD);
        hold_count = 0;
        for(ip = mlist; ip; ip = nip) {
            mp = THINGPTR(ip);
            nip = next(ip);
            if(on(*mp, DIDHOLD)) {
                turn_off(*mp, DIDHOLD);
                turn_on(*mp, CANHOLD);
            }
            turn_off(*mp, DIDSUFFOCATE); /* Suffocation -- see below */
        }
    }

    /* Make sure player does not suffocate */
    extinguish(d_suffocate);

    count = 0;
    running = FALSE;
    flushinp();
}

/*
 * whatis:
 *      What a certin object is
 */

void whatis(linked_list *what) {
    object *obj;
    linked_list *item;

    if(what == nullptr) { /* do we need to ask which one? */
        if((item = get_item(pack, "identify", IDENTABLE, FALSE, FALSE)) == nullptr) {
            return;
        }
    } else {
        item = what;
    }
    obj = OBJPTR(item);
    switch(obj->o_type) {
        case SCROLL:
            s_know[obj->o_which] = TRUE;
            if(s_guess[obj->o_which]) {
                free(s_guess[obj->o_which]);
                s_guess[obj->o_which] = nullptr;
            }
            break;
        case POTION:
            p_know[obj->o_which] = TRUE;
            if(p_guess[obj->o_which]) {
                free(p_guess[obj->o_which]);
                p_guess[obj->o_which] = nullptr;
            }
            break;
        case STICK:
            ws_know[obj->o_which] = TRUE;
            if(ws_guess[obj->o_which]) {
                free(ws_guess[obj->o_which]);
                ws_guess[obj->o_which] = nullptr;
            }
            break;
        case RING:
            r_know[obj->o_which] = TRUE;
            if(r_guess[obj->o_which]) {
                free(r_guess[obj->o_which]);
                r_guess[obj->o_which] = nullptr;
            }
            break;
        case MM:
            /* If it's an identified jug, identify its potion */
            if(obj->o_which == MM_JUG && (obj->o_flags & ISKNOW)) {
                if(obj->o_ac != JUG_EMPTY) {
                    p_know[obj->o_ac] = TRUE;
                }
                break;
            }

            m_know[obj->o_which] = TRUE;
            if(m_guess[obj->o_which]) {
                free(m_guess[obj->o_which]);
                m_guess[obj->o_which] = nullptr;
            }
            break;
        default:
            break;
    }
    obj->o_flags |= ISKNOW;
    if(what == nullptr) {
        msg(inv_name(obj, FALSE));
    }
}

/*
 *  Choose a quest item
 *      (if on STARTLEV equipage level = 0)
 */

void choose_qst() {
    bool doit = TRUE;
    bool escp = TRUE;

    /* let wizard in on this too */
    if(waswizard == TRUE || (levtype == POSTLEV && level == 0)) {
        wclear(hw);
        touchwin(hw);
        wmove(hw, 2, 0);
        wprintw(hw, "a) Cloak of Emori\n");
        wprintw(hw, "b) Ankh of Heil\n");
        wprintw(hw, "c) Quill of Nagrom\n");
        wprintw(hw, "d) Eye of Vecna\n");
        wprintw(hw, "e) Ring of Surtur\n");
        wprintw(hw, "f) Staff of Ming\n");
        wprintw(hw, "g) Wand of Orcus\n");
        wprintw(hw, "h) Rod of Asmodeus\n");
        wprintw(hw, "i) Amulet of Yendor\n");
        wprintw(hw, "j) Amulet of Stonebones\n");
        wprintw(hw, "k) Mandolin of Brian\n");
        wprintw(hw, "l) Horn of Geryon\n");
        wprintw(hw, "m) Daggers of Musty Doit\n");
        wprintw(hw, "n) Axe of Aklad\n");
        wprintw(hw, "o) Morning Star of Hruggek\n");
        wprintw(hw, "p) Flail of Yeenoghu\n");
        wprintw(hw, "q) Card of Alteran\n");
        mvwaddstr(hw, 0, 0, "Select a quest item: "); /* prompt */

        if(menu_overlay) /* Print the selections.  The longest line is
                          * Hruggek (26 characters).  The prompt is 21.
                          */
        {
            over_win(cw, hw, 20, 29, 0, 21, 0);
        } else {
            draw(hw);
        }

        while(doit) {
            switch(wgetch(cw)) {
                case EOF:
                case ESC:
                    escp = FALSE; /* used below */
                    doit = FALSE;
                    break;
                case 'a':
                    quest_item = EMORI_CLOAK;
                    doit = FALSE;
                    break;
                case 'b':
                    quest_item = HEIL_ANKH;
                    doit = FALSE;
                    break;
                case 'c':
                    quest_item = QUILL_NAGROM;
                    doit = FALSE;
                    break;
                case 'd':
                    quest_item = EYE_VECNA;
                    doit = FALSE;
                    break;
                case 'e':
                    quest_item = SURTUR_RING;
                    doit = FALSE;
                    break;
                case 'f':
                    quest_item = MING_STAFF;
                    doit = FALSE;
                    break;
                case 'g':
                    quest_item = ORCUS_WAND;
                    doit = FALSE;
                    break;
                case 'h':
                    quest_item = ASMO_ROD;
                    doit = FALSE;
                    break;
                case 'i':
                    quest_item = YENDOR_AMULET;
                    doit = FALSE;
                    break;
                case 'j':
                    quest_item = STONEBONES_AMULET;
                    doit = FALSE;
                    break;
                case 'k':
                    quest_item = BRIAN_MANDOLIN;
                    doit = FALSE;
                    break;
                case 'l':
                    quest_item = GERYON_HORN;
                    doit = FALSE;
                    break;
                case 'm':
                    quest_item = MUSTY_DAGGER;
                    doit = FALSE;
                    break;
                case 'n':
                    quest_item = AXE_AKLAD;
                    doit = FALSE;
                    break;
                case 'o':
                    quest_item = HRUGGEK_MSTAR;
                    doit = FALSE;
                    break;
                case 'p':
                    quest_item = YEENOGHU_FLAIL;
                    doit = FALSE;
                    break;
                case 'q':
                    quest_item = ALTERAN_CARD;
                    doit = FALSE;
                    break;
                default:
                    doit = TRUE;
            }
        }
        if(menu_overlay) {
            status(FALSE);
            touchwin(cw);
            if(escp == TRUE) {
                msg("Your quest item is the %s.  --More--", rel_magic[quest_item].mi_name);
                wait_for(' ');
            }
            return;
        }
        if(escp == TRUE) {
            wmove(hw, lines - 4, 0);
            wprintw(hw, "Your quest item is the %s.", rel_magic[quest_item].mi_name);
        }
        wmove(hw, lines - 1, 0);
        wprintw(hw, spacemsg);
        draw(hw);
        wait_for(' ');
        wclear(hw);
        draw(hw);
        wmove(cw, 0, 0);
        wclrtoeol(cw);
        status(FALSE);
        touchwin(cw);
        return;
    }
    msg("You can no longer select a quest item. ");
}
