/*
 * Functions for dealing with linked lists of goodies
 *
 * @(#)list.c     4.7 (Berkeley) 12/19/81
 *
 * Rogue: Exploring the Dungeons of Doom
 * Copyright (C) 1980, 1981, 1982 Michael Toy, Ken Arnold and Glenn Wichman
 * All rights reserved.
 *
 * See the file LICENSE.TXT for full copyright and licensing information.
 */

#include "rogue.h"

/*
 * detach:
 *     Takes an item out of whatever linked list it might be in
 */
void _detach(THING **list, THING *item) {
    if(*list == item) {
        *list = next((THING *)item);
    }
    if(prev((THING *)item) != nullptr) {
        ((THING *)item)->l_prev->l_next = next((THING *)item);
    }
    if(next((THING *)item) != nullptr) {
        ((THING *)item)->l_next->l_prev = prev((THING *)item);
    }
    ((THING *)item)->l_next = nullptr;
    ((THING *)item)->l_prev = nullptr;
}

/*
 * _attach:
 *     add an item to the head of a list
 */
void _attach(THING **list, THING *item) {
    if(*list != nullptr) {
        ((THING *)item)->l_next = *list;
        (*list)->l_prev = item;
        ((THING *)item)->l_prev = nullptr;
    } else {
        ((THING *)item)->l_next = nullptr;
        ((THING *)item)->l_prev = nullptr;
    }
    *list = item;
}

/*
 * _free_list:
 *     Throw the whole blamed thing away
 */
void _free_list(THING **ptr) {
    THING *item;

    while(*ptr != nullptr) {
        item = *ptr;
        *ptr = next(item);
        discard(item);
    }
}

/*
 * discard:
 *     Free up an item
 */
void discard(THING *item) {
    total--;
    free((char *)item);
}

/*
 * new_item
 *     Get a new item with a specified size
 */
THING *new_item() {
    THING *item;

    if((item = (THING *)calloc(1, sizeof *item)) == nullptr) {
        msg("ran out of memory after %d items", total);
    } else {
        total++;
    }
    item->l_next = item->l_prev = nullptr;
    return item;
}
