/*
 * Rogue definitions and variable declarations
 *
 * @(#)rogue.h     5.2 (Berkeley) 5/10/82
 *
 * Rogue: Exploring the Dungeons of Doom
 * Copyright (C) 1980, 1981, 1982 Michael Toy, Ken Arnold and Glenn Wichman
 * All rights reserved.
 *
 * See the file LICENSE.TXT for full copyright and licensing information.
 */

#pragma once

#include "metarog.h"
#include <curses.h>

struct STONE {
    const char *st_name;
    const int st_value;
};

extern const char *rainbow[];
extern const STONE stones[];
extern const char *sylls[];
extern const char *wood[];
extern const char *metal[];

#define NCOLORS 27
#define NSYLLS 159
#define NSTONES 26
#define NWOOD 33
#define NMETAL 22

/*
 * Maximum number of different things
 */
#define MAXDAEMONS 20
#define MAXROOMS 9
#define MAXTHINGS 9
#define MAXOBJ 9
#define MAXPACK 23
#define MAXTRAPS 10
#define AMULETLEVEL 26
#define NUMTHINGS 7 /* number of types of things */
#define MAXPASS 13  /* upper limit on number of passages */

/*
 * return values for get functions
 */
#define NORM 0  /* normal exit */
#define QUIT 1  /* quit option setting */
#define MINUS 2 /* back up one option */

/*
 * All the fun defines
 */
#define next(ptr) (*ptr).l_next
#define prev(ptr) (*ptr).l_prev
#define winat(y, x) (moat(y, x) != NULL ? moat(y, x)->t_disguise : chat(y, x))
#define DISTANCE(y1, x1, y2, x2) ((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1))
#define ce(a, b) ((a).x == (b).x && (a).y == (b).y)
#define hero player.t_pos
#define pstats player.t_stats
#define pack player.t_pack
#define proom player.t_room
#define max_hp player.t_stats.s_maxhp
#define attach(a, b) _attach(&a, b)
#define detach(a, b) _detach(&a, b)
#define free_list(a) _free_list(&a)
#define on(thing, flag) (((thing).t_flags & (flag)) != 0)
#undef CTRL
#define CTRL(ch) (ch & 037)
#define GOLDCALC() (rnd(50 + 10 * level) + 2)
#define ISRING(h, r) (cur_ring[h] != NULL && cur_ring[h]->o_which == r)
#define ISWEARING(r) (ISRING(LEFT, r) || ISRING(RIGHT, r))
#define ISMULT(type) (type == POTION || type == SCROLL || type == FOOD || type == GOLD)
#define INDEX(y, x) (((x) << 5) + (y))
#define chat(y, x) (_level[((x) << 5) + (y)])
#define flat(y, x) (_flags[((x) << 5) + (y)])
#define moat(y, x) (_monst[((x) << 5) + (y)])
#define unc(cp) (cp).y, (cp).x
#define debug                                                                                      \
    if(wizard)                                                                                     \
    msg

/*
 * Things that appear on the screens
 */
#define PASSAGE '#'
#define DOOR '+'
#define FLOOR '.'
#define PLAYER '@'
#define TRAP '^'
#define STAIRS '%'
#define GOLD '*'
#define POTION '!'
#define SCROLL '?'
#define MAGIC '$'
#define FOOD ':'
#define WEAPON ')'
#define ARMOR ']'
#define AMULET ','
#define RING '='
#define STICK '/'
#define CALLABLE -1

/*
 * Various constants
 */
#define PASSWD "mTuZ7WUV9RWkQ"
#define BEARTIME spread(3)
#define SLEEPTIME spread(5)
#define HEALTIME spread(30)
#define HOLDTIME spread(2)
#define WANDERTIME spread(70)
#define BEFORE spread(1)
#define AFTER spread(2)
#define HUHDURATION spread(20)
#define SEEDURATION spread(850)
#define HUNGERTIME spread(1300)
#define MORETIME 150
#define STOMACHSIZE 2000
#define STARVETIME 850
#define ESCAPE 27
#define LEFT 0
#define RIGHT 1
#define BOLT_LENGTH 6
#define LAMPDIST 3

/*
 * Save against things
 */
#define VS_POISON 00
#define VS_PARALYZATION 00
#define VS_DEATH 00
#define VS_BREATH 02
#define VS_MAGIC 03

/*
 * Various flag bits
 */
/* flags for rooms */
#define ISDARK 0000001 /* room is dark */
#define ISGONE 0000002 /* room is gone (a corridor) */

/* flags for objects */
#define ISCURSED 000001 /* object is cursed */
#define ISKNOW 0000002  /* player knows details about the object */
#define ISMISL 0000004  /* object is a missile type */
#define ISMANY 0000010  /* object comes in groups */

/* flags for creatures */
#define CANHUH 0000001  /* creature can confuse */
#define CANSEE 0000002  /* creature can see invisible creatures */
#define ISBLIND 0000004 /* creature is blind */
#define ISCANC 0000010  /* creature has special qualities cancelled */
#define ISFOUND 0000020 /* creature has been seen (used for objects) */
#define ISGREED 0000040 /* creature runs to protect gold */
#define ISHASTE 0000100 /* creature has been hastened */
#define ISHELD 0000400  /* creature has been held */
#define ISHUH 0001000   /* creature is confused */
#define ISINVIS 0002000 /* creature is invisible */
#define ISMEAN 0004000  /* creature can wake when player enters room */
#define ISREGEN 0010000 /* creature can regenerate */
#define ISRUN 0020000   /* creature is running at the player */
#define SEEMONST 040000 /* hero can detect unseen monsters */
#define ISSLOW 0100000  /* creature has been slowed */

/*
 * Flags for level map
 */
#define F_PASS 0x80    /* is a passageway */
#define F_SEEN 0x40    /* have seen this corridor before */
#define F_DROPPED 0x20 /* object was dropped here */
#define F_LOCKED 0x20  /* door is locked */
#define F_REAL 0x10    /* what you see is what you get */
#define F_PNUM 0x0f    /* passage number mask */
#define F_TMASK 0x07   /* trap number mask */

/*
 * Trap types
 */
#define T_DOOR 00
#define T_ARROW 01
#define T_SLEEP 02
#define T_BEAR 03
#define T_TELEP 04
#define T_DART 05
#define NTRAPS 6

/*
 * Potion types
 */
#define P_CONFUSE 0
#define P_PARALYZE 1
#define P_POISON 2
#define P_STRENGTH 3
#define P_SEEINVIS 4
#define P_HEALING 5
#define P_MFIND 6
#define P_TFIND 7
#define P_RAISE 8
#define P_XHEAL 9
#define P_HASTE 10
#define P_RESTORE 11
#define P_BLIND 12
#define P_NOP 13
#define MAXPOTIONS 14

/*
 * Scroll types
 */
#define S_CONFUSE 0
#define S_MAP 1
#define S_HOLD 2
#define S_SLEEP 3
#define S_ARMOR 4
#define S_IDENT 5
#define S_SCARE 6
#define S_GFIND 7
#define S_TELEP 8
#define S_ENCH 9
#define S_CREATE 10
#define S_REMOVE 11
#define S_AGGR 12
#define S_NOP 13
#define S_GENOCIDE 14
#define MAXSCROLLS 15

/*
 * Weapon types
 */
#define MACE 0
#define SWORD 1
#define BOW 2
#define ARROW 3
#define DAGGER 4
#define TWOSWORD 5
#define DART 6
#define CROSSBOW 7
#define BOLT 8
#define SPEAR 9
#define FLAME 10      /* fake entry for dragon breath (ick) */
#define MAXWEAPONS 10 /* this should equal FLAME */

/*
 * Armor types
 */
#define LEATHER 0
#define RING_MAIL 1
#define STUDDED_LEATHER 2
#define SCALE_MAIL 3
#define CHAIN_MAIL 4
#define SPLINT_MAIL 5
#define BANDED_MAIL 6
#define PLATE_MAIL 7
#define MAXARMORS 8

/*
 * Ring types
 */
#define R_PROTECT 0
#define R_ADDSTR 1
#define R_SUSTSTR 2
#define R_SEARCH 3
#define R_SEEINVIS 4
#define R_NOP 5
#define R_AGGR 6
#define R_ADDHIT 7
#define R_ADDDAM 8
#define R_REGEN 9
#define R_DIGEST 10
#define R_TELEPORT 11
#define R_STEALTH 12
#define R_SUSTARM 13
#define MAXRINGS 14

/*
 * Rod/Wand/Staff types
 */

#define WS_LIGHT 0
#define WS_HIT 1
#define WS_ELECT 2
#define WS_FIRE 3
#define WS_COLD 4
#define WS_POLYMORPH 5
#define WS_MISSILE 6
#define WS_HASTE_M 7
#define WS_SLOW_M 8
#define WS_DRAIN 9
#define WS_NOP 10
#define WS_TELAWAY 11
#define WS_TELTO 12
#define WS_CANCEL 13
#define MAXSTICKS 14

/*
 * Now we define the structures and types
 */

/*
 * Help list
 */

struct h_list {
    char h_ch;
    const char *h_desc;
};

/*
 * Coordinate data type
 */
struct coord {
    int x;
    int y;
};

/* daemon/fuse data type */

enum daemon_id {
    d_none,
    d_rollwand,
    d_doctor,
    d_stomach,
    d_runners,
    d_swander,
    d_nohste,
    d_unconfuse,
    d_unsee,
    d_sight,

    d_dont_save,

    d_nohaste,
    d_come_down,
    d_visuals,
    d_turn_see,
    d_land,

};

struct delayed_action {
    int d_type;
    enum daemon_id d_id;
    int d_arg;
    int d_time;
};

/**/

typedef unsigned int str_t;

/*
 * Stuff about magic items
 */

struct magic_item {
    const char *mi_name;
    int mi_prob;
    short mi_worth;
};

/*
 * Room structure
 */
struct room {
    coord r_pos;      /* Upper left corner */
    coord r_max;      /* Size of room */
    coord r_gold;     /* Where the gold is */
    int r_goldval;    /* How much the gold is worth */
    short r_flags;    /* Info about the room */
    int r_nexits;     /* Number of exits */
    coord r_exit[12]; /* Where the exits are */
};

/*
 * Structure describing a fighting being
 */
struct stats {
    str_t s_str;    /* Strength */
    int s_exp;      /* Experience */
    int s_lvl;      /* Level of mastery */
    int s_arm;      /* Armor class */
    short s_hpt;    /* Hit points */
    char s_dmg[16]; /* String describing damage done */
    int s_maxhp;    /* Max hit points */
};

/*
 * Structure for monsters and player
 */
union thing {
    struct {
        union thing *_l_next, *_l_prev; /* Next pointer in link */
        coord _t_pos;                   /* Position */
        bool _t_turn;                   /* If slowed, is it a turn to move */
        unsigned char _t_type;          /* What it is */
        char _t_disguise;               /* What mimic looks like */
        char _t_oldch;                  /* Character that was where it was */
        coord *_t_dest;                 /* Where it is running to */
        short _t_flags;                 /* State word */
        stats _t_stats;                 /* Physical description */
        room *_t_room;                  /* Current room for thing */
        union thing *_t_pack;           /* What the thing is carrying */
        int _t_reserved;
    } _t;
    struct {
        union thing *_l_next, *_l_prev; /* Next pointer in link */
        int _o_type;                    /* What kind of object it is */
        coord _o_pos;                   /* Where it lives on the screen */
        char *_o_text;                  /* What it says if you read it */
        char _o_launch;                 /* What you need to launch it */
        char _o_damage[8];              /* Damage if used like sword */
        char _o_hurldmg[8];             /* Damage if thrown */
        int _o_count;                   /* Count for plural objects */
        int _o_which;                   /* Which object of a type it is */
        int _o_hplus;                   /* Plusses to hit */
        int _o_dplus;                   /* Plusses to damage */
        short _o_ac;                    /* Armor class */
        short _o_flags;                 /* Information about objects */
        int _o_group;                   /* Group number for this object */
    } _o;
};

typedef union thing THING;

#define l_next _t._l_next
#define l_prev _t._l_prev
#define t_pos _t._t_pos
#define t_turn _t._t_turn
#define t_type _t._t_type
#define t_disguise _t._t_disguise
#define t_oldch _t._t_oldch
#define t_dest _t._t_dest
#define t_flags _t._t_flags
#define t_stats _t._t_stats
#define t_pack _t._t_pack
#define t_room _t._t_room
#define t_reserved _t._t_reserved
#define o_type _o._o_type
#define o_pos _o._o_pos
#define o_text _o._o_text
#define o_launch _o._o_launch
#define o_damage _o._o_damage
#define o_hurldmg _o._o_hurldmg
#define o_count _o._o_count
#define o_which _o._o_which
#define o_hplus _o._o_hplus
#define o_dplus _o._o_dplus
#define o_ac _o._o_ac
#define o_charges o_ac
#define o_goldval o_ac
#define o_flags _o._o_flags
#define o_group _o._o_group
#define o_reserved _o._o_reserved

/*
 * Array containing information on all the various types of mosnters
 */
struct monster {
    const char *m_name;  /* What to call the monster */
    const int m_carry;   /* Probability of carrying something */
    const short m_flags; /* Things about the monster */
    stats m_stats;       /* Initial stats */
};

/*
 * External variables
 */

extern delayed_action d_list[20];

extern THING *_monst[], *cur_armor, *cur_ring[], *cur_weapon, *lvl_obj, *mlist, player;

extern coord delta, oldpos;

extern h_list helpstr[];

extern room *oldrp, passages[], rooms[];

extern stats max_stats;

extern monster monsters[];

extern magic_item p_magic[], r_magic[], s_magic[], things[], ws_magic[];

extern char version[], encstr[];
extern bool _endwin;

/*
 * Function types
 */

#define MAXSTR 80   /* maximum length of strings */
#define MAXLINES 32 /* maximum number of screen lines used */
#define MAXCOLS 80  /* maximum number of screen columns used */

#define RN (((seed = seed * 11109 + 13849) >> 16) & 0xffff)

/*
 * Now all the global variables
 */

extern bool after, amulet, askme, door_stop, fight_flush, firstmove, jump, noscore, p_know[],
    passgo, playing, r_know[], running, s_know[], save_msg, slow_invent, terse, wizard, ws_know[];

extern const char *p_colors[], *r_stones[], *w_names[], *a_names[], *ws_made[], *ws_type[];
extern char _flags[], _level[], file_name[], fruit[], home[], huh[], outbuf[], *p_guess[], prbuf[],
    *r_guess[], release[], runch, *s_guess[], *s_names[], take, whoami[], *ws_guess[];

extern int a_chances[], a_class[], count, dnum, food_left, fung_hit, fd, group, hungry_state,
    inpack, lastscore, level, max_level, mpos, no_command, no_food, no_move, ntraps, purse, quiet,
    total;

extern long seed;

extern WINDOW *hw;

/*
 * Function types
 */

extern coord ch_ret;
extern int countch;
extern int direction;
extern int newcount;
extern int between;
extern char lvl_mons[27];
extern char wand_mons[27];
extern coord nh;
extern bool got_genocide;

// armor.h
void wear();
void take_off();
void waste_time();

// chase.h
void runners();
int do_chase(THING *th);
int see_monst(THING *mp);
void runto(coord *runner);
int chase(THING *tp, coord *ee);
room *roomin(coord *cp);
int diag_ok(coord *sp, coord *ep);
int cansee(int y, int x);
coord *find_dest(THING *tp);

// command.h
void command();
void illcom(char ch);
void search();
void help();
void identify();
void d_level();
void u_level();
void call();

// daemon.h
delayed_action *d_slot();
delayed_action *find_slot(enum daemon_id did);
void start_daemon(enum daemon_id did, int arg, int type);
void kill_daemon(enum daemon_id did);
void do_daemons(int flag);
void fuse(enum daemon_id did, int arg, int time, int type);
void lengthen(enum daemon_id did, int xtime);
void extinguish(enum daemon_id did);
void do_fuses(int flag);

// daemons.h
void doctor();
void swander();
void rollwand();
void unconfuse();
void unsee();
void sight();
void nohaste();
void stomach();

// extern.h

// fight.h
int fight(coord *mp, char mn, THING *weap, bool thrown);
int attack(THING *mp);
int swing(int at_lvl, int op_arm, int wplus);
void check_level();
int roll_em(THING *thatt, THING *thdef, THING *weap, bool hurl);
char *prname(const char *who, bool upper);
void hit(const char *er, const char *ee);
void miss(const char *er, const char *ee);
int save_throw(int which, THING *tp);
int save(int which);
int str_plus(str_t str);
int add_dam(str_t str);
void raise_level();
void thunk(THING *weap, const char *mname);
void bounce(THING *weap, const char *mname);
void remove_monster(coord *mp, THING *tp, bool waskill);
int is_magic(THING *obj);
void killed(THING *tp, bool pr);

// init.h
void init_player();
void init_things();
void init_colors();
void init_names();
void init_stones();
void init_materials();
void badcheck(const char *name, magic_item *magic, int bound);

// io.h
void msg(const char *fmt, ...);
void addmsg(const char *fmt, ...);
void endmsg();
void doadd(const char *fmt, va_list ap);
int step_ok(int ch);
int readcharw(WINDOW *win);
int readchar();
const char *unctrol(char ch);
void status();
void wait_for(char ch);
void w_wait_for(WINDOW *win, char ch);
void show_win(WINDOW *scr, const char *message);

// list.h
void _detach(THING **list, THING *item);
void _attach(THING **list, THING *item);
void _free_list(THING **ptr);
void discard(THING *item);
THING *new_item();

// mach_dep.h
void open_score();
void setup();
int issymlink(char *sp);
void flush_type();

// main.h
int main(int argc, char **argv);
void endit(int a);
int fatal(const char *s);
int rnd(int range);
int roll(int number, int sides);
void tstp(int a);
void playit();
void quit(int a);
[[noreturn]] void leave(int sig);

// mdport.h
FILE *md_fdopen(int fd, const char *mode);
int md_fileno(FILE *fp);
int md_getuid();
void md_ignore_signals();
int reread();
void unread(int c);
int md_readchar(WINDOW *win);

// misc.h
const char *tr_name(char type);
void look(bool wakeup);
THING *find_obj(int y, int x);
void eat();
void chg_str(int amt);
void add_str(str_t *sp, int amt);
int add_haste(bool potion);
void aggravate();
const char *vowelstr(const char *str);
int is_current(THING *obj);
int get_dir();
int sign(int nm);
int spread(int nm);
void call_it(bool know, char **guess);

// monsters.h
int randmonster(bool wander);
void new_monster(THING *tp, char type, coord *cp);
int exp_add(THING *tp);
void wanderer();
THING *wake_monster(int y, int x);
void genocide();
void give_pack(THING *tp);

// move.h
void do_run(char ch);
void do_move(int dy, int dx);
void turnref();
void door_open(room *rp);
int be_trapped(coord *tc);
coord *rndmove(THING *who);

// new_level.h
void new_level();
int rnd_room();
void put_things();
void treas_room();

// options.h
void option();
void put_bool(void *v);
void put_str(void *str);
int get_bool(void *vp, WINDOW *win);
int get_str(void *vp, WINDOW *win);
int get_num(short *opt, WINDOW *win);
void parse_opts(char *str);
void strucpy(char *s1, char *s2, size_t len);

// pack.h
void update_mdest(THING *obj);
void add_pack(THING *obj, bool silent);
int inventory(THING *list, int type);
void pick_up(char ch);
void picky_inven();
THING *get_item(const char *purpose, int type);
int pack_char(THING *obj);
void money(int value);

// passages.h
void do_passages();
void conn(int r1, int r2);
void door(room *rm, coord *cp);
void add_pass();
void passnum();
void numpass(int y, int x);

// potions.h
void quaff();
void invis_on();
int turn_see(bool turn_off);

// rings.h
void ring_on();
void ring_off();
int gethand();
int ring_eat(int hand);
const char *ring_num(THING *obj);

// rip.h
void score(int amount, int flags, char monst);
int death(char monst);
[[noreturn]] void total_winner();
char *killname(char monst, bool doart);

// rooms.h
void do_rooms();
void draw_room(room *rp);
void vert(room *rp, int startx);
void horiz(room *rp, int starty);
void rnd_pos(room *rp, coord *cp);
void enter_room(coord *cp);
void leave_room(coord *cp);

// save.h
int save_game();
[[noreturn]] void auto_save(int sig);
void save_file(FILE *savef);
int restore(char *file);
// void encwrite(void *starta, unsigned long size, FILE *outf);
int encread(void *starta, unsigned long size, int inf);
size_t encwrite(const void *buf, size_t size, FILE *outf);
size_t encread(void *buf, size_t size, FILE *inf);

// scrolls.h
void read_scroll();

// state.h
void *get_list_item(THING *l, int i);
int find_list_ptr(THING *l, void *ptr);
int list_size(THING *l);
int rs_write(FILE *savef, void *ptr, int size);
int rs_write_char(FILE *savef, char c);
int rs_write_boolean(FILE *savef, bool c);
int rs_write_booleans(FILE *savef, bool *c, int count);
int rs_write_short(FILE *savef, short c);
int rs_write_shorts(FILE *savef, short *c, int count);
int rs_write_ushort(FILE *savef, unsigned short c);
int rs_write_int(FILE *savef, int c);
int rs_write_ints(FILE *savef, int *c, int count);
int rs_write_uint(FILE *savef, unsigned int c);
int rs_write_long(FILE *savef, long c);
int rs_write_longs(FILE *savef, long *c, int count);
int rs_write_ulong(FILE *savef, unsigned long c);
int rs_write_ulongs(FILE *savef, unsigned long *c, int count);
int rs_write_string(FILE *savef, char *s);
int rs_write_string_index(FILE *savef, const char *master[], int max, const char *str);
int rs_write_strings(FILE *savef, char *s[], int count);
int rs_read(int inf, void *ptr, int size);
int rs_read_char(int inf, char *c);
int rs_read_uchar(int inf, unsigned char *c);
int rs_read_boolean(int inf, bool *i);
int rs_read_booleans(int inf, bool *i, int count);
int rs_read_short(int inf, short *i);
int rs_read_shorts(int inf, short *i, int count);
int rs_read_ushort(int inf, unsigned short *i);
int rs_read_int(int inf, int *i);
int rs_read_ints(int inf, int *i, int count);
int rs_read_uint(int inf, unsigned int *i);
int rs_read_long(int inf, long *i);
int rs_read_longs(int inf, long *i, int count);
int rs_read_ulong(int inf, unsigned long *i);
int rs_read_ulongs(int inf, unsigned long *i, int count);
int rs_read_string(int inf, char *s, int max);
int rs_read_new_string(int inf, char **s);
int rs_read_string_index(int inf, const char *master[], int maxindex, const char **str);
int rs_read_strings(int inf, char **s, int count, int max);
int rs_read_new_strings(int inf, char **s, int count);
int rs_write_str_t(FILE *savef, str_t st);
int rs_read_str_t(int inf, str_t *st);
int rs_write_coord(FILE *savef, coord c);
int rs_read_coord(int inf, coord *c);
void rs_write_window(FILE *savef, WINDOW *win);
int rs_read_window(int inf, WINDOW *win);
int rs_write_daemons(FILE *savef, delayed_action *d_list, int count);
int rs_read_daemons(int inf, delayed_action *d_list, int count);
int rs_write_magic_items(FILE *savef, magic_item *i, int count);
int rs_read_magic_items(int inf, magic_item *mi, int count);
int rs_write_room(FILE *savef, room *r);
int rs_write_rooms(FILE *savef, room r[], int count);
int rs_read_room(int inf, room *r);
int rs_read_rooms(int inf, room *r, int count);
int rs_write_room_reference(FILE *savef, room *rp);
int rs_read_room_reference(int inf, room **rp);
int rs_write_stats(FILE *savef, stats *s);
int rs_read_stats(int inf, stats *s);
int rs_write_object(FILE *savef, THING *o);
int rs_read_object(int inf, THING *o);
int rs_write_object_list(FILE *savef, THING *l);
int rs_read_object_list(int inf, THING **list);
int rs_write_object_reference(FILE *savef, THING *list, THING *item);
int rs_read_object_reference(int inf, THING *list, THING **item);
int find_thing_coord(THING *monlist, coord *c);
int find_room_coord(room *rmlist, coord *c, int n);
int find_object_coord(THING *objlist, coord *c);
int rs_write_thing(FILE *savef, THING *t);
void rs_fix_thing(THING *t);
void rs_fix_thing_list(THING *list);
int rs_read_thing(int inf, THING *t);
int rs_write_thing_list(FILE *savef, THING *l);
int rs_read_thing_list(int inf, THING **list);
int rs_write_monsters(FILE *savef, monster *m, int count);
int rs_read_monsters(int inf, monster *m, int count);
int rs_write_scrolls(FILE *savef);
int rs_read_scrolls(int inf);
int rs_write_potions(FILE *savef);
int rs_read_potions(int inf);
int rs_write_rings(FILE *savef);
int rs_read_rings(int inf);
int rs_write_sticks(FILE *savef);
int rs_read_sticks(int inf);
int rs_write_thing_reference(FILE *savef, THING *list, THING *item);
int rs_read_thing_reference(int inf, THING *list, THING **item);
int rs_write_thing_references(FILE *savef, THING *list, THING *items[], int count);
int rs_read_thing_references(int inf, THING *list, THING *items[], int count);
int rs_save_file(FILE *savef);
int rs_restore_file(int inf);

// sticks.h
void fix_stick(THING *cur);
void do_zap();
void drain();
void fire_bolt(coord *start, coord *dir, const char *name);
char *charge_str(THING *obj);

// things.h
char *inv_name(THING *obj, bool drop);
void drop();
int dropcheck(THING *op);
THING *new_thing();
int pick_one(magic_item *magic, int nitems);
void discovered();
void print_disc(char type);
void set_order(short *order, int numthings);
void add_line(const char *fmt, const char *arg);
void end_line();
char *nothing(char type);

// vers.h

// weapons.h
void missile(int ydelta, int xdelta);
void do_motion(THING *obj, int ydelta, int xdelta);
void fall(THING *obj, bool pr);
void init_weapon(THING *weap, char type);
int hit_monster(int y, int x, THING *obj);
char *num(int n1, int n2, char type);
void wield();
int fallpos(coord *pos, coord *newpos, bool pass);

// wizard.h
void whatis(bool insist);
void create_obj();
int teleport();
int passwd();
void show_map();

/* where scorefile should live */
#define SCOREFILE "rogue52.scr"
