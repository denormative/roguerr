/*
 * Read and execute the user commands
 *
 * @(#)command.c     4.31 (Berkeley) 4/6/82
 *
 * Rogue: Exploring the Dungeons of Doom
 * Copyright (C) 1980, 1981, 1982 Michael Toy, Ken Arnold and Glenn Wichman
 * All rights reserved.
 *
 * See the file LICENSE.TXT for full copyright and licensing information.
 */

#include "rogue.h"

int countch, direction, newcount = FALSE;

/*
 * command:
 *     Process the user commands
 */
void command() {
    char ch;
    int ntimes = 1; /* Number of player moves */

    if(on(player, ISHASTE)) {
        ntimes++;
    }
    /*
     * Let the daemons start up
     */
    do_daemons(BEFORE);
    do_fuses(BEFORE);
    while(ntimes--) {
        /*
         * these are illegal things for the player to be, so if any are
         * set, someone's been poking in memeory
         */
        if(on(player, ISSLOW | ISCANC | ISGREED | ISINVIS | ISMEAN | ISREGEN)) {
            auto_save(-1);
        }

        look(TRUE);
        if(!running) {
            door_stop = FALSE;
        }
        status();
        lastscore = purse;
        move(hero.y, hero.x);
        if(!((running || count) && jump)) {
            refresh(); /* Draw screen */
        }
        take = 0;
        after = TRUE;
        /*
         * Read command or continue run
         */
        if(wizard) {
            noscore = TRUE;
        }
        if(!no_command) {
            if(running) {
                ch = runch;
            } else if(count) {
                ch = (char)countch;
            } else {
                ch = (char)readchar();
                if(mpos != 0 && !running) { /* Erase message if its there */
                    msg("");
                }
            }
        } else {
            ch = '.';
        }
        if(no_command) {
            if(--no_command == 0) {
                player.t_flags |= ISRUN;
                msg("you can move again");
            }
        } else {
            /*
             * check for prefixes
             */
            if(isdigit(ch)) {
                count = 0;
                newcount = TRUE;
                while(isdigit(ch)) {
                    count = count * 10 + (ch - '0');
                    ch = (char)readchar();
                }
                countch = ch;
                /*
                 * turn off count for commands which don't make sense
                 * to repeat
                 */
                switch(ch) {
                    case 'h':
                    case 'j':
                    case 'k':
                    case 'l':
                    case 'y':
                    case 'u':
                    case 'b':
                    case 'n':
                    case 'H':
                    case 'J':
                    case 'K':
                    case 'L':
                    case 'Y':
                    case 'U':
                    case 'B':
                    case 'N':
                    case 'q':
                    case 'r':
                    case 's':
                    case 'f':
                    case 't':
                    case 'C':
                    case 'I':
                    case '.':
                    case 'z':
                    case CTRL('D'):
                    case CTRL('U'):
                        break;
                    default:
                        count = 0;
                }
            }
            switch(ch) {
                case 'f':
                    if(!on(player, ISBLIND)) {
                        door_stop = TRUE;
                        firstmove = TRUE;
                    }
                    if(count && !newcount) {
                        ch = (char)direction;
                    } else {
                        ch = (char)readchar();
                    }
                    switch(ch) {
                        case 'h':
                        case 'j':
                        case 'k':
                        case 'l':
                        case 'y':
                        case 'u':
                        case 'b':
                        case 'n':
                            ch = (char)toupper(ch);
                    }
                    direction = ch;
            }
            newcount = FALSE;
            /*
             * execute a command
             */
            if(count && !running) {
                count--;
            }
            switch(ch) {
                case 'h':
                    do_move(0, -1);
                    break;
                case 'j':
                    do_move(1, 0);
                    break;
                case 'k':
                    do_move(-1, 0);
                    break;
                case 'l':
                    do_move(0, 1);
                    break;
                case 'y':
                    do_move(-1, -1);
                    break;
                case 'u':
                    do_move(-1, 1);
                    break;
                case 'b':
                    do_move(1, -1);
                    break;
                case 'n':
                    do_move(1, 1);
                    break;
                case 'H':
                    do_run('h');
                    break;
                case 'J':
                    do_run('j');
                    break;
                case 'K':
                    do_run('k');
                    break;
                case 'L':
                    do_run('l');
                    break;
                case 'Y':
                    do_run('y');
                    break;
                case 'U':
                    do_run('u');
                    break;
                case 'B':
                    do_run('b');
                    break;
                case 'N':
                    do_run('n');
                    break;
                case 't':
                    if(!get_dir()) {
                        after = FALSE;
                    } else {
                        missile(delta.y, delta.x);
                    }
                    break;
                case 'Q':
                    after = FALSE;
                    quit(-1);
                    break;
                case 'i':
                    after = FALSE;
                    inventory(pack, 0);
                    break;
                case 'I':
                    after = FALSE;
                    picky_inven();
                    break;
                case 'd':
                    drop();
                    break;
                case 'q':
                    quaff();
                    break;
                case 'r':
                    read_scroll();
                    break;
                case 'e':
                    eat();
                    break;
                case 'w':
                    wield();
                    break;
                case 'W':
                    wear();
                    break;
                case 'T':
                    take_off();
                    break;
                case 'P':
                    ring_on();
                    break;
                case 'R':
                    ring_off();
                    break;
                case 'o':
                    option();
                    after = FALSE;
                    break;
                case 'c':
                    call();
                    after = FALSE;
                    break;
                case '>':
                    after = FALSE;
                    d_level();
                    break;
                case '<':
                    after = FALSE;
                    u_level();
                    break;
                case '?':
                    after = FALSE;
                    help();
                    break;
                case '/':
                    after = FALSE;
                    identify();
                    break;
                case 's':
                    search();
                    break;
                case 'z':
                    if(get_dir()) {
                        do_zap();
                    } else {
                        after = FALSE;
                    }
                    break;
                case 'D':
                    after = FALSE;
                    discovered();
                    break;
                case CTRL('R'):
                    after = FALSE;
                    msg(huh);
                    break;
                case CTRL('L'):
                    after = FALSE;
                    clearok(curscr, TRUE);
                    wrefresh(curscr);
                    break;
                case 'v':
                    after = FALSE;
                    msg("rogue version %s. (mctesq was here)", release);
                    break;
                case 'S':
                    after = FALSE;
                    if(save_game()) {
                        move(LINES - 1, 0);
                        clrtoeol();
                        refresh();
                        endwin();
                        exit(0);
                    }
                    break;
                case '.':; /* Rest command */
                    break;
                case ' ':
                    after = FALSE; /* "Legal" illegal command */
                    break;
                case '^':
                    after = FALSE;
                    if(get_dir()) {
                        delta.y += hero.y;
                        delta.x += hero.x;
                        if(chat(delta.y, delta.x) != TRAP) {
                            msg("no trap there");
                        } else {
                            msg(tr_name(flat(delta.y, delta.x) & F_TMASK));
                        }
                    }
                    break;
                case CTRL('P'):
                    after = FALSE;
                    if(wizard) {
                        wizard = FALSE;
                        turn_see(TRUE);
                        msg("not wizard any more");
                    } else {
                        if((wizard = passwd())) {
                            noscore = TRUE;
                            turn_see(FALSE);
                            msg("you are suddenly as smart as Ken Arnold in dungeon #%d", dnum);
                        } else {
                            msg("sorry");
                        }
                    }
                    break;
                case ESCAPE: /* Escape */
                    door_stop = FALSE;
                    count = 0;
                    after = FALSE;
                    break;
                default:
                    after = FALSE;
                    if(wizard) {
                        switch(ch) {
                            case '@':
                                msg("@ %d,%d", hero.y, hero.x);
                                break;
                            case 'C':
                                create_obj();
                                break;
                            case CTRL('I'):
                                inventory(lvl_obj, 0);
                                break;
                            case CTRL('W'):
                                whatis(FALSE);
                                break;
                            case CTRL('D'):
                                level++;
                                new_level();
                                break;
                            case CTRL('U'):
                                if(level > 1) {
                                    level--;
                                }
                                new_level();
                                break;
                            case CTRL('F'):
                                show_map();
                                break;
                            case CTRL('T'):
                                teleport();
                                break;
                            case CTRL('E'):
                                msg("food left: %d", food_left);
                                break;
                            case CTRL('A'):
                                msg("%d things in your pack", inpack);
                                break;
                            case CTRL('K'):
                                add_pass();
                                break;
                            case CTRL('X'):
                                turn_see(on(player, SEEMONST));
                                break;
                            case CTRL('N'): {
                                THING *item;

                                if((item = get_item("charge", STICK)) != nullptr) {
                                    item->o_charges = 10000;
                                }
                            } break;
                            case CTRL('H'): {
                                int i;
                                THING *obj;

                                for(i = 0; i < 9; i++) {
                                    raise_level();
                                }
                                /*
                                 * Give the rogue a sword (+1,+1)
                                 */
                                obj = new_item();
                                obj->o_type = WEAPON;
                                obj->o_which = TWOSWORD;
                                init_weapon(obj, SWORD);
                                obj->o_hplus = 1;
                                obj->o_dplus = 1;
                                obj->o_count = 1;
                                obj->o_group = 0;
                                add_pack(obj, TRUE);
                                cur_weapon = obj;
                                /*
                                 * And his suit of armor
                                 */
                                obj = new_item();
                                obj->o_type = ARMOR;
                                obj->o_which = PLATE_MAIL;
                                obj->o_ac = -5;
                                obj->o_flags |= ISKNOW;
                                obj->o_count = 1;
                                obj->o_group = 0;
                                cur_armor = obj;
                                add_pack(obj, TRUE);
                            } break;
                            default:
                                illcom(ch);
                        }
                    } else {
                        illcom(ch);
                    }
            }
            /*
             * turn off flags if no longer needed
             */
            if(!running) {
                door_stop = FALSE;
            }
        }
        /*
         * If he ran into something to take, let him pick it up.
         */
        if(take != 0) {
            pick_up(take);
        }
        if(!running) {
            door_stop = FALSE;
        }
        if(!after) {
            ntimes++;
        }
    }
    do_daemons(AFTER);
    do_fuses(AFTER);
    if(ISRING(LEFT, R_SEARCH)) {
        search();
    } else if(ISRING(LEFT, R_TELEPORT) && rnd(50) == 0) {
        teleport();
    }
    if(ISRING(RIGHT, R_SEARCH)) {
        search();
    } else if(ISRING(RIGHT, R_TELEPORT) && rnd(50) == 0) {
        teleport();
    }
}

/*
 * illcom:
 *     What to do with an illegal command
 */
void illcom(char ch) {
    save_msg = FALSE;
    count = 0;
    msg("illegal command '%s'", unctrol(ch));
    save_msg = TRUE;
}

/*
 * search:
 *     Player gropes about him to find hidden things.
 */
void search() {
    int y, x;
    char *fp;
    int ey, ex;

    if(on(player, ISBLIND)) {
        return;
    }
    ey = hero.y + 1;
    ex = hero.x + 1;
    for(y = hero.y - 1; y <= ey; y++) {
        for(x = hero.x - 1; x <= ex; x++) {
            if(y == hero.y && x == hero.x) {
                continue;
            }
            fp = &flat(y, x);
            if(!(*fp & F_REAL)) {
                switch(chat(y, x)) {
                    case '|':
                    case '-':
                        if(rnd(5) != 0) {
                            break;
                        }
                        chat(y, x) = DOOR;
                        *fp |= F_REAL;
                        count = running = FALSE;
                        break;
                    case FLOOR:
                        if(rnd(2) != 0) {
                            break;
                        }
                        chat(y, x) = TRAP;
                        *fp |= F_REAL;
                        count = running = FALSE;
                        msg("%s%s", terse ? "" : "you found ", tr_name(*fp & F_TMASK));
                        break;
                }
            }
        }
    }
}

/*
 * help:
 *     Give single character help, or the whole mess if he wants it
 */
void help() {
    const h_list *strp = helpstr;
    char helpch;
    int cnt;

    msg("character you want help for (* for all): ");
    helpch = (char)readchar();
    mpos = 0;
    /*
     * If its not a *, print the right help string
     * or an error if he typed a funny character.
     */
    if(helpch != '*') {
        move(0, 0);
        while(strp->h_ch) {
            if(strp->h_ch == helpch) {
                msg("%s%s", unctrol(strp->h_ch), strp->h_desc);
                break;
            }
            strp++;
        }
        if(strp->h_ch != helpch) {
            msg("unknown character '%s'", unctrol(helpch));
        }
        return;
    }
    /*
     * Here we print help for everything.
     * Then wait before we return to command mode
     */
    wclear(hw);
    cnt = 0;
    while(strp->h_ch) {
        mvwaddstr(hw, cnt % 23, cnt > 22 ? 40 : 0, unctrol(strp->h_ch));
        waddstr(hw, strp->h_desc);
        cnt++;
        strp++;
    }
    wmove(hw, LINES - 1, 0);
    wprintw(hw, "--Press space to continue--");
    wrefresh(hw);
    w_wait_for(hw, ' ');
    wmove(stdscr, 0, 0);
    wclrtoeol(stdscr);
    touchwin(stdscr);
    clearok(stdscr, TRUE);
    refresh();
}

/*
 * identify:
 *     Tell the player what a certain thing is.
 */
void identify() {
    char ch;
    const char *str;

    msg("what do you want identified? ");
    ch = (char)readchar();
    mpos = 0;
    if(ch == ESCAPE) {
        msg("");
        return;
    }
    if(isupper(ch)) {
        str = monsters[ch - 'A'].m_name;
    } else {
        switch(ch) {
            case '|':
            case '-':
                str = "wall of a room";
                break;
            case GOLD:
                str = "gold";
                break;
            case STAIRS:
                str = "a staircase";
                break;
            case DOOR:
                str = "door";
                break;
            case FLOOR:
                str = "room floor";
                break;
            case PLAYER:
                str = "you";
                break;
            case PASSAGE:
                str = "passage";
                break;
            case TRAP:
                str = "trap";
                break;
            case POTION:
                str = "potion";
                break;
            case SCROLL:
                str = "scroll";
                break;
            case FOOD:
                str = "food";
                break;
            case WEAPON:
                str = "weapon";
                break;
            case ' ':
                str = "solid rock";
                break;
            case ARMOR:
                str = "armor";
                break;
            case AMULET:
                str = "the Amulet of Yendor";
                break;
            case RING:
                str = "ring";
                break;
            case STICK:
                str = "wand or staff";
                break;
            default:
                str = "unknown character";
        }
    }
    msg("'%s': %s", unctrol(ch), str);
}

/*
 * d_level:
 *     He wants to go down a level
 */
void d_level() {
    if(chat(hero.y, hero.x) != STAIRS) {
        msg("I see no way down");
    } else {
        level++;
        new_level();
    }
}

/*
 * u_level:
 *     He wants to go up a level
 */
void u_level() {
    if(chat(hero.y, hero.x) == STAIRS) {
        if(amulet) {
            level--;
            if(level == 0) {
                total_winner();
            }
            new_level();
            msg("you feel a wrenching sensation in your gut");
        } else {
            msg("your way is magically blocked");
        }
    } else {
        msg("I see no way up");
    }
}

/*
 * call:
 *     Allow a user to call a potion, scroll, or ring something
 */
void call() {
    THING *obj;
    char **guess;
    const char *elsewise;
    bool *know;

    obj = get_item("call", CALLABLE);
    /*
     * Make certain that it is somethings that we want to wear
     */
    if(obj == nullptr) {
        return;
    }
    switch(obj->o_type) {
        case RING:
            guess = r_guess;
            know = r_know;
            elsewise =
                (r_guess[obj->o_which] != nullptr ? r_guess[obj->o_which] : r_stones[obj->o_which]);
            break;
        case POTION:
            guess = p_guess;
            know = p_know;
            elsewise =
                (p_guess[obj->o_which] != nullptr ? p_guess[obj->o_which] : p_colors[obj->o_which]);
            break;
        case SCROLL:
            guess = s_guess;
            know = s_know;
            elsewise =
                (s_guess[obj->o_which] != nullptr ? s_guess[obj->o_which] : s_names[obj->o_which]);
            break;
        case STICK:
            guess = ws_guess;
            know = ws_know;
            elsewise = (ws_guess[obj->o_which] != nullptr ? ws_guess[obj->o_which]
                                                          : ws_made[obj->o_which]);
            break;
        default:
            msg("you can't call that anything");
            return;
    }
    if(know[obj->o_which]) {
        msg("that has already been identified");
        return;
    }
    if(!terse) {
        addmsg("Was ");
    }
    msg("called \"%s\"", elsewise);
    if(terse) {
        msg("call it: ");
    } else {
        msg("what do you want to call it? ");
    }
    if(guess[obj->o_which] != nullptr) {
        free(guess[obj->o_which]);
    }
    strcpy(prbuf, elsewise);
    if(get_str(prbuf, stdscr) == NORM) {
        guess[obj->o_which] = (char *)malloc(strlen(prbuf) + 1);
        strcpy(guess[obj->o_which], prbuf);
    }
}
