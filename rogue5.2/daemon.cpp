/*
 * Contains functions for dealing with things that happen in the
 * future.
 *
 * @(#)daemon.c     4.4 (Berkeley) 1/12/82
 *
 * Rogue: Exploring the Dungeons of Doom
 * Copyright (C) 1980, 1981, 1982 Michael Toy, Ken Arnold and Glenn Wichman
 * All rights reserved.
 *
 * See the file LICENSE.TXT for full copyright and licensing information.
 */

#include "rogue.h"

static const int EMPTY = 0;
static const int DAEMON = -1;

delayed_action d_list[MAXDAEMONS] = {
    {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {},
};

/*
 * d_slot:
 *     Find an empty slot in the daemon/fuse list
 */
delayed_action *d_slot() {
    int i;
    delayed_action *dev;

    for(i = 0, dev = d_list; i < MAXDAEMONS; i++, dev++) {
        if(dev->d_type == EMPTY) {
            return dev;
        }
    }
    debug("Ran out of fuse slots");
    return nullptr;
}

/*
 * find_slot:
 *     Find a particular slot in the table
 */
delayed_action *find_slot(enum daemon_id did) {
    int i;
    delayed_action *dev;

    for(i = 0, dev = d_list; i < MAXDAEMONS; i++, dev++) {
        if(dev->d_type != EMPTY && did == dev->d_id) {
            return dev;
        }
    }
    return nullptr;
}

/*
 * start_daemon:
 *     Start a daemon, takes a function.
 */
void start_daemon(enum daemon_id did, int arg, int type) {
    delayed_action *dev;

    dev = d_slot();
    dev->d_type = type;
    dev->d_id = did;
    dev->d_arg = arg;
    dev->d_time = DAEMON;
}

/*
 * kill_daemon:
 *     Remove a daemon from the list
 */
void kill_daemon(enum daemon_id did) {
    delayed_action *dev;

    if((dev = find_slot(did)) == nullptr) {
        return;
    }
    /*
     * Take it out of the list
     */
    dev->d_type = EMPTY;
}

void execute_daemon(delayed_action *dev) {
    switch(dev->d_id) {
        case d_none:
            // nothing to do
            break;
        case d_rollwand:
            rollwand();
            break;
        case d_doctor:
            doctor();
            break;
        case d_stomach:
            stomach();
            break;
        case d_runners:
            runners();
            break;
        case d_swander:
            swander();
            break;
        case d_nohste:
            nohaste();
            break;
        case d_unconfuse:
            unconfuse();
            break;
        case d_unsee:
            unsee();
            break;
        case d_sight:
            sight();
            break;
        default:
            errormsg("daemon type not defined");
            //(*dev->d_func)(dev->d_arg);
    }
}

/*
 * do_daemons:
 *     Run all the daemons that are active with the current flag,
 *     passing the argument to the function.
 */
void do_daemons(int flag) {
    delayed_action *dev;

    /*
     * Loop through the devil list
     */
    for(dev = d_list; dev <= &d_list[MAXDAEMONS - 1]; dev++) {
        /*
         * Executing each one, giving it the proper arguments
         */
        if(dev->d_type == flag && dev->d_time == DAEMON) {
            execute_daemon(dev);
        }
    }
}

/*
 * fuse:
 *     Start a fuse to go off in a certain number of turns
 */
void fuse(enum daemon_id did, int arg, int time, int type) {
    delayed_action *wire;

    wire = d_slot();
    wire->d_type = type;
    wire->d_id = did;
    wire->d_arg = arg;
    wire->d_time = time;
}

/*
 * lengthen:
 *     Increase the time until a fuse goes off
 */
void lengthen(enum daemon_id did, int xtime) {
    delayed_action *wire;

    if((wire = find_slot(did)) == nullptr) {
        return;
    }
    wire->d_time += xtime;
}

/*
 * extinguish:
 *     Put out a fuse
 */
void extinguish(enum daemon_id did) {
    delayed_action *wire;

    if((wire = find_slot(did)) == nullptr) {
        return;
    }
    wire->d_type = EMPTY;
}

/*
 * do_fuses:
 *     Decrement counters and start needed fuses
 */
void do_fuses(int flag) {
    delayed_action *wire;

    /*
     * Step though the list
     */
    for(wire = d_list; wire <= &d_list[MAXDAEMONS - 1]; wire++) {
        /*
         * Decrementing counters and starting things we want.  We also need
         * to remove the fuse from the list once it has gone off.
         */
        if(flag == wire->d_type && wire->d_time > 0 && --wire->d_time == 0) {
            wire->d_type = EMPTY;
            execute_daemon(wire);
        }
    }
}
