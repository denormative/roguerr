/*
 * Various installation dependent routines
 *
 * @(#)mach_dep.c     4.23 (Berkeley) 5/19/82
 *
 * Rogue: Exploring the Dungeons of Doom
 * Copyright (C) 1980, 1981, 1982 Michael Toy, Ken Arnold and Glenn Wichman
 * All rights reserved.
 *
 * See the file LICENSE.TXT for full copyright and licensing information.
 */

/*
 * The various tuneable defines are:
 *
 *     SCOREFILE     Where/if the score file should live.
 */

#include "rogue.h"
#include <climits>
#include <csignal>
#include <cstdio>
#include <ctime>
#include <fcntl.h>
#include <sys/stat.h>

/*
 * open_score:
 *     Open up the score file for future use.
 */
void open_score() {
#ifdef SCOREFILE
    fd = open(SCOREFILE, O_RDWR | O_CREAT, 0666);
#else
    fd = -1;
#endif
}

/*
 * setup:
 *     Get starting setup for all games
 */
void setup() {
    /*
     * make sure that large terminals don't overflow the bounds
     * of the program
     */
    if(LINES > MAXLINES) {
        LINES = MAXLINES;
    }
    if(COLS > MAXCOLS) {
        COLS = MAXCOLS;
    }

#ifdef SIGHUP
    signal(SIGHUP, auto_save);
#endif
#ifndef DUMP
    signal(SIGILL, auto_save);
#ifdef SIGTRAP
    signal(SIGTRAP, auto_save);
#endif
#ifdef SIGIOT
    signal(SIGIOT, auto_save);
#endif
#ifdef SIGEMT
    signal(SIGEMT, auto_save);
#endif
    signal(SIGFPE, auto_save);
#ifdef SIGBUS
    signal(SIGBUS, auto_save);
#endif
    signal(SIGSEGV, auto_save);
#ifdef SIGSYS
    signal(SIGSYS, auto_save);
#endif
    signal(SIGTERM, auto_save);
#endif

    signal(SIGINT, quit);
#ifndef DUMP
#ifdef SIGQUIT
    signal(SIGQUIT, endit);
#endif
#endif
    crmode(); /* Cbreak mode */
    noecho(); /* Echo off */
}

/*
 * issymlink:
 *     See if the file has a symbolic link
 */
int issymlink(char *sp) {
#ifdef S_IFLNK
    struct stat sbuf2;

    if(lstat(sp, &sbuf2) < 0) {
        return FALSE;
    }
    return ((sbuf2.st_mode & S_IFMT) != S_IFREG);

#else
    return FALSE;
#endif
}

/*
 * flush_type:
 *     Flush typeahead for traps, etc.
 */
void flush_type() {
    flushinp();
}
