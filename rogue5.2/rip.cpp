/*
 * File for the fun ends
 * Death or a total win
 *
 * @(#)rip.c     4.28 (Berkeley) 4/12/82
 *
 * Rogue: Exploring the Dungeons of Doom
 * Copyright (C) 1980, 1981, 1982 Michael Toy, Ken Arnold and Glenn Wichman
 * All rights reserved.
 *
 * See the file LICENSE.TXT for full copyright and licensing information.
 */

#include "Scores.h"
#include <fmt/format.h>
#include "rogue.h"
#include <csignal>
#include <ctime>
#include <sys/types.h>

static const char *rip[] = {"                       __________",
                            "                      /          \\",
                            "                     /    REST    \\",
                            "                    /      IN      \\",
                            "                   /     PEACE      \\",
                            "                  /                  \\",
                            "                  |                  |",
                            "                  |                  |",
                            "                  |   killed by a    |",
                            "                  |                  |",
                            "                  |       1980       |",
                            "                 *|     *  *  *      | *",
                            R"(         ________)/\\_//(\/(/\)/\//\/|_)_______)",
                            nullptr};

ScoreEntry recordScore(const int amount, const int flags, const int monst) {
    ScoreEntry entry{};

    static const char *reason[] = {
        "killed",
        "quit",
        "A total winner",
    };

    entry.sc_score = amount;
    entry.sc_name = md_getusername();
    if(flags == 2) {
        entry.sc_level = max_level;
    } else {
        entry.sc_level = level;
    }
    entry.sc_monster = monst;
    entry.sc_flags = flags;
    entry.sc_reason = reason[flags];
    entry.sc_reason += fmt::format(" on level {}", entry.sc_level);
    if(flags == 0) {
        const char *killer = killname((char)monst, TRUE);
        entry.sc_reason += " by ";
        entry.sc_reason += killer;
    }
    entry.was_wizard = wizard;
    // TODO: probably should note user was wizard on scoreboard

    return entry;
}

void printScores(const Scores &scores) {
    printf("\n\nTop Ten Adventurers:\nRank\tScore\tName\n");
    int rank = 1;
    for(auto &e : scores.scores) {
        printf("%d\t%ld\t%s: %s.\n", rank, e.sc_score, e.sc_name.c_str(), e.sc_reason.c_str());
        rank++;
    }
}

/*
 * score:
 *     Figure score and post it.
 */
void score(int amount, int flags, char monst) {
    signal(SIGINT, SIG_DFL);
    if(flags != -1 || wizard) {
        mvaddstr(LINES - 1, 0, "[Press return to continue]");
        refresh();
        wgetnstr(stdscr, prbuf, 80);
        move(LINES - 1, 0);
        clrtoeol();
        refresh();
        endwin();
    }

    Scores scores("rogue52.scores");
    scores.load();

    ScoreEntry entry = recordScore(amount, flags, monst);

    scores.addScore(entry);
    scores.save();

    printScores(scores);
}

/*
 * death:
 *     Do something really fun when he dies
 */
int death(char monst) {
    const char **dp = rip;
    char *killer;
    struct tm *lt;
    time_t date;
    char buf[MAXSTR];

    signal(SIGINT, SIG_IGN);
    purse -= purse / 10;
    signal(SIGINT, leave);
    time(&date);
    lt = localtime(&date);
    clear();
    move(8, 0);
    while(*dp) {
        printw("%s\n", *dp++);
    }
    mvaddstr(14, (int)(28 - ((strlen(whoami) + 1) / 2)), whoami);
    sprintf(buf, "%d Au", purse);
    mvaddstr(15, (int)(28 - ((strlen(buf) + 1) / 2)), buf);
    killer = killname(monst, FALSE);
    mvaddstr(17, (int)(28 - ((strlen(killer) + 1) / 2)), killer);
    if(monst == 's') {
        mvaddch(16, 32, ' ');
    } else {
        mvaddstr(16, 33, vowelstr(killer));
    }
    sprintf(prbuf, "%2d", 1900 + lt->tm_year);
    mvaddstr(18, 26, prbuf);
    move(LINES - 1, 0);
    refresh();
    score(purse, 0, monst);
    exit(0);
}

/*
 * total_winner:
 *     Code for a winner
 */
[[noreturn]] void total_winner() {
    THING *obj;
    int worth = 0;
    char c;
    int oldpurse;

    clear();
    standout();
    addstr("                                                               \n");
    addstr("  @   @               @   @           @          @@@  @     @  \n");
    addstr("  @   @               @@ @@           @           @   @     @  \n");
    addstr("  @   @  @@@  @   @   @ @ @  @@@   @@@@  @@@      @  @@@    @  \n");
    addstr("   @@@@ @   @ @   @   @   @     @ @   @ @   @     @   @     @  \n");
    addstr("      @ @   @ @   @   @   @  @@@@ @   @ @@@@@     @   @     @  \n");
    addstr("  @   @ @   @ @  @@   @   @ @   @ @   @ @         @   @  @     \n");
    addstr("   @@@   @@@   @@ @   @   @  @@@@  @@@@  @@@     @@@   @@   @  \n");
    addstr("                                                               \n");
    addstr("     Congratulations, you have made it to the light of day!    \n");
    standend();
    addstr("\nYou have joined the elite ranks of those who have escaped the\n");
    addstr("Dungeons of Doom alive.  You journey home and sell all your loot at\n");
    addstr("a great profit and are admitted to the fighters guild.\n");
    mvaddstr(LINES - 1, 0, "--Press space to continue--");
    refresh();
    wait_for(' ');
    clear();
    mvaddstr(0, 0, "   Worth  Item");
    oldpurse = purse;
    for(c = 'a', obj = pack; obj != nullptr; c++, obj = next(obj)) {
        switch(obj->o_type) {
            case FOOD:
                worth = 2 * obj->o_count;
                break;
            case WEAPON:
                switch(obj->o_which) {
                    case MACE:
                        worth = 8;
                        break;
                    case SWORD:
                        worth = 15;
                        break;
                    case CROSSBOW:
                        worth = 30;
                        break;
                    case ARROW:
                        worth = 1;
                        break;
                    case DAGGER:
                        worth = 2;
                        break;
                    case TWOSWORD:
                        worth = 75;
                        break;
                    case DART:
                        worth = 1;
                        break;
                    case BOW:
                        worth = 15;
                        break;
                    case BOLT:
                        worth = 1;
                        break;
                    case SPEAR:
                        worth = 5;
                }
                worth *= 3 * (obj->o_hplus + obj->o_dplus) + obj->o_count;
                obj->o_flags |= ISKNOW;
                break;
            case ARMOR:
                switch(obj->o_which) {
                    case LEATHER:
                        worth = 20;
                        break;
                    case RING_MAIL:
                        worth = 25;
                        break;
                    case STUDDED_LEATHER:
                        worth = 20;
                        break;
                    case SCALE_MAIL:
                        worth = 30;
                        break;
                    case CHAIN_MAIL:
                        worth = 75;
                        break;
                    case SPLINT_MAIL:
                        worth = 80;
                        break;
                    case BANDED_MAIL:
                        worth = 90;
                        break;
                    case PLATE_MAIL:
                        worth = 150;
                }
                worth += (9 - obj->o_ac) * 100;
                worth += (10 * (a_class[obj->o_which] - obj->o_ac));
                obj->o_flags |= ISKNOW;
                break;
            case SCROLL:
                worth = s_magic[obj->o_which].mi_worth;
                worth *= obj->o_count;
                if(!s_know[obj->o_which]) {
                    worth /= 2;
                }
                s_know[obj->o_which] = TRUE;
                break;
            case POTION:
                worth = p_magic[obj->o_which].mi_worth;
                worth *= obj->o_count;
                if(!p_know[obj->o_which]) {
                    worth /= 2;
                }
                p_know[obj->o_which] = TRUE;
                break;
            case RING:
                worth = r_magic[obj->o_which].mi_worth;
                if(obj->o_which == R_ADDSTR || obj->o_which == R_ADDDAM ||
                   obj->o_which == R_PROTECT || obj->o_which == R_ADDHIT) {
                    if(obj->o_ac > 0) {
                        worth += obj->o_ac * 100;
                    } else {
                        worth = 10;
                    }
                }
                if(!(obj->o_flags & ISKNOW)) {
                    worth /= 2;
                }
                obj->o_flags |= ISKNOW;
                r_know[obj->o_which] = TRUE;
                break;
            case STICK:
                worth = ws_magic[obj->o_which].mi_worth;
                worth += 20 * obj->o_charges;
                if(!(obj->o_flags & ISKNOW)) {
                    worth /= 2;
                }
                obj->o_flags |= ISKNOW;
                ws_know[obj->o_which] = TRUE;
                break;
            case AMULET:
                worth = 1000;
        }
        if(worth < 0) {
            worth = 0;
        }
        mvprintw(c - 'a' + 1, 0, "%c) %5d  %s", c, worth, inv_name(obj, FALSE));
        purse += worth;
    }
    mvprintw(c - 'a' + 1, 0, "   %5d  Gold Pieces          ", oldpurse);
    refresh();
    score(purse, 2, 0);
    exit(0);
}

/*
 * killname:
 *     Convert a code to a monster name
 */
char *killname(char monst, bool doart) {
    const char *sp;
    bool article;

    sp = prbuf;
    article = TRUE;
    switch(monst) {
        case 'a':
            sp = "arrow";
            break;
        case 'b':
            sp = "bolt";
            break;
        case 'd':
            sp = "dart";
            break;
        case 's':
            sp = "starvation";
            article = FALSE;
            break;
        default:
            if(monst >= 'A' && monst <= 'Z') {
                sp = monsters[monst - 'A'].m_name;
            } else {
                sp = "God";
                article = FALSE;
            }
    }
    if(doart && article) {
        sprintf(prbuf, "a%s ", vowelstr(sp));
    } else {
        prbuf[0] = '\0';
    }
    strcat(prbuf, sp);
    return prbuf;
}
