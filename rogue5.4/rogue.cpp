/*
 * global variable initializaton
 *
 * @(#)extern.c     4.82 (Berkeley) 02/05/99
 *
 * Rogue: Exploring the Dungeons of Doom
 * Copyright (C) 1980-1983, 1985, 1999 Michael Toy, Ken Arnold and Glenn Wichman
 * All rights reserved.
 *
 * See the file LICENSE.TXT for full copyright and licensing information.
 */

/* All global variables are defined here, or in vers.c or init.c */

#include "rogue.h"

int after;               /* True if we want after daemons */
int again;               /* Repeating the last command */
int noscore;             /* Was a wizard sometime */
int seenstairs;          /* Have seen the stairs (for lsd) */
int amulet = FALSE;      /* He found the amulet */
int door_stop = FALSE;   /* Stop running when we pass a door */
int fight_flush = FALSE; /* True if toilet input */
int firstmove = FALSE;   /* First move after setting door_stop */
int has_hit = FALSE;     /* Has a "hit" message pending in msg */
int inv_describe = TRUE; /* Say which way items are being used */
int jump = FALSE;        /* Show running as series of jumps */
int kamikaze = FALSE;    /* to_death really to DEATH */
int lower_msg = FALSE;   /* Messages should start w/lower case */
int move_on = FALSE;     /* Next move shouldn't pick up items */
int msg_esc = FALSE;     /* Check for ESC from msg's --More-- */
int passgo = FALSE;      /* Follow passages */
int playing = TRUE;      /* True until he quits */
int q_comm = FALSE;      /* Are we executing a 'Q' command? */
int running = FALSE;     /* True if player is running */
int save_msg = TRUE;     /* Remember last msg */
int see_floor = TRUE;    /* Show the lamp illuminated floor */
int stat_msg = FALSE;    /* Should status() print as a msg() */
int terse = FALSE;       /* True if we should be short */
int to_death = FALSE;    /* Fighting is to the death! */
int tombstone = TRUE;    /* Print out tombstone at end */
int wizard = FALSE;      /* True if allows wizard commands */
int pack_used[26] = {    /* Is the character used in the pack? */
                     FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE,
                     FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE,
                     FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE};

int dir_ch;                       /* Direction from last get_dir() call */
char file_name[MAXSTR];           /* Save file name */
char huh[MAXSTR];                 /* The last message printed */
const char *p_colors[MAXPOTIONS]; /* Colors of the potions */
char prbuf[2 * MAXSTR];           /* buffer for sprintfs */
const char *r_stones[MAXRINGS];   /* Stone settings of the rings */
int runch;                        /* Direction player is running */
char *s_names[MAXSCROLLS];        /* Names of the scrolls */
int take;                         /* Thing she is taking */
char whoami[MAXSTR];              /* Name of player */
const char *ws_made[MAXSTICKS];   /* What sticks are made of */
const char *ws_type[MAXSTICKS];   /* Is it a wand or a staff */
char fruit[MAXSTR] =              /* Favorite fruit */
    {'s', 'l', 'i', 'm', 'e', '-', 'm', 'o', 'l', 'd', '\0'};
char home[MAXSTR] = {'\0'}; /* User's home directory */
const char *inv_t_name[] = {"Overwrite", "Slow", "Clear"};
int l_last_comm = '\0'; /* Last last_comm */
int l_last_dir = '\0';  /* Last last_dir */
int last_comm = '\0';   /* Last command typed */
int last_dir = '\0';    /* Last direction given */
const char *tr_name[] = {
    /* Names of the traps */
    // clang-format off
    "a trapdoor",
    "an arrow trap",
    "a sleeping gas trap",
    "a beartrap",
    "a teleport trap",
    "a poison dart trap",
    "a rust trap",
    "a mysterious trap"
    // clang-format on
};

int n_objs;           /* # items listed in inventory() call */
int ntraps;           /* Number of traps on this level */
int hungry_state = 0; /* How hungry is he */
int inpack = 0;       /* Number of things in pack */
int inv_type = 0;     /* Type of inventory to use */
int level = 1;        /* What level she is on */
int max_hit;          /* Max damage done to her in to_death */
int max_level;        /* Deepest player has gone */
size_t mpos = 0;      /* Where cursor is on top line */
int no_food = 0;      /* Number of levels without food */
const int a_class[MAXARMORS] = {
    /* Armor class for each armor type */
    8, /* LEATHER */
    7, /* RING_MAIL */
    7, /* STUDDED_LEATHER */
    6, /* SCALE_MAIL */
    5, /* CHAIN_MAIL */
    4, /* SPLINT_MAIL */
    4, /* BANDED_MAIL */
    3, /* PLATE_MAIL */
};

int count = 0;              /* Number of times to repeat command */
FILE *scoreboard = nullptr; /* File descriptor for score file */
int food_left;              /* Amount of food in hero's stomach */
int lastscore = -1;         /* Score before this turn */
int no_command = 0;         /* Number of turns asleep */
int no_move = 0;            /* Number of turns held in place */
int purse = 0;              /* How much gold he has */
int quiet = 0;              /* Number of quiet turns */
int vf_hit = 0;             /* Number of time flytrap has hit */

unsigned int dnum; /* Dungeon number */
unsigned int seed; /* Random number seed */
const int e_levels[] = {10L,     20L,     40L,     80L,      160L,     320L,     640L,
                        1300L,   2600L,   5200L,   13000L,   26000L,   50000L,   100000L,
                        200000L, 400000L, 800000L, 2000000L, 4000000L, 8000000L, 0L};

coord delta;  /* Change indicated to get_dir() */
coord oldpos; /* Position before last look() call */
coord stairs; /* Location of staircase */

PLACE places[MAXLINES * MAXCOLS]; /* level map */

THING *cur_armor;             /* What he is wearing */
THING *cur_ring[2];           /* Which rings are being worn */
THING *cur_weapon;            /* Which weapon he is weilding */
THING *l_last_pick = nullptr; /* Last last_pick */
THING *last_pick = nullptr;   /* Last object picked in get_item() */
THING *lvl_obj = nullptr;     /* List of objects on this level */
THING *mlist = nullptr;       /* List of monsters on the level */
THING player;                 /* His stats */
/* restart of game */

WINDOW *hw = nullptr; /* used as a scratch window */

#define INIT_STATS                                                                                 \
    { 16, 0, 1, 10, 12, "1x4", 12 }

stats max_stats = INIT_STATS; /* The maximum for the player */

room *oldrp;          /* Roomin(&oldpos) */
room rooms[MAXROOMS]; /* One for each room -- A level */
room passages[MAXPASS] = {
    /* One for each passage */
    // clang-format off
    { {0, 0}, {0, 0}, {0, 0}, 0, ISGONE | ISDARK, 0, {{0, 0}} },
    { {0, 0}, {0, 0}, {0, 0}, 0, ISGONE | ISDARK, 0, {{0, 0}} },
    { {0, 0}, {0, 0}, {0, 0}, 0, ISGONE | ISDARK, 0, {{0, 0}} },
    { {0, 0}, {0, 0}, {0, 0}, 0, ISGONE | ISDARK, 0, {{0, 0}} },
    { {0, 0}, {0, 0}, {0, 0}, 0, ISGONE | ISDARK, 0, {{0, 0}} },
    { {0, 0}, {0, 0}, {0, 0}, 0, ISGONE | ISDARK, 0, {{0, 0}} },
    { {0, 0}, {0, 0}, {0, 0}, 0, ISGONE | ISDARK, 0, {{0, 0}} },
    { {0, 0}, {0, 0}, {0, 0}, 0, ISGONE | ISDARK, 0, {{0, 0}} },
    { {0, 0}, {0, 0}, {0, 0}, 0, ISGONE | ISDARK, 0, {{0, 0}} },
    { {0, 0}, {0, 0}, {0, 0}, 0, ISGONE | ISDARK, 0, {{0, 0}} },
    { {0, 0}, {0, 0}, {0, 0}, 0, ISGONE | ISDARK, 0, {{0, 0}} },
    { {0, 0}, {0, 0}, {0, 0}, 0, ISGONE | ISDARK, 0, {{0, 0}} }
    // clang-format on

};

#define X1 1
#define X10 10
#define UNUSED_MAXHP 0
monster monsters[26] = {
    // clang-format off
    /* Name          CARRY     FLAG    str, exp, lvl, amr, hpt, dmg */
    { "aquator",       0,     ISMEAN,    { X10, 20,   5,   2, X1, "0x0/0x0", UNUSED_MAXHP } },
    { "bat",       0,     ISFLY,    { X10,  1,   1,   3, X1, "1x2", UNUSED_MAXHP } },
    { "centaur",      15,     0,    { X10, 17,   4,   4, X1, "1x2/1x5/1x5", UNUSED_MAXHP } },
    { "dragon",     100,     ISMEAN,    { X10, 5000, 10,  -1, X1, "1x8/1x8/3x10", UNUSED_MAXHP } },
    { "emu",       0,     ISMEAN,    { X10,  2,   1,   7, X1, "1x2", UNUSED_MAXHP } },
    { "venus flytrap", 0,     ISMEAN,    { X10, 80,   8,   3, X1, "%%%x0", UNUSED_MAXHP } },
    /* NOTE: the damage is %%% so that xstr won't merge this */
    /* string with others, since it is written on in the program */
    { "griffin",      20,     ISMEAN | ISFLY | ISREGEN, { X10, 2000, 13,   2, X1, "4x3/3x5", UNUSED_MAXHP } },
    { "hobgoblin",       0,     ISMEAN,    { X10,  3,   1,   5, X1, "1x8", UNUSED_MAXHP } },
    { "ice monster",   0,     0,    { X10,  5,   1,   9, X1, "0x0", UNUSED_MAXHP } },
    { "jabberwock",   70,     0,    { X10, 3000, 15,   6, X1, "2x12/2x4", UNUSED_MAXHP } },
    { "kestrel",       0,     ISMEAN | ISFLY,    { X10,  1,   1,   7, X1, "1x4", UNUSED_MAXHP } },
    { "leprechaun",       0,     0,    { X10, 10,   3,   8, X1, "1x1", UNUSED_MAXHP } },
    { "medusa",      40,     ISMEAN,    { X10, 200,   8,   2, X1, "3x4/3x4/2x5", UNUSED_MAXHP } },
    { "nymph",     100,     0,    { X10, 37,   3,   9, X1, "0x0", UNUSED_MAXHP } },
    { "orc",      15,     ISGREED, { X10,  5,   1,   6, X1, "1x8", UNUSED_MAXHP } },
    { "phantom",       0,     ISINVIS, { X10, 120,   8,   3, X1, "4x4", UNUSED_MAXHP } },
    { "quagga",       0,     ISMEAN,    { X10, 15,   3,   3, X1, "1x5/1x5", UNUSED_MAXHP } },
    { "rattlesnake",   0,     ISMEAN,    { X10,  9,   2,   3, X1, "1x6", UNUSED_MAXHP } },
    { "snake",       0,     ISMEAN,    { X10,  2,   1,   5, X1, "1x3", UNUSED_MAXHP } },
    { "troll",      50,     ISREGEN | ISMEAN, { X10, 120, 6, 4, X1, "1x8/1x8/2x6", UNUSED_MAXHP } },
    { "black unicorn", 0,     ISMEAN,    { X10, 190,   7,  -2, X1, "1x9/1x9/2x9", UNUSED_MAXHP } },
    { "vampire",      20,     ISREGEN | ISMEAN, { X10, 350,   8,   1, X1, "1x10", UNUSED_MAXHP } },
    { "wraith",       0,     0,    { X10, 55,   5,   4, X1, "1x6", UNUSED_MAXHP } },
    { "xeroc",      30,     0,    { X10, 100,   7,   7, X1, "4x4", UNUSED_MAXHP } },
    { "yeti",      30,     0,    { X10, 50,   4,   6, X1, "1x6/1x6", UNUSED_MAXHP } },
    { "zombie",       0,     ISMEAN,    { X10,  6,   2,   8, X1, "1x8", UNUSED_MAXHP } }
    // clang-format on
};
#undef X1
#undef X10

obj_info things[NUMTHINGS] = {
    // clang-format off
    { nullptr, 26, 0, nullptr, 0 },    /* potion */
    { nullptr, 36, 0, nullptr, 0 },    /* scroll */
    { nullptr, 16, 0, nullptr, 0 },    /* food */
    { nullptr,  7, 0, nullptr, 0 },    /* weapon */
    { nullptr,  7, 0, nullptr, 0 },    /* armor */
    { nullptr,  4, 0, nullptr, 0 },    /* ring */
    { nullptr,  4, 0, nullptr, 0 },    /* stick */
    // clang-format on
};

// clang-format off
obj_info arm_info[MAXARMORS] = {
    { "leather armor",         20,     20, nullptr, FALSE },
    { "ring mail",         15,     25, nullptr, FALSE },
    { "studded leather armor",     15,     20, nullptr, FALSE },
    { "scale mail",         13,     30, nullptr, FALSE },
    { "chain mail",         12,     75, nullptr, FALSE },
    { "splint mail",         10,     80, nullptr, FALSE },
    { "banded mail",         10,     90, nullptr, FALSE },
    { "plate mail",          5,     150, nullptr, FALSE },
};
obj_info pot_info[MAXPOTIONS] = {
    { "confusion",         7,   5, nullptr, FALSE },
    { "hallucination",         8,   5, nullptr, FALSE },
    { "poison",             8,   5, nullptr, FALSE },
    { "gain strength",         13, 150, nullptr, FALSE },
    { "see invisible",         3, 100, nullptr, FALSE },
    { "healing",         13, 130, nullptr, FALSE },
    { "monster detection",     6, 130, nullptr, FALSE },
    { "magic detection",     6, 105, nullptr, FALSE },
    { "raise level",         2, 250, nullptr, FALSE },
    { "extra healing",         5, 200, nullptr, FALSE },
    { "haste self",         5, 190, nullptr, FALSE },
    { "restore strength",     13, 130, nullptr, FALSE },
    { "blindness",         5,   5, nullptr, FALSE },
    { "levitation",         6,  75, nullptr, FALSE },
};
obj_info ring_info[MAXRINGS] = {
    { "protection",         9, 400, nullptr, FALSE },
    { "add strength",         9, 400, nullptr, FALSE },
    { "sustain strength",     5, 280, nullptr, FALSE },
    { "searching",         10, 420, nullptr, FALSE },
    { "see invisible",         10, 310, nullptr, FALSE },
    { "adornment",         1,  10, nullptr, FALSE },
    { "aggravate monster",     10,  10, nullptr, FALSE },
    { "dexterity",         8, 440, nullptr, FALSE },
    { "increase damage",     8, 400, nullptr, FALSE },
    { "regeneration",         4, 460, nullptr, FALSE },
    { "slow digestion",         9, 240, nullptr, FALSE },
    { "teleportation",         5,  30, nullptr, FALSE },
    { "stealth",         7, 470, nullptr, FALSE },
    { "maintain armor",         5, 380, nullptr, FALSE },
};
obj_info scr_info[MAXSCROLLS] = {
    { "monster confusion",         7, 140, nullptr, FALSE },
    { "magic mapping",             4, 150, nullptr, FALSE },
    { "hold monster",             2, 180, nullptr, FALSE },
    { "sleep",                 3,   5, nullptr, FALSE },
    { "enchant armor",             7, 160, nullptr, FALSE },
    { "identify potion",         10,  80, nullptr, FALSE },
    { "identify scroll",         10,  80, nullptr, FALSE },
    { "identify weapon",         6,  80, nullptr, FALSE },
    { "identify armor",              7, 100, nullptr, FALSE },
    { "identify ring, wand or staff",     10, 115, nullptr, FALSE },
    { "scare monster",             3, 200, nullptr, FALSE },
    { "food detection",             2,  60, nullptr, FALSE },
    { "teleportation",             5, 165, nullptr, FALSE },
    { "enchant weapon",             8, 150, nullptr, FALSE },
    { "create monster",             4,  75, nullptr, FALSE },
    { "remove curse",             7, 105, nullptr, FALSE },
    { "aggravate monsters",         3,  20, nullptr, FALSE },
    { "protect armor",             2, 250, nullptr, FALSE },
};
obj_info weap_info[MAXWEAPONS + 1] = {
    { "mace",                 11,   8, nullptr, FALSE },
    { "long sword",             11,  15, nullptr, FALSE },
    { "short bow",             12,  15, nullptr, FALSE },
    { "arrow",                 12,   1, nullptr, FALSE },
    { "dagger",              8,   3, nullptr, FALSE },
    { "two handed sword",   10,  75, nullptr, FALSE },
    { "dart",                 12,   2, nullptr, FALSE },
    { "shuriken",             12,   5, nullptr, FALSE },
    { "spear",                 12,   5, nullptr, FALSE },
    { nullptr,                  0,   0, nullptr, FALSE },    /* DO NOT REMOVE: fake entry for dragon's breath */
};
obj_info ws_info[MAXSTICKS] = {
    { "light",             12, 250, nullptr, FALSE },
    { "invisibility",         6,   5, nullptr, FALSE },
    { "lightning",         3, 330, nullptr, FALSE },
    { "fire",             3, 330, nullptr, FALSE },
    { "cold",             3, 330, nullptr, FALSE },
    { "polymorph",         15, 310, nullptr, FALSE },
    { "magic missile",         10, 170, nullptr, FALSE },
    { "haste monster",         10,   5, nullptr, FALSE },
    { "slow monster",         11, 350, nullptr, FALSE },
    { "drain life",         9, 300, nullptr, FALSE },
    { "nothing",         1,   5, nullptr, FALSE },
    { "teleport away",         6, 340, nullptr, FALSE },
    { "teleport to",         6,  50, nullptr, FALSE },
    { "cancellation",         5, 280, nullptr, FALSE },
};

const h_list helpstr[] = {
    {'?',    "     prints help",                 TRUE},
    {'/',    "     identify object",             TRUE},
    {'h',    "     left",                     TRUE},
    {'j',    "     down",                     TRUE},
    {'k',    "     up",                     TRUE},
    {'l',    "     right",                     TRUE},
    {'y',    "     up & left",                 TRUE},
    {'u',    "     up & right",                 TRUE},
    {'b',    "     down & left",                 TRUE},
    {'n',    "     down & right",                 TRUE},
    {'H',    "     run left",                 FALSE},
    {'J',    "     run down",                 FALSE},
    {'K',    "     run up",                 FALSE},
    {'L',    "     run right",                 FALSE},
    {'Y',    "     run up & left",                 FALSE},
    {'U',    "     run up & right",             FALSE},
    {'B',    "     run down & left",             FALSE},
    {'N',    "     run down & right",             FALSE},
    {CTRL('H'),    "     run left until adjacent",         FALSE},
    {CTRL('J'),    "     run down until adjacent",         FALSE},
    {CTRL('K'),    "     run up until adjacent",             FALSE},
    {CTRL('L'),    "     run right until adjacent",         FALSE},
    {CTRL('Y'),    "     run up & left until adjacent",         FALSE},
    {CTRL('U'),    "     run up & right until adjacent",         FALSE},
    {CTRL('B'),    "     run down & left until adjacent",     FALSE},
    {CTRL('N'),    "     run down & right until adjacent",     FALSE},
    {'\0',    "    <SHIFT><dir>: run that way",         TRUE},
    {'\0',    "    <CTRL><dir>: run till adjacent",     TRUE},
    {'f',    "<dir>     fight till death or near death",     TRUE},
    {'t',    "<dir>     throw something",             TRUE},
    {'m',    "<dir>     move onto without picking up",         TRUE},
    {'z',    "<dir>     zap a wand in a direction",         TRUE},
    {'^',    "<dir>     identify trap type",             TRUE},
    {'s',    "     search for trap/secret door",         TRUE},
    {'>',    "     go down a staircase",             TRUE},
    {'<',    "     go up a staircase",             TRUE},
    {'.',    "     rest for a turn",             TRUE},
    {',',    "     pick something up",             TRUE},
    {'i',    "     inventory",                 TRUE},
    {'I',    "     inventory single item",             TRUE},
    {'q',    "     quaff potion",                 TRUE},
    {'r',    "     read scroll",                 TRUE},
    {'e',    "     eat food",                 TRUE},
    {'w',    "     wield a weapon",             TRUE},
    {'W',    "     wear armor",                 TRUE},
    {'T',    "     take armor off",             TRUE},
    {'P',    "     put on ring",                 TRUE},
    {'R',    "     remove ring",                 TRUE},
    {'d',    "     drop object",                 TRUE},
    {'c',    "     call object",                 TRUE},
    {'a',    "     repeat last command",             TRUE},
    {')',    "     print current weapon",             TRUE},
    {']',    "     print current armor",             TRUE},
    {'=',    "     print current rings",             TRUE},
    {'@',    "     print current stats",             TRUE},
    {'D',    "     recall what's been discovered",         TRUE},
    {'o',    "     examine/set options",             TRUE},
    {CTRL('R'),    "     redraw screen",                 TRUE},
    {CTRL('P'),    "     repeat last message",             TRUE},
    {ESCAPE,    "     cancel command",             TRUE},
    {'S',    "     save game",                 TRUE},
    {'Q',    "     quit",                     TRUE},
    {'F',    "<dir>     fight till either of you dies",         TRUE},
    {'v',    "     print version number",             TRUE},
    {0,         nullptr, FALSE }
};
// clang-format on
size_t numscores;
const char *Numname;
int allscore;
int between;

#define X_EMPTY                                                                                    \
    {}

delayed_action d_list[MAXDAEMONS] = {
    X_EMPTY, X_EMPTY, X_EMPTY, X_EMPTY, X_EMPTY, X_EMPTY, X_EMPTY, X_EMPTY, X_EMPTY, X_EMPTY,
    X_EMPTY, X_EMPTY, X_EMPTY, X_EMPTY, X_EMPTY, X_EMPTY, X_EMPTY, X_EMPTY, X_EMPTY, X_EMPTY,
};

int group = 2;

const char *release = "5.4.5";
const char encstr[] =
    "\300k||`\251Y.'\305\321\201+\277~r\"]\240_\223=1\341)\222\212\241t;\t$\270\314/<#\201\254";
const char statlist[] =
    "\355kl{+\204\255\313idJ\361\214=4:\311\271\341wK<\312\321\213,,7\271/Rk%\b\312\f\246";
const char version[] = "rogue (rogueforge) 09/05/07";
