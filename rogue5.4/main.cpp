/*
 * Rogue: Exploring the Dungeons of Doom
 * Copyright (C) 1980-1983, 1985, 1999 Michael Toy, Ken Arnold and Glenn Wichman
 * All rights reserved.
 *
 * See the file LICENSE.TXT for full copyright and licensing information.
 *
 * @(#)main.c     4.22 (Berkeley) 02/05/99
 */

#include "rogue.h"
#include <csignal>
#include <ctime>

/*
 * main:
 *     The main program, of course
 */
int main(int argc, char **argv) {
    char *env;
    time_t lowtime;

    md_init();

    /*
     * Check to see if he is a wizard
     */
    if(argc >= 2 && argv[1][0] == '\0') {
        if(strcmp(PASSWD, md_crypt(md_getpass("wizard's password: "))) == 0) {
            wizard = TRUE;
            player.t_flags |= SEEMONST;
            argv++;
            argc--;
        }
    }

    /*
     * get home and options from environment
     */

    strcpy(home, md_gethomedir());

    if(strlen(home) > MAXSTR - strlen("rogue.save") - 1) {
        *home = 0;
    }

    strcpy(file_name, home);
    strcat(file_name, "rogue.save");

    if((env = getenv("ROGUEOPTS")) != nullptr) {
        parse_opts(env);
    }
    if(env == nullptr || whoami[0] == '\0') {
        strucpy(whoami, md_getusername(), strlen(md_getusername()));
    }
    lowtime = time(nullptr);
    if(getenv("SEED") != nullptr) {
        dnum = (unsigned int)atoi(getenv("SEED"));
        noscore = 1;
    } else {
        dnum = (unsigned int)lowtime;
    }
    seed = dnum;

    open_score();

    /*
     * check for print-score option
     */

    if(argc == 2) {
        if(strcmp(argv[1], "-s") == 0) {
            noscore = TRUE;
            score(0, -1, 0);
            exit(0);
        } else if(strcmp(argv[1], "-d") == 0) {
            dnum = (unsigned int)rnd(100); /* throw away some rnd()s to break patterns */
            while(--dnum) {
                rnd(100);
            }
            purse = rnd(100) + 1;
            level = rnd(100) + 1;
            initscr();
            death(death_monst());
        }
    }

    if(argc == 2) {
        if(!restore(argv[1])) { /* Note: restore will never return */
            my_exit(1);
        }
    }
    if(wizard) {
        printf("Hello %s, welcome to dungeon #%ud", whoami, dnum);
    } else {
        printf("Hello %s, just a moment while I dig the dungeon...", whoami);
    }
    fflush(stdout);

    initscr();        /* Start up cursor package */
    init_probs();     /* Set up prob tables for objects */
    init_player();    /* Set up initial player stats */
    init_names();     /* Set up names of scrolls */
    init_colors();    /* Set up colors of potions */
    init_stones();    /* Set up stone settings of rings */
    init_materials(); /* Set up materials of wands */
    setup();

    /*
     * The screen must be at least NUMLINES x NUMCOLS
     */
    if(LINES < NUMLINES || COLS < NUMCOLS) {
        printf("\nSorry, the screen must be at least %dx%d\n", NUMLINES, NUMCOLS);
        endwin();
        my_exit(1);
    }

    /*
     * Set up windows
     */
    hw = newwin(LINES, COLS, 0, 0);
    idlok(stdscr, TRUE);
    idlok(hw, TRUE);
    noscore = wizard;
    new_level(); /* Draw current level */
    /*
     * Start up daemons and fuses
     */
    start_daemon(d_runners, 0, AFTER);
    start_daemon(d_doctor, 0, AFTER);
    fuse(d_swander, 0, WANDERTIME, AFTER);
    start_daemon(d_stomach, 0, AFTER);
    playit();
}

/*
 * endit:
 *     Exit the program abnormally.
 */

[[noreturn]] void endit(int sig) {
    fatal("Okay, bye bye!\n", sig);
}

/*
 * fatal:
 *     Exit the program, printing a message.
 */

[[noreturn]] void fatal(const char *s, const int sig) {
    mvaddstr(LINES - 2, 0, s);
    refresh();
    endwin();
    my_exit(sig);
}

/*
 * rnd:
 *     Pick a very random number.
 */
int rnd(int range) {
    return range == 0 ? 0 : abs((int)RN) % range;
}

/*
 * roll:
 *     Roll a number of dice
 */
int roll(int number, int sides) {
    int dtotal = 0;

    while(number--) {
        dtotal += rnd(sides) + 1;
    }
    return dtotal;
}

/*
 * tstp:
 *     Handle stop and start signals
 */

void tstp(int /*ignored*/) {
    int y, x;
    int oy, ox;

    /*
     * leave nicely
     */
    getyx(curscr, oy, ox);
    mvcur(0, COLS - 1, LINES - 1, 0);
    endwin();
    fflush(stdout);

    /*
     * start back up again
     */
    raw();
    noecho();
    keypad(stdscr, 1);
    clearok(curscr, TRUE);
    wrefresh(curscr);
    getyx(curscr, y, x);
    mvcur(y, x, oy, ox);
    fflush(stdout);
    curscr->_cury = (short)oy;
    curscr->_curx = (short)ox;
}

/*
 * playit:
 *     The main loop of the program.  Loop until the game is over,
 *     refreshing things and looking at the proper times.
 */

[[noreturn]] void playit() {
    char *opts;

    /*
     * set up defaults for slow terminals
     */

    if(baudrate() <= 1200) {
        terse = TRUE;
        jump = TRUE;
        see_floor = FALSE;
    }

    inv_type = INV_CLEAR;

    /*
     * parse environment declaration of options
     */
    if((opts = getenv("ROGUEOPTS")) != nullptr) {
        parse_opts(opts);
    }

    oldpos = hero;
    oldrp = roomin(&hero);
    while(playing) {
        command(); /* Command execution */
    }
    endit(0);
}

/*
 * quit:
 *     Have player make certain, then exit.
 */

void quit(int sig) {
    int oy, ox;

    /*
     * Reset the signal in case we got here via an interrupt
     */
    if(!q_comm) {
        mpos = 0;
    }
    getyx(curscr, oy, ox);
    msg("really quit?");
    if(readchar() == 'y') {
        signal(SIGINT, leave);
        clear();
        mvprintw(LINES - 2, 0, "You quit with %d gold pieces", purse);
        move(LINES - 1, 0);
        refresh();
        score(purse, 1, 0);
        my_exit(sig);
    } else {
        move(0, 0);
        clrtoeol();
        status();
        move(oy, ox);
        refresh();
        mpos = 0;
        count = 0;
        to_death = FALSE;
    }
}

/*
 * leave:
 *     Leave quickly, but curteously
 */

[[noreturn]] void leave(int sig) {
    static char buf[BUFSIZ];

    setbuf(stdout, buf); /* throw away pending output */

    if(!isendwin()) {
        mvcur(0, COLS - 1, LINES - 1, 0);
        endwin();
    }

    putchar('\n');
    my_exit(sig);
}

/*
 * my_exit:
 *     Leave the process properly
 */

[[noreturn]] void my_exit(int st) {
    exit(st);
}
