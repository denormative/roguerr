/*
 * copies of several routines needed for score
 *
 * @(#)smisc.c     4.7 (Berkeley) 02/05/99
 *
 * Rogue: Exploring the Dungeons of Doom
 * Copyright (C) 1980-1983, 1985, 1999 Michael Toy, Ken Arnold and Glenn Wichman
 * All rights reserved.
 *
 * See the file LICENSE.TXT for full copyright and licensing information.
 */

#include <cstdio>

#include <ctime>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include "rogue.h"

#define TRUE 1
#define FALSE 0

struct MONST {
    const char *m_name;
};

static const char *lockfile = "/tmp/.fredlock";

static MONST monsters_name[] = {
    {"aquator"}, {"bat"},       {"centaur"},     {"dragon"},     {"emu"},    {"venus flytrap"},
    {"griffin"}, {"hobgoblin"}, {"ice monster"}, {"jabberwock"}, {"kobold"}, {"leprechaun"},
    {"medusa"},  {"nymph"},     {"orc"},         {"phantom"},    {"quasit"}, {"rattlesnake"},
    {"snake"},   {"troll"},     {"ur-vile"},     {"vampire"},    {"wraith"}, {"xeroc"},
    {"yeti"},    {"zombie"}};

/*
 * s_lock_sc:
 *     lock the score file.  If it takes too long, ask the user if
 *     they care to wait.  Return TRUE if the lock is successful.
 */
int s_lock_sc() {
    int cnt;
    struct stat sbuf;

over:
    close(8); /* just in case there are no files left */
    if(creat(lockfile, 0000) >= 0) {
        return TRUE;
    }
    for(cnt = 0; cnt < 5; cnt++) {
        sleep(1);
        if(creat(lockfile, 0000) >= 0) {
            return TRUE;
        }
    }
    if(stat(lockfile, &sbuf) < 0) {
        creat(lockfile, 0000);
        return TRUE;
    }
    if(time(nullptr) - sbuf.st_mtime > 10) {
        if(md_unlink(lockfile) < 0) {
            return FALSE;
        }
        goto over;
    } else {
        printf("The score file is very busy.  Do you want to wait longer\n");
        printf("for it to become free so your score can get posted?\n");
        printf("If so, type \"y\"\n");
        (void)fgets(prbuf, MAXSTR, stdin);
        if(prbuf[0] == 'y') {
            for(;;) {
                if(creat(lockfile, 0000) >= 0) {
                    return TRUE;
                }
                if(stat(lockfile, &sbuf) < 0) {
                    creat(lockfile, 0000);
                    return TRUE;
                }
                if(time(nullptr) - sbuf.st_mtime > 10) {
                    if(md_unlink(lockfile) < 0) {
                        return FALSE;
                    }
                }
                sleep(1);
            }
        } else {
            return FALSE;
        }
    }
}

/*
 * s_unlock_sc:
 *     Unlock the score file
 */
void s_unlock_sc() {
    md_unlink(lockfile);
}

/*
 * s_encwrite:
 *     Perform an encrypted write
 */
void s_encwrite(char *start, size_t size, FILE *outf) {
    const char *e1, *e2;
    char fb;
    int temp;

    e1 = encstr;
    e2 = statlist;
    fb = 0;

    while(size--) {
        putc(*start++ ^ *e1 ^ *e2 ^ fb, outf);
        temp = *e1++;
        fb += temp * *e2++;
        if(*e1 == '\0') {
            e1 = encstr;
        }
        if(*e2 == '\0') {
            e2 = statlist;
        }
    }
}

/*
 * s_encread:
 *     Perform an encrypted read
 */

void s_encread(char *start, size_t size, int inf) {
    const char *e1, *e2;
    char fb;
    int temp;
    ssize_t read_size;

    fb = 0;

    if((read_size = read(inf, start, size)) == 0 || read_size == -1) {
        return;
    }

    e1 = encstr;
    e2 = statlist;

    while(size--) {
        *start++ ^= *e1 ^ *e2 ^ fb;
        temp = *e1++;
        fb += temp * *e2++;
        if(*e1 == '\0') {
            e1 = encstr;
        }
        if(*e2 == '\0') {
            e2 = statlist;
        }
    }
}

/*
 * s_killname:
 *     Convert a code to a monster name
 */
char *s_killname(int monst, int doart) {
    const char *sp;
    int article;

    article = TRUE;
    switch(monst) {
        case 'a':
            sp = "arrow";
            break;
        case 'b':
            sp = "bolt";
            break;
        case 'd':
            sp = "dart";
            break;
        case 's':
            sp = "starvation";
            article = FALSE;
            break;
        case 'h':
            sp = "hypothermia";
            article = FALSE;
            break;
        default:
            if(isupper(monst)) {
                sp = monsters_name[monst - 'A'].m_name;
            } else {
                sp = "God";
                article = FALSE;
            }
    }
    if(doart && article) {
        sprintf(prbuf, "a%s ", s_vowelstr(sp));
    } else {
        prbuf[0] = '\0';
    }
    strcat(prbuf, sp);
    return prbuf;
}

/*
 * s_vowelstr:
 *      For printfs: if string starts with a vowel, return "n" for an
 *    "an".
 */
const char *s_vowelstr(const char *str) {
    switch(*str) {
        case 'a':
        case 'A':
        case 'e':
        case 'E':
        case 'i':
        case 'I':
        case 'o':
        case 'O':
        case 'u':
        case 'U':
            return "n";
        default:
            return "";
    }
}
