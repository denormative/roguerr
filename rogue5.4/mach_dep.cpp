/*
 * Various installation dependent routines
 *
 * @(#)mach_dep.c     4.37 (Berkeley) 05/23/83
 *
 * Rogue: Exploring the Dungeons of Doom
 * Copyright (C) 1980-1983, 1985, 1999 Michael Toy, Ken Arnold and Glenn Wichman
 * All rights reserved.
 *
 * See the file LICENSE.TXT for full copyright and licensing information.
 */

/*
 * The various tuneable defines are:
 *
 *     SCOREFILE     Where/if the score file should live.
 *     ALLSCORES     Score file is top ten scores, not top ten
 *             players.  This is only useful when only a few
 *             people will be playing; otherwise the score file
 *             gets hogged by just a few people.
 *     NUMSCORES     Number of scores in the score file (default 10).
 *     NUMNAME          String version of NUMSCORES (first character
 *             should be capitalized) (default "Ten").
 */

#include "rogue.h"
#include <cerrno>
#include <climits>
#include <csignal>

#include <ctime>

#include <sys/stat.h>
#include <sys/types.h>

#ifndef NUMSCORES
#define NUMSCORES 10
#define NUMNAME "Ten"
#endif

/*
 * open_score:
 *     Open up the score file for future use
 */

void open_score() {
#ifdef SCOREFILE
    const char *scorefile = SCOREFILE;

    numscores = NUMSCORES;
    Numname = NUMNAME;

#ifdef ALLSCORES
    allscore = TRUE;
#else  /* ALLSCORES */
    allscore = FALSE;
#endif /* ALLSCORES */

    /*
     * We drop setgid privileges after opening the score file, so subsequent
     * open()'s will fail.  Just reuse the earlier filehandle.
     */

    if(scoreboard != nullptr) {
        rewind(scoreboard);
        return;
    }

    scoreboard = fopen(scorefile, "rb+");

    if((scoreboard == nullptr) && (errno == ENOENT)) {
        scoreboard = fopen(scorefile, "wb+");
    }

    if(scoreboard == nullptr) {
        fprintf(stderr, "Could not open %s for writing: %s\n", scorefile, strerror(errno));
        fflush(stderr);
    }
#else
    scoreboard = nullptr;
#endif
}

/*
 * setup:
 *     Get starting setup for all games
 */

void setup() {
#ifdef DUMP
    md_onsignal_autosave();
#else
    md_onsignal_default();
#endif

    raw();    /* Raw mode */
    noecho(); /* Echo off */
    keypad(stdscr, 1);
}

/*
 * flush_type:
 *     Flush typeahead for traps, etc.
 */

void flush_type() {
    flushinp();
}
