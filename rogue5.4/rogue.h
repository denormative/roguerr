/*
 * Rogue definitions and variable declarations
 *
 * @(#)rogue.h     5.42 (Berkeley) 08/06/83
 *
 * Rogue: Exploring the Dungeons of Doom
 * Copyright (C) 1980-1983, 1985, 1999 Michael Toy, Ken Arnold and Glenn Wichman
 * All rights reserved.
 *
 * See the file LICENSE.TXT for full copyright and licensing information.
 */

#pragma once

#include "metarog.h"

/*
 * Don't change the constants, since they are used for sizes in many
 * places in the program.
 */

#undef SIGTSTP

#define MAXSTR 1024 /* maximum length of strings */
#define MAXLINES 32 /* maximum number of screen lines used */
#define MAXCOLS 80  /* maximum number of screen columns used */

#define RN (((seed = seed * 11109 + 13849) >> 16) & 0xffff)
#ifdef CTRL
#undef CTRL
#endif
#define CTRL(c) (c & 037)

#undef lines

#define CCHAR(x) ((x)&A_CHARTEXT)

#define MAXDAEMONS 20

/*
 * Maximum number of different things
 */
#define MAXROOMS 9
#define MAXTHINGS 9
#define MAXOBJ 9
#define MAXPACK 23
#define MAXTRAPS 10
#define AMULETLEVEL 26
#define NUMTHINGS 7 /* number of types of things */
#define MAXPASS 13  /* upper limit on number of passages */
#define NUMLINES 24
#define NUMCOLS 80
#define STATLINE (NUMLINES - 1)
#define BORE_LEVEL 50

/*
 * return values for get functions
 */
#define NORM 0  /* normal exit */
#define QUIT 1  /* quit option setting */
#define MINUS 2 /* back up one option */

/*
 * inventory types
 */
#define INV_OVER 0
#define INV_SLOW 1
#define INV_CLEAR 2

/*
 * All the fun defines
 */
#define until(expr) while(!(expr))
#define next(ptr) (*ptr).l_next
#define prev(ptr) (*ptr).l_prev
#define winat(y, x) (moat(y, x) != NULL ? moat(y, x)->t_disguise : chat(y, x))
#define ce(a, b) ((a).x == (b).x && (a).y == (b).y)
#define hero player.t_pos
#define pstats player.t_stats
#define pack player.t_pack
#define proom player.t_room
#define max_hp player.t_stats.s_maxhp
#define attach(a, b) _attach(&a, b)
#define detach(a, b) _detach(&a, b)
#define free_list(a) _free_list(&a)
#define on(thing, flag) ((((thing).t_flags & (flag)) != 0))
#define GOLDCALC() (rnd(50 + 10 * level) + 2)
#define ISRING(h, r) (cur_ring[h] != NULL && cur_ring[h]->o_which == r)
#define ISWEARING(r) (ISRING(LEFT, r) || ISRING(RIGHT, r))
#define ISMULT(type) (type == POTION || type == SCROLL || type == FOOD)
#define INDEX(y, x) (&places[((x) << 5) + (y)])
#define chat(y, x) (places[((x) << 5) + (y)].p_ch)
#define flat(y, x) (places[((x) << 5) + (y)].p_flags)
#define moat(y, x) (places[((x) << 5) + (y)].p_monst)
#define unc(cp) (cp).y, (cp).x
#define MASTER 1
#define debug                                                                                      \
    if(wizard)                                                                                     \
    msg

/*
 * things that appear on the screens
 */
#define PASSAGE '#'
#define DOOR '+'
#define FLOOR '.'
#define PLAYER '@'
#define TRAP '^'
#define STAIRS '%'
#define GOLD '*'
#define POTION '!'
#define SCROLL '?'
#define MAGIC '$'
#define FOOD ':'
#define WEAPON ')'
#define ARMOR ']'
#define AMULET ','
#define RING '='
#define STICK '/'
#define CALLABLE -1
#define R_OR_S -2

/*
 * Various constants
 */
#define BEARTIME spread(3)
#define SLEEPTIME spread(5)
#define HOLDTIME spread(2)
#define WANDERTIME spread(70)
#define BEFORE spread(1)
#define AFTER spread(2)
#define HEALTIME 30
#define HUHDURATION 20
#define SEEDURATION 850
#define HUNGERTIME 1300
#define MORETIME 150
#define STOMACHSIZE 2000
#define STARVETIME 850
#define ESCAPE 27
#define LEFT 0
#define RIGHT 1
#define BOLT_LENGTH 6
#define LAMPDIST 3
#ifndef PASSWD
#define PASSWD "mTBellIQOsLNA"
#endif

/*
 * Save against things
 */
#define VS_POISON 00
#define VS_PARALYZATION 00
#define VS_DEATH 00
#define VS_BREATH 02
#define VS_MAGIC 03

/*
 * Various flag bits
 */
/* flags for rooms */
#define ISDARK 0000001 /* room is dark */
#define ISGONE 0000002 /* room is gone (a corridor) */
#define ISMAZE 0000004 /* room is gone (a corridor) */

/* flags for objects */
#define ISCURSED 000001 /* object is cursed */
#define ISKNOW 0000002  /* player knows details about the object */
#define ISMISL 0000004  /* object is a missile type */
#define ISMANY 0000010  /* object comes in groups */
/*     ISFOUND 0000020         ...is used for both objects and creatures */
#define ISPROT 0000040 /* armor is permanently protected */

/* flags for creatures */
#define CANHUH 0000001  /* creature can confuse */
#define CANSEE 0000002  /* creature can see invisible creatures */
#define ISBLIND 0000004 /* creature is blind */
#define ISCANC 0000010  /* creature has special qualities cancelled */
#define ISLEVIT 0000010 /* hero is levitating */
#define ISFOUND 0000020 /* creature has been seen (used for objects) */
#define ISGREED 0000040 /* creature runs to protect gold */
#define ISHASTE 0000100 /* creature has been hastened */
#define ISTARGET 000200 /* creature is the target of an 'f' command */
#define ISHELD 0000400  /* creature has been held */
#define ISHUH 0001000   /* creature is confused */
#define ISINVIS 0002000 /* creature is invisible */
#define ISMEAN 0004000  /* creature can wake when player enters room */
#define ISHALU 0004000  /* hero is on acid trip */
#define ISREGEN 0010000 /* creature can regenerate */
#define ISRUN 0020000   /* creature is running at the player */
#define SEEMONST 040000 /* hero can detect unseen monsters */
#define ISFLY 0040000   /* creature can fly */
#define ISSLOW 0100000  /* creature has been slowed */

/*
 * Flags for level map
 */
#define F_PASS 0x80    /* is a passageway */
#define F_SEEN 0x40    /* have seen this spot before */
#define F_DROPPED 0x20 /* object was dropped here */
#define F_LOCKED 0x20  /* door is locked */
#define F_REAL 0x10    /* what you see is what you get */
#define F_PNUM 0x0f    /* passage number mask */
#define F_TMASK 0x07   /* trap number mask */

/*
 * Trap types
 */
#define T_DOOR 00
#define T_ARROW 01
#define T_SLEEP 02
#define T_BEAR 03
#define T_TELEP 04
#define T_DART 05
#define T_RUST 06
#define T_MYST 07
#define NTRAPS 8

/*
 * Potion types
 */
#define P_CONFUSE 0
#define P_LSD 1
#define P_POISON 2
#define P_STRENGTH 3
#define P_SEEINVIS 4
#define P_HEALING 5
#define P_MFIND 6
#define P_TFIND 7
#define P_RAISE 8
#define P_XHEAL 9
#define P_HASTE 10
#define P_RESTORE 11
#define P_BLIND 12
#define P_LEVIT 13
#define MAXPOTIONS 14

/*
 * Scroll types
 */
#define S_CONFUSE 0
#define S_MAP 1
#define S_HOLD 2
#define S_SLEEP 3
#define S_ARMOR 4
#define S_ID_POTION 5
#define S_ID_SCROLL 6
#define S_ID_WEAPON 7
#define S_ID_ARMOR 8
#define S_ID_R_OR_S 9
#define S_SCARE 10
#define S_FDET 11
#define S_TELEP 12
#define S_ENCH 13
#define S_CREATE 14
#define S_REMOVE 15
#define S_AGGR 16
#define S_PROTECT 17
#define MAXSCROLLS 18

/*
 * Weapon types
 */
#define MACE 0
#define SWORD 1
#define BOW 2
#define ARROW 3
#define DAGGER 4
#define TWOSWORD 5
#define DART 6
#define SHIRAKEN 7
#define SPEAR 8
#define FLAME 9      /* fake entry for dragon breath (ick) */
#define MAXWEAPONS 9 /* this should equal FLAME */

/*
 * Armor types
 */
#define LEATHER 0
#define RING_MAIL 1
#define STUDDED_LEATHER 2
#define SCALE_MAIL 3
#define CHAIN_MAIL 4
#define SPLINT_MAIL 5
#define BANDED_MAIL 6
#define PLATE_MAIL 7
#define MAXARMORS 8

/*
 * Ring types
 */
#define R_PROTECT 0
#define R_ADDSTR 1
#define R_SUSTSTR 2
#define R_SEARCH 3
#define R_SEEINVIS 4
#define R_NOP 5
#define R_AGGR 6
#define R_ADDHIT 7
#define R_ADDDAM 8
#define R_REGEN 9
#define R_DIGEST 10
#define R_TELEPORT 11
#define R_STEALTH 12
#define R_SUSTARM 13
#define MAXRINGS 14

/*
 * Rod/Wand/Staff types
 */
#define WS_LIGHT 0
#define WS_INVIS 1
#define WS_ELECT 2
#define WS_FIRE 3
#define WS_COLD 4
#define WS_POLYMORPH 5
#define WS_MISSILE 6
#define WS_HASTE_M 7
#define WS_SLOW_M 8
#define WS_DRAIN 9
#define WS_NOP 10
#define WS_TELAWAY 11
#define WS_TELTO 12
#define WS_CANCEL 13
#define MAXSTICKS 14

/*
 * Now we define the structures and types
 */

/*
 * Help list
 */
struct h_list {
    int h_ch;
    const char *h_desc;
    int h_print;
};

/*
 * Coordinate data type
 */
struct coord {
    int x;
    int y;
};

/*
 * Stuff about objects
 */
struct obj_info {
    const char *oi_name;
    int oi_prob;
    int oi_worth;
    char *oi_guess;
    int oi_know;
};

/*
 * Room structure
 */
struct room {
    coord r_pos;      /* Upper left corner */
    coord r_max;      /* Size of room */
    coord r_gold;     /* Where the gold is */
    int r_goldval;    /* How much the gold is worth */
    int r_flags;      /* info about the room */
    int r_nexits;     /* Number of exits */
    coord r_exit[12]; /* Where the exits are */
};

/*
 * Structure describing a fighting being
 */
struct stats {
    int s_str;      /* Strength */
    int s_exp;      /* Experience */
    int s_lvl;      /* level of mastery */
    int s_arm;      /* Armor class */
    int s_hpt;      /* Hit points */
    char s_dmg[13]; /* String describing damage done */
    int s_maxhp;    /* Max hit points */
};

/*
 * Structure for monsters and player
 */
union thing {
    struct {
        union thing *_l_next, *_l_prev; /* Next pointer in link */
        coord _t_pos;                   /* Position */
        int _t_turn;                    /* If slowed, is it a turn to move */
        int _t_type;                    /* What it is */
        int _t_disguise;                /* What mimic looks like */
        int _t_oldch;                   /* Character that was where it was */
        const coord *_t_dest;           /* Where it is running to */
        int _t_flags;                   /* State word */
        stats _t_stats;                 /* Physical description */
        room *_t_room;                  /* Current room for thing */
        union thing *_t_pack;           /* What the thing is carrying */
        int _t_reserved;
    } _t;
    struct {
        union thing *_l_next, *_l_prev; /* Next pointer in link */
        int _o_type;                    /* What kind of object it is */
        coord _o_pos;                   /* Where it lives on the screen */
        char *_o_text;                  /* What it says if you read it */
        int _o_launch;                  /* What you need to launch it */
        int _o_packch;                  /* What character it is in the pack */
        char _o_damage[8];              /* Damage if used like sword */
        char _o_hurldmg[8];             /* Damage if thrown */
        int _o_count;                   /* count for plural objects */
        int _o_which;                   /* Which object of a type it is */
        int _o_hplus;                   /* Plusses to hit */
        int _o_dplus;                   /* Plusses to damage */
        int _o_arm;                     /* Armor protection */
        int _o_flags;                   /* information about objects */
        int _o_group;                   /* group number for this object */
        char *_o_label;                 /* Label for object */
    } _o;
};

typedef union thing THING;

#define l_next _t._l_next
#define l_prev _t._l_prev
#define t_pos _t._t_pos
#define t_turn _t._t_turn
#define t_type _t._t_type
#define t_disguise _t._t_disguise
#define t_oldch _t._t_oldch
#define t_dest _t._t_dest
#define t_flags _t._t_flags
#define t_stats _t._t_stats
#define t_pack _t._t_pack
#define t_room _t._t_room
#define t_reserved _t._t_reserved
#define o_type _o._o_type
#define o_pos _o._o_pos
#define o_text _o._o_text
#define o_launch _o._o_launch
#define o_packch _o._o_packch
#define o_damage _o._o_damage
#define o_hurldmg _o._o_hurldmg
#define o_count _o._o_count
#define o_which _o._o_which
#define o_hplus _o._o_hplus
#define o_dplus _o._o_dplus
#define o_arm _o._o_arm
#define o_charges o_arm
#define o_goldval o_arm
#define o_flags _o._o_flags
#define o_group _o._o_group
#define o_label _o._o_label

/*
 * describe a place on the level map
 */
struct PLACE {
    int p_ch;
    int p_flags;
    THING *p_monst;
};

/*
 * Array containing information on all the various types of monsters
 */
struct monster {
    const char *m_name; /* What to call the monster */
    int m_carry;        /* Probability of carrying something */
    int m_flags;        /* things about the monster */
    stats m_stats;      /* Initial stats */
};

enum daemon_id {
    d_none,
    d_rollwand,
    d_doctor,
    d_stomach,
    d_runners,
    d_swander,
    d_nohaste,
    d_unconfuse,
    d_unsee,
    d_sight,

    d_come_down,
    d_visuals,
    d_turn_see,
    d_land,

    d_dont_save,

};

struct delayed_action {
    int d_type;
    enum daemon_id d_id;
    int d_arg;
    int d_time;
};

struct STONE {
    const char *st_name;
    int st_value;
};

/*
 * External variables
 */

extern int after, again, allscore, door_stop, fight_flush, firstmove, has_hit, inv_describe, jump,
    kamikaze, lower_msg, move_on, msg_esc, pack_used[], passgo, playing, q_comm, running, save_msg,
    see_floor, seenstairs, stat_msg, terse, to_death, tombstone, amulet, count, dir_ch, food_left,
    hungry_state, inpack, inv_type, lastscore, level, max_hit, max_level, take, n_objs, no_command,
    no_food, no_move, noscore, ntraps, purse, quiet, vf_hit, runch, last_comm, l_last_comm,
    last_dir, l_last_dir, total, between, group, cNWOOD, cNMETAL, cNSTONES, cNCOLORS;
extern size_t mpos, numscores;

extern char file_name[], home[], huh[], outbuf[], *s_names[];

extern const char *ws_made[], *inv_t_name[], *p_colors[], *r_stones[], *release, *tr_name[],
    *rainbow[], *wood[], *metal[], encstr[], statlist[], version[], *ws_type[];

extern const int a_class[], e_levels[];

extern unsigned int dnum, seed;

extern WINDOW *hw;

extern coord delta, oldpos, stairs;

extern PLACE places[];

extern THING *cur_armor, *cur_ring[], *cur_weapon, *l_last_pick, *last_pick, *lvl_obj, *mlist,
    player;

extern const h_list helpstr[];

extern room *oldrp, passages[], rooms[];

extern stats max_stats;

extern monster monsters[];

extern struct obj_info arm_info[], pot_info[], ring_info[], scr_info[], things[], ws_info[],
    weap_info[];

extern delayed_action d_list[MAXDAEMONS];

extern const STONE stones[];

/*
 * Now all the global variables
 */

extern int wizard;
extern char fruit[], prbuf[], whoami[];
extern FILE *scoreboard;
extern size_t numscores;
extern const char *Numname;
extern int allscore;

// EXTERNS
// armor.h
void wear();
void take_off();
void waste_time();

// chase.h
void runners();
int move_monst(THING *tp);
void relocate(THING *th, const coord *new_loc);
int do_chase(THING *th);
void set_oldch(THING *tp, const coord *cp);
int see_monst(const THING *mp);
void runto(const coord *runner);
int chase(THING *tp, const coord *ee);
room *roomin(const coord *cp);
int diag_ok(const coord *sp, const coord *ep);
int cansee(int y, int x);
const coord *find_dest(const THING *tp);
int dist(int y1, int x1, int y2, int x2);
int dist_cp(const coord *c1, const coord *c2);

// command.h
void command();
void illcom(int ch);
void search();
void help();
void identify();
void d_level();
void u_level();
int levit_check();
void call();
void current(const THING *cur, const char *how, const char *where);

// daemon.h
delayed_action *d_slot();
delayed_action *find_slot(enum daemon_id did);
void start_daemon(enum daemon_id did, int arg, int type);
void kill_daemon(enum daemon_id did);
void do_daemons(int flag);
void fuse(enum daemon_id did, int arg, int time, int type);
void lengthen(enum daemon_id did, int xtime);
void extinguish(enum daemon_id did);
void do_fuses(int flag);

// daemons.h
void doctor();
void swander();
void rollwand();
void unconfuse();
void unsee();
void sight();
void nohaste();
void stomach();
void come_down();
void visuals();
void land();

// extern.h

// fight.h
int fight(const coord *mp, const THING *weap, int thrown);
int attack(THING *mp);
const char *set_mname(const THING *tp);
int swing(int at_lvl, int op_arm, int wplus);
int roll_em(const THING *thatt, THING *thdef, const THING *weap, int hurl);
char *prname(const char *mname, int upper);
void thunk(const THING *weap, const char *mname, int noend);
void hit(const char *er, const char *ee, int noend);
void miss(const char *er, const char *ee, int noend);
void bounce(const THING *weap, const char *mname, int noend);
void remove_mon(const coord *mp, THING *tp, int waskill);
void killed(THING *tp, int pr);

// init.h
void init_player();
void init_colors();
void init_names();
void init_stones();
void init_materials();
void init_probs();
void badcheck(const char *name, const obj_info *info, int bound);
const char *pick_color(const char *col);

// io.h
int msg(const char *fmt, ...);
void addmsg(const char *fmt, ...);
int endmsg();
void doadd(const char *fmt, va_list args);
int step_ok(int ch);
int readchar();
int wreadchar(WINDOW *win);
void status();
void wait_for(WINDOW *win, int ch);
void show_win(const char *message);

// list.h
void _detach(THING **list, THING *item);
void _attach(THING **list, THING *item);
void _free_list(THING **ptr);
void discard(THING *item);
THING *new_item();

// mach_dep.h
void open_score();
void setup();
void flush_type();

// main.h
int main(int argc, char **argv);
[[noreturn]] void endit(int sig);
[[noreturn]] void fatal(const char *s, const int sig);
int rnd(int range);
int roll(int number, int sides);
void tstp(int ignored);
[[noreturn]] void playit();
void quit(int sig);
[[noreturn]] void leave(int sig);
[[noreturn]] void my_exit(int st);

// mdport.h
void md_init();
void md_onsignal_default();
void md_onsignal_exit();
void md_onsignal_autosave();
uid_t md_getuid();
int reread();
void unread(int c);
int md_readchar(WINDOW *win);

// misc.h
void look(int wakeup);
int trip_ch(int y, int x, int ch);
void erase_lamp(const coord *pos, const room *rp);
int show_floor();
THING *find_obj(int y, int x);
void eat();
void check_level();
void chg_str(int amt);
void add_str(int *sp, int amt);
int add_haste(int potion);
void aggravate();
const char *vowelstr(const char *str);
int is_current(const THING *obj);
int get_dir();
int sign(int nm);
int spread(int nm);
void call_it(obj_info *info);
int rnd_thing();
const char *choose_str(const char *ts, const char *ns);

// monsters.h
int randmonster(int wander);
void new_monster(THING *tp, int type, const coord *cp);
int exp_add(const THING *tp);
void wanderer();
const THING *wake_monster(int y, int x);
void give_pack(THING *tp);
int save_throw(int which, const THING *tp);
int save(int which);

// move.h
void do_run(int ch);
void do_move(int dy, int dx);
int turn_ok(int y, int x);
void turnref();
void door_open(const room *rp);
int be_trapped(const coord *tc);
coord rndmove(const THING *who);
void rust_armor(THING *arm);

// new_level.h
void new_level();
int rnd_room();
void put_things();
void treas_room();

// options.h
void option();
void put_bool(void *b);
void put_str(void *str);
void put_inv_t(void *ip);
int get_bool(void *vp, WINDOW *win);
int get_sf(void *vp, WINDOW *win);
int get_str(void *vopt, WINDOW *win);
int get_inv_t(void *vp, WINDOW *win);
int get_num(void *vp, WINDOW *win);
void parse_opts(char *str);
void strucpy(char *s1, const char *s2, size_t len);

// pack.h
void update_mdest(THING *obj);
void add_pack(THING *obj, int silent);
int pack_room(int from_floor, THING *obj);
THING *leave_pack(THING *obj, int newobj, int all);
int pack_char();
int inventory(const THING *list, int type);
void pick_up(int ch);
void move_msg(const THING *obj);
void picky_inven();
THING *get_item(const char *purpose, int type);
void money(int value);
int floor_ch();
int floor_at();
void reset_last();

// passages.h
void do_passages();
void conn(int r1, int r2);
void putpass(const coord *cp);
void door(room *rm, const coord *cp);
void add_pass();
void passnum();
void numpass(int y, int x);

// potions.h
void quaff();
int is_magic(const THING *obj);
void invis_on();
int turn_see(int turn_off);
int seen_stairs();
void raise_level();
void do_pot(int type, int knowit);

// rings.h
void ring_on();
void ring_off();
int gethand();
int ring_eat(int hand);
const char *ring_num(const THING *obj);

// rip.h
void score(int amount, int flags, int monst);
[[noreturn]] void death(int monst);
int center(const char *str);
[[noreturn]] void total_winner();
const char *killname(int monst, int doart);
int death_monst();

// rooms.h
void do_rooms();
void draw_room(const room *rp);
void vert(const room *rp, int startx);
void horiz(const room *rp, int starty);
void do_maze(const room *rp);
void dig(int y, int x);
void accnt_maze(int y, int x, int ny, int nx);
void rnd_pos(const room *rp, coord *cp);
int find_floor(const room *rp, coord *cp, int limit, int monst);
void enter_room(const coord *cp);
void leave_room(const coord *cp);

// save.h
void save_game();
[[noreturn]] void auto_save(int sig);
[[noreturn]] void save_file(FILE *savef);
int restore(const char *file);
int encerror();
void encseterr(int err);
int encclearerr();
size_t encwrite(const char *start, size_t size, FILE *outf);
size_t encread(char *start, size_t size, FILE *inf);

// scmisc.h
int s_lock_sc();
void s_unlock_sc();
void s_encwrite(char *start, size_t size, FILE *outf);
void s_encread(char *start, size_t size, int inf);
char *s_killname(int monst, int doart);
const char *s_vowelstr(const char *str);

// scrolls.h
void read_scroll();
void uncurse(THING *obj);

// state.h
void rs_write(FILE *savef, const void *ptr, size_t size);
void rs_read(FILE *savef, void *ptr, size_t size);
void rs_write_int(FILE *savef, int c);
void rs_read_int(FILE *savef, int *i);
void rs_write_uint(FILE *savef, unsigned int c);
void rs_read_uint(FILE *savef, unsigned int *i);
void rs_write_chars(FILE *savef, const char *c, size_t cnt);
void rs_read_chars(FILE *savef, char *i, int cnt);
void rs_write_ints(FILE *savef, int *c, int cnt);
void rs_read_ints(FILE *savef, int *i, int cnt);
void rs_write_marker(FILE *savef, unsigned int id);
void rs_read_marker(FILE *savef, unsigned int id);
void rs_write_string(FILE *savef, const char *s);
void rs_read_string(FILE *savef, char *s, int max);
void rs_read_new_string(FILE *savef, char **s);
void rs_write_string_index(FILE *savef, const char *master[], int max, const char *str);
void rs_read_string_index(FILE *savef, const char *master[], int maxindex, const char **str);
void rs_write_coord(FILE *savef, coord c);
void rs_read_coord(FILE *savef, coord *c);
void rs_write_window(FILE *savef, WINDOW *win);
void rs_read_window(FILE *savef, WINDOW *win);
void *get_list_item(THING *l, int i);
int find_list_ptr(THING *l, void *ptr);
int list_size(THING *l);
void rs_write_stats(FILE *savef, stats *s);
void rs_read_stats(FILE *savef, stats *s);
void rs_write_stone_index(FILE *savef, const STONE master[], int max, const char *str);
void rs_read_stone_index(FILE *savef, const STONE master[], int maxindex, const char **str);
void rs_write_scrolls(FILE *savef);
void rs_read_scrolls(FILE *savef);
void rs_write_potions(FILE *savef);
void rs_read_potions(FILE *savef);
void rs_write_rings(FILE *savef);
void rs_read_rings(FILE *savef);
void rs_write_sticks(FILE *savef);
void rs_read_sticks(FILE *savef);
void rs_write_daemons(FILE *savef, delayed_action *dlist, int cnt);
void rs_read_daemons(FILE *savef, delayed_action *dlist, int cnt);
void rs_write_obj_info(FILE *savef, obj_info *i, int cnt);
void rs_read_obj_info(FILE *savef, obj_info *mi, int cnt);
void rs_write_room(FILE *savef, room *r);
void rs_read_room(FILE *savef, room *r);
void rs_write_rooms(FILE *savef, room r[], int cnt);
void rs_read_rooms(FILE *savef, room *r, int cnt);
void rs_write_room_reference(FILE *savef, room *rp);
void rs_read_room_reference(FILE *savef, room **rp);
void rs_write_monsters(FILE *savef, monster *m, int cnt);
void rs_read_monsters(FILE *savef, monster *m, int cnt);
void rs_write_object(FILE *savef, THING *o);
void rs_read_object(FILE *savef, THING *o);
void rs_write_object_list(FILE *savef, THING *l);
void rs_read_object_list(FILE *savef, THING **list);
void rs_write_object_reference(FILE *savef, THING *list, THING *item);
void rs_read_object_reference(FILE *savef, THING *list, THING **item);
int find_room_coord(const room *rmlist, const coord *c, int n);
int find_thing_coord(THING *monlist, const coord *c);
int find_object_coord(THING *objlist, const coord *c);
void rs_write_thing(FILE *savef, THING *t);
void rs_read_thing(FILE *savef, THING *t);
void rs_fix_thing(THING *t);
void rs_write_thing_list(FILE *savef, THING *l);
void rs_read_thing_list(FILE *savef, THING **list);
void rs_fix_thing_list(THING *list);
void rs_write_thing_reference(FILE *savef, THING *list, THING *item);
void rs_read_thing_reference(FILE *savef, THING *list, THING **item);
void rs_write_thing_references(FILE *savef, THING *list, THING *items[], int cnt);
void rs_read_thing_references(FILE *savef, THING *list, THING *items[], int cnt);
void rs_write_places(FILE *savef, PLACE *p, int cnt);
void rs_read_places(FILE *savef, PLACE *p, int cnt);
int rs_save_file(FILE *savef);
int rs_restore_file(FILE *savef);

// sticks.h
void fix_stick(THING *cur);
void do_zap();
void drain();
void fire_bolt(const coord *start, coord *dir, const char *name);
const char *charge_str(const THING *obj);

// things.h
char *inv_name(const THING *obj, int drop);
void drop();
int dropcheck(const THING *obj);
THING *new_thing();
int pick_one(const obj_info *info, int nitems);
void discovered();
void print_disc(int type);
void set_order(int *order, int numthings);
int add_line(const char *fmt, const char *arg);
void end_line();
const char *nothing(int type);
void nameit(const THING *obj, const char *type, const char *which, const obj_info *op,
            const char *(*prfunc)(const THING *));
const char *nullstr(const THING *ignored);
void pr_list();
void pr_spec(const obj_info *info, int nitems);

// vers.h

// weapons.h
void missile(int ydelta, int xdelta);
void do_motion(THING *obj, int ydelta, int xdelta);
void fall(THING *obj, int pr);
void init_weapon(THING *weap, int which);
int hit_monster(int y, int x, const THING *obj);
const char *num(int n1, int n2, int type);
void wield();
int fallpos(const coord *pos, coord *newpos);

// wizard.h
void whatis(int insist, int type);
void set_know(THING *obj, obj_info *info);
const char *type_name(int type);
void create_obj();
void teleport();
int passwd();
void show_map();

// /EXTERNS

int md_readchar(WINDOW *win);
void md_onsignal_autosave();
void md_onsignal_exit();
void md_onsignal_default();
int md_issymlink(char *sp);

/* where scorefile should live */
#define SCOREFILE "rogue54.scr"
