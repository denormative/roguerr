/*
    list.c - Functions for dealing with linked lists of goodies

    UltraRogue: The Ultimate Adventure in the Dungeons of Doom
    Copyright (C) 1985, 1986, 1992, 1993, 1995 Herb Chong
    All rights reserved.

    Based on "Advanced Rogue"
    Copyright (C) 1984, 1985 Michael Morgan, Ken Dalka
    All rights reserved.

    Based on "Rogue: Exploring the Dungeons of Doom"
    Copyright (C) 1980, 1981 Michael Toy, Ken Arnold and Glenn Wichman
    All rights reserved.

    See the file LICENSE.TXT for full copyright and licensing information.
*/

#include "rogue.h"
#include <cstdio>

static char errbuf[2 * LINELEN];

/*
    ur_alloc()
    ur_free()

    These are just calls to the system alloc and free, and they also adjust
    the totals. The buffer is cleared out because idents need to be zero
    before going into the pack, or they will be used as indices!
*/

void *ur_alloc(size_t size) {
    char *buf_p;

    total++;

    buf_p = (char *)mem_malloc(size);

    if(buf_p == nullptr) {
        return (nullptr);
    }

    memset(buf_p, 0, size);

    return (buf_p);
}

void ur_free(void *buf_p) {
    mem_free(buf_p);
    total--;
}

/*
    detach()
        Takes an item out of whatever linked list it might be in
        .... function needs to be renamed....
*/

void _detach(linked_list **list, linked_list *item) {
    if(*list == item) {
        *list = next(item);
    }

    if(prev(item) != nullptr) {
        item->l_prev->l_next = next(item);
    }

    if(next(item) != nullptr) {
        item->l_next->l_prev = prev(item);
    }

    item->l_next = nullptr;
    item->l_prev = nullptr;
}

/*
    _attach()
        add an item to the head of a list
        ... this needs to be renamed as well ...
*/

void _attach(linked_list **list, linked_list *item) {
    if(*list != nullptr) {
        item->l_next = *list;
        (*list)->l_prev = item;
        item->l_prev = nullptr;
    } else {
        item->l_next = nullptr;
        item->l_prev = nullptr;
    }

    *list = item;
}

/*
    _attach_after()

    Attaches the given item after the supplied one in the list. If the listed
    item is NULL, the new item is attached at the head of the list.
*/

void _attach_after(linked_list **list_pp, linked_list *list_p, linked_list *new_p) {
    if(list_p == nullptr) {
        _attach(list_pp, new_p); /* stuff it at the beginning */
        return;
    }

    if(next(list_p) != nullptr) { /* something after this one? */
        new_p->l_next = next(list_p);
        list_p->l_next->l_prev = new_p;
    } else {
        new_p->l_next = nullptr;
    }

    list_p->l_next = new_p;
    new_p->l_prev = list_p;
}

/*
    _free_list()
        Throw the whole blamed thing away
*/

void _free_list(linked_list **ptr) {
    linked_list *item;

    while(*ptr != nullptr) {
        item = *ptr;
        *ptr = next(item);
        discard(item);
    }
}

/*
    discard()
        free up an item
*/

void discard(linked_list *item) {
    throw_away(item->data.obj);
    ur_free(item);
}

/*
    throw_away()
        toss out something (like discard, but without the link_list)
*/

void throw_away(object *ptr) {
    free_ident(ptr);
    ur_free(ptr);
}

/*
    new_item()
        get a new item with a specified size
*/

linked_list *new_item(int size) {
    linked_list *item;

    if((item = new_list()) == nullptr) {
        msg("Ran out of memory for header after %d items.", total);
    }

    if((item->data.l_data = new_alloc((size_t)size)) == nullptr) {
        msg("Ran out of memory for data after %d items.", total);
    }

    item->l_next = item->l_prev = nullptr;

    return (item);
}

void *new_alloc(size_t size) {
    void *space = ur_alloc(size);

    if(space == nullptr) {
        sprintf(errbuf, "Rogue ran out of memory.");
        fatal(errbuf);
    }

    return (space);
}

linked_list *new_list() {
    union { /* ugly_lint_hack */
        linked_list *ll;
        void *vptr;
    } newp;

    newp.vptr = mem_malloc(sizeof(linked_list));
    memset(newp.vptr, 0, sizeof(linked_list));
    return (newp.ll);
}
