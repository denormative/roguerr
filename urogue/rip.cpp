/*
    rip.c - File for the fun ends Death or a total win

    UltraRogue: The Ultimate Adventure in the Dungeons of Doom
    Copyright (C) 1985, 1986, 1992, 1993, 1995 Herb Chong
    All rights reserved.

    Based on "Advanced Rogue"
    Copyright (C) 1984, 1985 Michael Morgan, Ken Dalka
    All rights reserved.

    Based on "Rogue: Exploring the Dungeons of Doom"
    Copyright (C) 1980, 1981 Michael Toy, Ken Arnold and Glenn Wichman
    All rights reserved.

    See the file LICENSE.TXT for full copyright and licensing information.
*/

#include "Scores.h"
#include <fmt/format.h>
#include "rogue.h"
#include <ctime>

static const char *rip[] = {"                       __________",
                            "                      /          \\",
                            "                     /    REST    \\",
                            "                    /      IN      \\",
                            "                   /     PEACE      \\",
                            "                  /                  \\",
                            "                  |                  |",
                            "                  |                  |",
                            "                  |    killed by     |",
                            "                  |                  |",
                            "                  |       1993       |",
                            "                 *|     *  *  *      | *",
                            R"(       ________)/\\_//(\/(/\)/\//\/|_)_______)",
                            nullptr};

/*
    death()
        Do something really fun when he dies
*/

void death(int monst) {
    const char **dp = rip, *killer;
    struct tm *lt;
    time_t date;
    char buf[80];
    int c;

    if(is_wearing(R_RESURRECT) || rnd(wizard ? 3 : 67) == 0) {
        int die = TRUE;

        if(resurrect-- == 0) {
            msg("You've run out of lives.");
        } else if(!save_resurrect(ring_value(R_RESURRECT))) {
            msg("Your attempt to return from the grave fails.");
        } else {
            linked_list *item;
            linked_list *next_item;
            object *obj;
            int rm;
            unsigned long flags;
            coord pos;

            die = FALSE;
            msg("You feel a sudden warmth and then nothingness.");
            teleport();

            if(ring_value(R_RESURRECT) > 1 && rnd(10)) {
                pstats.s_hpt = 2 * pstats.s_const;
                pstats.s_const = max(pstats.s_const - 1, 3);
            } else {
                for(item = pack; item != nullptr; item = next_item) {
                    obj = OBJPTR(item);

                    if(obj->o_flags & ISOWNED || obj->o_flags & ISPROT) {
                        next_item = next(item);
                        continue;
                    }

                    flags = obj->o_flags;
                    obj->o_flags &= ~ISCURSED;
                    dropcheck(obj);
                    obj->o_flags = flags;
                    next_item = next(item);
                    rem_pack(obj);

                    if(obj->o_type == ARTIFACT) {
                        has_artifact &= ~(1 << obj->o_which);
                    }

                    do {
                        rm = rnd_room();
                        rnd_pos(&rooms[rm], &pos);
                    } while(winat(pos.y, pos.x) != FLOOR);

                    obj->o_pos = pos;
                    add_obj(item, obj->o_pos.y, obj->o_pos.x);
                }

                pstats.s_hpt = pstats.s_const;
                pstats.s_const = max(pstats.s_const - roll(2, 2), 3);
            }

            chg_str(roll(1, 4), TRUE, FALSE);
            pstats.s_lvl = max(pstats.s_lvl, 1);
            no_command += 2 + rnd(4);

            if(on(player, ISHUH)) {
                lengthen_fuse(FUSE_UNCONFUSE, rnd(8) + HUHDURATION);
            } else {
                light_fuse(FUSE_UNCONFUSE, nullptr, rnd(8) + HUHDURATION, AFTER);
            }

            turn_on(player, ISHUH);
            light(&hero);
        }

        if(die) {
            wmove(cw, mpos, 0);
            waddstr(cw, morestr);
            wrefresh(cw);
            wait_for(' ');
        } else {
            return;
        }
    }

    time(&date);
    lt = localtime(&date);
    clear();
    wclear(cw);
    move(8, 0);

    while(*dp) {
        printw("%s\n", *dp++);
    }

    mvaddstr(14, 28 - ((int)(strlen(whoami) + 1) / 2), whoami);
    sprintf(buf, "%d+%ld Points", pstats.s_lvl, pstats.s_exp);
    mvaddstr(15, 28 - ((int)(strlen(buf) + 1) / 2), buf);
    killer = killname(monst, buf);
    mvaddstr(17, 28 - ((int)(strlen(killer) + 1) / 2), killer);
    sprintf(prbuf, "%2d", lt->tm_year);
    mvaddstr(18, 28, prbuf);
    move(LINES - 1, 0);

    mvaddstr(LINES - 1, 0, retstr);

    while((c = readcharw(stdscr)) != '\n' && c != '\r') {
    }
    idenpack();
    wrefresh(cw);
    refresh();

    score(pstats.s_exp, pstats.s_lvl, KILLED, monst);
    byebye();
}

static const char *reason[] = {"killed", "quit", "a winner", "a total winner"};

ScoreEntry recordScore(const long amount, const int lvl, const int flags, const int monst) {
    ScoreEntry entry{};

    entry.sc_explvl = lvl;
    entry.sc_gold = purse;
    entry.sc_score = amount;
    entry.sc_name = fmt::format("{}, {}", md_getusername(), which_class(player.t_ctype));
    if(flags == WINNER) {
        entry.sc_level = max_level;
    } else {
        entry.sc_level = level;
    }
    entry.sc_monster = monst;
    entry.sc_flags = flags;
    entry.sc_reason = reason[flags];
    entry.sc_reason += fmt::format(" on level {}", entry.sc_level);
    entry.was_wizard = waswizard;
    entry.sc_artifacts = has_artifact;
    // TODO: probably should note user was wizard on scoreboard

    return entry;
}

void printScores(const Scores &scores) {
    printf("\nTop Ten Adventurers:\n%4s %15s %10s %s\n", "Rank", "Score", "Gold", "Name");

    int rank = 1;
    for(auto &e : scores.scores) {

        printf("%4d %15s %10ld %s:", rank, fmt::format("{}+{}", e.sc_explvl, e.sc_score).c_str(),
               e.sc_gold, e.sc_name.c_str());

        if(e.sc_artifacts) {
            string thangs;
            int first = TRUE;

            for(int n = 0; n <= maxartifact; n++) {
                if(e.sc_artifacts & (1 << n)) {
                    if(!thangs.empty()) {
                        thangs += ", ";
                    }

                    if(first) {
                        thangs += "retrieved ";
                        first = FALSE;
                    }
                    thangs += arts[n].ar_name;
                }
            }

            if(!thangs.empty()) {
                printf("%s,", thangs.c_str());
            }

            printf("\n%32s", " ");
        }

        printf("%s on level %ld", reason[e.sc_flags], e.sc_level);

        if(e.sc_flags == 0) {
            printf(" by \n%32s", " ");
            char buf[1024]; // FIXME: remove this
            const char *killer = killname(e.sc_monster, buf);
            printf(" %s", killer);
        }

        putchar('\n');

        rank++;
    }
}

/*
    score()
        figure score and post it.
*/

void score(long amount, int lvl, int flags, int monst) {
    if(flags != WINNER && flags != TOTAL) {
        const char *packend;
        if(flags == CHICKEN) {
            packend = "when you quit";
        } else {
            packend = "at your untimely demise";
        }

        noecho();
        nl();
        refresh();
        showpack(packend);
    }

    mvaddstr(LINES - 1, 0, retstr);
    refresh();
    fflush(stdout);
    wait_for('\n');

    clear();
    refresh();
    endwin();

    Scores scores("urogue.scores");
    scores.load();

    ScoreEntry entry = recordScore(amount, lvl, flags, monst);

    scores.addScore(entry);
    scores.save();

    printScores(scores);
}

[[noreturn]] void total_winner() {
    linked_list *item;
    object *obj;
    int worth;
    long oldpurse;
    char c;
    linked_list *bag = nullptr;

    clear();
    standout();
    addstr("                                                               \n");
    addstr("  @   @               @   @           @          @@@  @     @  \n");
    addstr("  @   @               @@ @@           @           @   @     @  \n");
    addstr("  @   @  @@@  @   @   @ @ @  @@@   @@@@  @@@      @  @@@    @  \n");
    addstr("   @@@@ @   @ @   @   @   @     @ @   @ @   @     @   @     @  \n");
    addstr("      @ @   @ @   @   @   @  @@@@ @   @ @@@@@     @   @     @  \n");
    addstr("  @   @ @   @ @  @@   @   @ @   @ @   @ @         @   @  @     \n");
    addstr("   @@@   @@@   @@ @   @   @  @@@@  @@@@  @@@     @@@   @@   @  \n");
    addstr("                                                               \n");
    addstr("     Congratulations, you have made it to the light of day!    \n");
    standend();
    addstr("\nYou have joined the elite ranks of those who have \n");
    addstr("escaped the Dungeons of Doom alive.  You journey home \n");
    addstr("and sell all your loot at a great profit.\n");
    addstr("The White Council approves the recommendation of\n");

    if(player.t_ctype == C_FIGHTER) {
        addstr("the fighters guild and appoints you Lord Protector\n");
    } else if(player.t_ctype == C_ASSASIN) {
        addstr("the assassins guild and appoints you Master Murderer\n");
    } else if(player.t_ctype == C_NINJA) {
        addstr("the ninja guild and appoints you Master of the Wind\n");
    } else if(player.t_ctype == C_ILLUSION) {
        addstr("the illusionists guild and appoints you Master Wizard\n");
    } else if(player.t_ctype == C_MAGICIAN) {
        addstr("the magicians guild and appoints you Master Wizard\n");
    } else if(player.t_ctype == C_CLERIC) {
        addstr("the temple priests and appoints you Master of the Flowers\n");
    } else if(player.t_ctype == C_DRUID) {
        addstr("the temple priests and appoints you Master of the Flowers\n");
    } else if(player.t_ctype == C_RANGER) {
        addstr("the rangers guild and appoints you Master Ranger\n");
    } else if(player.t_ctype == C_PALADIN) {
        addstr("the paladins guild and appoints you Master Paladin\n");
    } else if(player.t_ctype == C_THIEF) {
        addstr("the thieves guild under protest and appoints you\n");
        addstr("Master of the Highways\n");
    }

    addstr("of the Land Between the Mountains.\n");
    mvaddstr(LINES - 1, 0, spacemsg);
    refresh();
    wait_for(' ');
    clear();
    idenpack();
    oldpurse = purse;
    mvaddstr(0, 0, "   Worth  Item");

    for(c = 'a', item = pack; item != nullptr; c++, item = next(item)) {
        obj = OBJPTR(item);
        worth = get_worth(obj);
        purse += worth;

        if(obj->o_type == ARTIFACT && obj->o_which == TR_PURSE) {
            bag = obj->o_bag;
        }

        mvprintw(c - 'a' + 1, 0, "%c) %8d  %s", c, worth, inv_name(obj, UPPERCASE));
    }

    if(bag != nullptr) {
        mvaddstr(LINES - 1, 0, morestr);
        refresh();
        wait_for(' ');
        clear();
        mvprintw(0, 0, "Contents of the Magic Purse of Yendor:\n");

        for(c = 'a', item = bag; item != nullptr; c++, item = next(item)) {
            obj = OBJPTR(item);
            worth = get_worth(obj);
            whatis(item);
            purse += worth;
            mvprintw(c - 'a' + 1, 0, "%c) %8d %s\n", c, worth, inv_name(obj, UPPERCASE));
        }
    }

    mvprintw(c - 'a' + 1, 0, "   %6d  Gold Pieces          ", oldpurse);
    refresh();

    if(has_artifact == 255) {
        score(pstats.s_exp, pstats.s_lvl, TOTAL, 0);
    } else {
        score(pstats.s_exp, pstats.s_lvl, WINNER, 0);
    }

    byebye();
}

const char *killname(long monst, char *buf) {
    if(buf == nullptr) {
        return ("A bug in UltraRogue #102");
    }

    if(monst >= 0) {
        switch(monsters[monst].m_name[0]) {
            case 'a':
            case 'e':
            case 'i':
            case 'o':
            case 'u':
                sprintf(buf, "an %s", monsters[monst].m_name);
                break;
            default:
                sprintf(buf, "a %s", monsters[monst].m_name);
        }

        return (buf);
    }
    switch(monst) {
        case D_ARROW:
            strcpy(buf, "an arrow");
            break;
        case D_DART:
            strcpy(buf, "a dart");
            break;
        case D_BOLT:
            strcpy(buf, "a bolt");
            break;
        case D_POISON:
            strcpy(buf, "poison");
            break;
        case D_POTION:
            strcpy(buf, "a cursed potion");
            break;
        case D_PETRIFY:
            strcpy(buf, "petrification");
            break;
        case D_SUFFOCATION:
            strcpy(buf, "suffocation");
            break;
        case D_INFESTATION:
            strcpy(buf, "a parasite");
            break;
        case D_DROWN:
            strcpy(buf, "drowning");
            break;
        case D_FALL:
            strcpy(buf, "falling");
            break;
        case D_FIRE:
            strcpy(buf, "slow boiling in oil");
            break;
        case D_SPELLFUMBLE:
            strcpy(buf, "a botched spell");
            break;
        case D_DRAINLIFE:
            strcpy(buf, "a drain life spell");
            break;
        case D_ARTIFACT:
            strcpy(buf, "an artifact of the gods");
            break;
        case D_GODWRATH:
            strcpy(buf, "divine retribution");
            break;
        case D_CLUMSY:
            strcpy(buf, "excessive clumsyness");
            break;
        default:
            strcpy(buf, "stupidity");
            break;
    }

    return (buf);
}

/*
    showpack()
        Display the contents of the hero's pack
*/

void showpack(const char *howso) {
    char *iname;
    int worth;
    int cnt, ch;
    long oldpurse;
    linked_list *item;
    object *obj;
    linked_list *bag = nullptr;

    cnt = 1;
    clear();
    mvprintw(0, 0, "Contents of your pack %s:\n", howso);
    ch = 0;
    oldpurse = purse;
    purse = 0;

    for(item = pack; item != nullptr; item = next(item)) {
        obj = OBJPTR(item);
        worth = get_worth(obj);
        whatis(item);
        purse += worth;

        if(obj->o_type == ARTIFACT && obj->o_which == TR_PURSE) {
            bag = obj->o_bag;
        }

        iname = inv_name(obj, UPPERCASE);
        mvprintw(cnt, 0, "%d) %s\n", ch, iname);
        ch += 1;

        if(++cnt > LINES - 5 && next(item) != nullptr) {
            cnt = 1;
            mvaddstr(LINES - 1, 0, morestr);
            refresh();
            wait_for(' ');
            clear();
        }
    }

    if(bag != nullptr) {
        mvaddstr(LINES - 1, 0, morestr);
        refresh();
        wait_for(' ');
        clear();
        cnt = 1;
        ch = 0;

        mvprintw(0, 0, "Contents of the Magic Purse of Yendor %s:\n", howso);

        for(item = bag; item != nullptr; item = next(item)) {
            obj = OBJPTR(item);
            worth = get_worth(obj);
            whatis(item);
            purse += worth;
            mvprintw(cnt, 0, "%d) %s\n", ch, inv_name(obj, UPPERCASE));
            ch += 1;

            if(++cnt > LINES - 5 && next(item) != nullptr) {
                cnt = 1;
                mvaddstr(LINES - 1, 0, morestr);
                refresh();
                wait_for(' ');
                clear();
            }
        }
    }

    mvprintw(cnt + 1, 0, "Carrying %d gold pieces", oldpurse);
    mvprintw(cnt + 2, 0, "Carrying objects worth %d gold pieces", purse);
    purse += oldpurse;
    refresh();
}

[[noreturn]] void byebye() {
    endwin();
    printf("\n");
    exit(0);
}

/*
    save_resurrect()
        chance of resurrection according to modifed D&D probabilities
*/

int save_resurrect(int bonus) {
    int need, adjust;

    adjust = pstats.s_const + bonus - luck;

    if(adjust > 17) {
        return (TRUE);
    }
    if(adjust < 14) {
        need = 5 * (adjust + 5);
    } else {
        need = 90 + 2 * (adjust - 13);
    }

    return (roll(1, 100) < need);
}
