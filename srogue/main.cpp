/*
 * Rogue
 * Exploring the dungeons of doom
 *
 * @(#)main.c     9.0     (rdk)      7/17/84
 *
 * Super-Rogue
 * Copyright (C) 1984 Robert D. Kindelberger
 * All rights reserved.
 *
 * Based on "Rogue: Exploring the Dungeons of Doom"
 * Copyright (C) 1980, 1981 Michael Toy, Ken Arnold and Glenn Wichman
 * All rights reserved.
 *
 * See the file LICENSE.TXT for full copyright and licensing information.
 */

#include "rogue.h"
#include <climits>
#include <csignal>
#include <cstdio>
#include <ctime>

#include <pwd.h>
#include <sys/stat.h>
#include <termios.h>
#include <unistd.h>

struct termios terminal;

int main(int argc, char **argv) {
    char *env;
    linked_list *item;
    object *obj;
    struct passwd *pw;
    char alldone, wpt;
    int lowtime;
    time_t now;
    const char *homedir = md_getroguedir();

    playuid = (int)getuid();

    if(setuid((uid_t)playuid) < 0) {
        printf("Cannot change to effective uid: %d\n", playuid);
        exit(1);
    }
    playgid = (int)getgid();

    /* check for print-score option */

    strcpy(scorefile, homedir);

    if(*scorefile) {
        strcat(scorefile, "/");
    }
    strcat(scorefile, "srogue.scr");

    if(argc >= 2 && strcmp(argv[1], "-a") == 0) {
        wizard = TRUE;
        argv++;
        argc--;
    }

    /* Check to see if he is a wizard */

    if(argc >= 2 && strcmp(argv[1], "-w") == 0) {
        if(strcmp(PASSWD, md_crypt(getpass(wizstr))) == 0) {
            wizard = TRUE;
            argv++;
            argc--;
        }
    }
    time(&now);
    lowtime = (int)now;

    /* get home and options from environment */

    if((env = getenv("HOME")) != nullptr) {
        strcpy(home, env);
    } else if((pw = getpwuid((uid_t)playuid)) != nullptr) {
        strcpy(home, pw->pw_dir);
    } else {
        home[0] = '\0';
    }

    if(strcmp(home, "/") == 0) {
        home[0] = '\0';
    }

    if((strlen(home) > 0) && (home[strlen(home) - 1] != '/')) {
        strcat(home, "/");
    }

    strcpy(file_name, home);
    strcat(file_name, "srogue.sav");

    if((env = getenv("ROGUEOPTS")) != nullptr) {
        parse_opts(env);
    }

    if(env == nullptr || whoami[0] == '\0') {
        if((pw = getpwuid((uid_t)playuid)) == nullptr) {
            printf("Say, who are you?\n");
            exit(1);
        } else {
            strucpy(whoami, pw->pw_name, strlen(pw->pw_name));
        }
    }

    if(env == nullptr || fruit[0] == '\0') {
        strcpy(fruit, "juicy-fruit");
    }

    if(argc == 2) {
        if(!restore(argv[1])) { /* NOTE: NEVER RETURNS */
            exit(1);
        }
    }

    dnum = (wizard && getenv("SEED") != nullptr ? atoi(getenv("SEED")) : lowtime + getpid());

    if(wizard) {
        printf("Hello %s, welcome to dungeon #%d\n", whoami, dnum);
    } else {
        printf("Hello %s, One moment while I open the door to the dungeon...\n", whoami);
    }

    fflush(stdout);
    seed = dnum;
    srand48(seed); /* init rnd number gen */

    signal(SIGINT, byebye); /* just in case */
    signal(SIGQUIT, byebye);

    init_everything();

    initscr(); /* Start up cursor package */

    if(strcmp(termname(), "dumb") == 0) {
        endwin();
        printf("ERROR in terminal parameters.\n");
        printf("Check TERM in environment.\n");
        byebye(1);
    }

    if(LINES < 24 || COLS < 80) {
        endwin();
        printf("ERROR: screen size too small\n");
        byebye(1);
    }

    if((*whoami == '\0') || (strcmp(whoami, "dosuser") == 0)) {
        echo();
        mvaddstr(23, 2, "Rogue's Name? ");
        wgetnstr(stdscr, whoami, MAXSTR);
        noecho();
    }

    if(*whoami == '\0') {
        strcpy(whoami, "Rodney");
    }

    setup();

    /* Set up windows */

    cw = newwin(0, 0, 0, 0);
    mw = newwin(0, 0, 0, 0);
    hw = newwin(0, 0, 0, 0);
    waswizard = wizard;

    /* Draw current level */

    new_level(NORMLEV);

    /* Start up daemons and fuses */

    start_daemon(d_status, TRUE, BEFORE);
    start_daemon(d_doctor, TRUE, BEFORE);
    start_daemon(d_stomach, TRUE, BEFORE);
    start_daemon(d_runners, TRUE, AFTER);
    fuse(d_swander, TRUE, WANDERTIME);

    /* Give the rogue his weaponry */

    do {
        wpt = (char)pick_one(w_magic);
        switch(wpt) {
            case MACE:
            case SWORD:
            case TWOSWORD:
            case SPEAR:
            case TRIDENT:
            case SPETUM:
            case BARDICHE:
            case PIKE:
            case BASWORD:
            case HALBERD:
                alldone = TRUE;
                break;
            default:
                alldone = FALSE;
        }
    } while(!alldone);

    item = new_thing(FALSE, WEAPON, wpt);
    obj = OBJPTR(item);
    obj->o_hplus = rnd(3);
    obj->o_dplus = rnd(3);
    obj->o_flags = ISKNOW;
    add_pack(item, TRUE);
    cur_weapon = obj;

    /* Now a bow */

    item = new_thing(FALSE, WEAPON, BOW);
    obj = OBJPTR(item);
    obj->o_hplus = rnd(3);
    obj->o_dplus = rnd(3);
    obj->o_flags = ISKNOW;
    add_pack(item, TRUE);

    /* Now some arrows */

    item = new_thing(FALSE, WEAPON, ARROW);
    obj = OBJPTR(item);
    obj->o_count = 25 + rnd(15);
    obj->o_hplus = rnd(2);
    obj->o_dplus = rnd(2);
    obj->o_flags = ISKNOW;
    add_pack(item, TRUE);

    /* And his suit of armor */

    wpt = (char)pick_one(a_magic);
    item = new_thing(FALSE, ARMOR, wpt);
    obj = OBJPTR(item);
    obj->o_flags = ISKNOW;
    obj->o_ac = armors[(int)wpt].a_class - rnd(4);
    cur_armor = obj;
    add_pack(item, TRUE);

    /* Give him some food */

    item = new_thing(FALSE, FOOD, 0);
    add_pack(item, TRUE);

    playit();
}

/*
 * endit:
 *     Exit the program abnormally.
 */
[[noreturn]] void endit(int a) {
    a = 0; // silence warning
    fatal("Ok, if you want to exit that badly, I'll have to allow it");
}

/*
 * fatal:
 *     Exit the program, printing a message.
 */

[[noreturn]] void fatal(const char *s) {
    clear();
    refresh();
    endwin();
    fprintf(stderr, "%s\n\r", s);
    fflush(stderr);
    byebye(2);
}

/*
 * byebye:
 *     Exit here and reset the users terminal parameters
 *     to the way they were when he started
 */

[[noreturn]] void byebye(int how) {
    if(!isendwin()) {
        endwin();
    }

    exit(how); /* exit like flag says */
}

/*
 * rnd:
 *     Pick a very random number.
 */
int rnd(int range) {
    int wh;

    if(range == 0) {
        wh = 0;
    } else {
        wh = lrand48() % range;
        wh &= 0x7FFFFFFF;
    }
    return wh;
}

/*
 * roll:
 *     roll a number of dice
 */
int roll(int number, int sides) {
    int dtotal = 0;

    while(number-- > 0) {
        dtotal += rnd(sides) + 1;
    }
    return dtotal;
}

/*
** setup:      Setup signal catching functions
*/
void setup() {
    signal(SIGHUP, auto_save);
    signal(SIGINT, auto_save);
    signal(SIGQUIT, byebye);
    signal(SIGILL, game_err);
    signal(SIGTRAP, game_err);
#ifdef SIGIOT
    signal(SIGIOT, game_err);
#endif
#ifdef SIGEMT
    signal(SIGEMT, game_err);
#endif
    signal(SIGFPE, game_err);
#ifdef SIGBUS
    signal(SIGBUS, game_err);
#endif
    signal(SIGSEGV, game_err);
#ifdef SIGSYS
    signal(SIGSYS, game_err);
#endif
    signal(SIGPIPE, game_err);
    signal(SIGTERM, game_err);

    cbreak();
    noecho();
}

/*
** playit:     The main loop of the program.  Loop until the game is over,
**          refreshing things and looking at the proper times.
*/

[[noreturn]] void playit() {
    char *opts;

    tcgetattr(0, &terminal);

    /* parse environment declaration of options */

    if((opts = getenv("ROGUEOPTS")) != nullptr) {
        parse_opts(opts);
    }

    player.t_oldpos = hero;
    oldrp = roomin(&hero);
    nochange = FALSE;
    while(playing) {
        command(); /* Command execution */
    }
    endit(0);
}
