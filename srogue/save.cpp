/*
 * save and restore routines
 *
 * @(#)save.c     9.0     (rdk)      7/17/84
 *
 * Super-Rogue
 * Copyright (C) 1984 Robert D. Kindelberger
 * All rights reserved.
 *
 * Based on "Rogue: Exploring the Dungeons of Doom"
 * Copyright (C) 1980, 1981 Michael Toy, Ken Arnold and Glenn Wichman
 * All rights reserved.
 *
 * See the file LICENSE.TXT for full copyright and licensing information.
 */

#include "rogue.h"

#include <cerrno>
#include <csignal>
#include <ctime>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

using STAT = struct stat;
static STAT sbuf;

/*
 * ignore:
 *     Ignore ALL signals possible
 */
void ignore() {
    int i;

    for(i = 0; i < NSIG; i++) {
        signal(i, SIG_IGN);
    }
}

/*
 * save_game:
 *     Save the current game
 */
int save_game() {
    int c;
    char buf[LINLEN];

    mpos = 0;
    if(file_name[0] != '\0') {
        msg("Save file (%s)? ", file_name);
        do {
            c = wgetch(cw);
            if(c == ESCAPE) {
                msg("");
                return FALSE;
            }
        } while(c != 'n' && c != 'y');
        mpos = 0;
        if(c == 'y') {
            goto gotfile;
        }
    }
    msg("File name: ");
    mpos = 0;
    buf[0] = '\0';
    if(get_str(buf, cw) == QUIT) {
        msg("");
        return FALSE;
    }
    msg("");
    strcpy(file_name, buf);
gotfile:
    c = dosave(); /* try to save this game */
    if(c == FALSE) {
        msg("Could not save game to file %s", file_name);
    }
    return c;
}

/*
 * auto_save:
 *     Automatically save a game
 */
[[noreturn]] void auto_save(int a) {
    a = 0;     // silence warning
    dosave();  /* save this game */
    byebye(1); /* so long for now */
}

/*
 * game_err:
 *     When an error occurs. Set error flag and save game.
 */
[[noreturn]] void game_err(int a) {
    a = 0; // silence warning
    int ok;

    ok = dosave(); /* try to save this game */
    clear();
    refresh();
    endwin();

    printf("\nInternal error !!!\n\nYour game was ");
    if(ok) {
        printf("saved.");
    } else {
        printf("NOT saveable.");
    }

    fflush(stdout);

#ifdef SIGIOT
    signal(SIGIOT, SIG_DFL); /* allow core dump signal */
#endif

    // abort(); /* cause core dump */
    byebye(3);
}

/*
 * dosave:
 *     Set UID back to user and save the game
 */
int dosave() {
    FILE *savef;

    ignore();
    setuid((uid_t)playuid);
    setgid((uid_t)playgid);
    umask(022);

    if(file_name[0] != '\0') {
        if((savef = fopen(file_name, "wb")) != nullptr) {
            save_file(savef);
            return TRUE;
        }
    }
    return FALSE;
}

/*
 * save_file:
 *     Do the actual save of this game to a file
 */
void save_file(FILE *savef) {
    int fnum;
    int slines = LINES;
    int scols = COLS;

    /*
     * force allocation of the buffer now so that inodes, etc
     * can be checked when restoring saved games.
     */
    fnum = fileno(savef);
    fstat(fnum, &sbuf);
    write(fnum, "RDK", 4);
    lseek(fnum, 0L, 0);
    encwrite(const_cast<char *>(version), strlen(version) + 1, savef);
    encwrite(&sbuf.st_ino, sizeof(sbuf.st_ino), savef);
    encwrite(&sbuf.st_dev, sizeof(sbuf.st_dev), savef);
    encwrite(&sbuf.st_ctime, sizeof(sbuf.st_ctime), savef);
    encwrite(&sbuf.st_mtime, sizeof(sbuf.st_mtime), savef);
    encwrite(&slines, sizeof(slines), savef);
    encwrite(&scols, sizeof(scols), savef);
    msg("");
    rs_save_file(savef);
    close(fnum);
    signal(SIGINT, byebye);
    signal(SIGQUIT, byebye);
    wclear(cw);
    draw(cw);
}

/*
 * restore:
 *     Restore a saved game from a file
 */
int restore(const char *file) {
    int inf, pid;
    int ret_status;
    char buf[LINLEN];
    STAT sbuf2;
    int slines, scols;

    if((inf = open(file, O_RDONLY)) < 0) {
        printf("Cannot read save game %s\n", file);
        return FALSE;
    }

    encread(buf, strlen(version) + 1, inf);

    if(strcmp(buf, version) != 0) {
        printf("Sorry, saved game version is out of date.\n");
        return FALSE;
    }

    fstat(inf, &sbuf2);

    encread(&sbuf.st_ino, sizeof(sbuf.st_ino), inf);
    encread(&sbuf.st_dev, sizeof(sbuf.st_dev), inf);
    encread(&sbuf.st_ctime, sizeof(sbuf.st_ctime), inf);
    encread(&sbuf.st_mtime, sizeof(sbuf.st_mtime), inf);
    encread(&slines, sizeof(slines), inf);
    encread(&scols, sizeof(scols), inf);

    /*
     * we do not close the file so that we will have a hold of the
     * inode for as long as possible
     */

    if(!wizard) {
        if(sbuf2.st_ino != sbuf.st_ino || sbuf2.st_dev != sbuf.st_dev) {
            printf("Sorry, saved game is not in the same file.\n");
            return FALSE;
        }
    }

    initscr();

    if(slines > LINES) {
        endwin();
        printf("Sorry, original game was played on a screen with %d lines.\n", slines);
        printf("Current screen only has %d lines. Unable to restore game\n", LINES);
        return (FALSE);
    }

    if(scols > COLS) {
        endwin();
        printf("Sorry, original game was played on a screen with %d columns.\n", scols);
        printf("Current screen only has %d columns. Unable to restore game\n", COLS);
        return (FALSE);
    }

    cw = newwin(LINES, COLS, 0, 0);
    mw = newwin(LINES, COLS, 0, 0);
    hw = newwin(LINES, COLS, 0, 0);

    mpos = 0;
    mvwprintw(cw, 0, 0, "%s: %s", file, ctime(&sbuf2.st_mtime));

    /* defeat multiple restarting from the same place */

    if(!wizard) {
        if(sbuf2.st_nlink != 1) {
            endwin();
            printf("Cannot restore from a linked file\n");
            return FALSE;
        }
    }

    if(rs_restore_file(inf) == FALSE) {
        endwin();
        printf("Cannot restore file\n");
        return (FALSE);
    }

    if(!wizard) {
        endwin();
        while((pid = fork()) < 0) {
            sleep(1);
        }

        /* set id to unlink file */
        if(pid == 0) {
            setuid((uid_t)playuid);
            setgid((gid_t)playgid);
            unlink(file);
            exit(0);
        }
        /* wait for unlink to finish */
        else {
            while(wait(&ret_status) != pid) {
            }
            if(ret_status < 0) {
                printf("Cannot unlink file\n");
                return FALSE;
            }
        }
    }

    strcpy(file_name, file);
    setup();
    restscr(cw);
    srand48(getpid());
    playit();
    return TRUE;
}
