/*
 * Functions for dealing with linked lists of goodies
 *
 * @(#)list.c     9.0     (rdk)      7/17/84
 *
 * Super-Rogue
 * Copyright (C) 1984 Robert D. Kindelberger
 * All rights reserved.
 *
 * Based on "Rogue: Exploring the Dungeons of Doom"
 * Copyright (C) 1980, 1981 Michael Toy, Ken Arnold and Glenn Wichman
 * All rights reserved.
 *
 * See the file LICENSE.TXT for full copyright and licensing information.
 */

#include "rogue.h"

/*
 * detach:
 *     Takes an item out of whatever linked list it might be in
 */

void _detach(linked_list **list, linked_list *item) {
    if(*list == item) {
        *list = next(item);
    }
    if(prev(item) != nullptr) {
        item->l_prev->l_next = next(item);
    }
    if(next(item) != nullptr) {
        item->l_next->l_prev = prev(item);
    }
    item->l_next = nullptr;
    item->l_prev = nullptr;
}

/*
 * _attach:     add an item to the head of a list
 */
void _attach(linked_list **list, linked_list *item) {
    if(*list != nullptr) {
        item->l_next = *list;
        (*list)->l_prev = item;
        item->l_prev = nullptr;
    } else {
        item->l_next = nullptr;
        item->l_prev = nullptr;
    }
    *list = item;
}

/*
 * _free_list:     Throw the whole blamed thing away
 */
void _free_list(linked_list **ptr) {
    linked_list *item;

    while(*ptr != nullptr) {
        item = *ptr;
        *ptr = next(item);
        discard(item);
    }
}

/*
 * discard:  free up an item
 */
void discard(linked_list *item) {
    total -= 2;
    FREE(item->l_data);
    FREE(item);
}

/*
 * new_item:     get a new item with a specified size
 */
linked_list *new_item(int size) {
    linked_list *item;

    item = (linked_list *)newalloc(sizeof *item);
    item->l_data = newalloc((size_t)size);
    item->l_next = item->l_prev = nullptr;
    return item;
}

char *newalloc(size_t size) {
    char *space = (char *)ALLOC(size);

    if(space == nullptr) {
        sprintf(prbuf, "Rogue ran out of memory.");
        fatal(prbuf);
    }
    total++;
    return space;
}
