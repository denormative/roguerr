/*
 * Read and execute the user commands
 *
 * @(#)command.c     9.0     (rdk)      7/17/84
 *
 * Super-Rogue
 * Copyright (C) 1984 Robert D. Kindelberger
 * All rights reserved.
 *
 * Based on "Rogue: Exploring the Dungeons of Doom"
 * Copyright (C) 1980, 1981 Michael Toy, Ken Arnold and Glenn Wichman
 * All rights reserved.
 *
 * See the file LICENSE.TXT for full copyright and licensing information.
 */

#include "rogue.h"

#include <climits>
#include <csignal>
#include <unistd.h>

/*
 * command:
 *     Process the user commands
 */
void command() {
    char ch;
    int ntimes = 1; /* Number of player moves */
    static char countch, direction, newcount = FALSE;

    if(pl_on(ISHASTE)) {
        ntimes++;
    }
    /*
     * Let the daemons start up
     */
    while(ntimes-- > 0) {
        do_daemons(BEFORE);
        look(TRUE);
        if(!running) {
            door_stop = FALSE;
        }
        lastscore = purse;
        wmove(cw, hero.y, hero.x);
        if(!(running || repeat_count)) {
            draw(cw); /* Draw screen */
        }
        take = 0;
        after = TRUE;
        /*
         * Read command or continue run
         */
        if(wizard) {
            waswizard = TRUE;
        }
        if(player.t_nocmd <= 0) {
            player.t_nocmd = 0;
            if(running) {
                ch = runch;
            } else if(repeat_count) {
                ch = countch;
            } else {
                ch = (char)readchar();
                if(mpos != 0 && !running) { /* Erase message if its there */
                    msg("");
                }
            }
        } else {
            ch = '.';
        }
        if(player.t_nocmd > 0) {
            if(--player.t_nocmd <= 0) {
                msg("You can move again.");
            }
        } else {
            /*
             * check for prefixes
             */
            if(isdigit(ch)) {
                repeat_count = 0;
                newcount = TRUE;
                while(isdigit(ch)) {
                    repeat_count = repeat_count * 10 + (ch - '0');
                    ch = (char)readchar();
                }
                countch = ch;
                /*
                 * turn off count for commands which don't make sense
                 * to repeat
                 */
                switch(ch) {
                    case 'h':
                    case 'j':
                    case 'k':
                    case 'l':
                    case 'y':
                    case 'u':
                    case 'b':
                    case 'n':
                    case 'H':
                    case 'J':
                    case 'K':
                    case 'L':
                    case 'Y':
                    case 'U':
                    case 'B':
                    case 'N':
                    case 'q':
                    case 'r':
                    case 's':
                    case 'f':
                    case 't':
                    case 'C':
                    case 'I':
                    case '.':
                    case 'z':
                    case 'p':
                        break;
                    default:
                        repeat_count = 0;
                }
            }
            switch(ch) {
                case 'f':
                case 'g':
                    if(pl_off(ISBLIND)) {
                        door_stop = TRUE;
                        firstmove = TRUE;
                    }
                    if(repeat_count && !newcount) {
                        ch = direction;
                    } else {
                        ch = (char)readchar();
                    }
                    switch(ch) {
                        case 'h':
                        case 'j':
                        case 'k':
                        case 'l':
                        case 'y':
                        case 'u':
                        case 'b':
                        case 'n':
                            ch = (char)toupper(ch);
                    }
                    direction = ch;
            }
            newcount = FALSE;
            /*
             * execute a command
             */
            if(repeat_count && !running) {
                repeat_count--;
            }
            switch(ch) {
                case 'h':
                    do_move(0, -1);
                    break;
                case 'j':
                    do_move(1, 0);
                    break;
                case 'k':
                    do_move(-1, 0);
                    break;
                case 'l':
                    do_move(0, 1);
                    break;
                case 'y':
                    do_move(-1, -1);
                    break;
                case 'u':
                    do_move(-1, 1);
                    break;
                case 'b':
                    do_move(1, -1);
                    break;
                case 'n':
                    do_move(1, 1);
                    break;
                case 'H':
                    do_run('h');
                    break;
                case 'J':
                    do_run('j');
                    break;
                case 'K':
                    do_run('k');
                    break;
                case 'L':
                    do_run('l');
                    break;
                case 'Y':
                    do_run('y');
                    break;
                case 'U':
                    do_run('u');
                    break;
                case 'B':
                    do_run('b');
                    break;
                case 'N':
                    do_run('n');
                    break;
                case 't':
                    if(!get_dir()) {
                        after = FALSE;
                    } else {
                        missile(delta.y, delta.x);
                    }
                    break;
                case 'Q':
                    after = FALSE;
                    quit(-1);
                    break;
                case 'i':
                    after = FALSE;
                    inventory(pack, 0);
                    break;
                case 'I':
                    after = FALSE;
                    picky_inven();
                    break;
                case 'd':
                    drop(nullptr);
                    break;
                case 'q':
                    quaff();
                    break;
                case 'r':
                    read_scroll();
                    break;
                case 'e':
                    eat();
                    break;
                case 'w':
                    wield();
                    break;
                case 'W':
                    wear();
                    break;
                case 'T':
                    take_off();
                    break;
                case 'P':
                    ring_on();
                    break;
                case 'R':
                    ring_off();
                    break;
                case 'O':
                    option();
                    break;
                case 'c':
                    call();
                    break;
                case '>':
                    after = FALSE;
                    d_level();
                    break;
                case '<':
                    after = FALSE;
                    u_level();
                    break;
                case '?':
                    after = FALSE;
                    help();
                    break;
                case '/':
                    after = FALSE;
                    identify(0);
                    break;
                case 's':
                    search();
                    break;
                case 'z':
                    do_zap(FALSE);
                    break;
                case 'p':
                    if(get_dir()) {
                        do_zap(TRUE);
                    } else {
                        after = FALSE;
                    }
                    break;
                case 'v':
                    msg("Super Rogue version %s.", release);
                    break;
                case 'D':
                    dip_it();
                    break;
                case CTRL('L'):
                    after = FALSE;
                    restscr(cw);
                    break;
                case CTRL('R'):
                    after = FALSE;
                    msg(huh);
                    break;
                case 'a':
                    after = FALSE;
                    dispmax();
                    break;
                case '@':
                    if(wizard) {
                        msg("Hero @ %d,%d : Stairs @ %d,%d", hero.y, hero.x, stairs.y, stairs.x);
                    }
                    break;
                case 'S':
                    after = FALSE;
                    if(save_game()) {
                        wclear(cw);
                        draw(cw);
                        endwin();
                        byebye(0);
                    }
                    break;
                case '.':; /* Rest command */
                    break;
                case ' ':
                    after = FALSE; /* do nothing */
                    break;
                case '=':
                    if(wizard) {
                        activity();
                        after = FALSE;
                    }
                    break;
                case CTRL('P'):
                    after = FALSE;
                    if(wizard) {
                        wizard = FALSE;
                        msg("Not wizard any more");
                    } else {
                        wizard = passwd();
                        if(wizard) {
                            msg("Welcome back, Bob!!!!!");
                            waswizard = TRUE;
                        } else {
                            msg("Sorry");
                        }
                    }
                    break;
                case ESCAPE: /* Escape */
                    door_stop = FALSE;
                    repeat_count = 0;
                    after = FALSE;
                    break;
                case '#':
                    if(levtype == POSTLEV) { /* buy something */
                        buy_it();
                    }
                    after = FALSE;
                    break;
                case '$':
                    if(levtype == POSTLEV) { /* price something */
                        price_it();
                    }
                    after = FALSE;
                    break;
                case '%':
                    if(levtype == POSTLEV) { /* sell something */
                        sell_it();
                    }
                    after = FALSE;
                    break;
                default:
                    after = FALSE;
                    if(wizard) {
                        switch(ch) {
                            case CTRL('A'):;
                                break;
                            case 'C':
                                create_obj(FALSE);
                                break;
                            case CTRL('I'):
                                inventory(lvl_obj, 1);
                                break;
                            case CTRL('W'):
                                whatis(nullptr);
                                break;
                            case CTRL('D'):
                                level++;
                                new_level(NORMLEV);
                                break;
                            case CTRL('U'):
                                if(level > 1) {
                                    level--;
                                }
                                new_level(NORMLEV);
                                break;
                            case CTRL('F'):
                                displevl();
                                break;
                            case CTRL('X'):
                                dispmons();
                                break;
                            case CTRL('T'):
                                teleport(rndspot, &player);
                                break;
                            case CTRL('E'):
                                msg("food left: %d", food_left);
                                break;
                            case CTRL('O'):
                                add_pass();
                                break;
                            case 'M': {
                                int tlev, whichlev;
                                prbuf[0] = '\0';
                                msg("Which level? ");
                                if(get_str(prbuf, cw) == NORM) {
                                    whichlev = NORMLEV;
                                    tlev = atoi(prbuf);
                                    if(tlev < 1) {
                                        level = 1;
                                    }
                                    if(tlev >= 200) {
                                        tlev -= 199;
                                        whichlev = MAZELEV;
                                    } else if(tlev >= 100) {
                                        tlev -= 99;
                                        whichlev = POSTLEV;
                                    }
                                    level = tlev;
                                    new_level(whichlev);
                                }
                            } break;
                            case CTRL('N'): {
                                linked_list *item;

                                item = get_item("charge", STICK);
                                if(item != nullptr) {
                                    (OBJPTR(item))->o_charges = 10000;
                                    msg("");
                                }
                            } break;
                            case CTRL('H'): {
                                int i;
                                linked_list *item;
                                object *obj;

                                him->s_exp = (int)e_levels[him->s_lvl + 7] + 1;
                                check_level();
                                /*
                                 * Give the rogue a very good sword
                                 */
                                item = new_thing(FALSE, WEAPON, TWOSWORD);
                                obj = OBJPTR(item);
                                obj->o_hplus = 3;
                                obj->o_dplus = 3;
                                obj->o_flags = ISKNOW;
                                i = add_pack(item, TRUE);
                                if(i) {
                                    cur_weapon = obj;
                                } else {
                                    discard(item);
                                }
                                /*
                                 * And his suit of armor
                                 */
                                item = new_thing(FALSE, ARMOR, PLATEARMOR);
                                obj = OBJPTR(item);
                                obj->o_ac = -8;
                                obj->o_flags = ISKNOW;
                                i = add_pack(item, TRUE);
                                if(i) {
                                    cur_armor = obj;
                                } else {
                                    discard(item);
                                }
                                nochange = FALSE;
                            } break;
                            default:
                                msg(illegal, unctrl_int(ch));
                                repeat_count = 0;
                        }
                    } else {
                        msg(illegal, unctrl_int(ch));
                        repeat_count = 0;
                    }
            }
            /*
             * turn off flags if no longer needed
             */
            if(!running) {
                door_stop = FALSE;
            }
        }
        /*
         * If he ran into something to take, let the
         * hero pick it up if not in a trading post.
         */
        if(take != 0 && levtype != POSTLEV) {
            pick_up(take);
        }
        if(!running) {
            door_stop = FALSE;
        }
    }
    /*
     * Kick off the rest if the daemons and fuses
     */
    if(after) {
        int j;

        look(FALSE);
        do_daemons(AFTER);
        do_fuses();
        if(pl_on(ISSLOW)) {
            waste_time();
        }
        for(j = LEFT; j <= RIGHT; j++) {
            if(cur_ring[j] != nullptr) {
                if(cur_ring[j]->o_which == R_SEARCH) {
                    search();
                } else if(cur_ring[j]->o_which == R_TELEPORT) {
                    if(rnd(100) < 5) {
                        teleport(rndspot, &player);
                    }
                }
            }
        }
    }
}

/*
 * quit:
 *     Have player make certain, then exit.
 */
void quit(int a) {
    a = 0; // silence warning
    char ch, good;
    /*
     * Reset the signal in case we got here via an interrupt
     */
    if(signal(SIGINT, quit) != quit) {
        mpos = 0;
    }
    msg("Really quit? [y/n/s]");
    /*     ch = tolower(readchar());*/
    ch = (char)readchar();
    if(ch == 'y') {
        clear();
        move(LINES - 1, 0);
        refresh();
        score(purse, CHICKEN, 0);
        byebye(0);
    } else if(ch == 's') {
        good = (char)save_game();
        if(good) {
            wclear(cw);
            draw(cw);
            endwin();
            byebye(0);
        }
    } else {
        signal(SIGINT, quit);
        wmove(cw, 0, 0);
        wclrtoeol(cw);
        draw(cw);
        mpos = 0;
        repeat_count = 0;
        nochange = FALSE;
    }
}

/*
 * search:
 *     Player gropes about him to find hidden things.
 */

void search() {
    int x, y;
    char ch;

    /*
     * Look all around the hero, if there is something hidden there,
     * give him a chance to find it.  If its found, display it.
     */
    if(pl_on(ISBLIND)) {
        return;
    }
    for(x = hero.x - 1; x <= hero.x + 1; x++) {
        for(y = hero.y - 1; y <= hero.y + 1; y++) {
            ch = (char)winat(y, x);
            if(isatrap(ch)) { /* see if its a trap */
                trap *tp;

                if((tp = trap_at(y, x)) == nullptr) {
                    break;
                }
                if(tp->tr_flags & ISFOUND) {
                    break; /* no message if its seen */
                }
                if((char)mvwinch(cw, y, x) == ch) {
                    break;
                }
                if(rnd(100) > (him->s_lvl * 9 + herowis() * 5)) {
                    break;
                }
                tp->tr_flags |= ISFOUND;
                mvwaddch_int(cw, y, x, tp->tr_type);
                repeat_count = 0;
                running = FALSE;
                msg(tr_name(tp->tr_type));
            } else if(ch == SECRETDOOR) {
                if(rnd(100) < (him->s_lvl * 4 + herowis() * 5)) {
                    mvaddch(y, x, DOOR);
                    repeat_count = 0;
                }
            }
        }
    }
}

/*
 * help:
 *     Give single character help, or the whole mess if he wants it
 */
void help() {
    h_list *strp;
    char helpch;
    int cnt;

    strp = &helpstr[0];
    msg("Character you want help for (* for all): ");
    helpch = (char)readchar();
    mpos = 0;
    /*
     * If its not a *, print the right help string
     * or an error if he typed a funny character.
     */
    if(helpch != '*') {
        wmove(cw, 0, 0);
        while(strp->h_ch) {
            if(strp->h_ch == helpch) {
                msg("%s%s", unctrl_int(strp->h_ch), strp->h_desc);
                break;
            }
            strp++;
        }
        if(strp->h_ch != helpch) {
            msg("Unknown character '%s'", unctrl_int(helpch));
        }
        return;
    }
    /*
     * Here we print help for everything.
     * Then wait before we return to command mode
     */
    wclear(hw);
    cnt = 0;
    while(strp->h_ch) {
        mvwaddstr(hw, cnt % 23, cnt > 22 ? 40 : 0, unctrl_int(strp->h_ch));
        waddstr(hw, strp->h_desc);
        cnt++;
        strp++;
    }
    wmove(hw, LINES - 1, 0);
    wprintw(hw, spacemsg);
    draw(hw);
    wait_for(hw, ' ');
    wclear(hw);
    draw(hw);
    wmove(cw, 0, 0);
    wclrtoeol(cw);
    touchwin(cw);
    nochange = FALSE;
}

/*
 * identify:
 *     Tell the player what a certain thing is.
 */
const char *identify(int what) {
    char ch;
    const char *str;

    if(what == 0) {
        msg("What do you want identified? ");
        ch = (char)readchar();
        mpos = 0;
        if(ch == ESCAPE) {
            msg("");
            return nullptr;
        }
    } else {
        ch = (char)what;
    }
    if(isalpha(ch)) {
        str = monsters[midx(ch)].m_name;
    } else {
        switch(ch) {
            case '|':
            case '-':
                str = "the wall of a room";
                break;
            case GOLD:
                str = "gold";
                break;
            case STAIRS:
                str = "passage leading up/down";
                break;
            case DOOR:
                str = "door";
                break;
            case FLOOR:
                str = "room floor";
                break;
            case PLAYER:
                str = "you";
                break;
            case PASSAGE:
                str = "passage";
                break;
            case POST:
                str = "trading post";
                break;
            case MAZETRAP:
                str = "maze trap";
                break;
            case TRAPDOOR:
                str = "trapdoor";
                break;
            case ARROWTRAP:
                str = "arrow trap";
                break;
            case SLEEPTRAP:
                str = "sleeping gas trap";
                break;
            case BEARTRAP:
                str = "bear trap";
                break;
            case TELTRAP:
                str = "teleport trap";
                break;
            case DARTTRAP:
                str = "dart trap";
                break;
            case POOL:
                str = "magic pool";
                break;
            case POTION:
                str = "potion";
                break;
            case SCROLL:
                str = "scroll";
                break;
            case FOOD:
                str = "food";
                break;
            case WEAPON:
                str = "weapon";
                break;
            case ' ':
                str = "solid rock";
                break;
            case ARMOR:
                str = "armor";
                break;
            case AMULET:
                str = "The Amulet of Yendor";
                break;
            case RING:
                str = "ring";
                break;
            case STICK:
                str = "wand or staff";
                break;
            default:
                if(what == 0) {
                    str = "unknown character";
                } else {
                    str = "a magical ghost";
                }
        }
    }
    if(what == 0) {
        msg("'%s' : %s", unctrl_int(ch), str);
    }
    return str;
}

/*
 * d_level:
 *     He wants to go down a level
 */
void d_level() {
    if(winat(hero.y, hero.x) != STAIRS) {
        msg("I see no way down.");
    } else {
        if(pl_on(ISHELD)) {
            msg("You are being held.");
            return;
        }
        level++;
        new_level(NORMLEV);
    }
}

/*
 * u_level:
 *     He wants to go up a level
 */
void u_level() {
    if(winat(hero.y, hero.x) == STAIRS) {
        if(pl_on(ISHELD)) {
            msg("You are being held.");
            return;
        } /* player not held here */
        if(amulet) {
            level--;
            if(level == 0) {
                total_winner();
            }
            new_level(NORMLEV);
            msg("You feel a wrenching sensation in your gut.");
            return;
        }
    }
    msg("I see no way up.");
}

/*
 * call:
 *     Allow a user to call a potion, scroll, or ring something
 */
void call() {
    object *obj;
    linked_list *item;
    char **guess;
    const char *elsewise;
    int wh;

    if((item = get_item("call", 0)) == nullptr) {
        return;
    }
    obj = OBJPTR(item);
    wh = obj->o_which;
    switch(obj->o_type) {
        case RING:
            guess = r_guess;
            elsewise = (r_guess[wh] != nullptr ? r_guess[wh] : r_stones[wh]);
            break;
        case POTION:
            guess = p_guess;
            elsewise = (p_guess[wh] != nullptr ? p_guess[wh] : p_colors[wh]);
            break;
        case SCROLL:
            guess = s_guess;
            elsewise = (s_guess[wh] != nullptr ? s_guess[wh] : s_names[wh]);
            break;
        case STICK:
            guess = ws_guess;
            elsewise = (ws_guess[wh] != nullptr ? ws_guess[wh] : ws_stuff[wh].ws_made);
            break;
        default:
            msg("You can't call %ss anything", obj->o_typname);
            return;
    }
    msg("Was called \"%s\"", elsewise);
    msg(callit);
    if(guess[wh] != nullptr) {
        free(guess[wh]);
    }
    strcpy(prbuf, elsewise);
    if(get_str(prbuf, cw) == NORM) {
        guess[wh] = newalloc(strlen(prbuf) + 1);
        strcpy(guess[wh], prbuf);
    }
}
