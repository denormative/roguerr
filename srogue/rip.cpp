/*
 * File for the fun, ends in death or a total win
 *
 * @(#)rip.c     9.0     (rdk)      7/17/84
 *
 * Super-Rogue
 * Copyright (C) 1984 Robert D. Kindelberger
 * All rights reserved.
 *
 * Based on "Rogue: Exploring the Dungeons of Doom"
 * Copyright (C) 1980, 1981 Michael Toy, Ken Arnold and Glenn Wichman
 * All rights reserved.
 *
 * See the file LICENSE.TXT for full copyright and licensing information.
 */

#include "Scores.h"
#include <fmt/format.h>
#include "rogue.h"
#include <csignal>
#include <ctime>
#include <fcntl.h>
#include <pwd.h>
#include <sys/types.h>
#include <unistd.h>

static const char *rip[] = {
    "                          ____________________",
    "                         /                    \\",
    "                        /  Bob Kindelberger's  \\",
    "                       /       Graveyard        \\",
    "                      /                          \\",
    "                     /       REST IN PEACE        \\",
    "                    /                              \\",
    "                    |                              |",
    "                    |                              |",
    "                    |        Destroyed by a        |",
    "                    |                              |",
    "                    |                              |",
    "                    |                              |",
    "                    |                              |",
    "                    |                              |",
    "                    |                              |",
    "                    |                              |",
    "                   *|     *     *     *     *      |*",
    R"(          ________)\\//\//\)/\//\)/\//\)/\//\)/\//\//(________)",
};

#define RIP_LINES (sizeof rip / (sizeof(char *)))

/*
 * death:
 *     Do something really fun when he dies
 */

[[noreturn]] void death(int monst) {
    const char *killer;
    struct tm *lt;
    time_t date;
    char buf[LINLEN];

    time(&date);
    lt = localtime(&date);
    clear();
    move(3, 0);
    for(auto &dp : rip) {
        printw("%s\n", dp);
    }
    mvaddstr_int(10, 36 - ((strlen(whoami) + 1) / 2), whoami);
    killer = killname(monst);
    mvaddstr(12, 43, vowelstr(killer));
    mvaddstr_int(14, 36 - ((strlen(killer) + 1) / 2), killer);
    purse -= purse / 10;
    sprintf(buf, "%d Gold Pieces", purse);
    mvaddstr_int(16, 36 - ((strlen(buf) + 1) / 2), buf);
    sprintf(prbuf, "%d/%d/%d", lt->tm_mon + 1, lt->tm_mday, 1900 + lt->tm_year);
    mvaddstr(18, 32, prbuf);
    move(LINES - 1, 0);
    refresh();
    score(purse, KILLED, monst);
    byebye(0);
}

static const char *reason[] = {"Killed", "Chickened out", "A Total Winner"};

ScoreEntry recordScore(const int amount, const int flags, const int monst) {
    ScoreEntry entry{};

    entry.sc_score = amount;
    entry.sc_name = md_getusername();
    if(flags == WINNER) {
        entry.sc_level = max_level;
    } else {
        entry.sc_level = level;
    }
    entry.sc_monster = monst;
    entry.sc_flags = flags;
    entry.sc_reason = reason[flags];
    entry.sc_reason += fmt::format(" on level {}", entry.sc_level);
    entry.was_wizard = waswizard;
    entry.sc_explvl = him->s_lvl;
    entry.sc_exppts = him->s_exp;
    entry.sc_date = time(nullptr);
    // TODO: probably should note user was wizard on scoreboard

    return entry;
}

void printScores(const Scores &scores) {
    printf("\n\nTop Ten Adventurers:\nRank\tScore\tName\n");
    int rank = 1;
    for(auto &e : scores.scores) {
        printf("%d\t%ld\t%s: %s\t\t--> %s on level %ld", rank, e.sc_score, e.sc_name.c_str(),
               ctime(&e.sc_date), reason[e.sc_flags], e.sc_level);
        if(e.sc_flags == KILLED) {
            const char *killer = killname((int)e.sc_monster);
            printf(" by a%s %s", vowelstr(killer), killer);
        }
        printf(" [Exp: %ld/%ld]", e.sc_explvl, e.sc_exppts);
        printf("\n");
        rank++;
    }
}

static int oldpurse;

/*
 * score:
 *     Figure score and post it.
 */
void score(int amount, int aflag, int monst) {

    signal(SIGINT, byebye);
    signal(SIGQUIT, byebye);
    if(aflag != WINNER) {
        const char *packend;
        if(aflag == CHICKEN) {
            packend = "when you chickened out";
        } else {
            packend = "at your untimely demise";
        }
        mvaddstr(LINES - 1, 0, retstr);
        refresh();
        wgetnstr(stdscr, prbuf, 80);
        oldpurse = purse;
        showpack(FALSE, packend);
    }
    mvaddstr(LINES - 1, 0, retstr);
    refresh();
    wgetnstr(stdscr, prbuf, 80);

    ignore();
    signal(SIGINT, byebye);
    signal(SIGQUIT, byebye);
    clear();
    refresh();
    endwin();

    Scores scores("srogue.scores");
    scores.load();

    ScoreEntry entry = recordScore(amount, aflag, monst);

    scores.addScore(entry);
    scores.save();

    printScores(scores);
}

/*
 * total_winner:
 *     The hero made it back out alive
 */
[[noreturn]] void total_winner() {
    clear();
    addstr("                                                               \n");
    addstr("  @   @               @   @           @          @@@  @     @  \n");
    addstr("  @   @               @@ @@           @           @   @     @  \n");
    addstr("  @   @  @@@  @   @   @ @ @  @@@   @@@@  @@@      @  @@@    @  \n");
    addstr("   @@@@ @   @ @   @   @   @     @ @   @ @   @     @   @     @  \n");
    addstr("      @ @   @ @   @   @   @  @@@@ @   @ @@@@@     @   @     @  \n");
    addstr("  @   @ @   @ @  @@   @   @ @   @ @   @ @         @   @  @     \n");
    addstr("   @@@   @@@   @@ @   @   @  @@@@  @@@@  @@@     @@@   @@   @  \n");
    addstr("                                                               \n");
    addstr("     Congratulations, you have made it to the light of day!    \n");
    addstr("\nYou have joined the elite ranks of those who have escaped the\n");
    addstr("Dungeons of Doom alive.  You journey home and sell all your loot at\n");
    addstr("a great profit and are admitted to the fighters guild.\n");

    mvaddstr(LINES - 1, 0, spacemsg);
    refresh();
    wait_for(stdscr, ' ');
    clear();
    oldpurse = purse;
    showpack(TRUE, nullptr);
    score(purse, WINNER, 0);
    byebye(0);
}

/*
 * showpack:
 *     Display the contents of the hero's pack
 */
void showpack(bool winner, const char *howso) {
    char *iname;
    int cnt, worth, ch;
    linked_list *item;
    object *obj;

    idenpack();
    cnt = 1;
    clear();
    if(winner) {
        mvaddstr(0, 0, "   Worth  Item");
    } else {
        mvprintw(0, 0, "Contents of your pack %s:\n", howso);
    }
    ch = 'a';
    for(item = pack; item != nullptr; item = next(item)) {
        obj = OBJPTR(item);
        iname = inv_name(obj, FALSE);
        if(winner) {
            worth = get_worth(obj);
            worth *= obj->o_count;
            mvprintw(cnt, 0, "  %6d  %s", worth, iname);
            purse += worth;
        } else {
            mvprintw(cnt, 0, "%c) %s\n", ch, iname);
            ch = npch((char)ch);
        }
        if(++cnt >= LINES - 2 && next(item) != nullptr) {
            cnt = 1;
            mvaddstr(LINES - 1, 0, morestr);
            refresh();
            wait_for(stdscr, ' ');
            clear();
        }
    }
    mvprintw(cnt + 1, 0, "--- %d  Gold Pieces ---", oldpurse);
    refresh();
}

/*
 * killname:
 *     Returns what the hero was killed by.
 */
const char *killname(int monst) {
    if(monst < MAXMONS + 1) {
        return monsters[monst].m_name;
    } /* things other than monsters */
    switch(monst) {
        case K_ARROW:
            return "crooked arrow";
        case K_DART:
            return "sharp dart";
        case K_BOLT:
            return "jagged bolt";
        case K_POOL:
            return "magic pool";
        case K_ROD:
            return "exploding rod";
        case K_SCROLL:
            return "burning scroll";
        case K_STONE:
            return "transmogrification to stone";
        case K_STARVE:
            return "starvation";
    }

    return "Bob Kindelberger";
}
