/*
 * This file has all the code for the option command.
 *
 * @(#)options.c     9.0     (rdk)      7/17/84
 *
 * Super-Rogue
 * Copyright (C) 1984 Robert D. Kindelberger
 * All rights reserved.
 *
 * Based on "Rogue: Exploring the Dungeons of Doom"
 * Copyright (C) 1980, 1981 Michael Toy, Ken Arnold and Glenn Wichman
 * All rights reserved.
 *
 * See the file LICENSE.TXT for full copyright and licensing information.
 */

#include "rogue.h"

#include <termios.h>

/*
 * description of an option and what to do with it
 */
struct optstruct {
    const char *o_name;   /* option name */
    const char *o_prompt; /* prompt for interactive entry */
    char *o_opt;          /* pointer to thing to set */
};

using OPTION = optstruct;

static OPTION optlist[] = {
    {"name", "Name: ", whoami}, {"fruit", "Fruit: ", fruit}, {"file", "Save file: ", file_name}};
#define NUM_OPTS (sizeof optlist / sizeof(OPTION))

/*
 * print and then set options from the terminal
 */
void option() {
    OPTION *op;
    wclear(hw);
    touchwin(hw);
    /*
     * Display current values of options
     */
    for(op = optlist; op < &optlist[NUM_OPTS]; op++) {
        int wh = (int)(op - optlist);
        mvwaddstr(hw, wh, 0, op->o_prompt);
        mvwaddstr(hw, wh, 16, op->o_opt);
    }
    /*
     * Set values
     */
    wmove(hw, 0, 0);
    for(op = optlist; op < &optlist[NUM_OPTS]; op++) {
        wmove(hw, (int)(op - optlist), 16);
        int wh;
        if((wh = get_str(op->o_opt, hw))) {
            if(wh == QUIT) {
                break;
            }
            if(op > optlist) {
                wmove(hw, (int)(op - optlist), 0);
                op -= 2;
            } else {
                putchar(7);
                wmove(hw, 0, 0);
                op -= 1;
            }
        }
    }
    /*
     * Switch back to original screen
     */
    dbotline(hw, spacemsg);
    restscr(cw);
    after = FALSE;
}

/*
 * get_str:
 *     Set a string option
 */
#define CTRLB 2
int get_str(char *opt, WINDOW *awin) {
    char *sp;
    int c, oy, ox;
    char buf[LINLEN];

    draw(awin);
    getyx(awin, oy, ox);
    /*
     * loop reading in the string, and put it in a temporary buffer
     */
    for(sp = buf; (c = wgetch(awin)) != '\n' && c != '\r' && c != ESCAPE;
        wclrtoeol(awin), draw(awin)) {
        if((sp - buf) >= 50) {
            *sp = '\0'; /* line was too long */
            strucpy(opt, buf, strlen(buf));
            mvwaddstr(awin, 0, 0, "Name was truncated --More--");
            wclrtoeol(awin);
            draw(awin);
            wait_for(awin, ' ');
            mvwprintw(awin, 0, 0, "Called: %s", opt);
            draw(awin);
            return NORM;
        }
        if(c == -1) {
            continue;
        }
        if(c == terminal.c_cc[VERASE]) { /* process erase char */
            if(sp > buf) {
                sp--;
                for(size_t i = strlen(unctrl_int(*sp)); i; i--) {
                    waddch(awin, '\b');
                }
            }
            continue;
        }
        if(c == terminal.c_cc[VKILL]) { /* process kill character */
            sp = buf;
            wmove(awin, oy, ox);
            continue;
        }
        if(sp == buf) {
            if(c == CTRLB) { /* CTRL - B */
                break;
            }
            if(c == '~') {
                strcpy(buf, home);
                waddstr(awin, home);
                sp += strlen(home);
                continue;
            }
        }
        *sp++ = (char)c;
        waddstr(awin, unctrl_int(c));
    }
    *sp = '\0';
    if(sp > buf) { /* only change option if something was typed */
        strucpy(opt, buf, strlen(buf));
    }
    wmove(awin, oy, ox);
    waddstr(awin, opt);
    waddstr(awin, "\n\r");
    draw(awin);
    if(awin == cw) {
        mpos += sp - buf;
    }
    if(c == CTRLB) {
        return MINUS;
    }
    if(c == ESCAPE) {
        return QUIT;
    }
    return NORM;
}

/*
 * parse_opts:
 *     Parse options from string, usually taken from the environment.
 *     the string is a series of comma seperated values, with strings
 *     being "name=....", with the string being defined up to a comma
 *     or the end of the entire option string.
 */

void parse_opts(const char *str) {
    const char *sp;

    while(*str) {
        for(sp = str; isalpha(*sp); sp++) { /* get option name */
        }
        ssize_t len = (sp - str);
        for(OPTION *op = optlist; op < &optlist[NUM_OPTS]; op++) {
            if(EQSTR(str, op->o_name, (size_t)len)) {
                char *start;

                for(str = sp + 1; *str == '='; str++) {
                }
                if(*str == '~') {
                    strcpy(op->o_opt, home);
                    start = op->o_opt + strlen(home);
                    while(*++str == '/') {
                    }
                } else {
                    start = (char *)op->o_opt;
                }
                for(sp = str + 1; *sp && *sp != ','; sp++) {
                }
                strucpy(start, str, (size_t)(sp - str));
            }
        }
        /*
         * skip to start of next option name
         */
        while(*sp && !isalpha(*sp)) {
            sp++;
        }
        str = sp;
    }
}

/*
 * copy string using unctrl for things
 */
void strucpy(char *s1, const char *s2, size_t len) {
    const char *sp;

    while(len-- > 0) {
        strcpy(s1, (sp = unctrl_int(*s2)));
        s1 += strlen(sp);
        s2++;
    }
    *s1 = '\0';
}
