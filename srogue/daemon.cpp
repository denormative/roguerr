/*
 * Contains functions for dealing with things that
 * happen in the future.
 *
 * @(#)daemon.c     9.0     (rdk)      7/17/84
 *
 * Super-Rogue
 * Copyright (C) 1984 Robert D. Kindelberger
 * All rights reserved.
 *
 * Based on "Rogue: Exploring the Dungeons of Doom"
 * Copyright (C) 1980, 1981 Michael Toy, Ken Arnold and Glenn Wichman
 * All rights reserved.
 *
 * See the file LICENSE.TXT for full copyright and licensing information.
 */

#include "rogue.h"

static const int EMPTY = 0;
static const int DAEMON = -1;

delayed_action d_list[MAXDAEMONS] = {};

/*
 * d_insert:
 *     Insert a function in the daemon list.
 */
delayed_action *d_insert(enum daemon_id did, int arg, int type, int time) {
    delayed_action *dev;

    if(demoncnt < MAXDAEMONS) {
        dev = &d_list[demoncnt];
        dev->d_type = type;
        dev->d_time = time;
        dev->d_id = did;
        dev->d_arg = arg;
        demoncnt += 1;
        return dev;
    }
    return nullptr;
}

void d_delete(delayed_action *wire) {
    delayed_action *d1, *d2;

    for(d1 = d_list; d1 < &d_list[demoncnt]; d1++) {
        if(wire == d1) {
            for(d2 = d1 + 1; d2 < &d_list[demoncnt]; d2++) {
                *d1++ = *d2;
            }
            demoncnt -= 1;
            d1 = &d_list[demoncnt];
            d1->d_type = EMPTY;
            d1->d_id = d_none;
            return;
        }
    }
}
/*
 * find_slot:
 *     Find a particular slot in the table
 */
delayed_action *find_slot(enum daemon_id did) {
    delayed_action *dev;

    for(dev = d_list; dev < &d_list[demoncnt]; dev++) {
        if(dev->d_type != EMPTY && did == dev->d_id) {
            return dev;
        }
    }
    return nullptr;
}

/*
 * daemon:
 *     Start a daemon, takes a function.
 */
void start_daemon(enum daemon_id did, int arg, int type) {
    d_insert(did, arg, type, DAEMON);
}

void execute_daemon(delayed_action *dev) {
    switch(dev->d_id) {
        case d_none:
            // nothing to do
            break;
        case d_rollwand:
            rollwand();
            break;
        case d_doctor:
            doctor();
            break;
        case d_stomach:
            stomach();
            break;
        case d_runners:
            runners();
            break;
        case d_swander:
            swander();
            break;
        case d_nohste:
            nohaste();
            break;
        case d_unconfuse:
            unconfuse();
            break;
        case d_unsee:
            unsee();
            break;
        case d_sight:
            sight();
            break;
        case d_noteth:
            noteth();
            break;
        case d_sapem:
            sapem();
            break;
        case d_notslow:
            notslow();
            break;
        case d_notregen:
            notregen();
            break;
        case d_notinvinc:
            notinvinc();
            break;
        case d_rchg_str:
            rchg_str(dev->d_arg);
            break;
        case d_wghtchk:
            wghtchk();
            break;
        case d_status:
            status();
            break;
        case d_nohaste:
            nohaste();
            break;
        default:
            errormsg("daemon type not defined");
            //(*dev->d_func)(dev->d_arg);
    }
}

/*
 * do_daemons:
 *     Run all the daemons that are active with the current
 *     flag, passing the argument to the function.
 */
void do_daemons(int flag) {
    delayed_action *dev;

    for(dev = d_list; dev < &d_list[demoncnt]; dev++) {
        if(dev->d_type == flag && dev->d_time == DAEMON) {
            execute_daemon(dev);
        }
    }
}

/*
 * fuse:
 *     Start a fuse to go off in a certain number of turns
 */
void fuse(enum daemon_id did, int arg, int time) {
    d_insert(did, arg, AFTER, time);
}

/*
 * lengthen:
 *     Increase the time until a fuse goes off
 */
void lengthen(enum daemon_id did, int xtime) {
    delayed_action *wire;

    for(wire = d_list; wire < &d_list[demoncnt]; wire++) {
        if(wire->d_type != EMPTY && did == wire->d_id) {
            wire->d_time += xtime;
        }
    }
}

/*
 * extinguish:
 *     Put out a fuse. Find all such fuses and kill them.
 */
void extinguish(enum daemon_id did) {
    delayed_action *dev;

    for(dev = d_list; dev < &d_list[demoncnt]; dev++) {
        if(dev->d_type != EMPTY && did == dev->d_id) {
            d_delete(dev);
        }
    }
}

/*
 * do_fuses:
 *     Decrement counters and start needed fuses
 */
void do_fuses() {
    delayed_action *dev;

    for(dev = d_list; dev < &d_list[demoncnt]; dev++) {
        if(dev->d_type == AFTER && dev->d_time > DAEMON) {
            if(--dev->d_time == 0) {
                execute_daemon(dev);
                d_delete(dev);
            }
        }
    }
}

/*
 * activity:
 *     Show wizard number of demaons and memory blocks used
 */
void activity() {
    msg("Daemons = %d : Memory Items = %d", demoncnt, total);
}
