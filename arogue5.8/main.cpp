/*
 * Advanced Rogue
 * Copyright (C) 1984, 1985 Michael Morgan, Ken Dalka and AT&T
 * All rights reserved.
 *
 * Based on "Rogue: Exploring the Dungeons of Doom"
 * Copyright (C) 1980, 1981 Michael Toy, Ken Arnold and Glenn Wichman
 * All rights reserved.
 *
 * See the file LICENSE.TXT for full copyright and licensing information.
 */

/*
 * setup code
 */

#include "rogue.h"
#include <climits>
#include <csignal>
#include <ctime>
#include <sys/stat.h>
#include <unistd.h>

/*
 * fruits that you get at startup
 */
static array<string, 26> funfruit = {
    "candleberry", "caprifig",     "dewberry",     "elderberry", "gooseberry", "guanabana",
    "hagberry",    "ilama",        "imbu",         "jaboticaba", "jujube",     "litchi",
    "mombin",      "pitanga",      "prickly pear", "rambutan",   "sapodilla",  "soursop",
    "sweetsop",    "whortleberry", "jellybean",    "apple",      "strawberry", "blueberry",
    "peach",       "banana"};

int main(int argc, char **argv) {
    char *env;
    int lowtime;
    time_t now;
    const char *roguedir = md_getroguedir();

    /*
     * get home and options from environment
     */

    strncpy(home, md_gethomedir(), LINELEN);

    /* Get default save file */
    strcpy(file_name, home);
    strcat(file_name, "arogue58.sav");

    /* Get default score file */
    strcpy(score_file, roguedir);

    if(*score_file) {
        strcat(score_file, "/");
    }

    strcat(score_file, "arogue58.scr");

    if((env = getenv("ROGUEOPTS")) != nullptr) {
        parse_opts(env);
    }

    if(whoami[0] == '\0') {
        strucpy(whoami, md_getusername(), strlen(md_getusername()));
    }

    if(env == nullptr || fruit[0] == '\0') {
        md_srand(((int)time(nullptr)));
        strcpy(fruit, funfruit[rnd(funfruit.size())].c_str());
    }

    /*
     * check for print-score option
     */
    if(argc == 2 && strcmp(argv[1], "-s") == 0) {
        waswizard = TRUE;
        score(0, SCOREIT, 0);
        exit(0);
    }

    /*
     * Check to see if he is a wizard
     */
    if(argc >= 2 && argv[1][0] == '\0') {
        if(strcmp(PASSWD, md_crypt(md_getpass("Wizard's password: "))) == 0) {
            printf("Hail Mighty Wizard\n");
            wizard = TRUE;
            argv++;
            argc--;
        }
    }

    if(argc == 2) {
        if(!restore(argv[1])) { /* Note: restore will never return */
            exit(1);
        }
    }
    lowtime = (int)time(&now);
    dnum = (wizard && getenv("SEED") != nullptr ? atoi(getenv("SEED")) : lowtime);
    if(wizard) {
        printf("Hello %s, welcome to dungeon #%d\n", whoami, dnum);
    } else {
        printf("Hello %s, just a moment while I dig the dungeon...\n", whoami);
    }
    fflush(stdout);
    seed = dnum;
    md_srand(seed);

    init_things();    /* Set up probabilities of things */
    init_colors();    /* Set up colors of potions */
    init_stones();    /* Set up stone settings of rings */
    init_materials(); /* Set up materials of wands */
    initscr();        /* Start up cursor package */
    init_names();     /* Set up names of scrolls */
    init_misc();      /* Set up miscellaneous magic */
    if(LINES < 24 || COLS < 80) {
        printf("\nERROR: screen size to small for rogue\n");
        byebye(-1);
    }

    if((*whoami == '\0') || (strcmp(whoami, "dosuser") == 0)) {
        echo();
        mvaddstr(23, 2, "Rogue's Name? ");
        wgetnstr(stdscr, whoami, LINELEN);
        noecho();
    }

    if(*whoami == '\0') {
        strcpy(whoami, "Rodney");
    }

    setup();
    /*
     * Set up windows
     */
    cw = newwin(LINES, COLS, 0, 0);
    mw = newwin(LINES, COLS, 0, 0);
    hw = newwin(LINES, COLS, 0, 0);
    msgw = newwin(4, COLS, 0, 0);
    keypad(cw, 1);
    keypad(msgw, 1);

    init_player(); /* Roll up the rogue */
    waswizard = wizard;
    new_level(NORMLEV); /* Draw current level */
    /*
     * Start up daemons and fuses
     */
    start_daemon(d_player_doctor, nullptr, AFTER);
    fuse(d_swander, nullptr, WANDERTIME, AFTER);
    start_daemon(d_stomach, nullptr, AFTER);
    start_daemon(d_runners, nullptr, AFTER);
    if(player.t_ctype == C_THIEF) {
        start_daemon(d_trap_look, nullptr, AFTER);
    }

    /* Choose a quest item */
    quest_item = rnd(MAXRELIC);
    msg("You have been quested to retrieve the %s....", rel_magic[quest_item].mi_name);
    mpos = 0;
    playit();
}

/*
 * endit:
 *    Exit the program abnormally.
 */

[[noreturn]] void endit(int sig) {
    fatal("Ok, if you want to exit that badly, I'll have to allow it\n", sig);
}

/*
 * fatal:
 *    Exit the program, printing a message.
 */

[[noreturn]] void fatal(const char *s, const int sig) {
    clear();
    move(LINES - 2, 0);
    printw("%s", s);
    draw(stdscr);
    endwin();
    printf("\n"); /* So the cursor doesn't stop at the end of the line */
    exit(sig);
}

/*
 * rnd:
 *    Pick a very random number.
 */
int rnd(int range) {
    return (range == 0 ? 0 : md_rand() % range);
}

long rnd(long range) {
    return (range == 0 ? 0 : md_rand() % range);
}

size_t rnd(size_t range) {
    return (range == 0 ? 0 : (size_t)md_rand() % range);
}

/*
 * roll:
 *    roll a number of dice
 */

long roll(long number, long sides) {
    int dtotal = 0;

    while(number--) {
        dtotal += rnd(sides) + 1;
    }
    return dtotal;
}
#ifdef SIGTSTP
/*
 * handle stop and start signals
 */
void tstp(int sig) {
    sig = 0; // silence warning
    mvcur(0, COLS - 1, LINES - 1, 0);
    endwin();
    fflush(stdout);
    kill(0, SIGTSTP);
    signal(SIGTSTP, tstp);
    raw();
    noecho();
    keypad(cw, 1);
    clearok(curscr, TRUE);
    touchwin(cw);
    draw(cw);
    flushinp();
}
#endif

void setup() {
#ifndef DUMP
#ifdef SIGHUP
    signal(SIGHUP, auto_save);
#endif
    signal(SIGILL, bugkill);
#ifdef SIGTRAP
    signal(SIGTRAP, bugkill);
#endif
#ifdef SIGIOT
    signal(SIGIOT, bugkill);
#endif
#if 0
    signal(SIGEMT, bugkill);
    signal(SIGFPE, bugkill);
    signal(SIGBUS, bugkill);
    signal(SIGSEGV, bugkill);
    signal(SIGSYS, bugkill);
    signal(SIGPIPE, bugkill);
#endif
    signal(SIGTERM, auto_save);
#endif

    signal(SIGINT, quit);
#ifndef DUMP
#ifdef SIGQUIT
    signal(SIGQUIT, endit);
#endif
#endif
#ifdef SIGTSTP
    signal(SIGTSTP, tstp);
#endif
    crmode(); /* Cbreak mode */
    noecho(); /* Echo off */
}

/*
 * playit:
 *    The main loop of the program.  Loop until the game is over,
 * refreshing things and looking at the proper times.
 */

[[noreturn]] void playit() {
    char *opts;

    /*
     * parse environment declaration of options
     */
    if((opts = getenv("ROGUEOPTS")) != nullptr) {
        parse_opts(opts);
    }

    player.t_oldpos = hero;
    oldrp = roomin(&hero);
    after = TRUE;
    while(playing) {
        command(); /* Command execution */
    }
    endit(0);
}
