/*
 * Advanced Rogue
 * Copyright (C) 1984, 1985 Michael Morgan, Ken Dalka and AT&T
 * All rights reserved.
 *
 * Based on "Rogue: Exploring the Dungeons of Doom"
 * Copyright (C) 1980, 1981 Michael Toy, Ken Arnold and Glenn Wichman
 * All rights reserved.
 *
 * See the file LICENSE.TXT for full copyright and licensing information.
 */

/*
 * Function(s) for dealing with potions
 */

#include "rogue.h"

/*
 * Increase player's constitution
 */

void add_const(bool cursed) {
    /* Do the potion */
    if(cursed) {
        msg("You feel less healthy now.");
        pstats.s_const--;
        if(pstats.s_const <= 0) {
            death(D_CONSTITUTION);
        }
    } else {
        msg("You feel healthier now.");
        pstats.s_const = min(pstats.s_const + 1, 25L);
    }

    /* Adjust the maximum */
    if(max_stats.s_const < pstats.s_const) {
        max_stats.s_const = pstats.s_const;
    }
}

/*
 * Increase player's dexterity
 */

void add_dexterity(bool cursed) {
    int ring_str; /* Value of ring strengths */

    /* Undo any ring changes */
    ring_str = ring_value(R_ADDHIT);
    pstats.s_dext -= ring_str;

    /* Now do the potion */
    if(cursed) {
        msg("You feel less dextrous now.");
        pstats.s_dext--;
    } else {
        msg("You feel more dextrous now.  Watch those hands!");
        pstats.s_dext = min(pstats.s_dext + 1, 25L);
    }

    /* Adjust the maximum */
    if(max_stats.s_dext < pstats.s_dext) {
        max_stats.s_dext = pstats.s_dext;
    }

    /* Now put back the ring changes */
    if(ring_str) {
        pstats.s_dext += ring_str;
    }
}

/*
 * add_haste:
 *    add a haste to the player
 */

void add_haste(bool blessed) {
    int hasttime;

    if(blessed) {
        hasttime = HASTETIME * 2;
    } else {
        hasttime = HASTETIME;
    }

    if(on(player, ISSLOW)) { /* Is person slow? */
        extinguish(d_noslow);
        noslow();

        if(blessed) {
            hasttime = HASTETIME / 2;
        } else {
            return;
        }
    }

    if(on(player, ISHASTE)) {
        msg("You faint from exhaustion.");
        no_command += rnd(hasttime);
        lengthen(d_nohaste, roll(hasttime, hasttime));
    } else {
        turn_on(player, ISHASTE);
        fuse(d_nohaste, nullptr, roll(hasttime, hasttime), AFTER);
    }
}

/*
 * Increase player's intelligence
 */
void add_intelligence(bool cursed) {
    int ring_str; /* Value of ring strengths */

    /* Undo any ring changes */
    ring_str = ring_value(R_ADDINTEL);
    pstats.s_intel -= ring_str;

    /* Now do the potion */
    if(cursed) {
        msg("You feel slightly less intelligent now.");
        pstats.s_intel--;
    } else {
        msg("You feel more intelligent now.  What a mind!");
        pstats.s_intel = min(pstats.s_intel + 1, 25L);
    }

    /* Adjust the maximum */
    if(max_stats.s_intel < pstats.s_intel) {
        max_stats.s_intel = pstats.s_intel;
    }

    /* Now put back the ring changes */
    if(ring_str) {
        pstats.s_intel += ring_str;
    }
}

/*
 * this routine makes the hero move slower
 */
void add_slow() {
    if(on(player, ISHASTE)) { /* Already sped up */
        extinguish(d_nohaste);
        nohaste();
    } else {
        msg("You feel yourself moving %sslower.", on(player, ISSLOW) ? "even " : "");
        if(on(player, ISSLOW)) {
            lengthen(d_noslow, roll(HASTETIME, HASTETIME));
        } else {
            turn_on(player, ISSLOW);
            player.t_turn = TRUE;
            fuse(d_noslow, nullptr, roll(HASTETIME, HASTETIME), AFTER);
        }
    }
}

/*
 * Increase player's strength
 */

void add_strength(bool cursed) {

    if(cursed) {
        msg("You feel slightly weaker now.");
        chg_str(-1);
    } else {
        msg("You feel stronger now.  What bulging muscles!");
        chg_str(1);
    }
}

/*
 * Increase player's wisdom
 */

void add_wisdom(bool cursed) {
    int ring_str; /* Value of ring strengths */

    /* Undo any ring changes */
    ring_str = ring_value(R_ADDWISDOM);
    pstats.s_wisdom -= ring_str;

    /* Now do the potion */
    if(cursed) {
        msg("You feel slightly less wise now.");
        pstats.s_wisdom--;
    } else {
        msg("You feel wiser now.  What a sage!");
        pstats.s_wisdom = min(pstats.s_wisdom + 1, 25L);
    }

    /* Adjust the maximum */
    if(max_stats.s_wisdom < pstats.s_wisdom) {
        max_stats.s_wisdom = pstats.s_wisdom;
    }

    /* Now put back the ring changes */
    if(ring_str) {
        pstats.s_wisdom += ring_str;
    }
}

/*
 * Lower a level of experience
 */

void lower_level(long who) {
    long fewer, nsides = 0;

    if(--pstats.s_lvl == 0) {
        death(who); /* All levels gone */
    }
    msg("You suddenly feel less skillful.");
    pstats.s_exp /= 2;
    switch(player.t_ctype) {
        case C_FIGHTER:
            nsides = HIT_FIGHTER;
            break;
        case C_MAGICIAN:
            nsides = HIT_MAGICIAN;
            break;
        case C_CLERIC:
            nsides = HIT_CLERIC;
            break;
        case C_THIEF:
            nsides = HIT_THIEF;
    }
    fewer = max(1L, roll(1, nsides) + const_bonus());
    pstats.s_hpt -= fewer;
    max_stats.s_hpt -= fewer;
    if(pstats.s_hpt < 1) {
        pstats.s_hpt = 1;
    }
    if(max_stats.s_hpt < 1) {
        death(who);
    }
}

void quaff(long which, long flag, bool is_potion) {
    object *obj = nullptr;
    linked_list *item = nullptr, *titem;
    thing *th;
    bool cursed = FALSE, blessed = FALSE;
    char buf[LINELEN];

    if(which < 0) { /* figure out which ourselves */
        item = get_item(pack, "quaff", POTION);
        /*
         * Make certain that it is somethings that we want to drink
         */
        if(item == nullptr) {
            return;
        }

        obj = OBJPTR(item);
        /* remove it from the pack */
        inpack--;
        detach(pack, item);

        /*
         * Calculate the effect it has on the poor guy.
         */
        cursed = (obj->o_flags & ISCURSED) != 0;
        blessed = (obj->o_flags & ISBLESSED) != 0;

        which = obj->o_which;
    } else {
        cursed = flag & ISCURSED;
        blessed = flag & ISBLESSED;
    }

    switch(which) {
        case P_CLEAR:
            if(cursed) {
                if(off(player, ISCLEAR)) {
                    msg("Wait, what's going on here. Huh? What? Who?");
                    if(find_slot(d_unconfuse)) {
                        lengthen(d_unconfuse, rnd(8) + HUHDURATION);
                    } else {
                        fuse(d_unconfuse, nullptr, rnd(8) + HUHDURATION, AFTER);
                    }
                    turn_on(player, ISHUH);
                } else {
                    msg("You feel dizzy for a moment, but it quickly passes.");
                }
            } else {
                if(blessed) {                /* Make player immune for the whole game */
                    extinguish(d_unclrhead); /* If we have a fuse, put it out */
                    msg("A strong blue aura surrounds your head.");
                } else { /* Just light a fuse for how long player is safe */
                    if(off(player, ISCLEAR)) {
                        fuse(d_unclrhead, nullptr, CLRDURATION, AFTER);
                        msg("A faint blue aura surrounds your head.");
                    } else {
                        /* If we have a fuse lengthen it, else we
                         * are permanently clear.
                         */
                        if(find_slot(d_unclrhead) == nullptr) {
                            msg("Your blue aura continues to glow strongly.");
                        } else {
                            lengthen(d_unclrhead, CLRDURATION);
                            msg("Your blue aura brightens for a moment.");
                        }
                    }
                }
                turn_on(player, ISCLEAR);
                /* If player is confused, unconfuse him */
                if(on(player, ISHUH)) {
                    extinguish(d_unconfuse);
                    unconfuse();
                }
            }
            break;
        case P_HEALING:
            if(cursed) {
                if(!save(VS_POISON, &player, 0)) {
                    msg("You feel very sick now.");
                    pstats.s_hpt /= 2;
                    pstats.s_const--;
                    if(pstats.s_const <= 0 || pstats.s_hpt <= 0) {
                        death(D_POISON);
                    }
                } else {
                    msg("You feel momentarily sick.");
                }
            } else {
                if(blessed) {
                    if((pstats.s_hpt += roll(pstats.s_lvl, 8)) > max_stats.s_hpt) {
                        pstats.s_hpt = ++max_stats.s_hpt;
                    }
                    if(on(player, ISHUH)) {
                        extinguish(d_unconfuse);
                        unconfuse();
                    }
                } else {
                    if((pstats.s_hpt += roll(pstats.s_lvl, 4)) > max_stats.s_hpt) {
                        pstats.s_hpt = ++max_stats.s_hpt;
                    }
                }
                msg("You begin to feel %sbetter.", blessed ? "much " : "");
                sight();
                if(is_potion) {
                    p_know[P_HEALING] = TRUE;
                }
            }
            break;
        case P_ABIL: {
            long ctype;

            /*
             * if blessed then fix all attributes
             */
            if(blessed) {
                add_intelligence(FALSE);
                add_dexterity(FALSE);
                add_strength(FALSE);
                add_wisdom(FALSE);
                add_const(FALSE);
            }
            /* probably will be own ability */
            else {
                if(rnd(100) < 70) {
                    ctype = player.t_ctype;
                } else {
                    do {
                        ctype = rnd(4);
                    } while(ctype == player.t_ctype);
                }

                /* Small chance of doing constitution instead */
                if(rnd(100) < 10) {
                    add_const(cursed);
                } else {
                    switch(ctype) {
                        case C_FIGHTER:
                            add_strength(cursed);
                            break;
                        case C_MAGICIAN:
                            add_intelligence(cursed);
                            break;
                        case C_CLERIC:
                            add_wisdom(cursed);
                            break;
                        case C_THIEF:
                            add_dexterity(cursed);
                            break;
                        default:
                            msg("You're a strange type!");
                    }
                }
            }
            if(is_potion) {
                p_know[P_ABIL] = TRUE;
            }
        } break;
        case P_MFIND:
            /*
             * Potion of monster detection, if there are monters, detect them
             */
            if(mlist != nullptr) {
                msg("You begin to sense the presence of monsters.");
                waddstr(cw, morestr);
                overlay(mw, cw);
                draw(cw);
                wait_for(cw, ' ');
                msg("");
                if(is_potion) {
                    p_know[P_MFIND] = TRUE;
                }
            } else {
                msg("You have a strange feeling for a moment, then it passes.");
            }
            break;
        case P_TFIND:
            /*
             * Potion of magic detection.  Show the potions and scrolls
             */
            if(lvl_obj != nullptr) {
                linked_list *mobj;
                object *tp;
                bool show;

                show = FALSE;
                wclear(hw);
                for(mobj = lvl_obj; mobj != nullptr; mobj = next(mobj)) {
                    tp = OBJPTR(mobj);
                    if(is_magic(tp)) {
                        char mag_type = MAGIC;

                        /* Mark cursed items or bad weapons */
                        if((tp->o_flags & ISCURSED) ||
                           (tp->o_type == WEAPON && (tp->o_hplus < 0 || tp->o_dplus < 0))) {
                            mag_type = CMAGIC;
                        } else if((tp->o_flags & ISBLESSED) ||
                                  (tp->o_type == WEAPON && (tp->o_hplus > 0 || tp->o_dplus > 0))) {
                            mag_type = BMAGIC;
                        }
                        show = TRUE;
                        hwaddch(tp->o_pos.y, tp->o_pos.x, mag_type);
                    }
                    if(is_potion) {
                        p_know[P_TFIND] = TRUE;
                    }
                }
                for(titem = mlist; titem != nullptr; titem = next(titem)) {
                    linked_list *pitem;

                    th = THINGPTR(titem);
                    for(pitem = th->t_pack; pitem != nullptr; pitem = next(pitem)) {
                        tp = OBJPTR(pitem);
                        if(is_magic(tp)) {
                            char mag_type = MAGIC;

                            /* Mark cursed items or bad weapons */
                            if((tp->o_flags & ISCURSED) ||
                               (tp->o_type == WEAPON && (tp->o_hplus < 0 || tp->o_dplus < 0))) {
                                mag_type = CMAGIC;
                            } else if((tp->o_flags & ISBLESSED) ||
                                      (tp->o_type == WEAPON &&
                                       (tp->o_hplus > 0 || tp->o_dplus > 0))) {
                                mag_type = BMAGIC;
                            }
                            show = TRUE;
                            hwaddch(th->t_pos.y, th->t_pos.x, mag_type);
                        }
                        if(is_potion) {
                            p_know[P_TFIND] = TRUE;
                        }
                    }
                }
                if(show) {
                    msg("You sense the presence of magic on this level.");
                    waddstr(cw, morestr);
                    overlay(hw, cw);
                    draw(cw);
                    wait_for(cw, ' ');
                    msg("");
                    break;
                }
            }
            msg("You have a strange feeling for a moment, then it passes.");
            break;
        case P_SEEINVIS:
            if(cursed) {
                if(!find_slot(d_sight)) {
                    msg("A cloak of darkness falls around you.");
                    turn_on(player, ISBLIND);
                    fuse(d_sight, nullptr, SEEDURATION, AFTER);
                    light(&hero);
                } else {
                    lengthen(d_sight, SEEDURATION);
                }
            } else {
                if(off(player, CANSEE)) {
                    turn_on(player, CANSEE);
                    msg("Your eyes begin to tingle.");
                    fuse(d_unsee, nullptr, blessed ? SEEDURATION * 3 : SEEDURATION, AFTER);
                    light(&hero);
                } else if(find_slot(d_unsee) != nullptr) {
                    lengthen(d_unsee, blessed ? SEEDURATION * 3 : SEEDURATION);
                }
                sight();
            }
            break;
        case P_PHASE:
            if(cursed) {
                msg("You can't move.");
                no_command = FREEZETIME;
            } else {
                int duration;

                if(blessed) {
                    duration = 3;
                } else {
                    duration = 1;
                }

                if(on(player, CANINWALL)) {
                    lengthen(d_unphase, duration * PHASEDURATION);
                } else {
                    fuse(d_unphase, nullptr, duration * PHASEDURATION, AFTER);
                    turn_on(player, CANINWALL);
                }
                msg("You feel %slight-headed!", blessed ? "very " : "");
            }
            break;
        case P_FLY: {
            int duration;
            bool say_message;

            say_message = TRUE;

            if(blessed) {
                duration = 3;
            } else {
                duration = 1;
            }

            if(on(player, ISFLY)) {
                if(find_slot(d_land)) {
                    lengthen(d_land, duration * FLYTIME);
                } else {
                    msg("Nothing happens."); /* Flying by cloak */
                    say_message = FALSE;
                }
            } else {
                fuse(d_land, nullptr, duration * FLYTIME, AFTER);
                turn_on(player, ISFLY);
            }
            if(say_message) {
                msg("You feel %slighter than air!", blessed ? "much " : "");
            }
        } break;
        case P_RAISE:
            if(cursed) {
                lower_level(D_POTION);
            } else {
                msg("You suddenly feel %smore skillful", blessed ? "much " : "");
                p_know[P_RAISE] = TRUE;
                raise_level(TRUE);
                if(blessed) {
                    raise_level(TRUE);
                }
            }
            break;
        case P_HASTE:
            if(cursed) { /* Slow player down */
                add_slow();
            } else {
                if(off(player, ISSLOW)) {
                    msg("You feel yourself moving %sfaster.", blessed ? "much " : "");
                }
                add_haste(blessed);
                if(is_potion) {
                    p_know[P_HASTE] = TRUE;
                }
            }
            break;
        case P_RESTORE: {
            int i;

            msg("Hey, this tastes great.  It make you feel warm all over.");
            if(lost_str) {
                extinguish(d_res_strength);
                lost_str = 0;
            }
            for(i = 0; i < lost_dext; i++) {
                extinguish(d_un_itch);
            }
            lost_dext = 0;
            res_strength();
            res_wisdom();
            res_dexterity(0);
            res_intelligence();
            pstats.s_const = max_stats.s_const;
        } break;
        case P_INVIS:
            if(off(player, ISINVIS)) {
                turn_on(player, ISINVIS);
                msg("You have a tingling feeling all over your body");
                fuse(d_appear, nullptr, blessed ? GONETIME * 3 : GONETIME, AFTER);
                PLAYER = IPLAYER;
                light(&hero);
            } else {
                if(find_slot(d_appear)) {
                    msg("Your tingling feeling surges.");
                    lengthen(d_appear, blessed ? GONETIME * 3 : GONETIME);
                } else {
                    msg("Nothing happens."); /* Using cloak */
                }
            }

            break;
        default:
            msg("What an odd tasting potion!");
            return;
    }
    status(FALSE);
    if(is_potion && p_know[which] && p_guess[which]) {
        free(p_guess[which]);
        p_guess[which] = nullptr;
    } else if(is_potion && !p_know[which] && askme && (obj->o_flags & ISKNOW) == 0 &&
              (obj->o_flags & ISPOST) == 0 && p_guess[which] == nullptr) {
        msg(terse ? "Call it: " : "What do you want to call it? ");
        if(get_str(buf, cw) == NORM) {
            p_guess[which] = newalloc(strlen(buf) + 1);
            strcpy(p_guess[which], buf);
        }
    }
    if(item != nullptr) {
        o_discard(item);
    }
    updpack(TRUE);
}

/*
 * res_dexterity:
 *    Restore player's dexterity
 *    if called with zero the restore fully
 */

void res_dexterity(int howmuch) {
    /* Discount the ring value */
    int ring_str = ring_value(R_ADDHIT);
    pstats.s_dext -= ring_str;

    if(pstats.s_dext < max_stats.s_dext) {
        if(howmuch == 0) {
            pstats.s_dext = max_stats.s_dext;
        } else {
            pstats.s_dext = min(pstats.s_dext + howmuch, max_stats.s_dext);
        }
    }

    /* Redo the rings */
    if(ring_str) {
        long save_max = max_stats.s_dext;
        pstats.s_dext += ring_str;
        max_stats.s_dext = save_max;
    }
}

/*
 * res_intelligence:
 *    Restore player's intelligence
 */

void res_intelligence() {
    /* Discount the ring value */
    int ring_str = ring_value(R_ADDINTEL);
    pstats.s_intel -= ring_str;

    if(pstats.s_intel < max_stats.s_intel) {
        pstats.s_intel = max_stats.s_intel;
    }

    /* Redo the rings */
    if(ring_str) {
        long save_max = max_stats.s_intel;
        pstats.s_intel += ring_str;
        max_stats.s_intel = save_max;
    }
}

/*
 * res_wisdom:
 *    Restore player's wisdom
 */

void res_wisdom() {
    /* Discount the ring value */
    int ring_str = ring_value(R_ADDWISDOM);
    pstats.s_wisdom -= ring_str;

    if(pstats.s_wisdom < max_stats.s_wisdom) {
        pstats.s_wisdom = max_stats.s_wisdom;
    }

    /* Redo the rings */
    if(ring_str) {
        long save_max = max_stats.s_wisdom;
        pstats.s_wisdom += ring_str;
        max_stats.s_wisdom = save_max;
    }
}
