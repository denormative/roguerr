/*
 * Advanced Rogue
 * Copyright (C) 1984, 1985 Michael Morgan, Ken Dalka and AT&T
 * All rights reserved.
 *
 * Based on "Rogue: Exploring the Dungeons of Doom"
 * Copyright (C) 1980, 1981 Michael Toy, Ken Arnold and Glenn Wichman
 * All rights reserved.
 *
 * See the file LICENSE.TXT for full copyright and licensing information.
 */

/*
 * Functions for dealing with things that happen in the future.
 */

#include "rogue.h"

static const int EMPTY = 0;
static const int DAEMON = -1;

delayed_action d_list[MAXDAEMONS] = {};
delayed_action f_list[MAXFUSES] = {};
int demoncnt = 0; /* number of active daemons */
int fusecnt = 0;

/*
 * d_slot:
 *    Find an empty slot in the daemon list
 */
delayed_action *d_slot() {
    int i;
    delayed_action *dev;

    for(i = 0, dev = d_list; i < MAXDAEMONS; i++, dev++) {
        if(dev->d_type == EMPTY) {
            return dev;
        }
    }
    return nullptr;
}

/*
 * f_slot:
 *    Find an empty slot in the fuses list
 */
delayed_action *f_slot() {
    int i;
    delayed_action *dev;

    for(i = 0, dev = f_list; i < MAXFUSES; i++, dev++) {
        if(dev->d_type == EMPTY) {
            return dev;
        }
    }
    return nullptr;
}

/*
 * find_slot:
 *    Find a particular slot in the table
 */
delayed_action *find_slot(enum daemon_id did) {
    int i;
    delayed_action *dev;

    for(i = 0, dev = f_list; i < MAXFUSES; i++, dev++) {
        if(dev->d_type != EMPTY && did == dev->d_id) {
            return dev;
        }
    }
    return nullptr;
}

/*
 * daemon:
 *    Start a daemon, takes a function.
 */
void start_daemon(enum daemon_id did, void *arg, int type) {
    delayed_action *dev;

    dev = d_slot();
    if(dev != nullptr) {
        dev->d_type = type;
        dev->d_id = did;
        dev->d_arg = arg;
        dev->d_time = DAEMON;
        demoncnt += 1; /* update count */
    }
}

/*
 * kill_daemon:
 *    Remove a daemon from the list
 */
void kill_daemon(enum daemon_id did) {
    delayed_action *dev;
    int i;

    for(i = 0, dev = d_list; i < MAXDAEMONS; i++, dev++) {
        if(dev->d_type != EMPTY && did == dev->d_id) {
            break;
        }
    }
    if(i >= MAXDAEMONS) {
        return; /* if not found, forget it */
    }
    /*
     * Take it out of the list
     */
    dev->d_type = EMPTY;
    dev->d_arg = nullptr;
    dev->d_id = d_none;
    dev->d_time = 0;
    demoncnt -= 1; /* update count */
}

void execute_daemon(delayed_action *dev) {
    switch(dev->d_id) {
        case d_none:
            // nothing to do
            break;
        case d_rollwand:
            rollwand();
            break;
        case d_doctor:
            doctor((thing *)dev->d_arg); // FIXME: this can't work
            break;
        case d_stomach:
            stomach();
            break;
        case d_runners:
            runners();
            break;
        case d_swander:
            swander();
            break;
        case d_trap_look:
            trap_look();
            break;
        case d_ring_search:
            ring_search();
            break;
        case d_ring_teleport:
            ring_teleport();
            break;
        case d_strangle:
            strangle();
            break;
        case d_fumble:
            fumble();
            break;
        case d_wghtchk:
            wghtchk();
            break;
        case d_unstink:
            unstink();
            break;
        case d_res_strength:
            res_strength();
            break;
        case d_un_itch:
            un_itch();
            break;
        case d_cure_disease:
            cure_disease();
            break;
        case d_unconfuse:
            unconfuse();
            break;
        case d_suffocate:
            suffocate();
            // break;
        case d_undance:
            undance();
            break;
        case d_alchemy:
            alchemy((object *)dev->d_arg); // FIXME: this can't work
            break;
        case d_dust_appear:
            dust_appear();
            break;
        case d_unchoke:
            unchoke();
            break;
        case d_sight:
            sight();
            break;
        case d_noslow:
            noslow();
            break;
        case d_nohaste:
            nohaste();
            break;
        case d_unclrhead:
            unclrhead();
            break;
        case d_unsee:
            unsee();
            break;
        case d_unphase:
            unphase();
            break;
        case d_land:
            land();
            break;
        case d_appear:
            appear();
            break;
        case d_player_doctor:
            player_doctor();
            break;

        default:
            errormsg("daemon type not defined");
            //(*dev->d_func)(dev->d_arg);
    }
}

/*
 * do_daemons:
 *    Run all the daemons that are active with the current flag,
 *    passing the argument to the function.
 */
void do_daemons(int flag) {
    delayed_action *dev;

    /*
     * Loop through the devil list
     */
    for(dev = d_list; dev <= &d_list[MAXDAEMONS - 1]; dev++) {
        /*
         * Executing each one, giving it the proper arguments
         */
        if(dev->d_id != d_none && dev->d_type == flag && dev->d_time == DAEMON) {
            execute_daemon(dev);
        }
    }
}

/*
 * fuse:
 *    Start a fuse to go off in a certain number of turns
 */
void fuse(enum daemon_id did, void *arg, long time, int type) {
    delayed_action *wire;

    wire = f_slot();
    if(wire != nullptr) {
        wire->d_type = type;
        wire->d_id = did;
        wire->d_arg = arg;
        wire->d_time = time;
        fusecnt += 1; /* update count */
    }
}

/*
 * lengthen:
 *    Increase the time until a fuse goes off
 */
void lengthen(enum daemon_id did, long xtime) {
    delayed_action *wire;

    if((wire = find_slot(did)) == nullptr) {
        return;
    }
    wire->d_time += xtime;
}

/*
 * extinguish:
 *    Put out a fuse
 */
void extinguish(enum daemon_id did) {
    delayed_action *wire;

    if((wire = find_slot(did)) == nullptr) {
        return;
    }
    wire->d_type = EMPTY;
    wire->d_id = d_none;
    wire->d_arg = nullptr;
    wire->d_time = 0;
    fusecnt -= 1;
}

/*
 * do_fuses:
 *    Decrement counters and start needed fuses
 */
void do_fuses(int flag) {
    delayed_action *wire;

    /*
     * Step though the list
     */
    for(wire = f_list; wire <= &f_list[MAXFUSES - 1]; wire++) {
        /*
         * Decrementing counters and starting things we want.  We also need
         * to remove the fuse from the list once it has gone off.
         */
        if(flag == wire->d_type && wire->d_time > 0 && --wire->d_time == 0) {
            wire->d_type = EMPTY;
            if(wire->d_id != d_none) {
                execute_daemon(wire);
            }
            fusecnt -= 1;
        }
    }
}

/*
 * activity:
 *    Show wizard number of demaons and memory blocks used
 */
void activity() {
    msg("Daemons = %d : Fuses = %d : Memory Items = %d", demoncnt, fusecnt, total);
}
