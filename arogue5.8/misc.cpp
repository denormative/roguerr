/*
 * Advanced Rogue
 * Copyright (C) 1984, 1985 Michael Morgan, Ken Dalka and AT&T
 * All rights reserved.
 *
 * See the file LICENSE.TXT for full copyright and licensing information.
 */

/*
 * routines dealing specifically with miscellaneous magic
 */

#include "rogue.h"

/*
 * See if a monster has some magic it can use.  Use it and return TRUE if so.
 */
bool m_use_item(thing *monster, coord *monst_pos, coord *defend_pos) {
    linked_list *pitem;
    object *obj;
    coord *shoot_dir = can_shoot(monst_pos, defend_pos);
    int dist = DISTANCE(monst_pos->y, monst_pos->x, defend_pos->y, defend_pos->x);

    for(pitem = monster->t_pack; pitem; pitem = next(pitem)) {
        obj = OBJPTR(pitem);
        if(obj->o_type != RELIC) {
            continue; /* Only care about relics now */
        }
        switch(obj->o_which) {
            case MING_STAFF: {
                static object missile = {MISSILE,    {0, 0}, "", 0, "", "0d4 ", nullptr, 0,
                                         WS_MISSILE, 100,    1,  0, 0,  0,      0,       ""};

                if(shoot_dir != nullptr) {
                    sprintf(missile.o_hurldmg, "%ld4", monster->t_stats.s_lvl);
                    do_motion(&missile, shoot_dir->y, shoot_dir->x, monster);
                    hit_monster(unc(missile.o_pos), &missile, monster);
                    return (TRUE);
                }
            } break;
            case ASMO_ROD:
                /* The bolt must be able to reach the defendant */
                if(shoot_dir != nullptr && dist < BOLT_LENGTH * BOLT_LENGTH) {
                    const char *name;

                    switch(rnd(3)) { /* Select a function */
                        case 0:
                            name = "lightning bolt";
                            break;
                        case 1:
                            name = "flame";
                            break;
                        default:
                            name = "ice";
                    }
                    shoot_bolt(monster, *monst_pos, *shoot_dir, FALSE, monster->t_index, name,
                               roll(monster->t_stats.s_lvl, 6));
                    return (TRUE);
                }
                break;
            case BRIAN_MANDOLIN:
                /* The defendant must be the player and within 2 spaces */
                if(ce(*defend_pos, hero) && dist < 9 && no_command == 0 && rnd(100) < 33) {
                    if(!save(VS_MAGIC, &player, -4) && !ISWEARING(R_ALERT)) {
                        msg("Some beautiful music enthralls you.");
                        no_command += FREEZETIME;
                    } else {
                        msg("You wince at a sour note.");
                    }
                    return (TRUE);
                }
                break;
            case GERYON_HORN:
                /* The defendant must be the player and within 2 spaces */
                if(ce(*defend_pos, hero) && dist < 9 &&
                   (off(player, ISFLEE) || player.t_dest != &monster->t_pos) && rnd(100) < 33) {
                    if(!ISWEARING(R_HEROISM) && !save(VS_MAGIC, &player, -4)) {
                        turn_on(player, ISFLEE);
                        player.t_dest = &monster->t_pos;
                        msg("A shrill blast terrifies you.");
                    } else {
                        msg("A shrill blast sends chills up your spine.");
                    }
                    return (TRUE);
                }
        }
    }
    return (FALSE);
}

/*
 * add something to the contents of something else
 */
void put_contents(object *bag, linked_list *item) {
    linked_list *titem;
    object *tobj;

    bag->o_ac++;
    tobj = OBJPTR(item);
    for(titem = bag->contents; titem != nullptr; titem = next(titem)) {
        if((OBJPTR(titem))->o_which == tobj->o_which) {
            break;
        }
    }
    if(titem == nullptr) { /* if not a duplicate put at beginning */
        attach(bag->contents, item);
    } else {
        item->l_prev = titem;
        item->l_next = titem->l_next;
        if(next(titem) != nullptr) {
            (titem->l_next)->l_prev = item;
        }
        titem->l_next = item;
    }
}

/*
 * remove something from something else
 */
void take_contents(object *bag, linked_list *item) {
    if(bag->o_ac <= 0) {
        msg("Nothing to take out");
        return;
    }
    bag->o_ac--;
    detach(bag->contents, item);
    if(!add_pack(item, FALSE)) {
        put_contents(bag, item);
    }
}

void do_bag(linked_list *item) {

    linked_list *titem = nullptr;
    bool doit = TRUE;

    object *obj = OBJPTR(item);
    while(doit) {
        msg("What do you want to do? (* for a list): ");
        mpos = 0;
        switch(readchar()) {
            case EOF:
            case ESCAPE:
                msg("");
                doit = FALSE;
                break;
            case '1':
                inventory(obj->contents, ALL);

                break;
            case '2':
                if(obj->o_ac >= MAXCONTENTS) {
                    msg("the %s is full", m_magic[obj->o_which].mi_name);
                    break;
                }
                switch(obj->o_which) {
                    case MM_BEAKER:
                        titem = get_item(pack, "put in", POTION);
                        break;
                    case MM_BOOK:
                        titem = get_item(pack, "put in", SCROLL);
                }
                if(titem == nullptr) {
                    break;
                }
                detach(pack, titem);
                inpack--;
                put_contents(obj, titem);

                break;
            case '3':
                titem = get_item(obj->contents, "take out", ALL);
                if(titem == nullptr) {
                    break;
                }
                take_contents(obj, titem);

                break;
            case '4':
                switch(obj->o_which) {
                    case MM_BEAKER:
                        titem = get_item(obj->contents, "quaff", ALL);
                        if(titem == nullptr) {
                            break;
                        }
                        obj->o_ac--;
                        detach(obj->contents, titem);
                        quaff((OBJPTR(titem))->o_which,
                              (OBJPTR(titem))->o_flags & (ISCURSED | ISBLESSED), TRUE);
                        o_discard(titem);
                        break;
                    case MM_BOOK:
                        if(on(player, ISBLIND)) {
                            msg("You can't see to read anything");
                            break;
                        }
                        titem = get_item(obj->contents, "read", ALL);
                        if(titem == nullptr) {
                            break;
                        }
                        obj->o_ac--;
                        detach(obj->contents, titem);
                        read_scroll((OBJPTR(titem))->o_which,
                                    (OBJPTR(titem))->o_flags & (ISCURSED | ISBLESSED), TRUE);
                        o_discard(titem);
                }
                doit = FALSE;

                break;
            default:
                wclear(hw);
                touchwin(hw);
                hwaddstr(0, 0, "The following operations are available:");
                hwaddstr(2, 0, "[1]\tInventory\n");
                wprintw(hw, "[2]\tPut something in the %s\n", m_magic[obj->o_which].mi_name);
                wprintw(hw, "[3]\tTake something out of the %s\n", m_magic[obj->o_which].mi_name);
                switch(obj->o_which) {
                    case MM_BEAKER:
                        waddstr(hw, "[4]\tQuaff a potion\n");
                        break;
                    case MM_BOOK:
                        waddstr(hw, "[4]\tRead a scroll\n");
                }
                waddstr(hw, "[ESC]\tLeave this menu\n");
                hwaddstr(LINES - 1, 0, spacemsg);
                draw(hw);
                wait_for(hw, ' ');
                clearok(cw, TRUE);
                touchwin(cw);
        }
    }
}

void do_panic() {
    linked_list *mon;
    thing *th;

    for(int x = hero.x - 2; x <= hero.x + 2; x++) {
        for(int y = hero.y - 2; y <= hero.y + 2; y++) {
            if(y < 1 || x < 0 || y > LINES - 3 || x > COLS - 1) {
                continue;
            }
            if(isalpha(mwinch(y, x))) {
                if((mon = find_mons(y, x)) != nullptr) {
                    th = THINGPTR(mon);
                    if(!on(*th, ISUNDEAD) && !save(VS_MAGIC, th, 0)) {
                        turn_on(*th, ISFLEE);
                        turn_on(*th, WASTURNED);

                        /* If monster was suffocating, stop it */
                        if(on(*th, DIDSUFFOCATE)) {
                            turn_off(*th, DIDSUFFOCATE);
                            extinguish(d_suffocate);
                        }

                        /* If monster held us, stop it */
                        if(on(*th, DIDHOLD) && (--hold_count == 0)) {
                            turn_off(player, ISHELD);
                        }
                        turn_off(*th, DIDHOLD);
                    }
                    runto(th, &hero);
                }
            }
        }
    }
}

/*
 * print miscellaneous magic bonuses
 */
const char *misc_name(object *obj) {
    static char buf[LINELEN];
    char buf1[LINELEN];

    buf[0] = '\0';
    buf1[0] = '\0';
    if(!(obj->o_flags & ISKNOW)) {
        return (m_magic[obj->o_which].mi_name);
    }
    switch(obj->o_which) {
        case MM_BRACERS:
        case MM_PROTECT:
            strcat(buf, num(obj->o_ac, 0));
            strcat(buf, " ");
    }
    switch(obj->o_which) {
        case MM_G_OGRE:
        case MM_G_DEXTERITY:
        case MM_JEWEL:
        case MM_STRANGLE:
        case MM_R_POWERLESS:
        case MM_DANCE:
            if(obj->o_flags & ISCURSED) {
                strcat(buf, "cursed ");
            }
    }
    strcat(buf, m_magic[obj->o_which].mi_name);
    switch(obj->o_which) {
        case MM_JUG:
            if(obj->o_ac == JUG_EMPTY) {
                strcat(buf1, " [empty]");
            } else if(p_know[obj->o_ac]) {
                sprintf(buf1, " [containing a potion of %s (%s)]", p_magic[obj->o_ac].mi_name,
                        p_colors[obj->o_ac]);
            } else {
                sprintf(buf1, " [containing a%s %s liquid]", vowelstr(p_colors[obj->o_ac]),
                        p_colors[obj->o_ac]);
            }
            break;
        case MM_BEAKER:
        case MM_BOOK: {
            sprintf(buf1, " [containing %ld]", obj->o_ac);
        } break;
        case MM_OPEN:
        case MM_HUNGER:
            sprintf(buf1, " [%ld ring%s]", obj->o_charges, obj->o_charges == 1 ? "" : "s");
            break;
        case MM_DRUMS:
            sprintf(buf1, " [%ld beat%s]", obj->o_charges, obj->o_charges == 1 ? "" : "s");
            break;
        case MM_DISAPPEAR:
        case MM_CHOKE:
            sprintf(buf1, " [%ld pinch%s]", obj->o_charges, obj->o_charges == 1 ? "" : "es");
            break;
        case MM_KEOGHTOM:
            sprintf(buf1, " [%ld application%s]", obj->o_charges, obj->o_charges == 1 ? "" : "s");
            break;
        case MM_SKILLS:
            switch(obj->o_ac) {
                case C_MAGICIAN:
                    strcpy(buf1, " [magic user]");
                    break;
                case C_FIGHTER:
                    strcpy(buf1, " [fighter]");
                    break;
                case C_CLERIC:
                    strcpy(buf1, " [cleric]");
                    break;
                case C_THIEF:
                    strcpy(buf1, " [thief]");
            }
    }
    strcat(buf, buf1);
    return buf;
}

void use_emori() {
    int selection; /* Cloak function */
    int state = 0; /* Menu state */

    msg("What do you want to do? (* for a list): ");
    do {
        selection = tolower(readchar());
        switch(selection) {
            case '*':
                if(state != 1) {
                    wclear(hw);
                    touchwin(hw);
                    hwaddstr(2, 0, "[1] Fly\n[2] Stop flying\n");
                    waddstr(hw, "[3] Turn invisible\n[4] Turn Visible\n");
                    hwaddstr(0, 0, "What do you want to do? ");
                    draw(hw);
                    state = 1; /* Now in prompt window */
                }
                break;

            case ESCAPE:
                if(state == 1) {
                    clearok(cw, TRUE); /* Set up for redraw */
                    touchwin(cw);
                }
                msg("");

                after = FALSE;
                return;

                // break;
            case '1':
            case '2':
            case '3':
            case '4':
                if(state == 1) {       /* In prompt window */
                    clearok(cw, TRUE); /* Set up for redraw */
                    touchwin(cw);
                }

                msg("");

                state = 2; /* Finished */
                break;

            default:
                if(state == 1) { /* In the prompt window */
                    hwaddstr(0, 0, "Please enter a selection between 1 and 4:  ");
                    draw(hw);
                } else { /* Normal window */
                    mpos = 0;
                    msg("Please enter a selection between 1 and 4:  ");
                }
        }
    } while(state != 2);

    /* We now must have a selection between 1 and 4 */
    switch(selection) {
        case '1': /* Fly */
            if(on(player, ISFLY)) {
                extinguish(d_land); /* Extinguish in case of potion */
                msg("%slready flying.", terse ? "A" : "You are a");
            } else {
                msg("You feel lighter than air!");
                turn_on(player, ISFLY);
            }
            break;
        case '2': /* Stop flying */
            if(off(player, ISFLY)) {
                msg("%sot flying.", terse ? "N" : "You are n");
            } else {
                if(find_slot(d_land)) {
                    msg("%sot flying by the cloak.", terse ? "N" : "You are n");
                } else {
                    land();
                }
            }
            break;
        case '3': /* Turn invisible */
            if(off(player, ISINVIS)) {
                turn_on(player, ISINVIS);
                msg("You have a tingling feeling all over your body");
                PLAYER = IPLAYER;
                light(&hero);
            } else {
                extinguish(d_appear);      /* Extinguish in case of potion */
                extinguish(d_dust_appear); /* dust of disappearance        */
                msg("%slready invisible.", terse ? "A" : "You are a");
            }
            break;
        case '4': /* Turn visible */
            if(off(player, ISINVIS)) {
                msg("%sot invisible.", terse ? "N" : "You are n");
            } else {
                if(find_slot(d_appear) || find_slot(d_dust_appear)) {
                    msg("%sot invisible by the cloak.", terse ? "N" : "You are n");
                } else {
                    appear();
                }
            }
    }
}

void use_mm(long which) {
    object *obj = nullptr;
    linked_list *item = nullptr;
    bool cursed = FALSE, blessed = FALSE, is_mm = FALSE;
    char buf[LINELEN];

    if(which < 0) { /* A real miscellaneous magic item  */
        is_mm = TRUE;
        item = get_item(pack, "use", USEABLE);
        /*
         * Make certain that it is a micellaneous magic item
         */
        if(item == nullptr) {
            return;
        }

        obj = OBJPTR(item);
        cursed = (obj->o_flags & ISCURSED) != 0;
        blessed = (obj->o_flags & ISBLESSED) != 0;
        which = obj->o_which;
    }

    assert(obj != nullptr);
    if(obj->o_type == RELIC) { /* An artifact */
        is_mm = FALSE;
        switch(obj->o_which) {
            case EMORI_CLOAK:
                use_emori();
                break;
            case BRIAN_MANDOLIN:
                /* Put monsters around us to sleep */
                read_scroll(S_HOLD, 0, FALSE);
                break;
            case GERYON_HORN:
                /* Chase close monsters away */
                msg("The horn blasts a shrill tone.");
                do_panic();
                break;
            case HEIL_ANKH:
            case YENDOR_AMULET:
                /* Nothing happens by this mode */
                msg("Nothing happens.");
        }
    } else {
        switch(which) { /* Miscellaneous Magic */
            /*
             * the jug of alchemy manufactures potions when you drink
             * the potion it will make another after a while
             */
            case MM_JUG:
                if(obj->o_ac == JUG_EMPTY) {
                    msg("The jug is empty");
                    break;
                }
                quaff(obj->o_ac, 0, FALSE);
                obj->o_ac = JUG_EMPTY;
                fuse(d_alchemy, (void *)obj, ALCHEMYTIME, AFTER);
                if(!(obj->o_flags & ISKNOW)) {
                    whatis(item);
                }

                /*
                 * the beaker of plentiful potions is used to hold potions
                 * the book of infinite spells is used to hold scrolls
                 */
                break;
            case MM_BEAKER:
            case MM_BOOK:
                do_bag(item);

                /*
                 * the chime of opening opens up secret doors
                 */
                break;
            case MM_OPEN: {
                linked_list *exit;
                room *rp;
                coord *cp;

                if(obj->o_charges <= 0) {
                    msg("The chime is cracked!");
                    break;
                }
                obj->o_charges--;
                msg("chime... chime... hime... ime... me... e...");
                if((rp = roomin(&hero)) == nullptr) {
                    search(FALSE, TRUE); /* Non-failing search for door */
                    break;
                }
                for(exit = rp->r_exit; exit != nullptr; exit = next(exit)) {
                    cp = DOORPTR(exit);
                    if(winat(cp->y, cp->x) == SECRETDOOR) {
                        mvaddch(cp->y, cp->x, DOOR);
                        if(cansee(cp->y, cp->x)) {
                            cwaddch(cp->y, cp->x, DOOR);
                        }
                    }
                }
            }

            /*
             * the chime of hunger just makes the hero hungry
             */
            break;
            case MM_HUNGER:
                if(obj->o_charges <= 0) {
                    msg("The chime is cracked!");
                    break;
                }
                obj->o_charges--;
                food_left = MORETIME + 5;
                msg(terse ? "Getting hungry" : "You are starting to get hungry");
                hungry_state = F_HUNGRY;
                aggravate();

                /*
                 * the drums of panic make all creatures within two squares run
                 * from the hero in panic unless they save or they are mindless
                 * undead
                 */
                break;
            case MM_DRUMS:
                if(obj->o_charges <= 0) {
                    msg("The drum is broken!");
                    break;
                }
                obj->o_charges--;
                /*
                 * dust of disappearance makes the player invisible for a while
                 */
                break;
            case MM_DISAPPEAR:
                m_know[MM_DISAPPEAR] = TRUE;
                if(obj->o_charges <= 0) {
                    msg("No more dust!");
                    break;
                }
                obj->o_charges--;
                msg("aaAAACHOOOooo. Cough. Cough. Sneeze. Sneeze.");
                if(!find_slot(d_dust_appear)) {
                    turn_on(player, ISINVIS);
                    fuse(d_dust_appear, nullptr, DUSTTIME, AFTER);
                    PLAYER = IPLAYER;
                    light(&hero);
                } else {
                    lengthen(d_dust_appear, DUSTTIME);
                }

                /*
                 * dust of choking and sneezing can kill the hero if he misses
                 * the save
                 */
                break;
            case MM_CHOKE:
                m_know[MM_CHOKE] = TRUE;
                if(obj->o_charges <= 0) {
                    msg("No more dust!");
                    break;
                }
                obj->o_charges--;
                msg("aaAAACHOOOooo. Cough. Cough. Sneeze. Sneeze.");
                if(!save(VS_POISON, &player, 0)) {
                    msg("You choke to death!!! --More--");
                    pstats.s_hpt = -1; /* in case he hangs up the phone */
                    wait_for(cw, ' ');
                    death(D_CHOKE);
                } else {
                    msg("You begin to cough and choke uncontrollably");
                    if(find_slot(d_unchoke)) {
                        lengthen(d_unchoke, DUSTTIME);
                    } else {
                        fuse(d_unchoke, nullptr, DUSTTIME, AFTER);
                    }
                    turn_on(player, ISHUH);
                    turn_on(player, ISBLIND);
                    light(&hero);
                }

                break;
            case MM_KEOGHTOM:
                /*
                 * this is a very powerful healing ointment
                 * but it takes a while to put on...
                 */
                if(obj->o_charges <= 0) {
                    msg("The jar is empty!");
                    break;
                }
                obj->o_charges--;
                waste_time();
                if(on(player, HASDISEASE)) {
                    extinguish(d_cure_disease);
                    cure_disease();
                    msg(terse ? "You feel yourself improving."
                              : "You begin to feel yourself improving again.");
                }
                if(on(player, HASINFEST)) {
                    turn_off(player, HASINFEST);
                    infest_dam = 0;
                    msg(terse ? "You feel yourself improving."
                              : "You begin to feel yourself improving again.");
                }
                if(on(player, DOROT)) {
                    msg("You feel your skin returning to normal.");
                    turn_off(player, DOROT);
                }
                pstats.s_hpt += roll(pstats.s_lvl, 6);
                if(pstats.s_hpt > max_stats.s_hpt) {
                    pstats.s_hpt = max_stats.s_hpt;
                }
                sight();
                msg("You begin to feel much better.");

                /*
                 * The book has a character class associated with it.
                 * if your class matches that of the book, it will raise your
                 * level by one. If your class does not match the one of the book,
                 * it change your class to that of book.
                 * Note that it takes a while to read.
                 */
                break;
            case MM_SKILLS:
                detach(pack, item);
                inpack--;
                waste_time();
                waste_time();
                waste_time();
                waste_time();
                waste_time();
                if(obj->o_ac == player.t_ctype) {
                    msg("You feel more skillful");
                    raise_level(TRUE);
                } else {
                    /*
                     * reset his class and then use check_level to reset hit
                     * points and the right level for his exp pts
                     * drop exp pts by 10%
                     */
                    long save;

                    msg("You feel like a whole new person!");
                    /*
                     * if he becomes a thief he has to have leather armor
                     */
                    if(obj->o_ac == C_THIEF && cur_armor != nullptr &&
                       cur_armor->o_which != LEATHER && cur_armor->o_which != STUDDED_LEATHER) {
                        cur_armor->o_which = STUDDED_LEATHER;
                    }
                    /*
                     * if he's changing from a fighter then may have to change
                     * his sword since only fighter can use two-handed
                     * and bastard swords
                     */
                    if(player.t_ctype == C_FIGHTER && cur_weapon != nullptr &&
                       cur_weapon->o_type == WEAPON &&
                       (cur_weapon->o_which == BASWORD || cur_weapon->o_which == TWOSWORD)) {
                        cur_weapon->o_which = SWORD;
                    }

                    /*
                     * if he was a thief then take out the trap_look() daemon
                     */
                    if(player.t_ctype == C_THIEF) {
                        kill_daemon(d_trap_look);
                    }
                    /*
                     * if he becomes a thief then add the trap_look() daemon
                     */
                    if(obj->o_ac == C_THIEF) {
                        start_daemon(d_trap_look, nullptr, AFTER);
                    }
                    char_type = player.t_ctype = obj->o_ac;
                    save = pstats.s_hpt;
                    max_stats.s_hpt = pstats.s_hpt = 0;
                    max_stats.s_lvl = pstats.s_lvl = 0;
                    max_stats.s_exp = pstats.s_exp -= pstats.s_exp / 10;
                    check_level(TRUE);
                    if(pstats.s_hpt > save) { /* don't add to current hits */
                        pstats.s_hpt = save;
                    }
                }

                break;
            default:
                msg("What a strange magic item you have!");
        }
    }
    status(FALSE);
    if(is_mm && m_know[which] && m_guess[which]) {
        free(m_guess[which]);
        m_guess[which] = nullptr;
    } else if(is_mm && !m_know[which] && askme && (obj->o_flags & ISKNOW) == 0 &&
              m_guess[which] == nullptr) {
        msg(terse ? "Call it: " : "What do you want to call it? ");
        if(get_str(buf, cw) == NORM) {
            m_guess[which] = newalloc(strlen(buf) + 1);
            strcpy(m_guess[which], buf);
        }
    }
    if(item != nullptr && which == MM_SKILLS) {
        o_discard(item);
    }
    updpack(TRUE);
}
