/*
 * Advanced Rogue
 * Copyright (C) 1984, 1985 Michael Morgan, Ken Dalka and AT&T
 * All rights reserved.
 *
 * Based on "Rogue: Exploring the Dungeons of Doom"
 * Copyright (C) 1980, 1981 Michael Toy, Ken Arnold and Glenn Wichman
 * All rights reserved.
 *
 * See the file LICENSE.TXT for full copyright and licensing information.
 */

/*
 * save and restore routines
 */

#include "rogue.h"
#include <cerrno>
#include <csignal>
#include <ctime>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

using STAT = struct stat;

bool save_game() {
    FILE *savef;
    int c;
    char buf[LINELEN];

    /*
     * get file name
     */
    mpos = 0;
    if(file_name[0] != '\0') {
        msg("Save file (%s)? ", file_name);
        do {
            c = readchar();
            if(c == ESCAPE) {
                msg("");
                return (false);
            }
        } while(c != 'n' && c != 'N' && c != 'y' && c != 'Y');
        mpos = 0;
        if(c == 'y' || c == 'Y') {
            msg("File name: %s", file_name);
            goto gotfile;
        }
    }

    do {
        msg("File name: ");
        mpos = 0;
        buf[0] = '\0';
        if(get_str(buf, msgw) == QUIT) {
            msg("");
            return FALSE;
        }
        msg("");
        strcpy(file_name, buf);
    gotfile:
        if((savef = fopen(file_name, "wb")) == nullptr) {
            msg(strerror(errno)); /* fake perror() */
        }
    } while(savef == nullptr);

    /*
     * write out file (after a stat)
     */
    if(save_file(savef) == FALSE) {
        msg("Cannot create save file.");
        md_unlink(file_name);
        return (FALSE);
    }
    return (TRUE);
}

/*
 * automatically save a file.  This is used if a HUP signal is
 * recieved
 */
[[noreturn]] void auto_save(int sig) {
    FILE *savef;
    int i;

    for(i = 0; i < NSIG; i++) {
        signal(i, SIG_IGN);
    }
    if(file_name[0] != '\0' && pstats.s_hpt > 0 && (savef = fopen(file_name, "wb")) != nullptr) {
        save_file(savef);
    }
    endwin();
    exit(sig);
}

/*
 * write the saved game on the file
 */
bool save_file(FILE *savef) {
    int ret;

    wmove(cw, LINES - 1, 0);
    draw(cw);
    encwrite(version, strlen(version) + 1, savef);
    sprintf(prbuf, "%d x %d\n", LINES, COLS);
    encwrite(prbuf, 80, savef);

    msg("");
    ret = rs_save_file(savef);

    fclose(savef);
    return (!ret);
}

int restore(char *file) {
    FILE *inf;
    char buf[LINELEN];
    STAT sbuf2;
    int oldcol, oldline; /* Old number of columns and lines */

    if(strcmp(file, "-r") == 0) {
        file = file_name;
    }

    if((inf = fopen(file, "rb")) == nullptr) {
        perror(file);
        return FALSE;
    }

    fflush(stdout);

    encread(buf, strlen(version) + 1, inf);

    if(strcmp(buf, version) != 0) {
        printf("Sorry, saved game is out of date.\n");
        return FALSE;
    }

    /*
     * Get the lines and columns from the previous game
     */

    encread(buf, 80, inf);
    sscanf(buf, "%d x %d\n", &oldline, &oldcol);
    stat(file, &sbuf2);
    fflush(stdout);

    /* Set the new terminal and make sure we aren't going to a smaller screen. */
    initscr();

    if(COLS < oldcol || LINES < oldline) {
        endwin();
        printf("\nCannot restart the game on a smaller screen.\n");
        return FALSE;
    }

    cw = newwin(LINES, COLS, 0, 0);
    mw = newwin(LINES, COLS, 0, 0);
    hw = newwin(LINES, COLS, 0, 0);
    msgw = newwin(4, COLS, 0, 0);

    keypad(cw, 1);
    keypad(msgw, 1);

    mpos = 0;
    mvwprintw(cw, 0, 0, "%s: %s", file, ctime(&sbuf2.st_mtime));

    /*
     * defeat multiple restarting from the same place
     */
    if(!wizard && md_unlink(file) < 0) {
        endwin();
        printf("\nCannot unlink file\n");
        return FALSE;
    }

    if(rs_restore_file(inf) != 0) {
        endwin();
        printf("\nCannot restore file\n");
        fclose(inf);
        return (FALSE);
    }

    if(!wizard && md_unlink(file) < 0) {
        fclose(inf); /* only close if system insists */
        if(md_unlink(file) < 0) {
            endwin();
            printf("\nCannot unlink file\n");
            return FALSE;
        }
    }

    strcpy(file_name, file);
    setup();
    clearok(curscr, TRUE);
    touchwin(cw);
    md_srand((int)time(nullptr));
    playit();
    /*NOTREACHED*/
    return (FALSE);
}

/* perform a (fake) encrypted write */
size_t encwrite(const void *buf, size_t size, FILE *outf) {
    const char *start = (const char *)buf;

    size_t write_size;
    if((write_size = fwrite(start, 1, size, outf)) == 0) {
        return 0;
    }

    return write_size;
}

/* perform a (fake) encrypted read */
size_t encread(void *buf, size_t size, FILE *inf) {
    char *start = (char *)buf;

    size_t read_size;
    if((read_size = fread(start, 1, size, inf)) == 0) {
        return 0;
    }

    return read_size;
}
