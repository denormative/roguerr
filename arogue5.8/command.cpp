/*
 * Advanced Rogue
 * Copyright (C) 1984, 1985 Michael Morgan, Ken Dalka and AT&T
 * All rights reserved.
 *
 * Based on "Rogue: Exploring the Dungeons of Doom"
 * Copyright (C) 1980, 1981 Michael Toy, Ken Arnold and Glenn Wichman
 * All rights reserved.
 *
 * See the file LICENSE.TXT for full copyright and licensing information.
 */

/*
 * Read and execute the user commands
 */

#include "rogue.h"
#include <climits>
#include <csignal>

/*
 * command:
 *    Process the user commands
 */

void command() {
    int ch;
    int ntimes = 1; /* Number of player moves */
    static int countch, direction;
    static bool newcount = FALSE;
    linked_list *item;
    bool an_after = FALSE;

    if(on(player, ISHASTE)) {
        ntimes++;
        turns--; /* correct for later */
    }
    if(on(player, ISSLOW) || on(player, ISDANCE)) {
        if(player.t_turn != TRUE) {
            ntimes--;
            turns++;
            an_after = TRUE;
        }
        player.t_turn ^= TRUE;
    }

    /*
     * Let the daemons start up
     */
    do_daemons(BEFORE);
    do_fuses(BEFORE);
    while(ntimes-- > 0) {
        /* One more tick of the clock. */
        if((++turns % DAYLENGTH) == 0) {
            daytime ^= TRUE;
            if(levtype == OUTSIDE) {
                if(daytime) {
                    msg("The sun rises above the horizon");
                } else {
                    msg("The sun sinks below the horizon");
                }
            }
            light(&hero);
        }

        look(after, FALSE);
        if(!running) {
            door_stop = FALSE;
        }
        lastscore = purse;
        wmove(cw, hero.y, hero.x);
        if(!((running || count) && jump)) {
            status(FALSE);
            wmove(cw, hero.y, hero.x);
            draw(cw); /* Draw screen */
        }
        take = 0;
        after = TRUE;
        /*
         * Read command or continue run
         */
        if(!no_command) {
            if(running) {
                /* If in a corridor or maze, if we are at a turn with only one
                 * way to go, turn that way.
                 */
                if((winat(hero.y, hero.x) == PASSAGE || levtype == MAZELEV) && off(player, ISHUH) &&
                   (off(player, ISBLIND))) {
                    int y, x;
                    if(getdelta(runch, &y, &x) == TRUE) {
                        corr_move(y, x);
                    }
                }
                ch = runch;
            } else if(count) {
                ch = countch;
            } else {
                ch = readchar();
                if(mpos != 0 && !running) { /* Erase message if its there */
                    msg("");
                }
            }
        } else {
            ch = '.';
        }
        if(no_command) {
            if(--no_command == 0) {
                msg("You can move again.");
            }
        } else {
            /*
             * check for prefixes
             */
            if(isdigit(ch)) {
                count = 0;
                newcount = TRUE;
                while(isdigit(ch)) {
                    count = count * 10 + (ch - '0');
                    if(count > 255) {
                        count = 255;
                    }
                    ch = readchar();
                }
                countch = ch;
                /*
                 * turn off count for commands which don't make sense
                 * to repeat
                 */
                switch(ch) {
                    case 'h':
                    case 'j':
                    case 'k':
                    case 'l':
                    case 'y':
                    case 'u':
                    case 'b':
                    case 'n':
                    case 'H':
                    case 'J':
                    case 'K':
                    case 'L':
                    case 'Y':
                    case 'U':
                    case 'B':
                    case 'N':
                    case 'q':
                    case 'r':
                    case 's':
                    case 'f':
                    case 't':
                    case 'C':
                    case 'I':
                    case '.':
                    case 'z':
                    case 'p':
                        break;
                    default:
                        count = 0;
                }
            }

            /* Save current direction */
            if(!running) { /* If running, it is already saved */
                switch(ch) {
                    case 'h':
                    case 'j':
                    case 'k':
                    case 'l':
                    case 'y':
                    case 'u':
                    case 'b':
                    case 'n':
                    case 'H':
                    case 'J':
                    case 'K':
                    case 'L':
                    case 'Y':
                    case 'U':
                    case 'B':
                    case 'N':
                        runch = tolower(ch);
                }
            }

            /* Perform the action */
            switch(ch) {
                case 'f':
                    if(!on(player, ISBLIND)) {
                        door_stop = TRUE;
                        firstmove = TRUE;
                    }
                    if(count && !newcount) {
                        ch = direction;
                    } else {
                        ch = readchar();
                    }
                    switch(ch) {
                        case 'h':
                        case 'j':
                        case 'k':
                        case 'l':
                        case 'y':
                        case 'u':
                        case 'b':
                        case 'n':
                            ch = toupper(ch);
                    }
                    direction = ch;
            }
            newcount = FALSE;
            /*
             * execute a command
             */
            if(count && !running) {
                count--;
            }
            switch(ch) {
                case 'h':
                    do_move(0, -1);
                    break;
                case 'j':
                    do_move(1, 0);
                    break;
                case 'k':
                    do_move(-1, 0);
                    break;
                case 'l':
                    do_move(0, 1);
                    break;
                case 'y':
                    do_move(-1, -1);
                    break;
                case 'u':
                    do_move(-1, 1);
                    break;
                case 'b':
                    do_move(1, -1);
                    break;
                case 'n':
                    do_move(1, 1);
                    break;
                case 'H':
                    do_run('h');
                    break;
                case 'J':
                    do_run('j');
                    break;
                case 'K':
                    do_run('k');
                    break;
                case 'L':
                    do_run('l');
                    break;
                case 'Y':
                    do_run('y');
                    break;
                case 'U':
                    do_run('u');
                    break;
                case 'B':
                    do_run('b');
                    break;
                case 'N':
                    do_run('n');
                    break;
                case 't':
                    if((item = get_item(pack, "throw", ALL)) != nullptr && get_dir()) {
                        missile(delta.y, delta.x, item, &player);
                    } else {
                        after = FALSE;
                    }
                    break;
                case 'Q':
                    after = FALSE;
                    quit(-1);
                    break;
                case 'i':
                    after = FALSE;
                    inventory(pack, ALL);
                    break;
                case 'I':
                    after = FALSE;
                    picky_inven();
                    break;
                case 'd':
                    drop(nullptr);
                    break;
                case 'P':
                    grab(hero.y, hero.x);
                    break;
                case 'q':
                    quaff(-1, 0, TRUE);
                    break;
                case 'r':
                    read_scroll(-1, 0, TRUE);
                    break;
                case 'e':
                    eat();
                    break;
                case 'w':
                    wield();
                    break;
                case 'W':
                    wear();
                    break;
                case 'T':
                    take_off();
                    break;
                case 'o':
                    option();
                    break;
                case 'c':
                    call(FALSE);
                    break;
                case 'm':
                    call(TRUE);
                    break;
                case '>':
                    after = FALSE;
                    d_level();
                    break;
                case '<':
                    after = FALSE;
                    u_level();
                    break;
                case '?':
                    after = FALSE;
                    help();
                    break;
                case '/':
                    after = FALSE;
                    identify();
                    break;
                case CTRL('U'):
                    use_mm(-1);
                    break;
                case CTRL('T'):
                    if(get_dir()) {
                        steal();
                    } else {
                        after = FALSE;
                    }
                    break;
                case 'D':
                    dip_it();
                    break;
                case 'G':
                    gsense();
                    break;
                case '^':
                    set_trap(&player, hero.y, hero.x);
                    break;
                case 's':
                    search(FALSE, FALSE);
                    break;
                case 'z':
                    if(!do_zap(TRUE, 0, FALSE)) {
                        after = FALSE;
                    }
                    break;
                case 'p':
                    pray();
                    break;
                case 'C':
                    cast();
                    break;
                case 'a':
                    if(get_dir()) {
                        affect();
                    } else {
                        after = FALSE;
                    }
                    break;
                case 'v':
                    after = FALSE;
                    msg("Advanced Rogue Version %s.", release);
                    break;
                case CTRL('L'):
                    after = FALSE;
                    clearok(curscr, TRUE);
                    touchwin(cw); /* MMMMMMMMMM */
                    break;
                case CTRL('R'):
                    after = FALSE;
                    msg(huh);
                    break;
                case 'S':
                    after = FALSE;
                    if(save_game()) {
                        wclear(cw);
                        draw(cw);
                        endwin();
                        printf("\n");
                        exit(0);
                    }
                    break;
                case '.':; /* Rest command */
                    break;
                case ' ':
                    after = FALSE; /* Do Nothing */
                    break;
                case CTRL('P'):
                    after = FALSE;
                    if(wizard) {
                        wizard = FALSE;
                        trader = 0;
                        waswizard = true;
                        msg("Not wizard any more");
                    } else {
                        if(waswizard || passwd()) {
                            msg("Welcome, oh mighty wizard.");
                            wizard = waswizard = TRUE;
                        } else {
                            msg("Sorry");
                        }
                    }
                    break;
                case ESCAPE: /* Escape */
                    door_stop = FALSE;
                    count = 0;
                    after = FALSE;
                    break;
                case '#':
                    if(levtype == POSTLEV) { /* buy something */
                        buy_it();
                    }
                    after = FALSE;
                    break;
                case '$':
                    if(levtype == POSTLEV) { /* price something */
                        price_it();
                    }
                    after = FALSE;
                    break;
                case '%':
                    if(levtype == POSTLEV) { /* sell something */
                        sell_it();
                    }
                    after = FALSE;
                    break;
                default:
                    after = FALSE;
                    if(wizard) {
                        switch(ch) {
                            case 'M':
                                create_obj(TRUE, 0, 0);
                                break;
                            case CTRL('W'):
                                wanderer();
                                break;
                            case CTRL('I'):
                                inventory(lvl_obj, ALL);
                                break;
                            case CTRL('Z'):
                                whatis(nullptr);
                                break;
                            case CTRL('D'):
                                level++;
                                new_level(NORMLEV);
                                break;
                            case CTRL('F'):
                                overlay(stdscr, cw);
                                break;
                            case CTRL('X'):
                                overlay(mw, cw);
                                break;
                            case CTRL('J'):
                                teleport();
                                break;
                            case CTRL('E'):
                                msg("food left: %d\tfood level: %d", food_left, foodlev);
                                break;
                            case CTRL('A'):
                                activity();
                                break;
                            case CTRL('C'): {
                                int tlev;
                                prbuf[0] = '\0';
                                msg("Which level? ");
                                if(get_str(prbuf, cw) == NORM) {
                                    tlev = atoi(prbuf);
                                    if(tlev < 1) {
                                        mpos = 0;
                                        msg("Illegal level.");
                                    } else if(tlev > 199) {
                                        levtype = MAZELEV;
                                        level = tlev - 200 + 1;
                                    } else if(tlev > 99) {
                                        levtype = POSTLEV;
                                        level = tlev - 100 + 1;
                                    } else {
                                        levtype = NORMLEV;
                                        level = tlev;
                                    }
                                    new_level(levtype);
                                }
                            } break;
                            case CTRL('N'): {
                                if((item = get_item(pack, "charge", STICK)) != nullptr) {
                                    (OBJPTR(item))->o_charges = 10000;
                                }
                            } break;
                            case CTRL('H'): {
                                int i;
                                object *obj;

                                for(i = 0; i < 9; i++) {
                                    raise_level(TRUE);
                                }
                                /*
                                 * Give the rogue a sword
                                 */
                                if(cur_weapon == nullptr || cur_weapon->o_type != RELIC) {
                                    item = spec_item(WEAPON, TWOSWORD, 5, 5);
                                    add_pack(item, TRUE);
                                    cur_weapon = OBJPTR(item);
                                    cur_weapon->o_flags |= (ISKNOW | ISPROT);
                                }
                                /*
                                 * And his suit of armor
                                 */
                                if(player.t_ctype == C_THIEF) {
                                    item = spec_item(ARMOR, STUDDED_LEATHER, 10, 0);
                                } else {
                                    item = spec_item(ARMOR, PLATE_ARMOR, 7, 0);
                                }
                                obj = OBJPTR(item);
                                obj->o_flags |= (ISKNOW | ISPROT);
                                obj->o_weight = armors[PLATE_ARMOR].a_wght;
                                cur_armor = obj;
                                add_pack(item, TRUE);
                                purse += 20000;
                            } break;
                            default:
                                msg("Illegal command '%s'.", unctrl_int(ch));
                                count = 0;
                        }
                    } else {
                        msg("Illegal command '%s'.", unctrl_int(ch));
                        count = 0;
                        after = FALSE;
                    }
            }
            /*
             * turn off flags if no longer needed
             */
            if(!running) {
                door_stop = FALSE;
            }
        }
        /*
         * If he ran into something to take, let him pick it up.
         * unless its a trading post
         */
        if(auto_pickup && take != 0 && levtype != POSTLEV) {
            pick_up(take);
        }
        if(!running) {
            door_stop = FALSE;
        }

        /* If after is true, mark an_after as true so that if
         * we are hasted, the first "after" will be noted.
         * if after is FALSE then stay in this loop
         */
        if(after) {
            an_after = TRUE;
        } else {
            ntimes++;
        }
    }

    /*
     * Kick off the rest if the daemons and fuses
     */
    if(an_after) {
        /*
         * If player is infested, take off a hit point
         */
        if(on(player, HASINFEST)) {
            if((pstats.s_hpt -= infest_dam) <= 0) {
                death(D_INFESTATION);
            }
        }
        /*
         * if player has body rot then take off five hits
         */
        if(on(player, DOROT)) {
            if((pstats.s_hpt -= 5) <= 0) {
                death(D_ROT);
            }
        }
        do_daemons(AFTER);
        do_fuses(AFTER);
        if(!((running || count) && jump)) {
            look(FALSE, FALSE);
        }
    }
    t_free_list(monst_dead);
}

/*
 * quit:
 *    Have player make certain, then exit.
 */

void quit(int sig) {
    /*
     * Reset the signal in case we got here via an interrupt
     */
    if(signal(SIGINT, quit) != quit) {
        mpos = 0;
    }
    msg("Really quit?");
    draw(cw);
    if(readchar() == 'y') {
        clear();
        move(LINES - 1, 0);
        draw(stdscr);
        score(pstats.s_exp + purse, CHICKEN, 0);
        endwin();
        exit(sig);
    } else {
        signal(SIGINT, quit);
        wmove(cw, 0, 0);
        wclrtoeol(cw);
        status(FALSE);
        draw(cw);
        mpos = 0;
        count = 0;
        running = FALSE;
    }
}

/*
 * bugkill:
 *    killed by a program bug instead of voluntarily.
 */

[[noreturn]] void bugkill(int sig) {
    signal(sig, quit); /* If we get it again, give up */
    death(D_SIGNAL);   /* Killed by a bug */
}

/*
 * search:
 *    Player gropes about him to find hidden things.
 */

void search(bool is_thief, bool door_chime) {
    int x, y;
    int ch,  /* The trap or door character */
        sch, /* Trap or door character (as seen on screen) */
        mch; /* Monster, if a monster is on the trap or door */
    linked_list *item;
    thing *mp; /* Status on surrounding monster */

    /*
     * Look all around the hero, if there is something hidden there,
     * give him a chance to find it.  If its found, display it.
     */
    if(on(player, ISBLIND)) {
        return;
    }
    for(x = hero.x - 1; x <= hero.x + 1; x++) {
        for(y = hero.y - 1; y <= hero.y + 1; y++) {
            if(y == hero.y && x == hero.x) {
                continue;
            }

            /* Mch and ch will be the same unless there is a monster here */
            mch = winat(y, x);
            ch = ssinch(y, x);
            sch = cwinch(y, x); /* What's on the screen */

            if(door_chime == FALSE && isatrap(ch)) {
                trap *tp;

                /* Is there a monster on the trap? */
                if(mch != ch && (item = find_mons(y, x)) != nullptr) {
                    mp = THINGPTR(item);
                    if(sch == mch) {
                        sch = mp->t_oldch;
                    }
                } else {
                    mp = nullptr;
                }

                /*
                 * is this one found already?
                 */
                if(isatrap(sch)) {
                    continue; /* give him chance for other traps */
                }
                tp = trap_at(y, x);
                /*
                 * if the thief set it then don't display it.
                 * if its not a thief he has 50/50 shot
                 */
                if((tp->tr_flags & ISTHIEFSET) || (!is_thief && rnd(100) > 50)) {
                    continue; /* give him chance for other traps */
                }
                tp->tr_flags |= ISFOUND;

                /* Let's update the screen */
                if(mp != nullptr && cwinch(y, x) == mch) {
                    mp->t_oldch = ch; /* Will change when monst moves */
                } else {
                    cwaddch(y, x, ch);
                }

                count = 0;
                running = FALSE;
                msg(tr_name(tp->tr_type));
            } else if(ch == SECRETDOOR) {
                if(door_chime == TRUE || (!is_thief && rnd(100) < 20)) {
                    /* Is there a monster on the door? */
                    if(mch != ch && (item = find_mons(y, x)) != nullptr) {
                        mp = THINGPTR(item);

                        /* Screen will change when monster moves */
                        if(sch == mch) {
                            mp->t_oldch = ch;
                        }
                    }
                    mvaddch(y, x, DOOR);
                    count = 0;
                }
            }
        }
    }
}

/*
 * help:
 *    Give single character help, or the whole mess if he wants it
 */

void help() {
    h_list *strp = helpstr;
    h_list *wizp = wiz_help;
    int helpch;
    int cnt;

    msg("Character you want help for (* for all): ");
    helpch = readchar();
    mpos = 0;
    /*
     * If its not a *, print the right help string
     * or an error if he typed a funny character.
     */
    if(helpch != '*') {
        wmove(cw, 0, 0);
        while(strp->h_ch) {
            if(strp->h_ch == helpch) {
                msg("%s%s", unctrl_int(strp->h_ch), strp->h_desc);
                return;
            }
            strp++;
        }
        if(wizard) {
            while(wizp->h_ch) {
                if(wizp->h_ch == helpch) {
                    msg("%s%s", unctrl_int(wizp->h_ch), wizp->h_desc);
                    return;
                }
                wizp++;
            }
        }

        msg("Unknown character '%s'", unctrl_int(helpch));
        return;
    }
    /*
     * Here we print help for everything.
     * Then wait before we return to command mode
     */
    wclear(hw);
    cnt = 0;
    while(strp->h_ch) {
        hwaddstr(cnt % 23, cnt > 22 ? 40 : 0, unctrl_int(strp->h_ch));
        waddstr(hw, strp->h_desc);
        strp++;
        if(++cnt >= 46 && strp->h_ch) {
            wmove(hw, LINES - 1, 0);
            wprintw(hw, morestr);
            draw(hw);
            wait_for(hw, ' ');
            wclear(hw);
            cnt = 0;
        }
    }
    if(wizard) {
        while(wizp->h_ch) {
            hwaddstr(cnt % 23, cnt > 22 ? 40 : 0, unctrl_int(wizp->h_ch));
            waddstr(hw, wizp->h_desc);
            wizp++;
            if(++cnt >= 46 && wizp->h_ch) {
                wmove(hw, LINES - 1, 0);
                wprintw(hw, morestr);
                draw(hw);
                wait_for(hw, ' ');
                wclear(hw);
                cnt = 0;
            }
        }
    }
    wmove(hw, LINES - 1, 0);
    wprintw(hw, spacemsg);
    draw(hw);
    wait_for(hw, ' ');
    wclear(hw);
    draw(hw);
    wmove(cw, 0, 0);
    wclrtoeol(cw);
    status(FALSE);
    touchwin(cw);
}
/*
 * identify:
 *    Tell the player what a certain thing is.
 */

void identify(int ch) {
    const char *str;

    if(ch == 0) {
        msg("What do you want identified? ");
        ch = readchar();
        mpos = 0;
        if(ch == ESCAPE) {
            msg("");
            return;
        }
    }
    if(isalpha(ch)) {
        str = monsters[id_monst(ch)].m_name;
    } else {
        switch(ch) {
            case '|':
            case '-':
                str = (levtype == OUTSIDE) ? "boundary of sector" : "wall of a room";
                break;
            case GOLD:
                str = "gold";
                break;
            case STAIRS:
                str = (levtype == OUTSIDE) ? "entrance to a dungeon" : "passage leading down";
                break;
            case DOOR:
                str = "door";
                break;
            case FLOOR:
                str = (levtype == OUTSIDE) ? "meadow" : "room floor";
                break;
            case VPLAYER:
                str = "the hero of the game ---> you";
                break;
            case IPLAYER:
                str = "you (but invisible)";
                break;
            case PASSAGE:
                str = "passage";
                break;
            case POST:
                str = "trading post";
                break;
            case POOL:
                str = (levtype == OUTSIDE) ? "lake" : "a shimmering pool";
                break;
            case TRAPDOOR:
                str = "trapdoor";
                break;
            case ARROWTRAP:
                str = "arrow trap";
                break;
            case SLEEPTRAP:
                str = "sleeping gas trap";
                break;
            case BEARTRAP:
                str = "bear trap";
                break;
            case TELTRAP:
                str = "teleport trap";
                break;
            case DARTTRAP:
                str = "dart trap";
                break;
            case MAZETRAP:
                str = "entrance to a maze";
                break;
            case FOREST:
                str = "forest";
                break;
            case POTION:
                str = "potion";
                break;
            case SCROLL:
                str = "scroll";
                break;
            case FOOD:
                str = "food";
                break;
            case WEAPON:
                str = "weapon";
                break;
            case ' ':
                str = "solid rock";
                break;
            case ARMOR:
                str = "armor";
                break;
            case MM:
                str = "miscellaneous magic";
                break;
            case RING:
                str = "ring";
                break;
            case STICK:
                str = "wand or staff";
                break;
            case SECRETDOOR:
                str = "secret door";
                break;
            case RELIC:
                str = "artifact";
                break;
            default:
                str = "unknown character";
        }
    }
    msg("'%s' : %s", unctrl_int(ch), str);
}

/*
 * d_level:
 *    He wants to go down a level
 */

void d_level() {
    bool no_phase = FALSE;

    /* If we are at a top-level trading post, we probably can't go down */
    if(levtype == POSTLEV && level == 0 && rnd(100) < 80) {
        msg("I see no way down.");
        return;
    }

    if(winat(hero.y, hero.x) != STAIRS) {
        if(off(player, CANINWALL) || /* Must use stairs if can't phase */
           (levtype == OUTSIDE && rnd(100) < 90)) {
            msg("I see no way down.");
            return;
        }

        /* Is there any dungeon left below? */
        if(level >= nfloors) {
            msg("There is only solid rock below.");
            return;
        }

        extinguish(d_unphase); /* Using phase to go down gets rid of it */
        no_phase = TRUE;
    }

    /* Is this the bottom? */
    if(level >= nfloors) {
        msg("The stairway only goes up.");
        return;
    }

    level++;
    new_level(NORMLEV);
    if(no_phase) {
        unphase();
    }
}

/*
 * u_level:
 *    He wants to go up a level
 */

void u_level() {
    bool no_phase = FALSE;
    linked_list *item;
    thing *tp;
    object *obj;

    if(winat(hero.y, hero.x) != STAIRS) {
        if(off(player, CANINWALL)) { /* Must use stairs if can't phase */
            msg("I see no way up.");
            return;
        }

        extinguish(d_unphase);
        no_phase = TRUE;
    }

    if(level == 0) {
        msg("The stairway only goes down.");
        return;
    }

    /*
     * does he have the item he was quested to get?
     */
    if(level == 1) {
        for(item = pack; item != nullptr; item = next(item)) {
            obj = OBJPTR(item);
            if(obj->o_type == RELIC && obj->o_which == quest_item) {
                total_winner();
            }
        }
    }
    /*
     * check to see if he trapped a UNIQUE, If he did then put it back
     * in the monster table for next time
     */
    for(item = tlist; item != nullptr; item = next(item)) {
        tp = THINGPTR(item);
        if(on(*tp, ISUNIQUE)) {
            monsters[tp->t_index].m_normal = TRUE;
        }
    }
    t_free_list(tlist); /* Monsters that fell below are long gone! */

    if(levtype != POSTLEV) {
        level--;
    }
    if(level > 0) {
        new_level(NORMLEV);
    } else {
        level = -1; /* Indicate that we are new to the outside */
        msg("You emerge into the %s", daytime ? "light" : "night");
        new_level(OUTSIDE); /* Leaving the dungeon */
    }

    if(no_phase) {
        unphase();
    }
}

/*
 * allow a user to call a potion, scroll, or ring something
 */
void call(bool mark) {
    object *obj;
    linked_list *item;
    char **guess = nullptr;
    const char *elsewise = nullptr;
    bool *know;

    if(mark) {
        item = get_item(pack, "mark", ALL);
    } else {
        item = get_item(pack, "call", CALLABLE);
    }
    /*
     * Make certain that it is somethings that we want to wear
     */
    if(item == nullptr) {
        return;
    }
    obj = OBJPTR(item);
    switch(obj->o_type) {
        case RING:
            guess = r_guess;
            know = r_know;
            elsewise =
                (r_guess[obj->o_which] != nullptr ? r_guess[obj->o_which] : r_stones[obj->o_which]);
            break;
        case POTION:
            guess = p_guess;
            know = p_know;
            elsewise =
                (p_guess[obj->o_which] != nullptr ? p_guess[obj->o_which] : p_colors[obj->o_which]);
            break;
        case SCROLL:
            guess = s_guess;
            know = s_know;
            elsewise =
                (s_guess[obj->o_which] != nullptr ? s_guess[obj->o_which] : s_names[obj->o_which]);
            break;
        case STICK:
            guess = ws_guess;
            know = ws_know;
            elsewise = (ws_guess[obj->o_which] != nullptr ? ws_guess[obj->o_which]
                                                          : ws_made[obj->o_which]);
            break;
        case MM:
            guess = m_guess;
            know = m_know;
            elsewise = (m_guess[obj->o_which] != nullptr ? m_guess[obj->o_which] : "nothing");
            break;
        default:
            if(!mark) {
                msg("You can't call that anything.");
                return;
            } else {
                know = nullptr;
            }
    }
    if((obj->o_flags & ISPOST) || ((know && know[obj->o_which]) && !mark)) {
        msg("That has already been identified.");
        return;
    }
    if(mark) {
        if(obj->o_mark[0]) {
            addmsg(terse ? "M" : "Was m");
            msg("arked \"%s\"", obj->o_mark);
        }
        msg(terse ? "Mark it: " : "What do you want to mark it? ");
        prbuf[0] = '\0';
    } else {
        addmsg(terse ? "C" : "Was c");
        msg("alled \"%s\"", elsewise);
        msg(terse ? "Call it: " : "What do you want to call it? ");
        if(guess[obj->o_which] != nullptr) {
            free(guess[obj->o_which]);
        }
        strcpy(prbuf, elsewise);
    }
    if(get_str(prbuf, cw) == NORM) {
        if(mark) {
            strncpy(obj->o_mark, prbuf, MARKLEN - 1);
            obj->o_mark[MARKLEN - 1] = '\0';
        } else {
            guess[obj->o_which] = newalloc(strlen(prbuf) + 1);
            strcpy(guess[obj->o_which], prbuf);
        }
    }
}
