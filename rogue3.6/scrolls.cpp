
/*
 * Read a scroll and let it happen
 *
 * @(#)scrolls.c     3.5 (Berkeley) 6/15/81
 *
 * Rogue: Exploring the Dungeons of Doom
 * Copyright (C) 1980, 1981 Michael Toy, Ken Arnold and Glenn Wichman
 * All rights reserved.
 *
 * See the file LICENSE.TXT for full copyright and licensing information.
 */

#include "rogue.h"

void read_scroll() {
    linked_list *item = get_item("read", SCROLL);
    if(item == nullptr) {
        return;
    }

    auto *obj = lobject(item);
    if(obj->o_type != SCROLL) {
        if(!game.terse) {
            msg("There is nothing on it to read");
        } else {
            msg("Nothing to read");
        }
        return;
    }
    msg("As you read the scroll, it vanishes.");

    /* Calculate the effect it has on the poor guy. */
    if(obj == rogue.cur_weapon) {
        rogue.cur_weapon = nullptr;
    }
    switch(obj->o_which) {
        case S_CONFUSE:
            /* Scroll of monster confusion. Give him that power. */
            msg("Your hands begin to glow red");
            rogue.player.t_flags |= CANHUH;
            break;
        case S_LIGHT:
            items.s_know[S_LIGHT] = TRUE;
            room *rp;
            if((rp = roomin(rogue.hero())) == nullptr) {
                msg("The corridor glows and then fades");
            } else {
                addmsg("The room is lit");
                if(!game.terse) {
                    addmsg(" by a shimmering blue light.");
                }
                endmsg();
                rp->r_flags &= ~ISDARK;
                /* Light the room and put the player back up */
                light(rogue.hero());
                mvwaddch(ui.cw.w, rogue.hero().y, rogue.hero().x, PLAYER);
            }
            break;
        case S_ARMOR:
            if(rogue.cur_armor != nullptr) {
                msg("Your armor glows faintly for a moment");
                rogue.cur_armor->o_ac--;
                rogue.cur_armor->o_flags &= ~ISCURSED;
            }
            break;
        case S_HOLD:
            /*
             * Hold monster scroll.  Stop all monsters within two spaces
             * from chasing after the hero.
             */
            {
                int x, y;
                linked_list *mon;

                for(x = rogue.hero().x - 2; x <= rogue.hero().x + 2; x++) {
                    for(y = rogue.hero().y - 2; y <= rogue.hero().y + 2; y++) {
                        if(y > 0 && x > 0 && isupper((int)mvwinch(ui.mw.w, y, x))) {
                            if((mon = find_mons(y, x)) != nullptr) {
                                thing *th;

                                th = lthing(mon);
                                th->t_flags &= ~ISRUN;
                                th->t_flags |= ISHELD;
                            }
                        }
                    }
                }
            }
            break;
        case S_SLEEP:
            /* Scroll which makes you fall asleep */
            items.s_know[S_SLEEP] = TRUE;
            msg("You fall asleep.");
            rogue.no_command += 4 + rnd(Game::SLEEPTIME);
            break;
        case S_CREATE:
            /*
             * Create a monster
             * First look in a circle around him, next try his room
             * otherwise give up
             */
            {
                int x, y;
                int appear = 0;
                coord mp;

                /* Search for an open place */
                for(y = rogue.hero().y - 1; y <= rogue.hero().y + 1; y++) {
                    for(x = rogue.hero().x - 1; x <= rogue.hero().x + 1; x++) {
                        /* Don't put a monster in top of the player. */
                        if(y == rogue.hero().y && x == rogue.hero().x) {
                            continue;
                        }
                        /* Or anything else nasty */
                        if(step_ok((int)ui.winat(y, x))) {
                            if(rnd(++appear) == 0) {
                                mp.y = y;
                                mp.x = x;
                            }
                        }
                    }
                }
                if(appear) {
                    linked_list *titem = new_item(sizeof(thing));
                    new_monster(titem, randmonster(FALSE), mp);
                } else {
                    msg("You hear a faint cry of anguish in the distance.");
                }
            }
            break;
        case S_IDENT:
            /* Identify, let the rogue figure something out */
            msg("This scroll is an identify scroll");
            items.s_know[S_IDENT] = TRUE;
            whatis();
            break;
        case S_MAP:
            /* Scroll of magic mapping. */
            items.s_know[S_MAP] = TRUE;
            msg("Oh, now this scroll has a map on it.");
            overwrite(ui.stdScr.w, ui.hw.w);
            /* Take all the things we want to keep hidden out of the window */
            for(int i = 0; i < LINES; i++) {
                for(int j = 0; j < COLS; j++) {
                    int ch, nch;
                    switch(nch = ch = (int)mvwinch(ui.hw.w, i, j)) {
                        case SECRETDOOR:
                            nch = DOOR;
                            mvaddch_int(i, j, nch);
                            [[clang::fallthrough]];
                        case '-':
                        case '|':
                        case DOOR:
                        case PASSAGE:
                        case ' ':
                        case STAIRS:
                            if(mvwinch(ui.mw.w, i, j) != ' ') {
                                thing *it;

                                it = lthing(find_mons(i, j));
                                if((it != nullptr) && (it->t_oldch == ' ')) {
                                    it->t_oldch = nch;
                                }
                            }
                            break;
                        default:
                            nch = ' ';
                    }
                    if(nch != ch) {
                        waddch_int(ui.hw.w, nch);
                    }
                }
            }
            /* Copy in what he has discovered */
            overlay(ui.cw.w, ui.hw.w);
            /* And set up for display */
            overwrite(ui.hw.w, ui.cw.w);
            break;
        case S_GFIND:
            /* Potion of gold detection */
            {
                int gtotal = 0;

                ui.hw.clearWindow();
                for(auto &room : floorMap.rooms) {
                    gtotal += room.r_goldval;
                    if(room.r_goldval != 0 &&
                       mvwinch(ui.stdScr.w, room.r_gold.y, room.r_gold.x) == GOLD) {
                        mvwaddch(ui.hw.w, room.r_gold.y, room.r_gold.x, GOLD);
                    }
                }
                if(gtotal) {
                    items.s_know[S_GFIND] = TRUE;
                    show_win(ui.hw.w, "You begin to feel greedy and you sense gold.--More--");
                } else {
                    msg("You begin to feel a pull downward");
                }
            }
            break;
        case S_TELEP:
            /*
             * Scroll of teleportation:
             * Make him dissapear and reappear
             */
            {
                int rm;
                room *cur_room;

                cur_room = roomin(rogue.hero());
                rm = teleport();
                if(cur_room != &floorMap.rooms[rm]) {
                    items.s_know[S_TELEP] = TRUE;
                }
            }
            break;
        case S_ENCH:
            if(rogue.cur_weapon == nullptr) {
                msg("You feel a strange sense of loss.");
            } else {
                rogue.cur_weapon->o_flags &= ~ISCURSED;
                if(rnd(100) > 50) {
                    rogue.cur_weapon->o_hplus++;
                } else {
                    rogue.cur_weapon->o_dplus++;
                }
                msg("Your %s glows blue for a moment.",
                    Items::w_names[(size_t)rogue.cur_weapon->o_which].c_str());
            }
            break;
        case S_SCARE:
            /*
             * A monster will refuse to step on a scare monster scroll
             * if it is dropped.  Thus reading it is a mistake and produces
             * laughter at the poor rogue's boo boo.
             */
            msg("You hear maniacal laughter in the distance.");
            break;
        case S_REMOVE:
            if(rogue.cur_armor != nullptr) {
                rogue.cur_armor->o_flags &= ~ISCURSED;
            }
            if(rogue.cur_weapon != nullptr) {
                rogue.cur_weapon->o_flags &= ~ISCURSED;
            }
            if(rogue.cur_ring[Rogue::LEFT] != nullptr) {
                rogue.cur_ring[Rogue::LEFT]->o_flags &= ~ISCURSED;
            }
            if(rogue.cur_ring[Rogue::RIGHT] != nullptr) {
                rogue.cur_ring[Rogue::RIGHT]->o_flags &= ~ISCURSED;
            }
            msg("You feel as if somebody is watching over you.");
            break;
        case S_AGGR:
            /*
             * This scroll aggravates all the monsters on the current
             * level and sets them running towards the hero
             */
            aggravate();
            msg("You hear a high pitched humming noise.");
            break;
        case S_NOP:
            msg("This scroll seems to be blank.");
            break;
        case S_GENOCIDE:
            msg("You have been granted the boon of genocide");
            genocide();
            items.s_know[S_GENOCIDE] = TRUE;
            break;
        default:
            msg("What a puzzling scroll!");
            return;
    }
    look(TRUE); /* put the result of the scroll on the screen */
    status();
    if(items.s_know[(size_t)obj->o_which] && !items.s_guess[(size_t)obj->o_which].empty()) {
        items.s_guess[(size_t)obj->o_which].clear();
    } else if(!items.s_know[(size_t)obj->o_which] && game.askme &&
              items.s_guess[(size_t)obj->o_which].empty()) {
        msg(game.terse ? "Call it: " : "What do you want to call it? ");
        char buf[80];
        if(get_str(buf, ui.cw.w) == UI::NORM) {
            items.s_guess[(size_t)obj->o_which] = buf;
        }
    }
    /* Get rid of the thing */
    rogue.inpack--;
    if(obj->o_count > 1) {
        obj->o_count--;
    } else {
        detach(rogue.pack(), item);
        discard(item);
    }
}
