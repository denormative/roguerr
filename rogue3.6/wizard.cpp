
/*
 * Special wizard commands (some of which are also non-wizard commands
 * under strange circumstances)
 *
 * @(#)wizard.c     3.8 (Berkeley) 6/3/81
 *
 * Rogue: Exploring the Dungeons of Doom
 * Copyright (C) 1980, 1981 Michael Toy, Ken Arnold and Glenn Wichman
 * All rights reserved.
 *
 * See the file LICENSE.TXT for full copyright and licensing information.
 */

#include "rogue.h"

/*
 * whatis:
 *     What a certin object is
 */

void whatis() {
    linked_list *item;

    if((item = get_item("identify", 0)) == nullptr) {
        return;
    }
    auto *obj = lobject(item);
    switch(obj->o_type) {
        case SCROLL:
            items.s_know[(size_t)obj->o_which] = TRUE;
            if(!items.s_guess[(size_t)obj->o_which].empty()) {
                items.s_guess[(size_t)obj->o_which].clear();
            }
            break;
        case POTION:
            items.p_know[(size_t)obj->o_which] = TRUE;
            if(!items.p_guess[(size_t)obj->o_which].empty()) {
                items.p_guess[(size_t)obj->o_which].clear();
            }
            break;
        case STICK:
            items.ws_know[(size_t)obj->o_which] = TRUE;
            obj->o_flags |= ISKNOW;
            if(!items.ws_guess[(size_t)obj->o_which].empty()) {
                items.ws_guess[(size_t)obj->o_which].clear();
            }
            break;
        case WEAPON:
        case ARMOR:
            obj->o_flags |= ISKNOW;
            break;
        case RING:
            items.r_know[(size_t)obj->o_which] = TRUE;
            obj->o_flags |= ISKNOW;
            if(!items.r_guess[(size_t)obj->o_which].empty()) {
                items.r_guess[(size_t)obj->o_which].clear();
            }
    }
    msg(inv_name(obj, FALSE));
}

/*
 * create_obj:
 *     Wizard command for getting anything he wants
 */

void create_obj() {
    object *obj;
    linked_list *item = new_item(sizeof *obj);
    obj = lobject(item);
    msg("Type of item: ");
    obj->o_type = readchar(ui.cw.w);
    ui.mpos = 0;
    msg("Which %c do you want? (0-f)", obj->o_type);
    int ch;
    obj->o_which = (isdigit((ch = readchar(ui.cw.w))) ? ch - '0' : ch - 'a' + 10);
    obj->o_group = 0;
    obj->o_count = 1;
    ui.mpos = 0;
    if(obj->o_type == WEAPON || obj->o_type == ARMOR) {
        msg("Blessing? (+,-,n)");
        int bless;
        bless = readchar(ui.cw.w);
        ui.mpos = 0;
        if(obj->o_type == WEAPON) {
            init_weapon(obj, obj->o_which);
            if(bless == '-') {
                obj->o_hplus -= rnd(3) + 1;
                obj->o_flags |= ISCURSED;
            }
            if(bless == '+') {
                obj->o_hplus += rnd(3) + 1;
            }
        } else {
            obj->o_ac = Items::a_class[(size_t)obj->o_which];
            if(bless == '-') {
                obj->o_ac += rnd(3) + 1;
                obj->o_flags |= ISCURSED;
            }
            if(bless == '+') {
                obj->o_ac -= rnd(3) + 1;
            }
        }
    } else if(obj->o_type == RING) {
        switch(obj->o_which) {
            case R_PROTECT:
            case R_ADDSTR:
            case R_ADDHIT:
            case R_ADDDAM:
                msg("Blessing? (+,-,n)");
                int bless = readchar(ui.cw.w);
                ui.mpos = 0;
                if(bless == '-') {
                    obj->o_flags |= ISCURSED;
                }
                obj->o_ac = (bless == '-' ? -1 : rnd(2) + 1);
        }
    } else if(obj->o_type == STICK) {
        fix_stick(obj);
    }
    add_pack(item, FALSE);
}

/*
 * telport:
 *     Bamf the hero someplace else
 */

int teleport() {
    int rm;

    coord c = rogue.hero();
    mvwaddch(ui.cw.w, rogue.hero().y, rogue.hero().x,
             mvwinch(ui.stdScr.w, rogue.hero().y, rogue.hero().x));
    do {
        rm = rnd_room();
        rnd_pos(&floorMap.rooms[rm], rogue.hero());
    } while(ui.winat(rogue.hero().y, rogue.hero().x) != FLOOR);
    light(c);
    light(rogue.hero());
    mvwaddch(ui.cw.w, rogue.hero().y, rogue.hero().x, PLAYER);
    /*
     * turn off ISHELD in case teleportation was done while fighting
     * a Fungi
     */
    if(on(rogue.player, ISHELD)) {
        rogue.player.t_flags &= ~ISHELD;
        game.fung_hit = 0;
        strcpy(Game::monsters['F' - 'A'].m_stats.s_dmg, "000d0");
    }
    ui.count = 0;
    game.running = FALSE;
    flush_type(); /* flush typeahead */
    return rm;
}

/*
 * passwd:
 *     see if user knows password
 */

int passwd() {
    msg("Wizard's Password:");
    ui.mpos = 0;
    char buf[80];
    char *sp = buf;
    char c;
    while((c = (char)readchar(ui.cw.w)) != '\n' && c != '\r' && c != '\033') {
        if(c == killchar()) {
            sp = buf;
        } else if(c == erasechar() && sp > buf) {
            sp--;
        } else {
            *sp++ = c;
        }
    }
    if(sp == buf) {
        return FALSE;
    }
    *sp = '\0';
    return (strcmp(Game::PASSWD, buf) == 0);
}
