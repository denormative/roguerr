/*
    state.c - Portable Rogue Save State Code

    Copyright (C) 1999, 2000, 2005, 2007, 2008 Nicholas J. Kisseberth
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions
    are met:
    1. Redistributions of source code must retain the above copyright
       notice, this list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright
       notice, this list of conditions and the following disclaimer in the
       documentation and/or other materials provided with the distribution.
    3. Neither the name(s) of the author(s) nor the names of other contributors
       may be used to endorse or promote products derived from this software
       without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE AUTHOR(S) AND CONTRIBUTORS ``AS IS'' AND
    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR(S) OR CONTRIBUTORS BE LIABLE
    FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
    DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
    OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
    HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
    LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
    OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
    SUCH DAMAGE.
*/

#include "rogue.h"

/************************************************************************/
/* Save State Code                                                      */
/************************************************************************/

#define RSID_STATS 0xABCD0001
#define RSID_THING 0xABCD0002
#define RSID_THING_NULL 0xDEAD0002
#define RSID_OBJECT 0xABCD0003
#define RSID_MAGICITEMS 0xABCD0004
#define RSID_KNOWS 0xABCD0005
#define RSID_GUESSES 0xABCD0006
#define RSID_OBJECTLIST 0xABCD0007
#define RSID_BAGOBJECT 0xABCD0008
#define RSID_MONSTERLIST 0xABCD0009
#define RSID_MONSTERSTATS 0xABCD000A
#define RSID_MONSTERS 0xABCD000B
#define RSID_TRAP 0xABCD000C
#define RSID_WINDOW 0xABCD000D
#define RSID_DAEMONS 0xABCD000E
#define RSID_IWEAPS 0xABCD000F
#define RSID_IARMOR 0xABCD0010
#define RSID_SPELLS 0xABCD0011
#define RSID_ILIST 0xABCD0012
#define RSID_HLIST 0xABCD0013
#define RSID_DEATHTYPE 0xABCD0014
#define RSID_CTYPES 0XABCD0015
#define RSID_COORDLIST 0XABCD0016
#define RSID_ROOMS 0XABCD0017

static int endian = 0x01020304;
#define big_endian (*((char *)&endian) == 0x01)

void rs_write(FILE *savef, const void *ptr, size_t size) {
    encwrite(ptr, size, savef);
}

void rs_read(FILE *savef, void *ptr, size_t size) {
    encread(ptr, size, savef);
}

void rs_read(FILE *savef, string &ptr, size_t size) {
    ptr.resize(size, ' ');
    encread((void *)ptr.c_str(), size, savef);
}

void rs_write_int(FILE *savef, int c) {
    char bytes[4];
    char *buf = (char *)&c;

    if(big_endian) {
        bytes[3] = buf[0];
        bytes[2] = buf[1];
        bytes[1] = buf[2];
        bytes[0] = buf[3];
        buf = bytes;
    }

    rs_write(savef, buf, 4);
}

void rs_read_int(FILE *savef, int *i) {
    char bytes[4];
    int input = 0;
    char *buf = (char *)&input;

    rs_read(savef, &input, 4);

    if(encerror()) {
        return;
    }

    if(big_endian) {
        bytes[3] = buf[0];
        bytes[2] = buf[1];
        bytes[1] = buf[2];
        bytes[0] = buf[3];
        buf = bytes;
    }

    *i = *((int *)buf);
}

void rs_write_chars(FILE *savef, const char *c, int cnt) {
    rs_write_int(savef, cnt);
    rs_write(savef, c, (size_t)cnt);
}

void rs_read_chars(FILE *savef, char *i, int cnt) {
    int value = 0;

    rs_read_int(savef, &value);

    if(!encerror() && (value != cnt)) {
        encseterr(EILSEQ);
    }

    rs_read(savef, i, (size_t)cnt);
}

void rs_read_chars(FILE *savef, string &i, const int cnt) {
    int value = 0;

    rs_read_int(savef, &value);

    if(!encerror() && (value != cnt)) {
        encseterr(EILSEQ);
    }

    rs_read(savef, i, (size_t)cnt);
}

void rs_write_marker(FILE *savef, unsigned int id) {
    rs_write_int(savef, (int)id);
}

void rs_read_marker(FILE *savef, unsigned int id) {
    int nid;

    rs_read_int(savef, &nid);

    if(!encerror() && (id != (unsigned int)nid)) {
        encseterr(EILSEQ);
    }
}

/******************************************************************************/

template <size_t T>
void rs_write_string_index(FILE *savef, array<char *, T> &master, const char *str) {
    for(size_t i = 0; i < master.size(); i++) {
        if(str == master[i]) {
            rs_write_int(savef, (int)i);
            return;
        }
    }

    rs_write_int(savef, -1);
}

template <size_t T>
void rs_write_string_index(FILE *savef, array<string, T> &master, const string &str) {
    for(size_t i = 0; i < master.size(); i++) {
        if(str == master[i]) {
            rs_write_int(savef, (int)i);
            return;
        }
    }

    rs_write_int(savef, -1);
}

template <size_t T>
void rs_read_string_index(FILE *savef, array<string, T> &master, string &str) {
    int i;

    rs_read_int(savef, &i);

    if(!encerror() && (i > (int)master.size())) {
        encseterr(EILSEQ);
    } else if(i >= 0) {
        str = master[(size_t)i];
    } else {
        str.clear();
    }
}

void rs_write_coord(FILE *savef, coord c) {
    rs_write_int(savef, c.x);
    rs_write_int(savef, c.y);
}

void rs_read_coord(FILE *savef, coord &c) {
    coord in;

    rs_read_int(savef, &in.x);
    rs_read_int(savef, &in.y);

    if(!encerror()) {
        c.x = in.x;
        c.y = in.y;
    }
}

void rs_write_str_t(FILE *savef, str_t str) {
    rs_write_int(savef, str.st_str);
    rs_write_int(savef, str.st_add);
}

void rs_read_str_t(FILE *savef, str_t *str) {
    str_t in;

    rs_read_int(savef, &in.st_str);
    rs_read_int(savef, &in.st_add);

    if(!encerror()) {
        str->st_str = in.st_str;
        str->st_add = in.st_add;
    }
}

/******************************************************************************/

void *get_list_item(linked_list *l, int i) {
    int cnt;

    for(cnt = 0; l != nullptr; cnt++, l = l->l_next) {
        if(cnt == i) {
            return (l->l_data);
        }
    }

    return (nullptr);
}

int find_list_ptr(linked_list *l, void *ptr) {
    int cnt;

    for(cnt = 0; l != nullptr; cnt++, l = l->l_next) {
        if(l->l_data == ptr) {
            return (cnt);
        }
    }

    return (-1);
}

int list_size(linked_list *l) {
    int cnt;

    for(cnt = 0; l != nullptr; cnt++, l = l->l_next) {
        if(l->l_data == nullptr) {
            return (cnt);
        }
    }

    return (cnt);
}

/******************************************************************************/

void rs_write_stats(FILE *savef, stats *s) {
    rs_write_marker(savef, RSID_STATS);
    rs_write_str_t(savef, s->s_str);
    rs_write_int(savef, s->s_exp);
    rs_write_int(savef, s->s_lvl);
    rs_write_int(savef, s->s_arm);
    rs_write_int(savef, s->s_hpt);
    rs_write_chars(savef, s->s_dmg, sizeof(s->s_dmg));
}

void rs_read_stats(FILE *savef, stats *s) {
    rs_read_marker(savef, RSID_STATS);
    rs_read_str_t(savef, &s->s_str);
    rs_read_int(savef, &s->s_exp);
    rs_read_int(savef, &s->s_lvl);
    rs_read_int(savef, &s->s_arm);
    rs_read_int(savef, &s->s_hpt);
    rs_read_chars(savef, s->s_dmg, sizeof(s->s_dmg));
}

void rs_write_daemons(FILE *savef) {
    rs_write_marker(savef, RSID_DAEMONS);
    rs_write_int(savef, (int)daemons.actions.size());

    for(const auto &a : daemons.actions) {
        // FIXME: Don't think this is needed but not sure...
        if(a.d_id >= d_dont_save) {
            exit(42);
        }
        rs_write_int(savef, (int)a.d_id);
        rs_write_int(savef, a.d_arg);
        rs_write_int(savef, a.d_time);
        rs_write_int(savef, a.d_type);
    }
}

void rs_read_daemons(FILE *savef) {
    int cnt = 0;

    rs_read_marker(savef, RSID_DAEMONS);
    rs_read_int(savef, &cnt);

    if(encerror()) {
        encseterr(EILSEQ);
        return;
    }

    for(int i = 0; i < cnt; i++) {
        int l_type, l_id, l_arg, l_time;
        rs_read_int(savef, &l_id);
        rs_read_int(savef, &l_arg);
        rs_read_int(savef, &l_time);
        rs_read_int(savef, &l_type);
        // FIXME: Bit of a hack using startFuse; probably should have a proper load/save
        daemons.startFuse((daemon_id)l_id, l_arg, l_time, l_type);

        if(encerror()) {
            return;
        }
    }
}

void rs_write_room(FILE *savef, room *r) {
    rs_write_coord(savef, r->r_pos);
    rs_write_coord(savef, r->r_max);
    rs_write_coord(savef, r->r_gold);
    rs_write_int(savef, r->r_goldval);
    rs_write_int(savef, r->r_flags);
    rs_write_int(savef, (int)r->r_exit.size());
    for(auto &exit : r->r_exit) {
        rs_write_coord(savef, exit);
    }
}

void rs_read_room(FILE *savef, room *r) {
    rs_read_coord(savef, r->r_pos);
    rs_read_coord(savef, r->r_max);
    rs_read_coord(savef, r->r_gold);
    rs_read_int(savef, &r->r_goldval);
    rs_read_int(savef, &r->r_flags);
    int nexits;
    rs_read_int(savef, &nexits);
    for(int i = 0; i < nexits; i++) {
        coord exit;
        rs_read_coord(savef, exit);
        r->r_exit.push_back(exit);
    }
}

void rs_write_rooms(FILE *savef, room r[], int cnt) {
    int n = 0;

    rs_write_int(savef, cnt);

    for(n = 0; n < cnt; n++) {
        rs_write_room(savef, &r[n]);
    }
}

void rs_read_rooms(FILE *savef, room *r, int cnt) {
    int value = 0, n = 0;

    rs_read_int(savef, &value);

    if(!encerror() && (value > cnt)) {
        encseterr(EILSEQ);
    } else {
        for(n = 0; n < value; n++) {
            rs_read_room(savef, &r[n]);
        }
    }
}

void rs_write_room_reference(FILE *savef, room *rp) {
    int i, room = -1;

    for(i = 0; i < Floor::MAXROOMS; i++) {
        if(&floorMap.rooms[i] == rp) {
            room = i;
        }
    }

    rs_write_int(savef, room);
}

void rs_read_room_reference(FILE *savef, room **rp) {
    int i;

    rs_read_int(savef, &i);

    if(!encerror()) {
        *rp = &floorMap.rooms[i];
    }
}

void rs_write_object(FILE *savef, object *o) {
    rs_write_marker(savef, RSID_OBJECT);
    rs_write_int(savef, o->o_type);
    rs_write_coord(savef, o->o_pos);
    rs_write_int(savef, o->o_launch);
    rs_write_chars(savef, o->o_damage, sizeof(o->o_damage));
    rs_write_chars(savef, o->o_hurldmg, sizeof(o->o_hurldmg));
    rs_write_int(savef, o->o_count);
    rs_write_int(savef, o->o_which);
    rs_write_int(savef, o->o_hplus);
    rs_write_int(savef, o->o_dplus);
    rs_write_int(savef, o->o_ac);
    rs_write_int(savef, o->o_flags);
    rs_write_int(savef, o->o_group);
}

void rs_read_object(FILE *savef, object *o) {
    rs_read_marker(savef, RSID_OBJECT);
    rs_read_int(savef, &o->o_type);
    rs_read_coord(savef, o->o_pos);
    rs_read_int(savef, &o->o_launch);
    rs_read_chars(savef, o->o_damage, sizeof(o->o_damage));
    rs_read_chars(savef, o->o_hurldmg, sizeof(o->o_hurldmg));
    rs_read_int(savef, &o->o_count);
    rs_read_int(savef, &o->o_which);
    rs_read_int(savef, &o->o_hplus);
    rs_read_int(savef, &o->o_dplus);
    rs_read_int(savef, &o->o_ac);
    rs_read_int(savef, &o->o_flags);
    rs_read_int(savef, &o->o_group);
}

void rs_write_object_list(FILE *savef, linked_list *l) {
    rs_write_marker(savef, RSID_OBJECTLIST);
    rs_write_int(savef, list_size(l));

    for(; l != nullptr; l = l->l_next) {
        rs_write_object(savef, (object *)l->l_data);
    }
}

void rs_read_object_list(FILE *savef, linked_list **list) {
    int i, cnt;
    linked_list *l = nullptr, *previous = nullptr, *head = nullptr;

    rs_read_marker(savef, RSID_OBJECTLIST);
    rs_read_int(savef, &cnt);

    if(encerror()) {
        return;
    }

    for(i = 0; i < cnt; i++) {
        l = new_item(sizeof(object));

        memset(l->l_data, 0, sizeof(object));

        l->l_prev = previous;

        if(previous != nullptr) {
            previous->l_next = l;
        }

        rs_read_object(savef, (object *)l->l_data);

        if(previous == nullptr) {
            head = l;
        }

        previous = l;
    }

    if(l != nullptr) {
        l->l_next = nullptr;
    }

    *list = head;
}

void rs_write_object_reference(FILE *savef, linked_list *list, object *item) {
    int i;

    i = find_list_ptr(list, item);

    rs_write_int(savef, i);
}

void rs_read_object_reference(FILE *savef, linked_list *list, object **item) {
    int i;

    rs_read_int(savef, &i);

    if(!encerror()) {
        *item = (object *)get_list_item(list, i);
    } else {
        *item = nullptr;
    }
}

int find_room_coord(const room *rmlist, const coord &c, int n) {
    int i = 0;

    for(i = 0; i < n; i++) {
        if(rmlist[i].r_gold == c) {
            return (i);
        }
    }

    return (-1);
}

int find_thing_coord(linked_list *monlist, const coord &c) {
    linked_list *mitem;
    thing *tp;
    int i = 0;

    for(mitem = monlist; mitem != nullptr; mitem = mitem->l_next) {
        tp = THINGPTR(mitem);

        if(c == tp->t_pos) {
            return (i);
        }

        i++;
    }

    return (-1);
}

int find_object_coord(linked_list *objlist, const coord &c) {
    linked_list *oitem;
    object *obj;
    int i = 0;

    for(oitem = objlist; oitem != nullptr; oitem = oitem->l_next) {
        obj = OBJPTR(oitem);

        if(c == obj->o_pos) {
            return (i);
        }

        i++;
    }

    return (-1);
}

void rs_write_thing(FILE *savef, thing *t) {
    int i = -1;

    rs_write_marker(savef, RSID_THING);

    if(t == nullptr) {
        rs_write_int(savef, 0);
        return;
    }

    rs_write_int(savef, 1);
    rs_write_coord(savef, t->t_pos);
    rs_write_int(savef, t->t_turn);
    rs_write_int(savef, t->t_type);
    rs_write_int(savef, t->t_disguise);
    rs_write_int(savef, t->t_oldch);

    /*
        t_dest can be:
        0,0: NULL
        0,1: location of hero
        1,i: location of a thing (monster)
        2,i: location of an object
        3,i: location of gold in a room

        We need to remember what we are chasing rather than
        the current location of what we are chasing.
    */

    if(t->t_dest == hero_pos) {
        rs_write_int(savef, 0);
        rs_write_int(savef, 1);
    } else if(t->t_dest != coord{}) {
        i = find_thing_coord(floorMap.mlist, t->t_dest);

        if(i >= 0) {
            rs_write_int(savef, 1);
            rs_write_int(savef, i);
        } else {
            i = find_object_coord(floorMap.lvl_obj, t->t_dest);

            if(i >= 0) {
                rs_write_int(savef, 2);
                rs_write_int(savef, i);
            } else {
                i = find_room_coord(floorMap.rooms, t->t_dest, Floor::MAXROOMS);

                if(i >= 0) {
                    rs_write_int(savef, 3);
                    rs_write_int(savef, i);
                } else {
                    rs_write_int(savef, 0);
                    rs_write_int(savef, 1); /* chase the hero anyway */
                }
            }
        }
    } else {
        rs_write_int(savef, 0);
        rs_write_int(savef, 0);
    }

    rs_write_int(savef, t->t_flags);
    rs_write_stats(savef, &t->t_stats);
    rs_write_object_list(savef, t->t_pack);
}

void rs_read_thing(FILE *savef, thing *t) {
    int listid = 0, index = -1;
    linked_list *item;

    rs_read_marker(savef, RSID_THING);
    rs_read_int(savef, &index);

    if(encerror()) {
        return;
    }

    if(index == 0) {
        return;
    }

    rs_read_coord(savef, t->t_pos);
    rs_read_int(savef, &t->t_turn);
    rs_read_int(savef, &t->t_type);
    rs_read_int(savef, &t->t_disguise);
    rs_read_int(savef, &t->t_oldch);

    /*
        t_dest can be (listid,index):
        0,0: NULL
        0,1: location of hero
        1,i: location of a thing (monster)
        2,i: location of an object
        3,i: location of gold in a room

        We need to remember what we are chasing rather than
        the current location of what we are chasing.
    */

    rs_read_int(savef, &listid);
    rs_read_int(savef, &index);
    t->t_reserved = -1;

    if(encerror()) {
        return;
    }

    if(listid == 0) { /* hero or NULL */
        if(index == 1) {
            t->t_dest = hero_pos;
        } else {
            t->t_dest = coord{};
        }
    } else if(listid == 1) { /* monster/thing */
        t->t_dest = coord{};
        t->t_reserved = index;
    } else if(listid == 2) { /* object */
        object *obj;

        item = (linked_list *)get_list_item(floorMap.lvl_obj, index);

        if(item != nullptr) {
            obj = OBJPTR(item);
            t->t_dest = obj->o_pos;
        }
    } else if(listid == 3) { /* gold */
        t->t_dest = floorMap.rooms[index].r_gold;
    } else {
        t->t_dest = coord{};
    }

    rs_read_int(savef, &t->t_flags);
    rs_read_stats(savef, &t->t_stats);
    rs_read_object_list(savef, &t->t_pack);
}

void rs_fix_thing(thing *t) {
    // FIXME: Think we can completely remove this function
    linked_list *item;
    thing *tp;

    if(t->t_reserved < 0) {
        return;
    }

    item = (linked_list *)get_list_item(floorMap.mlist, t->t_reserved);

    if(item != nullptr) {
        tp = THINGPTR(item);
        t->t_dest = tp->t_pos;
    }
}

void rs_write_thing_list(FILE *savef, linked_list *l) {
    int cnt = 0;

    rs_write_marker(savef, RSID_MONSTERLIST);

    cnt = list_size(l);

    rs_write_int(savef, cnt);

    if(cnt < 1) {
        return;
    }

    while(l != nullptr) {
        rs_write_thing(savef, (thing *)l->l_data);
        l = l->l_next;
    }
}

void rs_read_thing_list(FILE *savef, linked_list **list) {
    int i, cnt;
    linked_list *l = nullptr, *previous = nullptr, *head = nullptr;

    rs_read_marker(savef, RSID_MONSTERLIST);
    rs_read_int(savef, &cnt);

    if(encerror()) {
        return;
    }

    for(i = 0; i < cnt; i++) {
        l = new_item(sizeof(thing));

        l->l_prev = previous;

        if(previous != nullptr) {
            previous->l_next = l;
        }

        rs_read_thing(savef, (thing *)l->l_data);

        if(previous == nullptr) {
            head = l;
        }

        previous = l;
    }

    if(l != nullptr) {
        l->l_next = nullptr;
    }

    *list = head;
}

void rs_fix_thing_list(linked_list *list) {
    linked_list *item;

    for(item = list; item != nullptr; item = item->l_next) {
        rs_fix_thing(THINGPTR(item));
    }
}
template <size_t T>
void rs_fix_magic_items(array<magic_item, T> &mi, size_t cnt) {
    for(size_t i = 0; i < cnt; i++) {
        if(i > 0) {
            mi[i].mi_prob += mi[i - 1].mi_prob;
        }
    }
}

void rs_fix_monsters(array<monster, 26> &mons) {
    sprintf(mons['F' - 'A'].m_stats.s_dmg, "%dd1", game.fung_hit);
}

int rs_save_file(FILE *savef) {
    encclearerr();

    rs_write_thing(savef, &rogue.player);
    rs_write_object_list(savef, floorMap.lvl_obj);
    rs_write_thing_list(savef, floorMap.mlist);
    rs_write_room_reference(savef, game.oldrp);
    rs_write_object_reference(savef, rogue.player.t_pack, rogue.cur_weapon);
    rs_write_object_reference(savef, rogue.player.t_pack, rogue.cur_armor);
    rs_write_object_reference(savef, rogue.player.t_pack, rogue.cur_ring[0]);
    rs_write_object_reference(savef, rogue.player.t_pack, rogue.cur_ring[1]);
    rs_write_chars(savef, game.whoami, 80);
    rs_write_chars(savef, game.fruit, 80);
    rs_write_daemons(savef);                                         /* daemon.c     */
    rs_write_chars(savef, Game::lvl_mons, sizeof(Game::lvl_mons));   /* monsters.c   */
    rs_write_chars(savef, Game::wand_mons, sizeof(Game::wand_mons)); /* monsters.c   */

    return (encclearerr());
}

int rs_restore_file(FILE *savef) {
    encclearerr();

    rs_read_thing(savef, &rogue.player);
    rs_read_object_list(savef, &floorMap.lvl_obj);
    rs_read_thing_list(savef, &floorMap.mlist);
    rs_fix_thing_list(floorMap.mlist);
    rs_read_room_reference(savef, &game.oldrp);
    rs_read_object_reference(savef, rogue.player.t_pack, &rogue.cur_weapon);
    rs_read_object_reference(savef, rogue.player.t_pack, &rogue.cur_armor);
    rs_read_object_reference(savef, rogue.player.t_pack, &rogue.cur_ring[0]);
    rs_read_object_reference(savef, rogue.player.t_pack, &rogue.cur_ring[1]);
    rs_fix_magic_items(Items::things, Items::NUMTHINGS);
    rs_fix_magic_items(Items::s_magic, MAXSCROLLS);
    rs_fix_magic_items(Items::p_magic, MAXPOTIONS);
    rs_fix_magic_items(Items::r_magic, MAXRINGS);
    rs_fix_magic_items(Items::ws_magic, MAXSTICKS);
    rs_read_chars(savef, game.whoami, 80);
    rs_read_chars(savef, game.fruit, 80);
    rs_read_daemons(savef);                                         /* daemon.c     */
    rs_read_chars(savef, Game::lvl_mons, sizeof(Game::lvl_mons));   /* monsters.c   */
    rs_read_chars(savef, Game::wand_mons, sizeof(Game::wand_mons)); /* monsters.c   */
    rs_fix_monsters(Game::monsters);

    return (encclearerr());
}
