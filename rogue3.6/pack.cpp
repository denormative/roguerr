/*
 * Routines to deal with the pack
 *
 * @(#)pack.c     3.6 (Berkeley) 6/15/81
 *
 * Rogue: Exploring the Dungeons of Doom
 * Copyright (C) 1980, 1981 Michael Toy, Ken Arnold and Glenn Wichman
 * All rights reserved.
 *
 * See the file LICENSE.TXT for full copyright and licensing information.
 */

#include "rogue.h"

void picked_up(const linked_list *item, const int silent) {
    /*
     * Notify the user
     */
    auto *obj = lobject(item);
    if(game.notify && !silent) {
        if(!game.terse) {
            addmsg("You now have ");
        }
        msg("%s (%c)", inv_name(obj, !game.terse), pack_char(obj));
    }
    if(obj->o_type == AMULET) {
        game.amulet = TRUE;
    }
}
/*
 * add_pack:
 *     Pick up an object and add it to the pack.  If the argument is non-null
 * use it as the linked_list pointer instead of gettting it off the ground.
 */
void add_pack(linked_list *item, const int silent) {
    linked_list *ip, *lp = nullptr;
    object *op = nullptr;
    int exact, from_floor;

    if(item == nullptr) {
        from_floor = TRUE;
        if((item = find_obj(rogue.hero().y, rogue.hero().x)) == nullptr) {
            return;
        }
    } else {
        from_floor = FALSE;
    }
    auto *obj = lobject(item);
    /*
     * Link it into the pack.  Search the pack for a object of similar type
     * if there isn't one, stuff it at the beginning, if there is, look for one
     * that is exactly the same and just increment the count if there is.
     * it  that.  Food is always put at the beginning for ease of access, but
     * is not ordered so that you can't tell good food from bad.  First check
     * to see if there is something in thr same group and if there is then
     * increment the count.
     */
    if(obj->o_group) {
        for(ip = rogue.pack(); ip != nullptr; ip = next_p(ip)) {
            op = lobject(ip);
            if(op->o_group == obj->o_group) {
                /*
                 * Put it in the pack and notify the user
                 */
                op->o_count++;
                if(from_floor) {
                    detach(floorMap.lvl_obj, item);
                    mvaddch(rogue.hero().y, rogue.hero().x,
                            (roomin(rogue.hero()) == nullptr ? PASSAGE : FLOOR));
                }
                discard(item);
                item = ip;
                picked_up(item, silent);
                return;
            }
        }
    }
    /*
     * Check if there is room
     */
    if(rogue.inpack == Rogue::MAXPACK - 1) {
        msg("You can't carry anything else.");
        return;
    }
    /*
     * Check for and deal with scare monster scrolls
     */
    if(obj->o_type == SCROLL && obj->o_which == S_SCARE) {
        if(obj->o_flags & ISFOUND) {
            msg("The scroll turns to dust as you pick it up.");
            detach(floorMap.lvl_obj, item);
            mvaddch(rogue.hero().y, rogue.hero().x, FLOOR);
            return;
        }
        obj->o_flags |= ISFOUND;
    }

    rogue.inpack++;
    if(from_floor) {
        detach(floorMap.lvl_obj, item);
        mvaddch(rogue.hero().y, rogue.hero().x,
                (roomin(rogue.hero()) == nullptr ? PASSAGE : FLOOR));
    }
    /*
     * Search for an object of the same type
     */
    exact = FALSE;
    for(ip = rogue.pack(); ip != nullptr; ip = next_p(ip)) {
        op = lobject(ip);
        if(obj->o_type == op->o_type) {
            break;
        }
    }
    if(ip == nullptr) {
        /*
         * Put it at the end of the pack since it is a new type
         */
        for(ip = rogue.pack(); ip != nullptr; ip = next_p(ip)) {
            op = lobject(ip);
            if(op->o_type != FOOD) {
                break;
            }
            lp = ip;
        }
    } else {
        /*
         * Search for an object which is exactly the same
         */
        assert(op != nullptr);
        while(ip != nullptr && op->o_type == obj->o_type) {
            if(op->o_which == obj->o_which) {
                exact = TRUE;
                break;
            }
            lp = ip;
            if((ip = next_p(ip)) == nullptr) {
                break;
            }
            op = lobject(ip);
        }
    }
    if(ip == nullptr) {
        /*
         * Didn't find an exact match, just stick it here
         */
        if(rogue.pack() == nullptr) {
            rogue.pack() = item;
        } else {
            assert(lp != nullptr);
            lp->l_next = item;
            item->l_prev = lp;
            item->l_next = nullptr;
        }
    } else {
        /*
         * If we found an exact match.  If it is a potion, food, or a
         * scroll, increase the count, otherwise put it with its clones.
         */
        if(exact && ISMULT(obj->o_type)) {
            op->o_count++;
            discard(item);
            item = ip;
            picked_up(item, silent);
            return;
        }
        if((item->l_prev = prev_p(ip)) != nullptr) {
            item->l_prev->l_next = item;
        } else {
            rogue.pack() = item;
        }
        item->l_next = ip;
        ip->l_prev = item;
    }
    picked_up(item, silent);
}

/*
 * inventory:
 *     list what is in the pack
 */
int inventory(linked_list *list, const int type) {
    char inv_temp[80];
    int n_objs = 0;

    for(int ch = 'a'; list != nullptr; ch++, list = next_p(list)) {
        auto *obj = lobject(list);
        if(type && type != obj->o_type &&
           !(type == CALLABLE && (obj->o_type == SCROLL || obj->o_type == POTION ||
                                  obj->o_type == RING || obj->o_type == STICK))) {
            continue;
        }
        switch(n_objs++) {
            /*
             * For the first thing in the inventory, just save the string
             * in case there is only one.
             */
            case 0:
                sprintf(inv_temp, "%c) %s", ch, inv_name(obj, FALSE));
                break;
            /*
             * If there is more than one, clear the screen, print the
             * saved message and fall through to ...
             */
            case 1:
                if(game.slow_invent) {
                    msg(inv_temp);
                } else {
                    ui.hw.clearWindow();
                    waddstr(ui.hw.w, inv_temp);
                    waddch(ui.hw.w, '\n');
                }
                [[clang::fallthrough]];
            /*
             * Print the line for this object
             */
            default:
                if(game.slow_invent) {
                    msg("%c) %s", ch, inv_name(obj, FALSE));
                } else {
                    wprintw(ui.hw.w, "%c) %s\n", ch, inv_name(obj, FALSE));
                }
        }
    }
    if(n_objs == 0) {
        if(game.terse) {
            msg(type == 0 ? "Empty handed." : "Nothing appropriate");
        } else {
            msg(type == 0 ? "You are empty handed." : "You don't have anything appropriate");
        }
        return FALSE;
    }
    if(n_objs == 1) {
        msg(inv_temp);
        return TRUE;
    }
    if(!game.slow_invent) {
        mvwaddstr(ui.hw.w, LINES - 1, 0, "--Press space to continue--");
        draw(ui.hw.w);
        wait_for(ui.hw.w, ' ');
        clearok(ui.cw.w, TRUE);
        touchwin(ui.cw.w);
    }
    return TRUE;
}

/*
 * pick_up:
 *     Add something to characters pack.
 */
void pick_up(const int ch) {
    switch(ch) {
        case GOLD:
            money();
            break;
        default:
            debug("Where did you pick that up???");
            [[clang::fallthrough]];
        case ARMOR:
        case POTION:
        case FOOD:
        case WEAPON:
        case SCROLL:
        case AMULET:
        case RING:
        case STICK:
            add_pack(nullptr, FALSE);
            break;
    }
}

/*
 * picky_inven:
 *     Allow player to inventory a single item
 */
void picky_inven() {
    if(rogue.pack() == nullptr) {
        msg("You aren't carrying anything");
    } else if(next_p(rogue.pack()) == nullptr) {
        msg("a) %s", inv_name(lobject(rogue.pack()), FALSE));
    } else {
        msg(game.terse ? "Item: " : "Which item do you wish to inventory: ");
        ui.mpos = 0;
        int mch;
        if((mch = readchar(ui.cw.w)) == Game::ESCAPE) {
            msg("");
            return;
        }
        int ch;
        linked_list *item;
        for(ch = 'a', item = rogue.pack(); item != nullptr; item = next_p(item), ch++) {
            if(ch == mch) {
                msg("%c) %s", ch, inv_name(lobject(item), FALSE));
                return;
            }
        }
        if(!game.terse) {
            msg("'%s' not in pack", unctrl_int(mch));
        }
        msg("Range is 'a' to '%c'", --ch);
    }
}

/*
 * get_item:
 *     pick something out of a pack for a purpose
 */
linked_list *get_item(const char *purpose, const int type) {
    if(rogue.pack() == nullptr) {
        msg("You aren't carrying anything.");
    } else {
        for(;;) {
            if(!game.terse) {
                addmsg("Which object do you want to ");
            }
            addmsg(purpose);
            if(game.terse) {
                addmsg(" what");
            }
            msg("? (* for list): ");
            int ch = readchar(ui.cw.w);
            ui.mpos = 0;
            /*
             * Give the poor player a chance to abort the command
             */
            if(ch == Game::ESCAPE || ch == CTRL('G')) {
                game.after = FALSE;
                msg("");
                return nullptr;
            }
            if(ch == '*') {
                ui.mpos = 0;
                if(inventory(rogue.pack(), type) == 0) {
                    game.after = FALSE;
                    return nullptr;
                }
                continue;
            }
            linked_list *obj;
            int och;
            for(obj = rogue.pack(), och = 'a'; obj != nullptr; obj = next_p(obj), och++) {
                if(ch == och) {
                    break;
                }
            }
            if(obj == nullptr) {
                msg("Please specify a letter between 'a' and '%c'", och - 1);
                continue;
            }
            return obj;
        }
    }
    return nullptr;
}

int pack_char(const object *obj) {
    int c = 'a';

    for(linked_list *item = rogue.pack(); item != nullptr; item = next_p(item)) {
        if(lobject(item) == obj) {
            return c;
        }
        c++;
    }
    return 'z';
}
