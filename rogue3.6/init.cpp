/*
 * global variable initializaton
 *
 * @(#)init.c     3.33 (Berkeley) 6/15/81
 *
 * Rogue: Exploring the Dungeons of Doom
 * Copyright (C) 1980, 1981 Michael Toy, Ken Arnold and Glenn Wichman
 * All rights reserved.
 *
 * See the file LICENSE.TXT for full copyright and licensing information.
 */

#include "rogue.h"

const char *Game::PASSWD = "mTBellIQOsLNA";

// clang-format off
#define X_X 1
#define _x {1,1}
array<monster, 26> Game::monsters {
    monster
    /* Name           CARRY     FLAG    str, exp, lvl, amr, hpt, dmg */
    { "giant ant",      0,     ISMEAN,     { _x, 10,   2,   3, X_X, "1d6" } },
    { "bat",      0,     0,     { _x,  1,   1,   3, X_X, "1d2" } },
    { "centaur",      15,     0,     { _x, 15,   4,   4, X_X, "1d6/1d6" } },
    { "dragon",      100,     ISGREED, { _x, 9000, 10,  -1, X_X, "1d8/1d8/3d10" } },
    { "floating eye", 0,     0,     { _x,  5,   1,   9, X_X, "0d0" } },
    { "violet fungi", 0,     ISMEAN,     { _x, 85,   8,   3, X_X, "000d0" } },
    { "gnome",      10,     0,     { _x,  8,   1,   5, X_X, "1d6" } },
    { "hobgoblin",      0,     ISMEAN,     { _x,  3,   1,   5, X_X, "1d8" } },
    { "invisible stalker", 0, ISINVIS, { _x, 120,   8,   3, X_X, "4d4" } },
    { "jackal",      0,     ISMEAN,     { _x,  2,   1,   7, X_X, "1d2" } },
    { "kobold",      0,     ISMEAN,     { _x,  1,   1,   7, X_X, "1d4" } },
    { "leprechaun",      0,     0,     { _x, 10,   3,   8, X_X, "1d1" } },
    { "mimic",      30,     0,     { _x, 140,   7,   7, X_X, "3d4" } },
    { "nymph",      100,     0,     { _x, 40,   3,   9, X_X, "0d0" } },
    { "orc",      15,     ISBLOCK, { _x,  5,   1,   6, X_X, "1d8" } },
    { "purple worm", 70,     0,     { _x, 7000, 15,   6, X_X, "2d12/2d4" } },
    { "quasit",      30,     ISMEAN,     { _x, 35,   3,   2, X_X, "1d2/1d2/1d4" } },
    { "rust monster", 0,     ISMEAN,     { _x, 25,   5,   2, X_X, "0d0/0d0" } },
    { "snake",      0,     ISMEAN,     { _x,  3,   1,   5, X_X, "1d3" } },
    { "troll",      50,     ISREGEN | ISMEAN, { _x, 55,   6,   4, X_X, "1d8/1d8/2d6" } },
    { "umber hulk",      40,     ISMEAN,     { _x, 130,   8,   2, X_X, "3d4/3d4/2d5" } },
    { "vampire",      20,     ISREGEN | ISMEAN, { _x, 380,   8,   1, X_X, "1d10" } },
    { "wraith",      0,     0,     { _x, 55,   5,   4, X_X, "1d6" } },
    { "xorn",      0,     ISMEAN,     { _x, 120,   7,  -2, X_X, "1d3/1d3/1d3/4d6" } },
    { "yeti",      30,     0,     { _x, 50,   4,   6, X_X, "1d6/1d6" } },
    { "zombie",      0,     ISMEAN,     { _x,  7,   2,   8, X_X, "1d8" } }
};
#undef X_X
// clang-format on

/*
 * init_player:
 *     roll up the rogue
 */

void Rogue::init_player() {
    pstats().s_lvl = 1;
    pstats().s_exp = 0L;
    max_hp = pstats().s_hpt = 12;
    if(rnd(100) == 7) {
        pstats().s_str.st_str = 18;
        pstats().s_str.st_add = rnd(100) + 1;
    } else {
        pstats().s_str.st_str = 16;
        pstats().s_str.st_add = 0;
    }
    strcpy(pstats().s_dmg, "1d4");
    pstats().s_arm = 10;
    max_stats = pstats();
    pack() = nullptr;
}

/* Contains defintions and functions for dealing with things like potions and scrolls */

array<string, 24> Game::rainbow{
    "red",    "blue",      "green",   "yellow", "black", "brown",  "orange", "pink",
    "purple", "grey",      "white",   "silver", "gold",  "violet", "clear",  "vermilion",
    "ecru",   "turquoise", "magenta", "amber",  "topaz", "plaid",  "tan",    "tangerine",
};

array<string, 159> Game::sylls{
    "a",    "ab",   "ag",   "aks",  "ala",  "an",   "ankh", "app", "arg",  "arze", "ash", "ban",
    "bar",  "bat",  "bek",  "bie",  "bin",  "bit",  "bjor", "blu", "bot",  "bu",   "byt", "comp",
    "con",  "cos",  "cre",  "dalf", "dan",  "den",  "do",   "e",   "eep",  "el",   "eng", "er",
    "ere",  "erk",  "esh",  "evs",  "fa",   "fid",  "for",  "fri", "fu",   "gan",  "gar", "glen",
    "gop",  "gre",  "ha",   "he",   "hyd",  "i",    "ing",  "ion", "ip",   "ish",  "it",  "ite",
    "iv",   "jo",   "kho",  "kli",  "klis", "la",   "lech", "man", "mar",  "me",   "mi",  "mic",
    "mik",  "mon",  "mung", "mur",  "nej",  "nelg", "nep",  "ner", "nes",  "nes",  "nih", "nin",
    "o",    "od",   "ood",  "org",  "orn",  "ox",   "oxy",  "pay", "pet",  "ple",  "plu", "po",
    "pot",  "prok", "re",   "rea",  "rhov", "ri",   "ro",   "rog", "rok",  "rol",  "sa",  "san",
    "sat",  "see",  "sef",  "seh",  "shu",  "ski",  "sna",  "sne", "snik", "sno",  "so",  "sol",
    "sri",  "sta",  "sun",  "ta",   "tab",  "tem",  "ther", "ti",  "tox",  "trol", "tue", "turs",
    "u",    "ulk",  "um",   "un",   "uni",  "ur",   "val",  "viv", "vly",  "vom",  "wah", "wed",
    "werg", "wex",  "whon", "wun",  "xo",   "y",    "yot",  "yu",  "zant", "zap",  "zeb", "zim",
    "zok",  "zon",  "zum",
};

array<string, 20> Game::stones{
    "agate", "alexandrite", "amethyst",     "carnelian", "diamond",  "emerald",   "granite",
    "jade",  "kryptonite",  "lapus lazuli", "moonstone", "obsidian", "onyx",      "opal",
    "pearl", "ruby",        "saphire",      "tiger eye", "topaz",    "turquoise",
};

array<string, 22> Game::wood{
    "avocado wood", "balsa",     "banyan", "birch",      "cedar",          "cherry",
    "cinnibar",     "driftwood", "ebony",  "eucalyptus", "hemlock",        "ironwood",
    "mahogany",     "manzanita", "maple",  "oak",        "persimmon wood", "redwood",
    "rosewood",     "teak",      "walnut", "zebra wood",
};

array<string, 11> Game::metal{
    "aluminium", "bone",   "brass", "bronze", "copper", "iron",
    "lead",      "pewter", "steel", "tin",    "zinc",
};

// clang-format off
array<magic_item, Items::NUMTHINGS> Items::things {
    magic_item
    { "",               27, 0 },     /* potion */
    { "",               27, 0 },     /* scroll */
    { "",               18, 0 },     /* food */
    { "",                9, 0 },     /* weapon */
    { "",                9, 0 },     /* armor */
    { "",                5, 0 },     /* ring */
    { "",                5, 0 },     /* stick */
};

array<magic_item, MAXSCROLLS> Items::s_magic {
    magic_item
    { "monster confusion",      8, 170 },
    { "magic mapping",           5, 180 },
    { "light",               10, 100 },
    { "hold monster",           2, 200 },
    { "sleep",                5,  50 },
    { "enchant armor",           8, 130 },
    { "identify",          21, 100 },
    { "scare monster",           4, 180 },
    { "gold detection",           4, 110 },
    { "teleportation",           7, 175 },
    { "enchant weapon",          10, 150 },
    { "create monster",           5,  75 },
    { "remove curse",           8, 105 },
    { "aggravate monsters",      1,  60 },
    { "blank paper",           1,  50 },
    { "genocide",           1, 200 },
};

array<magic_item, MAXPOTIONS> Items::p_magic {
    magic_item
    { "confusion",           8,  50 },
    { "paralysis",          10,  50 },
    { "poison",                8,  50 },
    { "gain strength",          15, 150 },
    { "see invisible",           2, 170 },
    { "healing",          15, 130 },
    { "monster detection",      6, 120 },
    { "magic detection",      6, 105 },
    { "raise level",           2, 220 },
    { "extra healing",           5, 180 },
    { "haste self",           4, 200 },
    { "restore strength",     14, 120 },
    { "blindness",           4,  50 },
    { "thirst quenching",      1,  50 },
};

array<magic_item, MAXRINGS> Items::r_magic {
    magic_item
    { "protection",           9, 200 },
    { "add strength",           9, 200 },
    { "sustain strength",      5, 180 },
    { "searching",          10, 200 },
    { "see invisible",          10, 175 },
    { "adornment",           1, 100 },
    { "aggravate monster",     11, 100 },
    { "dexterity",           8, 220 },
    { "increase damage",      8, 220 },
    { "regeneration",           4, 260 },
    { "slow digestion",           9, 240 },
    { "teleportation",           9, 100 },
    { "stealth",           7, 100 },
};

array<magic_item, MAXSTICKS> Items::ws_magic {
    magic_item
    { "light",               12, 120 },
    { "striking",           9, 115 },
    { "lightning",           3, 200 },
    { "fire",                3, 200 },
    { "cold",                3, 200 },
    { "polymorph",          15, 210 },
    { "magic missile",          10, 170 },
    { "haste monster",           9,  50 },
    { "slow monster",          11, 220 },
    { "drain life",           9, 210 },
    { "nothing",           1,  70 },
    { "teleport away",           5, 140 },
    { "teleport to",           5,  60 },
    { "cancellation",           5, 130 },
};
// clang-format on

array<int, MAXARMORS> Items::a_class{
    8, 7, 7, 6, 5, 4, 4, 3,
};

array<string, MAXARMORS> Items::a_names{
    "leather armor", "ring mail",   "studded leather armor", "scale mail",
    "chain mail",    "splint mail", "banded mail",           "plate mail",
};

array<int, MAXARMORS> Items::a_chances{20, 35, 50, 63, 75, 85, 95, 100};

#define MAX3(a, b, c) ((a) > (b) ? ((a) > (c) ? (a) : (c)) : ((b) > (c) ? (b) : (c)))
int Game::used[MAX3(Game::rainbow.size(), Game::stones.size(), Game::wood.size())];

template <size_t T>
void badcheck(const char *name, array<magic_item, T> &magic) {
    if(magic.back().mi_prob == 100) {
        return;
    }
    printf("\nBad percentages for %s:\n", name);
    for(auto i = magic.rbegin(); i != magic.rend(); ++i) {
        printf("%3d%% %s\n", i->mi_prob, i->mi_name.c_str());
    }
    printf("[hit RETURN to continue]");
    fflush(stdout);
    while(getchar() != '\n') {
        // discard \n's.
    }
}

/*
 * init_things
 *     Initialize the probabilities for types of things
 */
void init_things() {
    for(magic_item *mp = &Items::things[1]; mp <= &Items::things[Items::NUMTHINGS - 1]; mp++) {
        mp->mi_prob += (mp - 1)->mi_prob;
    }
    badcheck("things", Items::things);
}

/*
 * init_colors:
 *     Initialize the potion color scheme for this time
 */

void init_colors() {
    for(int &i : Game::used) {
        i = 0;
    }
    for(size_t i = 0; i < MAXPOTIONS; i++) {
        size_t j;

        do {
            j = rnd(Game::rainbow.size());
        } while(Game::used[j]);

        Game::used[j] = TRUE;
        items.p_colors[i] = Game::rainbow[j];
        items.p_know[i] = FALSE;
        if(i > 0) {
            Items::p_magic[i].mi_prob += Items::p_magic[i - 1].mi_prob;
        }
    }
    badcheck("potions", Items::p_magic);
}

/*
 * init_names:
 *     Generate the names of the various scrolls
 */

void init_names() {
    for(size_t i = 0; i < MAXSCROLLS; i++) {
        char *cp = ui.prbuf;
        int nwords = rnd(4) + 2;
        while(nwords--) {
            int nsyl = rnd(3) + 1;
            while(nsyl--) {
                const char *sp = Game::sylls[rnd(Game::sylls.size())].c_str();
                while(*sp) {
                    *cp++ = *sp++;
                }
            }
            *cp++ = ' ';
        }
        *--cp = '\0';
        items.s_know[i] = FALSE;
        items.s_names[i] = ui.prbuf;
        if(i > 0) {
            Items::s_magic[i].mi_prob += Items::s_magic[i - 1].mi_prob;
        }
    }
    badcheck("scrolls", Items::s_magic);
}

/*
 * init_stones:
 *     Initialize the ring stone setting scheme for this time
 */

void init_stones() {
    for(size_t i = 0; i < Game::stones.size(); i++) {
        Game::used[i] = FALSE;
    }
    for(size_t i = 0; i < MAXRINGS; i++) {
        size_t j;

        do {
            j = rnd(Game::stones.size());
        } while(Game::used[j]);

        Game::used[j] = TRUE;
        items.r_stones[i] = Game::stones[j];
        items.r_know[i] = FALSE;
        if(i > 0) {
            Items::r_magic[i].mi_prob += Items::r_magic[i - 1].mi_prob;
        }
    }
    badcheck("rings", Items::r_magic);
}

/*
 * init_materials:
 *     Initialize the construction materials for wands and staffs
 */

void init_materials() {
    static int metused[Game::metal.size()];

    for(size_t i = 0; i < Game::wood.size(); i++) {
        Game::used[i] = FALSE;
    }
    for(int &i : metused) {
        i = FALSE;
    }

    for(size_t i = 0; i < MAXSTICKS; i++) {
        for(;;) {
            if(rnd(100) > 50) {
                auto j = rnd(Game::metal.size());
                if(!metused[j]) {
                    metused[j] = TRUE;
                    items.ws_made[i] = Game::metal[j];
                    items.ws_type[i] = "wand";
                    break;
                }
            } else {
                auto j = rnd(Game::wood.size());
                if(!Game::used[j]) {
                    Game::used[j] = TRUE;
                    items.ws_made[i] = Game::wood[j];
                    items.ws_type[i] = "staff";
                    break;
                }
            }
        }

        items.ws_know[i] = FALSE;
        if(i > 0) {
            Items::ws_magic[i].mi_prob += Items::ws_magic[i - 1].mi_prob;
        }
    }
    badcheck("sticks", Items::ws_magic);
}

Game::Game() noexcept {
    // clang-format off
    helpstr = {
        h_list
        {'?',     "     prints help"},
        {'/',     "     identify object"},
        {'h',     "     left"},
        {'j',     "     down"},
        {'k',     "     up"},
        {'l',     "     right"},
        {'y',     "     up & left"},
        {'u',     "     up & right"},
        {'b',     "     down & left"},
        {'n',     "     down & right"},
        {'H',     "     run left"},
        {'J',     "     run down"},
        {'K',     "     run up"},
        {'L',     "     run right"},
        {'Y',     "     run up & left"},
        {'U',     "     run up & right"},
        {'B',     "     run down & left"},
        {'N',     "     run down & right"},
        {'t',     "<dir>     throw something"},
        {'f',     "<dir>     forward until find something"},
        {'p',     "<dir>     zap a wand in a direction"},
        {'z',     "     zap a wand or staff"},
        {'>',     "     go down a staircase"},
        {'s',     "     search for trap/secret door"},
        {' ',     "     (space) rest for a while"},
        {'i',     "     inventory"},
        {'I',     "     inventory single item"},
        {'q',     "     quaff potion"},
        {'r',     "     read paper"},
        {'e',     "     eat food"},
        {'w',     "     wield a weapon"},
        {'W',     "     wear armor"},
        {'T',     "     take armor off"},
        {'P',     "     put on ring"},
        {'R',     "     remove ring"},
        {'d',     "     drop object"},
        {'c',     "     call object"},
        {'o',     "     examine/set options"},
        {CTRL('L'),     "     redraw screen"},
        {CTRL('R'),     "     repeat last message"},
        {Game::ESCAPE,     "     cancel command"},
        {'v',     "     print program version number"},
        {'S',     "     save game"},
        {'Q',     "     quit"},
    };
    // clang-format on
}
