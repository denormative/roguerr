/*
 * All the daemon and fuse functions are in here
 *
 * @(#)daemons.c     3.7 (Berkeley) 6/15/81
 *
 * Rogue: Exploring the Dungeons of Doom
 * Copyright (C) 1980, 1981 Michael Toy, Ken Arnold and Glenn Wichman
 * All rights reserved.
 *
 * See the file LICENSE.TXT for full copyright and licensing information.
 */

#include "rogue.h"

/*
 * doctor:
 *     A healing daemon that restors hit points after rest
 */

void doctor() {
    int lv = rogue.pstats().s_lvl;
    int ohp = rogue.pstats().s_hpt;
    game.quiet++;
    if(lv < 8) {
        if(game.quiet > 20 - lv * 2) {
            rogue.pstats().s_hpt++;
        }
    } else if(game.quiet >= 3) {
        rogue.pstats().s_hpt += rnd(lv - 7) + 1;
    }
    if(rogue.isRing(Rogue::LEFT, R_REGEN)) {
        rogue.pstats().s_hpt++;
    }
    if(rogue.isRing(Rogue::RIGHT, R_REGEN)) {
        rogue.pstats().s_hpt++;
    }
    if(ohp != rogue.pstats().s_hpt) {
        if(rogue.pstats().s_hpt > rogue.max_hp) {
            rogue.pstats().s_hpt = rogue.max_hp;
        }
        game.quiet = 0;
    }
}

/*
 * Swander:
 *     Called when it is time to start rolling for wandering monsters
 */

void swander() {
    daemons.startDaemon(d_rollwand, 0, RogueDaemons::Before);
}

/*
 * rollwand:
 *     Called to roll to see if a wandering monster starts up
 */

void rollwand() {
    if(++game.between >= 4) {
        if(roll(1, 6) == 4) {
            wanderer();
            daemons.killDaemon(d_rollwand);
            daemons.startFuse(d_swander, 0, Game::WANDERTIME, RogueDaemons::Before);
        }
        game.between = 0;
    }
}

/*
 * unconfuse:
 *     Release the poor player from his confusion
 */

void unconfuse() {
    rogue.player.t_flags &= ~ISHUH;
    msg("You feel less confused now");
}

/*
 * unsee:
 *     He lost his see invisible power
 */

void unsee() {
    rogue.player.t_flags &= ~CANSEE;
}

/*
 * sight:
 *     He gets his sight back
 */

void sight() {
    if(on(rogue.player, ISBLIND)) {
        daemons.extinguish(d_sight);
        rogue.player.t_flags &= ~ISBLIND;
        light(rogue.hero());
        msg("The veil of darkness lifts");
    }
}

/*
 * nohaste:
 *     End the hasting
 */

void nohaste() {
    rogue.player.t_flags &= ~ISHASTE;
    msg("You feel yourself slowing down.");
}

/*
 * digest the hero's food
 */
void stomach() {
    if(rogue.food_left <= 0) {
        /*
         * the hero is fainting
         */
        if(rogue.no_command || rnd(100) > 20) {
            return;
        }
        rogue.no_command = rnd(8) + 4;
        if(!game.terse) {
            addmsg("You feel too weak from lack of food.  ");
        }
        msg("You faint");
        game.running = FALSE;
        ui.count = 0;
        rogue.hungry_state = 3;
    } else {
        int oldfood = rogue.food_left;
        rogue.food_left -= ring_eat(Rogue::LEFT) + ring_eat(Rogue::RIGHT) + 1 - game.amulet;

        if(rogue.food_left < Game::MORETIME && oldfood >= Game::MORETIME) {
            msg("You are starting to feel weak");
            rogue.hungry_state = 2;
        } else if(rogue.food_left < 2 * Game::MORETIME && oldfood >= 2 * Game::MORETIME) {
            if(!game.terse) {
                msg("You are starting to get hungry");
            } else {
                msg("Getting hungry");
            }
            rogue.hungry_state = 1;
        }
    }
}
