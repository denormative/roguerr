/*
 * all sorts of miscellaneous routines
 *
 * @(#)misc.c     3.13 (Berkeley) 6/15/81
 *
 * Rogue: Exploring the Dungeons of Doom
 * Copyright (C) 1980, 1981 Michael Toy, Ken Arnold and Glenn Wichman
 * All rights reserved.
 *
 * See the file LICENSE.TXT for full copyright and licensing information.
 */

#include "rogue.h"

/*
 * tr_name:
 *     print the name of a trap
 */

const char *trapName(const int ch) {
    const char *s = "";

    switch(ch) {
        case TRAPDOOR:
            s = game.terse ? "A trapdoor." : "You found a trapdoor.";
            break;
        case BEARTRAP:
            s = game.terse ? "A beartrap." : "You found a beartrap.";
            break;
        case SLEEPTRAP:
            s = game.terse ? "A sleeping gas trap." : "You found a sleeping gas trap.";
            break;
        case ARROWTRAP:
            s = game.terse ? "An arrow trap." : "You found an arrow trap.";
            break;
        case TELTRAP:
            s = game.terse ? "A teleport trap." : "You found a teleport trap.";
            break;
        case DARTTRAP:
            s = game.terse ? "A dart trap." : "You found a poison dart trap.";
    }
    return s;
}

/*
 * Look:
 *     A quick glance all around the player
 */

void look(const int wakeup) {
    int passcount = 0;

    int oldx, oldy;
    getyx(ui.cw.w, oldy, oldx);

    if(game.oldrp != nullptr && (game.oldrp->r_flags & ISDARK) && off(rogue.player, ISBLIND)) {
        for(int x = game.oldpos.x - 1; x <= game.oldpos.x + 1; x++) {
            for(int y = game.oldpos.y - 1; y <= game.oldpos.y + 1; y++) {
                if((y != rogue.hero().y || x != rogue.hero().x) && show(y, x) == FLOOR) {
                    mvwaddch(ui.cw.w, y, x, ' ');
                }
            }
        }
    }
    room *rp;
    int inpass = ((rp = roomin(rogue.hero())) == nullptr);
    int ey = rogue.hero().y + 1;
    int ex = rogue.hero().x + 1;
    for(int x = rogue.hero().x - 1; x <= ex; x++) {
        if(x >= 0 && x < COLS) {
            for(int y = rogue.hero().y - 1; y <= ey; y++) {
                if(y <= 0 || y >= LINES - 1) {
                    continue;
                }
                if(isupper((int)mvwinch(ui.mw.w, y, x))) {
                    linked_list *it;
                    thing *tp;

                    if(wakeup) {
                        it = wake_monster(y, x);
                    } else {
                        it = find_mons(y, x);
                    }
                    tp = lthing(it);
                    if((tp->t_oldch = (int)mvinch(y, x)) == TRAP) {
                        tp->t_oldch = (trap_at(y, x)->tr_flags & ISFOUND) ? TRAP : FLOOR;
                    }
                    if(tp->t_oldch == FLOOR && (rp != nullptr) && (rp->r_flags & ISDARK) &&
                       off(rogue.player, ISBLIND)) {
                        tp->t_oldch = ' ';
                    }
                }

                int ch;
                /* Secret doors show as walls */
                if((ch = show(y, x)) == SECRETDOOR) {
                    ch = secretdoor(y, x);
                }
                /* Don't show room walls if he is in a passage */
                if(off(rogue.player, ISBLIND)) {
                    if((y == rogue.hero().y && x == rogue.hero().x) ||
                       (inpass && (ch == '-' || ch == '|'))) {
                        continue;
                    }
                } else if(y != rogue.hero().y || x != rogue.hero().x) {
                    continue;
                }
                wmove(ui.cw.w, y, x);
                waddch_int(ui.cw.w, ch);
                if(game.door_stop && !game.firstmove && game.running) {
                    switch(game.runch) {
                        case 'h':
                            if(x == ex) {
                                continue;
                            }
                            break;
                        case 'j':
                            if(y == rogue.hero().y - 1) {
                                continue;
                            }
                            break;
                        case 'k':
                            if(y == ey) {
                                continue;
                            }
                            break;
                        case 'l':
                            if(x == rogue.hero().x - 1) {
                                continue;
                            }
                            break;
                        case 'y':
                            if((x + y) - (rogue.hero().x + rogue.hero().y) >= 1) {
                                continue;
                            }
                            break;
                        case 'u':
                            if((y - x) - (rogue.hero().y - rogue.hero().x) >= 1) {
                                continue;
                            }
                            break;
                        case 'n':
                            if((x + y) - (rogue.hero().x + rogue.hero().y) <= -1) {
                                continue;
                            }
                            break;
                        case 'b':
                            if((y - x) - (rogue.hero().y - rogue.hero().x) <= -1) {
                                continue;
                            }
                    }
                    switch(ch) {
                        case DOOR:
                            if(x == rogue.hero().x || y == rogue.hero().y) {
                                game.running = FALSE;
                            }
                            break;
                        case PASSAGE:
                            if(x == rogue.hero().x || y == rogue.hero().y) {
                                passcount++;
                            }
                            break;
                        case FLOOR:
                        case '|':
                        case '-':
                        case ' ':
                            break;
                        default:
                            game.running = FALSE;
                            break;
                    }
                }
            }
        }
    }
    if(game.door_stop && !game.firstmove && passcount > 1) {
        game.running = FALSE;
    }
    mvwaddch(ui.cw.w, rogue.hero().y, rogue.hero().x, PLAYER);
    wmove(ui.cw.w, oldy, oldx);
    game.oldpos = rogue.hero();
    game.oldrp = rp;
}

/*
 * secret_door:
 *     Figure out what a secret door looks like.
 */

int secretdoor(const int y, const int x) {
    int i;
    room *rp;
    coord cp{x, y};
    for(rp = floorMap.rooms, i = 0; i < Floor::MAXROOMS; rp++, i++) {
        if(inroom(*rp, cp)) {
            if(y == rp->r_pos.y || y == rp->r_pos.y + rp->r_max.y - 1) {
                return ('-');
            }
            return ('|');
        }
    }

    return ('p');
}

/*
 * find_obj:
 *     find the unclaimed object at y, x
 */

linked_list *find_obj(const int y, const int x) {
    linked_list *obj;
    object *op;

    for(obj = floorMap.lvl_obj; obj != nullptr; obj = next_p(obj)) {
        op = lobject(obj);
        if(op->o_pos.y == y && op->o_pos.x == x) {
            return obj;
        }
    }
    sprintf(ui.prbuf, "Non-object %d,%d", y, x);
    debug(ui.prbuf);
    return nullptr;
}

/*
 * eat:
 *     She wants to eat something, so let her try
 */

void eat() {
    linked_list *item;
    object *obj;

    if((item = get_item("eat", FOOD)) == nullptr) {
        return;
    }
    obj = lobject(item);
    if(obj->o_type != FOOD) {
        if(!game.terse) {
            msg("Ugh, you would get ill if you ate that.");
        } else {
            msg("That's Inedible!");
        }
        return;
    }
    rogue.inpack--;
    if(obj->o_which == 1) {
        msg("My, that was a yummy %s", game.fruit);
    } else if(rnd(100) > 70) {
        msg("Yuk, this food tastes awful");
        rogue.pstats().s_exp++;
        check_level();
    } else {
        msg("Yum, that tasted good");
    }
    if((rogue.food_left += Game::HUNGERTIME + rnd(400) - 200) > Game::STOMACHSIZE) {
        rogue.food_left = Game::STOMACHSIZE;
    }
    rogue.hungry_state = 0;
    if(obj == rogue.cur_weapon) {
        rogue.cur_weapon = nullptr;
    }
    if(--obj->o_count < 1) {
        detach(rogue.pack(), item);
        discard(item);
    }
}

/*
 * Used to modify the playes strength
 * it keeps track of the highest it has been, just in case
 */

void chg_str(int amt) {
    if(amt == 0) {
        return;
    }
    if(amt > 0) {
        while(amt--) {
            if(rogue.pstats().s_str.st_str < 18) {
                rogue.pstats().s_str.st_str++;
            } else if(rogue.pstats().s_str.st_add == 0) {
                rogue.pstats().s_str.st_add = rnd(50) + 1;
            } else if(rogue.pstats().s_str.st_add <= 50) {
                rogue.pstats().s_str.st_add = 51 + rnd(24);
            } else if(rogue.pstats().s_str.st_add <= 75) {
                rogue.pstats().s_str.st_add = 76 + rnd(14);
            } else if(rogue.pstats().s_str.st_add <= 90) {
                rogue.pstats().s_str.st_add = 91;
            } else if(rogue.pstats().s_str.st_add < 100) {
                rogue.pstats().s_str.st_add++;
            }
        }
        if(rogue.pstats().s_str.st_str > rogue.max_stats.s_str.st_str ||
           (rogue.pstats().s_str.st_str == 18 &&
            rogue.pstats().s_str.st_add > rogue.max_stats.s_str.st_add)) {
            rogue.max_stats.s_str = rogue.pstats().s_str;
        }
    } else {
        while(amt++) {
            if(rogue.pstats().s_str.st_str < 18 || rogue.pstats().s_str.st_add == 0) {
                rogue.pstats().s_str.st_str--;
            } else if(rogue.pstats().s_str.st_add < 51) {
                rogue.pstats().s_str.st_add = 0;
            } else if(rogue.pstats().s_str.st_add < 76) {
                rogue.pstats().s_str.st_add = 1 + rnd(50);
            } else if(rogue.pstats().s_str.st_add < 91) {
                rogue.pstats().s_str.st_add = 51 + rnd(25);
            } else if(rogue.pstats().s_str.st_add < 100) {
                rogue.pstats().s_str.st_add = 76 + rnd(14);
            } else {
                rogue.pstats().s_str.st_add = 91 + rnd(8);
            }
        }
        if(rogue.pstats().s_str.st_str < 3) {
            rogue.pstats().s_str.st_str = 3;
        }
    }
}

/*
 * add_haste:
 *     add a haste to the player
 */

void add_haste(int potion) {
    if(on(rogue.player, ISHASTE)) {
        msg("You faint from exhaustion.");
        rogue.no_command += rnd(8);
        daemons.extinguish(d_nohaste);
    } else {
        rogue.player.t_flags |= ISHASTE;
        if(potion) {
            daemons.startFuse(d_nohaste, 0, rnd(4) + 4, RogueDaemons::After);
        }
    }
}

/*
 * aggravate:
 *     aggravate all the monsters on this level
 */

void aggravate() {
    for(linked_list *mi = floorMap.mlist; mi != nullptr; mi = next_p(mi)) {
        runto((lthing(mi))->t_pos, hero_pos);
    }
}

/*
 * for printfs: if string starts with a vowel, return "n" for an "an"
 */
std::string vowelstr(const std::string &str) {
    if(str.empty()) {
        return "";
    }

    switch(str[0]) {
        case 'a':
        case 'e':
        case 'i':
        case 'o':
        case 'u':
            return "n";
        default:
            return "";
    }
}

/*
 * see if the object is one of the currently used items
 */
int is_current(const object *obj) {
    if(obj == nullptr) {
        return FALSE;
    }
    if(obj == rogue.cur_armor || obj == rogue.cur_weapon || obj == rogue.cur_ring[Rogue::LEFT] ||
       obj == rogue.cur_ring[Rogue::RIGHT]) {
        msg(game.terse ? "In use." : "That's already in use.");
        return TRUE;
    }
    return FALSE;
}

/*
 * set up the direction co_ordinate for use in varios "prefix" commands
 */
int get_dir() {
    const char *prompt;
    int gotit;

    if(!game.terse) {
        msg(prompt = "Which direction? ");
    } else {
        prompt = "Direction: ";
    }
    do {
        gotit = TRUE;
        switch(readchar(ui.cw.w)) {
            case 'h':
            case 'H':
                game.delta.y = 0;
                game.delta.x = -1;
                break;
            case 'j':
            case 'J':
                game.delta.y = 1;
                game.delta.x = 0;
                break;
            case 'k':
            case 'K':
                game.delta.y = -1;
                game.delta.x = 0;
                break;
            case 'l':
            case 'L':
                game.delta.y = 0;
                game.delta.x = 1;
                break;
            case 'y':
            case 'Y':
                game.delta.y = -1;
                game.delta.x = -1;
                break;
            case 'u':
            case 'U':
                game.delta.y = -1;
                game.delta.x = 1;
                break;
            case 'b':
            case 'B':
                game.delta.y = 1;
                game.delta.x = -1;
                break;
            case 'n':
            case 'N':
                game.delta.y = 1;
                game.delta.x = 1;
                break;
            case Game::ESCAPE:
                return FALSE;
                // break;
            default:
                ui.mpos = 0;
                msg(prompt);
                gotit = FALSE;
        }
    } while(!gotit);
    if(on(rogue.player, ISHUH) && rnd(100) > 80) {
        do {
            game.delta.y = rnd(3) - 1;
            game.delta.x = rnd(3) - 1;
        } while(game.delta.y == 0 && game.delta.x == 0);
    }
    ui.mpos = 0;
    return TRUE;
}
