/*
 * This file has all the code for the option command.
 * I would rather this command were not necessary, but
 * it is the only way to keep the wolves off of my back.
 *
 * @(#)options.c     3.3 (Berkeley) 5/25/81
 *
 * Rogue: Exploring the Dungeons of Doom
 * Copyright (C) 1980, 1981 Michael Toy, Ken Arnold and Glenn Wichman
 * All rights reserved.
 *
 * See the file LICENSE.TXT for full copyright and licensing information.
 */

#include "rogue.h"

#define NUM_OPTS (sizeof UI::optlist / sizeof(UI::OPTION))

inline bool eqstr(const char *a, const string &b, size_t n) {
    return (strncmp(a, b.c_str(), n) == 0);
}

UI::OPTION UI::optlist[] = {
    {"terse", "Terse output: ", (int *)&game.terse, put_bool, get_bool},
    {"flush", "Flush typeahead during battle: ", (int *)&game.fight_flush, put_bool, get_bool},
    {"jump", "Show position only at end of run: ", (int *)&game.jump, put_bool, get_bool},
    {"step", "Do inventories one line at a time: ", (int *)&game.slow_invent, put_bool, get_bool},
    {"askme", "Ask me about unidentified things: ", (int *)&game.askme, put_bool, get_bool},
    {"name", "Name: ", (int *)game.whoami, put_str, get_str},
    {"fruit", "Fruit: ", (int *)game.fruit, put_str, get_str},
    {"file", "Save file: ", (int *)game.save_file_name, put_str, get_str}};

/* print and then set options from the terminal */
void option() {
    ui.hw.clearWindow();
    touchwin(ui.hw.w);
    /* Display current values of options */
    for(UI::OPTION *op = UI::optlist; op <= &UI::optlist[NUM_OPTS - 1]; op++) {
        waddstr(ui.hw.w, op->o_prompt.c_str());
        (*op->o_putfunc)(op->o_opt);
        waddch(ui.hw.w, '\n');
    }
    /* Set values */
    wmove(ui.hw.w, 0, 0);
    for(UI::OPTION *op = UI::optlist; op <= &UI::optlist[NUM_OPTS - 1]; op++) {
        waddstr(ui.hw.w, op->o_prompt.c_str());
        int retval;
        if((retval = (*op->o_getfunc)(op->o_opt, ui.hw.w))) {
            if(retval == UI::QUIT) {
                break;
            }
            if(op > UI::optlist) { /* MINUS */
                wmove(ui.hw.w, (int)(op - UI::optlist) - 1, 0);
                op -= 2;
            } else { /* trying to back up beyond the top */
                beep();
                wmove(ui.hw.w, 0, 0);
                op--;
            }
        }
    }
    /* Switch back to original screen */
    mvwaddstr(ui.hw.w, LINES - 1, 0, "--Press space to continue--");
    draw(ui.hw.w);
    wait_for(ui.hw.w, ' ');
    clearok(ui.cw.w, TRUE);
    touchwin(ui.cw.w);
    game.after = FALSE;
}

/* put out a boolean */
void put_bool(void *b) {
    waddstr(ui.hw.w, *(int *)b ? "True" : "False");
}

/* put out a string */
void put_str(void *str) {
    waddstr(ui.hw.w, (char *)str);
}

/* allow changing a boolean option and print it out */
int get_bool(void *vp, WINDOW *win) {
    int *bp = (int *)vp;
    int oy, ox;
    int op_bad = TRUE;

    getyx(win, oy, ox);
    waddstr(win, *bp ? "True" : "False");
    while(op_bad) {
        wmove(win, oy, ox);
        draw(win);
        switch(readchar(win)) {
            case 't':
            case 'T':
                *bp = TRUE;
                op_bad = FALSE;
                break;
            case 'f':
            case 'F':
                *bp = FALSE;
                op_bad = FALSE;
                break;
            case '\n':
            case '\r':
                op_bad = FALSE;
                break;
            case '\033':
            case '\007':
                return UI::QUIT;
            case '-':
                return UI::MINUS;
            default:
                mvwaddstr(win, oy, ox + 10, "(T or F)");
        }
    }
    wmove(win, oy, ox);
    waddstr(win, *bp ? "True" : "False");
    waddch(win, '\n');
    return UI::NORM;
}

/* set a string option */
int get_str(void *vopt, WINDOW *win) {
    char *opt = (char *)vopt;
    char *sp;
    int c, oy, ox;
    char buf[80];

    draw(win);
    getyx(win, oy, ox);
    /* loop reading in the string, and put it in a temporary buffer */
    for(sp = buf; (c = readchar(win)) != '\n' && c != '\r' && c != '\033' && c != '\007';
        wclrtoeol(win), draw(win)) {
        if(c == -1) {
            continue;
        }
        if(c == erasechar()) { /* process erase character */
            if(sp > buf) {
                int myx, myy;

                sp--;

                for(size_t i = strlen(unctrl((chtype)*sp)); i; i--) {
                    getyx(win, myy, myx);
                    if((myx == 0) && (myy > 0)) {
                        wmove(win, myy - 1, getmaxx(win) - 1);
                        waddch(win, ' ');
                        wmove(win, myy - 1, getmaxx(win) - 1);
                    } else {
                        waddch(win, '\b');
                    }
                }
            }
            continue;
        }
        if(c == killchar()) { /* process kill character */
            sp = buf;
            wmove(win, oy, ox);
            continue;
        }
        if(sp == buf) {
            if(c == '-') {
                break;
            }
            if(c == '~') {
                strcpy(buf, game.home);
                waddstr(win, game.home);
                sp += strlen(game.home);
                continue;
            }
        }

        if((sp - buf) < 78) { /* Avoid overflow */
            *sp++ = (char)c;
            waddstr(win, unctrl_int(c));
        }
    }
    *sp = '\0';
    if(sp > buf) { /* only change option if something has been typed */
        strucpy(opt, buf, strlen(buf));
    }
    wmove(win, oy, ox);
    waddstr(win, opt);
    waddch(win, '\n');
    draw(win);
    if(win == ui.cw.w) {
        ui.mpos += (int)(sp - buf);
    }
    if(c == '-') {
        return UI::MINUS;
    }
    if(c == '\033' || c == '\007') {
        return UI::QUIT;
    }
    return UI::NORM;
}

/*
 * parse options from string, usually taken from the environment.
 * the string is a series of comma seperated values, with booleans
 * being stated as "name" (true) or "noname" (false), and strings
 * being "name=....", with the string being defined up to a comma
 * or the end of the entire option string.
 */

void parse_opts(char *str) {
    while(*str) {
        /* Get option name */
        char *sp;
        for(sp = str; isalpha(*sp); sp++) {
            // keep going while a character.
        }
        auto len = (size_t)(sp - str);
        /* Look it up and deal with it */
        for(UI::OPTION *op = UI::optlist; op <= &UI::optlist[NUM_OPTS - 1]; op++) {
            if(eqstr(str, op->o_name, len)) {
                if(op->o_putfunc == put_bool) { /* if option is a boolean */
                    *(int *)op->o_opt = TRUE;
                } else { /* string option */
                    char *start;
                    /* Skip to start of string value */
                    for(str = sp + 1; *str == '='; str++) {
                        // skip past the =
                    }
                    if(*str == '~') {
                        strcpy((char *)op->o_opt, game.home);
                        start = (char *)op->o_opt + strlen(game.home);
                        while(*++str == '/') {
                            // skip the /'s
                        }
                    } else {
                        start = (char *)op->o_opt;
                    }
                    /* Skip to end of string value */
                    for(sp = str + 1; *sp && *sp != ','; sp++) {
                        // skip past the ,
                    }
                    strucpy(start, str, (size_t)(sp - str));
                }
                break;
            }
            /* check for "noname" for booleans */
            if(op->o_putfunc == put_bool && eqstr(str, "no", 2) &&
               eqstr(str + 2, op->o_name, len - 2)) {
                *(int *)op->o_opt = FALSE;
                break;
            }
        }

        /* skip to start of next option name */
        while(*sp && !isalpha(*sp)) {
            sp++;
        }
        str = sp;
    }
}

/* copy string using unctrl for things */
void strucpy(char *s1, char *s2, size_t len) {
    while(len--) {
        const char *sp = unctrl((chtype)*s2);
        strcpy(s1, sp);
        s1 += strlen(sp);
        s2++;
    }
    *s1 = '\0';
}
