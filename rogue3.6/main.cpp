/*
 * @(#)main.c     3.27 (Berkeley) 6/15/81
 *
 * Rogue: Exploring the Dungeons of Doom
 * Copyright (C) 1980, 1981 Michael Toy, Ken Arnold and Glenn Wichman
 * All rights reserved.
 *
 * See the file LICENSE.TXT for full copyright and licensing information.
 */

#include "rogue.h"
#include <csignal>

char Game::encstr[] =
    "\354\251\243\332A\201|\301\321p\210\251\327\"\257\365t\341%3\271^`~\203z{\341};"
    "\f\341\231\222e\234\351]\321";
char Game::version[] = "@(#)vers.c     3.6 (Berkeley) 4/21/81";

RogueDaemons daemons;
Floor floorMap;
Game game;
Rogue rogue;
UI ui;
Items items;

int main(int argc, char **argv) {
    md_init(MD_STRIP_CTRL_KEYPAD);

    /* check for print-score option */
    if(argc == 2 && strcmp(argv[1], "-s") == 0) {
        game.waswizard = TRUE;
        score(0, -1, 0);
        exit(0);
    }
    /* Check to see if he is a wizard */
    if(argc == 2 && strcmp(argv[1], "-w") == 0) {
        game.wizard = TRUE;
        argv++;
        argc--;
    }

    /* get home and options from environment */
    strcpy(game.home, md_gethomedir());

    if(strlen(game.home) > PATH_MAX - strlen("rogue.save") - 1) {
        *game.home = 0;
    }

    strcpy(game.save_file_name, game.home);
    strcat(game.save_file_name, "rogue.save");

    char *env;
    if((env = getenv("ROGUEOPTS")) != nullptr) {
        parse_opts(env);
    }
    if(env == nullptr || game.whoami[0] == '\0') {
        strucpy(game.whoami, md_getusername(), strlen(md_getusername()));
    }
    if(env == nullptr || game.fruit[0] == '\0') {
        strcpy(game.fruit, "slime-mold");
    }

    if(argc == 2) {
        if(!restore(argv[1])) { /* Note: restore will never return */
            exit(1);
        }
    }

    time_t now;
    time(&now);
    int lowtime = (int)now;

    env = getenv("SEED");

    if(env) {
        game.seed = atoi(env);
    } else {
        game.seed = 0;
    }

    if(game.seed > 0) {
        game.waswizard = 1; /* don't save scores if SEED specified */
        game.dnum = game.seed;
    } else {
        game.dnum = lowtime;
    }

    if(game.wizard || env) {
        printf("Hello %s, welcome to dungeon #%d", game.whoami, game.dnum);
    } else {
        printf("Hello %s, just a moment while I dig the dungeon...", game.whoami);
    }

    fflush(stdout);
    game.seed = game.dnum;
    rogue.init_player(); /* Roll up the rogue */
    init_things();       /* Set up probabilities of things */
    init_names();        /* Set up names of scrolls */
    init_colors();       /* Set up colors of potions */
    init_stones();       /* Set up stone settings of rings */
    init_materials();    /* Set up materials of wands */
    ui.initScr();

    if(COLS < 70) {
        endwin();
        printf("\n\nSorry, %s, but your terminal window has too few columns.\n", game.whoami);
        printf("Your terminal has %d columns, needs 70.\n", COLS);
        exit(1);
    }
    if(LINES < 22) {
        endwin();
        printf("\n\nSorry, %s, but your terminal window has too few lines.\n", game.whoami);
        printf("Your terminal has %d lines, needs 22.\n", LINES);
        exit(1);
    }

    setup();
    /* Set up windows */
    ui.cw.newWindow(LINES, COLS);
    ui.mw.newWindow(LINES, COLS);
    ui.hw.newWindow(LINES, COLS);
    keypad(ui.cw.w, true);
    game.waswizard = game.wizard;
    new_level(); /* Draw current level */

    /* Start up daemons and fuses */
    daemons.startDaemon(d_doctor, 0, RogueDaemons::After);
    daemons.startFuse(d_swander, 0, Game::WANDERTIME, RogueDaemons::After);
    daemons.startDaemon(d_stomach, 0, RogueDaemons::After);
    daemons.startDaemon(d_runners, 0, RogueDaemons::After);

    /* Give the rogue his weaponry. */
    linked_list *item;
    object *obj;

    /*  First a mace */
    item = new_item(sizeof *obj);
    obj = lobject(item);
    obj->o_type = WEAPON;
    obj->o_which = MACE;
    init_weapon(obj, MACE);
    obj->o_hplus = 1;
    obj->o_dplus = 1;
    obj->o_flags |= ISKNOW;
    add_pack(item, TRUE);
    rogue.cur_weapon = obj;

    /* Now a +1 bow */
    item = new_item(sizeof *obj);
    obj = lobject(item);
    obj->o_type = WEAPON;
    obj->o_which = BOW;
    init_weapon(obj, BOW);
    obj->o_hplus = 1;
    obj->o_dplus = 0;
    obj->o_flags |= ISKNOW;
    add_pack(item, TRUE);

    /* Now some arrows */
    item = new_item(sizeof *obj);
    obj = lobject(item);
    obj->o_type = WEAPON;
    obj->o_which = ARROW;
    init_weapon(obj, ARROW);
    obj->o_count = 25 + rnd(15);
    obj->o_hplus = obj->o_dplus = 0;
    obj->o_flags |= ISKNOW;
    add_pack(item, TRUE);

    /* And his suit of armor */
    item = new_item(sizeof *obj);
    obj = lobject(item);
    obj->o_type = ARMOR;
    obj->o_which = RING_MAIL;
    obj->o_ac = Items::a_class[RING_MAIL] - 1;
    obj->o_flags |= ISKNOW;
    rogue.cur_armor = obj;
    add_pack(item, TRUE);

    /* Give him some food too */
    item = new_item(sizeof *obj);
    obj = lobject(item);
    obj->o_type = FOOD;
    obj->o_count = 1;
    obj->o_which = 0;
    add_pack(item, TRUE);
    playit();
}

/*
 * endit:
 *     Exit the program abnormally.
 */

[[noreturn]] void endit(int p) {
    p = 0; // silence warning
    fatal("Ok, if you want to exit that badly, I'll have to allow it\n");
}

/*
 * fatal:
 *     Exit the program, printing a message.
 */

[[noreturn]] void fatal(const char *s) {
    clear();
    move(LINES - 2, 0);
    printw("%s", s);
    draw(ui.stdScr.w);
    endwin();
    exit(0);
}

/*
 * rnd:
 *     Pick a very random number.
 */

int rnd(const int range) {
    return range == 0 ? 0 : abs(game.RN()) % range;
}

size_t rnd(const size_t range) {
    return range == 0 ? 0 : (size_t)abs(game.RN()) % range;
}

/*
 * roll:
 *     roll a number of dice
 */

int roll(int number, const int sides) {
    int dtotal = 0;

    while(number--) {
        dtotal += rnd(sides) + 1;
    }
    return dtotal;
}

/* handle stop and start signals */
void tstp(int p) {
    p = 0; // silence warning
#ifdef SIGTSTP
    signal(SIGTSTP, SIG_IGN);
#endif
    mvcur(0, COLS - 1, LINES - 1, 0);
    endwin();
    fflush(stdout);
#ifdef SIGTSTP
    signal(SIGTSTP, SIG_DFL);
    kill(0, SIGTSTP);
    signal(SIGTSTP, tstp);
#endif
    crmode();
    noecho();
    clearok(ui.curScr.w, TRUE);
    touchwin(ui.cw.w);
    draw(ui.cw.w);
    flush_type(); /* flush input */
}

void setup() {
#ifdef SIGHUP
    signal(SIGHUP, auto_save);
#endif
    signal(SIGILL, auto_save);
#ifdef SIGTRAP
    signal(SIGTRAP, auto_save);
#endif
#ifdef SIGIOT
    signal(SIGIOT, auto_save);
#endif
#ifdef SIGEMT
    signal(SIGEMT, auto_save);
#endif
    signal(SIGFPE, auto_save);
#ifdef SIGBUS
    signal(SIGBUS, auto_save);
#endif
    signal(SIGSEGV, auto_save);
#ifdef SIGSYS
    signal(SIGSYS, auto_save);
#endif
#ifdef SIGPIPE
    signal(SIGPIPE, auto_save);
#endif
    signal(SIGTERM, auto_save);
    signal(SIGINT, quit);
#ifdef SIGQUIT
    signal(SIGQUIT, endit);
#endif
#ifdef SIGTSTP
    signal(SIGTSTP, tstp);
#endif

    crmode(); /* Cbreak mode */
    noecho(); /* Echo off */
}

/*
 * playit:
 *     The main loop of the program.  Loop until the game is over,
 * refreshing things and looking at the proper times.
 */

[[noreturn]] void playit() {
    char *opts;

    // FIXME: pointless
    /* set up defaults for slow terminals */
    if(baudrate() < 1200) {
        game.terse = TRUE;
        game.jump = TRUE;
    }

    /* parse environment declaration of options */
    if((opts = getenv("ROGUEOPTS")) != nullptr) {
        parse_opts(opts);
    }

    game.oldpos = rogue.hero();
    game.oldrp = roomin(rogue.hero());
    // FIXME: this is pointless.
    int playing = TRUE; /* True until he quits */
    while(playing) {
        command(); /* Command execution */
    }
    endit(-1);
}
