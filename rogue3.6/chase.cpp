/*
 * Code for one object to chase another
 *
 * @(#)chase.c     3.17 (Berkeley) 6/15/81
 *
 * Rogue: Exploring the Dungeons of Doom
 * Copyright (C) 1980, 1981 Michael Toy, Ken Arnold and Glenn Wichman
 * All rights reserved.
 *
 * See the file LICENSE.TXT for full copyright and licensing information.
 */

#include "rogue.h"

/*
 * runners:
 *     Make all the running monsters move.
 */

void runners() {
    for(linked_list *item = floorMap.mlist; item != nullptr;) {
        auto *tp = lthing(item);
        item = next_p(item);
        if(off(*tp, ISHELD) && on(*tp, ISRUN)) {
            if(off(*tp, ISSLOW) || tp->t_turn) {
                if(do_chase(tp) == -1) {
                    continue;
                }
            }
            if(on(*tp, ISHASTE)) {
                if(do_chase(tp) == -1) {
                    continue;
                }
            }
            tp->t_turn ^= TRUE;
        }
    }
}

/*
 * do_chase:
 *     Make one thing chase another.
 */

int do_chase(thing *th) {
    const coord th_dest = th->t_dest == hero_pos ? rogue.hero() : th->t_dest;
    room *rer = roomin(th->t_pos); /* Find room of chaser */
    room *ree = roomin(th_dest);   /* Find room of chasee */
    /*
     * We don't count doors as inside rooms for this routine
     */
    if(mvwinch(ui.stdScr.w, th->t_pos.y, th->t_pos.x) == DOOR) {
        rer = nullptr;
    }

    coord cthis = th_dest; /* Temporary destination for chaser */
    int mindist = 32767;
    /*
     * If the object of our desire is in a different room,
     * than we are and we ar not in a corridor, run to the
     * door nearest to our goal.
     */
    if(rer != nullptr && rer != ree) {
        for(auto &exit : rer->r_exit) {
            int dist = distance(th_dest.y, th_dest.x, exit.y, exit.x);
            if(dist < mindist) { /* minimize distance */
                cthis = exit;
                mindist = dist;
            }
        }
    }

    /*
     * this now contains what we want to run to this time
     * so we run to it.  If we hit it we either want to fight it
     * or stop running
     */
    int stoprun = FALSE; /* TRUE means we are there */
    if(!chase(th, cthis)) {
        if(cthis == rogue.hero()) {
            return (attack(th));
        }
        if(th->t_type != 'F') {
            stoprun = TRUE;
        }
    } else if(th->t_type == 'F') {
        return (0);
    }
    mvwaddch_int(ui.cw.w, th->t_pos.y, th->t_pos.x, th->t_oldch);
    int sch = (int)mvwinch(ui.cw.w, game.ch_ret.y, game.ch_ret.x);
    if(rer != nullptr && (rer->r_flags & ISDARK) && sch == FLOOR &&
       distance(game.ch_ret.y, game.ch_ret.x, th->t_pos.y, th->t_pos.x) < 3 &&
       off(rogue.player, ISBLIND)) {
        th->t_oldch = ' ';
    } else {
        th->t_oldch = sch;
    }

    if(cansee(unc(game.ch_ret)) && !on(*th, ISINVIS)) {
        mvwaddch_int(ui.cw.w, game.ch_ret.y, game.ch_ret.x, th->t_type);
    }
    ui.mw.moveThingTo(th, game.ch_ret);
    th->t_pos = game.ch_ret;
    /*
     * And stop running if need be
     */
    if(stoprun && (th->t_pos == th_dest)) {
        th->t_flags &= ~ISRUN;
    }

    return (0);
}

/*
 * runto:
 *     Set a mosnter running after something
 *     or stop it from running (for when it dies)
 */

void runto(coord &runner, const coord &spot) {
    /* If we couldn't find him, something is funny */
    linked_list *item;
    if((item = find_mons(runner.y, runner.x)) == nullptr) {
        msg("CHASER '%s'", unctrl(ui.winat(runner.y, runner.x)));
        return;
    }
    auto *tp = lthing(item);
    /* Start the beastie running */
    tp->t_dest = spot;
    tp->t_flags |= ISRUN;
    tp->t_flags &= ~ISHELD;
}

/*
 * chase:
 *     Find the spot for the chaser(er) to move closer to the
 *     chasee(ee).  Returns TRUE if we want to keep on chasing later
 *     FALSE if we reach the goal.
 */

int chase(thing *tp, const coord &ee) {
    int dist = 0;

    /*
     * If the thing is confused, let it move randomly. Invisible
     * Stalkers are slightly confused all of the time, and bats are
     * quite confused all the time
     */
    if((on(*tp, ISHUH) && rnd(10) < 8) || (tp->t_type == 'I' && rnd(100) < 20) ||
       (tp->t_type == 'B' && rnd(100) < 50)) {
        /* get a valid random move */
        game.ch_ret = rndmove(tp);
        dist = distance(game.ch_ret.y, game.ch_ret.x, ee.y, ee.x);
        /* Small chance that it will become un-confused */
        if(rnd(1000) < 50) {
            tp->t_flags &= ~ISHUH;
        }
    }
    /*
     * Otherwise, find the empty spot next to the chaser that is
     * closest to the chasee.
     */
    else {
        const coord er = tp->t_pos;
        /*
         * This will eventually hold where we move to get closer
         * If we can't find an empty spot, we stay where we are.
         */
        dist = distance(er.y, er.x, ee.y, ee.x);
        game.ch_ret = er;

        int ey = er.y + 1;
        int ex = er.x + 1;
        for(int x = er.x - 1; x <= ex; x++) {
            for(int y = er.y - 1; y <= ey; y++) {
                coord tryp;

                tryp.x = x;
                tryp.y = y;
                if(!diag_ok(er, tryp)) {
                    continue;
                }
                int ch = (int)ui.winat(y, x);
                if(step_ok(ch)) {
                    /*
                     * If it is a scroll, it might be a scare monster scroll
                     * so we need to look it up to see what type it is.
                     */
                    if(ch == SCROLL) {
                        linked_list *item;
                        object *obj = nullptr;
                        for(item = floorMap.lvl_obj; item != nullptr; item = next_p(item)) {
                            obj = lobject(item);
                            if(y == obj->o_pos.y && x == obj->o_pos.x) {
                                break;
                            }
                        }
                        assert(obj != nullptr);
                        if(item != nullptr && obj->o_which == S_SCARE) {
                            continue;
                        }
                    }
                    /*
                     * If we didn't find any scrolls at this place or it
                     * wasn't a scare scroll, then this place counts
                     */
                    int thisdist = distance(y, x, ee.y, ee.x);
                    if(thisdist < dist) {
                        game.ch_ret = tryp;
                        dist = thisdist;
                    }
                }
            }
        }
    }
    return (dist != 0);
}

/*
 * roomin:
 *     Find what room some coordinates are in. NULL means they aren't
 *     in any room.
 */

room *roomin(const coord &cp) {
    assert(cp != hero_pos);
    assert(cp != coord{});
    for(auto &rp : floorMap.rooms) {
        if(inroom(rp, cp)) {
            return &rp;
        }
    }
    return nullptr;
}

/*
 * find_mons:
 *     Find the monster from his corrdinates
 */

linked_list *find_mons(const int y, const int x) {
    for(linked_list *item = floorMap.mlist; item != nullptr; item = next_p(item)) {
        auto *th = lthing(item);
        if(th->t_pos.y == y && th->t_pos.x == x) {
            return item;
        }
    }
    return nullptr;
}

/*
 * diag_ok:
 *     Check to see if the move is legal if it is diagonal
 */

int diag_ok(const coord &sp, const coord &ep) {
    assert(sp != hero_pos);
    assert(sp != coord{});
    assert(ep != hero_pos);
    assert(ep != coord{});
    if(ep.x == sp.x || ep.y == sp.y) {
        return TRUE;
    }
    return (step_ok((int)mvinch(ep.y, sp.x)) && step_ok((int)mvinch(sp.y, ep.x)));
}

/*
 * cansee:
 *     returns true if the hero can see a certain coordinate.
 */

int cansee(const int y, const int x) {
    if(on(rogue.player, ISBLIND)) {
        return FALSE;
    }

    coord tp;
    tp.y = y;
    tp.x = x;
    const room *rer = roomin(tp);
    /*
     * We can only see if the hero in the same room as
     * the coordinate and the room is lit or if it is close.
     */
    return (rer != nullptr && rer == roomin(rogue.hero()) && !(rer->r_flags & ISDARK)) ||
           distance(y, x, rogue.hero().y, rogue.hero().x) < 3;
}
