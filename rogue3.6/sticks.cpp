/*
 * Functions to implement the various sticks one might find
 * while wandering around the dungeon.
 *
 * @(#)sticks.c     3.14 (Berkeley) 6/15/81
 *
 * Rogue: Exploring the Dungeons of Doom
 * Copyright (C) 1980, 1981 Michael Toy, Ken Arnold and Glenn Wichman
 * All rights reserved.
 *
 * See the file LICENSE.TXT for full copyright and licensing information.
 */

#include "rogue.h"

void fix_stick(object *cur) {
    if(items.ws_type[(size_t)cur->o_which] == "staff") {
        strcpy(cur->o_damage, "2d3");
    } else {
        strcpy(cur->o_damage, "1d1");
    }
    strcpy(cur->o_hurldmg, "1d1");

    cur->o_charges = 3 + rnd(5);
    switch(cur->o_which) {
        case WS_HIT:
            cur->o_hplus = 3;
            cur->o_dplus = 3;
            strcpy(cur->o_damage, "1d8");
            break;
        case WS_LIGHT:
            cur->o_charges = 10 + rnd(10);
    }
}

void do_zap(const int gotdir) {
    linked_list *item;
    if((item = get_item("zap with", STICK)) == nullptr) {
        return;
    }
    auto *obj = lobject(item);
    if(obj->o_type != STICK) {
        msg("You can't zap with that!");
        game.after = FALSE;
        return;
    }
    if(obj->o_charges == 0) {
        msg("Nothing happens.");
        return;
    }
    if(!gotdir) {
        do {
            game.delta.y = rnd(3) - 1;
            game.delta.x = rnd(3) - 1;
        } while(game.delta.y == 0 && game.delta.x == 0);
    }
    switch(obj->o_which) {
        case WS_LIGHT: {
            /*
             * Reddy Kilowat wand.  Light up the room
             */
            items.ws_know[WS_LIGHT] = TRUE;
            room *rp;
            if((rp = roomin(rogue.hero())) == nullptr) {
                msg("The corridor glows and then fades");
            } else {
                addmsg("The room is lit");
                if(!game.terse) {
                    addmsg(" by a shimmering blue light.");
                }
                endmsg();
                rp->r_flags &= ~ISDARK;
                /*
                 * Light the room and put the player back up
                 */
                light(rogue.hero());
                mvwaddch(ui.cw.w, rogue.hero().y, rogue.hero().x, PLAYER);
            }
        } break;
        case WS_DRAIN: {
            /*
             * Take away 1/2 of hero's hit points, then take it away
             * evenly from the monsters in the room (or next to hero
             * if he is in a passage)
             */
            room *rp;
            if(rogue.pstats().s_hpt < 2) {
                msg("You are too weak to use it.");
                return;
            }
            if((rp = roomin(rogue.hero())) == nullptr) {
                drain(rogue.hero().y - 1, rogue.hero().y + 1, rogue.hero().x - 1,
                      rogue.hero().x + 1);
            } else {
                drain(rp->r_pos.y, rp->r_pos.y + rp->r_max.y, rp->r_pos.x,
                      rp->r_pos.x + rp->r_max.x);
            }
        } break;
        case WS_POLYMORPH:
        case WS_TELAWAY:
        case WS_TELTO:
        case WS_CANCEL: {
            int y = rogue.hero().y;
            int x = rogue.hero().x;
            while(step_ok((int)ui.winat(y, x))) {
                y += game.delta.y;
                x += game.delta.x;
            }
            int monster;
            if(isupper(monster = (int)mvwinch(ui.mw.w, y, x))) {
                int omonst = monster;

                if(monster == 'F') {
                    rogue.player.t_flags &= ~ISHELD;
                }
                item = find_mons(y, x);
                auto *tp = lthing(item);
                if(obj->o_which == WS_POLYMORPH) {
                    detach(floorMap.mlist, item);
                    int oldch = tp->t_oldch;
                    game.delta.y = y;
                    game.delta.x = x;
                    new_monster(item, monster = rnd(26) + 'A', game.delta);
                    if(!(tp->t_flags & ISRUN)) {
                        runto(game.delta, hero_pos);
                    }
                    if(isupper((int)mvwinch(ui.cw.w, y, x))) {
                        mvwaddch_int(ui.cw.w, y, x, monster);
                    }
                    tp->t_oldch = oldch;
                    items.ws_know[WS_POLYMORPH] |= (monster != omonst);
                } else if(obj->o_which == WS_CANCEL) {
                    tp->t_flags |= ISCANC;
                    tp->t_flags &= ~ISINVIS;
                } else {
                    if(obj->o_which == WS_TELAWAY) {
                        do {
                            int rm = rnd_room();
                            rnd_pos(&floorMap.rooms[rm], tp->t_pos);
                        } while(ui.winat(tp->t_pos.y, tp->t_pos.x) != FLOOR);
                    } else {
                        tp->t_pos.y = rogue.hero().y + game.delta.y;
                        tp->t_pos.x = rogue.hero().x + game.delta.x;
                    }
                    if(isupper((int)mvwinch(ui.cw.w, y, x))) {
                        mvwaddch_int(ui.cw.w, y, x, tp->t_oldch);
                    }
                    tp->t_dest = hero_pos;
                    tp->t_flags |= ISRUN;
                    mvwaddch(ui.mw.w, y, x, ' ');
                    mvwaddch_int(ui.mw.w, tp->t_pos.y, tp->t_pos.x, monster);
                    if(tp->t_pos.y != y || tp->t_pos.x != x) {
                        tp->t_oldch = (int)mvwinch(ui.cw.w, tp->t_pos.y, tp->t_pos.x);
                    }
                }
            }
        } break;
        case WS_MISSILE: {
            static object bolt = {'*', {0, 0}, 0, "", "1d4", 0, 0, 100, 1, {0}, 0, 0};

            do_motion(&bolt, game.delta.y, game.delta.x);
            if(isupper((int)mvwinch(ui.mw.w, bolt.o_pos.y, bolt.o_pos.x)) &&
               !save_throw(VS_MAGIC, THINGPTR(find_mons(unc(bolt.o_pos))))) {
                hit_monster(unc(bolt.o_pos), &bolt);
            } else if(game.terse) {
                msg("Missile vanishes");
            } else {
                msg("The missile vanishes with a puff of smoke");
            }
            items.ws_know[WS_MISSILE] = TRUE;
        } break;
        case WS_HIT: {
            game.delta.y += rogue.hero().y;
            game.delta.x += rogue.hero().x;
            int ch = (int)ui.winat(game.delta.y, game.delta.x);
            if(isupper(ch)) {
                if(rnd(20) == 0) {
                    strcpy(obj->o_damage, "3d8");
                    obj->o_dplus = 9;
                } else {
                    strcpy(obj->o_damage, "1d8");
                    obj->o_dplus = 3;
                }
                fight(game.delta, ch, obj, FALSE);
            }
        } break;
        case WS_HASTE_M:
        case WS_SLOW_M: {
            int y = rogue.hero().y;
            int x = rogue.hero().x;
            while(step_ok((int)ui.winat(y, x))) {
                y += game.delta.y;
                x += game.delta.x;
            }
            if(isupper((int)mvwinch(ui.mw.w, y, x))) {
                item = find_mons(y, x);
                auto *tp = lthing(item);
                if(obj->o_which == WS_HASTE_M) {
                    if(on(*tp, ISSLOW)) {
                        tp->t_flags &= ~ISSLOW;
                    } else {
                        tp->t_flags |= ISHASTE;
                    }
                } else {
                    if(on(*tp, ISHASTE)) {
                        tp->t_flags &= ~ISHASTE;
                    } else {
                        tp->t_flags |= ISSLOW;
                    }
                    tp->t_turn = TRUE;
                }
                game.delta.y = y;
                game.delta.x = x;
                runto(game.delta, hero_pos);
            }
        } break;
        case WS_ELECT:
        case WS_FIRE:
        case WS_COLD: {
            static object bolt = {'*', {0, 0}, 0, "", "6d6", 0, 0, 100, 0, {0}, 0, 0};

            int dirch = '\0';
            switch(game.delta.y + game.delta.x) {
                case 0:
                    dirch = '/';
                    break;
                case 1:
                case -1:
                    dirch = (game.delta.y == 0 ? '-' : '|');
                    break;
                case 2:
                case -2:
                    dirch = '\\';
            }
            coord pos = rogue.hero();
            int bounced = FALSE;
            int used = FALSE;
            const char *name;
            if(obj->o_which == WS_ELECT) {
                name = "bolt";
            } else if(obj->o_which == WS_FIRE) {
                name = "flame";
            } else {
                name = "ice";
            }
            int y;
            coord spotpos[Game::BOLT_LENGTH];
            for(y = 0; y < Game::BOLT_LENGTH && !used; y++) {
                int ch = (int)ui.winat(pos.y, pos.x);
                spotpos[y] = pos;
                switch(ch) {
                    case DOOR:
                    case SECRETDOOR:
                    case '|':
                    case '-':
                    case ' ':
                        bounced = TRUE;
                        game.delta.y = -game.delta.y;
                        game.delta.x = -game.delta.x;
                        y--;
                        msg("The bolt bounces");
                        break;
                    default:
                        if(!bounced && isupper(ch)) {
                            if(!save_throw(VS_MAGIC, THINGPTR(find_mons(unc(pos))))) {
                                bolt.o_pos = pos;
                                hit_monster(unc(pos), &bolt);
                                used = TRUE;
                            } else if(ch != 'M' || show(pos.y, pos.x) == 'M') {
                                if(game.terse) {
                                    msg("%s misses", name);
                                } else {
                                    msg("The %s whizzes past the %s", name,
                                        Game::monsters[(size_t)ch - 'A'].m_name);
                                }
                                runto(pos, hero_pos);
                            }
                        } else if(bounced && pos.y == rogue.hero().y && pos.x == rogue.hero().x) {
                            bounced = FALSE;
                            if(!save(VS_MAGIC)) {
                                if(game.terse) {
                                    msg("The %s hits", name);
                                } else {
                                    msg("You are hit by the %s", name);
                                }
                                if((rogue.pstats().s_hpt -= roll(6, 6)) <= 0) {
                                    death('b');
                                }
                                used = TRUE;
                            } else {
                                msg("The %s whizzes by you", name);
                            }
                        }
                        mvwaddch_int(ui.cw.w, pos.y, pos.x, dirch);
                        draw(ui.cw.w);
                }
                pos.y += game.delta.y;
                pos.x += game.delta.x;
            }
            for(int x = 0; x < y; x++) {
                mvwaddch_int(ui.cw.w, spotpos[x].y, spotpos[x].x, show(spotpos[x].y, spotpos[x].x));
            }
            items.ws_know[(size_t)obj->o_which] = TRUE;
        } break;
        case WS_NOP:
            break;
        default:
            msg("What a bizarre schtick!");
    }
    obj->o_charges--;
}

/*
 * drain:
 *     Do drain hit points from player shtick
 */

void drain(int ymin, int ymax, int xmin, int xmax) {
    /* First count how many things we need to spread the hit points among */
    int cnt = 0;
    for(int i = ymin; i <= ymax; i++) {
        for(int j = xmin; j <= xmax; j++) {
            if(isupper((int)mvwinch(ui.mw.w, i, j))) {
                cnt++;
            }
        }
    }
    if(cnt == 0) {
        msg("You have a tingling feeling");
        return;
    }
    cnt = rogue.pstats().s_hpt / cnt;
    rogue.pstats().s_hpt /= 2;
    /* Now zot all of the monsters */
    for(int i = ymin; i <= ymax; i++) {
        for(int j = xmin; j <= xmax; j++) {
            linked_list *item;
            if(isupper((int)mvwinch(ui.mw.w, i, j)) && ((item = find_mons(i, j)) != nullptr)) {
                auto *ick = lthing(item);
                if((ick->t_stats.s_hpt -= cnt) < 1) {
                    killed(item, cansee(i, j) && !on(*ick, ISINVIS));
                }
            }
        }
    }
}

/* charge a wand for wizards. */
char *charge_str(const object *obj) {
    static char buf[20];

    if(!(obj->o_flags & ISKNOW)) {
        buf[0] = '\0';
    } else if(game.terse) {
        sprintf(buf, " [%d]", obj->o_charges);
    } else {
        sprintf(buf, " [%d charges]", obj->o_charges);
    }
    return buf;
}
