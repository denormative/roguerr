/*
 * Contains functions for dealing with things like
 * potions and scrolls
 *
 * @(#)things.c     3.37 (Berkeley) 6/15/81
 *
 * Rogue: Exploring the Dungeons of Doom
 * Copyright (C) 1980, 1981 Michael Toy, Ken Arnold and Glenn Wichman
 * All rights reserved.
 *
 * See the file LICENSE.TXT for full copyright and licensing information.
 */

#include "rogue.h"

/*
 * inv_name:
 *     return the name of something as it would appear in an
 *     inventory.
 */
char *inv_name(const object *obj, const int drop) {
    char *pb;

    switch(obj->o_type) {
        case SCROLL:
            if(obj->o_count == 1) {
                strcpy(ui.prbuf, "A scroll ");
            } else {
                sprintf(ui.prbuf, "%d scrolls ", obj->o_count);
            }
            pb = &ui.prbuf[strlen(ui.prbuf)];
            if(items.s_know[(size_t)obj->o_which]) {
                sprintf(pb, "of %s", Items::s_magic[(size_t)obj->o_which].mi_name.c_str());
            } else if(!items.s_guess[(size_t)obj->o_which].empty()) {
                sprintf(pb, "called %s", items.s_guess[(size_t)obj->o_which].c_str());
            } else {
                sprintf(pb, "titled '%s'", items.s_names[(size_t)obj->o_which].c_str());
            }
            break;
        case POTION:
            if(obj->o_count == 1) {
                strcpy(ui.prbuf, "A potion ");
            } else {
                sprintf(ui.prbuf, "%d potions ", obj->o_count);
            }
            pb = &ui.prbuf[strlen(ui.prbuf)];
            if(items.p_know[(size_t)obj->o_which]) {
                sprintf(pb, "of %s(%s)", Items::p_magic[(size_t)obj->o_which].mi_name.c_str(),
                        items.p_colors[(size_t)obj->o_which].c_str());
            } else if(!items.p_guess[(size_t)obj->o_which].empty()) {
                sprintf(pb, "called %s(%s)", items.p_guess[(size_t)obj->o_which].c_str(),
                        items.p_colors[(size_t)obj->o_which].c_str());
            } else if(obj->o_count == 1) {
                sprintf(ui.prbuf, "A%s %s potion",
                        vowelstr(items.p_colors[(size_t)obj->o_which]).c_str(),
                        items.p_colors[(size_t)obj->o_which].c_str());
            } else {
                sprintf(ui.prbuf, "%d %s potions", obj->o_count,
                        items.p_colors[(size_t)obj->o_which].c_str());
            }
            break;
        case FOOD:
            if(obj->o_which == 1) {
                if(obj->o_count == 1) {
                    sprintf(ui.prbuf, "A%s %s", vowelstr(game.fruit).c_str(), game.fruit);
                } else {
                    sprintf(ui.prbuf, "%d %ss", obj->o_count, game.fruit);
                }
            } else if(obj->o_count == 1) {
                strcpy(ui.prbuf, "Some food");
            } else {
                sprintf(ui.prbuf, "%d rations of food", obj->o_count);
            }
            break;
        case WEAPON:
            if(obj->o_count > 1) {
                sprintf(ui.prbuf, "%d ", obj->o_count);
            } else {
                strcpy(ui.prbuf, "A ");
            }
            pb = &ui.prbuf[strlen(ui.prbuf)];
            if(obj->o_flags & ISKNOW) {
                sprintf(pb, "%s %s", num(obj->o_hplus, obj->o_dplus),
                        Items::w_names[(size_t)obj->o_which].c_str());
            } else {
                sprintf(pb, "%s", Items::w_names[(size_t)obj->o_which].c_str());
            }
            if(obj->o_count > 1) {
                strcat(ui.prbuf, "s");
            }
            break;
        case ARMOR:
            if(obj->o_flags & ISKNOW) {
                sprintf(ui.prbuf, "%s %s", num(Items::a_class[(size_t)obj->o_which] - obj->o_ac, 0),
                        Items::a_names[(size_t)obj->o_which].c_str());
            } else {
                sprintf(ui.prbuf, "%s", Items::a_names[(size_t)obj->o_which].c_str());
            }
            break;
        case AMULET:
            strcpy(ui.prbuf, "The Amulet of Yendor");
            break;
        case STICK:
            sprintf(ui.prbuf, "A %s ", items.ws_type[(size_t)obj->o_which].c_str());
            pb = &ui.prbuf[strlen(ui.prbuf)];
            if(items.ws_know[(size_t)obj->o_which]) {
                sprintf(pb, "of %s%s(%s)", Items::ws_magic[(size_t)obj->o_which].mi_name.c_str(),
                        charge_str(obj), items.ws_made[(size_t)obj->o_which].c_str());
            } else if(!items.ws_guess[(size_t)obj->o_which].empty()) {
                sprintf(pb, "called %s(%s)", items.ws_guess[(size_t)obj->o_which].c_str(),
                        items.ws_made[(size_t)obj->o_which].c_str());
            } else {
                sprintf(&ui.prbuf[2], "%s %s", items.ws_made[(size_t)obj->o_which].c_str(),
                        items.ws_type[(size_t)obj->o_which].c_str());
            }
            break;
        case RING:
            if(items.r_know[(size_t)obj->o_which]) {
                sprintf(ui.prbuf, "A%s ring of %s(%s)", ring_num(obj),
                        Items::r_magic[(size_t)obj->o_which].mi_name.c_str(),
                        items.r_stones[(size_t)obj->o_which].c_str());
            } else if(!items.r_guess[(size_t)obj->o_which].empty()) {
                sprintf(ui.prbuf, "A ring called %s(%s)",
                        items.r_guess[(size_t)obj->o_which].c_str(),
                        items.r_stones[(size_t)obj->o_which].c_str());
            } else {
                sprintf(ui.prbuf, "A%s %s ring",
                        vowelstr(items.r_stones[(size_t)obj->o_which]).c_str(),
                        items.r_stones[(size_t)obj->o_which].c_str());
            }
            break;
        default:
            debug("Picked up something funny");
            sprintf(ui.prbuf, "Something bizarre %s", unctrl_int(obj->o_type));
    }
    if(obj == rogue.cur_armor) {
        strcat(ui.prbuf, " (being worn)");
    }
    if(obj == rogue.cur_weapon) {
        strcat(ui.prbuf, " (weapon in hand)");
    }
    if(obj == rogue.cur_ring[Rogue::LEFT]) {
        strcat(ui.prbuf, " (on left hand)");
    } else if(obj == rogue.cur_ring[Rogue::RIGHT]) {
        strcat(ui.prbuf, " (on right hand)");
    }
    if(drop && isupper(ui.prbuf[0])) {
        ui.prbuf[0] = (char)tolower(ui.prbuf[0]);
    } else if(!drop && islower(*ui.prbuf)) {
        *ui.prbuf = (char)toupper(*ui.prbuf);
    }
    if(!drop) {
        strcat(ui.prbuf, ".");
    }
    return ui.prbuf;
}

/*
 * money:
 *     Add to characters purse
 */
void money() {
    for(room *rp = floorMap.rooms; rp <= &floorMap.rooms[Floor::MAXROOMS - 1]; rp++) {
        if(rogue.hero() == rp->r_gold) {
            if(game.notify) {
                if(!game.terse) {
                    addmsg("You found ");
                }
                msg("%d gold pieces.", rp->r_goldval);
            }
            rogue.purse += rp->r_goldval;
            rp->r_goldval = 0;
            cmov(rp->r_gold);
            addch(FLOOR);
            return;
        }
    }
    msg("That gold must have been counterfeit");
}

/*
 * drop:
 *     put something down
 */
void drop() {
    int ch = (int)mvwinch(ui.stdScr.w, rogue.hero().y, rogue.hero().x);
    if(ch != FLOOR && ch != PASSAGE) {
        msg("There is something there already");
        return;
    }
    linked_list *obj;
    if((obj = get_item("drop", 0)) == nullptr) {
        return;
    }
    auto *op = lobject(obj);
    if(!dropcheck(op)) {
        return;
    }
    /*
     * Take it out of the pack
     */
    if(op->o_count >= 2 && op->o_type != WEAPON) {
        linked_list *nobj = new_item(sizeof *op);
        op->o_count--;
        op = lobject(nobj);
        *op = *(lobject(obj));
        op->o_count = 1;
        obj = nobj;
        if(op->o_group != 0) {
            rogue.inpack++;
        }
    } else {
        detach(rogue.pack(), obj);
    }
    rogue.inpack--;
    /*
     * Link it into the level object list
     */
    attach(floorMap.lvl_obj, obj);
    mvaddch_int(rogue.hero().y, rogue.hero().x, op->o_type);
    op->o_pos = rogue.hero();
    msg("Dropped %s", inv_name(op, TRUE));
}

/*
 * do special checks for dropping or unweilding|unwearing|unringing
 */
int dropcheck(const object *op) {
    if(op == nullptr) {
        return TRUE;
    }
    if(op != rogue.cur_armor && op != rogue.cur_weapon && op != rogue.cur_ring[Rogue::LEFT] &&
       op != rogue.cur_ring[Rogue::RIGHT]) {
        return TRUE;
    }
    if(op->o_flags & ISCURSED) {
        msg("You can't.  It appears to be cursed.");
        return FALSE;
    }
    if(op == rogue.cur_weapon) {
        rogue.cur_weapon = nullptr;
    } else if(op == rogue.cur_armor) {
        daemons.waste_time();
        rogue.cur_armor = nullptr;
    } else if(op == rogue.cur_ring[Rogue::LEFT] || op == rogue.cur_ring[Rogue::RIGHT]) {
        switch(op->o_which) {
            case R_ADDSTR: {
                str_t save_max = rogue.max_stats.s_str;
                chg_str(-op->o_ac);
                rogue.max_stats.s_str = save_max;
            } break;
            case R_SEEINVIS:
                rogue.player.t_flags &= ~CANSEE;
                daemons.extinguish(d_unsee);
                light(rogue.hero());
                mvwaddch(ui.cw.w, rogue.hero().y, rogue.hero().x, PLAYER);
                break;
        }
        rogue.cur_ring[op == rogue.cur_ring[Rogue::LEFT] ? Rogue::LEFT : Rogue::RIGHT] = nullptr;
    }
    return TRUE;
}

/* pick an item out of a list of nitems possible magic items */
template <size_t T>
int pick_one(array<magic_item, T> &magic, const int nitems) {
    int chance = rnd(100);
    int i = 0;
    for(i = 0; i < nitems; i++) {
        if(chance < magic[(size_t)i].mi_prob) {
            return i;
        }
    }

    if(game.wizard) {
        msg("bad pick_one: %d from %d items", chance, nitems);
        for(i = 0; i < nitems; i++) {
            msg("%s: %d%%", magic[(size_t)i].mi_name.c_str(), magic[(size_t)i].mi_prob);
        }
    }
    // FIXME: this should error
    return nitems - 1;
}

/*
 * return a new thing
 */
linked_list *new_thing() {
    object *cur;
    linked_list *item = new_item(sizeof *cur);
    cur = lobject(item);
    cur->o_hplus = cur->o_dplus = 0;
    strcpy(cur->o_damage, "0d0");
    strcpy(cur->o_hurldmg, "0d0");
    cur->o_ac = 11;
    cur->o_count = 1;
    cur->o_group = 0;
    cur->o_flags = 0;
    /*
     * Decide what kind of object it will be
     * If we haven't had food for a while, let it be food.
     */
    switch(game.no_food > 3 ? 2 : pick_one(Items::things, Items::NUMTHINGS)) {
        case 0:
            cur->o_type = POTION;
            cur->o_which = pick_one(Items::p_magic, MAXPOTIONS);
            break;
        case 1:
            cur->o_type = SCROLL;
            cur->o_which = pick_one(Items::s_magic, MAXSCROLLS);
            break;
        case 2:
            game.no_food = 0;
            cur->o_type = FOOD;
            if(rnd(100) > 10) {
                cur->o_which = 0;
            } else {
                cur->o_which = 1;
            }
            break;
        case 3: {
            cur->o_type = WEAPON;
            cur->o_which = rnd(MAXWEAPONS);
            init_weapon(cur, cur->o_which);
            int k;
            if((k = rnd(100)) < 10) {
                cur->o_flags |= ISCURSED;
                cur->o_hplus -= rnd(3) + 1;
            } else if(k < 15) {
                cur->o_hplus += rnd(3) + 1;
            }
        } break;
        case 4:
            cur->o_type = ARMOR;
            size_t j;
            int k;
            for(j = 0, k = rnd(100); j < MAXARMORS; j++) {
                if(k < Items::a_chances[j]) {
                    break;
                }
            }
            if(j == MAXARMORS) {
                debug("Picked a bad armor %d", k);
                j = 0;
            }
            cur->o_which = (int)j;
            cur->o_ac = Items::a_class[j];
            if((k = rnd(100)) < 20) {
                cur->o_flags |= ISCURSED;
                cur->o_ac += rnd(3) + 1;
            } else if(k < 28) {
                cur->o_ac -= rnd(3) + 1;
            }
            break;
        case 5:
            cur->o_type = RING;
            cur->o_which = pick_one(Items::r_magic, MAXRINGS);
            switch(cur->o_which) {
                case R_ADDSTR:
                case R_PROTECT:
                case R_ADDHIT:
                case R_ADDDAM:
                    if((cur->o_ac = rnd(3)) == 0) {
                        cur->o_ac = -1;
                        cur->o_flags |= ISCURSED;
                    }
                    break;
                case R_AGGR:
                case R_TELEPORT:
                    cur->o_flags |= ISCURSED;
            }
            break;
        case 6:
            cur->o_type = STICK;
            cur->o_which = pick_one(Items::ws_magic, MAXSTICKS);
            fix_stick(cur);
            break;
        default:
            debug("Picked a bad kind of object");
            wait_for(ui.stdScr.w, ' ');
    }
    return item;
}
