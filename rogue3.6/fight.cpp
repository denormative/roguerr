/*
 * All the fighting gets done here
 *
 * @(#)fight.c     3.28 (Berkeley) 6/15/81
 *
 * Rogue: Exploring the Dungeons of Doom
 * Copyright (C) 1980, 1981 Michael Toy, Ken Arnold and Glenn Wichman
 * All rights reserved.
 *
 * See the file LICENSE.TXT for full copyright and licensing information.
 */

#include "rogue.h"
#include <algorithm>

array<int, 20> Game::e_levels{10,     20,     40,     80,      160,     320,   640,
                              1280,   2560,   5120,   10240,   20480,   40920, 81920,
                              163840, 327680, 655360, 1310720, 2621440, 0};

/*
 * fight:
 *     The player attacks the monster.
 */

int fight(coord &mp, const int mn, object *weap, int thrown) {
    int did_hit = TRUE;

    /*
     * Find the monster we want to fight
     */
    linked_list *item;
    if((item = find_mons(mp.y, mp.x)) == nullptr) {
        debug("Fight what @ %d,%d", mp.y, mp.x);
        return (0);
    }
    auto *tp = lthing(item);
    /* Since we are fighting, things are not quiet so no healing takes place. */
    game.quiet = 0;
    runto(mp, hero_pos);
    /* Let him know it was really a mimic (if it was one). */
    if(tp->t_type == 'M' && tp->t_disguise != 'M' && off(rogue.player, ISBLIND)) {
        msg("Wait! That's a mimic!");
        tp->t_disguise = 'M';
        did_hit = thrown;
    }
    if(did_hit) {
        const char *mname;

        did_hit = FALSE;
        if(on(rogue.player, ISBLIND)) {
            mname = "it";
        } else {
            mname = Game::monsters[(size_t)mn - 'A'].m_name;
        }
        if(roll_em(&rogue.pstats(), &tp->t_stats, weap, thrown)) {
            did_hit = TRUE;
            if(thrown) {
                thunk(weap, mname);
            } else {
                hit(nullptr, mname);
            }
            if(on(rogue.player, CANHUH)) {
                msg("Your hands stop glowing red");
                msg("The %s appears confused.", mname);
                tp->t_flags |= ISHUH;
                rogue.player.t_flags &= ~CANHUH;
            }
            if(tp->t_stats.s_hpt <= 0) {
                killed(item, TRUE);
            }
        } else if(thrown) {
            bounce(weap, mname);
        } else {
            miss(nullptr, mname);
        }
    }
    ui.count = 0;
    return did_hit;
}

/*
 * attack:
 *     The monster attacks the player
 */

int attack(thing *mp) {
    const char *mname;

    /*
     * Since this is an attack, stop running and any healing that was
     * going on at the time.
     */
    game.running = FALSE;
    game.quiet = 0;
    if(mp->t_type == 'M' && off(rogue.player, ISBLIND)) {
        mp->t_disguise = 'M';
    }
    if(on(rogue.player, ISBLIND)) {
        mname = "it";
    } else {
        mname = Game::monsters[(size_t)mp->t_type - 'A'].m_name;
    }
    if(roll_em(&mp->t_stats, &rogue.pstats(), nullptr, FALSE)) {
        if(mp->t_type != 'E') {
            hit(mname, nullptr);
        }
        if(rogue.pstats().s_hpt <= 0) {
            death(mp->t_type); /* Bye bye life ... */
        }
        if(off(*mp, ISCANC)) {
            switch(mp->t_type) {
                case 'R':
                    /*
                     * If a rust monster hits, you lose armor
                     */
                    if(rogue.cur_armor != nullptr && rogue.cur_armor->o_ac < 9) {
                        if(!game.terse) {
                            msg("Your armor appears to be weaker now. Oh my!");
                        } else {
                            msg("Your armor weakens");
                        }
                        rogue.cur_armor->o_ac++;
                    }
                    break;
                case 'E':
                    /*
                     * The gaze of the floating eye hypnotizes you
                     */
                    if(on(rogue.player, ISBLIND)) {
                        break;
                    }
                    if(!rogue.no_command) {
                        addmsg("You are transfixed");
                        if(!game.terse) {
                            addmsg(" by the gaze of the floating eye.");
                        }
                        endmsg();
                    }
                    rogue.no_command += rnd(2) + 2;
                    if(rogue.no_command > 100 && rogue.food_left <= 0) {
                        death('E');
                    }
                    break;
                case 'A':
                    /*
                     * Ants have poisonous bites
                     */
                    if(!save(VS_POISON)) {
                        if(!rogue.isWearing(R_SUSTSTR)) {
                            chg_str(-1);
                            if(!game.terse) {
                                msg("You feel a sting in your arm and now feel weaker");
                            } else {
                                msg("A sting has weakened you");
                            }
                        } else if(!game.terse) {
                            msg("A sting momentarily weakens you");
                        } else {
                            msg("Sting has no effect");
                        }
                    }
                    break;
                case 'W':
                    /*
                     * Wraiths might drain energy levels
                     */
                    if(rnd(100) < 15) {
                        int fewer;

                        if(rogue.pstats().s_exp == 0) {
                            death('W'); /* All levels gone */
                        }
                        msg("You suddenly feel weaker.");
                        if(--rogue.pstats().s_lvl == 0) {
                            rogue.pstats().s_exp = 0;
                            rogue.pstats().s_lvl = 1;
                        } else {
                            rogue.pstats().s_exp =
                                Game::e_levels[(size_t)rogue.pstats().s_lvl - 1] + 1;
                        }
                        fewer = roll(1, 10);
                        rogue.pstats().s_hpt -= fewer;
                        rogue.max_hp -= fewer;
                        if(rogue.pstats().s_hpt < 1) {
                            rogue.pstats().s_hpt = 1;
                        }
                        if(rogue.max_hp < 1) {
                            death('W');
                        }
                    }
                    break;
                case 'F':
                    /*
                     * Violet fungi stops the poor guy from moving
                     */
                    rogue.player.t_flags |= ISHELD;
                    sprintf(Game::monsters['F' - 'A'].m_stats.s_dmg, "%dd1", ++game.fung_hit);
                    break;
                case 'L': {
                    /*
                     * Leperachaun steals some gold
                     */
                    int lastpurse;

                    lastpurse = rogue.purse;
                    rogue.purse -= goldCalc();
                    if(!save(VS_MAGIC)) {
                        rogue.purse -= goldCalc() + goldCalc() + goldCalc() + goldCalc();
                    }
                    if(rogue.purse < 0) {
                        rogue.purse = 0;
                    }
                    if(rogue.purse != lastpurse) {
                        msg("Your purse feels lighter");
                    }
                    remove_monster(mp->t_pos, find_mons(mp->t_pos.y, mp->t_pos.x));
                    mp = nullptr;
                } break;
                case 'N': {
                    linked_list *list, *steal;
                    int nobj;

                    /*
                     * Nymph's steal a magic item, look through the pack
                     * and pick out one we like.
                     */
                    steal = nullptr;
                    for(nobj = 0, list = rogue.pack(); list != nullptr; list = next_p(list)) {
                        auto *obj = lobject(list);
                        if(obj != rogue.cur_armor && obj != rogue.cur_weapon &&
                           obj != rogue.cur_ring[Rogue::LEFT] &&
                           obj != rogue.cur_ring[Rogue::RIGHT] && /* Nymph bug fix */
                           is_magic(obj) && rnd(++nobj) == 0) {
                            steal = list;
                        }
                    }
                    if(steal != nullptr) {
                        auto *sobj = lobject(steal);
                        remove_monster(mp->t_pos, find_mons(mp->t_pos.y, mp->t_pos.x));
                        mp = nullptr;
                        if(sobj->o_count > 1 && sobj->o_group == 0) {
                            int oc;

                            oc = sobj->o_count;
                            sobj->o_count = 1;
                            msg("She stole %s!", inv_name(sobj, TRUE));
                            sobj->o_count = oc - 1;
                        } else {
                            msg("She stole %s!", inv_name(sobj, TRUE));
                            detach(rogue.pack(), steal);
                            discard(steal);
                        }
                        rogue.inpack--;
                    }
                } break;
                default:
                    break;
            }
        }
    } else if((mp != nullptr) && mp->t_type != 'E') {
        if(mp->t_type == 'F') {
            rogue.pstats().s_hpt -= game.fung_hit;
            if(rogue.pstats().s_hpt <= 0) {
                death(mp->t_type); /* Bye bye life ... */
            }
        }
        miss(mname, nullptr);
    }
    /*
     * Check to see if this is a regenerating monster and let it heal if
     * it is.
     */
    if((mp != nullptr) && (on(*mp, ISREGEN) && rnd(100) < 33)) {
        mp->t_stats.s_hpt++;
    }
    if(game.fight_flush) {
        flush_type(); /* flush typeahead */
    }
    ui.count = 0;
    status();

    if(mp == nullptr) {
        return (-1);
    }
    return (0);
}

/*
 * swing:
 *     returns true if the swing hits
 */

int swing(const int at_lvl, const int op_arm, const int wplus) {
    int res = rnd(20) + 1;
    int need = (21 - at_lvl) - op_arm;

    return (res + wplus >= need);
}

/*
 * check_level:
 *     Check to see if the guy has gone up a level.
 */

void check_level() {
    int i;

    for(i = 0; Game::e_levels[(size_t)i] != 0; i++) {
        if(Game::e_levels[(size_t)i] > rogue.pstats().s_exp) {
            break;
        }
    }
    i++;
    if(i > rogue.pstats().s_lvl) {
        int add = roll(i - rogue.pstats().s_lvl, 10);
        rogue.max_hp += add;
        if((rogue.pstats().s_hpt += add) > rogue.max_hp) {
            rogue.pstats().s_hpt = rogue.max_hp;
        }
        msg("Welcome to level %d", i);
    }
    rogue.pstats().s_lvl = i;
}

/*
 * roll_em:
 *     Roll several attacks
 */

int roll_em(const stats *att, stats *def, object *weap, const int hurl) {
    const char *cp;
    int def_arm;
    int did_hit = FALSE;
    int prop_hplus = 0;
    int prop_dplus = 0;

    if(weap == nullptr) {
        cp = att->s_dmg;
    } else if(hurl) {
        if((weap->o_flags & ISMISL) && rogue.cur_weapon != nullptr &&
           rogue.cur_weapon->o_which == weap->o_launch) {
            cp = weap->o_hurldmg;
            prop_hplus = rogue.cur_weapon->o_hplus;
            prop_dplus = rogue.cur_weapon->o_dplus;
        } else {
            cp = (weap->o_flags & ISMISL ? weap->o_damage : weap->o_hurldmg);
        }
    } else {
        cp = weap->o_damage;
        /*
         * Drain a staff of striking
         */
        if(weap->o_type == STICK && weap->o_which == WS_HIT && weap->o_charges == 0) {
            strcpy(weap->o_damage, "0d0");
            weap->o_hplus = weap->o_dplus = 0;
        }
    }
    for(;;) {
        int damage;
        int hplus = prop_hplus + (weap == nullptr ? 0 : weap->o_hplus);
        int dplus = prop_dplus + (weap == nullptr ? 0 : weap->o_dplus);

        if(weap == rogue.cur_weapon) {
            if(rogue.isRing(Rogue::LEFT, R_ADDDAM)) {
                dplus += rogue.cur_ring[Rogue::LEFT]->o_ac;
            } else if(rogue.isRing(Rogue::LEFT, R_ADDHIT)) {
                hplus += rogue.cur_ring[Rogue::LEFT]->o_ac;
            }
            if(rogue.isRing(Rogue::RIGHT, R_ADDDAM)) {
                dplus += rogue.cur_ring[Rogue::RIGHT]->o_ac;
            } else if(rogue.isRing(Rogue::RIGHT, R_ADDHIT)) {
                hplus += rogue.cur_ring[Rogue::RIGHT]->o_ac;
            }
        }
        int ndice = atoi(cp);
        if((cp = strchr(cp, 'd')) == nullptr) {
            break;
        }
        int nsides = atoi(++cp);
        if(def == &rogue.pstats()) {
            if(rogue.cur_armor != nullptr) {
                def_arm = rogue.cur_armor->o_ac;
            } else {
                def_arm = def->s_arm;
            }
            if(rogue.isRing(Rogue::LEFT, R_PROTECT)) {
                def_arm -= rogue.cur_ring[Rogue::LEFT]->o_ac;
            } else if(rogue.isRing(Rogue::RIGHT, R_PROTECT)) {
                def_arm -= rogue.cur_ring[Rogue::RIGHT]->o_ac;
            }
        } else {
            def_arm = def->s_arm;
        }
        if(swing(att->s_lvl, def_arm, hplus + str_plus(&att->s_str))) {
            int proll;

            proll = roll(ndice, nsides);
            if(ndice + nsides > 0 && proll < 1) {
                debug("Damage for %dd%d came out %d.", ndice, nsides, proll);
            }
            damage = dplus + proll + add_dam(&att->s_str);
            def->s_hpt -= std::max(0, damage);
            did_hit = TRUE;
        }
        if((cp = strchr(cp, '/')) == nullptr) {
            break;
        }
        cp++;
    }
    return did_hit;
}

/*
 * prname:
 *     The print name of a combatant
 */

char *prname(const char *who, const int upper) {
    static char tbuf[80];

    *tbuf = '\0';
    if(who == nullptr) {
        strcpy(tbuf, "you");
    } else if(on(rogue.player, ISBLIND)) {
        strcpy(tbuf, "it");
    } else {
        strcpy(tbuf, "the ");
        strcat(tbuf, who);
    }
    if(upper) {
        *tbuf = (char)toupper(*tbuf);
    }
    return tbuf;
}

/*
 * hit:
 *     Print a message to indicate a succesful hit
 */

void hit(const char *er, const char *ee) {
    const char *s = "";

    addmsg(prname(er, TRUE));
    if(game.terse) {
        s = " hit.";
    } else {
        switch(rnd(4)) {
            case 0:
                s = " scored an excellent hit on ";
                break;
            case 1:
                s = " hit ";
                break;
            case 2:
                s = (er == nullptr ? " have injured " : " has injured ");
                break;
            case 3:
                s = (er == nullptr ? " swing and hit " : " swings and hits ");
        }
    }
    addmsg(s);
    if(!game.terse) {
        addmsg(prname(ee, FALSE));
    }
    endmsg();
}

/*
 * miss:
 *     Print a message to indicate a poor swing
 */

void miss(const char *er, const char *ee) {
    const char *s = "";

    addmsg(prname(er, TRUE));
    switch(game.terse ? 0 : rnd(4)) {
        case 0:
            s = (er == nullptr ? " miss" : " misses");
            break;
        case 1:
            s = (er == nullptr ? " swing and miss" : " swings and misses");
            break;
        case 2:
            s = (er == nullptr ? " barely miss" : " barely misses");
            break;
        case 3:
            s = (er == nullptr ? " don't hit" : " doesn't hit");
    }
    addmsg(s);
    if(!game.terse) {
        addmsg(" %s", prname(ee, FALSE));
    }
    endmsg();
}

/*
 * save_throw:
 *     See if a creature save against something
 */
int save_throw(const int which, const thing *tp) {
    int need;

    need = 14 + which - tp->t_stats.s_lvl / 2;
    return (roll(1, 20) >= need);
}

/*
 * save:
 *     See if he saves against various nasty things
 */

int save(const int which) {
    return save_throw(which, &rogue.player);
}

/*
 * str_plus:
 *     compute bonus/penalties for strength on the "to hit" roll
 */

int str_plus(const str_t *str) {
    if(str->st_str == 18) {
        if(str->st_add == 100) {
            return 3;
        }
        if(str->st_add > 50) {
            return 2;
        }
    }
    if(str->st_str >= 17) {
        return 1;
    }
    if(str->st_str > 6) {
        return 0;
    }
    return str->st_str - 7;
}

/*
 * add_dam:
 *     compute additional damage done for exceptionally high or low strength
 */

int add_dam(const str_t *str) {
    if(str->st_str == 18) {
        if(str->st_add == 100) {
            return 6;
        }
        if(str->st_add > 90) {
            return 5;
        }
        if(str->st_add > 75) {
            return 4;
        }
        if(str->st_add != 0) {
            return 3;
        }
        return 2;
    }
    if(str->st_str > 15) {
        return 1;
    }
    if(str->st_str > 6) {
        return 0;
    }
    return str->st_str - 7;
}

/*
 * raise_level:
 *     The guy just magically went up a level.
 */

void raise_level() {
    rogue.pstats().s_exp = Game::e_levels[(size_t)rogue.pstats().s_lvl - 1] + 1L;
    check_level();
}

/*
 * thunk:
 *     A missile hits a monster
 */

void thunk(const object *weap, const char *mname) {
    if(weap->o_type == WEAPON) {
        msg("The %s hits the %s", Items::w_names[(size_t)weap->o_which].c_str(), mname);
    } else {
        msg("You hit the %s.", mname);
    }
}

/*
 * bounce:
 *     A missile misses a monster
 */

void bounce(const object *weap, const char *mname) {
    if(weap->o_type == WEAPON) {
        msg("The %s misses the %s", Items::w_names[(size_t)weap->o_which].c_str(), mname);
    } else {
        msg("You missed the %s.", mname);
    }
}

/*
 * remove a monster from the screen
 */
void remove_monster(const coord &mp, linked_list *item) {
    mvwaddch(ui.mw.w, mp.y, mp.x, ' ');
    mvwaddch_int(ui.cw.w, mp.y, mp.x, (lthing(item))->t_oldch);
    detach(floorMap.mlist, item);
    discard(item);
}

/*
 * is_magic:
 *     Returns true if an object radiates magic
 */

int is_magic(const object *obj) {
    switch(obj->o_type) {
        case ARMOR:
            return obj->o_ac != Items::a_class[(size_t)obj->o_which];
            // break;
        case WEAPON:
            return obj->o_hplus != 0 || obj->o_dplus != 0;
            // break;
        case POTION:
        case SCROLL:
        case STICK:
        case RING:
        case AMULET:
            return TRUE;
    }
    return FALSE;
}

/*
 * killed:
 *     Called to put a monster to death
 */

void killed(linked_list *item, const int pr) {
    auto *tp = lthing(item);
    if(pr) {
        addmsg(game.terse ? "Defeated " : "You have defeated ");
        if(on(rogue.player, ISBLIND)) {
            msg("it.");
        } else {
            if(!game.terse) {
                addmsg("the ");
            }
            msg("%s.", Game::monsters[(size_t)tp->t_type - 'A'].m_name);
        }
    }
    rogue.pstats().s_exp += tp->t_stats.s_exp;
    /*
     * Do adjustments if he went up a level
     */
    check_level();
    /*
     * If the monster was a violet fungi, un-hold him
     */
    switch(tp->t_type) {
        case 'F':
            rogue.player.t_flags &= ~ISHELD;
            game.fung_hit = 0;
            strcpy(Game::monsters['F' - 'A'].m_stats.s_dmg, "000d0");
            break;
        case 'L': {
            room *rp;

            if((rp = roomin(tp->t_pos)) == nullptr) {
                break;
            }
            if(rp->r_goldval != 0 || fallpos(tp->t_pos, rp->r_gold, FALSE)) {
                rp->r_goldval += goldCalc();
                if(save(VS_MAGIC)) {
                    rp->r_goldval += goldCalc() + goldCalc() + goldCalc() + goldCalc();
                }
                mvwaddch(ui.stdScr.w, rp->r_gold.y, rp->r_gold.x, GOLD);
                if(!(rp->r_flags & ISDARK)) {
                    light(rogue.hero());
                    mvwaddch(ui.cw.w, rogue.hero().y, rogue.hero().x, PLAYER);
                }
            }
        }
    }
    /*
     * Empty the monsters pack
     */
    linked_list *pitem = tp->t_pack;
    while(pitem != nullptr) {
        linked_list *nexti = next_p(tp->t_pack);
        auto *obj = lobject(pitem);
        obj->o_pos = tp->t_pos;
        detach(tp->t_pack, pitem);
        fall(pitem, FALSE);
        pitem = nexti;
    }
    /*
     * Get rid of the monster.
     */
    remove_monster(tp->t_pos, item);
}
