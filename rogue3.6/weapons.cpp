/*
 * Functions for dealing with problems brought about by weapons
 *
 * @(#)weapons.c     3.17 (Berkeley) 6/15/81
 *
 * Rogue: Exploring the Dungeons of Doom
 * Copyright (C) 1980, 1981 Michael Toy, Ken Arnold and Glenn Wichman
 * All rights reserved.
 *
 * See the file LICENSE.TXT for full copyright and licensing information.
 */

#include "rogue.h"

#define NONE 100

array<string, MAXWEAPONS> Items::w_names{
    "mace",  "long sword", "long bow", "arrow",         "dagger", "rock", "two handed sword",
    "sling", "dart",       "crossbow", "crossbow bolt", "spear",
};

static struct init_weps {
    const char *iw_dam;
    const char *iw_hrl;
    int iw_launch;
    int iw_flags;
} init_dam[MAXWEAPONS] = {
    {"2d4", "1d3", NONE, 0},                    /* Mace */
    {"1d10", "1d2", NONE, 0},                   /* Long sword */
    {"1d1", "1d1", NONE, 0},                    /* Bow */
    {"1d1", "1d6", BOW, ISMANY | ISMISL},       /* Arrow */
    {"1d6", "1d4", NONE, ISMISL},               /* Dagger */
    {"1d2", "1d4", SLING, ISMANY | ISMISL},     /* Rock */
    {"3d6", "1d2", NONE, 0},                    /* 2h sword */
    {"0d0", "0d0", NONE, 0},                    /* Sling */
    {"1d1", "1d3", NONE, ISMANY | ISMISL},      /* Dart */
    {"1d1", "1d1", NONE, 0},                    /* Crossbow */
    {"1d2", "1d10", CROSSBOW, ISMANY | ISMISL}, /* Crossbow bolt */
    {"1d8", "1d6", NONE, ISMISL},               /* Spear */
};

/*
 * missile:
 *     Fire a missile in a given direction
 */

void missile(const int ydelta, const int xdelta) {
    /* Get which thing we are hurling */
    linked_list *item;
    if((item = get_item("throw", WEAPON)) == nullptr) {
        return;
    }
    auto *obj = lobject(item);
    if(!dropcheck(obj) || is_current(obj)) {
        return;
    }
    /*
     * Get rid of the thing.  If it is a non-multiple item object, or
     * if it is the last thing, just drop it.  Otherwise, create a new
     * item with a count of one.
     */
    if(obj->o_count < 2) {
        detach(rogue.pack(), item);
        rogue.inpack--;
    } else {
        obj->o_count--;
        if(obj->o_group == 0) {
            rogue.inpack--;
        }
        auto *nitem = (linked_list *)new_item(sizeof *obj);
        obj = lobject(nitem);
        *obj = *(lobject(item));
        obj->o_count = 1;
        item = nitem;
    }
    do_motion(obj, ydelta, xdelta);
    /*
     * AHA! Here it has hit something.  If it is a wall or a door,
     * or if it misses (combat) the mosnter, put it on the floor
     */
    if(!isupper((int)mvwinch(ui.mw.w, obj->o_pos.y, obj->o_pos.x)) ||
       !hit_monster(unc(obj->o_pos), obj)) {
        fall(item, TRUE);
    }
    mvwaddch(ui.cw.w, rogue.hero().y, rogue.hero().x, PLAYER);
}

/* do the actual motion on the screen done by an object traveling across the room */
void do_motion(object *obj, const int ydelta, const int xdelta) {
    /* Come fly with us ... */
    obj->o_pos = rogue.hero();
    for(;;) {
        int ch;

        /* Erase the old one */
        if(!(obj->o_pos == rogue.hero()) && cansee(unc(obj->o_pos)) &&
           mvwinch(ui.cw.w, obj->o_pos.y, obj->o_pos.x) != ' ') {
            mvwaddch_int(ui.cw.w, obj->o_pos.y, obj->o_pos.x, show(obj->o_pos.y, obj->o_pos.x));
        }
        /* Get the new position */
        obj->o_pos.y += ydelta;
        obj->o_pos.x += xdelta;
        if(step_ok(ch = (int)ui.winat(obj->o_pos.y, obj->o_pos.x)) && ch != DOOR) {
            /* It hasn't hit anything yet, so display it. If it alright. */
            if(cansee(unc(obj->o_pos)) && mvwinch(ui.cw.w, obj->o_pos.y, obj->o_pos.x) != ' ') {
                mvwaddch_int(ui.cw.w, obj->o_pos.y, obj->o_pos.x, obj->o_type);
                draw(ui.cw.w);
            }
            continue;
        }
        break;
    }
}

/*
 * fall:
 *     Drop an item someplace around here.
 */

void fall(linked_list *item, const int pr) {
    static coord fpos;

    auto *obj = lobject(item);
    if(fallpos(obj->o_pos, fpos, TRUE)) {
        mvaddch_int(fpos.y, fpos.x, obj->o_type);
        obj->o_pos = fpos;
        room *rp;
        if((rp = roomin(rogue.hero())) != nullptr && !(rp->r_flags & ISDARK)) {
            light(rogue.hero());
            mvwaddch(ui.cw.w, rogue.hero().y, rogue.hero().x, PLAYER);
        }
        attach(floorMap.lvl_obj, item);
        return;
    }
    if(pr) {
        if(obj->o_type == WEAPON) { /* BUGFUX: Identification trick */
            msg("Your %s vanishes as it hits the ground.",
                Items::w_names[(size_t)obj->o_which].c_str());
        } else {
            msg("%s vanishes as it hits the ground.", inv_name(obj, TRUE));
        }
    }
    discard(item);
}

/*
 * init_weapon:
 *     Set up the initial goodies for a weapon
 */

#define newgrp() ++game.group

void init_weapon(object *weap, const int type) {
    init_weps *iwp = &init_dam[type];
    strcpy(weap->o_damage, iwp->iw_dam);
    strcpy(weap->o_hurldmg, iwp->iw_hrl);
    weap->o_launch = iwp->iw_launch;
    weap->o_flags = iwp->iw_flags;
    if(weap->o_flags & ISMANY) {
        weap->o_count = rnd(8) + 8;
        weap->o_group = newgrp();
    } else {
        weap->o_count = 1;
    }
}

/* Does the missile hit the monster */
int hit_monster(const int y, const int x, object *obj) {
    static coord mp;

    mp.y = y;
    mp.x = x;
    return fight(mp, (int)ui.winat(y, x), obj, TRUE);
}

/*
 * num:
 *     Figure out the plus number for armor/weapons
 */

const char *num(const int n1, const int n2) {
    static char numbuf[80];

    if(n1 == 0 && n2 == 0) {
        return "+0";
    }
    if(n2 == 0) {
        sprintf(numbuf, "%s%d", n1 < 0 ? "" : "+", n1);
    } else {
        sprintf(numbuf, "%s%d,%s%d", n1 < 0 ? "" : "+", n1, n2 < 0 ? "" : "+", n2);
    }
    return numbuf;
}

/*
 * wield:
 *     Pull out a certain weapon
 */

void wield() {
    object *oweapon = rogue.cur_weapon;
    if(!dropcheck(rogue.cur_weapon)) {
        rogue.cur_weapon = oweapon;
        return;
    }
    rogue.cur_weapon = oweapon;
    linked_list *item;
    if((item = get_item("wield", WEAPON)) == nullptr) {
        game.after = FALSE;
        return;
    }

    auto *obj = lobject(item);
    if(obj->o_type == ARMOR) {
        msg("You can't wield armor");
        game.after = FALSE;
        return;
    }
    if(is_current(obj)) {
        game.after = FALSE;
        return;
    }

    if(game.terse) {
        addmsg("W");
    } else {
        addmsg("You are now w");
    }
    msg("ielding %s", inv_name(obj, TRUE));
    rogue.cur_weapon = obj;
}

/* pick a random position around the give (y, x) coordinates */
int fallpos(const coord &pos, coord &newpos, const int passages) {
    int cnt = 0;
    for(int y = pos.y - 1; y <= pos.y + 1; y++) {
        for(int x = pos.x - 1; x <= pos.x + 1; x++) {
            /*
             * check to make certain the spot is empty, if it is,
             * put the object there, set it in the level list
             * and re-draw the room if he can see it
             */
            if(y == rogue.hero().y && x == rogue.hero().x) {
                continue;
            }
            int ch = (int)ui.winat(y, x);
            if((ch == FLOOR || (passages && ch == PASSAGE)) && rnd(++cnt) == 0) {
                newpos.y = y;
                newpos.x = x;
            }
        }
    }
    return (cnt != 0);
}
