/*
 * Functions for dealing with linked lists of goodies
 *
 * @(#)list.c     3.3 (Berkeley) 6/15/81
 *
 * Rogue: Exploring the Dungeons of Doom
 * Copyright (C) 1980, 1981 Michael Toy, Ken Arnold and Glenn Wichman
 * All rights reserved.
 *
 * See the file LICENSE.TXT for full copyright and licensing information.
 */

#include "rogue.h"

/*
 * detach:
 *     Takes an item out of whatever linked list it might be in
 */

void _detach(linked_list **list, linked_list *item) {
    if(*list == item) {
        *list = next_p(item);
    }
    if(prev_p(item) != nullptr) {
        item->l_prev->l_next = next_p(item);
    }
    if(next_p(item) != nullptr) {
        item->l_next->l_prev = prev_p(item);
    }
    item->l_next = nullptr;
    item->l_prev = nullptr;
}

/*
 * _attach:
 *     add an item to the head of a list
 */

void _attach(linked_list **list, linked_list *item) {
    if(*list != nullptr) {
        item->l_next = *list;
        (*list)->l_prev = item;
        item->l_prev = nullptr;
    } else {
        item->l_next = nullptr;
        item->l_prev = nullptr;
    }

    *list = item;
}

/*
 * _free_list:
 *     Throw the whole blamed thing away
 */

void _free_list(linked_list **ptr) {
    while(*ptr != nullptr) {
        linked_list *item = *ptr;
        *ptr = next_p(item);
        discard(item);
    }
}

/*
 * discard:
 *     free up an item
 */

void discard(linked_list *item) {
    free((char *)item->l_data);
    free((char *)item);
}

/*
 * new_item
 *     get a new item with a specified size
 */

linked_list *new_item(const int size) {
    linked_list *item;

    if((item = (linked_list *)_new(sizeof *item)) == nullptr) {
        msg("Ran out of memory for header.");
        return nullptr;
    }

    if((item->l_data = _new((size_t)size)) == nullptr) {
        msg("Ran out of memory for data.");
        free(item);
        return nullptr;
    }

    item->l_next = item->l_prev = nullptr;
    memset(item->l_data, 0, (size_t)size);
    return item;
}

char *_new(const size_t size) {
    char *space = (char *)malloc((unsigned int)size);

    if(space == nullptr) {
        sprintf(ui.prbuf, "Rogue ran out of memory.  Fatal error!");
        fatal(ui.prbuf);
    }
    return space;
}
