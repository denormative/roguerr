/*
 * Rogue definitions and variable declarations
 *
 * @(#)rogue.h     3.38 (Berkeley) 6/15/81
 *
 * Rogue: Exploring the Dungeons of Doom
 * Copyright (C) 1980, 1981 Michael Toy, Ken Arnold and Glenn Wichman
 * All rights reserved.
 *
 * See the file LICENSE.TXT for full copyright and licensing information.
 */

#pragma once

#include "metarog.h"
#include <vector>
using std::vector;
#include <deque>
using std::deque;

#include <cereal/archives/json.hpp>
#include <cereal/types/array.hpp>
#include <cereal/types/deque.hpp>
#include <cereal/types/string.hpp>
#include <cereal/types/vector.hpp>

/* All the fun defines */
#define debug                                                                                      \
    if(game.wizard)                                                                                \
    msg
#define unc(cp) (cp).y, (cp).x

inline int distance(const int y1, const int x1, const int y2, const int x2) {
    return ((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
}

#define OBJPTR(what) (object *)((*what).l_data)
#define THINGPTR(what) (thing *)((*what).l_data)
#define draw(window) wrefresh(window)
#define attach(a, b) _attach(&a, b)
#define detach(a, b) _detach(&a, b)
#define free_list(a) _free_list(&a)
#undef CTRL
#define CTRL(ch) (ch & 037)

/* Things that appear on the screens */
#define PASSAGE '#'
#define DOOR '+'
#define FLOOR '.'
#define PLAYER '@'
#define TRAP '^'
#define TRAPDOOR '>'
#define ARROWTRAP '{'
#define SLEEPTRAP '$'
#define BEARTRAP '}'
#define TELTRAP '~'
#define DARTTRAP '`'
#define SECRETDOOR '&'
#define STAIRS '%'
#define GOLD '*'
#define POTION '!'
#define SCROLL '?'
#define MAGIC '$'
#define FOOD ':'
#define WEAPON ')'
#define ARMOR ']'
#define AMULET ','
#define RING '='
#define STICK '/'
#define CALLABLE -1

/* Save against things */
enum save_t {
    VS_POISON = 00,
    VS_PARALYZATION = 00,
    VS_DEATH = 00,
    VS_PETRIFICATION = 01,
    VS_BREATH = 02,
    VS_MAGIC = 03,
};

/* Various flag bits */
enum flag_t {
    ISDARK = 0000001,
    ISCURSED = 000001,
    ISBLIND = 0000001,
    ISGONE = 0000002,
    ISKNOW = 0000002,
    ISRUN = 0000004,
    ISFOUND = 0000010,
    ISINVIS = 0000020,
    ISMEAN = 0000040,
    ISGREED = 0000100,
    ISBLOCK = 0000200,
    ISHELD = 0000400,
    ISHUH = 0001000,
    ISREGEN = 0002000,
    CANHUH = 0004000,
    CANSEE = 0010000,
    ISMISL = 0020000,
    ISCANC = 0020000,
    ISMANY = 0040000,
    ISSLOW = 0040000,
    ISHASTE = 0100000,
};

/* Potion types */
enum potion_t {
    P_CONFUSE = 0,
    P_PARALYZE = 1,
    P_POISON = 2,
    P_STRENGTH = 3,
    P_SEEINVIS = 4,
    P_HEALING = 5,
    P_MFIND = 6,
    P_TFIND = 7,
    P_RAISE = 8,
    P_XHEAL = 9,
    P_HASTE = 10,
    P_RESTORE = 11,
    P_BLIND = 12,
    P_NOP = 13,
    MAXPOTIONS = 14,
};

/* Scroll types */
enum scroll_t {
    S_CONFUSE = 0,
    S_MAP = 1,
    S_LIGHT = 2,
    S_HOLD = 3,
    S_SLEEP = 4,
    S_ARMOR = 5,
    S_IDENT = 6,
    S_SCARE = 7,
    S_GFIND = 8,
    S_TELEP = 9,
    S_ENCH = 10,
    S_CREATE = 11,
    S_REMOVE = 12,
    S_AGGR = 13,
    S_NOP = 14,
    S_GENOCIDE = 15,
    MAXSCROLLS = 16,
};

/* Weapon types */
enum weapon_t {
    MACE = 0,
    SWORD = 1,
    BOW = 2,
    ARROW = 3,
    DAGGER = 4,
    ROCK = 5,
    TWOSWORD = 6,
    SLING = 7,
    DART = 8,
    CROSSBOW = 9,
    BOLT = 10,
    SPEAR = 11,
    MAXWEAPONS = 12,
};

/* Armor types */
enum armor_t {
    LEATHER = 0,
    RING_MAIL = 1,
    STUDDED_LEATHER = 2,
    SCALE_MAIL = 3,
    CHAIN_MAIL = 4,
    SPLINT_MAIL = 5,
    BANDED_MAIL = 6,
    PLATE_MAIL = 7,
    MAXARMORS = 8,
};

/* Ring types */
enum ring_t {
    R_PROTECT = 0,
    R_ADDSTR = 1,
    R_SUSTSTR = 2,
    R_SEARCH = 3,
    R_SEEINVIS = 4,
    R_NOP = 5,
    R_AGGR = 6,
    R_ADDHIT = 7,
    R_ADDDAM = 8,
    R_REGEN = 9,
    R_DIGEST = 10,
    R_TELEPORT = 11,
    R_STEALTH = 12,
    MAXRINGS = 13,
};

/* Rod/Wand/Staff types */
enum wandstaff_t {
    WS_LIGHT = 0,
    WS_HIT = 1,
    WS_ELECT = 2,
    WS_FIRE = 3,
    WS_COLD = 4,
    WS_POLYMORPH = 5,
    WS_MISSILE = 6,
    WS_HASTE_M = 7,
    WS_SLOW_M = 8,
    WS_DRAIN = 9,
    WS_NOP = 10,
    WS_TELAWAY = 11,
    WS_TELTO = 12,
    WS_CANCEL = 13,
    MAXSTICKS = 14,
};

/* Now we define the structures and types */

/* Help list */
struct h_list {
    int h_ch;
    string h_desc;
};

/* Coordinate data type */
struct coord {
    int x = -1;
    int y = -1;

    template <class Archive>
    void serialize(Archive &ar) {
        ar(CEREAL_NVP(x), CEREAL_NVP(y));
    }
};

const coord hero_pos{-2, -2};

inline bool operator==(const coord &a, const coord &b) {
    return ((a).x == (b).x && (a).y == (b).y);
}

inline bool operator!=(const coord &a, const coord &b) {
    return !(a == b);
}

inline void cmov(const coord &xy) {
    move(xy.y, xy.x);
}

struct str_t {
    int st_str;
    int st_add;

    template <class Archive>
    void serialize(Archive &ar) {
        // clang-format off
        ar( CEREAL_NVP(st_str),
            CEREAL_NVP(st_add)
        );
        // clang-format on
    }
};

/* Linked list data type */
struct linked_list {
    linked_list *l_next;
    linked_list *l_prev;
    char *l_data; /* Various structure pointers */
};

inline linked_list *next_p(linked_list *ptr) {
    return (*ptr).l_next;
}
inline linked_list *prev_p(linked_list *ptr) {
    return (*ptr).l_prev;
}

/* Stuff about magic items */
struct magic_item {
    string mi_name;
    int mi_prob;
    int mi_worth;

    template <class Archive>
    void serialize(Archive &ar) {
        ar(CEREAL_NVP(mi_name), CEREAL_NVP(mi_prob), CEREAL_NVP(mi_worth));
    }
};

/* Room structure */
struct room {
    coord r_pos;          /* Upper left corner */
    coord r_max;          /* Size of room */
    coord r_gold;         /* Where the gold is */
    int r_goldval;        /* How much the gold is worth */
    int r_flags;          /* Info about the room */
    vector<coord> r_exit; /* Where the exits are */

    template <class Archive>
    void serialize(Archive &ar) {
        // clang-format off
        ar( CEREAL_NVP(r_pos),
            CEREAL_NVP(r_max),
            CEREAL_NVP(r_gold),
            CEREAL_NVP(r_goldval),
            CEREAL_NVP(r_flags),
            CEREAL_NVP(r_exit)
        );
        // clang-format on
    }
};

/* Array of all traps on this level */
struct trap {
    coord tr_pos; /* Where trap is */
    int tr_type;  /* What kind of trap */
    int tr_flags; /* Info about trap (i.e. ISFOUND) */

    template <class Archive>
    void serialize(Archive &ar) {
        // clang-format off
        ar( CEREAL_NVP(tr_pos),
            CEREAL_NVP(tr_type),
            CEREAL_NVP(tr_flags)
        );
        // clang-format on
    }
};

/* Structure describing a fighting being */
struct stats {
    str_t s_str;    /* Strength */
    int s_exp;      /* Experience */
    int s_lvl;      /* Level of mastery */
    int s_arm;      /* Armor class */
    int s_hpt;      /* Hit points */
    char s_dmg[30]; /* String describing damage done */

    template <class Archive>
    void serialize(Archive &ar) {
        // clang-format off
        ar( CEREAL_NVP(s_str),
            CEREAL_NVP(s_exp),
            CEREAL_NVP(s_lvl),
            CEREAL_NVP(s_arm),
            CEREAL_NVP(s_hpt),
            CEREAL_NVP(s_dmg)
        );
        // clang-format on
    }
};

/* Structure for monsters and player */
struct thing {
    coord t_pos;         /* Position */
    int t_turn;          /* If slowed, is it a turn to move */
    int t_type;          /* What it is */
    int t_disguise;      /* What mimic looks like */
    int t_oldch;         /* Character that was where it was */
    coord t_dest;        /* Where it is running to */
    int t_flags;         /* State word */
    stats t_stats;       /* Physical description */
    linked_list *t_pack; /* What the thing is carrying */
    int t_reserved;      /* reserved for save/restore code */

    template <class Archive>
    void serialize(Archive &ar) {
        // clang-format off
        ar( CEREAL_NVP(t_pos),
            CEREAL_NVP(t_turn),
            CEREAL_NVP(t_type),
            CEREAL_NVP(t_disguise),
            CEREAL_NVP(t_oldch),
             CEREAL_NVP(t_dest),
            CEREAL_NVP(t_flags),
            CEREAL_NVP(t_stats),
            // CEREAL_NVP(t_pack),
            CEREAL_NVP(t_reserved)
        );
        // clang-format on
    }
};

/* Array containing information on all the various types of mosnters */
struct monster {
    char m_name[20]; /* What to call the monster */
    int m_carry;     /* Probability of carrying something */
    int m_flags;     /* Things about the monster */
    stats m_stats;   /* Initial stats */
};

/* Structure for a thing that the rogue can carry */
struct object {
    int o_type;        /* What kind of object it is */
    coord o_pos;       /* Where it lives on the screen */
    int o_launch;      /* What you need to launch it */
    char o_damage[8];  /* Damage if used like sword */
    char o_hurldmg[8]; /* Damage if thrown */
    int o_count;       /* Count for plural objects */
    int o_which;       /* Which object of a type it is */
    int o_hplus;       /* Plusses to hit */
    int o_dplus;       /* Plusses to damage */
    union {
        int o_ac;      /* Armor class */
        int o_charges; /* Charges if weapon */
    };
    int o_flags; /* Information about objects */
    int o_group; /* Group number for this object */
};

inline object *lobject(linked_list *ptr) {
    return reinterpret_cast<object *>((*ptr).l_data);
}
inline const object *lobject(const linked_list *ptr) {
    return reinterpret_cast<const object *>((*ptr).l_data);
}

inline thing *lthing(linked_list *ptr) {
    return reinterpret_cast<thing *>((*ptr).l_data);
}
inline const thing *lthing(const linked_list *ptr) {
    return reinterpret_cast<const thing *>((*ptr).l_data);
}

inline auto inroom(const room &rp, const coord &cp) {
    return ((cp).x <= (rp).r_pos.x + ((rp).r_max.x - 1) && (rp).r_pos.x <= (cp).x &&
            (cp).y <= (rp).r_pos.y + ((rp).r_max.y - 1) && (rp).r_pos.y <= (cp).y);
}

inline bool on(thing &thing, const flag_t flag) {
    return (((thing).t_flags & flag) != 0);
}
inline bool off(thing &thing, const flag_t flag) {
    return (((thing).t_flags & flag) == 0);
}
inline bool ISMULT(const int type) {
    return (type == POTION || type == SCROLL || type == FOOD);
}

class Floor {
public:
    static const int MAXROOMS = 9;
    static const int MAXTRAPS = 10;
    static const int MAXOBJ = 9;

    room rooms[Floor::MAXROOMS]; /* One for each room -- A level */
    int ntraps;                  /* Number of traps on this level */
    trap traps[Floor::MAXTRAPS];
    linked_list *lvl_obj = nullptr; /* List of objects on this level */
    linked_list *mlist = nullptr;   /* List of monsters on the level */

    template <class Archive>
    void serialize(Archive &ar) {
        // clang-format off
        ar( // CEREAL_NVP(lvl_obj),
            // CEREAL_NVP(mlist),
            CEREAL_NVP(traps),
            CEREAL_NVP(rooms),
            CEREAL_NVP(ntraps)
        );
        // clang-format on
    }
};
extern Floor floorMap;

class Game {
public:
    int curr_level = 1;      /* What level rogue is on */
    int max_level = 0;       /* Deepest player has gone */
    int amulet = FALSE;      /* He found the amulet */
    int dnum = 0;            /* Dungeon number */
    int waswizard = 0;       /* Was a wizard sometime */
    int wizard = FALSE;      /* True if allows wizard commands */
    int running = FALSE;     /* True if player is running */
    int notify = TRUE;       /* True if player wants to know */
    int fight_flush = FALSE; /* True if toilet input */
    int terse = FALSE;       /* True if we should be long */
    int door_stop = FALSE;   /* Stop running when we pass a door */
    int jump = FALSE;        /* Show running as series of jumps */
    int slow_invent = FALSE; /* Inventory one line at a time */
    // FIXME: not sure what this 'firstmove' thing is about
    int firstmove = FALSE;        /* First move after setting door_stop */
    int askme = FALSE;            /* Ask about unidentified things */
    string release = "3.6.4";     /* Release number of rogue */
    int lastscore = -1;           /* Score before this turn */
    int seed = 0;                 /* Random number seed */
    char whoami[80] = "";         /* Name of player */
    char fruit[80] = "";          /* Favorite fruit */
    char save_file_name[80] = ""; /* Save file name */
    int take = 0;                 /* Thing the rogue is taking */
    int runch = 0;                /* Direction player is running */
    char home[PATH_MAX] = "";     /* User's home directory */
    int after = 0;                /* True if we want after daemons */
    coord oldpos;                 /* Position before last look() call */
    room *oldrp = nullptr;        /* Roomin(&oldpos) */
    coord delta;                  /* Change indicated to get_dir()    */
    int no_food = 0;              /* Number of levels without food */
    int quiet = 0;                /* Number of quiet turns */
    int fung_hit = 0;             /* Number of time fungi has hit */
    int group = 1;                /* Current group number */
    static char lvl_mons[27];
    static char wand_mons[27];
    int between = 0;
    coord ch_ret; /* Where chasing takes you */

    static array<string, 11> metal;
    static array<monster, 26> monsters; /* The initial monster states */
    static array<string, 24> rainbow;
    static array<string, 20> stones;
    static array<string, 22> wood;
    static char version[];
    static char encstr[];

    static array<int, 20> e_levels;
    static array<string, 159> sylls;
    static int used[];
    array<h_list, 45> helpstr;

    auto RN() {
        return (((seed = seed * 11109 + 13849) & 0x7fff) >> 1);
    }

    /* Various constants */
    static const char *PASSWD;
    static const int BEARTIME = 3;
    static const int SLEEPTIME = 5;
    static const int HOLDTIME = 2;
    static const int WANDERTIME = 70;
    static const int HUHDURATION = 20;
    static const int SEEDURATION = 850;
    static const int HUNGERTIME = 1300;
    static const int MORETIME = 150;
    static const int STOMACHSIZE = 2000;
    static const int ESCAPE = 27;
    static const int BOLT_LENGTH = 6;

    Game() noexcept;
    template <class Archive>
    void serialize(Archive &ar) {
        // clang-format off
        ar( // CEREAL_NVP(oldrp),
            CEREAL_NVP(curr_level),
            CEREAL_NVP(lastscore),
            CEREAL_NVP(no_food),
            CEREAL_NVP(seed),
            CEREAL_NVP(dnum),
            CEREAL_NVP(fung_hit),
            CEREAL_NVP(quiet),
            CEREAL_NVP(max_level),
            CEREAL_NVP(group),
            CEREAL_NVP(runch),
            // rs_write_scrolls(savef);
            // rs_write_potions(savef);
            // rs_write_rings(savef);
            // rs_write_sticks(savef);
            // CEREAL_NVP(whoami, 80),
            // CEREAL_NVP(fruit, 80),
            CEREAL_NVP(running),
            CEREAL_NVP(wizard),
            CEREAL_NVP(after),
            CEREAL_NVP(notify),
            CEREAL_NVP(fight_flush),
            CEREAL_NVP(terse),
            CEREAL_NVP(door_stop),
            CEREAL_NVP(jump),
            CEREAL_NVP(slow_invent),
            CEREAL_NVP(firstmove),
            CEREAL_NVP(waswizard),
            CEREAL_NVP(askme),
            CEREAL_NVP(amulet),
            CEREAL_NVP(oldpos),
            CEREAL_NVP(delta),
            CEREAL_NVP(ch_ret),
            // rs_write_daemons(savef);                                         /* daemon.c     */
            CEREAL_NVP(between)
            // CEREAL_NVP(Game::lvl_mons, sizeof(Game::lvl_mons)),
            // CEREAL_NVP(Game::wand_mons, sizeof(Game::wand_mons)),
        );
        // clang-format on
    }
};
extern Game game;

class Rogue {
public:
    static const int MAXPACK = 23;
    enum ring_h {
        LEFT = 0,
        RIGHT = 1,
    };

    coord &hero() {
        return player.t_pos;
    }
    stats &pstats() {
        return player.t_stats;
    }
    linked_list *&pack() {
        return player.t_pack;
    }

    // FIXME: null everything?
    object *cur_armor;                /* What a well dresssed rogue wears */
    object *cur_ring[2];              /* Which rings are being worn */
    object *cur_weapon = nullptr;     /* Which weapon he is weilding */
    thing player;                     /* The rogue */
    int purse = 0;                    /* How much gold the rogue has */
    int food_left = Game::HUNGERTIME; /* Amount of food in hero's stomach */
    int hungry_state = 0;             /* How hungry is he */
    int inpack = 0;                   /* Number of things in pack */
    int max_hp;                       /* Player's max hit points */
    stats max_stats;                  /* The maximum for the player */
    int no_command = 0;               /* Number of turns asleep */
    int no_move = 0;                  /* Number of turns held in place */

    /*
     * Used to hold the new hero position
     */
    coord nh;

    bool isRing(const ring_h h, const ring_t r) {
        return (cur_ring[h] != nullptr && cur_ring[h]->o_which == r);
    }

    bool isWearing(const ring_t r) {
        return (isRing(LEFT, r) || isRing(RIGHT, r));
    }
    void init_player(); /* Roll up the rogue */

    template <class Archive>
    void serialize(Archive &ar) {
        // clang-format off
        ar( CEREAL_NVP(player),
            CEREAL_NVP(max_stats),
            // rs_write_object_reference(savef, rogue.player.t_pack, rogue.cur_weapon);
            // rs_write_object_reference(savef, rogue.player.t_pack, rogue.cur_armor);
            // rs_write_object_reference(savef, rogue.player.t_pack, rogue.cur_ring[0]);
            // rs_write_object_reference(savef, rogue.player.t_pack, rogue.cur_ring[1]);
            // CEREAL_NVP(player.t_pack, cur_weapon),
            // CEREAL_NVP(player.t_pack, cur_armor),
            // CEREAL_NVP(player.t_pack, cur_ring[0]),
            // CEREAL_NVP(player.t_pack, cur_ring[1]),
            CEREAL_NVP(purse),
            CEREAL_NVP(no_move),
            CEREAL_NVP(no_command),
            CEREAL_NVP(inpack),
            CEREAL_NVP(max_hp),
            CEREAL_NVP(food_left),
            CEREAL_NVP(hungry_state)
        );
        // clang-format on
    }
};
extern Rogue rogue;

class CursesWindow {
public:
    WINDOW *w;
    void showWindow(const char *message);
    void newWindow(const int lines, const int columns) {
        w = newwin(lines, columns, 0, 0);
    }
    void clearWindow() {
        wclear(w);
    }
    void touchWindow() {
        touchwin(w);
    }
    void drawWindow() {
        draw(w);
    }
    template <class Archive>
    void save(Archive &archive) const {
        const int width = getmaxx(w);
        const int height = getmaxy(w);
        deque<chtype> window;

        for(int row = 0; row < height; row++) {
            for(int col = 0; col < width; col++) {
                window.push_back(mvwinch(w, row, col));
            }
        }
        archive(width, height, window);
    }

    template <class Archive>
    void load(Archive &archive) {
        int width;
        int height;
        deque<chtype> window;

        archive(width, height, window);
        int maxcols = getmaxx(w);
        int maxlines = getmaxy(w);

        for(int row = 0; row < height; row++) {
            for(int col = 0; col < width; col++) {
                if((row < maxlines) && (col < maxcols)) {
                    mvwaddch_int(w, row, col, window.front());
                    window.pop_front();
                }
            }
        }
    }
};

class StdScr : public CursesWindow {
public:
    StdScr() {
        w = stdscr;
    }
};

class CurScr : public CursesWindow {
public:
    CurScr() {
        w = curscr;
    }
};

class MWindow : public CursesWindow {
public:
    void moveThingTo(const thing *th, const coord &c) {
        mvwaddch_int(w, th->t_pos.y, th->t_pos.x, ' ');
        mvwaddch_int(w, c.y, c.x, th->t_type);
    }
};

class HWindow : public CursesWindow {
public:
};

class CWindow : public CursesWindow {
public:
};

class UI {
public:
    /* return values for get functions */
    enum get_return {
        NORM = 0,  /* normal exit */
        QUIT = 1,  /* quit option setting */
        MINUS = 2, /* back up one option */
    };

    CWindow cw; /* Window that the player sees */
    HWindow hw; /* Used for the help command */
    MWindow mw; /* Used to store monsters */
    // curses internal windows
    StdScr stdScr;
    CurScr curScr;

    void initScr() {
        initscr(); /* Start up cursor package */
        stdScr = StdScr();
        curScr = CurScr();
    }

    char huh[80];   /* The last message printed */
    char prbuf[80]; /* Buffer for sprintfs */

    // FIXME: unsigned ints should be chtype
    unsigned int winat(int y, int x) {
        return (mvwinch(mw.w, y, x) == ' ' ? mvwinch(stdScr.w, y, x) : winch(mw.w));
    }
    int mpos = 0;  /* Where cursor is on top line */
    int count = 0; /* Number of times to repeat command */

    static char msgbuf[BUFSIZ];
    static int newpos;

    /*
     * description of an option and what to do with it
     */
    struct optstruct {
        string o_name;                            /* option name */
        string o_prompt;                          /* prompt for interactive entry */
        void *o_opt;                              /* pointer to thing to set */
        void (*o_putfunc)(void *opt);             /* function to print value */
        int (*o_getfunc)(void *opt, WINDOW *win); /* function to get value interactively */
    };

    typedef optstruct OPTION;

    static OPTION optlist[];

    template <class Archive>
    void serialize(Archive &ar) {
        // clang-format off
        ar( CEREAL_NVP(mpos),
            CEREAL_NVP(count),
            CEREAL_NVP(cw),
            CEREAL_NVP(mw),
            CEREAL_NVP(stdScr)
        );
        // clang-format on
    }
};
extern UI ui;

class Items {
public:
    static const int NUMTHINGS = 7; /* number of types of things (scrolls, rings, etc.) */

    array<int, MAXSCROLLS> s_know; /* Does he know what a scroll does */
    array<int, MAXPOTIONS> p_know; /* Does he know what a potion does */
    array<int, MAXRINGS> r_know;   /* Does he know what a ring does */
    array<int, MAXSTICKS> ws_know; /* Does he know what a stick does */

    array<string, MAXSCROLLS> s_guess; /* Players guess at what scroll is */
    array<string, MAXPOTIONS> p_guess; /* Players guess at what potion is */
    array<string, MAXRINGS> r_guess;   /* Players guess at what ring is */
    array<string, MAXSTICKS> ws_guess; /* Players guess at what wand is */

    // FIXME: give these consistent names like know/guess
    array<string, MAXSCROLLS> s_names;  /* Names of the scrolls */
    array<string, MAXPOTIONS> p_colors; /* Colors of the potions */
    array<string, MAXRINGS> r_stones;   /* Stone settings of the rings */
    array<string, MAXSTICKS> ws_made;   /* What sticks are made of */
    // FIME: Maybe integrate this into the ws_made above
    array<string, MAXSTICKS> ws_type; /* Is it a wand or a staff */

    static array<int, MAXARMORS> a_chances;  /* Probabilities for armor */
    static array<int, MAXARMORS> a_class;    /* Armor class for various armors */
    static array<string, MAXARMORS> a_names; /* Names of armor types */

    static array<magic_item, Items::NUMTHINGS> things; /* Chances for each type of item */
    static array<magic_item, MAXSCROLLS> s_magic;      /* Names and chances for scrolls */
    static array<magic_item, MAXRINGS> r_magic;        /* Names and chances for rings */
    static array<magic_item, MAXPOTIONS> p_magic;      /* Names and chances for potions */
    static array<magic_item, MAXSTICKS> ws_magic;      /* Names and chances for sticks */
    static array<string, MAXWEAPONS> w_names;          /* Names of the various weapons */

    template <class Archive>
    void serialize(Archive &ar) {
        ar(CEREAL_NVP(s_know), CEREAL_NVP(p_know), CEREAL_NVP(r_know), CEREAL_NVP(ws_know),
           CEREAL_NVP(s_guess), CEREAL_NVP(p_guess), CEREAL_NVP(r_guess), CEREAL_NVP(ws_guess),
           CEREAL_NVP(s_names), CEREAL_NVP(p_colors), CEREAL_NVP(r_stones), CEREAL_NVP(ws_made),
           CEREAL_NVP(ws_type), CEREAL_NVP(a_chances), CEREAL_NVP(a_class), CEREAL_NVP(a_names),
           CEREAL_NVP(things), CEREAL_NVP(s_magic), CEREAL_NVP(r_magic), CEREAL_NVP(p_magic),
           CEREAL_NVP(ws_magic), CEREAL_NVP(w_names));
    }
};
extern Items items;

// armor.h
void wear();
void take_off();

// chase.h
void runners();
int do_chase(thing *th);
void runto(coord &runner, const coord &spot);
int chase(thing *tp, const coord &ee);
room *roomin(const coord &cp);
linked_list *find_mons(const int y, const int x);
int diag_ok(const coord &sp, const coord &ep);
int cansee(const int y, const int x);

// command.h
void command();
void quit(const int p);
void search();
void help();
void identify();
void d_level();
void u_level();
void call();

// daemons.h
void doctor();
void swander();
void rollwand();
void unconfuse();
void unsee();
void sight();
void nohaste();
void stomach();

// fight.h
int fight(coord &mp, const int mn, object *weap, const int thrown);
int attack(thing *mp);
int swing(const int at_lvl, const int op_arm, const int wplus);
void check_level();
int roll_em(const stats *att, stats *def, object *weap, const int hurl);
char *prname(const char *who, const int upper);
void hit(const char *er, const char *ee);
void miss(const char *er, const char *ee);
int save_throw(const int which, const thing *tp);
int save(const int which);
int str_plus(const str_t *str);
int add_dam(const str_t *str);
void raise_level();
void thunk(const object *weap, const char *mname);
void bounce(const object *weap, const char *mname);
void remove_monster(const coord &mp, linked_list *item);
int is_magic(const object *obj);
void killed(linked_list *item, const int pr);

// init.h
void init_things();
void init_colors();
void init_names();
void init_stones();
void init_materials();
void badcheck(char *name, magic_item *magic, int bound);

// io.h
void msg(const char *fmt, ...);
void addmsg(const char *fmt, ...);
void endmsg();
void doadd(const char *fmt, va_list ap);
int step_ok(const int ch);
int readchar(WINDOW *win);
void status();
void wait_for(WINDOW *win, const int ch);
void show_win(WINDOW *scr, const char *message);
void flush_type();

// list.h
void _detach(linked_list **list, linked_list *item);
void _attach(linked_list **list, linked_list *item);
void _free_list(linked_list **ptr);
void discard(linked_list *item);
linked_list *new_item(const int size);
char *_new(const size_t size);

// main.h
int main(int argc, char **argv);
[[noreturn]] void endit(int p);
[[noreturn]] void fatal(const char *s);
int rnd(const int range);
size_t rnd(const size_t range);
int roll(int number, const int sides);
void tstp(int p);
void setup();
[[noreturn]] void playit();

// mdport.h
#define MD_STRIP_CTRL_KEYPAD 1
void md_init(const int options);
void md_onsignal_default();
void md_onsignal_exit();
int reread();
void unread(int c);
int md_readchar(WINDOW *win);

// misc.h
const char *trapName(const int ch);
void look(const int wakeup);
int secretdoor(const int y, const int x);
linked_list *find_obj(const int y, const int x);
void eat();
void chg_str(int amt);
void add_haste(int potion);
void aggravate();
string vowelstr(const string &str);
int is_current(const object *obj);
int get_dir();

// monsters.h
int randmonster(const int wander);
void new_monster(linked_list *item, const int type, coord &cp);
void wanderer();
linked_list *wake_monster(const int y, const int x);
void genocide();

// move.h
void do_run(const int ch);
void do_move(const int dy, const int dx);
void light(const coord &cp);
int show(const int y, const int x);
int be_trapped(const coord &tc);
trap *trap_at(const int y, const int x);
coord rndmove(thing *who);

// newlevel.h
void new_level();
int rnd_room();
void put_things();

// options.h
void option();
void put_bool(void *b);
void put_str(void *str);
int get_bool(void *vp, WINDOW *win);
int get_str(void *vopt, WINDOW *win);
void parse_opts(char *str);
void strucpy(char *s1, char *s2, size_t len);

// pack.h
void add_pack(linked_list *item, int silent);
int inventory(linked_list *list, const int type);
void pick_up(const int ch);
void picky_inven();
linked_list *get_item(const char *purpose, const int type);
int pack_char(const object *obj);

// passages.h
void do_passages();
void conn(const int r1, const int r2);
void door(room *rm, const coord &cp);
void add_pass();

// potions.h
void quaff();

// rings.h
void ring_on();
void ring_off();
int gethand();
int ring_eat(int hand);
const char *ring_num(const object *obj);

// rip.h
[[noreturn]] void death(const int monst);
void score(int amount, const int flags, const int monst);
[[noreturn]] void total_winner();
const char *killname(const int monst);

// rooms.h
void do_rooms();
void draw_room(const room *rp);
void horiz(int cnt);
void vert(int cnt);
void rnd_pos(const room *rp, coord &cp);

// save.h
int save_game();
[[noreturn]] void auto_save(int p);
int save_file(FILE *savef);
int restore(char *file);
int encerror();
void encseterr(const int err);
int encclearerr();
size_t encwrite(const void *buf, size_t size, FILE *outf);
size_t encread(void *buf, size_t size, FILE *inf);

// scrolls.h
void read_scroll();

// state.h
int rs_save_file(FILE *savef);
int rs_restore_file(FILE *savef);

// sticks.h
void fix_stick(object *cur);
void do_zap(const int gotdir);
void drain(int ymin, int ymax, int xmin, int xmax);
char *charge_str(const object *obj);

// things.h
char *inv_name(const object *obj, const int drop);
void money();
void drop();
int dropcheck(const object *op);
linked_list *new_thing();
int pick_one(const magic_item *start, int nitems);

// weapons.h
void missile(int ydelta, const int xdelta);
void do_motion(object *obj, const int ydelta, const int xdelta);
void fall(linked_list *item, int pr);
void init_weapon(object *weap, const int type);
int hit_monster(const int y, const int x, object *obj);
const char *num(const int n1, const int n2);
void wield();
int fallpos(const coord &pos, coord &newpos, const int passages);

// wizard.h
void whatis();
void create_obj();
int teleport();
int passwd();

#include "Daemons.h"

enum daemon_id {
    d_none = 0,

    d_rollwand,
    d_doctor,
    d_stomach,
    d_runners,
    d_swander,
    d_nohste,
    d_unconfuse,
    d_unsee,
    d_sight,

    d_dont_save,

    d_nohaste,
};

// TODO: Stuff below this really needs to go in other files...

inline int goldCalc() {
    return (rnd(50 + 10 * game.curr_level) + 2);
};

// FIXME: This warning seems more annoying than anything...
#pragma clang diagnostic ignored "-Wweak-vtables"

class RogueDaemons : public Daemons<daemon_id> {
public:
    void executeAction(const Action &a) {
        switch(a.d_id) {
            case d_none:
                // nothing to do
                break;
            case d_rollwand:
                rollwand();
                break;
            case d_doctor:
                doctor();
                break;
            case d_stomach:
                stomach();
                break;
            case d_runners:
                runners();
                break;
            case d_swander:
                swander();
                break;
            case d_nohste:
                nohaste();
                break;
            case d_unconfuse:
                unconfuse();
                break;
            case d_unsee:
                unsee();
                break;
            case d_sight:
                sight();
                break;
            case d_dont_save:
                assert(false);
                // can't happen
                break;
            case d_nohaste:
                nohaste();
                break;

            default:
                errormsg("daemon type not defined");
        }
    }

    // waste_time: Do nothing but let other things happen
    void waste_time() {
        doDaemons(RogueDaemons::Before);
        doFuses(RogueDaemons::Before);
        doDaemons(RogueDaemons::After);
        doFuses(RogueDaemons::After);
    }
};

extern RogueDaemons daemons;

/* where scorefile should live */
#define SCOREFILE "rogue36.scr"
