/*
 * Read and execute the user commands
 *
 * @(#)command.c     3.45 (Berkeley) 6/15/81
 *
 * Rogue: Exploring the Dungeons of Doom
 * Copyright (C) 1980, 1981 Michael Toy, Ken Arnold and Glenn Wichman
 * All rights reserved.
 *
 * See the file LICENSE.TXT for full copyright and licensing information.
 */

#include "rogue.h"

/*
 * command:
 *     Process the user commands
 */

void command() {
    int ch;
    int ntimes = 1; /* Number of player moves */
    static int countch, direction, newcount = FALSE;

    if(on(rogue.player, ISHASTE)) {
        ntimes++;
    }
    /*
     * Let the daemons start up
     */
    daemons.doDaemons(RogueDaemons::Before);
    daemons.doFuses(RogueDaemons::Before);
    while(ntimes--) {
        look(TRUE);
        if(!game.running) {
            game.door_stop = FALSE;
        }
        status();
        game.lastscore = rogue.purse;
        wmove(ui.cw.w, rogue.hero().y, rogue.hero().x);
        if(!((game.running || ui.count) && game.jump)) {
            draw(ui.cw.w); /* Draw screen */
        }
        game.take = 0;
        game.after = TRUE;
        /*
         * Read command or continue run
         */
        if(game.wizard) {
            game.waswizard = TRUE;
        }
        if(!rogue.no_command) {
            if(game.running) {
                ch = game.runch;
            } else if(ui.count) {
                ch = countch;
            } else {
                ch = readchar(ui.cw.w);
                if(ui.mpos != 0 && !game.running) { /* Erase message if its there */
                    msg("");
                }
            }
        } else {
            ch = ' ';
        }
        if(rogue.no_command) {
            if(--rogue.no_command == 0) {
                msg("You can move again.");
            }
        } else {
            /*
             * check for prefixes
             */
            if(isdigit(ch)) {
                ui.count = 0;
                newcount = TRUE;
                while(isdigit(ch)) {
                    ui.count = ui.count * 10 + (ch - '0');
                    ch = readchar(ui.cw.w);
                }
                countch = ch;
                /*
                 * turn off count for commands which don't make sense
                 * to repeat
                 */
                switch(ch) {
                    case 'h':
                    case 'j':
                    case 'k':
                    case 'l':
                    case 'y':
                    case 'u':
                    case 'b':
                    case 'n':
                    case 'H':
                    case 'J':
                    case 'K':
                    case 'L':
                    case 'Y':
                    case 'U':
                    case 'B':
                    case 'N':
                    case 'q':
                    case 'r':
                    case 's':
                    case 'f':
                    case 't':
                    case 'C':
                    case 'I':
                    case ' ':
                    case 'z':
                    case 'p':
                        break;
                    default:
                        ui.count = 0;
                }
            }
            switch(ch) {
                case 'f':
                    if(!on(rogue.player, ISBLIND)) {
                        game.door_stop = TRUE;
                        game.firstmove = TRUE;
                    }
                    if(ui.count && !newcount) {
                        ch = direction;
                    } else {
                        ch = readchar(ui.cw.w);
                    }
                    switch(ch) {
                        case 'h':
                        case 'j':
                        case 'k':
                        case 'l':
                        case 'y':
                        case 'u':
                        case 'b':
                        case 'n':
                            ch = toupper(ch);
                    }
                    direction = ch;
            }
            newcount = FALSE;
            /*
             * execute a command
             */
            if(ui.count && !game.running) {
                ui.count--;
            }
            switch(ch) {
                case 'h':
                    do_move(0, -1);
                    break;
                case 'j':
                    do_move(1, 0);
                    break;
                case 'k':
                    do_move(-1, 0);
                    break;
                case 'l':
                    do_move(0, 1);
                    break;
                case 'y':
                    do_move(-1, -1);
                    break;
                case 'u':
                    do_move(-1, 1);
                    break;
                case 'b':
                    do_move(1, -1);
                    break;
                case 'n':
                    do_move(1, 1);
                    break;
                case 'H':
                    do_run('h');
                    break;
                case 'J':
                    do_run('j');
                    break;
                case 'K':
                    do_run('k');
                    break;
                case 'L':
                    do_run('l');
                    break;
                case 'Y':
                    do_run('y');
                    break;
                case 'U':
                    do_run('u');
                    break;
                case 'B':
                    do_run('b');
                    break;
                case 'N':
                    do_run('n');
                    break;
                case 't':
                    if(!get_dir()) {
                        game.after = FALSE;
                    } else {
                        missile(game.delta.y, game.delta.x);
                    }
                    break;
                case 'Q':
                    game.after = FALSE;
                    quit(0);
                    break;
                case 'i':
                    game.after = FALSE;
                    inventory(rogue.pack(), 0);
                    break;
                case 'I':
                    game.after = FALSE;
                    picky_inven();
                    break;
                case 'd':
                    drop();
                    break;
                case 'q':
                    quaff();
                    break;
                case 'r':
                    read_scroll();
                    break;
                case 'e':
                    eat();
                    break;
                case 'w':
                    wield();
                    break;
                case 'W':
                    wear();
                    break;
                case 'T':
                    take_off();
                    break;
                case 'P':
                    ring_on();
                    break;
                case 'R':
                    ring_off();
                    break;
                case 'o':
                    option();
                    break;
                case 'c':
                    call();
                    break;
                case '>':
                    game.after = FALSE;
                    d_level();
                    break;
                case '<':
                    game.after = FALSE;
                    u_level();
                    break;
                case '?':
                    game.after = FALSE;
                    help();
                    break;
                case '/':
                    game.after = FALSE;
                    identify();
                    break;
                case 's':
                    search();
                    break;
                case 'z':
                    do_zap(FALSE);
                    break;
                case 'p':
                    if(get_dir()) {
                        do_zap(TRUE);
                    } else {
                        game.after = FALSE;
                    }
                    break;
                case 'v':
                    msg("Rogue version %s. (mctesq was here)", game.release.c_str());
                    break;
                case CTRL('L'):
                    game.after = FALSE;
                    clearok(ui.curScr.w, TRUE);
                    draw(ui.curScr.w);
                    break;
                case CTRL('R'):
                    game.after = FALSE;
                    msg(ui.huh);
                    break;
                case 'S':
                    game.after = FALSE;
                    if(save_game()) {
                        wmove(ui.cw.w, LINES - 1, 0);
                        wclrtoeol(ui.cw.w);
                        draw(ui.cw.w);
                        endwin();
                        exit(0);
                    }
                    break;
                case ' ':; /* Rest command */
                    break;
                case CTRL('P'):
                    game.after = FALSE;
                    if(game.wizard) {
                        game.wizard = FALSE;
                        msg("Not wizard any more");
                    } else {
                        if((game.wizard = passwd())) {
                            msg("You are suddenly as smart as Ken Arnold in dungeon #%d",
                                game.dnum);
                            game.wizard = TRUE;
                            game.waswizard = TRUE;
                        } else {
                            msg("Sorry");
                        }
                    }
                    break;
                case Game::ESCAPE: /* Escape */
                    game.door_stop = FALSE;
                    ui.count = 0;
                    game.after = FALSE;
                    break;
                default:
                    game.after = FALSE;
                    if(game.wizard) {
                        switch(ch) {
                            case '@':
                                msg("@ %d,%d", rogue.hero().y, rogue.hero().x);
                                break;
                            case 'C':
                                create_obj();
                                break;
                            case CTRL('I'):
                                inventory(floorMap.lvl_obj, 0);
                                break;
                            case CTRL('W'):
                                whatis();
                                break;
                            case CTRL('D'):
                                game.curr_level++;
                                new_level();
                                break;
                            case CTRL('U'):
                                game.curr_level--;
                                new_level();
                                break;
                            case CTRL('F'):
                                show_win(ui.stdScr.w, "--More (level map)--");
                                break;
                            case CTRL('X'):
                                show_win(ui.mw.w, "--More (monsters)--");
                                break;
                            case CTRL('T'):
                                teleport();
                                break;
                            case CTRL('E'):
                                msg("food left: %d", rogue.food_left);
                                break;
                            case CTRL('A'):
                                msg("%d things in your pack", rogue.inpack);
                                break;
                            case CTRL('C'):
                                add_pass();
                                break;
                            case CTRL('N'): {
                                linked_list *item;

                                if((item = get_item("charge", STICK)) != nullptr) {
                                    (lobject(item))->o_charges = 10000;
                                }
                            } break;
                            case CTRL('H'): {
                                int i;
                                linked_list *item;
                                object *obj;

                                for(i = 0; i < 9; i++) {
                                    raise_level();
                                }
                                /*
                                 * Give the rogue a sword (+1,+1)
                                 */
                                item = new_item(sizeof *obj);
                                obj = lobject(item);
                                obj->o_type = WEAPON;
                                obj->o_which = TWOSWORD;
                                init_weapon(obj, SWORD);
                                obj->o_hplus = 1;
                                obj->o_dplus = 1;
                                add_pack(item, TRUE);
                                rogue.cur_weapon = obj;
                                /*
                                 * And his suit of armor
                                 */
                                item = new_item(sizeof *obj);
                                obj = lobject(item);
                                obj->o_type = ARMOR;
                                obj->o_which = PLATE_MAIL;
                                obj->o_ac = -5;
                                obj->o_flags |= ISKNOW;
                                rogue.cur_armor = obj;
                                add_pack(item, TRUE);
                            } break;
                            default:
                                msg("Illegal command '%s'.", unctrl_int(ch));
                                ui.count = 0;
                        }
                    } else {
                        msg("Illegal command '%s'.", unctrl_int(ch));
                        ui.count = 0;
                    }
            }
            /*
             * turn off flags if no longer needed
             */
            if(!game.running) {
                game.door_stop = FALSE;
            }
        }
        /*
         * If he ran into something to take, let him pick it up.
         */
        if(game.take != 0) {
            pick_up(game.take);
        }
        if(!game.running) {
            game.door_stop = FALSE;
        }
        if(!game.after) {
            ntimes++;
        }
    }
    /*
     * Kick off the rest if the daemons and fuses
     */
    if(game.after) {
        look(FALSE);
        daemons.doDaemons(RogueDaemons::After);
        daemons.doFuses(RogueDaemons::After);
        if(rogue.isRing(Rogue::LEFT, R_SEARCH)) {
            search();
        } else if(rogue.isRing(Rogue::LEFT, R_TELEPORT) && rnd(100) < 2) {
            teleport();
        }
        if(rogue.isRing(Rogue::RIGHT, R_SEARCH)) {
            search();
        } else if(rogue.isRing(Rogue::RIGHT, R_TELEPORT) && rnd(100) < 2) {
            teleport();
        }
    }
}

/*
 * quit:
 *     Have player make certain, then exit.
 */

void quit(const int p) {
    /* Reset the signal in case we got here via an interrupt */
    if(signal(SIGINT, quit) != &quit) {
        ui.mpos = 0;
    }
    msg("Really quit?");
    draw(ui.cw.w);
    if(readchar(ui.cw.w) == 'y') {
        clear();
        move(LINES - 1, 0);
        draw(ui.stdScr.w);
        endwin();
        score(rogue.purse, 1, 0);
        exit(p);
    } else {
        signal(SIGINT, quit);
        wmove(ui.cw.w, 0, 0);
        wclrtoeol(ui.cw.w);
        status();
        draw(ui.cw.w);
        ui.mpos = 0;
        ui.count = 0;
    }
}

/*
 * search:
 *     Player gropes about him to find hidden things.
 */

void search() {
    /*
     * Look all around the hero, if there is something hidden there,
     * give him a chance to find it.  If its found, display it.
     */
    if(on(rogue.player, ISBLIND)) {
        return;
    }
    for(int x = rogue.hero().x - 1; x <= rogue.hero().x + 1; x++) {
        for(int y = rogue.hero().y - 1; y <= rogue.hero().y + 1; y++) {
            auto ch = ui.winat(y, x);
            switch(ch) {
                case SECRETDOOR:
                    if(rnd(100) < 20) {
                        mvaddch(y, x, DOOR);
                        ui.count = 0;
                    }
                    break;
                case TRAP: {
                    if(mvwinch(ui.cw.w, y, x) == TRAP) {
                        break;
                    }
                    if(rnd(100) > 50) {
                        break;
                    }

                    trap *tp = trap_at(y, x);
                    tp->tr_flags |= ISFOUND;
                    mvwaddch(ui.cw.w, y, x, TRAP);
                    ui.count = 0;
                    game.running = FALSE;
                    msg(trapName(tp->tr_type));
                }
            }
        }
    }
}

/*
 * help:
 *     Give single character help, or the whole mess if he wants it
 */

void help() {
    msg("Character you want help for (* for all): ");
    int helpch = readchar(ui.cw.w);
    ui.mpos = 0;
    /*
     * If its not a *, print the right help string
     * or an error if he typed a funny character.
     */
    if(helpch != '*') {
        wmove(ui.cw.w, 0, 0);
        for(const auto &strp : game.helpstr) {
            if(strp.h_ch == helpch) {
                msg("%s%s", unctrl_int(strp.h_ch), strp.h_desc.c_str());
                return;
            }
        }
        msg("Unknown character '%s'", unctrl_int(helpch));
        return;
    }
    /*
     * Here we print help for everything.
     * Then wait before we return to command mode
     */
    ui.hw.clearWindow();
    int cnt = 0;
    for(const auto &strp : game.helpstr) {
        mvwaddstr(ui.hw.w, cnt % 23, cnt > 22 ? 40 : 0, unctrl_int(strp.h_ch));
        waddstr(ui.hw.w, strp.h_desc.c_str());
        cnt++;
    }
    wmove(ui.hw.w, LINES - 1, 0);
    wprintw(ui.hw.w, "--Press space to continue--");
    draw(ui.hw.w);
    wait_for(ui.hw.w, ' ');
    ui.hw.clearWindow();
    draw(ui.hw.w);
    wmove(ui.cw.w, 0, 0);
    wclrtoeol(ui.cw.w);
    status();
    touchwin(ui.cw.w);
}

/*
 * identify:
 *     Tell the player what a certain thing is.
 */

void identify() {
    msg("What do you want identified? ");
    int ch = readchar(ui.cw.w);
    ui.mpos = 0;
    if(ch == Game::ESCAPE) {
        msg("");
        return;
    }

    const char *str = nullptr;
    if(isalpha(ch) && isupper(ch)) {
        str = Game::monsters[(size_t)ch - 'A'].m_name;
    } else {
        switch(ch) {
            case '|':
            case '-':
                str = "wall of a room";
                break;
            case GOLD:
                str = "gold";
                break;
            case STAIRS:
                str = "passage leading down";
                break;
            case DOOR:
                str = "door";
                break;
            case FLOOR:
                str = "room floor";
                break;
            case PLAYER:
                str = "you";
                break;
            case PASSAGE:
                str = "passage";
                break;
            case TRAP:
                str = "trap";
                break;
            case POTION:
                str = "potion";
                break;
            case SCROLL:
                str = "scroll";
                break;
            case FOOD:
                str = "food";
                break;
            case WEAPON:
                str = "weapon";
                break;
            case ' ':
                str = "solid rock";
                break;
            case ARMOR:
                str = "armor";
                break;
            case AMULET:
                str = "The Amulet of Yendor";
                break;
            case RING:
                str = "ring";
                break;
            case STICK:
                str = "wand or staff";
                break;
            default:
                str = "unknown character";
        }
    }
    msg("'%s' : %s", unctrl_int(ch), str);
}

/*
 * d_level:
 *     He wants to go down a level
 */

void d_level() {
    if(ui.winat(rogue.hero().y, rogue.hero().x) != STAIRS) {
        msg("I see no way down.");
    } else {
        game.curr_level++;
        new_level();
    }
}

/*
 * u_level:
 *     He wants to go up a level
 */

void u_level() {
    if(ui.winat(rogue.hero().y, rogue.hero().x) == STAIRS) {
        if(game.amulet) {
            game.curr_level--;
            if(game.curr_level == 0) {
                total_winner();
            }
            new_level();
            msg("You feel a wrenching sensation in your gut.");
            return;
        }
    }
    msg("I see no way up.");
}

template <size_t T>
void callit_1(object *obj, array<string, T> &guess, array<int, T> &know, const string &elsewise) {
    if(know[(size_t)obj->o_which]) {
        msg("That has already been identified");
        return;
    }
    if(game.terse) {
        addmsg("C");
    } else {
        addmsg("Was c");
    }
    msg("alled \"%s\"", elsewise.c_str());
    if(game.terse) {
        msg("Call it: ");
    } else {
        msg("What do you want to call it? ");
    }
    strcpy(ui.prbuf, elsewise.c_str());
    if(get_str(ui.prbuf, ui.cw.w) == UI::NORM) {
        guess[(size_t)obj->o_which] = ui.prbuf;
    }
}

template <size_t T>
void callit_1(object *obj, array<char *, T> &guess, array<int, T> &know, const string &elsewise) {
    if(know[(size_t)obj->o_which]) {
        msg("That has already been identified");
        return;
    }
    if(game.terse) {
        addmsg("C");
    } else {
        addmsg("Was c");
    }
    msg("alled \"%s\"", elsewise.c_str());
    if(game.terse) {
        msg("Call it: ");
    } else {
        msg("What do you want to call it? ");
    }
    strcpy(ui.prbuf, elsewise.c_str());
    if(get_str(ui.prbuf, ui.cw.w) == UI::NORM) {
        if(guess[(size_t)obj->o_which] != NULL) {
            free(guess[(size_t)obj->o_which]);
        }
        guess[(size_t)obj->o_which] = (char *)malloc(strlen(ui.prbuf) + 1);
        if(guess[(size_t)obj->o_which] != NULL) {
            strcpy(guess[(size_t)obj->o_which], ui.prbuf);
        }
    }
}
/*
 * allow a user to call a potion, scroll, or ring something
 */
void call() {
    string elsewise;

    linked_list *item = get_item("call", CALLABLE);
    /*
     * Make certain that it is somethings that we want to wear
     */
    if(item == nullptr) {
        return;
    }
    auto *obj = lobject(item);
    switch(obj->o_type) {
        case RING:
            elsewise = (!items.r_guess[(size_t)obj->o_which].empty()
                            ? items.r_guess[(size_t)obj->o_which]
                            : items.r_stones[(size_t)obj->o_which]);
            callit_1(obj, items.r_guess, items.r_know, elsewise);
            break;
        case POTION:
            elsewise = (!items.p_guess[(size_t)obj->o_which].empty()
                            ? items.p_guess[(size_t)obj->o_which]
                            : items.p_colors[(size_t)obj->o_which]);
            callit_1(obj, items.p_guess, items.p_know, elsewise);
            break;
        case SCROLL:
            elsewise = (!items.s_guess[(size_t)obj->o_which].empty()
                            ? items.s_guess[(size_t)obj->o_which]
                            : items.s_names[(size_t)obj->o_which]);
            callit_1(obj, items.s_guess, items.s_know, elsewise);
            break;
        case STICK:
            elsewise = (!items.ws_guess[(size_t)obj->o_which].empty()
                            ? items.ws_guess[(size_t)obj->o_which]
                            : items.ws_made[(size_t)obj->o_which]);
            callit_1(obj, items.ws_guess, items.ws_know, elsewise);
            break;
        default:
            msg("You can't call that anything");
            return;
    }
}
