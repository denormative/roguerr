/*
 * File with various monster functions in it
 *
 * @(#)monsters.c     3.18 (Berkeley) 6/15/81
 *
 * Rogue: Exploring the Dungeons of Doom
 * Copyright (C) 1980, 1981 Michael Toy, Ken Arnold and Glenn Wichman
 * All rights reserved.
 *
 * See the file LICENSE.TXT for full copyright and licensing information.
 */

#include "rogue.h"

/*
 * List of monsters in rough order of vorpalness
 */
char Game::lvl_mons[27] = "KJBSHEAOZGLCRQNYTWFIXUMVDP";
char Game::wand_mons[27] = "KJBSH AOZG CRQ Y W IXU V  ";

/*
 * randmonster:
 *     Pick a monster to show up.  The lower the level,
 *     the meaner the monster.
 */

int randmonster(const int wander) {
    int d;

    char *mons = wander ? Game::wand_mons : Game::lvl_mons;
    do {
        d = game.curr_level + (rnd(10) - 5);
        if(d < 1) {
            d = rnd(5) + 1;
        }
        if(d > 26) {
            d = rnd(5) + 22;
        }
    } while(mons[--d] == ' ');
    return mons[d];
}

/*
 * new_monster:
 *     Pick a new monster and add it to the list
 */

void new_monster(linked_list *item, const int type, coord &cp) {
    attach(floorMap.mlist, item);

    auto *tp = lthing(item);
    tp->t_type = type;
    tp->t_pos = cp;
    tp->t_oldch = (int)mvwinch(ui.cw.w, cp.y, cp.x);
    mvwaddch_int(ui.mw.w, cp.y, cp.x, tp->t_type);

    monster *mp = &Game::monsters[(size_t)tp->t_type - 'A'];
    tp->t_stats.s_hpt = roll(mp->m_stats.s_lvl, 8);
    tp->t_stats.s_lvl = mp->m_stats.s_lvl;
    tp->t_stats.s_arm = mp->m_stats.s_arm;
    strcpy(tp->t_stats.s_dmg, mp->m_stats.s_dmg);
    tp->t_stats.s_exp = mp->m_stats.s_exp;
    tp->t_stats.s_str.st_str = 10;
    tp->t_flags = mp->m_flags;
    tp->t_turn = TRUE;
    tp->t_pack = nullptr;
    if(rogue.isWearing(R_AGGR)) {
        runto(cp, hero_pos);
    }
    if(type == 'M') {
        int mch = 0;

        if(tp->t_pack != nullptr) {
            mch = (lobject(tp->t_pack))->o_type;
        } else {
            switch(rnd(game.curr_level > 25 ? 9 : 8)) {
                case 0:
                    mch = GOLD;
                    break;
                case 1:
                    mch = POTION;
                    break;
                case 2:
                    mch = SCROLL;
                    break;
                case 3:
                    mch = STAIRS;
                    break;
                case 4:
                    mch = WEAPON;
                    break;
                case 5:
                    mch = ARMOR;
                    break;
                case 6:
                    mch = RING;
                    break;
                case 7:
                    mch = STICK;
                    break;
                case 8:
                    mch = AMULET;
            }
        }
        tp->t_disguise = mch;
    }
}

/*
 * wanderer:
 *     A wandering monster has awakened and is headed for the player
 */

void wanderer() {
    int ch = '\0';
    room *rp;
    room *hr = roomin(rogue.hero());
    thing *tp;
    coord cp;

    linked_list *item = new_item(sizeof *tp);
    do {
        int i = rnd_room();
        if((rp = &floorMap.rooms[i]) == hr) {
            continue;
        }
        rnd_pos(rp, cp);
        if((ch = (int)mvwinch(ui.stdScr.w, cp.y, cp.x)) == ERR) {
            debug("Routine wanderer: mvwinch failed to %d,%d", cp.y, cp.x);
            if(game.wizard) {
                wait_for(ui.cw.w, '\n');
            }
            return;
        }
    } while(!(hr != rp && step_ok(ch)));
    new_monster(item, randmonster(TRUE), cp);
    tp = lthing(item);
    tp->t_flags |= ISRUN;
    tp->t_pos = cp;
    tp->t_dest = hero_pos;
    if(game.wizard) {
        msg("Started a wandering %s", Game::monsters[(size_t)tp->t_type - 'A'].m_name);
    }
}

/* what to do when the hero steps next to a monster */
linked_list *wake_monster(const int y, const int x) {
    linked_list *it;

    if((it = find_mons(y, x)) == nullptr) {
        fatal("Can't find monster in wake");
    }

    auto *tp = lthing(it);
    int ch = tp->t_type;

    /* Every time he sees mean monster, it might start chasing him */
    if(rnd(100) > 33 && on(*tp, ISMEAN) && off(*tp, ISHELD) && !rogue.isWearing(R_STEALTH)) {
        tp->t_dest = hero_pos;
        tp->t_flags |= ISRUN;
    }
    if(ch == 'U' && off(rogue.player, ISBLIND)) {
        room *rp = roomin(rogue.hero());
        if((rp != nullptr && !(rp->r_flags & ISDARK)) ||
           distance(y, x, rogue.hero().y, rogue.hero().x) < 3) {
            if(off(*tp, ISFOUND) && !save(VS_MAGIC)) {
                msg("The umber hulk's gaze has confused you.");
                if(on(rogue.player, ISHUH)) {
                    daemons.lengthen(d_unconfuse, rnd(20) + Game::HUHDURATION);
                } else {
                    daemons.startFuse(d_unconfuse, 0, rnd(20) + Game::HUHDURATION,
                                      RogueDaemons::After);
                }
                rogue.player.t_flags |= ISHUH;
            }
            tp->t_flags |= ISFOUND;
        }
    }
    /* Hide invisible monsters */
    if(on(*tp, ISINVIS) && off(rogue.player, CANSEE)) {
        ch = (int)mvwinch(ui.stdScr.w, y, x);
    }
    /* Let greedy ones guard gold */
    if(on(*tp, ISGREED) && off(*tp, ISRUN)) {
        room *rp = roomin(rogue.hero());

        if(rp != nullptr && rp->r_goldval) {
            tp->t_dest = rp->r_gold;
            tp->t_flags |= ISRUN;
        }
    }

    return it;
}

void genocide() {
    addmsg("Which monster");
    if(!game.terse) {
        addmsg(" do you wish to wipe out");
    }
    msg("? ");

    int c;
    while(!isalpha(c = readchar(ui.cw.w))) {
        if(c == Game::ESCAPE) {
            return;
        }
        ui.mpos = 0;
        msg("Please specify a letter between 'A' and 'Z'");
    }
    if(islower(c)) {
        c = toupper(c);
    }
    linked_list *nip;
    for(linked_list *ip = floorMap.mlist; ip; ip = nip) {
        auto *mp = lthing(ip);
        nip = next_p(ip);
        if(mp->t_type == c) {
            remove_monster(mp->t_pos, ip);
        }
    }
    for(int i = 0; i < 26; i++) {
        if(Game::lvl_mons[i] == c) {
            Game::lvl_mons[i] = ' ';
            Game::wand_mons[i] = ' ';
            break;
        }
    }
}
