/*
 *      @(#)potions.c     3.1     3.1     5/7/81
 * Function(s) for dealing with potions
 *
 * Rogue: Exploring the Dungeons of Doom
 * Copyright (C) 1980, 1981 Michael Toy, Ken Arnold and Glenn Wichman
 * All rights reserved.
 *
 * See the file LICENSE.TXT for full copyright and licensing information.
 */

#include "rogue.h"

void quaff() {
    linked_list *item = get_item("quaff", POTION);
    /* Make certain that it is somethings that we want to drink */
    if(item == nullptr) {
        return;
    }
    auto *obj = lobject(item);
    if(obj->o_type != POTION) {
        if(!game.terse) {
            msg("Yuk! Why would you want to drink that?");
        } else {
            msg("That's undrinkable");
        }
        return;
    }
    if(obj == rogue.cur_weapon) {
        rogue.cur_weapon = nullptr;
    }

    /* Calculate the effect it has on the poor guy. */
    switch(obj->o_which) {
        case P_CONFUSE:
            if(off(rogue.player, ISHUH)) {
                msg("Wait, what's going on here. Huh? What? Who?");
            }

            if(on(rogue.player, ISHUH)) {
                daemons.lengthen(d_unconfuse, rnd(8) + Game::HUHDURATION);
            } else {
                daemons.startFuse(d_unconfuse, 0, rnd(8) + Game::HUHDURATION, RogueDaemons::After);
            }

            rogue.player.t_flags |= ISHUH;
            items.p_know[P_CONFUSE] = TRUE;
            break;
        case P_POISON:
            if(!rogue.isWearing(R_SUSTSTR)) {
                chg_str(-(rnd(3) + 1));
                msg("You feel very sick now.");
            } else {
                msg("You feel momentarily sick");
            }
            items.p_know[P_POISON] = TRUE;
            break;
        case P_HEALING:
            if((rogue.pstats().s_hpt += roll(rogue.pstats().s_lvl, 4)) > rogue.max_hp) {
                rogue.pstats().s_hpt = ++rogue.max_hp;
            }
            msg("You begin to feel better.");
            sight();
            items.p_know[P_HEALING] = TRUE;
            break;
        case P_STRENGTH:
            msg("You feel stronger, now.  What bulging muscles!");
            chg_str(1);
            items.p_know[P_STRENGTH] = TRUE;
            break;
        case P_MFIND:
            /* Potion of monster detection, if there are monters, detect them */
            if(floorMap.mlist != nullptr) {
                ui.hw.clearWindow();
                overwrite(ui.mw.w, ui.hw.w);
                show_win(ui.hw.w, "You begin to sense the presence of monsters.--More--");
                items.p_know[P_MFIND] = TRUE;
            } else {
                msg("You have a strange feeling for a moment, then it passes.");
            }
            break;
        case P_TFIND:
            /* Potion of magic detection.  Show the potions and scrolls */
            if(floorMap.lvl_obj != nullptr) {
                int show = FALSE;
                ui.hw.clearWindow();
                for(linked_list *mobj = floorMap.lvl_obj; mobj != nullptr; mobj = next_p(mobj)) {
                    auto *tp = lobject(mobj);
                    if(is_magic(tp)) {
                        show = TRUE;
                        mvwaddch(ui.hw.w, tp->o_pos.y, tp->o_pos.x, MAGIC);
                    }
                    items.p_know[P_TFIND] = TRUE;
                }
                for(linked_list *titem = floorMap.mlist; titem != nullptr; titem = next_p(titem)) {
                    auto *th = lthing(titem);
                    for(linked_list *pitem = th->t_pack; pitem != nullptr; pitem = next_p(pitem)) {
                        if(is_magic(OBJPTR(pitem))) {
                            show = TRUE;
                            mvwaddch(ui.hw.w, th->t_pos.y, th->t_pos.x, MAGIC);
                        }
                        items.p_know[P_TFIND] = TRUE;
                    }
                }
                if(show) {
                    show_win(ui.hw.w, "You sense the presence of magic on this level.--More--");
                    break;
                }
            }
            msg("You have a strange feeling for a moment, then it passes.");
            break;
        case P_PARALYZE:
            msg("You can't move.");
            rogue.no_command = Game::HOLDTIME;
            items.p_know[P_PARALYZE] = TRUE;
            break;
        case P_SEEINVIS:
            msg("This potion tastes like %s juice.", game.fruit);
            if(off(rogue.player, CANSEE)) {
                rogue.player.t_flags |= CANSEE;
                daemons.startFuse(d_unsee, 0, Game::SEEDURATION, RogueDaemons::After);
                light(rogue.hero());
            }
            sight();
            break;
        case P_RAISE:
            msg("You suddenly feel much more skillful");
            items.p_know[P_RAISE] = TRUE;
            raise_level();
            break;
        case P_XHEAL:
            if((rogue.pstats().s_hpt += roll(rogue.pstats().s_lvl, 8)) > rogue.max_hp) {
                rogue.pstats().s_hpt = ++rogue.max_hp;
            }
            msg("You begin to feel much better.");
            items.p_know[P_XHEAL] = TRUE;
            sight();
            break;
        case P_HASTE:
            add_haste(TRUE);
            msg("You feel yourself moving much faster.");
            items.p_know[P_HASTE] = TRUE;
            break;
        case P_RESTORE:
            msg("Hey, this tastes great.  It make you feel warm all over.");
            if(rogue.pstats().s_str.st_str < rogue.max_stats.s_str.st_str ||
               (rogue.pstats().s_str.st_str == 18 &&
                rogue.pstats().s_str.st_add < rogue.max_stats.s_str.st_add)) {
                rogue.pstats().s_str = rogue.max_stats.s_str;
            }
            break;
        case P_BLIND:
            msg("A cloak of darkness falls around you.");
            if(off(rogue.player, ISBLIND)) {
                rogue.player.t_flags |= ISBLIND;
                daemons.startFuse(d_sight, 0, Game::SEEDURATION, RogueDaemons::After);
                look(FALSE);
            }
            items.p_know[P_BLIND] = TRUE;
            break;
        case P_NOP:
            msg("This potion tastes extremely dull.");
            break;
        default:
            msg("What an odd tasting potion!");
            return;
    }
    status();
    if(items.p_know[(size_t)obj->o_which] && !items.p_guess[(size_t)obj->o_which].empty()) {
        items.p_guess[(size_t)obj->o_which].clear();
    } else if(!items.p_know[(size_t)obj->o_which] && game.askme &&
              items.p_guess[(size_t)obj->o_which].empty()) {
        msg(game.terse ? "Call it: " : "What do you want to call it? ");
        char buf[80];
        if(get_str(buf, ui.cw.w) == UI::NORM) {
            items.p_guess[(size_t)obj->o_which] = buf;
        }
    }
    /* Throw the item away */
    rogue.inpack--;
    if(obj->o_count > 1) {
        obj->o_count--;
    } else {
        detach(rogue.pack(), item);
        discard(item);
    }
}
