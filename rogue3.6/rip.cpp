/*
 * File for the fun ends
 * Death or a total win
 *
 * @(#)rip.c     3.13 (Berkeley) 6/16/81
 *
 * Rogue: Exploring the Dungeons of Doom
 * Copyright (C) 1980, 1981 Michael Toy, Ken Arnold and Glenn Wichman
 * All rights reserved.
 *
 * See the file LICENSE.TXT for full copyright and licensing information.
 */

#include "Scores.h"
#include <fmt/format.h>
#include "rogue.h"

/*
 * death:
 *     Do something really fun when he dies
 */

[[noreturn]] void death(const int monst) {
    // clang-format off
    static const char *rip[] = {
        "                       __________",
        "                      /          \\",
        "                     /    REST    \\",
        "                    /      IN      \\",
        "                   /     PEACE      \\",
        "                  /                  \\",
        "                  |                  |",
        "                  |                  |",
        "                  |   killed by a    |",
        "                  |                  |",
        "                  |       1980       |",
        "                 *|     *  *  *      | *",
     R"(         ________)/\\_//(\/(/\)/\//\/|_)_______)",
        nullptr
    };
    // clang-format on

    const char **dp = rip;

    time_t date;
    time(&date);
    tm *lt = localtime(&date);

    clear();
    move(8, 0);
    while(*dp) {
        printw("%s\n", *dp++);
    }
    mvaddstr(14, 28 - (int)((strlen(game.whoami) + 1) / 2), game.whoami);
    rogue.purse -= rogue.purse / 10;
    char buf[80];
    sprintf(buf, "%d Au", rogue.purse);
    mvaddstr(15, 28 - (int)((strlen(buf) + 1) / 2), buf);
    const char *killer = killname(monst);
    mvaddstr(17, 28 - (int)((strlen(killer) + 1) / 2), killer);
    mvaddstr(16, 33, vowelstr(killer).c_str());
    sprintf(ui.prbuf, "%4d", 1900 + lt->tm_year);
    mvaddstr(18, 26, ui.prbuf);
    move(LINES - 1, 0);
    draw(ui.stdScr.w);
    score(rogue.purse, 0, monst);
    exit(0);
}

ScoreEntry recordScore(const int amount, const int flags, const int monst) {
    ScoreEntry entry{};

    static const char *reason[] = {
        "killed",
        "quit",
        "A total winner",
    };

    entry.sc_score = amount;
    entry.sc_name = md_getusername();
    if(flags == 2) {
        entry.sc_level = game.max_level;
    } else {
        entry.sc_level = game.curr_level;
    }
    entry.sc_flags = flags;
    entry.sc_reason = reason[flags];
    entry.sc_reason += fmt::format(" on level {}", entry.sc_level);
    if(flags == 0) {
        const char *killer = killname(monst);
        if(*killer == 'a' || *killer == 'e' || *killer == 'i' || *killer == 'o' || *killer == 'u') {
            entry.sc_reason += " by an ";
        } else {
            entry.sc_reason += " by a ";
        }
        entry.sc_reason += killer;
    }
    entry.sc_monster = monst;
    entry.was_wizard = game.waswizard;
    // TODO: probably should note user was wizard on scoreboard

    return entry;
}

void printScores(const Scores &scores) {
    printf("\n\nTop Ten Adventurers:\nRank\tScore\tName\n");
    int rank = 1;
    for(auto &e : scores.scores) {
        printf("%d\t%ld\t%s: %s.\n", rank, e.sc_score, e.sc_name.c_str(), e.sc_reason.c_str());
        rank++;
    }
}

void score(const int amount, const int flags, const int monst) {
    signal(SIGINT, SIG_DFL);
    if((flags != -1) && (flags != 1)) {
        mvaddstr(LINES - 1, 0, "[Press return to continue]");
        draw(ui.stdScr.w);
        ui.prbuf[0] = 0;
        get_str(ui.prbuf, ui.stdScr.w);
        endwin();
    }

    Scores scores("rogue36.scores");
    scores.load();

    ScoreEntry entry = recordScore(amount, flags, monst);

    scores.addScore(entry);
    scores.save();

    printScores(scores);
}

[[noreturn]] void total_winner() {
    clear();
    standout();
    addstr("                                                               \n");
    addstr("  @   @               @   @           @          @@@  @     @  \n");
    addstr("  @   @               @@ @@           @           @   @     @  \n");
    addstr("  @   @  @@@  @   @   @ @ @  @@@   @@@@  @@@      @  @@@    @  \n");
    addstr("   @@@@ @   @ @   @   @   @     @ @   @ @   @     @   @     @  \n");
    addstr("      @ @   @ @   @   @   @  @@@@ @   @ @@@@@     @   @     @  \n");
    addstr("  @   @ @   @ @  @@   @   @ @   @ @   @ @         @   @  @     \n");
    addstr("   @@@   @@@   @@ @   @   @  @@@@  @@@@  @@@     @@@   @@   @  \n");
    addstr("                                                               \n");
    addstr("     Congratulations, you have made it to the light of day!    \n");
    standend();
    addstr("\nYou have joined the elite ranks of those who have escaped the\n");
    addstr("Dungeons of Doom alive.  You journey home and sell all your loot at\n");
    addstr("a great profit and are admitted to the fighters guild.\n");
    mvaddstr(LINES - 1, 0, "--Press space to continue--");
    refresh();
    wait_for(ui.stdScr.w, ' ');
    clear();
    mvaddstr(0, 0, "   Worth  Item");
    int oldpurse = rogue.purse;
    int worth = 0;
    linked_list *item;
    int c;
    for(c = 'a', item = rogue.pack(); item != nullptr; c++, item = next_p(item)) {
        auto *obj = lobject(item);
        switch(obj->o_type) {
            case FOOD:
                worth = 2 * obj->o_count;
                break;
            case WEAPON:
                switch(obj->o_which) {
                    case MACE:
                        worth = 8;
                        break;
                    case SWORD:
                        worth = 15;
                        break;
                    case BOW:
                        worth = 75;
                        break;
                    case ARROW:
                        worth = 1;
                        break;
                    case DAGGER:
                        worth = 2;
                        break;
                    case ROCK:
                        worth = 1;
                        break;
                    case TWOSWORD:
                        worth = 30;
                        break;
                    case SLING:
                        worth = 1;
                        break;
                    case DART:
                        worth = 1;
                        break;
                    case CROSSBOW:
                        worth = 15;
                        break;
                    case BOLT:
                        worth = 1;
                        break;
                    case SPEAR:
                        worth = 2;
                        break;
                    default:
                        worth = 0;
                }
                worth *= (1 + (10 * obj->o_hplus + 10 * obj->o_dplus));
                worth *= obj->o_count;
                obj->o_flags |= ISKNOW;
                break;
            case ARMOR:
                switch(obj->o_which) {
                    case LEATHER:
                        worth = 5;
                        break;
                    case RING_MAIL:
                        worth = 30;
                        break;
                    case STUDDED_LEATHER:
                        worth = 15;
                        break;
                    case SCALE_MAIL:
                        worth = 3;
                        break;
                    case CHAIN_MAIL:
                        worth = 75;
                        break;
                    case SPLINT_MAIL:
                        worth = 80;
                        break;
                    case BANDED_MAIL:
                        worth = 90;
                        break;
                    case PLATE_MAIL:
                        worth = 400;
                        break;
                    default:
                        worth = 0;
                }
                if(obj->o_which >= MAXARMORS) {
                    break;
                }
                worth *= (1 + (10 * (Items::a_class[(size_t)obj->o_which] - obj->o_ac)));
                obj->o_flags |= ISKNOW;
                break;
            case SCROLL:
                items.s_know[(size_t)obj->o_which] = TRUE;
                worth = Items::s_magic[(size_t)obj->o_which].mi_worth;
                worth *= obj->o_count;
                break;
            case POTION:
                items.p_know[(size_t)obj->o_which] = TRUE;
                worth = Items::p_magic[(size_t)obj->o_which].mi_worth;
                worth *= obj->o_count;
                break;
            case RING:
                obj->o_flags |= ISKNOW;
                items.r_know[(size_t)obj->o_which] = TRUE;
                worth = Items::r_magic[(size_t)obj->o_which].mi_worth;
                if(obj->o_which == R_ADDSTR || obj->o_which == R_ADDDAM ||
                   obj->o_which == R_PROTECT || obj->o_which == R_ADDHIT) {
                    if(obj->o_ac > 0) {
                        worth += obj->o_ac * 20;
                    } else {
                        worth = 50;
                    }
                }
                break;
            case STICK:
                obj->o_flags |= ISKNOW;
                items.ws_know[(size_t)obj->o_which] = TRUE;
                worth = Items::ws_magic[(size_t)obj->o_which].mi_worth;
                worth += 20 * obj->o_charges;
                break;
            case AMULET:
                worth = 1000;
        }
        mvprintw(c - 'a' + 1, 0, "%c) %5d  %s", c, worth, inv_name(obj, FALSE));
        rogue.purse += worth;
    }
    mvprintw(c - 'a' + 1, 0, "   %5d  Gold Peices          ", oldpurse);
    refresh();
    score(rogue.purse, 2, 0);
    // wait for keypress
    readchar(ui.cw.w);
    exit(0);
}

const char *killname(const int monst) {
    if(isupper(monst)) {
        return Game::monsters[(size_t)monst - 'A'].m_name;
    }
    switch(monst) {
        case 'a':
            return "arrow";
        case 'd':
            return "dart";
        case 'b':
            return "bolt";
    }

    return ("");
}
