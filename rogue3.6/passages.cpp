/*
 * Draw the connecting passages
 *
 * @(#)passages.c     3.4 (Berkeley) 6/15/81
 *
 * Rogue: Exploring the Dungeons of Doom
 * Copyright (C) 1980, 1981 Michael Toy, Ken Arnold and Glenn Wichman
 * All rights reserved.
 *
 * See the file LICENSE.TXT for full copyright and licensing information.
 */

#include "rogue.h"

/*
 * do_passages:
 *     Draw all the passages on a level.
 */

void do_passages() {
    static struct rdes {
        bool conn[Floor::MAXROOMS];   /* possible to connect to room i? */
        bool isconn[Floor::MAXROOMS]; /* connection been made to room i? */
        bool ingraph;                 /* this room in graph already? */
    } rdes[Floor::MAXROOMS] = {
        {{false, true, false, true, false, false, false, false, false},
         {false, false, false, false, false, false, false, false, false},
         false},
        {{true, false, true, false, true, false, false, false, false},
         {false, false, false, false, false, false, false, false, false},
         false},
        {{false, true, false, false, false, true, false, false, false},
         {false, false, false, false, false, false, false, false, false},
         false},
        {{true, false, false, false, true, false, true, false, false},
         {false, false, false, false, false, false, false, false, false},
         false},
        {{false, true, false, true, false, true, false, true, false},
         {false, false, false, false, false, false, false, false, false},
         false},
        {{false, false, true, false, true, false, false, false, true},
         {false, false, false, false, false, false, false, false, false},
         false},
        {{false, false, false, true, false, false, false, true, false},
         {false, false, false, false, false, false, false, false, false},
         false},
        {{false, false, false, false, true, false, true, false, true},
         {false, false, false, false, false, false, false, false, false},
         false},
        {{false, false, false, false, false, true, false, true, false},
         {false, false, false, false, false, false, false, false, false},
         false},
    };

    /*
     * reinitialize room graph description
     */
    for(struct rdes *r1 = rdes; r1 <= &rdes[Floor::MAXROOMS - 1]; r1++) {
        for(bool &j : r1->isconn) {
            j = FALSE;
        }
        r1->ingraph = FALSE;
    }

    /*
     * starting with one room, connect it to a random adjacent room and
     * then pick a new room to start with.
     */
    int roomcount = 1;
    struct rdes *r1 = &rdes[rnd(Floor::MAXROOMS)];
    struct rdes *r2 = nullptr;
    r1->ingraph = TRUE;
    do {
        /*
         * find a room to connect with
         */
        int j = 0;
        for(int i = 0; i < Floor::MAXROOMS; i++) {
            if(r1->conn[i] && !rdes[i].ingraph && rnd(++j) == 0) {
                r2 = &rdes[i];
            }
        }
        /*
         * if no adjacent rooms are outside the graph, pick a new room
         * to look from
         */
        if(j == 0) {
            do {
                r1 = &rdes[rnd(Floor::MAXROOMS)];
            } while(!(r1->ingraph));
        }
        /*
         * otherwise, connect new room to the graph, and draw a tunnel
         * to it
         */
        else {
            assert(r2 != nullptr);
            r2->ingraph = TRUE;
            int i = (int)(r1 - rdes);
            j = (int)(r2 - rdes);
            conn(i, j);
            r1->isconn[j] = TRUE;
            r2->isconn[i] = TRUE;
            roomcount++;
        }
    } while(roomcount < Floor::MAXROOMS);

    /*
     * attempt to add passages to the graph a random number of times so
     * that there isn't just one unique passage through it.
     */
    for(roomcount = rnd(5); roomcount > 0; roomcount--) {
        r1 = &rdes[rnd(Floor::MAXROOMS)]; /* a random room to look from */
        /*
         * find an adjacent room not already connected
         */
        int j = 0;
        for(int i = 0; i < Floor::MAXROOMS; i++) {
            if(r1->conn[i] && !r1->isconn[i] && rnd(++j) == 0) {
                r2 = &rdes[i];
            }
        }
        /*
         * if there is one, connect it and look for the next added
         * passage
         */
        if(j != 0) {
            int i = (int)(r1 - rdes);
            j = (int)(r2 - rdes);
            conn(i, j);
            r1->isconn[j] = TRUE;
            r2->isconn[i] = TRUE;
        }
    }
}

/*
 * conn:
 *     Draw a corridor from a room in a certain direction.
 */

void conn(const int r1, const int r2) {
    int distance = 0;
    int turn_spot = 0;
    int turn_distance = 0;
    int direc;

    int rm;
    if(r1 < r2) {
        rm = r1;
        if(r1 + 1 == r2) {
            direc = 'r';
        } else {
            direc = 'd';
        }
    } else {
        rm = r2;
        if(r2 + 1 == r1) {
            direc = 'r';
        } else {
            direc = 'd';
        }
    }
    room *rpf = &floorMap.rooms[rm];
    room *rpt = nullptr;
    coord pdelta;
    coord turn_delta;
    coord spos;
    coord epos;
    /*
     * Set up the movement variables, in two cases:
     * first drawing one down.
     */
    if(direc == 'd') {
        const int rmt = rm + 3;     /* room # of dest */
        rpt = &floorMap.rooms[rmt]; /* room pointer of dest */
        pdelta.x = 0;               /* direction of move */
        pdelta.y = 1;
        spos.x = rpf->r_pos.x; /* start of move */
        spos.y = rpf->r_pos.y;
        epos.x = rpt->r_pos.x; /* end of move */
        epos.y = rpt->r_pos.y;
        if(!(rpf->r_flags & ISGONE)) { /* if not gone pick door pos */
            spos.x += rnd(rpf->r_max.x - 2) + 1;
            spos.y += rpf->r_max.y - 1;
        }
        if(!(rpt->r_flags & ISGONE)) {
            epos.x += rnd(rpt->r_max.x - 2) + 1;
        }
        distance = abs(spos.y - epos.y) - 1; /* distance to move */
        turn_delta.y = 0;                    /* direction to turn */
        turn_delta.x = (spos.x < epos.x ? 1 : -1);
        turn_distance = abs(spos.x - epos.x); /* how far to turn */
        turn_spot = rnd(distance - 1) + 1;    /* where turn starts */
    } else if(direc == 'r') {                 /* setup for moving right */
        const int rmt = rm + 1;
        rpt = &floorMap.rooms[rmt];
        pdelta.x = 1;
        pdelta.y = 0;
        spos.x = rpf->r_pos.x;
        spos.y = rpf->r_pos.y;
        epos.x = rpt->r_pos.x;
        epos.y = rpt->r_pos.y;
        if(!(rpf->r_flags & ISGONE)) {
            spos.x += rpf->r_max.x - 1;
            spos.y += rnd(rpf->r_max.y - 2) + 1;
        }
        if(!(rpt->r_flags & ISGONE)) {
            epos.y += rnd(rpt->r_max.y - 2) + 1;
        }
        distance = abs(spos.x - epos.x) - 1;
        turn_delta.y = (spos.y < epos.y ? 1 : -1);
        turn_delta.x = 0;
        turn_distance = abs(spos.y - epos.y);
        turn_spot = rnd(distance - 1) + 1;
    } else {
        fatal("error in connection tables");
    }
    /*
     * Draw in the doors on either side of the passage or just put #'s
     * if the rooms are gone.
     */
    if(!(rpf->r_flags & ISGONE)) {
        door(rpf, spos);
    } else {
        cmov(spos);
        addch('#');
    }
    if(!(rpt->r_flags & ISGONE)) {
        door(rpt, epos);
    } else {
        cmov(epos);
        addch('#');
    }
    /* Get ready to move... */
    coord curr;
    curr.x = spos.x;
    curr.y = spos.y;
    while(distance) {
        /* Move to new position */
        curr.x += pdelta.x;
        curr.y += pdelta.y;
        /* Check if we are at the turn place, if so do the turn */
        if(distance == turn_spot && turn_distance > 0) {
            while(turn_distance--) {
                cmov(curr);
                addch(PASSAGE);
                curr.x += turn_delta.x;
                curr.y += turn_delta.y;
            }
        }
        /* Continue digging along */
        cmov(curr);
        addch(PASSAGE);
        distance--;
    }
    curr.x += pdelta.x;
    curr.y += pdelta.y;
    if(curr != epos) {
        msg("Warning, connectivity problem on this level.");
    }
}

/*
 * Add a door or possibly a secret door
 * also enters the door in the exits array of the room.
 */

void door(room *rm, const coord &cp) {
    cmov(cp);
    addch((rnd(10) < game.curr_level - 1 && rnd(100) < 20 ? SECRETDOOR : DOOR));
    rm->r_exit.push_back(cp);
}

/*
 * add_pass:
 *     add the passages to the current window (wizard command)
 */

void add_pass() {
    for(int y = 1; y < LINES - 2; y++) {
        for(int x = 0; x < COLS; x++) {
            const auto ch = mvinch(y, x);
            if(ch == PASSAGE || ch == DOOR || ch == SECRETDOOR) {
                mvwaddch(ui.cw.w, y, x, ch);
            }
        }
    }
}
