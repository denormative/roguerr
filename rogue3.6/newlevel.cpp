/*
 * new_level:
 *     Dig and draw a new level
 *
 * @(#)new_level.c     3.7 (Berkeley) 6/2/81
 *
 * Rogue: Exploring the Dungeons of Doom
 * Copyright (C) 1980, 1981 Michael Toy, Ken Arnold and Glenn Wichman
 * All rights reserved.
 *
 * See the file LICENSE.TXT for full copyright and licensing information.
 */

#include "rogue.h"

void new_level() {
    int ch = 0;
    coord stairs;

    if(game.curr_level > game.max_level) {
        game.max_level = game.curr_level;
    }
    ui.cw.clearWindow();
    ui.mw.clearWindow();
    clear();
    status();
    /*
     * Free up the monsters on the last level
     */
    free_list(floorMap.mlist);
    do_rooms();    /* Draw rooms */
    do_passages(); /* Draw passages */
    game.no_food++;
    put_things(); /* Place objects (if any) */
    /*
     * Place the staircase down.
     */
    do {
        int rm = rnd_room();
        rnd_pos(&floorMap.rooms[rm], stairs);
    } while(ui.winat(stairs.y, stairs.x) != FLOOR);
    addch(STAIRS);
    /*
     * Place the traps
     */
    if(rnd(10) < game.curr_level) {
        floorMap.ntraps = rnd(game.curr_level / 4) + 1;
        if(floorMap.ntraps > Floor::MAXTRAPS) {
            floorMap.ntraps = Floor::MAXTRAPS;
        }
        int i = floorMap.ntraps;
        while(i--) {
            do {
                int rm = rnd_room();
                rnd_pos(&floorMap.rooms[rm], stairs);
            } while(ui.winat(stairs.y, stairs.x) != FLOOR);
            switch(rnd(6)) {
                case 0:
                    ch = TRAPDOOR;
                    break;
                case 1:
                    ch = BEARTRAP;
                    break;
                case 2:
                    ch = SLEEPTRAP;
                    break;
                case 3:
                    ch = ARROWTRAP;
                    break;
                case 4:
                    ch = TELTRAP;
                    break;
                case 5:
                    ch = DARTTRAP;
            }
            addch(TRAP);
            floorMap.traps[i].tr_type = ch;
            floorMap.traps[i].tr_flags = 0;
            floorMap.traps[i].tr_pos = stairs;
        }
    }
    do {
        int rm = rnd_room();
        rnd_pos(&floorMap.rooms[rm], rogue.hero());
    } while(ui.winat(rogue.hero().y, rogue.hero().x) != FLOOR);
    light(rogue.hero());
    wmove(ui.cw.w, rogue.hero().y, rogue.hero().x);
    waddch(ui.cw.w, PLAYER);
}

/*
 * Pick a room that is really there
 */

int rnd_room() {
    int rm;

    do {
        rm = rnd(Floor::MAXROOMS);
    } while(floorMap.rooms[rm].r_flags & ISGONE);
    return rm;
}

/*
 * put_things:
 *     put potions and scrolls on this level
 */

void put_things() {
    coord tp;

    /*
     * Throw away stuff left on the previous level (if anything)
     */
    free_list(floorMap.lvl_obj);
    /*
     * Once you have found the amulet, the only way to get new stuff is
     * go down into the dungeon.
     */
    if(game.amulet && game.curr_level < game.max_level) {
        return;
    }
    /*
     * Do MAXOBJ attempts to put things on a level
     */
    for(int i = 0; i < Floor::MAXOBJ; i++) {
        if(rnd(100) < 35) {
            /*
             * Pick a new object and link it in the list
             */
            linked_list *item = new_thing();
            attach(floorMap.lvl_obj, item);
            auto *cur = lobject(item);
            /*
             * Put it somewhere
             */
            do {
                int rm = rnd_room();
                rnd_pos(&floorMap.rooms[rm], tp);
            } while(ui.winat(tp.y, tp.x) != FLOOR);
            mvaddch_int(tp.y, tp.x, cur->o_type);
            cur->o_pos = tp;
        }
    }
    /*
     * If he is really deep in the dungeon and he hasn't found the
     * amulet yet, put it somewhere on the ground
     */
    if(game.curr_level > 25 && !game.amulet) {
        object *cur;
        linked_list *item = new_item(sizeof *cur);
        attach(floorMap.lvl_obj, item);
        cur = lobject(item);
        cur->o_hplus = cur->o_dplus = 0;
        strcpy(cur->o_damage, "0d0");
        strcpy(cur->o_hurldmg, "0d0");
        cur->o_ac = 11;
        cur->o_type = AMULET;
        /*
         * Put it somewhere
         */
        do {
            int rm = rnd_room();
            rnd_pos(&floorMap.rooms[rm], tp);
        } while(ui.winat(tp.y, tp.x) != FLOOR);
        mvaddch_int(tp.y, tp.x, cur->o_type);
        cur->o_pos = tp;
    }
}
