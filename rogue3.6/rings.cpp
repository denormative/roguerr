/*
 * routines dealing specifically with rings
 *
 * @(#)rings.c     3.17 (Berkeley) 6/15/81
 *
 * Rogue: Exploring the Dungeons of Doom
 * Copyright (C) 1980, 1981 Michael Toy, Ken Arnold and Glenn Wichman
 * All rights reserved.
 *
 * See the file LICENSE.TXT for full copyright and licensing information.
 */

#include "rogue.h"

void ring_on() {
    linked_list *item = get_item("put on", RING);
    /* Make certain that it is somethings that we want to wear */
    if(item == nullptr) {
        return;
    }
    auto *obj = lobject(item);
    if(obj->o_type != RING) {
        if(!game.terse) {
            msg("It would be difficult to wrap that around a finger");
        } else {
            msg("Not a ring");
        }
        return;
    }

    /* find out which hand to put it on */
    if(is_current(obj)) {
        return;
    }

    int ring;
    if(rogue.cur_ring[Rogue::LEFT] == nullptr && rogue.cur_ring[Rogue::RIGHT] == nullptr) {
        if((ring = gethand()) < 0) {
            return;
        }
    } else if(rogue.cur_ring[Rogue::LEFT] == nullptr) {
        ring = Rogue::LEFT;
    } else if(rogue.cur_ring[Rogue::RIGHT] == nullptr) {
        ring = Rogue::RIGHT;
    } else {
        if(!game.terse) {
            msg("You already have a ring on each hand");
        } else {
            msg("Wearing two");
        }
        return;
    }
    rogue.cur_ring[ring] = obj;

    /* Calculate the effect it has on the poor guy. */
    switch(obj->o_which) {
        case R_ADDSTR:
            str_t save_max;
            save_max = rogue.max_stats.s_str;
            chg_str(obj->o_ac);
            rogue.max_stats.s_str = save_max;
            break;
        case R_SEEINVIS:
            rogue.player.t_flags |= CANSEE;
            light(rogue.hero());
            mvwaddch(ui.cw.w, rogue.hero().y, rogue.hero().x, PLAYER);
            break;
        case R_AGGR:
            aggravate();
            break;
    }
    status();

    if(obj->o_which >= MAXRINGS) {
        return;
    }

    if(items.r_know[(size_t)obj->o_which] && !items.r_guess[(size_t)obj->o_which].empty()) {
        items.r_guess[(size_t)obj->o_which].clear();
    } else if(!items.r_know[(size_t)obj->o_which] && game.askme &&
              items.r_guess[(size_t)obj->o_which].empty()) {
        ui.mpos = 0;
        msg(game.terse ? "Call it: " : "What do you want to call it? ");
        char buf[80];
        if(get_str(buf, ui.cw.w) == UI::NORM) {
            items.r_guess[(size_t)obj->o_which] = buf;
        }
        msg("");
    }
}

void ring_off() {
    int ring;
    if(rogue.cur_ring[Rogue::LEFT] == nullptr && rogue.cur_ring[Rogue::RIGHT] == nullptr) {
        if(game.terse) {
            msg("No rings");
        } else {
            msg("You aren't wearing any rings");
        }
        return;
    }
    if(rogue.cur_ring[Rogue::LEFT] == nullptr) {
        ring = Rogue::RIGHT;
    } else if(rogue.cur_ring[Rogue::RIGHT] == nullptr) {
        ring = Rogue::LEFT;
    } else if((ring = gethand()) < 0) {
        return;
    }
    ui.mpos = 0;
    object *obj = rogue.cur_ring[ring];
    if(obj == nullptr) {
        msg("Not wearing such a ring");
        return;
    }
    if(dropcheck(obj)) {
        msg("Was wearing %s", inv_name(obj, TRUE));
    }
}

int gethand() {
    for(;;) {
        if(game.terse) {
            msg("Left or Right ring? ");
        } else {
            msg("Left hand or right hand? ");
        }
        int c = readchar(ui.cw.w);
        if(c == 'l' || c == 'L') {
            return Rogue::LEFT;
        }
        if(c == 'r' || c == 'R') {
            return Rogue::RIGHT;
        }
        if(c == Game::ESCAPE) {
            return -1;
        }
        ui.mpos = 0;
        if(game.terse) {
            msg("L or R");
        } else {
            msg("Please type L or R");
        }
    }
}

/*
 * how much food does this ring use up?
 */
int ring_eat(const int hand) {
    if(rogue.cur_ring[hand] == nullptr) {
        return 0;
    }
    switch(rogue.cur_ring[hand]->o_which) {
        case R_REGEN:
            return 2;
        case R_SUSTSTR:
            return 1;
        case R_SEARCH:
            return (rnd(100) < 33);
        case R_DIGEST:
            return -(rnd(100) < 50);
        default:
            return 0;
    }
}

/*
 * print ring bonuses
 */
const char *ring_num(const object *obj) {
    static char buf[5];

    if(!(obj->o_flags & ISKNOW)) {
        return "";
    }
    switch(obj->o_which) {
        case R_PROTECT:
        case R_ADDSTR:
        case R_ADDDAM:
        case R_ADDHIT:
            buf[0] = ' ';
            strcpy(&buf[1], num(obj->o_ac, 0));
            break;
        default:
            return "";
    }
    return buf;
}
