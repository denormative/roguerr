/*
 * Various input/output functions
 *
 * @(#)io.c     3.10 (Berkeley) 6/15/81
 *
 * Rogue: Exploring the Dungeons of Doom
 * Copyright (C) 1980, 1981 Michael Toy, Ken Arnold and Glenn Wichman
 * All rights reserved.
 *
 * See the file LICENSE.TXT for full copyright and licensing information.
 */

#include "rogue.h"

/*
 * msg:
 *     Display a message at the top of the screen.
 */

char UI::msgbuf[BUFSIZ];
int UI::newpos = 0;

/*VARARGS1*/
void msg(const char *fmt, ...) {
    va_list ap;
    /*
     * if the string is "", just clear the line
     */
    if(*fmt == '\0') {
        wmove(ui.cw.w, 0, 0);
        wclrtoeol(ui.cw.w);
        ui.mpos = 0;
        return;
    }
    /*
     * otherwise add to the message and flush it out
     */
    va_start(ap, fmt);
    doadd(fmt, ap);
    va_end(ap);
    endmsg();
}

/*
 * add things to the current message
 */
void addmsg(const char *fmt, ...) {
    va_list ap;

    va_start(ap, fmt);
    doadd(fmt, ap);
    va_end(ap);
}

/*
 * Display a new msg (giving him a chance to see the previous one if it
 * is up there with the --More--)
 */
void endmsg() {
    strncpy(ui.huh, UI::msgbuf, 80);
    ui.huh[79] = 0;

    if(ui.mpos) {
        wmove(ui.cw.w, 0, ui.mpos);
        waddstr(ui.cw.w, "--More--");
        draw(ui.cw.w);
        wait_for(ui.cw.w, ' ');
    }
    mvwaddstr(ui.cw.w, 0, 0, UI::msgbuf);
    wclrtoeol(ui.cw.w);
    ui.mpos = UI::newpos;
    UI::newpos = 0;
    draw(ui.cw.w);
}

void doadd(const char *fmt, va_list ap) {
#pragma clang diagnostic ignored "-Wformat-nonliteral"
    vsprintf(&UI::msgbuf[UI::newpos], fmt, ap);
    UI::newpos = (int)strlen(UI::msgbuf);
}

/*
 * step_ok:
 *     returns true if it is ok to step on ch
 */

int step_ok(const int ch) {
    switch(ch) {
        case ' ':
        case '|':
        case '-':
        case SECRETDOOR:
            return FALSE;
        default:
            return (!isalpha(ch));
    }
}

/*
 * readchar:
 *     flushes stdout so that screen is up to date and then returns
 *     getchar.
 */

int readchar(WINDOW *win) {
    int ch;

    ch = md_readchar(win);

    if((ch == 3) || (ch == 0)) {
        quit(0);
        return (27);
    }

    return (ch);
}

/*
 * status:
 *     Display the important stats line.  Keep the cursor where it was.
 */

void status() {
    int oy, ox, temp;
    char *pb;
    static char buf[80];
    static int hpwidth = 0, s_hungry = -1;
    static int s_lvl = -1, s_pur, s_hp = -1, s_str, s_add, s_ac = 0;
    static long s_exp = 0;

    /*
     * If nothing has changed since the last status, don't
     * bother.
     */
    if(s_hp == rogue.pstats().s_hpt && s_exp == rogue.pstats().s_exp && s_pur == rogue.purse &&
       s_ac == (rogue.cur_armor != nullptr ? rogue.cur_armor->o_ac : rogue.pstats().s_arm) &&
       s_str == rogue.pstats().s_str.st_str && s_add == rogue.pstats().s_str.st_add &&
       s_lvl == game.curr_level && s_hungry == rogue.hungry_state) {
        return;
    }

    getyx(ui.cw.w, oy, ox);
    if(s_hp != rogue.max_hp) {
        temp = s_hp = rogue.max_hp;
        for(hpwidth = 0; temp; hpwidth++) {
            temp /= 10;
        }
    }
    sprintf(buf, "Level: %d  Gold: %-5d  Hp: %*d(%*d)  Str: %-2d", game.curr_level, rogue.purse,
            hpwidth, rogue.pstats().s_hpt, hpwidth, rogue.max_hp, rogue.pstats().s_str.st_str);
    if(rogue.pstats().s_str.st_add != 0) {
        pb = &buf[strlen(buf)];
        sprintf(pb, "/%d", rogue.pstats().s_str.st_add);
    }
    pb = &buf[strlen(buf)];
    sprintf(pb, "  Ac: %-2d  Exp: %d/%d",
            rogue.cur_armor != nullptr ? rogue.cur_armor->o_ac : rogue.pstats().s_arm,
            rogue.pstats().s_lvl, rogue.pstats().s_exp);
    /*
     * Save old status
     */
    s_lvl = game.curr_level;
    s_pur = rogue.purse;
    s_hp = rogue.pstats().s_hpt;
    s_str = rogue.pstats().s_str.st_str;
    s_add = rogue.pstats().s_str.st_add;
    s_exp = rogue.pstats().s_exp;
    s_ac = (rogue.cur_armor != nullptr ? rogue.cur_armor->o_ac : rogue.pstats().s_arm);
    mvwaddstr(ui.cw.w, LINES - 1, 0, buf);
    switch(rogue.hungry_state) {
        case 0:;
            break;
        case 1:
            waddstr(ui.cw.w, "  Hungry");
            break;
        case 2:
            waddstr(ui.cw.w, "  Weak");
            break;
        case 3:
            waddstr(ui.cw.w, "  Fainting");
    }
    wclrtoeol(ui.cw.w);
    s_hungry = rogue.hungry_state;
    wmove(ui.cw.w, oy, ox);
}

/*
 * wait_for
 *     Sit around until the guy types the right key
 */

void wait_for(WINDOW *win, const int ch) {
    if(ch == '\n') {
        int c;
        while((c = readchar(win)) != '\n' && c != '\r') {
            // discard \n's.
        }
    } else {
        while(readchar(win) != ch) {
            // discard non-'ch' characters.
        }
    }
}

/*
 * show_win:
 *     function used to display a window and wait before returning
 */

void show_win(WINDOW *scr, const char *message) {
    mvwaddstr(scr, 0, 0, message);
    touchwin(scr);
    wmove(scr, rogue.hero().y, rogue.hero().x);
    draw(scr);
    wait_for(scr, ' ');
    clearok(ui.cw.w, TRUE);
    touchwin(ui.cw.w);
}

void CursesWindow::showWindow(const char *message) {
    mvwaddstr(w, 0, 0, message);
    this->touchWindow();
    wmove(w, rogue.hero().y, rogue.hero().x);
    this->drawWindow();
    wait_for(w, ' ');
    clearok(ui.cw.w, TRUE);
    ui.cw.touchWindow();
}

void flush_type() {
    flushinp();
}
