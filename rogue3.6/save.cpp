/*
 * save and restore routines
 *
 * @(#)save.c     3.9 (Berkeley) 6/16/81
 *
 * Rogue: Exploring the Dungeons of Doom
 * Copyright (C) 1980, 1981 Michael Toy, Ken Arnold and Glenn Wichman
 * All rights reserved.
 *
 * See the file LICENSE.TXT for full copyright and licensing information.
 */

#include "rogue.h"
#include <fstream>

int save_game() {
    FILE *savef;

    /* get file name */
    ui.mpos = 0;
    if(game.save_file_name[0] != '\0') {
        msg("Save file (%s)? ", game.save_file_name);
        int c;
        do {
            c = readchar(ui.cw.w);
        } while(c != 'n' && c != 'N' && c != 'y' && c != 'Y');
        ui.mpos = 0;
        if(c == 'y' || c == 'Y') {
            msg("File name: %s", game.save_file_name);
            goto gotfile;
        }
    }

    do {
        msg("File name: ");
        ui.mpos = 0;
        char buf[80];
        buf[0] = '\0';
        if(get_str(buf, ui.cw.w) == UI::QUIT) {
            msg("");
            return FALSE;
        }
        strcpy(game.save_file_name, buf);
    gotfile:
        if((savef = fopen(game.save_file_name, "wb")) == nullptr) {
            msg(strerror(errno)); /* fake perror() */
        }
    } while(savef == nullptr);

    /*
     * write out encrpyted file (after a stat)
     * The fwrite is to force allocation of the buffer before the write
     */
    if(save_file(savef) != 0) {
        msg("Save game failed!");
        return FALSE;
    }
    return TRUE;
}

/* automatically save a file.  This is used if a HUP signal is recieved */
[[noreturn]] void auto_save(int p) {
    p = 0; // silence warning
    FILE *savef;

    for(int i = 0; i < NSIG; i++) {
        signal(i, SIG_IGN);
    }
    if(game.save_file_name[0] != '\0' && (savef = fopen(game.save_file_name, "wb")) != nullptr) {
        save_file(savef);
    }
    endwin();
    exit(1);
}

/* write the saved game on the file */
int save_file(FILE *savef) {
    char buf[80];

    wmove(ui.cw.w, LINES - 1, 0);
    draw(ui.cw.w);
    (void)fseek(savef, 0L, 0);

    memset(buf, 0, 80);
    strcpy(buf, Game::version);
    encwrite(buf, 80, savef);
    memset(buf, 0, 80);
    strcpy(buf, "R36 2\n");
    encwrite(buf, 80, savef);
    memset(buf, 0, 80);
    sprintf(buf, "%d x %d\n", LINES, COLS);
    encwrite(buf, 80, savef);

    int ret = rs_save_file(savef);

    fclose(savef);

    std::ofstream os(string(game.save_file_name) + ".json", std::ios::binary);
    cereal::JSONOutputArchive archive(os);

    archive(CEREAL_NVP(floorMap));
    archive(CEREAL_NVP(game));
    archive(CEREAL_NVP(rogue));
    archive(CEREAL_NVP(ui));
    archive(CEREAL_NVP(items));

    return (ret);
}

int restore(char *file) {
    char buf[80];

    if(strcmp(file, "-r") == 0) {
        file = game.save_file_name;
    }

    FILE *inf;
    if((inf = fopen(file, "rb")) == nullptr) {
        perror(file);
        return FALSE;
    }

    fflush(stdout);
    encread(buf, 80, inf);

    if(strcmp(buf, Game::version) != 0) {
        printf("Sorry, saved game is out of date.\n");
        return FALSE;
    }

    encread(buf, 80, inf);
    int rogue_version = 0, savefile_version = 0;
    (void)sscanf(buf, "R%d %d\n", &rogue_version, &savefile_version);

    if((rogue_version != 36) && (savefile_version != 2)) {
        printf("Sorry, saved game format is out of date.\n");
        return FALSE;
    }

    encread(buf, 80, inf);
    int slines, scols;
    (void)sscanf(buf, "%d x %d\n", &slines, &scols);

    /*
     * we do not close the file so that we will have a hold of the
     * inode for as long as possible
     */

    ui.initScr();

    if(slines > LINES) {
        endwin();
        printf("Sorry, original game was played on a screen with %d lines.\n", slines);
        printf("Current screen only has %d lines. Unable to restore game\n", LINES);
        return (FALSE);
    }

    if(scols > COLS) {
        endwin();
        printf("Sorry, original game was played on a screen with %d columns.\n", scols);
        printf("Current screen only has %d columns. Unable to restore game\n", COLS);
        return (FALSE);
    }

    ui.cw.newWindow(LINES, COLS);
    ui.mw.newWindow(LINES, COLS);
    ui.hw.newWindow(LINES, COLS);
    nocrmode();
    keypad(ui.cw.w, 1);
    ui.mpos = 0;
    mvwprintw(ui.cw.w, 0, 0, "%s", file);

    std::ifstream is(string(game.save_file_name) + ".json", std::ios::binary);
    cereal::JSONInputArchive archive(is);

    archive(CEREAL_NVP(floorMap));
    archive(CEREAL_NVP(game));
    archive(CEREAL_NVP(rogue));
    archive(CEREAL_NVP(ui));
    archive(CEREAL_NVP(items));

    if(rs_restore_file(inf) != 0) {
        endwin();
        printf("Cannot restore file\n");
        return (FALSE);
    }

    if(!game.wizard && (md_unlink(file) < 0)) {
        endwin();
        printf("Cannot unlink file\n");
        return FALSE;
    }

    strcpy(game.save_file_name, file);
    setup();
    clearok(ui.curScr.w, TRUE);
    touchwin(ui.cw.w);
    md_srand((int)time(nullptr));
    status();
    playit();
    /*NOTREACHED*/
}

static int encerrno = 0;

int encerror() {
    return encerrno;
}

void encseterr(const int err) {
    encerrno = err;
}

int encclearerr() {
    int n = encerrno;

    encerrno = 0;

    return (n);
}

/* perform an encrypted write */
size_t encwrite(const void *buf, size_t size, FILE *outf) {
    const char *start = (const char *)buf;

    size_t write_size;
    if((write_size = fwrite(start, 1, size, outf)) == 0) {
        return 0;
    }

    return write_size;
}

/* perform an encrypted read */
size_t encread(void *buf, size_t size, FILE *inf) {
    char *start = (char *)buf;

    size_t read_size;
    if((read_size = fread(start, 1, size, inf)) == 0) {
        return 0;
    }

    return read_size;
}
