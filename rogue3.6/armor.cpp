/*
 * This file contains misc functions for dealing with armor
 * @(#)armor.c     3.9 (Berkeley) 6/15/81
 *
 * Rogue: Exploring the Dungeons of Doom
 * Copyright (C) 1980, 1981 Michael Toy, Ken Arnold and Glenn Wichman
 * All rights reserved.
 *
 * See the file LICENSE.TXT for full copyright and licensing information.
 */

#include "rogue.h"

/*
 * wear:
 *     The player wants to wear something, so let him/her put it on.
 */

void wear() {
    if(rogue.cur_armor != nullptr) {
        addmsg("You are already wearing some");
        if(!game.terse) {
            addmsg(".  You'll have to take it off first");
        }
        endmsg();
        game.after = FALSE;
        return;
    }

    linked_list *item;
    if((item = get_item("wear", ARMOR)) == nullptr) {
        return;
    }

    auto *obj = lobject(item);
    if(obj->o_type != ARMOR) {
        msg("You can't wear that.");
        return;
    }
    daemons.waste_time();
    if(!game.terse) {
        addmsg("You are now w");
    } else {
        addmsg("W");
    }
    msg("earing %s.", Items::a_names[(size_t)obj->o_which].c_str());
    rogue.cur_armor = obj;
    obj->o_flags |= ISKNOW;
}

/*
 * take_off:
 *     Get the armor off of the players back
 */

void take_off() {
    object *obj;
    if((obj = rogue.cur_armor) == nullptr) {
        if(game.terse) {
            msg("Not wearing armor");
        } else {
            msg("You aren't wearing any armor");
        }
        return;
    }
    if(!dropcheck(rogue.cur_armor)) {
        return;
    }
    rogue.cur_armor = nullptr;
    if(game.terse) {
        addmsg("Was");
    } else {
        addmsg("You used to be ");
    }
    msg(" wearing %c) %s", pack_char(obj), inv_name(obj, TRUE));
}
