/*
 * Hero movement commands
 *
 * @(#)move.c     3.26 (Berkeley) 6/15/81
 *
 * Rogue: Exploring the Dungeons of Doom
 * Copyright (C) 1980, 1981 Michael Toy, Ken Arnold and Glenn Wichman
 * All rights reserved.
 *
 * See the file LICENSE.TXT for full copyright and licensing information.
 */

#include "rogue.h"

/*
 * do_run:
 *     Start the hero running
 */

void do_run(const int ch) {
    game.running = TRUE;
    game.after = FALSE;
    game.runch = ch;
}

void move_stuff(int ch) {
    if(ch == PASSAGE && ui.winat(rogue.hero().y, rogue.hero().x) == DOOR) {
        light(rogue.hero());
    } else if(ch == DOOR) {
        game.running = FALSE;
        if(ui.winat(rogue.hero().y, rogue.hero().x) == PASSAGE) {
            light(rogue.nh);
        }
    } else if(ch == STAIRS) {
        game.running = FALSE;
    } else if(isupper(ch)) {
        game.running = FALSE;
        fight(rogue.nh, ch, rogue.cur_weapon, FALSE);
        return;
    }
    ch = (int)ui.winat(rogue.hero().y, rogue.hero().x);
    wmove(ui.cw.w, unc(rogue.hero()));
    waddch_int(ui.cw.w, ch);
    rogue.hero() = rogue.nh;
    wmove(ui.cw.w, unc(rogue.hero()));
    waddch(ui.cw.w, PLAYER);
}
/*
 * do_move:
 *     Check to see that a move is legal.  If it is handle the
 * consequences (fighting, picking up, etc.)
 */

void do_move(const int dy, const int dx) {
    game.firstmove = FALSE;
    if(rogue.no_move) {
        rogue.no_move--;
        msg("You are still stuck in the bear trap");
        return;
    }
    /*
     * Do a confused move (maybe)
     */
    if(rnd(100) < 80 && on(rogue.player, ISHUH)) {
        rogue.nh = rndmove(&rogue.player);
    } else {
        rogue.nh.y = rogue.hero().y + dy;
        rogue.nh.x = rogue.hero().x + dx;
    }

    /*
     * Check if he tried to move off the screen or make an illegal
     * diagonal move, and stop him if he did.
     */
    if(rogue.nh.x < 0 || rogue.nh.x > COLS - 1 || rogue.nh.y < 0 || rogue.nh.y > LINES - 1 ||
       !diag_ok(rogue.hero(), rogue.nh)) {
        game.after = FALSE;
        game.running = FALSE;
        return;
    }
    if(game.running && (rogue.hero() == rogue.nh)) {
        game.after = game.running = FALSE;
    }
    int ch = (int)ui.winat(rogue.nh.y, rogue.nh.x);
    if(on(rogue.player, ISHELD) && ch != 'F') {
        msg("You are being held");
        return;
    }
    switch(ch) {
        case ' ':
        case '|':
        case '-':
        case SECRETDOOR:
            game.after = game.running = FALSE;
            return;
        case TRAP:
            ch = be_trapped(rogue.nh);
            if(ch == TRAPDOOR || ch == TELTRAP) {
                return;
            }
            move_stuff(ch);
            break;
        case GOLD:
            [[clang::fallthrough]];
        case POTION:
        case SCROLL:
        case FOOD:
        case WEAPON:
        case ARMOR:
        case RING:
        case AMULET:
        case STICK:
            game.running = FALSE;
            game.take = ch;
            [[clang::fallthrough]];
        default:
            move_stuff(ch);
    }
}

/*
 * Called to illuminate a room.
 * If it is dark, remove anything that might move.
 */

void light(const coord &cp) {
    room *rp;

    if((rp = roomin(cp)) != nullptr && !on(rogue.player, ISBLIND)) {
        for(int j = 0; j < rp->r_max.y; j++) {
            for(int k = 0; k < rp->r_max.x; k++) {
                int ch = show(rp->r_pos.y + j, rp->r_pos.x + k);
                wmove(ui.cw.w, rp->r_pos.y + j, rp->r_pos.x + k);
                /*
                 * Figure out how to display a secret door
                 */
                if(ch == SECRETDOOR) {
                    if(j == 0 || j == rp->r_max.y - 1) {
                        ch = '-';
                    } else {
                        ch = '|';
                    }
                }
                /*
                 * If the room is a dark room, we might want to remove
                 * monsters and the like from it (since they might
                 * move)
                 */
                if(isupper(ch)) {
                    linked_list *item = wake_monster(rp->r_pos.y + j, rp->r_pos.x + k);
                    if((lthing(item))->t_oldch == ' ') {
                        if(!(rp->r_flags & ISDARK)) {
                            (lthing(item))->t_oldch =
                                (int)mvwinch(ui.stdScr.w, rp->r_pos.y + j, rp->r_pos.x + k);
                        }
                    }
                }
                if(rp->r_flags & ISDARK) {
                    int rch = (int)mvwinch(ui.cw.w, rp->r_pos.y + j, rp->r_pos.x + k);
                    switch(rch) {
                        case DOOR:
                        case STAIRS:
                        case TRAP:
                        case '|':
                        case '-':
                        case ' ':
                            ch = rch;
                            break;
                        case FLOOR:
                            ch = (on(rogue.player, ISBLIND) ? FLOOR : ' ');
                            break;
                        default:
                            ch = ' ';
                    }
                }
                mvwaddch_int(ui.cw.w, rp->r_pos.y + j, rp->r_pos.x + k, ch);
            }
        }
    }
}

/*
 * show:
 *     returns what a certain thing will display as to the un-initiated
 */

int show(const int y, const int x) {
    int ch = (int)ui.winat(y, x);

    if(ch == TRAP) {
        return (trap_at(y, x)->tr_flags & ISFOUND) ? TRAP : FLOOR;
    }
    if(ch == 'M' || ch == 'I') {
        linked_list *it;
        if((it = find_mons(y, x)) == nullptr) {
            fatal("Can't find monster in show");
        }
        auto *tp = lthing(it);
        if(ch == 'M') {
            ch = tp->t_disguise;
        }
        /*
         * Hide invisible monsters
         */
        else if(off(rogue.player, CANSEE)) {
            ch = (int)mvwinch(ui.stdScr.w, y, x);
        }
    }
    return ch;
}

/*
 * be_trapped:
 *     The guy stepped on a trap.... Make him pay.
 */

int be_trapped(const coord &tc) {
    int ch;

    trap *tp = trap_at(tc.y, tc.x);
    ui.count = game.running = FALSE;
    mvwaddch(ui.cw.w, tp->tr_pos.y, tp->tr_pos.x, TRAP);
    tp->tr_flags |= ISFOUND;
    switch(ch = tp->tr_type) {
        case TRAPDOOR:
            game.curr_level++;
            new_level();
            msg("You fell into a trap!");
            break;
        case BEARTRAP:
            rogue.no_move += Game::BEARTIME;
            msg("You are caught in a bear trap");
            break;
        case SLEEPTRAP:
            rogue.no_command += Game::SLEEPTIME;
            msg("A strange white mist envelops you and you fall asleep");
            break;
        case ARROWTRAP:
            if(swing(rogue.pstats().s_lvl - 1, rogue.pstats().s_arm, 1)) {
                msg("Oh no! An arrow shot you");
                if((rogue.pstats().s_hpt -= roll(1, 6)) <= 0) {
                    msg("The arrow killed you.");
                    death('a');
                }
            } else {
                linked_list *item;
                object *arrow;

                msg("An arrow shoots past you.");
                item = new_item(sizeof *arrow);
                arrow = lobject(item);
                arrow->o_type = WEAPON;
                arrow->o_which = ARROW;
                init_weapon(arrow, ARROW);
                arrow->o_count = 1;
                arrow->o_pos = rogue.hero();
                arrow->o_hplus = arrow->o_dplus = 0; /* "arrow bug" FIX */
                fall(item, FALSE);
            }
            break;
        case TELTRAP:
            teleport();
            break;
        case DARTTRAP:
            if(swing(rogue.pstats().s_lvl + 1, rogue.pstats().s_arm, 1)) {
                msg("A small dart just hit you in the shoulder");
                if((rogue.pstats().s_hpt -= roll(1, 4)) <= 0) {
                    msg("The dart killed you.");
                    death('d');
                }
                if(!rogue.isWearing(R_SUSTSTR)) {
                    chg_str(-1);
                }
            } else {
                msg("A small dart whizzes by your ear and vanishes.");
            }
    }
    flush_type(); /* flush typeahead */
    return (ch);
}

/*
 * trap_at:
 *     find the trap at (y,x) on screen.
 */

trap *trap_at(const int y, const int x) {
    trap *tp;
    trap *ep = &floorMap.traps[floorMap.ntraps];
    for(tp = floorMap.traps; tp < ep; tp++) {
        if(tp->tr_pos.y == y && tp->tr_pos.x == x) {
            break;
        }
    }
    if(tp == ep) {
        sprintf(ui.prbuf, "Trap at %d,%d not in array", y, x);
        fatal(ui.prbuf);
    }
    return tp;
}

/*
 * rndmove:
 *     move in a random direction if the monster/person is confused
 */

coord rndmove(thing *who) {
    // FIXME: These shouldn't be static; should just be returning a 'coord' value
    static coord ret; /* what we will be returning */
    static coord dest;

    ret = who->t_pos;
    /*
     * Now go through the spaces surrounding the player and
     * set that place in the array to true if the space can be
     * moved into
     */
    int ey = ret.y + 1;
    int ex = ret.x + 1;
    int nopen = 0;
    for(int y = who->t_pos.y - 1; y <= ey; y++) {
        if(y >= 0 && y < LINES) {
            for(int x = who->t_pos.x - 1; x <= ex; x++) {
                if(x < 0 || x >= COLS) {
                    continue;
                }
                int ch = (int)ui.winat(y, x);
                if(step_ok(ch)) {
                    dest.y = y;
                    dest.x = x;
                    if(!diag_ok(who->t_pos, dest)) {
                        continue;
                    }
                    if(ch == SCROLL) {
                        linked_list *item = nullptr;
                        object *obj = nullptr;
                        for(item = floorMap.lvl_obj; item != nullptr; item = next_p(item)) {
                            obj = lobject(item);
                            if(y == obj->o_pos.y && x == obj->o_pos.x) {
                                break;
                            }
                        }
                        assert(obj != nullptr);
                        if(item != nullptr && obj->o_which == S_SCARE) {
                            continue;
                        }
                    }
                    if(rnd(++nopen) == 0) {
                        ret = dest;
                    }
                }
            }
        }
    }
    return ret;
}
