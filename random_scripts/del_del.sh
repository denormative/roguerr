#!/bin/sh

# example execution:
# ./random_scripts/del_del.sh rogue3.6
if [ -d "$1" ]; then
	sed -e '/\/\/DEL/d' -i .del "$1"/*.c "$1"/*.h
fi


