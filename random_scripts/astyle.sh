#!/bin/bash

# this needs astyle of course, eg:
# brew install astyle

styleFiles() {
astyle --style=java "$1/*.c" "$1/*.h" --indent=tab --indent-switches --indent-cases --indent-preprocessor --indent-col1-comments --unpad-paren --align-pointer=name --lineend=linux  --pad-oper --add-brackets
}

if [ -d "$1" ]; then
	styleFiles $1
fi

#styleFiles "arogue5.8"

#styleFiles "rogue3.6"
#styleFiles "rogue5.2"
#styleFiles "rogue5.4"
#styleFiles "arogue5.8s"
#styleFiles "arogue7.7"
#styleFiles "xrogue"
#styleFiles "librogue"
#styleFiles "urogue1.02"
#styleFiles "urogue/trunk"
#styleFiles "srogue/trunk"

