#!/bin/bash
find . -iname "*orig" -exec rm {} \;
find . -iname "*.bak" -exec rm {} \;
find . -iname "*.test" -exec rm {} \;
find . -iname "*.del" -exec rm {} \;

