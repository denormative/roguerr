#!/bin/sh

# example execution:
# ./random_scripts/cleanup.sh rogue3.6
if [ -d "$1" ]; then
	sed -E -e 's/\s*(reg|register) //' -i .orig "$1"/*.c "$1"/*.h
fi

#perl -p0777 -i.orig -e 's/^([\w][^\n]*)\n^([\w][^\n]*\))\n^\{/\1 \2 {/gm' $1/*.c $1/*.h

