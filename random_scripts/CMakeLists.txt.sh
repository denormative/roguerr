#!/bin/bash
#ls -d */ | sed s/[/]//g |xargs -I % ./random_scripts/CMakeLists.txt.sh %

# I think this one works better:
# ls -d *rogue* |xargs -I % ./random_scripts/CMakeLists.txt.sh %

echo $1

lowername=`echo $1 | sed s/[.]//g`
uppername=`echo $lowername | tr '[:lower:]' '[:upper:]'`

echo $lowername
echo $uppername

echo "set(CMAKE_C_FLAGS \${ROGUE_FLAGS})
set(CMAKE_CXX_FLAGS \${ROGUE_FLAGS})

include_directories(".")
file(GLOB ${uppername}SRC "*.c")
file(GLOB ${uppername}INC "*.h")

set_source_files_properties(
    \${${uppername}SRC} \${${uppername}SRC} \${VARIANTSRC_A}
    PROPERTIES LANGUAGE \${ROGUE_LANG} )

add_executable (${lowername}
    \${${uppername}SRC}  \${${uppername}INC}
    \${PDCURSESRC}   \${PDCURSEINC}
    \${METAROGSRC}  \${METAROGINC}
    \${VARIANTSRC_A} \${VARIANTINC_A}
    )

if(CURSES_FOUND)
    target_link_libraries (${lowername} \${CURSES_LIBRARIES})
endif()" > $1/CMakeLists.txt

