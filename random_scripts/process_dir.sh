#!/bin/bash

if [ -d "$1" ]; then
	./random_scripts/astyle.sh $1
	git add $1/*.c $1/*.h
	git commit -m "auto: astyle $1"
	./random_scripts/cleanup.sh $1
	git add $1/*.c $1/*.h
	git commit -m "auto: cleanup $1"
	./random_scripts/fix_cfunc.pl $1
	git add $1/*.c $1/*.h
	git commit -m "auto: fix_cfunc $1"
	./random_scripts/del_del.sh $1
	git add $1/*.c $1/*.h
	git commit -m "auto: del DEL $1"
	./random_scripts/astyle.sh $1
	git add $1/*.c $1/*.h
	git commit -m "auto: astyle $1"
fi
