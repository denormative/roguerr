#!/bin/bash

for d in $(ls -d *rogue* | grep -v 'rogue1.03');
do
	echo "Formatting $d..."
	clang-format -i $d/*.{cpp,h}
done

echo "Formatting metarog & tests..."
clang-format -i metarog/*.{c,cpp,h} tests/*.cpp

