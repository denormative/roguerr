/*
 * Advanced Rogue
 * Copyright (C) 1984, 1985 Michael Morgan, Ken Dalka and AT&T
 * All rights reserved.
 *
 * Based on "Super-Rogue"
 * Copyright (C) 1984 Robert D. Kindelberger
 * All rights reserved.
 *
 * See the file LICENSE.TXT for full copyright and licensing information.
 */

/*
 * Anything to do with trading posts
 */

#include "rogue.h"

/*
 * buy_it:
 *    Buy the item on which the hero stands
 */
void buy_it() {
    linked_list *item;

    if(purse <= 0) {
        msg("You have no money.");
        return;
    }

    if((item = get_item(pack, "buy", ALL)) == nullptr) {
        return;
    }
    object *obj = OBJPTR(item);
    if(!(obj->o_flags & ISUNPAID)) {
        msg("You already own that.");
        return;
    }

    long wo = get_worth(obj);
    if(wo > purse) {
        msg("You can't afford that!");
        msg("It costs %d pieces of gold.", wo);
        return;
    }
    purse -= wo;
    not_paid--;
    obj->o_flags &= ~ISUNPAID;
}

/*
 * get_worth:
 *    Calculate an objects worth in gold
 */
long get_worth(object *obj) {
    long worth = 0;
    int wh = obj->o_which;
    switch(obj->o_type) {
        case FOOD:
            worth = 125;
            break;
        case WEAPON:
            if(wh < MAXWEAPONS) {
                worth = weaps[wh].w_worth;
                worth *= 2 * (10 + (obj->o_hplus + obj->o_dplus) / 2);
            }
            break;
        case ARMOR:
            if(wh < MAXARMORS) {
                worth = armors[wh].a_worth;
                worth *= (4 + ((armors[wh].a_class - obj->o_ac)) / 2);
            }
            break;
        case SCROLL:
            if(wh < MAXSCROLLS) {
                worth = 2 * s_magic[wh].mi_worth;
            }
            break;
        case POTION:
            if(wh < MAXPOTIONS) {
                worth = 2 * p_magic[wh].mi_worth;
            }
            break;
        case RING:
            if(wh < MAXRINGS) {
                worth = 2 * r_magic[wh].mi_worth;
                worth += obj->o_ac * 80;
            }
            break;
        case STICK:
            if(wh < MAXSTICKS) {
                worth = 2 * ws_magic[wh].mi_worth;
                worth += 40 * obj->o_charges;
            }
            break;
        case MM:
            if(wh < MAXMM) {
                worth = m_magic[wh].mi_worth;
                switch(wh) {
                    case MM_BRACERS:
                        worth += 40 * obj->o_ac;
                        break;
                    case MM_PROTECT:
                        worth += 60 * obj->o_ac;
                        break;
                    case MM_DISP: /* ac already figured in price*/
                        break;
                    default:
                        worth += 20 * obj->o_ac;
                }
            }
            break;
        case RELIC:
            if(wh < MAXRELIC) {
                worth = rel_magic[wh].mi_worth;
                if(wh == quest_item) {
                    worth *= 10;
                }
            }
            break;
        default:
            worth = 0;
    }
    if(obj->o_flags & ISPROT) { /* 300% more for protected */
        worth *= 3;
    }
    if(obj->o_flags & ISBLESSED) { /* 50% more for blessed */
        worth = worth * 3 / 2;
    }
    if(obj->o_flags & ISCURSED) { /* half for cursed */
        worth /= 2;
    }
    if(worth < 25) {
        worth = 25;
    }
    return worth;
}

/*
 * price_it:
 *    Price the object that the hero stands on
 */
void price_it() {
    linked_list *item;
    if((item = get_item(pack, "price", ALL)) == nullptr) {
        return;
    }
    object *obj = OBJPTR(item);
    if(!(obj->o_flags & ISUNPAID)) {
        msg("You already own that.");
        return;
    }
    msg("Your %s costs %d pieces of gold.", typ_name(obj), get_worth(obj));
}

/*
 * typ_name:
 *     Return the name for this type of object
 */
char *typ_name(object *obj) {
    static char buff[20];
    int wh;

    switch(obj->o_type) {
        case POTION:
            wh = TYP_POTION;
            break;
        case SCROLL:
            wh = TYP_SCROLL;
            break;
        case STICK:
            wh = TYP_STICK;
            break;
        case RING:
            wh = TYP_RING;
            break;
        case ARMOR:
            wh = TYP_ARMOR;
            break;
        case WEAPON:
            wh = TYP_WEAPON;
            break;
        case MM:
            wh = TYP_MM;
            break;
        case FOOD:
            wh = TYP_FOOD;
            break;
        case RELIC:
            wh = TYP_RELIC;
            break;
        default:
            wh = -1;
    }
    if(wh < 0) {
        strcpy(buff, "unknown");
    } else {
        strcpy(buff, things[wh].mi_name);
    }
    return (buff);
}
