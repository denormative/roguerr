/*
 * Advanced Rogue
 * Copyright (C) 1984, 1985 Michael Morgan, Ken Dalka and AT&T
 * All rights reserved.
 *
 * Based on "Rogue: Exploring the Dungeons of Doom"
 * Copyright (C) 1980, 1981 Michael Toy, Ken Arnold and Glenn Wichman
 * All rights reserved.
 *
 * See the file LICENSE.TXT for full copyright and licensing information.
 */

/*
 * Special wizard commands (some of which are also non-wizard commands
 * under strange circumstances)
 */

#include "rogue.h"

/*
 * create_obj:
 *    Create any object for wizard, scroll, magician, or cleric
 */
void create_obj(bool prompt, int which_item, int which_type) {
    linked_list *item;
    object *obj;
    int wh, whc, ch;
    char msz;
    const char *pt;
    WINDOW *thiswin;
    int newitem, newtype = 0;

    thiswin = cw;
    if(prompt) {
        bool nogood = TRUE;

        thiswin = hw;
        wclear(hw);
        wprintw(hw, "Item\t\t\tKey\n\n");
        wprintw(hw, "%s\t\t\t%c\n%s\t\t\t%c\n", things[TYP_RING].mi_name, RING,
                things[TYP_STICK].mi_name, STICK);
        wprintw(hw, "%s\t\t\t%c\n%s\t\t\t%c\n", things[TYP_POTION].mi_name, POTION,
                things[TYP_SCROLL].mi_name, SCROLL);
        wprintw(hw, "%s\t\t\t%c\n%s\t\t\t%c\n", things[TYP_ARMOR].mi_name, ARMOR,
                things[TYP_WEAPON].mi_name, WEAPON);
        wprintw(hw, "%s\t%c\n", things[TYP_MM].mi_name, MM);
        wprintw(hw, "%s\t\t\t%c\n", things[TYP_FOOD].mi_name, FOOD);
        if(wizard) {
            wprintw(hw, "%s\t\t%c\n", things[TYP_RELIC].mi_name, RELIC);
            waddstr(hw, "monster\t\t\tm");
        }
        wprintw(hw, "\n\nWhat do you want to create? ");
        draw(hw);
        do {
            ch = wgetch(hw);
            if(ch == ESCAPE) {
                restscr(cw);
                return;
            }
            switch(ch) {
                case RING:
                case STICK:
                case POTION:
                case SCROLL:
                case ARMOR:
                case WEAPON:
                case FOOD:
                case MM:
                    nogood = FALSE;
                    break;
                case RELIC:
                case 'm':
                    if(wizard) {
                        nogood = FALSE;
                    }
                    break;
                default:
                    nogood = TRUE;
            }
        } while(nogood);
        newitem = ch;
    } else {
        newitem = which_item;
    }

    pt = "those";
    msz = 0;
    if(newitem == 'm') {
        makemonster(TRUE); /* make monster and be done with it */
        return;
    }
    if(newitem == GOLD) {
        pt = "gold";
    }
    /* else if(isatrap(newitem))
    pt = "traps";
    */
    switch(newitem) {
        case POTION:
            whc = TYP_POTION;
            msz = MAXPOTIONS;
            break;
        case SCROLL:
            whc = TYP_SCROLL;
            msz = MAXSCROLLS;
            break;
        case WEAPON:
            whc = TYP_WEAPON;
            msz = MAXWEAPONS;
            break;
        case ARMOR:
            whc = TYP_ARMOR;
            msz = MAXARMORS;
            break;
        case RING:
            whc = TYP_RING;
            msz = MAXRINGS;
            break;
        case STICK:
            whc = TYP_STICK;
            msz = MAXSTICKS;
            break;
        case MM:
            whc = TYP_MM;
            msz = MAXMM;
            break;
        case RELIC:
            whc = TYP_RELIC;
            msz = MAXRELIC;
            break;
        case FOOD:
            whc = TYP_FOOD;
            msz = MAXFOODS;
            if(thiswin == hw) {
                restscr(cw);
            }
            mpos = 0;
            break;
        default:
            if(thiswin == hw) {
                restscr(cw);
            }
            mpos = 0;
            msg("Even wizards can't create %s !!", pt);
            return;
    }
    if(msz == 1) { /* if only one type of item */
        ch = 'a';
    } else if(prompt) {
        int ii;
        long old_prob;

        mpos = 0;
        magic_item *wmi = nullptr;
        char wmn = 0;
        switch(newitem) {
            case POTION:
                wmi = &p_magic[0];
                break;
            case SCROLL:
                wmi = &s_magic[0];
                break;
            case RING:
                wmi = &r_magic[0];
                break;
            case STICK:
                wmi = &ws_magic[0];
                break;
            case MM:
                wmi = &m_magic[0];
                break;
            case RELIC:
                wmi = &rel_magic[0];
                break;
            case WEAPON:
                wmn = 1;
                break;
            case ARMOR:
                wmn = 2;
        }
        wclear(hw);
        thiswin = hw;
        if(wmi != nullptr) {
            ii = old_prob = 0;
            while(ii < msz) {
                if(wmi->mi_prob == old_prob && wizard == FALSE) {
                    msz--; /* can't make a unique item */
                } else {
                    hwaddch(ii % 13, ii > 12 ? COLS / 2 : 0, ii + 'a');
                    waddstr(hw, ") ");
                    waddstr(hw, wmi->mi_name);
                    ii++;
                }
                old_prob = wmi->mi_prob;
                wmi++;
            }
        } else if(wmn != 0) {
            for(ii = 0; ii < msz; ii++) {
                hwaddch(ii % 13, ii > 12 ? COLS / 2 : 0, ii + 'a');
                waddstr(hw, ") ");
                if(wmn == 1) {
                    waddstr(hw, weaps[ii].w_name);
                } else {
                    waddstr(hw, armors[ii].a_name);
                }
            }
        }
        sprintf(prbuf, "Which %s? ", things[whc].mi_name);
        hwaddstr(LINES - 1, 0, prbuf);
        draw(hw);
        do {
            ch = wgetch(hw);
            if(ch == ESCAPE) {
                restscr(cw);
                msg("");
                return;
            }
        } while(!isalpha(ch));
        if(thiswin == hw) { /* restore screen if need be */
            restscr(cw);
        }
        newtype = tolower(ch) - 'a';
        if(newtype < 0 || newtype >= msz) { /* if an illegal value */
            mpos = 0;
            msg("There is no such %s", things[whc].mi_name);
            return;
        }
    } else {
        newtype = which_type;
    }
    item = new_item(sizeof *obj); /* get some memory */
    obj = OBJPTR(item);
    obj->o_type = newitem; /* store the new items */
    obj->o_mark[0] = '\0';
    obj->o_which = newtype;
    obj->o_group = 0;
    obj->contents = nullptr;
    obj->o_count = 1;
    obj->o_flags = 0;
    obj->o_dplus = obj->o_hplus = 0;
    obj->o_weight = 0;
    wh = obj->o_which;
    mpos = 0;
    if(!wizard) { /* users get 0 to +3 */
        whc = rnd(4);
    } else { /* wizard gets to choose */
        whc = getbless();
    }
    if(whc < 0) {
        obj->o_flags |= ISCURSED;
    }
    switch(obj->o_type) {
        case WEAPON:
        case ARMOR:
            if(obj->o_type == WEAPON) {
                init_weapon(obj, wh);
                obj->o_hplus += whc;
                obj->o_dplus += whc;
            } else { /* armor here */
                obj->o_weight = armors[wh].a_wght;
                obj->o_ac = armors[wh].a_class - whc;
            }
            break;
        case RING:
            if(whc > 1 && r_magic[wh].mi_bless != 0) {
                obj->o_flags |= ISBLESSED;
            }
            r_know[wh] = TRUE;
            switch(wh) {
                case R_ADDSTR:
                case R_ADDWISDOM:
                case R_ADDINTEL:
                case R_PROTECT:
                case R_ADDHIT:
                case R_ADDDAM:
                case R_DIGEST:
                    obj->o_ac = whc + 1;
                    break;
                default:
                    obj->o_ac = 0;
            }
            obj->o_weight = things[TYP_RING].mi_wght;
            break;
        case MM:
            if(whc > 1 && m_magic[wh].mi_bless != 0) {
                obj->o_flags |= ISBLESSED;
            }
            m_know[wh] = TRUE;
            switch(wh) {
                case MM_JUG:
                    switch(rnd(9)) {
                        case 0:
                            obj->o_ac = P_PHASE;
                            break;
                        case 1:
                            obj->o_ac = P_CLEAR;
                            break;
                        case 2:
                            obj->o_ac = P_SEEINVIS;
                            break;
                        case 3:
                            obj->o_ac = P_HEALING;
                            break;
                        case 4:
                            obj->o_ac = P_MFIND;
                            break;
                        case 5:
                            obj->o_ac = P_TFIND;
                            break;
                        case 6:
                            obj->o_ac = P_HASTE;
                            break;
                        case 7:
                            obj->o_ac = P_RESTORE;
                            break;
                        case 8:
                            obj->o_ac = P_FLY;
                    }
                    break;
                case MM_OPEN:
                case MM_HUNGER:
                case MM_DRUMS:
                case MM_DISAPPEAR:
                case MM_CHOKE:
                case MM_KEOGHTOM:
                    if(whc < 0) {
                        whc = -whc; /* these cannot be negative */
                    }
                    obj->o_ac = (whc + 1) * 5;
                    break;
                case MM_BRACERS:
                    obj->o_ac = whc * 2 + 1;
                    break;
                case MM_DISP:
                    obj->o_ac = 2;
                    break;
                case MM_PROTECT:
                    obj->o_ac = whc;
                    break;
                case MM_SKILLS:
                    if(wizard && whc != 0) {
                        obj->o_ac = rnd(4);
                    } else {
                        obj->o_ac = player.t_ctype;
                    }
                    break;
                default:
                    obj->o_ac = 0;
            }
            obj->o_weight = things[TYP_MM].mi_wght;
            break;
        case STICK:
            if(whc > 1 && ws_magic[wh].mi_bless != 0) {
                obj->o_flags |= ISBLESSED;
            }
            ws_know[wh] = TRUE;
            fix_stick(obj);
            break;
        case SCROLL:
            if(whc > 1 && s_magic[wh].mi_bless != 0) {
                obj->o_flags |= ISBLESSED;
            }
            obj->o_weight = things[TYP_SCROLL].mi_wght;
            s_know[wh] = TRUE;
            break;
        case POTION:
            if(whc > 1 && p_magic[wh].mi_bless != 0) {
                obj->o_flags |= ISBLESSED;
            }
            obj->o_weight = things[TYP_POTION].mi_wght;
            p_know[wh] = TRUE;
            break;
        case RELIC:
            obj->o_weight = things[TYP_RELIC].mi_wght;
    }
    mpos = 0;
    obj->o_flags |= ISKNOW;
    if(add_pack(item, FALSE) == FALSE) {
        obj->o_pos = hero;
        fall(item, TRUE);
    }
}

/*
 * getbless:
 *    Get a blessing for a wizards object
 */
int getbless() {
    int bless;

    msg("Blessing? (+,-,n)");
    bless = readchar();
    if(bless == '+') {
        return (rnd(3) + 2);
    }
    if(bless == '-') {
        return (-rnd(3) - 1);
    }
    return (0);
}

/*
 * get a non-monster death type
 */
long getdeath() {
    int which_death;
    char label[80];

    clear();
    for(int i = 0; i < DEATHNUM; i++) {
        sprintf(label, "[%d] %s", i + 1, deaths[i].name);
        mvaddstr(i + 2, 0, label);
    }
    mvaddstr(0, 0, "Which death? ");
    refresh();

    /* Get the death */
    for(;;) {
        get_str(label, stdscr);
        which_death = atoi(label);
        if((which_death < 1 || which_death > DEATHNUM)) {
            mvaddstr(0, 0, "Please enter a number in the displayed range -- ");
            refresh();
        } else {
            break;
        }
    }
    return (deaths[which_death - 1].reason);
}

/*
 * make a monster for the wizard
 */
long makemonster(bool create) {
    long which_monst, pres_monst = 1;
    int num_monst = monsters.size() - 1, num_lines = 2 * (LINES - 3);
    char monst_name[40];

    /* Print out the monsters */
    while(num_monst > 0) {
        int left_limit;

        if(num_monst < num_lines) {
            left_limit = (num_monst + 1) / 2;
        } else {
            left_limit = num_lines / 2;
        }

        wclear(hw);
        touchwin(hw);

        /* Print left column */
        wmove(hw, 2, 0);
        for(int i = 0; i < left_limit; i++) {
            sprintf(monst_name, "[%ld] %s\n", pres_monst, monsters[pres_monst].m_name);
            waddstr(hw, monst_name);
            pres_monst++;
        }

        /* Print right column */
        for(int i = 0; i < left_limit && pres_monst <= monsters.size() - 1; i++) {
            sprintf(monst_name, "[%ld] %s", pres_monst, monsters[pres_monst].m_name);
            wmove(hw, i + 2, COLS / 2);
            waddstr(hw, monst_name);
            pres_monst++;
        }

        if((num_monst -= num_lines) > 0) {
            hwaddstr(LINES - 1, 0, morestr);
            draw(hw);
            wait_for(hw, ' ');
        }

        else {
            hwaddstr(0, 0, "Which monster");
            if(!terse && create) {
                waddstr(hw, " do you wish to create");
            }
            waddstr(hw, "? ");
            draw(hw);
        }
    }

get_monst:
    if(get_str(monst_name, hw) != NORM) {
        restscr(cw);
        touchwin(cw);
        return (0);
    }
    which_monst = atoi(monst_name);
    if((which_monst < 1 || which_monst > monsters.size() - 1)) {
        hwaddstr(0, 0, "Please enter a number in the displayed range -- ");
        draw(hw);
        goto get_monst;
    }
    restscr(cw);
    if(create) {
        creat_mons(&player, which_monst, TRUE);
        light(&hero);
    }
    touchwin(cw);
    return (which_monst);
}

/*
 * passwd:
 *    see if user knows password
 */
int passwd() {
    char *sp;
    int c;
    char buf[LINELEN];

    msg("Wizard's Password:");
    mpos = 0;
    sp = buf;
    while((c = readchar()) != '\n' && c != '\r' && c != '\033') {
        if(c == killchar()) {
            sp = buf;
        } else if(c == erasechar() && sp > buf) {
            sp--;
        } else {
            *sp++ = (char)c;
        }
    }
    if(sp == buf) {
        return FALSE;
    }
    *sp = '\0';
    return (strcmp(PASSWD, md_crypt(buf)) == 0);
}

/*
 * teleport:
 *    Bamf the hero someplace else
 */
int teleport() {
    room *new_rp, *old_rp = roomin(&hero);
    int rm;
    coord c;

    c = hero;
    cwaddch(hero.y, hero.x, ssinch(hero.y, hero.x));
    do {
        rm = rnd_room();
        rnd_pos(&rooms[rm], &hero);
    } while(winat(hero.y, hero.x) != FLOOR);
    player.t_oldpos = c; /* Save last position */

    /* set iron ball's position */
    if(winat(hero.y + 1, hero.x) == FLOOR) {
        heavy_ball.y = hero.y + 1;
        heavy_ball.x = hero.x;
    }
    if(winat(hero.y - 1, hero.x) == FLOOR) {
        heavy_ball.y = hero.y - 1;
        heavy_ball.x = hero.x;
    }
    if(winat(hero.y, hero.x + 1) == FLOOR) {
        heavy_ball.y = hero.y;
        heavy_ball.x = hero.x + 1;
    }
    if(winat(hero.y, hero.x - 1) == FLOOR) {
        heavy_ball.y = hero.y;
        heavy_ball.x = hero.x - 1;
    }

    /* If hero gets moved, darken old room */
    new_rp = &rooms[rm];
    if(old_rp && old_rp != new_rp) {
        rm_turn_on(old_rp, FORCEDARK); /* Force darkness */
        light(&c);
        rm_turn_off(old_rp, FORCEDARK); /* Restore light state */
    }

    if(new_rp != nullptr && levtype != MAZELEV) {
        if((rm_on(new_rp, ISSHOP)) && (!in_shop) && (not_paid <= 0)) {
            if(first_time) {
                msg("Welcome to %s's %s shop.", shkname, shtype);
            } else {
                msg("Welcome again to %s's %s shop.", shkname, shtype);
            }
            first_time = FALSE;
            in_shop = TRUE;
        }
        if((rm_on(new_rp, ISNEST)) && !disturbed) {
            msg("You just invaded a rat's nest!");
            disturbed = TRUE;
        }
        if((rm_on(new_rp, ISORCDEN)) && !disturbed) {
            msg("You just invaded the orc's den!");
            disturbed = TRUE;
        }
        if((rm_on(new_rp, ISHIVE)) && !disturbed) {
            msg("You just disturbed a bee hive!");
            disturbed = TRUE;
        }
        if((rm_on(new_rp, ISHGDEN)) && !disturbed) {
            msg("You just invaded the hobgoblin's den!");
            disturbed = TRUE;
        }
        if((rm_on(new_rp, ISMORGUE)) && !disturbed) {
            msg("You just invaded a morgue!");
            disturbed = TRUE;
        }
        if((rm_on(new_rp, ISTRDEN)) && !disturbed) {
            msg("You just invaded the troll's den!");
            disturbed = TRUE;
        }
        if((rm_on(new_rp, ISVAMDEN)) && !disturbed) {
            msg("You just invaded the vampire's den!");
            disturbed = TRUE;
        }
        if((rm_on(new_rp, ISHILL)) && !disturbed) {
            msg("You just invaded an ant hill!");
            disturbed = TRUE;
        }
        if((rm_on(new_rp, ISTOMB)) && !disturbed) {
            msg("You just opened a mummy's tomb!");
            disturbed = TRUE;
        }
        if((rm_on(new_rp, ISTHROOM)) && !disturbed) {
            msg("You just stumbled into a throne room. Good work.");
            disturbed = TRUE;
        }
        if((rm_off(new_rp, ISSHOP)) && (in_shop)) {
            in_shop = FALSE;
            if(not_paid > 0 && on(*shk, ISSHK)) {
                msg("As you leave the %s shop, %s becomes angry.", shtype, shkname);
                turn_off(*shk, ISSHK);
                turn_on(*shk, ISMEAN);
                runto(shk, &hero);
            }
        }
    }

    /* Darken where we just came from */
    else if(levtype == MAZELEV) {
        light(&c);
    }

    light(&hero);
    cwaddch(hero.y, hero.x, PLAYER);

    /* Reset current room and position */
    oldrp = new_rp; /* Used in look() */
    player.t_oldpos = hero;

    /*
     * turn off ISHELD in case teleportation was done while fighting
     * something that holds you
     */
    if(on(player, ISHELD)) {
        linked_list *ip, *nip;
        thing *mp;

        turn_off(player, ISHELD);
        hold_count = 0;
        for(ip = mlist; ip; ip = nip) {
            mp = THINGPTR(ip);
            nip = next(ip);
            if(on(*mp, DIDHOLD)) {
                turn_off(*mp, DIDHOLD);
                turn_on(*mp, CANHOLD);
            }
            turn_off(*mp, DIDSUFFOCATE); /* Suffocation -- see below */
        }
    }

    /* Make sure player does not suffocate */
    extinguish(d_suffocate);

    count = 0;
    running = FALSE;
    flushinp();
    return rm;
}

/*
 * whatis:
 *    What a certin object is
 */
void whatis(linked_list *what) {
    object *obj;
    linked_list *item;

    if(what == nullptr) { /* do we need to ask which one? */
        if((item = get_item(pack, "identify", IDENTABLE)) == nullptr) {
            return;
        }
    } else {
        item = what;
    }
    obj = OBJPTR(item);
    switch(obj->o_type) {
        case SCROLL:
            s_know[obj->o_which] = TRUE;
            if(s_guess[obj->o_which]) {
                free(s_guess[obj->o_which]);
                s_guess[obj->o_which] = nullptr;
            }
            break;
        case POTION:
            p_know[obj->o_which] = TRUE;
            if(p_guess[obj->o_which]) {
                free(p_guess[obj->o_which]);
                p_guess[obj->o_which] = nullptr;
            }
            break;
        case STICK:
            ws_know[obj->o_which] = TRUE;
            if(ws_guess[obj->o_which]) {
                free(ws_guess[obj->o_which]);
                ws_guess[obj->o_which] = nullptr;
            }
            break;
        case RING:
            r_know[obj->o_which] = TRUE;
            if(r_guess[obj->o_which]) {
                free(r_guess[obj->o_which]);
                r_guess[obj->o_which] = nullptr;
            }
            break;
        case MM:
            /* If it's an identified jug, identify its potion */
            if(obj->o_which == MM_JUG && (obj->o_flags & ISKNOW)) {
                if(obj->o_ac != JUG_EMPTY) {
                    p_know[obj->o_ac] = TRUE;
                }
                break;
            }

            m_know[obj->o_which] = TRUE;
            if(m_guess[obj->o_which]) {
                free(m_guess[obj->o_which]);
                m_guess[obj->o_which] = nullptr;
            }
            break;
        default:
            break;
    }
    obj->o_flags |= ISKNOW;
    if(what == nullptr) {
        msg(inv_name(obj, FALSE));
    }
}
