/*
    list.c  -  Functions for dealing with linked lists of goodies

    UltraRogue
    Copyright (C) 1985 Herb Chong
    All rights reserved.

    Based on "Advanced Rogue"
    Copyright (C) 1982, 1984, 1985 Michael Morgan, Ken Dalka and AT&T
    All rights reserved.

    Based on "Rogue: Exploring the Dungeons of Doom"
    Copyright (C) 1980, 1981 Michael Toy, Ken Arnold and Glenn Wichman
    All rights reserved.

    See the file LICENSE.TXT for full copyright and licensing information.
*/

#include "rogue.h"

/*
 * detach:
 *     Takes an item out of whatever linked list it might be in
 */

void _detach(linked_list **list, linked_list *item) {
    if(*list == item) {
        *list = next(item);
    }
    if(prev(item) != nullptr) {
        item->l_prev->l_next = next(item);
    }
    if(next(item) != nullptr) {
        item->l_next->l_prev = prev(item);
    }
    item->l_next = nullptr;
    item->l_prev = nullptr;
}

/*
 * _attach:
 *     add an item to the head of a list
 */

void _attach(linked_list **list, linked_list *item) {
    if(*list != nullptr) {
        item->l_next = *list;
        (*list)->l_prev = item;
        item->l_prev = nullptr;
    } else {
        item->l_next = nullptr;
        item->l_prev = nullptr;
    }

    *list = item;
}

/*
 * _free_list:
 *     Throw the whole blamed thing away
 */
void _free_list(linked_list **ptr) {
    linked_list *item;

    while(*ptr != nullptr) {
        item = *ptr;
        *ptr = next(item);
        discard(item);
    }
}

/*
 * discard:
 *     free up an item
 */
void discard(linked_list *item) {
    total -= 2;
    FREE(item->l_data);
    FREE(item);
}

/*
 * new_item
 *     get a new item with a specified size
 */

linked_list *new_item(int size) {
    linked_list *item;

    if((item = (linked_list *)newalloc(sizeof *item)) == nullptr) {
        msg("Ran out of memory for header after %d items", total);
    }
    if((item->l_data = newalloc((size_t)size)) == nullptr) {
        msg("Ran out of memory for data after %d items", total);
    }
    item->l_next = item->l_prev = nullptr;
    return item;
}

/*
 * creat_item:
 *      Create just an item structure -- don't make any contents
 */

linked_list *creat_item() {
    linked_list *item;

    if((item = (linked_list *)newalloc(sizeof *item)) == nullptr) {
        msg("Ran out of memory for header after %d items", total);
    }
    item->l_next = item->l_prev = nullptr;
    return item;
}

char *newalloc(size_t size) {
    char *space = (char *)ALLOC(size);
    static char errbuf[LINELEN];

    if(space == nullptr) {
        sprintf(errbuf, "Rogue ran out of memory (wanted = %d).", (int)size);
        fatal(errbuf);
    }
    total++;
    return space;
}
