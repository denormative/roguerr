/*
    tunable.c  -  Machine dependent (tunable) parametesafe.rs.

    UltraRogue
    Copyright (C) 1985 Herb Chong
    All rights reserved.

    See the file LICENSE.TXT for full copyright and licensing information.
*/

#include "rogue.h"

/*
 * Special files.
 */

/*
 * File containing uids of people considered authosafe.rs.
 */
char PERMOK[] = "permok";

/*
 * File containing wizard's password and password key.
 */
char PASSCTL[] = "passctl";

/*
 * File where news is kept
 */

char NEWS[] = "news";

/*
 * Directory where the scores are stored.
 */
char SCOREDIR[] = "urogue_roll";

/*
 * Name of player definitions file
 */
char ROGDEFS[] = ".rog_defs";
