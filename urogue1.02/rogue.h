/*
 *
 * Rogue definitions and variable declarations
 *
 */
#include "metarog.h"
#include <curses.h>

#define CCHAR(x) ((x & A_CHARTEXT))

#define MAXDAEMONS 60

#define NCOLORS 32
#define NSTONES 35
#define NWOOD 24
#define NMETAL 15
#define NSYLLS 159

/*
 * Maximum number of different things
 */
#define MAXROOMS 9
#define MAXTHINGS 9
#define MAXOBJ 9
#define MAXPACK 23
#define MAXTREAS 30   /* number monsters/treasure in treasure room */
#define MAXTRAPS 30   /* max traps per level */
#define MAXTRPTRY 16  /* attempts/level allowed for setting traps */
#define MAXDOORS 4    /* Maximum doors to a room */
#define MAXPRAYERS 12 /* Maximum number of prayers for cleric */
#define MAXSPELLS 21  /* Maximum number of spells (for magician) */
#define NUMMONST 124  /* Current number of monsters */
#define NUMUNIQUE 17  /* number of UNIQUE creatures */
#define NLEVMONS 2    /* Number of new monsters per level */
#define MAXPURCH 8    /* max purchases per trading post visit */
#define LINELEN 80    /* characters in a buffer */

/* These defines are used by get_play.c */
#define I_STR 0
#define I_INTEL 1
#define I_WIS 2
#define I_DEX 3
#define I_WELL 4
#define I_APPEAL 5
#define I_HITS 6
#define I_ARM 7
#define I_WEAP 8
#define I_CHAR 9
#define I_WEAPENCH 10
#define MAXPATT 11 /* Total Number of above defines. */
#define MAXPDEF 4  /* Maximum number of pre-defined chars */

/* Movement penalties */
#define BACKPENALTY 3
#define SHOTPENALTY 2 /* In line of sight of missile */
#define DOORPENALTY 1 /* Moving out of current room */

/*
 * stuff to do with encumberance
 */
#define NORMENCB 1500 /* normal encumberance */
#define F_OK 0        /* have plenty of food in stomach */
#define F_HUNGRY 1    /* player is hungry */
#define F_WEAK 2      /* weak from lack of food */
#define F_FAINT 3     /* fainting from lack of food */

/*
 * return values for get functions
 */
#define NORM 0  /* normal exit */
#define QUIT 1  /* quit option setting */
#define MINUS 2 /* back up one option */

/*
 * The character types
 */
#define C_FIGHTER 0
#define C_MAGICIAN 1
#define C_CLERIC 2
#define C_THIEF 3
#define C_MONSTER 4

/*
 * values for games end
 */
#define SCOREIT -1
#define KILLED 0
#define CHICKEN 1
#define WINNER 2
#define TOTAL 3

/*
 * definitions for function step_ok:
 *     MONSTOK indicates it is OK to step on a monster -- it
 *     is only OK when stepping diagonally AROUND a monster
 */
#define MONSTOK 1
#define NOMONST 2

/*
 * used for ring stuff
 */
#define LEFT_1 0
#define LEFT_2 1
#define LEFT_3 2
#define LEFT_4 3
#define RIGHT_1 4
#define RIGHT_2 5
#define RIGHT_3 6
#define RIGHT_4 7

/*
 * All the fun defines
 */
#define inroom(rp, cp)                                                                             \
    ((cp)->x <= (rp)->r_pos.x + ((rp)->r_max.x - 1) && (rp)->r_pos.x <= (cp)->x &&                 \
     (cp)->y <= (rp)->r_pos.y + ((rp)->r_max.y - 1) && (rp)->r_pos.y <= (cp)->y)
#define winat(y, x)                                                                                \
    (mvwinch(mw, y, x) == ' ' ? (mvwinch(stdscr, y, x) & A_CHARTEXT) : (winch(mw) & A_CHARTEXT))
#define debug                                                                                      \
    if(wizard)                                                                                     \
    msg
#define RN (((seed = seed * 11109 + 13849) & 0x7fff) >> 1)
#define unc(cp) (cp).y, (cp).x
#define cmov(xy) move((xy).y, (xy).x)
#define DISTANCE(y1, x1, y2, x2) ((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1))
#define OBJPTR(what) (object *)((*what).l_data)
#define THINGPTR(what) (thing *)((*what).l_data)
#define ce(a, b) ((a).x == (b).x && (a).y == (b).y)
#define draw(window) wrefresh(window)
#define hero player.t_pos
#define pstats player.t_stats
#define max_stats player.maxstats
#define pack player.t_pack
#define attach(a, b) _attach(&a, b)
#define detach(a, b) _detach(&a, b)
#define free_list(a) _free_list(&a)
#ifndef CTRL
#define CTRL(ch) (ch & 037)
#endif
#define ALLOC(x) calloc((size_t)(x), 1)
#define FREE(x) free((char *)x)
#define EQSTR(a, b, c) (strncmp(a, b, (size_t)c) == 0)
#define GOLDCALC() (rnd(50 + 10 * level) + 2)
#define ISRING(h, r) (cur_ring[h] != NULL && cur_ring[h]->o_which == r)
#define ISWEARING(r)                                                                               \
    (ISRING(LEFT_1, r) || ISRING(LEFT_2, r) || ISRING(LEFT_3, r) || ISRING(LEFT_4, r) ||           \
     ISRING(RIGHT_1, r) || ISRING(RIGHT_2, r) || ISRING(RIGHT_3, r) || ISRING(RIGHT_4, r))
#define newgrp() ++group
#define o_charges o_ac
#define ISMULT(type) (type == FOOD)
#define isrock(ch) ((ch == WALL) || (ch == '-') || (ch == '|'))
#define is_stealth(tp) (rnd(25) < (tp)->t_stats.s_dext || (tp == &player && ISWEARING(R_STEALTH)))
#define mi_wght mi_worth

/*
 * Ways to die
 */
#define D_PETRIFY -1
#define D_ARROW -2
#define D_DART -3
#define D_POISON -4
#define D_BOLT -5
#define D_SUFFOCATION -6
#define D_POTION -7
#define D_INFESTATION -8
#define D_DROWN -9
#define D_FALL -10
#define D_FIRE -11

/*
 * Things that appear on the screens
 */
#define WALL ' '
#define PASSAGE '#'
#define DOOR '+'
#define FLOOR '.'
#define VPLAYER '@'
#define IPLAYER '_'
#define POST '^'
#define LAIR '('
#define RUSTTRAP ';'
#define TRAPDOOR '>'
#define ARROWTRAP '{'
#define SLEEPTRAP '$'
#define BEARTRAP '}'
#define TELTRAP '~'
#define DARTTRAP '`'
#define POOL '"'
#define MAZETRAP '\\'
#define FIRETRAP '<'
#define POISONTRAP '['
#define ARTIFACT ','
#define SECRETDOOR '&'
#define STAIRS '%'
#define GOLD '*'
#define POTION '!'
#define SCROLL '?'
#define MAGIC '$'
#define BMAGIC '>' /*     Blessed     magic     */
#define CMAGIC '<' /*     Cursed     magic     */
#define FOOD ':'
#define WEAPON ')'
#define ARMOR ']'
#define RING '='
#define STICK '/'
#define CALLABLE -1
#define MARKABLE -2

/*
 * Various constants
 */
#define MAXAUTH 10 /* Let's be realistic! */
#define PASSWD rpass.rp_pass
#define PASSWDSEED rpass.rp_pkey
#define BEARTIME 3
#define SLEEPTIME 4
#define FREEZETIME 6
#define HEALTIME 30
#define HOLDTIME 2
#define CHILLTIME (roll(2, 4))
#define SMELLTIME 20
#define STONETIME 8
#define SICKTIME 10
#define STPOS 0
#define WANDERTIME 70
#define HEROTIME 20
#define BEFORE 1
#define AFTER 2
#define HUHDURATION 20
#define SEEDURATION 850
#define CLRDURATION 15
#define GONETIME 200
#define PHASEDURATION 300
#define HUNGERTIME 1300
#define MORETIME 150
#define STINKTIME 6
#define STOMACHSIZE 2000
#define ESCAPE 27
#define LINEFEED 10
#define CARRIAGE_RETURN 13
#define BOLT_LENGTH 10
#define MARKLEN 20

/*
 * Save against things
 */
#define VS_POISON 0
#define VS_PARALYZATION 0
#define VS_DEATH 0
#define VS_PETRIFICATION 1
#define VS_WAND 2
#define VS_BREATH 3
#define VS_MAGIC 4

/*
 * attributes for treasures in dungeon
 */
#define ISCURSED 01u
#define ISKNOW 02u
#define ISPOST 04u /* object is in a trading post */
#define ISMETAL 010u
#define ISPROT 020u /* object is protected */
#define ISBLESSED 040u
#define ISZAPPED 0100u   /* weapon has been charged by dragon */
#define ISVORPED 0200u   /* vorpalized weapon */
#define ISSILVER 0400u   /* silver weapon */
#define ISPOISON 01000u  /* poisoned weapon */
#define CANRETURN 02000u /* weapon returns if misses */
#define ISOWNED 04000u   /* weapon returns always */
#define ISLOST 010000u   /* weapon always disappears */
#define ISMISL 020000u
#define ISMANY 040000
#define CANBURN 0100000u /* burns monsters */
#define ISHOLY 0200000u  /* double damage on undead monsters */
/*
 * Various flag bits
 */
#define ISDARK 01u
#define ISGONE 02u
#define ISTREAS 04u
#define ISFOUND 010u
#define ISTHIEFSET 020u
/*
 * 1st set of creature flags (this might include player)
 */
#define ISBLIND 01u
#define ISINWALL 02u
#define ISRUN 04u
#define ISFLEE 010u
#define ISINVIS 020u
#define ISMEAN 040u
#define ISGREED 0100u
#define CANSHOOT 0200u
#define ISHELD 0400u
#define ISHUH 01000u
#define ISREGEN 02000u
#define CANHUH 04000u
#define CANSEE 010000u
#define HASFIRE 020000u
#define ISSLOW 040000u
#define ISHASTE 0100000u
#define ISCLEAR 0200000u
#define CANINWALL 0400000u
#define ISDISGUISE 01000000u
#define CANBLINK 02000000u
#define CANSNORE 04000000u
#define HALFDAMAGE 010000000u
#define CANSUCK 020000000u
#define CANRUST 040000000u
#define CANPOISON 0100000000u
#define CANDRAIN 0200000000u
#define ISUNIQUE 0400000000u
#define STEALGOLD 01000000000u
#define STEALMAGIC 02000000000u
#define CANDISEASE 04000000000u

/*
 * Second set of flags
 */
#define HASDISEASE 010000000001u
#define CANSUFFOCATE 010000000002u
#define DIDSUFFOCATE 010000000004u
#define BOLTDIVIDE 010000000010u
#define BLOWDIVIDE 010000000020u
#define NOCOLD 010000000040u
#define TOUCHFEAR 010000000100u
#define BMAGICHIT 010000000200u
#define NOFIRE 010000000400u
#define NOBOLT 010000001000u
#define CARRYGOLD 010000002000u
#define CANITCH 010000004000u
#define HASITCH 010000010000u
#define DIDDRAIN 010000020000u
#define WASTURNED 010000040000u
#define CANSELL 010000100000u
#define CANBLIND 010000200000u
#define CANBBURN 010000400000u
#define ISCHARMED 010001000000u
#define CANSPEAK 010002000000u
#define CANFLY 010004000000u
#define ISFRIENDLY 010010000000u
#define CANHEAR 010020000000u
#define ISDEAF 010040000000u
#define CANSCENT 010100000000u
#define ISUNSMELL 010200000000u
#define WILLRUST 010400000000u
#define WILLROT 011000000000u
#define SUPEREAT 012000000000u
#define PERMBLIND 014000000000u

/*
 * Third set of flags
 */
#define MAGICHIT 020000000001u
#define CANINFEST 020000000002u
#define HASINFEST 020000000004u
#define NOMOVE 020000000010u
#define CANSHRIEK 020000000020u
#define CANDRAW 020000000040u
#define CANSMELL 020000000100u
#define CANPARALYZE 020000000200u
#define CANROT 020000000400u
#define ISSCAVENGE 020000001000u
#define DOROT 020000002000u
#define CANSTINK 020000004000u
#define HASSTINK 020000010000u
#define ISSHADOW 020000020000u
#define CANCHILL 020000040000u
#define CANHUG 020000100000u
#define CANSURPRISE 020000200000u
#define CANFRIGHTEN 020000400000u
#define CANSUMMON 020001000000u
#define TOUCHSTONE 020002000000u
#define LOOKSTONE 020004000000u
#define CANHOLD 020010000000u
#define DIDHOLD 020020000000u
#define DOUBLEDRAIN 020040000000u
#define ISUNDEAD 020100000000u
#define BLESSMAP 020200000000u
#define BLESSGOLD 020400000000u
#define BLESSMONS 021000000000u
#define BLESSMAGIC 022000000000u
#define BLESSFOOD 024000000000u

/*
 * Fourth set of flags
 */
#define CANBRANDOM 030000000001u /* Types of breath */
#define CANBACID 030000000002u
#define CANBFIRE 030000000004u
#define CANBBOLT 030000000010u
#define CANBGAS 030000000020u
#define CANBICE 030000000040u
#define CANBPGAS 030000000100u   /* Paralyze gas */
#define CANBSGAS 030000000200u   /* Sleeping gas */
#define CANBSLGAS 030000000400u  /* Slow gas */
#define CANBFGAS 030000001000u   /* Fear gas */
#define CANBREATHE 030000001777u /* Can it breathe at all? */
#define STUMBLER 030000002000u
#define POWEREAT 030000004000u
#define ISELECTRIC 030000010000u
#define HASOXYGEN 030000020000u
#define POWERDEXT 030000040000u
#define POWERSTR 030000100000u
#define POWERWISDOM 030000200000u
#define POWERINTEL 030000400000u
#define POWERCONST 030001000000u
#define SUPERHERO 030002000000u
#define ISUNHERO 030004000000u

#define ISREADY 040000000001u
#define ISDEAD 040000000002u

/* Masks for choosing the right flag */
#define FLAGMASK 030000000000u
#define FLAGINDEX 000000000003u
#define FLAGSHIFT 30u

/*
 * Mask for cancelling special abilities
 * The flags listed here will be the ones left on after the
 * cancellation takes place
 */
#define CANC0MASK                                                                                  \
    (ISBLIND | ISINWALL | ISRUN | ISFLEE | ISMEAN | ISGREED | CANSHOOT | ISHELD | ISHUH | ISSLOW | \
     ISHASTE | ISCLEAR | ISUNIQUE | CARRYGOLD)
#define CANC1MASK                                                                                  \
    (HASDISEASE | DIDSUFFOCATE | CARRYGOLD | HASITCH | CANSELL | CANBBURN | CANSPEAK | CANFLY |    \
     ISFRIENDLY)
#define CANC2MASK (HASINFEST | NOMOVE | ISSCAVENGE | DOROT | HASSTINK | DIDHOLD | ISUNDEAD)
#define CANC3MASK (CANBREATHE)

/* types of things */
#define TYP_POTION 0
#define TYP_SCROLL 1
#define TYP_FOOD 2
#define TYP_WEAPON 3
#define TYP_ARMOR 4
#define TYP_RING 5
#define TYP_STICK 6
#define TYP_ARTIFACT 7
#define NUMTHINGS 8

/*
 * Potion types
 */
#define P_CLEAR 0
#define P_ABIL 1
#define P_SEEINVIS 2
#define P_HEALING 3
#define P_MFIND 4
#define P_TFIND 5
#define P_RAISE 6
#define P_HASTE 7
#define P_RESTORE 8
#define P_PHASE 9
#define P_INVIS 10
#define P_SMELL 11
#define P_HEAR 12
#define P_SHERO 13
#define P_DISGUISE 14
#define MAXPOTIONS 15

/*
 * Scroll types
 */
#define S_CONFUSE 0
#define S_MAP 1
#define S_LIGHT 2
#define S_HOLD 3
#define S_SLEEP 4
#define S_ALLENCH 5
#define S_IDENT 6
#define S_SCARE 7
#define S_GFIND 8
#define S_TELEP 9
#define S_CREATE 10
#define S_REMOVE 11
#define S_PETRIFY 12
#define S_GENOCIDE 13
#define S_CURING 14
#define S_MAKEIT 15
#define S_PROTECT 16
#define S_NOTHING 17
#define S_SILVER 18
#define S_OWNERSHIP 19
#define S_FOODFIND 20
#define S_ELECTRIFY 21
#define MAXSCROLLS 22

/*
 * Weapon types
 */
#define MACE 0         /* mace */
#define SWORD 1        /* long sword */
#define BOW 2          /* short bow */
#define ARROW 3        /* arrow */
#define DAGGER 4       /* dagger */
#define ROCK 5         /* rocks */
#define TWOSWORD 6     /* two-handed sword */
#define SLING 7        /* sling */
#define DART 8         /* darts */
#define CROSSBOW 9     /* crossbow */
#define BOLT 10        /* crossbow bolt */
#define SPEAR 11       /* spear */
#define TRIDENT 12     /* trident */
#define SPETUM 13      /* spetum */
#define BARDICHE 14    /* bardiche */
#define SPIKE 15       /* short pike */
#define BASWORD 16     /* bastard sword */
#define HALBERD 17     /* halberd */
#define BATTLEAXE 18   /* battle axe */
#define SILVERARROW 19 /* silver arrows */
#define HANDAXE 20     /* hand axe */
#define CLUB 21        /* club */
#define FLAIL 22       /* flail */
#define GLAIVE 23      /* glaive */
#define GUISARME 24    /* guisarme */
#define HAMMER 25      /* hammer */
#define JAVELIN 26     /* javelin */
#define MSTAR 27       /* morning star */
#define PARTISAN 28    /* partisan */
#define PICK 29        /* pick */
#define LPIKE 30       /* long pike */
#define SCIMITAR 31    /* scimitar */
#define BULLET 32      /* sling bullet */
#define QSTAFF 33      /* quarter staff */
#define BRSWORD 34     /* broad sword */
#define SHSWORD 35     /* short sword */
#define SHIRIKEN 36    /* shurikens */
#define BOOMERANG 37   /* boomerangs */
#define MOLOTOV 38     /* molotov cocktails */
#define CLAYMORE 39    /* claymore sword */
#define CRYSKNIFE 40   /* crysknife */
#define FOOTBOW 41     /* footbow */
#define FBBOLT 42      /* footbow bolt */
#define MACHETE 43     /* machete */
#define LEUKU 44       /* leuku */
#define TOMAHAWK 45    /* tomahawk */
#define PERTUSKA 46    /* pertuska */
#define SABRE 47       /* sabre */
#define CUTLASS 48     /* cutlass sword */
#define GRENADE 49     /* grenade */
#define MAXWEAPONS 50  /* types of weapons */
#define NONE 100       /* no weapon */

/*
 * Armor types
 */
#define LEATHER 0
#define RING_MAIL 1
#define STUDDED_LEATHER 2
#define SCALE_MAIL 3
#define PADDED_ARMOR 4
#define CHAIN_MAIL 5
#define SPLINT_MAIL 6
#define BANDED_MAIL 7
#define PLATE_MAIL 8
#define PLATE_ARMOR 9
#define MITHRIL 10
#define CRYSTAL_ARMOR 11
#define MAXARMORS 12

/*
 * Ring types
 */
#define R_PROTECT 0
#define R_ADDSTR 1
#define R_SUSABILITY 2
#define R_SEARCH 3
#define R_SEEINVIS 4
#define R_ALERT 5
#define R_AGGR 6
#define R_ADDHIT 7
#define R_ADDDAM 8
#define R_REGEN 9
#define R_DIGEST 10
#define R_TELEPORT 11
#define R_STEALTH 12
#define R_ADDINTEL 13
#define R_ADDWISDOM 14
#define R_HEALTH 15
#define R_VREGEN 16
#define R_LIGHT 17
#define R_DELUSION 18
#define R_CARRYING 19
#define R_ADORNMENT 20
#define R_LEVITATION 21
#define R_FIRERESIST 22
#define R_COLDRESIST 23
#define R_ELECTRESIST 24
#define R_RESURRECT 25
#define R_BREATHE 26
#define R_FREEDOM 27
#define R_WIZARD 28
#define R_TELCONTROL 29
#define MAXRINGS 30

/*
 * Rod/Wand/Staff types
 */

#define WS_LIGHT 0
#define WS_HIT 1
#define WS_ELECT 2
#define WS_FIRE 3
#define WS_COLD 4
#define WS_POLYMORPH 5
#define WS_MISSILE 6
#define WS_SLOW_M 7
#define WS_DRAIN 8
#define WS_CHARGE 9
#define WS_TELMON 10
#define WS_CANCEL 11
#define WS_CONFMON 12
#define WS_ANNIH 13
#define WS_ANTIM 14
#define WS_PARALYZE 15
#define WS_MDEG 16
#define WS_NOTHING 17
#define WS_INVIS 18
#define WS_BLAST 19
#define MAXSTICKS 20

/*
 * Food types
 */

#define FD_RATION 0
#define FD_FRUIT 1
#define FD_CRAM 2
#define FD_CAKES 3
#define FD_LEMBA 4
#define FD_MIRUVOR 5
#define MAXFOODS 6

/*
 * Artifact types
 */

#define TR_PURSE 0
#define TR_PHIAL 1
#define TR_AMULET 2
#define TR_PALANTIR 3
#define TR_CROWN 4
#define TR_SCEPTRE 5
#define TR_SILMARIL 6
#define TR_WAND 7
#define MAXARTIFACT 8

/*
 * Artifact flags
 */

#define ISUSED 01u
#define ISACTIVE 02u

/*
 * Now we define the structures and types
 */

enum daemon_id {
    d_none,
    d_rollwand,
    d_doctor,
    d_stomach,
    d_runners,
    d_swander,
    d_unscent,
    d_unelectrify,
    d_unshero,
    d_unbhero,
    d_unxray,
    d_wghtchk,
    d_unstink,
    d_res_strength,
    d_un_itch,
    d_cure_disease,
    d_unconfuse,
    d_suffocate,
    d_undisguise,
    d_shero,
    d_hear,
    d_unhear,
    d_sight,
    d_scent,
    d_nohaste,
    d_unclrhead,
    d_unsee,
    d_unphase,

    d_noslow,
    d_appear,

    // d_player_doctor,

    d_dont_save,

};

struct delayed_action {
    int d_type;
    enum daemon_id d_id;
    void *d_arg;
    int d_time;
};

/*
 * level types
 */
typedef enum {
    NORMLEV, /* normal level */
    POSTLEV, /* trading post level */
    MAZELEV, /* maze level */
    THRONE   /* unique monster's throne room */
} LEVTYPE;

/*
 * Help list
 */

struct h_list {
    int h_ch;
    const char *h_desc;
};

/*
 * Coordinate data type
 */
struct coord {
    int x;
    int y;
};

/*
 * Linked list data type
 */
struct linked_list {
    linked_list *l_next;
    linked_list *l_prev;
    char *l_data;  /* Various structure pointers */
    char l_letter; /* Letter for inventory */
};

inline linked_list *next(linked_list *ptr) {
    return (*ptr).l_next;
}
inline linked_list *prev(linked_list *ptr) {
    return (*ptr).l_prev;
}
inline char *&ldata(linked_list *ptr) {
    return (*ptr).l_data;
}

/*
 * Stuff about magic items
 */

struct magic_item {
    const char *mi_name;
    int mi_prob;
    int mi_worth;
    int mi_curse;
    int mi_bless;
};

/*
 * Room structure
 */
struct room {
    coord r_pos;            /* Upper left corner */
    coord r_max;            /* Size of room */
    unsigned int r_flags;   /* Info about the room */
    int r_fires;            /* Number of fires in room */
    int r_nexits;           /* Number of exits */
    coord r_exit[MAXDOORS]; /* Where the exits are */
};

/*
 * Initial artifact stats
 */
struct init_artifact {
    const char *ar_name; /* name of the artifact */
    int ar_level;        /* first level where it appears */
    int ar_rings;        /* number of ring effects */
    int ar_potions;      /* number of potion effects */
    int ar_scrolls;      /* number of scroll effects */
    int ar_wands;        /* number of wand effects */
    int ar_worth;        /* gold pieces */
    int ar_weight;       /* weight of object */
};

/*
 * Artifact attributes
 */
struct artifact {
    unsigned int ar_flags;   /* general flags */
    unsigned int ar_rings;   /* ring effects flags */
    unsigned int ar_potions; /* potion effects flags */
    unsigned int ar_scrolls; /* scroll effects flags */
    unsigned int ar_wands;   /* wand effects flags */
    linked_list *t_art;      /* linked list pointer */
};

/*
 * Array of all traps on this level
 */
struct trap {
    int tr_type;           /* What kind of trap */
    int tr_show;           /* Where disguised trap looks like */
    coord tr_pos;          /* Where trap is */
    unsigned int tr_flags; /* Info about trap (i.e. ISFOUND) */
};

/*
 * Structure describing a fighting being
 */
struct stats {
    int s_str;      /* Strength */
    int s_intel;    /* Intelligence */
    int s_wisdom;   /* Wisdom */
    int s_dext;     /* Dexterity */
    int s_const;    /* Constitution */
    int s_charisma; /* Charisma */
    int s_exp;      /* Experience */
    int s_lvl;      /* Level of mastery */
    int s_arm;      /* Armor class */
    int s_hpt;      /* Hit points */
    int s_pack;     /* current weight of his pack */
    int s_carry;    /* max weight he can carry */
    char s_dmg[30]; /* String describing damage done */
};

/*
 * Structure describing a fighting being (monster at initialization)
 */
struct mstats {
    int s_str;         /* Strength */
    int s_exp;         /* Experience */
    int s_lvl;         /* Level of mastery */
    int s_arm;         /* Armor class */
    const char *s_hpt; /* Hit points */
    const char *s_dmg; /* String describing damage done */
};

/*
 * Structure for monsters and player
 */
struct thing {
    int t_turn;              /* If slowed, is it a turn to move */
    int t_wasshot;           /* Was character shot last round? */
    int t_type;              /* What it is */
    int t_disguise;          /* What mimic looks like */
    int t_oldch;             /* Character that was where it was */
    int t_ctype;             /* Character type */
    int t_index;             /* Index into monster table */
    int t_no_move;           /* How long the thing can't move */
    int t_quiet;             /* used in healing */
    int t_doorgoal;          /* What door are we heading to? */
    coord t_pos;             /* Position */
    coord t_oldpos;          /* Last position */
    coord *t_dest;           /* Where it is running to */
    unsigned int t_flags[4]; /* State word */
    linked_list *t_pack;     /* What the thing is carrying */
    stats t_stats;           /* Physical description */
    stats maxstats;          /* maximum(or initial) stats */
    int t_reserved;
};

inline auto on(thing &th, unsigned long flag) {
    return (((th).t_flags[(flag >> FLAGSHIFT) & FLAGINDEX] & flag) != 0);
}
inline auto off(thing &th, unsigned long flag) {
    return (((th).t_flags[(flag >> FLAGSHIFT) & FLAGINDEX] & flag) == 0);
}
inline void turn_on(thing &th, unsigned long flag) {
    ((th).t_flags[(flag >> FLAGSHIFT) & FLAGINDEX] |= (flag & ~FLAGMASK));
}
inline void turn_off(thing &th, unsigned long flag) {
    ((th).t_flags[(flag >> FLAGSHIFT) & FLAGINDEX] &= ~flag);
}

/*
 * Array containing information on all the various types of monsters
 */
struct monster {
    const char *m_name;       /* What to call the monster */
    int m_carry;              /* Probability of carrying something */
    int m_normal;             /* Does monster exist? */
    int m_wander;             /* Does monster wander? */
    int m_appear;             /* What does monster look like? */
    const char *m_intel;      /* Intelligence range */
    unsigned int m_flags[10]; /* Things about the monster */
    const char *m_typesum;    /* type of creature can he summon */
    int m_numsum;             /* how many creatures can he summon */
    int m_add_exp;            /* Added experience per hit point */
    mstats m_stats;           /* Initial stats */
};

/*
 * Structure for a thing that the rogue can carry
 */

struct object {
    int o_type;           /* What kind of object it is */
    coord o_pos;          /* Where it lives on the screen */
    const char *o_text;   /* What it says if you read it */
    int o_launch;         /* What you need to launch it */
    char o_damage[8];     /* Damage if used like sword */
    char o_hurldmg[8];    /* Damage if thrown */
    int o_count;          /* Count for plural objects */
    int o_which;          /* Which object of a type it is */
    int o_hplus;          /* Plusses to hit */
    int o_dplus;          /* Plusses to damage */
    int o_ac;             /* Armor class */
    unsigned int o_flags; /* Information about objects */
    int o_group;          /* Group number for this object */
    int o_weight;         /* weight of this object */
    char o_mark[MARKLEN]; /* Mark the specific object */
    artifact art_stats;   /* substructure for artifacts */
};
/*
 * weapon structure
 */
struct init_weps {
    const char *w_name; /* name of weapon */
    const char *w_dam;  /* hit damage */
    const char *w_hrl;  /* hurl damage */
    int w_launch;       /* need to launch it */
    int w_flags;        /* flags */
    int w_wght;         /* weight of weapon */
    int w_worth;        /* worth of this weapon */
};

/*
 * armor structure
 */
struct init_armor {
    const char *a_name; /* name of armor */
    int a_prob;         /* chance of getting armor */
    int a_class;        /* normal armor class */
    int a_worth;        /* worth of armor */
    int a_wght;         /* weight of armor */
};

struct matrix {
    int base;    /* Base to-hit value (AC 10) */
    int max_lvl; /* Maximum level for changing value */
    int factor;  /* Amount base changes each time */
    int offset;  /* What to offset level */
    int range;   /* Range of levels for each offset */
};

struct spells {
    int s_which;   /* which scroll or potion */
    int s_cost;    /* cost of casting spell */
    int s_type;    /* scroll or potion */
    int s_blessed; /* is the spell blessed? */
};

struct real_pass {
    char rp_pass[20];
    char rp_pkey[2];
};

void waste_time(); /* armor.c */
void wear();
void take_off();

void new_artifact(int which, object *cur); /* artifact.c */
int make_artifact();
void apply();
int possessed(int artifact);
void do_minor(object *tr);
void do_major();
void do_phial();
void do_palantir();
void do_silmaril();
void do_amulet();
void do_bag(object *obj);
void do_sceptre();
void do_wand();
void do_crown();
void add_bag(linked_list **bag);
linked_list *get_bag(linked_list **bag);
void bag_inventory(linked_list *list);
int bag_char(object *obj, linked_list *bag);
void bagletter(linked_list *item);
void delbagletter(linked_list *item);
int is_carrying(int artifact);

int cansee(int y, int x); /* chase.c */
void runto(coord *runner, coord *spot);
int diag_ok(coord *sp, coord *ep, thing *flgptr);
room *roomin(coord *cp);
linked_list *find_mons(int y, int x);
void runners();
int can_blink(thing *tp);
coord *can_shoot(coord *er, coord *ee);
linked_list *get_hurl(thing *tp);
int straight_shot(int ery, int erx, int eey, int eex, coord *shooting);
void do_chase(thing *th, int flee);
int chase(thing *tp, coord *ee, int flee, int *mdead);

void quit(int sig); /* command.c */
void command();
void call(int mark);
void d_level();
void u_level();
void help();
void identify();
void search(int is_thief);

void extinguish(enum daemon_id did); /* daemon.c */
delayed_action *find_slot(enum daemon_id did);
void lengthen(enum daemon_id did, int xtime);
void fuse(enum daemon_id did, void *arg, int time, int type);
void start_daemon(enum daemon_id did, void *arg, int type);
void kill_daemon(enum daemon_id did);
void do_daemons(int flag);
void do_fuses(int flag);
void activity();

void unsee(); /* daemons.c */
void unconfuse();
void unscent();
void sight();
void res_strength();
void nohaste();
void noslow();
void suffocate();
void stomach();
void cure_disease();
void un_itch();
void appear();
void unelectrify();
void unshero();
void unbhero();
void unxray();
void undisguise();
void shero();
void unphase();
void unclrhead();
void unstink();
void hear();
void unhear();
void scent();
void rollwand();
void swander();
void doctor(thing *tp);

void updpack(int getmax); /* encumb.c */
int itemweight(object *wh);
int totalenc();
int hitweight();
int packweight();
int playenc();
void wghtchk();

int fight(coord *mp, object *weap, int thrown); /* fight.c */
int attack(thing *mp, object *weapon, int thrown);
int save_throw(int which, thing *tp);
void killed(linked_list *item, int pr, int points);
int save(int which);
int is_magic(object *obj);
void raise_level();
void check_level();
int swing(int cclass, int at_lvl, int op_arm, int wplus);
void remove_monster(coord *mp, linked_list *item);
object *wield_weap(object *weapon, thing *mp);
void m_thunk(object *weap, const char *mname);
void bounce(object *weap, const char *mname);
void m_bounce(object *weap, const char *mname);
void do_fight(int y, int x, int tothedeath);
int roll_em(thing *att_er, thing *def_er, object *weap, int hurl, object *cur_weapon);
int hung_dam();
void hit(const char *er, const char *ee);
void miss(const char *er, const char *ee);
int dext_plus(int dexterity);
int dext_prot(int dexterity);
int str_plus(int str);
int add_dam(int str);
void thunk(object *weap, const char *mname);
const char *prname(const char *who, int upper);

int geta_player(); /* get_play.c */
int puta_player(int arm, int wpt, int hpadd, int dmadd);

void init_things(); /* init.c */
void init_fd();
void init_colors();
void init_names();
void init_stones();
void init_materials();
void badcheck(const char *name, magic_item *magic, int bound);
void init_player();

void doadd(const char *fmt, va_list ap); /* io.c */
void msg(const char *fmt, ...);
void addmsg(const char *fmt, ...);
void endmsg();
int readchar(WINDOW *win);
void restscr(WINDOW *scr);
void wait_for(WINDOW *win, int ch);
int shoot_ok(int ch);
void status(int display);
int step_ok(int y, int x, int can_on_monst, thing *flgptr);
void dbotline(WINDOW *scr, const char *message);
void show_win(WINDOW *scr, const char *message);
void ministat();

void _detach(linked_list **list, linked_list *item); /* list.c */
void _attach(linked_list **list, linked_list *item);
void _free_list(linked_list **ptr);
void discard(linked_list *item);
linked_list *new_item(int size);
char *newalloc(size_t size);
linked_list *creat_item();

int rnd(int range); /* main.c */
int roll(int number, int sides);
int makesure();
void setup();
[[noreturn]] void playit();
[[noreturn]] void fatal(const char *s);
void areuok(char *file);
[[noreturn]] void endit(int sig);
void tstp(int sig);

void do_maze(); /* maze.c */
int maze_view(int y, int x);
char *moffset(int y, int x);
char *foffset(int y, int x);
int findcells(int y, int x);
void rmwall(int newy, int newx, int oldy, int oldx);
void crankout();
void draw_maze();

int is_current(object *obj); /* misc.c */
void chg_str(int amt, int both, int lost);
void chg_dext(int amt, int both, int lost);
void aggravate();
void look(int wakeup);
void add_haste(int blessed);
int get_dir();
const char *tr_name(int ch);
int secretdoor(int y, int x);
linked_list *find_obj(int y, int x);
void eat();
const char *vowelstr(const char *str);
void listenfor();

void new_monster(linked_list *item, int type, coord *cp, int max_monster); /* monsters.c */
void check_residue(thing *tp);
void genocide();
int randmonster(int wander, int no_unique);
void wanderer();
linked_list *wake_monster(int y, int x);
int id_monst(int monster);
void sell(thing *tp);

void light(coord *cp); /* move.c */
int show(int y, int x);
int blue_light(int blessed, int cursed);
void do_run(int ch);
void corr_move(int dy, int dx);
void do_move(int dy, int dx);
int be_trapped(thing *th, coord *tc);
void dip_it();
coord *rndmove(thing *who);
int isatrap(int ch);
trap *trap_at(int y, int x);
void set_trap(thing *tp, int y, int x);

int rnd_room(); /* new_level.c */
void new_level(LEVTYPE ltype);
void put_things(LEVTYPE ltype);
void do_throne();

int get_str(void *vp, WINDOW *win); /* options.c */
void option();
void put_bool(void *b, WINDOW *win);
void put_str(void *str, WINDOW *win);
int get_bool(void *vp, WINDOW *win);
int get_abil(void *vp, WINDOW *win);
void parse_opts(char *str);
void strucpy(char *s1, char *s2, size_t len);
void put_abil(void *ability, WINDOW *win);

void freeletter(linked_list *item); /* pack.c */
void idenpack();
int add_pack(linked_list *item, int silent);
int inventory(linked_list *list, int type);
void pick_up(int ch);
void picky_inven();
linked_list *get_item(const char *purpose, int type);
void del_pack(linked_list *what);
void cur_null(object *op);
void getletter(linked_list *item);
int pack_char(object *obj);
void show_floor();

void do_passages(); /* passages.c */
void conn(int r1, int r2);
void door(room *rm, coord *cp);

int const_bonus(); /* player.c */
void gsense();
void affect();
void cast();
void steal();
void pray();

void quaff(int which, int blessed); /* potions.c */
void add_intelligence(int cursed);
void add_wisdom(int cursed);
void add_dexterity(int cursed);
void add_const(int cursed);
void add_strength(int cursed);
void lower_level(int who);
void res_dexterity();
void res_wisdom();
void res_intelligence();

int ring_value(int type); /* rings.c */
void ring_on();
int ring_eat(int hand);
const char *ring_num(object *obj);
void ring_off();

void death(int monst); /* rip.c */
[[noreturn]] void byebye(int sig);
void score(int amount, int flags, int monst);
void showpack(const char *howso);
[[noreturn]] void total_winner();
const char *killname(int monst);
int save_resurrect(int bonus);

void rnd_pos(room *rp, coord *cp); /* rooms.c */
void draw_room(room *rp);
void do_rooms();
void vert(int cnt);
void horiz(int cnt);

int save_game(); /* save.c */
[[noreturn]] void auto_save(int sig);
int save_file(char *save_file_name);
size_t encwrite(void *start, size_t size, FILE *outf);
size_t encread(void *start, size_t size, FILE *infd);
int restore(char *file);
size_t putword(int word, FILE *file);
int getword(FILE *fd);

void read_scroll(int which, int blessed); /* scrolls.c */
int creat_mons(thing *person, int monster, int report);
int is_r_on(object *obj);

int rs_save_file(const char *save_file_name); /* state.c */
int rs_restore_file(const char *save_file_name);

void fix_stick(object *cur); /* sticks.c */
char *charge_str(object *obj);
void do_zap(int gotdir, int which, int blessed);
void drain(int ymin, int ymax, int xmin, int xmax);
int shoot_bolt(thing *shooter, coord start, coord dir, int get_points, int reason, const char *name,
               int damage);

int dropcheck(object *op); /* things.c */
int drop(linked_list *item);
const char *inv_name(object *obj, int drop);
linked_list *spec_item(int type, int which, int hit, int damage);
int pick_one(magic_item *magic, int nitems);
int extras();
const char *blesscurse(unsigned int flags);
linked_list *new_thing();

void do_post(); /* trader.c */
int price_it();
void buy_it();
void sell_it();
int open_market();
char *typ_name(object *obj);
int get_worth(object *obj);
void trans_line();

int fall(linked_list *item, int pr); /* weapons.c */
void init_weapon(object *weap, int type);
void missile(int ydelta, int xdelta, linked_list *item, thing *tp);
void do_motion(object *obj, int ydelta, int xdelta, thing *tp);
int fallpos(coord *pos, coord *newpos, int passages);
int hit_monster(int y, int x, object *obj, thing *tp);
void wield();
const char *num(int n1, int n2);

void makemon(); /* wizard.c */
int getbless();
int teleport();
int passwd();
void whatis(linked_list *what);
void create_obj(int which_item, int which_type, int cursed);

/*
 * Now all the global variables
 */
extern h_list helpstr[];
extern FILE *fd_score;
extern trap traps[];
extern room rooms[];       /* One for each room -- A level */
extern room *oldrp;        /* Roomin(&oldpos) */
extern linked_list *mlist; /* List of monsters on the level */
extern linked_list *monst_dead;
extern thing player;                    /* The rogue */
extern thing *beast;                    /* The last monster attacking */
extern monster monsters[];              /* The initial monster states */
extern linked_list *lvl_obj;            /* List of objects on this level */
extern object *cur_weapon;              /* Which weapon he is weilding */
extern object *cur_armor;               /* What a well dresssed rogue wears */
extern object *cur_ring[];              /* Which rings are being worn */
extern magic_item things[];             /* Chances for each type of item */
extern magic_item s_magic[];            /* Names and chances for scrolls */
extern magic_item p_magic[];            /* Names and chances for potions */
extern magic_item r_magic[];            /* Names and chances for rings */
extern magic_item ws_magic[];           /* Names and chances for sticks */
extern magic_item fd_data[];            /* Names and chances for food */
extern spells magic_spells[];           /* spells for magic users */
extern spells cleric_spells[];          /* spells for magic users */
extern real_pass rpass;                 /* For protection's sake! */
extern const char *cnames[][11];        /* Character level names */
extern char curpurch[];                 /* name of item ready to buy */
extern int PLAYER;                      /* what the player looks like */
extern int def_array[MAXPDEF][MAXPATT]; /* Pre-def'd chars */
extern int resurrect;                   /* resurrection counter */
extern int char_type;                   /* what type of character is player */
extern int foodlev;                     /* how fast he eats food */
extern int see_dist;                    /* how far he can see^2 */
extern int level;                       /* What level rogue is on */
extern int monslevel;                   /* What level the monsters are from */
extern int trader;                      /* number of purchases */
extern int curprice;                    /* price of an item */
extern int purse;                       /* How much gold the rogue has */
extern size_t mpos;                     /* Where cursor is on top line */
extern int ntraps;                      /* Number of traps on this level */
extern int no_move;                     /* Number of turns held in place */
extern int no_command;                  /* Number of turns asleep */
extern int inpack;                      /* Number of things in pack */
extern int total;                       /* Total dynamic memory bytes */
extern int lastscore;                   /* Score before this turn */
extern int no_food;                     /* Number of levels without food */
extern int seed;                        /* Random number seed */
extern int command_repeat_count;        /* Number of times to repeat command */
extern int dnum;                        /* Dungeon number */
extern int max_level;                   /* Deepest player has gone */
extern int food_left;                   /* Amount of food in hero's stomach */
extern int group;                       /* Current group number */
extern int hungry_state;                /* How hungry is he */
extern int infest_dam;                  /* Damage from parasites */
extern int lost_str;                    /* Amount of strength lost */
extern int lost_dext;                   /* amount of dexterity lost */
extern int hold_count;                  /* Number of monsters holding player */
extern int trap_tries;                  /* Number of attempts to set traps */
extern int spell_power;                 /* Spell power left at this level */
extern int auth_or[MAXAUTH];            /* MAXAUTH priviledged players */
extern int has_artifact;                /* set for possesion of artifacts */
extern int picked_artifact;             /* set for any artifacts picked up */
extern int msg_index;                   /* pointer to current message buffer */
extern int luck;                        /* how expensive things to buy thing */
extern int take;                        /* Thing the rogue is taking */
extern char prbuf[];                    /* Buffer for sprintfs */
extern char outbuf[];                   /* Output buffer for stdout */
extern int runch;                       /* Direction player is running */
extern const char *s_names[];           /* Names of the scrolls */
extern const char *p_colors[];          /* Colors of the potions */
extern const char *r_stones[];          /* Stone settings of the rings */
extern init_weps weaps[];               /* weapons and attributes */
extern init_armor armors[];             /* armors and attributes */
extern init_artifact arts[];            /* artifacts and attributes */
extern const char *ws_made[];           /* What sticks are made of */
extern const char *release;             /* Release number of rogue */
extern char whoami[];                   /* Name of player */
extern char fruit[];                    /* Favorite fruit */
extern char msgbuf[10][2 * BUFSIZ];     /* message buffer */
extern char *s_guess[];                 /* Players guess at what scroll is */
extern char *p_guess[];                 /* Players guess at what potion is */
extern char *r_guess[];                 /* Players guess at what ring is */
extern char *ws_guess[];                /* Players guess at what wand is */
extern const char *ws_type[];           /* Is it a wand or a staff */
extern char save_file_name[];           /* Save file name */
extern char score_file[];               /* Score file name */
extern char home[];                     /* User's home directory */
extern WINDOW *cw;                      /* Window that the player sees */
extern WINDOW *hw;                      /* Used for the help command */
extern WINDOW *mw;                      /* Used to store mosnters */
extern WINDOW *msgw;
extern int pool_teleport; /* just teleported from a pool */
extern int inwhgt;        /* true if from wghtchk() */
extern int running;       /* True if player is running */
extern int fighting;      /* True if player is fighting */
extern int playing;       /* True until he quits */
extern int wizard;        /* True if allows wizard commands */
extern int after;         /* True if we want after daemons */
extern int notify;        /* True if player wants to know */
extern int fight_flush;   /* True if toilet input */
extern int terse;         /* True if we should be short */
extern int door_stop;     /* Stop running when we pass a door */
extern int jump;          /* Show running as series of jumps */
extern int slow_invent;   /* Inventory one line at a time */
extern int firstmove;     /* First move after setting door_stop */
extern int waswizard;     /* Was a wizard sometime */
extern int canwizard;     /* Will be permitted to do this */
extern int askme;         /* Ask about unidentified things */
extern int moving;        /* move using 'm' command */
extern int s_know[];      /* Does he know what a scroll does */
extern int p_know[];      /* Does he know what a potion does */
extern int r_know[];      /* Does he know what a ring does */
extern int ws_know[];     /* Does he know what a stick does */
extern coord oldpos;      /* Position before last look() call */
extern coord delta;       /* Change indicated to get_dir() */
extern const char *spacemsg;
extern const char *morestr;
extern const char *retstr;
extern LEVTYPE levtype;
extern const char *rainbow[NCOLORS];
extern const char *sylls[NSYLLS];
extern const char *stones[NSTONES];
extern const char *metal[NMETAL];
extern const char *wood[NWOOD];
extern coord ch_ret;
extern int demoncnt;
extern delayed_action d_list[MAXDAEMONS];
extern int between;
extern char prbuf[2 * LINELEN];
extern int inbag;
extern char bag_letters[];
extern char *bag_index;
extern char *bag_end;
extern char pack_letters[];
extern char *pack_index;
extern char *pack_end;
extern char version[];
extern int oldcol;
extern int oldline;
extern const char *oversion;

int md_getuid();
void md_init();
int md_readchar(WINDOW *win);
void md_onsignal_autosave();
void md_onsignal_exit();
void md_onsignal_default();
int md_issymlink(char *sp);
unsigned int md_htonl(unsigned int);
unsigned int md_ntohl(unsigned int);
extern void md_dobinaryio();
extern char *md_getpass();

struct EFILE {
    FILE *fp;
    unsigned int efp_cksum;
    unsigned int efp_seed;
    int efp_iomode;
    int efp_error;
};

void efclearerr(EFILE *efp);
void efseterr(EFILE *efp, int err);
int eferror(EFILE *efp);
EFILE *efopen(const char *filename, const char *mode);
int efclose(EFILE *efp);
size_t efread(void *ptr, size_t size, int nitems, EFILE *efp);
size_t efread(void *ptr, size_t size, size_t nitems, EFILE *efp);
size_t efwrite(const void *ptr, size_t size, int nitems, EFILE *efp);
size_t efwrite(const void *ptr, size_t size, size_t nitems, EFILE *efp);
size_t efwriten(const void *ptr, size_t size, EFILE *efp);
size_t efreadn(void *ptr, size_t size, EFILE *efp);

extern char PERMOK[];
extern char PASSCTL[];
extern char SCOREDIR[];
extern char NEWS[];
extern char ROGDEFS[];
