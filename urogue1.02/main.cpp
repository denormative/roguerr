/*
        main.c  -  Rogue

        UltraRogue
        Copyright (C) 1985 Herb Chong
        All rights reserved.

        Based on "Advanced Rogue"
        Copyright (C) 1982, 1984, 1985 Michael Morgan, Ken Dalka and AT&T
        All rights reserved.

        Based on "Rogue: Exploring the Dungeons of Doom"
        Copyright (C) 1980, 1981 Michael Toy, Ken Arnold and Glenn Wichman
        All rights reserved.

        See the file LICENSE.TXT for full copyright and licensing information.
*/

#include "rogue.h"
#include <csignal>
#include <cstdio>
#include <ctime>
#include <unistd.h>

FILE *fd_score = nullptr; /* file descriptor the score file */

int main(int argc, char *argv[]) {
    char *env;
    linked_list *item;
    object *obj;
    int lowtime, wpt = 0, i, j, hpadd, dmadd;
    int alldone, predef;
    time_t now;

    md_init();

    areuok(PERMOK); /***another void here ***/
#ifdef SIGQUIT
    signal(SIGQUIT, SIG_IGN);
#endif
    /*
     * get home and options from environment
     */
    strncpy(home, md_gethomedir(), LINELEN);

    /* Get default save file */
    strcpy(save_file_name, home);
    strcat(save_file_name, "rogue.save");

    /* Get default score file */
    strcpy(score_file, SCOREDIR);

    /*
     * Grab a file descriptor to the score file before the
     * effective uid and gid are reset to the real ones.
     */
    if((fd_score = fopen(score_file, "rb+")) == nullptr) {
        fd_score = fopen(score_file, "wb+");
    }

    if((env = getenv("SROGUEOPTS")) != nullptr) {
        parse_opts(env);
    }
    if(env == nullptr || whoami[0] == '\0') {
        strucpy(whoami, md_getusername(), strlen(md_getusername()));
    }

    if(whoami[0] == '\0') {
        printf("Say, who the hell are you?\n");
        printf("You can't run without a name.\n");
        sleep(4);
        exit(1);
    }

    if(env == nullptr || fruit[0] == '\0') {
        static const char *funfruit[] = {
            "candleberry", "caprifig",  "dewberry", "elderberry", "gooseberry",
            "guanabana",   "hagberry",  "ilama",    "imbu",       "jaboticaba",
            "jujube",      "litchi",    "mombin",   "pitanga",    "prickly pear",
            "rambutan",    "sapodilla", "soursop",  "sweetsop",   "whortleberry"};

        md_srand((int)time(nullptr) + 255);
        strcpy(fruit, funfruit[rnd(sizeof(funfruit) / sizeof(funfruit[0]))]);
    }

    /* put a copy of fruit in the right place */
    fd_data[1].mi_name = fruit;

    /*
     * check for print-score option
     */
    if(argc == 2 && strcmp(argv[1], "-s") == 0) {
        waswizard = TRUE;
        score(0, SCOREIT, 0);
        exit(0);
    }

    /*
     * check for news option
     */
    if(argc == 2 && strcmp(argv[1], "-n") == 0) {
        // FIXME: print 'NEWS'; this used to exec() but let's not.
        exit(0);
    }

    /*
     * Check to see if he is a wizard
     */
    makesure();
    if(argc >= 2 && argv[1][0] == '\0') {
        if(strcmp(PASSWD, md_crypt(md_getpass("Wizard's password: "))) == 0) {
            if(canwizard) {
#ifdef SIGQUIT
                signal(SIGQUIT, SIG_DFL);
#endif
                wizard = TRUE;
                argv++;
                argc--;
            } else {
                printf("Well that's odd...\n");
            }
        }
    }

    if(argc == 2 && argv[1][0] != '\0') {
        if(!restore(argv[1])) { /* Note: restore returns on error only */
            exit(1);
        }
    }
    lowtime = (int)time(&now);
    dnum = (wizard && getenv("SEED") != nullptr ? atoi(getenv("SEED")) : lowtime);
    if(wizard) {
        printf("Hello %s, welcome to dungeon #%d", whoami, dnum);
    } else {
        printf("Hello %s, just a moment while I dig the dungeon...", whoami);
    }
    fflush(stdout);
    seed = dnum;
    md_srand(seed);

    init_things();    /* Set up probabilities of things */
    init_fd();        /* Set up food probabilities */
    init_colors();    /* Set up colors of potions */
    init_stones();    /* Set up stone settings of rings */
    init_materials(); /* Set up materials of wands */
    initscr();        /* Start up cursor package */
    init_names();     /* Set up names of scrolls */
    setup();

    /*
     * Set up windows
     */
    cw = newwin(LINES, COLS, 0, 0);
    mw = newwin(LINES, COLS, 0, 0);
    hw = newwin(LINES, COLS, 0, 0);
    msgw = newwin(4, COLS, 0, 0);
    keypad(cw, 1);
    keypad(msgw, 1);

    predef = geta_player();
    waswizard = wizard;
re_roll:
    if(!predef) {
        init_player(); /* Roll up the rogue */
    } else {
        goto get_food; /* Using a pre-rolled rogue */
    }
    /*
     * Give the rogue his weaponry.
     */
    alldone = FALSE;
    do {
        int ch = 0;
        i = rnd(16); /* number of acceptable weapons */
        switch(i) {
            case 0:
                ch = 25;
                wpt = MACE;
                break;
            case 1:
                ch = 25;
                wpt = SWORD;
                break;
            case 2:
                ch = 15;
                wpt = TWOSWORD;
                break;
            case 3:
                ch = 10;
                wpt = SPEAR;
                break;
            case 4:
                ch = 20;
                wpt = TRIDENT;
                break;
            case 5:
                ch = 20;
                wpt = SPETUM;
                break;
            case 6:
                ch = 20;
                wpt = BARDICHE;
                break;
            case 7:
                ch = 15;
                wpt = SPIKE;
                break;
            case 8:
                ch = 15;
                wpt = BASWORD;
                break;
            case 9:
                ch = 20;
                wpt = HALBERD;
                break;
            case 10:
                ch = 20;
                wpt = BATTLEAXE;
                break;
            case 11:
                ch = 20;
                wpt = GLAIVE;
                break;
            case 12:
                ch = 20;
                wpt = LPIKE;
                break;
            case 13:
                ch = 20;
                wpt = BRSWORD;
                break;
            case 14:
                ch = 20;
                wpt = CRYSKNIFE;
                break;
            case 15:
                ch = 20;
                wpt = CLAYMORE;
        }
        if(rnd(100) < ch) { /* create this weapon */
            alldone = TRUE;
        }
    } while(!alldone);
    hpadd = rnd(2) + 1;
    dmadd = rnd(2) + 1;
    if(player.t_ctype == C_FIGHTER) {
        if(rnd(100) > 50) {
            wpt = TWOSWORD;
        } else {
            wpt = CLAYMORE;
        }
        hpadd = hpadd - 1;
    }
    /*
     * Find out what the armor is.
     */
    i = rnd(100) + 1;
    j = 0;
    while(armors[j].a_prob < i) {
        j++;
    }
    /*
     * See if this rogue is acceptable to the player.
     */
    if(!puta_player(j, wpt, hpadd, dmadd)) {
        goto re_roll;
    }
    /*
     * It's OK. Add this stuff to the rogue's pack.
     */
    item = spec_item(WEAPON, wpt, hpadd, dmadd);
    obj = (object *)ldata(item);
    obj->o_flags |= ISKNOW;
    add_pack(item, TRUE);
    cur_weapon = obj;
    /*
     * And his suit of armor
     */
    item = spec_item(ARMOR, j, 0, 0);
    obj = (object *)ldata(item);
    obj->o_flags |= ISKNOW;
    obj->o_weight = armors[j].a_wght;
    add_pack(item, TRUE);
    cur_armor = obj;
    /*
     * Give him some food too
     */
get_food:
    item = spec_item(FOOD, 0, 0, 0);
    obj = OBJPTR(item);
    obj->o_weight = things[TYP_FOOD].mi_wght;
    add_pack(item, TRUE);
    new_level(NORMLEV); /* Draw current level */
    /*
     * Start up daemons and fuses
     */
    start_daemon(d_doctor, &player, AFTER);
    fuse(d_swander, nullptr, WANDERTIME, AFTER);
    start_daemon(d_stomach, nullptr, AFTER);
    start_daemon(d_runners, nullptr, AFTER);
    playit();
}

/*
 * endit:
 *     Exit the program abnormally.
 */
[[noreturn]] void endit(int sig) {
    sig = 0; // silence warning
    fatal("Ok, if you want to exit that badly, I'll have to allow it\n");
}

/*
 * fatal:
 *     Exit the program, printing a message.
 */
[[noreturn]] void fatal(const char *s) {
    clear();
    move(LINES - 2, 0);
    printw("%s", s);
    draw(stdscr);
    endwin();
    printf("\n"); /* So the cursor doesn't stop at the end of the line */
    exit(0);
}

/*
 * rnd:
 *     Pick a very random number.
 */
int rnd(int range) {
    return (range == 0 ? 0 : md_rand() % range);
}

/*
 * roll:
 *     roll a number of dice
 */
int roll(int number, int sides) {
    int dtotal = 0;

    while(number--) {
        dtotal += rnd(sides) + 1;
    }
    return dtotal;
}

/*
 * handle stop and start signals
 */

#ifdef SIGTSTP
void tstp(int sig) {
    mvcur(0, COLS - 1, LINES - 1, 0);
    endwin();
    fflush(stdout);
    signal(SIGTSTP, SIG_IGN);
    kill(sig, SIGTSTP);
    signal(SIGTSTP, tstp);
    crmode();
    noecho();
    clearok(curscr, TRUE);
    touchwin(cw);
    draw(cw);
    flushinp();
}
#endif

void setup() {
#ifdef SIGHUP
    signal(SIGHUP, auto_save);
#endif
#ifdef SIGINT
    signal(SIGINT, quit);
#endif
#ifdef SIGTSTP
    signal(SIGTSTP, tstp);
#endif

    crmode(); /* Cbreak mode */
    noecho(); /* Echo off */
}

/*
 * playit:
 *     The main loop of the program.  Loop until the game is over,
 * refreshing things and looking at the proper times.
 */

[[noreturn]] void playit() {
    /*
     * set up defaults for slow terminals
     */

    char_type = player.t_ctype;

    player.t_oldpos = hero;
    oldrp = roomin(&hero);
    after = TRUE;
    while(playing) {
        command(); /* Command execution */
    }
    endit(0);
}

int makesure() {
    FILE *fd;
    int i;

    canwizard = FALSE;
    if((fd = fopen(PASSCTL, "rb")) == nullptr) {
        return (FALSE);
    }

    for(i = 0; i < 20; i++) {
        rpass.rp_pass[i] = 0;
    }
    for(i = 0; i < 2; i++) {
        rpass.rp_pkey[i] = 0;
    }

    encread(rpass.rp_pkey, sizeof(rpass.rp_pkey), fd);
    encread(rpass.rp_pass, sizeof(rpass.rp_pass), fd);

    if((*(rpass.rp_pkey) != 0) && (*(rpass.rp_pass) != 0)) {
        canwizard = TRUE;
    }

    fclose(fd);
    return (FALSE);
}

void areuok(char *file) {
    FILE *fd;

    if((fd = fopen(file, "rb")) == nullptr) {
        return;
    }

    encread((char *)auth_or, sizeof(auth_or), fd);

    fclose(fd);
}
