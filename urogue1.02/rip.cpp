/*
    rip.c  -  File for the fun ends. Death or a total win

    UltraRogue
    Copyright (C) 1985 Herb Chong
    All rights reserved.

    Based on "Advanced Rogue"
    Copyright (C) 1982, 1984, 1985 Michael Morgan, Ken Dalka and AT&T
    All rights reserved.

    Based on "Rogue: Exploring the Dungeons of Doom"
    Copyright (C) 1980, 1981 Michael Toy, Ken Arnold and Glenn Wichman
    All rights reserved.

    See the file LICENSE.TXT for full copyright and licensing information.
*/

#include "Scores.h"
#include <fmt/format.h>
#include "rogue.h"
#include <csignal>
#include <ctime>
#include <sys/types.h>

static const char *rip[15] = {"                       __________",
                              "                      /          \\",
                              "                     /    REST    \\",
                              "                    /      IN      \\",
                              "                   /     PEACE      \\",
                              "                  /                  \\",
                              "                  |                  |",
                              "                  |                  |",
                              "                  |    killed by     |",
                              "                  |                  |",
                              "                  |       1980       |",
                              "                 *|     *  *  *      | *",
                              R"(         ________)/\\_//(\/(/\)/\//\/|_)_______)",
                              nullptr};

/*
 * death:
 *     Do something really fun when he dies
 */
void death(int monst) {
    const char **dp = rip, *killer; /**replace w rip**/
    struct tm *lt;
    time_t date;
    char buf[80];

    if(ISWEARING(R_RESURRECT)) {
        int die = TRUE;

        if(resurrect-- == 0) {
            msg("You've run out of lives.");
        } else if(!save_resurrect(ring_value(R_RESURRECT))) {
            msg("Your attempt to return from the grave fails.");
        } else {
            linked_list *item;
            object *obj;
            int rm;
            unsigned int flags;
            coord pos;

            die = FALSE;
            msg("You feel sudden warmth and then nothingness.");
            teleport();
            if(ring_value(R_RESURRECT) > 1 && rnd(10)) {
                pstats.s_hpt = 2 * pstats.s_const;
                pstats.s_const = max(pstats.s_const - 1, 3);
            } else {
                item = pack;
                while(item != nullptr) {
                    obj = (object *)ldata(item);
                    flags = obj->o_flags;
                    obj->o_flags &= ~ISCURSED;
                    dropcheck(obj);
                    obj->o_flags = flags;
                    detach(pack, item);
                    freeletter(item);
                    attach(lvl_obj, item);
                    inpack--;
                    if(obj->o_type == ARTIFACT) {
                        has_artifact &= ~(1 << obj->o_which);
                    }
                    do {
                        rm = rnd_room();
                        rnd_pos(&rooms[rm], &pos);
                    } while(!(winat(pos.y, pos.x) == FLOOR));
                    mvaddch(pos.y, pos.x, obj->o_type);
                    obj->o_pos = pos;
                    item = pack;
                }
                pstats.s_hpt = pstats.s_const;
                pstats.s_const = max(pstats.s_const - roll(2, 2), 3);
            }
            pstats.s_lvl = max(pstats.s_lvl, 1);
            no_command += 2 + rnd(4);
            if(on(player, ISHUH)) {
                lengthen(d_unconfuse, rnd(8) + HUHDURATION);
            } else {
                fuse(d_unconfuse, nullptr, rnd(8) + HUHDURATION, AFTER);
            }
            turn_on(player, ISHUH);
            light(&hero);
        }
        if(!die) {
            wmove(cw, (int)mpos, 0);
        }
        waddstr(cw, morestr);
        wrefresh(cw);
        wait_for(cw, ' ');
        return;
    }
    time(&date);
    lt = localtime(&date);
    clear();
    move(8, 0);
    while(*dp) {
        printw("%s\n", *dp++);
    }
    mvaddstr(14, 28 - (int)((strlen(whoami) + 1) / 2), whoami);
    sprintf(buf, "%d Points", pstats.s_exp);
    mvaddstr(15, 28 - (int)((strlen(buf) + 1) / 2), buf);
    killer = killname(monst);
    mvaddstr(17, 28 - (int)((strlen(killer) + 1) / 2), killer);
    sprintf(prbuf, "%4d", 1900 + lt->tm_year);
    mvaddstr(18, 26, prbuf);
    move(LINES - 1, 0);
    idenpack();
    refresh();
    score(pstats.s_exp, KILLED, monst);
    exit(0);
}

static const char *reason[] = {"killed", "quit", "a winner", "a total winner"};

ScoreEntry recordScore(const long amount, const int flags, const int monst) {
    ScoreEntry entry{};

    entry.sc_gold = purse;
    entry.sc_score = amount;
    entry.sc_name = fmt::format("{}, Level {} {}", md_getusername(), pstats.s_lvl,
                                cnames[player.t_ctype][min(pstats.s_lvl, 11) - 1]);
    if(flags == WINNER || flags == TOTAL) {
        entry.sc_level = max_level;
    } else {
        entry.sc_level = level;
    }
    entry.sc_monster = monst;
    entry.sc_flags = flags;
    entry.sc_reason = reason[flags];
    entry.sc_reason += fmt::format(" on level {}", entry.sc_level);
    entry.was_wizard = waswizard;
    entry.sc_artifacts = picked_artifact;
    // TODO: probably should note user was wizard on scoreboard

    return entry;
}

void printScores(const Scores &scores) {
    printf("\nTop Ten Adventurers:\nRank\tScore\tGold\t\tName\n");

    int rank = 1;
    for(auto &e : scores.scores) {

        printf("%d\t%ld\t%ld\t%s:\n", rank, e.sc_score, e.sc_gold, e.sc_name.c_str());
        if(e.sc_artifacts) {
            int i;
            int first = TRUE;
            string sthings;

            for(i = 0; i <= MAXARTIFACT; i++) {
                if(e.sc_artifacts & (1 << i)) {
                    if(!sthings.empty()) {
                        sthings += ", ";
                    }
                    if(first) {
                        sthings += "retrieved ";
                        first = FALSE;
                    }
                    sthings += arts[i].ar_name;
                }
            }
            if(!sthings.empty()) {
                printf("\t\t\t%s,", sthings.c_str());
            }
            putchar('\n');
        }
        printf("\t\t\t%s on level %ld", reason[e.sc_flags], e.sc_level);
        if(e.sc_flags == 0) {
            printf(" by");
            printf(" %s", killname((int)e.sc_monster));
        }
        printf(".\n");

        rank++;
    }
}

/* score -- figure score and post it. */
void score(int amount, int flags, int monst) {
    const char *packend;

    signal(SIGINT, byebye);
    if(flags != WINNER && flags != TOTAL && flags != SCOREIT) {
        if(flags == CHICKEN) {
            packend = "when you quit";
        } else {
            packend = "at your untimely demise";
        }
        mvaddstr(LINES - 1, 0, retstr);
        refresh();
        getstr(prbuf);
        showpack(packend);
    }

    signal(SIGINT, SIG_DFL);
    mvaddstr(LINES - 1, 0, retstr);
    refresh();
    fflush(stdout);
    getstr(prbuf);
    clear();
    refresh();
    endwin();
    Scores scores("urogue102.scores");
    scores.load();

    ScoreEntry entry = recordScore(amount, flags, monst);

    scores.addScore(entry);
    scores.save();

    printScores(scores);
}

[[noreturn]] void total_winner() {
    linked_list *item;
    object *obj;
    int worth, oldpurse;
    int c;
    linked_list *bag = nullptr;

    clear();
    standout();
    addstr("                                                               \n");
    addstr("  @   @               @   @           @          @@@  @     @  \n");
    addstr("  @   @               @@ @@           @           @   @     @  \n");
    addstr("  @   @  @@@  @   @   @ @ @  @@@   @@@@  @@@      @  @@@    @  \n");
    addstr("   @@@@ @   @ @   @   @   @     @ @   @ @   @     @   @     @  \n");
    addstr("      @ @   @ @   @   @   @  @@@@ @   @ @@@@@     @   @     @  \n");
    addstr("  @   @ @   @ @  @@   @   @ @   @ @   @ @         @   @  @     \n");
    addstr("   @@@   @@@   @@ @   @   @  @@@@  @@@@  @@@     @@@   @@   @  \n");
    addstr("                                                               \n");
    addstr("     Congratulations, you have made it to the light of day!    \n");
    standend();
    addstr("\nYou have joined the elite ranks of those who have escaped the\n");
    addstr("Dungeons of Doom alive.  You journey home and sell all your loot at\n");
    addstr("a great profit and are admitted to the fighters guild.\n");
    mvaddstr(LINES - 1, 0, spacemsg);
    refresh();
    wait_for(cw, ' ');
    clear();
    idenpack();
    oldpurse = purse;
    mvaddstr(0, 0, "   Worth  Item");
    for(c = 'a', item = pack; item != nullptr; c++, item = next(item)) {
        obj = (object *)ldata(item);
        worth = get_worth(obj);
        purse += worth;
        if(obj->o_type == ARTIFACT && obj->o_which == TR_PURSE) {
            bag = obj->art_stats.t_art;
        }
        mvprintw(c - 'a' + 1, 0, "%c) %8d  %s", c, worth, inv_name(obj, FALSE));
    }
    if(bag != nullptr) {
        mvaddstr(LINES - 1, 0, morestr);
        refresh();
        wait_for(cw, ' ');
        clear();
        mvprintw(0, 0, "Contents of the Magic Purse of Yendor:\n");
        for(c = 'a', item = bag; item != nullptr; item = next(item)) {
            obj = OBJPTR(item);
            worth = get_worth(obj);
            whatis(item);
            purse += worth;
            mvprintw(c - 'a' + 1, 0, "%c) %8d %s\n", c, worth, inv_name(obj, FALSE));
        }
    }
    mvprintw(c - 'a' + 1, 0, "   %6d  Gold Pieces          ", oldpurse);
    refresh();
    if(has_artifact == 255) {
        score(pstats.s_exp, TOTAL, 0);
    } else {
        score(pstats.s_exp, WINNER, 0);
    }
    exit(0);
}

const char *killname(int monst) {
    static char mons_name[80];

    if(monst >= 0) {
        switch(monsters[monst].m_name[0]) {
            case 'a':
            case 'e':
            case 'i':
            case 'o':
            case 'u':
                sprintf(mons_name, "an %s", monsters[monst].m_name);
                break;
            default:
                sprintf(mons_name, "a %s", monsters[monst].m_name);
        }
        return (mons_name);
    }
    switch(monst) {
        case D_ARROW:
            return "an arrow";
        case D_DART:
            return "a dart";
        case D_BOLT:
            return "a bolt";
        case D_POISON:
            return "poison"; /* Cursed healing potion */
        case D_POTION:
            return "a cursed potion";
        case D_PETRIFY:
            return "petrification";
        case D_SUFFOCATION:
            return "suffocation";
        case D_INFESTATION:
            return "a parasite";
        case D_DROWN:
            return "drowning";
        case D_FALL:
            return "falling";
        case D_FIRE:
            return "slow boiling in oil";
    }

    return "";
}

/* showpack: Display the contents of the hero's pack */
void showpack(const char *howso) {
    const char *iname;
    int cnt, worth, ch, oldpurse;
    linked_list *item;
    object *obj;
    linked_list *bag = nullptr;

    cnt = 1;
    clear();
    mvprintw(0, 0, "Contents of your pack %s:\n", howso);
    ch = 'a';
    oldpurse = purse;
    purse = 0;
    for(item = pack; item != nullptr; item = next(item)) {
        obj = OBJPTR(item);
        worth = get_worth(obj);
        whatis(item);
        purse += worth;
        if(obj->o_type == ARTIFACT && obj->o_which == TR_PURSE) {
            bag = obj->art_stats.t_art;
        }
        iname = inv_name(obj, FALSE);
        mvprintw(cnt, 0, "%c) %s\n", ch, iname);
        ch += 1;
        if(++cnt > LINES - 5 && next(item) != nullptr) {
            cnt = 1;
            mvaddstr(LINES - 1, 0, morestr);
            refresh();
            wait_for(cw, ' ');
            clear();
        }
    }
    if(bag != nullptr) {
        mvaddstr(LINES - 1, 0, morestr);
        refresh();
        wait_for(cw, ' ');
        clear();
        cnt = 1;
        ch = 'a';
        mvprintw(0, 0, "Contents of the Magic Purse of Yendor %s:\n", howso);
        for(item = bag; item != nullptr; item = next(item)) {
            obj = OBJPTR(item);
            worth = get_worth(obj);
            whatis(item);
            purse += worth;
            mvprintw(cnt, 0, "%c) %s\n", ch, inv_name(obj, FALSE));
            ch += 1;
            if(++cnt > LINES - 5 && next(item) != nullptr) {
                cnt = 1;
                mvaddstr(LINES - 1, 0, morestr);
                refresh();
                wait_for(cw, ' ');
                clear();
            }
        }
    }
    mvprintw(cnt + 1, 0, "Carrying %d gold pieces", oldpurse);
    mvprintw(cnt + 2, 0, "Carrying objects worth %d gold pieces", purse);
    purse += oldpurse;
    refresh();
}

[[noreturn]] void byebye(int sig) {
    if(!isendwin()) {
        clear();
        endwin();
    }
    printf("\n");
    exit(sig);
}

/* save_resurrect: chance of resurrection according to modifed D&D probabilities */
int save_resurrect(int bonus) {
    int need, adjust;

    adjust = pstats.s_const + bonus - luck;
    if(adjust > 17) {
        return TRUE;
    }
    if(adjust < 14) {
        need = 5 * (adjust + 5);
    } else {
        need = 90 + 2 * (adjust - 13);
    }
    return (roll(1, 100) < need);
}
